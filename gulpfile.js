'use strict';

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var path = require('path');
var webpackStream = require('webpack-stream');
var webpack = require('webpack');
var postcss = require('gulp-postcss');
var realFavicon = require ('gulp-real-favicon');
var fs = require('fs');

var webpackExtensions = ["", ".webpack.js", ".web.js", ".js", ".jsx"];

function displayCompilationIssues(stats) {
  if (stats.compilation.errors.length) {
    console.log('Errors:');
    console.log(stats.compilation.errors.toString());
  }

  if (stats.compilation.warnings.length) {
    console.log('Warnings:');
    console.log(stats.compilation.warnings.toString());
  }
}

// Watch
gulp.task('watch', ['wrapper-dev'], function() {

});

gulp.task('bundle', ['wrapper-prod'], function () {

});

// Wrapper
var wrapperAppFile = 'spa/index.js';
var wrapperPath = path.resolve(__dirname, 'spa/scripts');
var pluginPath = path.resolve(__dirname, 'spa/scripts/tools');

var wrapperLoaders = [
  {
    test: /\.js$/,
    include: [
      path.resolve(__dirname, "spa"),
      wrapperPath
    ],
    exclude: /(node_modules|bower_components)/,
    loader: 'babel',
    query: {
      presets: ['es2015', 'stage-0', 'react'],
      plugins: [
        [pluginPath + "/babelRelayPlugin"],
        ["transform-class-properties"],
        ["transform-decorators-legacy"],
        'transform-es2015-block-scoping',
        ['transform-es2015-classes', {loose: 'true'}],
        'transform-proto-to-assign'
      ],
      cacheDirectory: false
    }
  },
  {
    test: /\.(jpe?g|png|gif|svg)$/i,
    loader: 'url'
  },
  {
    test: /\.css$/,
    loader: "style-loader!css-loader"
  },
];

// JS + styles for development without uglify
gulp.task("wrapper-dev", function () {
  return gulp.src(wrapperAppFile)
    .pipe(webpackStream({
      watch: true,
      output: {
        filename: 'bundle.js'
      },
      module: {
        loaders: wrapperLoaders
      },
      resolve: {
        extensions: webpackExtensions,
        alias: {
          'ie': 'component-ie'
        }
      },
      plugins: [
        new webpack.DefinePlugin({
          'process.env': {
            'NODE_ENV': '"development"'
          }
        })
      ]
    }, null, function (err, stats) {
      displayCompilationIssues(stats);

      fs.writeFile("./webpack.stats.json", JSON.stringify(stats.toJson()), function (err) {
        if (err) {
          console.log(err);
        }
      });
    }))
    .pipe(gulp.dest('spa/public/js/'));
});

// JS + styles for production (doesn't watch for changes) with uglify
gulp.task("wrapper-prod", function () {
  return gulp.src(wrapperAppFile)
    .pipe(webpackStream({
      watch: false,
      output: {
        filename: 'bundle.js'
      },
      module: {
        loaders: wrapperLoaders
      },
      resolve: {
        extensions: webpackExtensions,
        alias: {
          'ie': 'component-ie'
        }
      },
      plugins: [
        new webpack.DefinePlugin({
          'process.env': {
            'NODE_ENV': '"production"'
          }
        })
      ]
    }, null, function (err, stats) {
      displayCompilationIssues(stats);

      fs.writeFile("./webpack.stats.json", JSON.stringify(stats.toJson()), function (err) {
        if (err) {
          console.log(err);
        }
      });
    }))
    .pipe(uglify())
    .pipe(gulp.dest('spa/public/js/'));
});

// File where the favicon markups are stored
var FAVICON_DATA_FILE = 'faviconData.json';

// Generate the icons. This task takes a few seconds to complete.
// You should run it at least once to create the icons. Then,
// you should run it whenever RealFaviconGenerator updates its
// package (see the check-for-favicon-update task below).
gulp.task('generate-favicon', function(done) {
  realFavicon.generateFavicon({
    masterPicture: 'spa/source_favicon.png',
    dest: 'spa/public',
    iconsPath: '/',
    design: {
      ios: {
        pictureAspect: 'noChange',
        assets: {
          ios6AndPriorIcons: false,
          ios7AndLaterIcons: false,
          precomposedIcons: false,
          declareOnlyDefaultIcon: true
        }
      },
      desktopBrowser: {},
      windows: {
        pictureAspect: 'noChange',
        backgroundColor: '#fff',
        onConflict: 'override',
        assets: {
          windows80Ie10Tile: false,
          windows10Ie11EdgeTiles: {
            small: false,
            medium: true,
            big: false,
            rectangle: false
          }
        }
      },
      androidChrome: {
        pictureAspect: 'noChange',
        themeColor: '#fff',
        manifest: {
          name: 'Авто история',
          display: 'browser',
          orientation: 'notSet',
          onConflict: 'override',
          declared: true
        },
        assets: {
          legacyIcon: false,
          lowResolutionIcons: false
        }
      },
      safariPinnedTab: {
        pictureAspect: 'blackAndWhite',
        threshold: 40.9375,
        themeColor: '#fff'
      }
    },
    settings: {
      compression: 0,
      scalingAlgorithm: 'Mitchell',
      errorOnImageTooSmall: false
    },
    markupFile: FAVICON_DATA_FILE
  }, function() {
    done();
  });
});

// Inject the favicon markups in your HTML pages. You should run
// this task whenever you modify a page. You can keep this task
// as is or refactor your existing HTML pipeline.
gulp.task('inject-favicon-markups', function() {
  gulp.src([ 'spa/public/index.html' ])
    .pipe(realFavicon.injectFaviconMarkups(JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).favicon.html_code))
    .pipe(gulp.dest('spa/public'));
});

// Check for updates on RealFaviconGenerator (think: Apple has just
// released a new Touch icon along with the latest version of iOS).
// Run this task from time to time. Ideally, make it part of your
// continuous integration system.
gulp.task('check-for-favicon-update', function(done) {
  var currentVersion = JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).version;
  realFavicon.checkForUpdates(currentVersion, function(err) {
    if (err) {
      throw err;
    }
  });
});
