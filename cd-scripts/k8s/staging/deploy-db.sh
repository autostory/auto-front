#!/bin/bash

set -ex

cd "$(dirname "$0")/"

kubectl delete secret database-params; kubectl create -f ./secrets/database.yml

cat ./templates/postgres-deployment.yaml | kubectl apply -f -


