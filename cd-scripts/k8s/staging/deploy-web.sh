#!/bin/bash

set -ex

if [[ -z $1 ]]
then
    echo 'APP version tag is not specified.'
    exit 0
fi

DOCKER_REGESTRY_BASE=evtuhovdo

cd "$(dirname "$0")/"

kubectl delete secret database-params > /dev/null 2>&1 || true > /dev/null 2>&1; kubectl create -f ./secrets/database.yml
kubectl delete secret googl > /dev/null 2>&1 || true > /dev/null 2>&1; kubectl create -f ./secrets/googl.yml
kubectl delete secret mail-params > /dev/null 2>&1 || true > /dev/null 2>&1; kubectl create -f ./secrets/mail.yml
kubectl delete secret sms-params > /dev/null 2>&1 || true > /dev/null 2>&1; kubectl create -f ./secrets/sms.yml

kubectl delete secret ssl-certs-for-frontend > /dev/null 2>&1 || true > /dev/null 2>&1; kubectl create secret generic ssl-certs-for-frontend \
    --from-file=./secrets/ssl/domain.crt --from-file=./secrets/ssl/domain.key

kubectl delete secret ssh-keys-for-backup > /dev/null 2>&1 || true > /dev/null 2>&1; kubectl create secret generic ssh-keys-for-backup \
    --from-file=./secrets/ssh-keys-for-backups/id_rsa

docker build --tag=${DOCKER_REGESTRY_BASE}/ah-php:$1 ./../../../docker/php --build-arg git_tag=$1 \
    && docker push ${DOCKER_REGESTRY_BASE}/ah-php:$1

docker build --tag=${DOCKER_REGESTRY_BASE}/ah-nginx:$1 ./../../../docker/nginx --build-arg git_tag=$1 \
    && docker push ${DOCKER_REGESTRY_BASE}/ah-nginx:$1

docker build --tag=${DOCKER_REGESTRY_BASE}/ah-cron:$1 ./../../../docker/cron \
    && docker push ${DOCKER_REGESTRY_BASE}/ah-cron:$1

cat ./templates/web-deployment-template.yaml | sed "s/<VERSION>/$1/g" | sed "s/<DOCKER_REGESTRY_BASE>/${DOCKER_REGESTRY_BASE}/g" | kubectl apply -f -


