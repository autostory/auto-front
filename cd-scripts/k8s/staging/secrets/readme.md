## Для подключения php к БД
1. Создать database.yml из database.yml.dist
2. Значения заполнять в base64  ```echo -n "postgres" | base64```
3. Создание секрета ```kubectl create -f ./database.yml```
  
## Для предоставления ssl сертификатов домена для nginx
1. Положить файлы domain.crt и domain.key в папку ssl
2. Создание секрета
```kubectl create secret generic ssl-certs-for-frontend --from-file=./domain.crt --from-file=./domain.key```
  
## SQL Proxy (не используется на данный момент)
1. Создать сервисный аккаунт Cloud Sql Client скачать credentials.json
2. kubectl create secret generic sql-proxy-credentials --from-file=credentials.json=./credentials.json

https://github.com/GoogleCloudPlatform/kubernetes-engine-samples/blob/master/cloudsql/postgres_deployment.yaml
  
