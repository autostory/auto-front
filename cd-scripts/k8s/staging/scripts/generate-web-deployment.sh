#!/bin/bash

if [[ -z $1 ]]
then
   echo 'APP version number tag is not specified.'
   exit 0
fi

if [[ -z $2 ]]
then
   echo 'Google project name is not specified.'
   exit 0
fi

cd "$(dirname "$0")/../"
cat ./templates/web-deployment-template.yaml | sed "s/<VERSION>/$1/g" | sed "s/<PROJECT>/$2/g"