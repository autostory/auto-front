Тут в конфигах указан внешний IP, если что его можно просто поменять.

kubectl get services

NAME                        CLUSTER-IP      EXTERNAL-IP    PORT(S)         AGE
ah-prod-load-balancer-443   10.11.252.114   35.187.46.52   443:31956/TCP   32s
ah-prod-load-balancer-80    10.11.246.24    35.187.46.52   80:32356/TCP    2m
kubernetes                  10.11.240.1     <none>         443/TCP         8h
