--- Загрузить дам в mysql
--- выгрузить из mysql в postgres
--- pgloader mysql://root:root@127.0.0.1:8889/car_1 postgresql://postgres:111@localhost:5433/ah_prod
--- pgloader mysql://root:root@127.0.0.1:8889/car_2 postgresql://postgres:1111@localhost:5433/ah_prod
--- pgloader mysql://root:root@127.0.0.1:8889/car_3 postgresql://postgres:1111@localhost:5433/ah_prod
--- И уже потом выполнить sql код ниже
--- П.С. brew install pgloader

set session "myapp.user_id" = "0";

BEGIN;

UPDATE car SET
catalog_car_equipment_id = NULL,
catalog_car_generation_id = NULL,
catalog_car_mark_id = NULL,
catalog_car_model_id = NULL,
catalog_car_modification_id = NULL,
catalog_car_serie_id = NULL,
catalog_car_type_id = NULL;

DELETE FROM public.car_option_value;
DELETE FROM public.car_option;
DELETE FROM public.car_equipment;
DELETE FROM public.car_characteristic_value;
DELETE FROM public.car_modification;
DELETE FROM public.car_characteristic;
DELETE FROM public.car_serie;
DELETE FROM public.car_generation;
DELETE FROM public.car_model;
DELETE FROM public.car_mark;
DELETE FROM public.car_type;

COMMIT;

INSERT INTO public.car_type SELECT * FROM car_1.car_type;
INSERT INTO public.car_type SELECT * FROM car_2.car_type;
INSERT INTO public.car_type SELECT * FROM car_3.car_type;

INSERT INTO public.car_mark ("date_create", "date_update", "id_car_mark", "id_car_type", "name", "name_rus") SELECT "date_create", "date_update", "id_car_mark", "id_car_type", "name", "name_rus" FROM car_1.car_mark;
INSERT INTO public.car_mark ("date_create", "date_update", "id_car_mark", "id_car_type", "name", "name_rus") SELECT "date_create", "date_update", "id_car_mark", "id_car_type", "name", NULL FROM car_2.car_mark;
INSERT INTO public.car_mark ("date_create", "date_update", "id_car_mark", "id_car_type", "name", "name_rus") SELECT "date_create", "date_update", "id_car_mark", "id_car_type", "name", "name_rus" FROM car_3.car_mark;

INSERT INTO public.car_model ( "date_create", "date_update", "id_car_mark", "id_car_model", "id_car_type", "name", "name_rus" ) SELECT "date_create", "date_update", "id_car_mark", "id_car_model", "id_car_type", "name", "name_rus" FROM car_1.car_model;
INSERT INTO public.car_model ( "date_create", "date_update", "id_car_mark", "id_car_model", "id_car_type", "name", "name_rus" ) SELECT "date_create", "date_update", "id_car_mark", "id_car_model", "id_car_type", "name", NULL FROM car_2.car_model;
INSERT INTO public.car_model ( "date_create", "date_update", "id_car_mark", "id_car_model", "id_car_type", "name", "name_rus" ) SELECT "date_create", "date_update", "id_car_mark", "id_car_model", "id_car_type", "name", "name_rus" FROM car_3.car_model;

INSERT INTO public.car_generation ( "date_create", "date_update", "id_car_generation", "id_car_model", "id_car_type", "name", "year_begin", "year_end" ) SELECT "date_create", "date_update", "id_car_generation", "id_car_model", "id_car_type", "name", "year_begin", "year_end" FROM car_1.car_generation;

INSERT INTO public.car_serie ("date_create", "date_update", "id_car_generation", "id_car_model", "id_car_serie", "id_car_type", "name") SELECT "date_create", "date_update", "id_car_generation", "id_car_model", "id_car_serie", "id_car_type", "name" FROM car_1.car_serie;
INSERT INTO public.car_serie ("date_create", "date_update", "id_car_generation", "id_car_model", "id_car_serie", "id_car_type", "name") SELECT "date_create", "date_update", NULL, "id_car_model", "id_car_serie", "id_car_type", "name" FROM car_2.car_serie;
INSERT INTO public.car_serie ("date_create", "date_update", "id_car_generation", "id_car_model", "id_car_serie", "id_car_type", "name") SELECT "date_create", "date_update", NULL, "id_car_model", "id_car_serie", "id_car_type", "name" FROM car_3.car_serie;

INSERT INTO public.car_characteristic ("date_create", "date_delete", "date_update", "id_car_characteristic", "id_car_type", "id_parent", "name") SELECT "date_create", NULL, "date_update", "id_car_characteristic", "id_car_type", "id_parent", "name" FROM car_1.car_characteristic;
INSERT INTO public.car_characteristic ("date_create", "date_delete", "date_update", "id_car_characteristic", "id_car_type", "id_parent", "name") SELECT "date_create", "date_delete", "date_update", "id_car_characteristic", "id_car_type", "id_parent", "name" FROM car_2.car_characteristic;
INSERT INTO public.car_characteristic ("date_create", "date_delete", "date_update", "id_car_characteristic", "id_car_type", "id_parent", "name") SELECT "date_create", NULL, "date_update", "id_car_characteristic", "id_car_type", "id_parent", "name" FROM car_3.car_characteristic;

INSERT INTO public.car_modification ("date_create", "date_update", "end_production_year", "id_car_model", "id_car_modification", "id_car_serie", "id_car_type", "name", "start_production_year") SELECT "date_create", "date_update", "end_production_year", "id_car_model", "id_car_modification", "id_car_serie", "id_car_type", "name", "start_production_year" FROM car_1.car_modification;
INSERT INTO public.car_modification ("date_create", "date_update", "end_production_year", "id_car_model", "id_car_modification", "id_car_serie", "id_car_type", "name", "start_production_year") SELECT "date_create", "date_update", "end_production_year", "id_car_model", "id_car_modification", "id_car_serie", "id_car_type", "name", "start_production_year" FROM car_2.car_modification;
INSERT INTO public.car_modification ("date_create", "date_update", "end_production_year", "id_car_model", "id_car_modification", "id_car_serie", "id_car_type", "name", "start_production_year") SELECT "date_create", "date_update", "end_production_year", "id_car_model", "id_car_modification", "id_car_serie", "id_car_type", "name", "start_production_year" FROM car_3.car_modification;

INSERT INTO public.car_characteristic_value ( "date_create", "date_update", "id_car_characteristic", "id_car_characteristic_value", "id_car_modification", "id_car_type", "unit", "value" ) SELECT "date_create", "date_update", "id_car_characteristic", "id_car_characteristic_value", "id_car_modification", "id_car_type", "unit", "value" FROM car_1.car_characteristic_value;
INSERT INTO public.car_characteristic_value ( "date_create", "date_update", "id_car_characteristic", "id_car_characteristic_value", "id_car_modification", "id_car_type", "unit", "value" ) SELECT "date_create", "date_update", "id_car_characteristic", "id_car_characteristic_value", "id_car_modification", "id_car_type", "unit", "value" FROM car_2.car_characteristic_value;
INSERT INTO public.car_characteristic_value ( "date_create", "date_update", "id_car_characteristic", "id_car_characteristic_value", "id_car_modification", "id_car_type", "unit", "value" ) SELECT "date_create", "date_update", "id_car_characteristic", "id_car_characteristic_value", "id_car_modification", "id_car_type", "unit", "value" FROM car_3.car_characteristic_value;

INSERT INTO public.car_equipment ( "date_create", "date_update", "id_car_equipment", "id_car_modification", "id_car_type", "name", "price_min", "year" ) SELECT "date_create", "date_update", "id_car_equipment", "id_car_modification", "id_car_type", "name", "price_min", "year" FROM car_1.car_equipment;

INSERT INTO public.car_option ( "date_create", "date_update", "id_car_option", "id_car_type", "id_parent", "name" ) SELECT "date_create", "date_update", "id_car_option", "id_car_type", "id_parent", "name" FROM car_1.car_option;

INSERT INTO public.car_option_value ("date_create", "date_update", "id_car_equipment", "id_car_option", "id_car_option_value", "id_car_type", "is_base") SELECT "date_create", "date_update", "id_car_equipment", "id_car_option", "id_car_option_value", "id_car_type", "is_base" FROM car_1.car_option_value;




