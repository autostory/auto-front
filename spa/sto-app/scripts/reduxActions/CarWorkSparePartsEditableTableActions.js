import {
  SET_UNIT_TYPES,
  SET_MATERIAL_ADDED_ROW,
  SET_MATERIAL_EDITING_ROWS_IDS_CHANGES,
  SET_MATERIAL_ROW_CHANGES,
} from './../reduxConstants/actionTypes';

export function setUnitTypes(unitTypes) {
  return {
    type: SET_UNIT_TYPES,
    unitTypes,
  };
}

export function setRowChanges(rowChanges) {
  return {
    type: SET_MATERIAL_ROW_CHANGES,
    rowChanges,
  };
}

export function setEditingRowIds(editingRowIds) {
  return {
    type: SET_MATERIAL_EDITING_ROWS_IDS_CHANGES,
    editingRowIds,
  };
}

export function setAddedRows(addedRows) {
  return {
    type: SET_MATERIAL_ADDED_ROW,
    addedRows,
  };
}
