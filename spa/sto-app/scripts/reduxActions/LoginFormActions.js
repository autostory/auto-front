import {
  SET_LOGIN_FORM_LOGIN,
  SET_LOGIN_FORM_PASSWORD,
  SET_LOGIN_FORM_REQUEST_PENDING,
  SET_LOGIN_FORM_ERROR,
} from './../reduxConstants/actionTypes';

export function setLogin(login) {
  return {
    type: SET_LOGIN_FORM_LOGIN,
    login,
  };
}

export function setPassword(password) {
  return {
    type: SET_LOGIN_FORM_PASSWORD,
    password,
  };
}

export function setRequestPending(requestPending) {
  return {
    type: SET_LOGIN_FORM_REQUEST_PENDING,
    requestPending,
  };
}

export function setError(error) {
  return {
    type: SET_LOGIN_FORM_ERROR,
    error,
  };
}
