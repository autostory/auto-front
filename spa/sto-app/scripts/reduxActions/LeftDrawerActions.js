import {
  SET_LEFT_DRAWER_OPEN,
} from './../reduxConstants/actionTypes';

export function setOpen(open) {
  return {
    type: SET_LEFT_DRAWER_OPEN,
    open,
  };
}