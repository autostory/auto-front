import {
  SET_SCHEDULE_ADDED_ROW,
  SET_SCHEDULE_EDITING_ROWS_IDS_CHANGES,
  SET_SCHEDULE_ROW_CHANGES,
} from './../reduxConstants/actionTypes';

export function setRowChanges(rowChanges) {
  return {
    type: SET_SCHEDULE_ROW_CHANGES,
    rowChanges,
  };
}

export function setEditingRowIds(editingRowIds) {
  return {
    type: SET_SCHEDULE_EDITING_ROWS_IDS_CHANGES,
    editingRowIds,
  };
}

export function setAddedRows(addedRows) {
  return {
    type: SET_SCHEDULE_ADDED_ROW,
    addedRows,
  };
}
