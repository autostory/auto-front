import {
  SET_REQUEST_DIALOG_OPEN,
  SET_WFM_ACTIVE_OFFICE_ID,
  SET_REQUEST,
  SET_LAST_ORDER_CREATE_TIMESTAMP,
  SET_SHOW_ACTIVE_OFFICE_SELECTOR,
  SET_SHOW_REFRESH_BUTTON,
} from './../reduxConstants/actionTypes';

export function setRequestDialogOpen(requestDialogOpen) {
  return {
    type: SET_REQUEST_DIALOG_OPEN,
    requestDialogOpen,
  };
}

export function setRequest(request) {
  return {
    type: SET_REQUEST,
    request,
  };
}

export function setLastOrderCreateTimestamp(lastOrderCreateTimestamp) {
  return {
    type: SET_LAST_ORDER_CREATE_TIMESTAMP,
    lastOrderCreateTimestamp,
  };
}

export function setWFMActiveOfficeId(officeId) {
  return {
    type: SET_WFM_ACTIVE_OFFICE_ID,
    officeId,
  };
}

export function setShowActiveOfficeSelector(showActiveOfficeSelector) {
  return {
    type: SET_SHOW_ACTIVE_OFFICE_SELECTOR,
    showActiveOfficeSelector,
  };
}

export function setShowRefreshButton(showRefreshButton) {
  return {
    type: SET_SHOW_REFRESH_BUTTON,
    showRefreshButton,
  };
}
