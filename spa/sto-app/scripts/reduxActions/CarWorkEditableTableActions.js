import {
  SET_MANAGERS,
  SET_SERVICE_WORK_ACTION_TYPES,
  SET_SPARE_PART_NODES,
  SET_SERVICE_WORK_ADDED_ROW,
  SET_SERVICE_WORK_EDITING_ROWS_IDS_CHANGES,
  SET_SERVICE_WORK_ROW_CHANGES,
} from './../reduxConstants/actionTypes';

export function setManagers(managers) {
  return {
    type: SET_MANAGERS,
    managers,
  };
}

export function setServiceWorkActionsTypes(serviceWorkActionsTypes) {
  return {
    type: SET_SERVICE_WORK_ACTION_TYPES,
    serviceWorkActionsTypes,
  };
}

export function setSparePartNodes(sparePartNodes) {
  return {
    type: SET_SPARE_PART_NODES,
    sparePartNodes,
  };
}

export function setRowChanges(rowChanges) {
  return {
    type: SET_SERVICE_WORK_ROW_CHANGES,
    rowChanges,
  };
}

export function setEditingRowIds(editingRowIds) {
  return {
    type: SET_SERVICE_WORK_EDITING_ROWS_IDS_CHANGES,
    editingRowIds,
  };
}

export function setAddedRows(addedRows) {
  return {
    type: SET_SERVICE_WORK_ADDED_ROW,
    addedRows,
  };
}
