import {SET_PAGE_HEADER} from './../reduxConstants/actionTypes';

export function setPageHeader(header) {
  return {
    type: SET_PAGE_HEADER,
    header,
  };
}