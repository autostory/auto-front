import {
  SET_FIRM_CLIENTS_PHONE_FILTER,
  SET_FIRM_CLIENTS_FIO_FILTER,
  SET_FIRM_CLIENTS_EMAIL_FILTER,
  SET_FIRM_CLIENTS_REGISTRATION_SIGN_NUMBER_FILTER,
  SET_FIRM_CLIENTS_ID_FILTER,
  SET_FIRM_CLIENTS_ORDER_BY_FILTER,
  SET_FIRM_CLIENTS_ORDER_TYPE_FILTER,
} from './../reduxConstants/actionTypes';

export function setIdFilter(id) {
  return {
    type: SET_FIRM_CLIENTS_ID_FILTER,
    id,
  };
}

export function setOrberByFilter(orderBy) {
  return {
    type: SET_FIRM_CLIENTS_ORDER_BY_FILTER,
    orderBy,
  };
}

export function setOrderTypeFilter(orderType) {
  return {
    type: SET_FIRM_CLIENTS_ORDER_TYPE_FILTER,
    orderType,
  };
}

export function setEmailFilter(email) {
  return {
    type: SET_FIRM_CLIENTS_EMAIL_FILTER,
    email,
  };
}

export function setFioFilter(fio) {
  return {
    type: SET_FIRM_CLIENTS_FIO_FILTER,
    fio,
  };
}

export function setPhoneFilter(phone) {
  return {
    type: SET_FIRM_CLIENTS_PHONE_FILTER,
    phone,
  };
}

export function setRegistrationSignNumberFilter(registrationSignNumber) {
  return {
    type: SET_FIRM_CLIENTS_REGISTRATION_SIGN_NUMBER_FILTER,
    registrationSignNumber,
  };
}
