import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import withWidth from '@material-ui/core/withWidth';
import withStyles from '@material-ui/core/styles/withStyles';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';

import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import ScheduleIcon from '@material-ui/icons/Schedule';
import BuildIcon from '@material-ui/icons/Build';
import FirmIcon from '@material-ui/icons/BusinessCenter';
import InsertChart from '@material-ui/icons/InsertChart';
import AttachMoney from '@material-ui/icons/AttachMoney';
import { connect } from 'react-redux';

import Header from '../components/main/Header/index';
import LeftDrawer from '../components/main/LeftDrawer';
import themeDefault from '../mui-theme/DefaultTheme';

const styles = theme => ({
  container: {
    [theme.breakpoints.up('sm')]: {
      margin: '73px 10px 10px 10px',
    },
    margin: '58px 10px 10px 10px',
    paddingLeft: 0,
  },
  containerDesktopWithOpenedDrawer: {
    paddingLeft: 221,
  },
  containerDesktopWithClosedDrawer: {
    paddingLeft: 71,
  },
});

const menus = [
  {
    text: 'Статистика',
    icon: <InsertChart />,
    link: '/',
  },
  {
    text: 'Рабочий стол',
    icon: <ScheduleIcon />,
    link: '/wfm',
  },
  {
    text: 'Мои клиенты',
    icon: <PermIdentityIcon />,
    link: '/my-clients',
  },
  // {
  //   text: 'Заявки',
  //   icon: <PhoneIcon />,
  //   link: '/requests',
  // },
  {
    text: 'Заказы',
    icon: <BuildIcon />,
    link: '/orders',
  },
  {
    text: 'Моя фирма',
    icon: <FirmIcon />,
    link: '/my-firm',
  },
  {
    text: 'Транзакции',
    icon: <AttachMoney />,
    link: '/my-firm/payments',
  },
];

@connect(state => ({
  drawerOpen: state.leftDrawerStore.open,
}))
@withStyles(styles)
class App extends React.Component {
  static propTypes = {
    width: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']).isRequired,
  };

  render() {
    const { classes, width, drawerOpen } = this.props;

    return (
      <MuiThemeProvider theme={themeDefault}>
        <React.Fragment>
          <Header />
          <LeftDrawer menus={menus} />

          <div
            className={classNames([
              classes.container,
              (width !== 'xs' && drawerOpen) && classes.containerDesktopWithOpenedDrawer,
              (width !== 'xs' && !drawerOpen) && classes.containerDesktopWithClosedDrawer,
            ])}
          >
            {this.props.children}
          </div>
        </React.Fragment>
      </MuiThemeProvider>
    );
  }
}

export default withWidth()(App);
