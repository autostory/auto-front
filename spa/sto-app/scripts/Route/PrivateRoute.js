import React from 'react';
import { Route } from 'react-router-dom';
import EnsureLoggedInApp from '../components/Auth/EnsureLoggedInApp';
import App from '../containers/App';

class PrivateRoute extends React.PureComponent {
  render() {
    const { component: Component, ...rest } = this.props;

    return (
      <Route
        {...rest}
        render={props => (
          <EnsureLoggedInApp>
            <App {...props}>
              <Component {...props} />
            </App>
          </EnsureLoggedInApp>
        )}
      />
    );
  }
}

export default PrivateRoute;
