import {
  SET_SCHEDULE_ADDED_ROW,
  SET_SCHEDULE_EDITING_ROWS_IDS_CHANGES,
  SET_SCHEDULE_ROW_CHANGES,
} from './../reduxConstants/actionTypes';

const initialState = {
  editingRowIds: [],
  addedRows: [],
  rowChanges: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_SCHEDULE_ADDED_ROW: {
      return {
        ...state,
        addedRows: action.addedRows,
      };
    }
    case SET_SCHEDULE_EDITING_ROWS_IDS_CHANGES: {
      return {
        ...state,
        editingRowIds: action.editingRowIds,
      };
    }
    case SET_SCHEDULE_ROW_CHANGES: {
      return {
        ...state,
        rowChanges: action.rowChanges,
      };
    }
    default:
      return state;
  }
}
