import {
  SET_REQUEST_DIALOG_OPEN,
  SET_REQUEST,
  SET_LAST_ORDER_CREATE_TIMESTAMP,
  SET_WFM_ACTIVE_OFFICE_ID,
  SET_SHOW_ACTIVE_OFFICE_SELECTOR,
  SET_SHOW_REFRESH_BUTTON,
} from './../reduxConstants/actionTypes';

const initialState = {
  requestDialogOpen: false,
  request: null,
  officeId: null,
  lastOrderCreateTimestamp: new Date().getTime(),
  showActiveOfficeSelector: false,
  showRefreshButton: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_REQUEST_DIALOG_OPEN: {
      return {
        ...state,
        requestDialogOpen: action.requestDialogOpen,
      };
    }
    case SET_WFM_ACTIVE_OFFICE_ID: {
      return {
        ...state,
        officeId: action.officeId,
      };
    }
    case SET_SHOW_ACTIVE_OFFICE_SELECTOR: {
      return {
        ...state,
        showActiveOfficeSelector: action.showActiveOfficeSelector,
      };
    }
    case SET_LAST_ORDER_CREATE_TIMESTAMP: {
      return {
        ...state,
        lastOrderCreateTimestamp: action.lastOrderCreateTimestamp,
      };
    }
    case SET_SHOW_REFRESH_BUTTON: {
      return {
        ...state,
        showRefreshButton: action.showRefreshButton,
      };
    }
    case SET_REQUEST: {
      return {
        ...state,
        request: action.request,
      };
    }
    default:
      return state;
  }
}
