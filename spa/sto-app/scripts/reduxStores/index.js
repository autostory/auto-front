import firmClientsFilterStore from './firmClientsFilterStore';
import loginFormStore from './loginFormStore';
import wfmPageStore from './wfmPageStore';
import carWorkEditableTableStore from './carWorkEditableTableStore';
import carWorkSparePartsEditableTableStore from './carWorkSparePartsEditableTableStore';
import pageHeaderStore from './pageHeaderStore';
import orderScheduleEditableTableStore from './orderScheduleEditableTableStore';
import leftDrawerStore from './leftDrawerStore';

export default {
  firmClientsFilterStore,
  loginFormStore,
  wfmPageStore,
  carWorkEditableTableStore,
  carWorkSparePartsEditableTableStore,
  orderScheduleEditableTableStore,
  pageHeaderStore,
  leftDrawerStore,
};
