import {
  SET_UNIT_TYPES,
  SET_MATERIAL_ADDED_ROW,
  SET_MATERIAL_EDITING_ROWS_IDS_CHANGES,
  SET_MATERIAL_ROW_CHANGES,
} from './../reduxConstants/actionTypes';

const initialState = {
  unitTypes: [],
  editingRowIds: [],
  addedRows: [],
  rowChanges: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_UNIT_TYPES: {
      return {
        ...state,
        unitTypes: action.unitTypes,
      };
    }
    case SET_MATERIAL_ADDED_ROW: {
      return {
        ...state,
        addedRows: action.addedRows,
      };
    }
    case SET_MATERIAL_EDITING_ROWS_IDS_CHANGES: {
      return {
        ...state,
        editingRowIds: action.editingRowIds,
      };
    }
    case SET_MATERIAL_ROW_CHANGES: {
      return {
        ...state,
        rowChanges: action.rowChanges,
      };
    }
    default:
      return state;
  }
}
