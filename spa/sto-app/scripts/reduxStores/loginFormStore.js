import {
  SET_LOGIN_FORM_LOGIN,
  SET_LOGIN_FORM_PASSWORD,
  SET_LOGIN_FORM_REQUEST_PENDING,
  SET_LOGIN_FORM_ERROR,
} from './../reduxConstants/actionTypes';

const initialState = {
  login: '',
  password: '',
  requestPending: false,
  error: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_LOGIN_FORM_LOGIN: {
      return {
        ...state,
        login: action.login,
      };
    }
    case SET_LOGIN_FORM_PASSWORD: {
      return {
        ...state,
        password: action.password,
      };
    }
    case SET_LOGIN_FORM_REQUEST_PENDING: {
      return {
        ...state,
        requestPending: action.requestPending,
      };
    }
    case SET_LOGIN_FORM_ERROR: {
      return {
        ...state,
        error: action.error,
      };
    }
    default:
      return state;
  }
}
