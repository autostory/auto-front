import {
  SET_MANAGERS,
  SET_SERVICE_WORK_ACTION_TYPES,
  SET_SPARE_PART_NODES,
  SET_SERVICE_WORK_ADDED_ROW,
  SET_SERVICE_WORK_EDITING_ROWS_IDS_CHANGES,
  SET_SERVICE_WORK_ROW_CHANGES,
} from './../reduxConstants/actionTypes';
const initialState = {
  managers: [],
  serviceWorkActionsTypes: [],
  sparePartNodes: [],
  editingRowIds: [],
  addedRows: [],
  rowChanges: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_MANAGERS: {
      return {
        ...state,
        managers: action.managers,
      };
    }
    case SET_SERVICE_WORK_ACTION_TYPES: {
      return {
        ...state,
        serviceWorkActionsTypes: action.serviceWorkActionsTypes,
      };
    }
    case SET_SPARE_PART_NODES: {
      return {
        ...state,
        sparePartNodes: action.sparePartNodes,
      };
    }
    case SET_SERVICE_WORK_ADDED_ROW: {
      return {
        ...state,
        addedRows: action.addedRows,
      };
    }
    case SET_SERVICE_WORK_EDITING_ROWS_IDS_CHANGES: {
      return {
        ...state,
        editingRowIds: action.editingRowIds,
      };
    }
    case SET_SERVICE_WORK_ROW_CHANGES: {
      return {
        ...state,
        rowChanges: action.rowChanges,
      };
    }
    default:
      return state;
  }
}
