import {
  SET_FIRM_CLIENTS_EMAIL_FILTER,
  SET_FIRM_CLIENTS_FIO_FILTER,
  SET_FIRM_CLIENTS_PHONE_FILTER,
  SET_FIRM_CLIENTS_REGISTRATION_SIGN_NUMBER_FILTER,
  SET_FIRM_CLIENTS_ID_FILTER,
  SET_FIRM_CLIENTS_ORDER_BY_FILTER,
  SET_FIRM_CLIENTS_ORDER_TYPE_FILTER,
} from './../reduxConstants/actionTypes';

const initialState = {
  id: '',
  fio: '',
  email: '',
  phone: '',
  registrationSignNumber: '',
  orderBy: 'id',
  orderType: 'desc',
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_FIRM_CLIENTS_ORDER_BY_FILTER: {
      return {
        ...state,
        orderBy: action.orderBy,
      };
    }
    case SET_FIRM_CLIENTS_ORDER_TYPE_FILTER: {
      return {
        ...state,
        orderType: action.orderType,
      };
    }
    case SET_FIRM_CLIENTS_ID_FILTER: {
      return {
        ...state,
        id: action.id,
      };
    }
    case SET_FIRM_CLIENTS_EMAIL_FILTER: {
      return {
        ...state,
        email: action.email,
      };
    }
    case SET_FIRM_CLIENTS_FIO_FILTER: {
      return {
        ...state,
        fio: action.fio,
      };
    }
    case SET_FIRM_CLIENTS_PHONE_FILTER: {
      return {
        ...state,
        phone: action.phone,
      };
    }
    case SET_FIRM_CLIENTS_REGISTRATION_SIGN_NUMBER_FILTER: {
      return {
        ...state,
        registrationSignNumber: action.registrationSignNumber,
      };
    }
    default:
      return state;
  }
}
