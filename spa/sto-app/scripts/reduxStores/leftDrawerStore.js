import {
  SET_LEFT_DRAWER_OPEN,
} from './../reduxConstants/actionTypes';

const initialState = {
  open: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_LEFT_DRAWER_OPEN: {
      return {
        ...state,
        open: action.open,
      };
    }
    default:
      return state;
  }
}
