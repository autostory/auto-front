import { SET_PAGE_HEADER } from './../reduxConstants/actionTypes';

const initialState = {
  header: '',
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_PAGE_HEADER: {
      return {
        ...state,
        header: action.header,
      };
    }
    default:
      return state;
  }
}
