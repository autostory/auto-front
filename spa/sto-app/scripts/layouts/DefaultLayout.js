import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Drawer from '@material-ui/core/Drawer';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
// import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import { withRouter } from 'react-router';
import Paper from '@material-ui/core/Paper';

import { AutoSizer } from 'react-virtualized';

const style = {
  paper: {
    margin: '8px 0 8px 0',
    padding: 8,
    position: 'relative',
    top: 70,
    flex: '1 1 auto',
  },
  appBar: {
    position: 'fixed',
    top: 0,
    left: 0,
  },
};

@withRouter
class DefaultLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showDrawer: false };
  }

  handleToggle = () => this.setState({ showDrawer: !this.state.showDrawer });

  handleTouchTapToMain = () => {
    this.setState({ showDrawer: false });
    this.props.history.push('/');
  };

  handleTouchTapToWorkerUI = () => {
    this.setState({ showDrawer: false });
    this.props.history.push('/worker');
  };

  handleTouchTapToClientUI = () => {
    this.setState({ showDrawer: false });
    this.props.history.push('/carWorkList');
  };

  handleTouchTapToDoc = () => {
    this.setState({ showDrawer: false });
    this.props.history.push('/doc');
  };

  render() {
    return (
      <div>
        <AppBar
          title="Автоистория"
          style={style.appBar}
          onTitleTouchTap={this.handleTouchTap}
          onLeftIconButtonTouchTap={this.handleToggle}
        />
        <Drawer
          docked={false}
          open={this.state.showDrawer}
          onRequestChange={showDrawer => this.setState({ showDrawer })}
        >
          <MenuItem onTouchTap={this.handleTouchTapToMain}>Главная</MenuItem>
          <MenuItem onTouchTap={this.handleTouchTapToClientUI}>Клиент UI</MenuItem>
          <MenuItem onTouchTap={this.handleTouchTapToWorkerUI}>СТО UI</MenuItem>
          <Divider />
          <MenuItem onTouchTap={this.handleTouchTapToDoc}>Graphql Documentation</MenuItem>
        </Drawer>
        <Paper style={style.paper} zDepth={1} transitionEnabled={false}>
          { this.props.children }
        </Paper>
      </div>
    );
  }
}

export default DefaultLayout;
