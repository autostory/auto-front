import React from 'react';

import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import Relay from 'react-relay/classic';
import { withRouter } from 'react-router';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import map from 'lodash/map';
import get from 'lodash/get';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

import relayContainer from '../../../../common-code/decorators/relayContainer';
import ConfiguredRadium from '../../radium/ConfiguredRadium';
import {
  setRequestDialogOpen,
  setRequest,
  setLastOrderCreateTimestamp,
  setWFMActiveOfficeId,
  setShowActiveOfficeSelector,
} from '../../reduxActions/WfmPageActions';
import AddPaymentToOrderWMFDialog from './Dialogs/AddPaymentToOrderWMFDialog';
import AddSchedulesToOrderDialog from './Dialogs/AddSchedulesToOrderDialog';
import AddWorkToOrderDialog from './Dialogs/AddWorkToOrderDialog';
import CreateNewOrderDialog from './Dialogs/CreateNewOrderDialog/CreateNewOrderDialog';
import MarkWorkOnScheduleAsCompletedDialog from './Dialogs/MarkWorkOnScheduleAsCompletedDialog';
import DrawerRequestViewer from './DrawerRequestViewer';
import DrawerRequestViewerInner from './DrawerRequestViewer/DrawerRequestViewerInner';
import SingleFAB from './includes/SingleFAB';
import BoxOrdersSchedule from './Orders/BoxOrdersSchedule';
import BoxReleaseTime from './Orders/BoxReleaseTime';
import MyFirmOrdersWithoutSchedule from './Orders/MyFirmOrdersWithoutSchedule';
import OrderCountInParking from './Orders/OrderCountInParking';
import ParkingOrders from './Orders/ParkingOrders';
import MyFirmRequests from './Requests/MyFirmRequests';
import OrderCountWithoutSchedule from './Orders/OrderCountWithoutSchedule';
import ActiveRequestCount from './Requests/ActiveRequestCount';
import OrderCountWithSchedule from './Orders/OrderCountWithSchedule';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  paperRoot: {
    padding: 14,
    background: '#DFE2E7',
    borderRadius: 5,
    maxHeight: 'calc(100vh - 164px)',
    height: 'calc(100vh - 164px)',
    overflowX: 'auto',
  },
  boxPaperRoot: {
    padding: 14,
    background: '#DFE2E7',
    borderRadius: 5,
    overflowX: 'auto',
    height: '100%',
  },
});

const style = {
  table: {
    width: '100%',
  },
  boxTitle: {
    maxWidth: 250,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    textAlign: 'center',
  },
  header: {
    fontWeight: 'bold',
    verticalAlign: 'bottom',
    textAlign: 'center',
  },
  td: {
    verticalAlign: 'top',
    padding: '2px 8px 8px 2px',
    minWidth: 280,
    overflow: 'auto',
  },
  tdBox: {
    verticalAlign: 'top',
    padding: '2px 8px 8px 2px',
    minWidth: 280,
    overflow: 'auto',
    height: '100%',
  },
  container: {
    overflowX: 'auto',
    maxHeight: 'calc(100vh - 86px)',
    height: 'calc(100vh - 86px)',
    padding: '0 12px',
  },
  loading: {
    textAlign: 'center',
  },
  wfmLoading: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  box: {
    fontSize: 14,
    width: 250,
    color: '#22273D',
    boxShadow: '0 0 10px 0 #D2D7E0',
    borderRadius: 5,
    background: 'white',
    borderTop: '4px solid green',
    lineHeight: '17px',
    transition: 'transform 0.15s',
    marginBottom: 15,
    ':hover': {
      transform: 'scale(1.05)',
    },
  },
  boxAlert: {
    borderTop: '4px solid #DF526B',
  },
  boxContainer: {
    fontSize: 14,
    background: '#FFF',
    padding: '5px 10px',
  },
  boxHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    fontWeight: 'bold',
  },
  boxIcon: {
    width: 16,
    height: 16,
    position: 'relative',
    top: 3,
    color: '#909090',
  },
  boxData: {
    fontSize: 14,
    background: '#FFF',
    padding: '5px 10px',
    borderTop: '1px solid #dedede',
    borderRadius: '0 0 5px 5px',
  },
  requestActions: {
    display: 'flex',
    justifyContent: 'center',
  },
  callButton: {
    color: '#F5F5F5',
    width: '100%',
    margin: '10px 20px',
    backgroundColor: '#3D5C96',
    ':hover': {
      backgroundColor: '#5080cd',
    },
  },
  noBoxPaper: {
    padding: 24,
    background: '#ffe9ea',
    margin: '0 auto',
  },
  emptyFirmPaper: {
    width: 400,
    padding: 24,
    background: '#ffe9ea',
    margin: '0 auto',
  },
  doubleHeightColumnTitle: {
    height: 42,
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  columnTitle: {
    textAlign: 'center',
  },
  rowsContainer: {
    display: 'flex',
    width: 'calc(100vw - 110px)',
    height: '100%',
  },
  noBoxContainer: {
    width: 400,
    margin: 44,
  },
  boxOrdersContainer: {
    display: 'flex',
    flex: 1,
  },
};

const REFETCH_INTERVAL = 60 * 1000;

const SHOW_REQUESTS = false; // в mvp скрываем заявкм

@relayContainer(
  {
    viewer: () => Relay.QL`
        fragment on Viewer {
            myFirm {
                id
                offices {
                    itemsCount
                }
                ${OrderCountWithoutSchedule.getFragment('firm')}
                ${OrderCountWithSchedule.getFragment('firm')}
                ${ActiveRequestCount.getFragment('firm')}
                ${OrderCountInParking.getFragment('firm')}
                ${DrawerRequestViewerInner.getFragment('firm')}
            }
            office(officeId: $officeId) @include(if: $officeSelected) {
                id
                title
                boxes(active: true) {
                    id
                    title
                    ${BoxReleaseTime.getFragment('box')}
                }
                managers {
                    edges {
                        node {
                            id
                        }
                    }
                }
            }
        }
    `,
  },
  {
    officeId: null,
    officeSelected: false,
  },
)
@withRouter
@ConfiguredRadium
@withStyles(styles)
@connect(
  state => ({}),
  dispatch => ({
    setRequestDialogOpen: open => dispatch(setRequestDialogOpen(open)),
    setWFMActiveOfficeId: id => dispatch(setWFMActiveOfficeId(id)),
    setRequest: request => dispatch(setRequest(request)),
    setLastOrderCreateTimestamp: timestamp => dispatch(setLastOrderCreateTimestamp(timestamp)),
    setShowActiveOfficeSelector: value => dispatch(setShowActiveOfficeSelector(value)),
  }),
)
class WFM extends React.PureComponent {
  constructor(props) {
    super(props);

    if (props.officeId) {
      props.relay.setVariables({
        officeId: props.officeId,
        officeSelected: true,
      });
    }
  }

  state = {
    orderCreateDialogOpen: false,
    addPaymentDialogOpen: false,
    refreshHandler: null,
    addWorkToOrderDialogOpen: false,
    addSchedulesToOrderDialogOpen: false,
    markWorkOnScheduleAsCompletedDialogOpen: false,
    orderUsedInDialogWindow: null,
    scheduleUsedInDialogWindow: null,
  };

  updateWfmData = () => {
    if (!this.state.orderCreateDialogOpen) {
      this.props.relay.forceFetch();
    }
  };

  componentWillMount() {
    this.setState({
      refreshHandler: setInterval(this.updateWfmData, REFETCH_INTERVAL),
    });

    document.getElementsByTagName('html')[0].style.overflow = 'hidden';
  }

  componentWillUnmount = () => {
    if (this.state.refreshHandler !== null) {
      clearInterval(this.state.refreshHandler);
      this.setState({
        refreshHandler: null,
      });
    }

    document.getElementsByTagName('html')[0].style.overflow = '';
  };

  onOrderCreated = (orderId) => {
    this.props.setLastOrderCreateTimestamp(new Date().getTime());

    this.closeOrderCreateDialog();
  };

  onMakeOrderFromRequest = (orderId) => {
    this.props.setLastOrderCreateTimestamp(new Date().getTime());
    this.props.history.push(`/order/${orderId}`);
  };

  showRequestDialog = request => () => {
    this.props.setRequest(request);
    this.props.setRequestDialogOpen(true);
  };

  closeRequestDialog = () => {
    this.props.setRequestDialogOpen(false);
    setTimeout(() => {
      this.props.setRequest(null);
    }, 300);
  };

  closeOrderCreateDialog = () => {
    this.setState({
      orderCreateDialogOpen: false,
    });
  };

  showOrderCreateDialog = () => {
    this.setState({
      orderCreateDialogOpen: true,
    });
  };

  closeAddWorkToOrderDialog = () => {
    this.setState({
      orderUsedInDialogWindow: null,
      addWorkToOrderDialogOpen: false,
    });
  };

  openAddWorkToOrderDialog = (orderId) => {
    this.setState({
      orderUsedInDialogWindow: orderId,
      addWorkToOrderDialogOpen: true,
    });
  };

  openAddPaymentDialog = (orderId) => {
    this.setState({
      orderUsedInDialogWindow: orderId,
      addPaymentDialogOpen: true,
    });
  };

  closeAddPaymentDialogOpen = () => {
    this.setState({
      orderUsedInDialogWindow: null,
      addPaymentDialogOpen: false,
    });
  };

  closeMarkWorkOnScheduleAsCompletedDialog = () => {
    this.setState({
      scheduleUsedInDialogWindow: null,
      markWorkOnScheduleAsCompletedDialogOpen: false,
    });
  };

  openMarkWorkOnScheduleAsCompletedDialog = (scheduleId) => {
    this.setState({
      scheduleUsedInDialogWindow: scheduleId,
      markWorkOnScheduleAsCompletedDialogOpen: true,
    });
  };

  closeAddSchedulesToOrderDialog = () => {
    this.setState({
      orderUsedInDialogWindow: null,
      addSchedulesToOrderDialogOpen: false,
    });
  };

  openAddSchedulesToOrderDialog = (orderId) => {
    this.setState({
      orderUsedInDialogWindow: orderId,
      addSchedulesToOrderDialogOpen: true,
    });
  };

  renderLoading = () => <div style={style.loading}><CircularProgress /></div>;

  render() {
    const { classes } = this.props;
    const { office, myFirm } = this.props.viewer;
    const { offices } = myFirm;

    if (offices.itemsCount === 0) {
      return (
        <Paper style={style.emptyFirmPaper}>
          <div>
            У вашей фирмы не создано ни одного офиса.<br />
            Сначала необходимо:<br />
            1) добавить офисы фирмы.<br />
            2) добавить боксы офисам.<br />
            2) добавить сотрудников в офисы или назначить существующих к офисам.<br />
            <br />
            Перейдите на страницу <Link to="/my-firm">Моя фирма</Link> и добавьте необходимое.
          </div>
        </Paper>
      );
    }

    if (office && get(office, 'managers.edges', null) === null) {
      return (
        <Paper style={style.emptyFirmPaper}>
          У офиса <b>{office.title}</b> не назначено ни одного сотрудника.<br />
          Чтобы иметь возможность планировать загрузку боксов, офису следует указать сотрудников, на{' '}
          <Link to={`/my-firm/office/edit/${office.id}`}>странице редактирования офиса</Link>.
        </Paper>
      );
    }

    if (!office) {
      return <CircularProgress />;
    }

    const { boxes } = office;

    return (
      <Paper style={style.container}>
        {SHOW_REQUESTS && (
          <DrawerRequestViewer
            firm={myFirm}
            onDrawerClose={this.closeRequestDialog}
            onMakeOrderFromRequest={this.onMakeOrderFromRequest}
          />
        )}
        <CreateNewOrderDialog
          forceFetch={false}
          onOrderCreated={this.onOrderCreated}
          onRequestClose={this.closeOrderCreateDialog}
          open={this.state.orderCreateDialogOpen}
        />
        <MarkWorkOnScheduleAsCompletedDialog
          afterScheduleStatusChange={this.closeMarkWorkOnScheduleAsCompletedDialog}
          scheduleId={this.state.scheduleUsedInDialogWindow}
          onRequestClose={this.closeMarkWorkOnScheduleAsCompletedDialog}
          open={this.state.markWorkOnScheduleAsCompletedDialogOpen}
        />
        <AddWorkToOrderDialog
          onRequestClose={this.closeAddWorkToOrderDialog}
          orderId={this.state.orderUsedInDialogWindow}
          open={this.state.addWorkToOrderDialogOpen}
        />
        <AddSchedulesToOrderDialog
          onRequestClose={this.closeAddSchedulesToOrderDialog}
          orderId={this.state.orderUsedInDialogWindow}
          open={this.state.addSchedulesToOrderDialogOpen}
        />
        <AddPaymentToOrderWMFDialog
          onRequestClose={this.closeAddPaymentDialogOpen}
          orderId={this.state.orderUsedInDialogWindow}
          open={this.state.addPaymentDialogOpen}
        />

        <div style={style.rowsContainer}>
          {SHOW_REQUESTS && (
            <div>
              <div style={style.doubleHeightColumnTitle}>
                <div style={style.columnTitle}>
                  Заявки (<ActiveRequestCount firm={myFirm} officeId={office.id} />)
                </div>
              </div>
              <div style={style.td}>
                <Paper classes={{ root: classes.paperRoot }}>
                  <div>
                    <MyFirmRequests
                      renderLoading={this.renderLoading}
                      onRequestClick={this.showRequestDialog}
                    />
                  </div>
                </Paper>
              </div>
            </div>
          )}

          <div>
            <div style={style.doubleHeightColumnTitle}>
              <div style={style.columnTitle}>
                Заказы не запланированные (<OrderCountWithoutSchedule firm={myFirm} officeId={office.id} />)
              </div>
            </div>
            <div style={style.td}>
              <Paper classes={{ root: classes.paperRoot }}>
                <MyFirmOrdersWithoutSchedule
                  initialVariables={{
                    officeId: office.id,
                  }}
                  openAddWorkToOrderDialog={this.openAddWorkToOrderDialog}
                  openAddSchedulesToOrderDialog={this.openAddSchedulesToOrderDialog}
                  forceFetch
                  renderLoading={this.renderLoading}
                />
              </Paper>
            </div>
          </div>

          {boxes.length === 0 && (
            <div style={style.noBoxContainer}>
              <div>
                <Paper style={style.noBoxPaper}>
                  У офиса <b>{office.title}</b> нет боксов.<br />
                  Чтобы иметь возможность планировать загрузку боксов из следует добавить на{' '}
                  <Link to={`/my-firm/office/edit/${office.id}`}>странице редактирования офиса</Link>.
                </Paper>
              </div>
            </div>
          )}

          {boxes.length > 0 && (
            <div
              style={{
                display: 'flex',
                height: 'calc(100vh - 108px)',
                maxHeight: 'calc(100vh - 108px)',
                flexDirection: 'column',
              }}
            >
              <div style={style.columnTitle}>
                Рабочая зона (<OrderCountWithSchedule firm={myFirm} officeId={office.id} />)
              </div>
              <div style={style.boxOrdersContainer}>
                {map(boxes, box => (
                  <div
                    key={box.id}
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                    }}
                  >
                    <div style={style.columnTitle}>
                      <div style={style.boxTitle} title={box.title}>{box.title}</div>
                      <BoxReleaseTime box={box} />
                    </div>
                    <div style={style.tdBox}>
                      <Paper classes={{ root: classes.boxPaperRoot }}>
                        <BoxOrdersSchedule
                          initialVariables={{
                            boxId: box.id,
                          }}
                          openMarkWorkOnScheduleAsCompletedDialog={this.openMarkWorkOnScheduleAsCompletedDialog}
                          forceFetch
                          renderLoading={this.renderLoading}
                        />
                      </Paper>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          )}

          {boxes.length > 0 && (
            <div>
              <div style={style.doubleHeightColumnTitle}>
                <div style={style.columnTitle}>
                  Стоянка (<OrderCountInParking firm={myFirm} officeId={office.id} />)
                </div>
              </div>
              <div style={style.td}>
                <Paper classes={{ root: classes.paperRoot }}>
                  <ParkingOrders
                    forceFetch
                    initialVariables={{
                      officeId: office.id,
                    }}
                    openAddPaymentDialog={this.openAddPaymentDialog}
                    renderLoading={this.renderLoading}
                  />
                </Paper>
              </div>
            </div>
          )}
        </div>

        <SingleFAB>
          <Tooltip title="Создать заказ">
            <Button
              variant="fab"
              aria-label="Создать заказ"
              color="primary"
              onClick={this.showOrderCreateDialog}
            >
              <NoteAddIcon />
            </Button>
          </Tooltip>
        </SingleFAB>
      </Paper>
    );
  }
}

export default WFM;
