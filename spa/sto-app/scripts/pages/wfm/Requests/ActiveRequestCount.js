import React from 'react';
import Relay from 'react-relay/classic';
import relayContainer from '../../../../../common-code/decorators/relayContainer';
import ConfiguredRadium from '../../../radium/ConfiguredRadium';

@relayContainer({
  firm: () => Relay.QL`
      fragment on Firm {
          activeRequestCount: requestCount(active: true)
      }
  `,
})
@ConfiguredRadium
class ActiveRequestCount extends React.PureComponent {
  render() {
    return <span>{this.props.firm.activeRequestCount}</span>;
  }
}

export default ActiveRequestCount;
