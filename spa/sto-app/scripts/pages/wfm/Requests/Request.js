import React from 'react';
import PersonIcon from '@material-ui/icons/Person';
import PhoneIcon from '@material-ui/icons/Phone';
import CarIcon from '@material-ui/icons/DirectionsCar';
import Button from '@material-ui/core/Button';
import Relay from 'react-relay/classic';
import Typography from '@material-ui/core/Typography';
import RequestDate from './RequestDate';

import phoneFormat from '../../../../../common-code/utils/text/phoneFormat';
import relayContainer from '../../../../../common-code/decorators/relayContainer';
import ConfiguredRadium from '../../../radium/ConfiguredRadium';

const style = {
  listRow: {
    display: 'flex',
  },
  boxIconContainer: {
    marginRight: 6,
  },
  box: {
    fontSize: 14,
    width: 250,
    color: '#22273D',
    boxShadow: '0 0 10px 0 #D2D7E0',
    borderRadius: 5,
    background: 'white',
    borderTop: '4px solid green',
    lineHeight: '17px',
    transition: 'transform 0.15s',
    marginBottom: 15,
    ':hover': {
      transform: 'scale(1.05)',
    },
  },
  boxAlert: {
    borderTop: '4px solid #DF526B',
  },
  boxContainer: {
    fontSize: 14,
    background: '#FFF',
    padding: '5px 10px',
  },
  boxHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    fontWeight: 'bold',
  },
  boxIcon: {
    width: 16,
    height: 16,
    position: 'relative',
    top: 3,
    color: '#909090',
  },
  boxData: {
    fontSize: 14,
    background: '#FFF',
    padding: '5px 10px',
    borderTop: '1px solid #dedede',
    borderRadius: '0 0 5px 5px',
  },
  requestActions: {
    display: 'flex',
    justifyContent: 'center',
  },
  callButton: {
    color: '#F5F5F5',
    width: '100%',
    margin: '10px 20px',
    backgroundColor: '#3D5C96',
    ':hover': {
      backgroundColor: '#5080cd',
    },
  },
};

@relayContainer({
  request: () => Relay.QL`
      fragment on Request {
          ${RequestDate.getFragment('request')}
          car {
              displayName
          }
          client {
              fullName
              phone
          }
      }
  `,
})
@ConfiguredRadium
class Request extends React.PureComponent {
  render() {
    const {
      id, alert, onActionClick, statusName, request,
    } = this.props;

    const { client, car } = request;

    return (
      <div style={[style.box, alert && style.boxAlert]}>
        <div style={style.boxContainer}>
          <div style={style.boxHeader}>
            <div>{statusName}</div>
            <div>заявка №{id}</div>
          </div>
          <div style={style.boxHeader}>
            <div><RequestDate request={request} /></div>
          </div>
        </div>
        <div style={style.boxData}>
          <Typography style={style.listRow} component="div">
            <div style={style.boxIconContainer}><PersonIcon style={style.boxIcon} /></div>
            <span>{client.fullName}</span>
          </Typography>
          <Typography style={style.listRow} component="div">
            <div style={style.boxIconContainer}><PhoneIcon style={style.boxIcon} /></div>
            <span>{phoneFormat(client.phone)}</span>
          </Typography>
          <Typography style={style.listRow} component="div">
            <div style={style.boxIconContainer}><CarIcon style={style.boxIcon} /></div>
            {car.displayName}
          </Typography>
        </div>
        <div style={style.requestActions}>
          <Button
            color="primary"
            onClick={onActionClick}
            style={style.callButton}
          >
            <PhoneIcon />{' '}Позвонить
          </Button>
        </div>
      </div>
    );
  }
}

export default Request;

