import { format } from "date-fns";
import React from 'react';
import Countdown from 'react-countdown-now';
import Relay from 'react-relay/classic';
import TimeAgo from 'react-timeago';
import russianStrings from 'react-timeago/lib/language-strings/ru';
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter';
import get from 'lodash/get';
import { RECALL, NO_ANSWER } from '../../../../../common-code/constants/resultRequestCommunicationTypes';
import relayContainer from '../../../../../common-code/decorators/relayContainer';

const formatter = buildFormatter(russianStrings);

@relayContainer({
  request: () => Relay.QL`
      fragment on Request {
          makeAt {
              timestamp
          }
          lastCommunication {
              id
              resultId
              nextCommunicationDateTime {
                  timestamp
              }
              communicationDateTime {
                  timestamp
              }
          }
      }
  `,
})
class RequestDate extends React.PureComponent {
  rendererCountDown = ({ days, hours, minutes, completed }) => {
    if (completed) {
      const { relay } = this.props;
      relay.forceFetch();

      return null;
    }

    const date = [];

    if (days && days > 0) {
      date.push(`${days} д.`);
    }

    if (hours && hours > 0) {
      date.push(`${hours} ч.`);
    }

    if (minutes && minutes > 0) {
      date.push(`${minutes} мин.`);
    }

    return <span>{date.join(' ')}</span>;
  };

  render() {
    const { request } = this.props;

    const now = parseInt(Date.now() / 1000, 10);
    let nextCallTimestamp = null;
    const createTimestamp = request.makeAt.timestamp;
    const createDate = new Date();
    createDate.setTime(createTimestamp * 1000);

    const resultId = get(request, 'lastCommunication.resultId', null);
    if (resultId === RECALL || resultId === NO_ANSWER) {
      nextCallTimestamp = request.lastCommunication.nextCommunicationDateTime.timestamp;
    }

    if (!nextCallTimestamp) {
      return (
        <div>
          поступила {format(createDate, 'DD.MM.YY HH:mm')}<br />
          <TimeAgo date={createDate} formatter={formatter} minPeriod={10} />
        </div>
      );
    }

    const nextCommunicationDate = new Date();
    nextCommunicationDate.setTime(nextCallTimestamp * 1000);

    if (now < nextCallTimestamp) {
      return (
        <div>
          позвонить {format(nextCommunicationDate, 'DD.MM.YY HH:mm')}<br />
          через <Countdown date={nextCommunicationDate} renderer={this.rendererCountDown} zeroPadLength={1} />
        </div>
      );
    }

    return (
      <div>
        нужно было позвонить {format(nextCommunicationDate, 'DD.MM.YY HH:mm')}<br />
        <TimeAgo date={nextCommunicationDate} formatter={formatter} minPeriod={10} />
      </div>
    );
  }
}

export default RequestDate;

