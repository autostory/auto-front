import React from 'react';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';
import get from 'lodash/get';
import isUndefined from 'lodash/isUndefined';
import Typography from '@material-ui/core/Typography';

import relaySuperContainer from '../../../../../common-code/decorators/relaySuperContainer';
import Request from './Request';
import { fromGlobalId } from '../../../../../common-code/relay/globalIdUtils';
import DrawerRequestViewerInner from '../DrawerRequestViewer/DrawerRequestViewerInner';

@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          myFirm {
              requests(active: true) {
                  edges {
                      node {
                          ${Request.getFragment('request')}
                          id
                          makeAt {
                              formattedDateTime
                              timestamp
                          }
                          active
                          ${DrawerRequestViewerInner.getFragment('request')}
                          communicationHistory {
                              id
                              nextCommunicationDateTime {
                                  formattedDateTime
                                  timestamp
                              }
                              resultId
                          }
                      }
                  }
              }
          }
      }
  `,
})
class MyFirmRequests extends React.PureComponent {
  static propTypes = {
    onRequestClick: PropTypes.func.isRequired,
  };

  render() {
    const { viewer, onRequestClick } = this.props;
    const firm = viewer.myFirm;
    const requests = get(firm, 'requests.edges');

    if (isUndefined(requests)) {
      return <Typography style={{ textAlign: 'center' }} variant="subheading">нет заявок</Typography>;
    }

    return (
      <div>
        {requests.map((node) => {
          const request = get(node, 'node');
          if (!request.active) {
            return false;
          }

          let statusName = 'Новая';
          let alert = false;
          const { communicationHistory } = request;
          if (communicationHistory.length) {
            const lastCommunication = communicationHistory[request.communicationHistory.length - 1];
            if (lastCommunication.resultId === 'NO_ANSWER' || lastCommunication.resultId === 'RECALL') {
              statusName = 'Перезвонить';

              const now = new Date().getTime() / 1000;

              if (lastCommunication.nextCommunicationDateTime.timestamp < now) {
                alert = true;
              }
            }
          } else {
            const makeAtTimeStamp = request.makeAt.timestamp;
            const now = Date.now() / 1000;

            // Если заявка висит более 10 минут то она подсвечивается как просроченная
            if (makeAtTimeStamp + (10 * 60) < now) {
              alert = true;
            }
          }

          const onActionClick = onRequestClick(request);

          return (
            <Request
              key={request.id}
              alert={alert}
              statusName={statusName}
              request={request}
              id={fromGlobalId(request.id).id}
              onActionClick={onActionClick}
            />
          );
        })}
      </div>
    );
  }
}

export default MyFirmRequests;

