import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';

const styles = theme => ({
  root: {
    height: 380,
  },
  speedDial: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 3,
  },
});

class CreateFAB extends React.Component {
  state = {
    open: false,
    hidden: false,
    isMouseOnButton: false,
  };

  handleOpen = () => {
    if (!this.state.hidden) {
      this.setState({
        open: true,
      });
    }
  };

  handleClose = () => {
    this.setState({
      open: false,
      isMouseOnButton: false,
    });
  };
  onMouseEnter = () => {
    this.setState({
      isMouseOnButton: true,
    });

    return true;
  };

  render() {
    const { classes, actions } = this.props;
    const { hidden, open } = this.state;

    return (
      <SpeedDial
        ariaLabel="Действия"
        className={classes.speedDial}
        hidden={hidden}
        icon={<SpeedDialIcon />}
        onMouseEnter={this.onMouseEnter && this.handleOpen}
        onMouseLeave={this.handleClose}
        open={open}
      >
        {actions.map(action => (
          <SpeedDialAction
            key={action.name}
            icon={action.icon}
            tooltipTitle={action.name}
            onClick={action.onClick}
          />
        ))}
      </SpeedDial>
    );
  }
}

CreateFAB.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CreateFAB);
