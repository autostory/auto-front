import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  speedDial: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 3,
  },
});

class SingleFAB extends React.Component {
  render() {
    const { classes } = this.props;

    return (
      <div
        className={classes.speedDial}
      >
        {this.props.children}
      </div>
    );
  }
}

SingleFAB.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SingleFAB);
