import React from 'react';
import Relay from 'react-relay/classic';
import get from 'lodash/get';
import CircularProgress from '@material-ui/core/CircularProgress';
import relayContainer from '../../../../../common-code/decorators/relayContainer';
import ConfiguredRadium from '../../../radium/ConfiguredRadium';

@relayContainer(
  {
    firm: () => Relay.QL`
        fragment on Firm {
            orderCountInParking: orderCount(active: true, statuses: [WAIT_CAR_RETURN, WAIT_PAYMENT], officeId: $officeId) @include(if: $officeSelected)
        }
    `,
  },
  {
    officeId: null,
    officeSelected: false,
  },
)
@ConfiguredRadium
class OrderCountInParking extends React.PureComponent {
  constructor(props) {
    super(props);

    if (props.officeId) {
      props.relay.setVariables({
        officeId: props.officeId,
        officeSelected: true,
      });
    }
  }

  render() {
    const count = get(this.props, 'firm.orderCountInParking', null);

    if (count === null) {
      return <CircularProgress size={15} />;
    }

    return (
      <span>{count}</span>
    );
  }
}

export default OrderCountInParking;
