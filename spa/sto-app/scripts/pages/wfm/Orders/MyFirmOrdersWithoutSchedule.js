import filter from 'lodash/filter';
import indexOf from 'lodash/indexOf';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Relay from 'react-relay/classic';
import TimeIcon from '@material-ui/icons/AccessTime';
import ServiceIcon from '@material-ui/icons/Build';
import get from 'lodash/get';
import map from 'lodash/map';
import { withRouter } from 'react-router';
import Typography from '@material-ui/core/Typography';
import { setLastOrderCreateTimestamp } from '../../../reduxActions/WfmPageActions';
import SetOrderFactCarArriveDateTimeMutation from '../../../relay/mutation/order/SetOrderFactCarArriveDateTimeMutation';
import {
  WAIT_CAR_ARRIVE,
  WAIT_PLANING,
  WAIT_WORK_ADD,
} from '../../../../../common-code/constants/orderStatusTypes';

import relaySuperContainer from '../../../../../common-code/decorators/relaySuperContainer';
import Order from './Order';
import OrderActionButton from './OrderActionButton';
import setOrderFactCarArriveDateTime from './setOrderFactCarArriveDateTime';

const ALERT_AFTER = 3600; // 1 час
const REFETCH_INTERVAL = 60 * 1000;

const OrderStatuses = [WAIT_CAR_ARRIVE, WAIT_WORK_ADD, WAIT_PLANING];

@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          myFirm {
              orders(
                  active: true,
                  withActiveSchedules: false,
                  statuses: [WAIT_CAR_ARRIVE, WAIT_WORK_ADD, WAIT_PLANING],
                  officeId: $officeId,
                  after: null,
                  first: 100500
              ) {
                  edges {
                      node {
                          id
                          status
                          makeAt {
                              timestamp
                          }
                          ${SetOrderFactCarArriveDateTimeMutation.getFragment('order')}
                          ${Order.getFragment('order')}
                      }
                  }
              }
          }
      }
  `,
})
@connect(
  state => ({
    lastOrderCreateTimestamp: state.wfmPageStore.lastOrderCreateTimestamp,
  }),
  dispatch => ({
    setLastOrderCreateTimestamp: timestamp => dispatch(setLastOrderCreateTimestamp(timestamp)),
  }),
)
@withRouter
class MyFirmOrdersWithoutSchedule extends React.Component {
  static defaultProps = {
    lastOrderCreateTimestamp: null,
  };

  static propTypes = {
    lastOrderCreateTimestamp: PropTypes.number,
    openAddWorkToOrderDialog: PropTypes.func.isRequired,
    openAddSchedulesToOrderDialog: PropTypes.func.isRequired,
  };

  state = {
    refreshHandler: null,
    requestPending: false,
  };

  componentWillMount() {
    this.setState({
      refreshHandler: setInterval(this.props.relay.forceFetch, REFETCH_INTERVAL),
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.lastOrderCreateTimestamp !== this.props.lastOrderCreateTimestamp) {
      this.props.relay.forceFetch();
    } else {
      const { viewer, setLastOrderCreateTimestamp } = this.props;
      const orders = map(get(viewer, 'myFirm.orders.edges'), 'node');
      const ordersNotInNeededStatus = filter(orders, order => indexOf(OrderStatuses, order.status) >= 0);

      if (ordersNotInNeededStatus.length) {
        setLastOrderCreateTimestamp(Date.now());
        this.props.relay.forceFetch();
      }
    }
  }

  componentWillUnmount = () => {
    if (this.state.refreshHandler !== null) {
      clearInterval(this.state.refreshHandler);
      this.setState({
        refreshHandler: null,
      });
    }
  };

  onRequestEnd = () => {
    this.setState({
      requestPending: false,
    });
  };

  onActionClick = (orderId, anchor) => (event) => {
    if (anchor === 'service') {
      this.props.openAddWorkToOrderDialog(orderId);
    } else if (anchor === 'schedule') {
      this.props.openAddSchedulesToOrderDialog(orderId);
    }

    event.preventDefault();
    return false;
  };

  onCarArriveClick = order => (event) => {
    this.setState({
      requestPending: true,
    }, this.sendOrderFactCarArriveDateTime(order));

    event.preventDefault();
    return false;
  };

  sendOrderFactCarArriveDateTime = order => () => {
    const date = parseInt(Date.now() / 1000, 10);

    setOrderFactCarArriveDateTime(order, { factClientVisitDateTime: date })
      .then(this.onRequestEnd, this.onRequestEnd);
  };

  render() {
    const { viewer } = this.props;

    const { requestPending } = this.state;

    const firm = viewer.myFirm;
    const orders = map(get(firm, 'orders.edges'), 'node');
    const now = parseInt(Date.now() / 1000, 10);

    if (!orders.length) {
      return <Typography style={{ textAlign: 'center' }} variant="subheading">нет заказов</Typography>;
    }

    return (
      <div>
        {orders.map((order) => {
          let alert = false;
          if (now >= (order.makeAt.timestamp + ALERT_AFTER)) {
            alert = true;
          }

          return (
            <Order
              overdue={alert}
              key={order.id}
              order={order}
              scheduleInfo={null}
              alert={alert}
            >
              {order.status === WAIT_CAR_ARRIVE && (
                <OrderActionButton
                  disabled={requestPending}
                  pending={requestPending}
                  alert={alert}
                  onClick={this.onCarArriveClick(order)}
                  title="Авто прибыло в СТО"
                />
              )}
              {(order.status === WAIT_WORK_ADD || order.status === WAIT_PLANING) && (
                <OrderActionButton
                  disabled={false}
                  pending={false}
                  alert={alert}
                  onClick={this.onActionClick(order.id, 'service')}
                  title="Добавить работы"
                  icon={ServiceIcon}
                />
              )}
              {order.status === WAIT_PLANING && (
                <OrderActionButton
                  disabled={false}
                  pending={false}
                  alert={alert}
                  onClick={this.onActionClick(order.id, 'schedule')}
                  title="Запланировать работы"
                  icon={TimeIcon}
                />
              )}
            </Order>
          );
        })}
      </div>
    );
  }
}

export default MyFirmOrdersWithoutSchedule;

