import React from 'react';
import { formatRelative, format } from 'date-fns';
import { ru } from 'date-fns/esm/locale';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';
import PersonIcon from '@material-ui/icons/Person';
import PhoneIcon from '@material-ui/icons/Phone';
import CarIcon from '@material-ui/icons/DirectionsCar';
import DollarIcon from '@material-ui/icons/AttachMoney';
import get from 'lodash/get';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import { Link } from 'react-router-dom';
import { getOrderStatusTypeName, WAIT_CAR_ARRIVE, WAIT_CAR_RETURN } from '../../../../../common-code/constants/orderStatusTypes';
import phoneFormat from '../../../../../common-code/utils/text/phoneFormat';
import { fromGlobalId } from '../../../../../common-code/relay/globalIdUtils';
import relayContainer from '../../../../../common-code/decorators/relayContainer';
import ConfiguredRadium from '../../../radium/ConfiguredRadium';

const style = {
  listRow: {
    display: 'flex',
  },
  boxIconContainer: {
    marginRight: 6,
  },
  box: {
    fontSize: 14,
    width: 250,
    color: '#22273D',
    boxShadow: '0 0 10px 0 #D2D7E0',
    borderRadius: 5,
    background: 'white',
    borderTop: '4px solid green',
    lineHeight: '17px',
    transition: 'transform 0.15s',
    marginBottom: 15,
    ':hover': {
      transform: 'scale(1.05)',
    },
  },
  boxAlert: {
    borderTop: '4px solid #DF526B',
  },
  boxContainer: {
    fontSize: 14,
    background: '#FFF',
    padding: '5px 10px',
  },
  boxHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    fontWeight: 'bold',
  },
  boxIcon: {
    width: 16,
    height: 16,
    position: 'relative',
    top: 3,
    color: '#909090',
  },
  carArrivedIcon: {
    color: '#008a53',
    width: 16,
    height: 16,
    position: 'relative',
    top: 3,
  },
  waitCarArriveIcon: {
    color: '#c34838',
    width: 16,
    height: 16,
    position: 'relative',
    top: 3,
  },
  boxData: {
    fontSize: 14,
    background: '#FFF',
    padding: '5px 10px',
    borderTop: '1px solid #dedede',
    borderRadius: '0 0 5px 5px',
  },
  requestActions: {
    display: 'flex',
    justifyContent: 'center',
  },
  callButton: {
    color: '#F5F5F5',
    width: '100%',
    margin: '10px 20px',
  },
  carNumber: {
    whiteSpace: 'nowrap',
  },
};

@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          id
          car {
              id
              displayName
              registrationSignNumber
          }
          client {
              id
              fullName
              phone
          }
          payments {
              id
              amount
          }
          paymentsSum
          totalBill
          status
          payments {
              id
              amount
          }
          client {
              id
              fullName
          }
          phoneSameAsInProfile
          contactName
          contactNote
          contactPhone
          schedules {
              id
              start {
                  formattedDateTime
              }
              finish {
                  formattedDateTime
              }
          }
          serviceWorks {
              id
          }
          estimatedClientArrivalDate {
              timestamp
          }
          factClientVisitDateTime {
              timestamp
          }
      }
  `,
})
@ConfiguredRadium
class Order extends React.PureComponent {
  static defaultProps = {
    alert: false,
    scheduleInfo: '',
    overdue: false,
  };

  static propTypes = {
    alert: PropTypes.bool,
    overdue: PropTypes.bool,
    order: PropTypes.object.isRequired,
    scheduleInfo: PropTypes.string,
  };

  render() {
    const {
      id, status, paymentsSum, totalBill, client, car,
      phoneSameAsInProfile, contactName, contactNote, contactPhone,
    } = this.props.order;

    const todayUnixTimestamp = parseInt(Date.now() / 1000, 10);
    const twoDaySeconds = 2 * 24 * 60 * 60;

    const estimatedClientArrivalTimestamp = get(this.props.order, 'estimatedClientArrivalDate.timestamp', null);
    let clientArriveInPast = false;
    let estimatedClientArrivalDate = null;
    if (estimatedClientArrivalTimestamp) {
      estimatedClientArrivalDate = new Date().setTime(estimatedClientArrivalTimestamp * 1000);

      if (estimatedClientArrivalTimestamp < todayUnixTimestamp) {
        clientArriveInPast = true;
      }
    }

    const { scheduleInfo, alert, overdue } = this.props;

    let paymentColor = 'primary';
    if (totalBill && status === WAIT_CAR_RETURN && paymentsSum < totalBill) {
      paymentColor = 'error';
    }

    let scheduleColor = 'inherit';
    if (overdue) {
      scheduleColor = 'error';
    }

    let clientVisitTimeColor = 'inherit';
    if (clientArriveInPast) {
      clientVisitTimeColor = 'error';
    }

    const isCarArrived = get(this.props.order, 'factClientVisitDateTime.timestamp', false) !== false;

    let carIconTitle = 'Ждем приезда клиента';
    if (isCarArrived) {
      carIconTitle = 'Авто прибыло в офис';
    }

    if (!isCarArrived && estimatedClientArrivalTimestamp) {
      carIconTitle = `${carIconTitle} ${format(estimatedClientArrivalDate, 'DD.MM.YY HH:mm')}`;
    }

    return (
      <Link to={`/order/${id}`}>
        <div style={[style.box, alert && style.boxAlert]}>
          <div style={style.boxContainer}>
            <div style={style.boxHeader}>
              <div>{getOrderStatusTypeName(status)}</div>
              <div>заказ №{fromGlobalId(id).id}</div>
            </div>
            {scheduleInfo && (
              <Typography component="div" color={scheduleColor}>
                {scheduleInfo}
              </Typography>
            )}
            {status === WAIT_CAR_ARRIVE && estimatedClientArrivalDate && (
              <Typography component="div" color={clientVisitTimeColor}>
                клиент приедет <b>{format(estimatedClientArrivalDate, 'DD.MM.YY HH:mm')}</b>
                {Math.abs(estimatedClientArrivalTimestamp - todayUnixTimestamp) <= twoDaySeconds && (
                  <div>
                    {' '}
                    (<i>{formatRelative(estimatedClientArrivalDate, new Date(), { locale: ru })}</i>)
                  </div>
                )}
              </Typography>
            )}
          </div>
          <div style={style.boxData}>
            <Typography style={style.listRow} component="div">
              <div style={style.boxIconContainer}><PersonIcon style={style.boxIcon} /></div>
              <span>{client.fullName}</span>
            </Typography>
            <Typography style={style.listRow} component="div">
              <div style={style.boxIconContainer}><PhoneIcon style={style.boxIcon} /></div>
              <div>
                {phoneSameAsInProfile && phoneFormat(client.phone)}
                {!phoneSameAsInProfile && `${phoneFormat(contactPhone)} ${contactName}`}
                {contactNote}
              </div>
            </Typography>
            <Typography style={style.listRow} component="div">
              <div style={style.boxIconContainer}>
                <Tooltip title={carIconTitle}>
                  <CarIcon style={isCarArrived ? style.carArrivedIcon : style.waitCarArriveIcon} />
                </Tooltip>
              </div>
              {car.displayName}{' '}<b style={style.carNumber}>{car.registrationSignNumber}</b>
            </Typography>
            <Typography style={style.listRow} component="div" color={paymentColor}>
              <div style={style.boxIconContainer}><DollarIcon style={style.boxIcon} /></div>
              <div>
                к оплате {totalBill}р.<br />
                {!paymentsSum && totalBill > 0 && '(не оплачен)'}
                {paymentsSum > 0 && (
                  <div>
                    <b>оплачено {paymentsSum} р.</b>{' '}
                    {paymentsSum >= totalBill && '(оплачен полностью)'}
                    {paymentsSum < totalBill && '(оплачен частично)'}
                  </div>
                )}
              </div>
            </Typography>
          </div>
          {this.props.children}
        </div>
      </Link>
    );
  }
}

export default Order;
