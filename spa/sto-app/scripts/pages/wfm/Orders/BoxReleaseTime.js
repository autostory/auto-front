import React from 'react';
import Relay from 'react-relay/classic';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import Countdown from 'react-countdown-now';
import Typography from '@material-ui/core/Typography';
import { format } from 'date-fns';
import relayContainer from '../../../../../common-code/decorators/relayContainer';
import ConfiguredRadium from '../../../radium/ConfiguredRadium';

@relayContainer({
  box: () => Relay.QL`
      fragment on OfficeBox {
          releaseDateTime {
              timestamp
              formattedDateTime
          }
      }
  `,
})
@ConfiguredRadium
class BoxReleaseTime extends React.PureComponent {
  rendererCountDown = ({ days, hours, minutes}) => {
    const date = [];

    if (days && days > 0) {
      date.push(`${days} д.`);
    }

    if (hours && hours > 0) {
      date.push(`${hours} ч.`);
    }

    if (minutes && minutes > 0) {
      date.push(`${minutes} мин.`);
    }

    return <span>{date.join(' ')}</span>;
  };

  render() {
    const { box } = this.props;

    if (isEmpty(get(box, 'releaseDateTime', null))) {
      return null;
    }

    const timestamp = get(box, 'releaseDateTime.timestamp', null);
    if (!timestamp) {
      return false;
    }

    if (timestamp * 1000 < Date.now()) {
      return (
        <Typography color="error">
          есть просроченные заказы
        </Typography>
      );
    }

    const date = new Date().setTime(timestamp * 1000);

    return (
      <span>
        освободится <b>{format(date, 'DD.MM.YY HH:mm')}</b> <br />
        <i>через <Countdown date={date} renderer={this.rendererCountDown} zeroPadLength={1} /></i>
      </span>
    );
  }
}

export default BoxReleaseTime;
