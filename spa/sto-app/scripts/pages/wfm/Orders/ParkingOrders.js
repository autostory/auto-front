import React from 'react';

import filter from 'lodash/filter';
import indexOf from 'lodash/indexOf';
import pdfMake from 'pdfmake/build/pdfmake';
import vfsFonts from 'pdfmake/build/vfs_fonts';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Relay from 'react-relay/classic';
import get from 'lodash/get';
import map from 'lodash/map';
import Typography from '@material-ui/core/Typography';
import { WAIT_CAR_RETURN, WAIT_PAYMENT } from '../../../../../common-code/constants/orderStatusTypes';

import relaySuperContainer from '../../../../../common-code/decorators/relaySuperContainer';
import { setLastOrderCreateTimestamp } from '../../../reduxActions/WfmPageActions';
import { fromGlobalId } from '../../../../../common-code/relay/globalIdUtils';
import { makePdfActParams } from '../../../../../common-code/utils/pdf/makePdfActParams';
import CarReturnAction from '../../manager/OrderPage/ReturnCarDialog/CarReturnAction';
import Order from './Order';
import OrderActionButton from './OrderActionButton';
import setOrderFactCarReturnDateTime from './setOrderFactCarReturnDateTime';
import SetOrderFactCarReturnDateTimeMutation from '../../../relay/mutation/order/SetOrderFactCarReturnDateTimeMutation';

const REFETCH_INTERVAL = 60 * 1000;
const OrderParkingStatuses = [WAIT_CAR_RETURN, WAIT_PAYMENT];

@relaySuperContainer(
  {
    viewer: () => Relay.QL`
        fragment on Viewer {
            myFirm {
                orders(
                    statuses: [WAIT_CAR_RETURN, WAIT_PAYMENT],
                    active: true,
                    officeId: $officeId,
                    after: null,
                    first: 100500,
                ) {
                    edges {
                        node {
                            id
                            status
                            ${Order.getFragment('order')}
                            ${SetOrderFactCarReturnDateTimeMutation.getFragment('order')}
                            ${CarReturnAction.getFragment('order')}
                            paymentsSum
                            totalBill
                            status
                            totalBill
                            paymentsSum
                            office {
                                id
                            }
                            makeAt {
                                timestamp
                            }
                            factClientVisitDateTime {
                                timestamp
                            }
                            factCarReturnDateTime {
                                timestamp
                            }
                            office {
                                id
                                title
                                address
                            }
                            firm {
                                id
                                name
                            }
                            serviceWorks {
                                id
                                amount
                                discountPercent
                                totalCost
                                costPerUnit
                                sparePartNode {
                                    id
                                    name
                                }
                                note
                                action {
                                    id
                                    name
                                }
                            }
                            client {
                                id
                                fullName
                            }
                            serviceMaterials {
                                id
                                name
                                amount
                                totalCost
                                costPerUnit
                                discountPercent
                                unitMeasureType {
                                    id
                                    name
                                }
                            }
                        }
                    }
                }
            }
        }
    `,
  },
  {
    officeId: null,
  },
)
@connect(
  state => ({
    lastOrderCreateTimestamp: state.wfmPageStore.lastOrderCreateTimestamp,
  }),
  dispatch => ({
    setLastOrderCreateTimestamp: timestamp => dispatch(setLastOrderCreateTimestamp(timestamp)),
  }),
)
class ParkingOrders extends React.PureComponent {
  static propTypes = {
    openAddPaymentDialog: PropTypes.func.isRequired,
  };

  state = {
    requestPending: false,
    refreshHandler: null,
  };

  componentWillMount() {
    this.setState({
      refreshHandler: setInterval(this.props.relay.forceFetch, REFETCH_INTERVAL),
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.lastOrderCreateTimestamp !== this.props.lastOrderCreateTimestamp) {
      this.props.relay.forceFetch();
    }

    if (prevProps.lastOrderCreateTimestamp !== this.props.lastOrderCreateTimestamp) {
      this.props.relay.forceFetch();
    } else {
      const { viewer, setLastOrderCreateTimestamp } = this.props;
      const orders = map(get(viewer, 'myFirm.orders.edges'), 'node');
      const ordersNotInNeededStatus = filter(orders, order => indexOf(OrderParkingStatuses, order.status) >= 0);

      if (ordersNotInNeededStatus.length) {
        setLastOrderCreateTimestamp(Date.now());
        this.props.relay.forceFetch();
      }
    }
  }

  componentWillUnmount = () => {
    if (this.state.refreshHandler !== null) {
      clearInterval(this.state.refreshHandler);
      this.setState({
        refreshHandler: null,
      });
    }
  };

  onRequestEnd = () => {
    this.setState({
      requestPending: false,
    });
  };

  onCarReturnClick = order => (event) => {
    const orderId = fromGlobalId(order.id).id;

    const { vfs } = vfsFonts.pdfMake;
    pdfMake.vfs = vfs;
    pdfMake
      .createPdf(makePdfActParams({ order }))
      .download(`Акт выполненных работ по заказу №${orderId}`);

    this.setState({
      requestPending: true,
    }, this.setOrderFactCarReturnDateTime(order));

    event.preventDefault();
    return false;
  };

  onAddPaymentClick = order => (event) => {
    this.props.openAddPaymentDialog(order.id);

    event.preventDefault();
    return false;
  };

  setOrderFactCarReturnDateTime = order => () => {
    const date = parseInt(Date.now() / 1000, 10);

    setOrderFactCarReturnDateTime(order, { factCarReturnDateTime: date })
      .then(this.onRequestEnd, this.onRequestEnd);
  };

  render() {
    const { viewer } = this.props;
    const { requestPending } = this.state;
    const orders = map(get(viewer, 'myFirm.orders.edges'), 'node');

    if (!orders.length) {
      return <Typography style={{ textAlign: 'center' }} variant="subheading">нет заказов</Typography>;
    }

    return (
      <div>
        {orders.map((order) => {
          return (
            <Order
              key={order.id}
              order={order}
              alert={false}
            >
              {order.status === WAIT_CAR_RETURN && (
                <div style={{ margin: '10px 20px', display: 'inline-flex' }}>
                  <CarReturnAction order={order}>
                    <OrderActionButton
                      disabled={requestPending}
                      pending={requestPending}
                      alert={false}
                      onClick={this.onCarReturnClick(order)}
                      title="В акте все правильно, вернуть авто клиенту и получить акт"
                    />
                  </CarReturnAction>
                </div>
              )}
              {order.status === WAIT_PAYMENT && (
                <OrderActionButton
                  pending={false}
                  disabled={false}
                  alert={false}
                  onClick={this.onAddPaymentClick(order)}
                  title="Провести оплату"
                />
              )}
            </Order>
          );
        })}
      </div>
    );
  }
}

export default ParkingOrders;

