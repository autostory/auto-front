import SetOrderFactCarArriveDateTimeMutation from '../../../relay/mutation/order/SetOrderFactCarArriveDateTimeMutation';
import commitMutationPromise from '../../../../../common-code/relay/commitMutationPromise';

function setOrderFactCarArriveDateTime(order, input) {
  return commitMutationPromise(SetOrderFactCarArriveDateTimeMutation, { order, input });
}

export default setOrderFactCarArriveDateTime;
