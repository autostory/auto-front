import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

const style = {
  orderActions: {
    display: 'flex',
    justifyContent: 'center',
  },
  callButton: {
    width: '100%',
    margin: '10px 20px',
    display: 'flex',
    justifyContent: 'space-evenly',
    color: 'white',
  },
};

class OrderActionButton extends React.PureComponent {
  static defaultProps = {
    icon: null,
  };

  static propTypes = {
    alert: PropTypes.bool.isRequired,
    disabled: PropTypes.bool.isRequired,
    icon: PropTypes.symbol,
    onClick: PropTypes.func.isRequired,
    pending: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
  };

  render() {
    const {
      alert, disabled, pending, onClick, title, icon: Icon, ...rest,
    } = this.props;

    let color = 'primary';
    if (alert) {
      color = 'secondary';
    }

    return (
      <div>
        <div style={style.orderActions}>
          <Button
            disabled={disabled}
            variant="raised"
            color={color}
            onClick={onClick}
            style={style.callButton}
            {...rest}
          >
            {Icon && <Icon />} {title} {pending && <CircularProgress size={15} />}
          </Button>
        </div>

      </div>
    );
  }
}

export default OrderActionButton;

