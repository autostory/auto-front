import PropTypes from 'prop-types';
import React from 'react';
import { isSameDay, format } from 'date-fns';
import { connect } from 'react-redux';
import Relay from 'react-relay/classic';
import sortBy from 'lodash/sortBy';
import get from 'lodash/get';
import filter from 'lodash/filter';
import indexOf from 'lodash/indexOf';
import map from 'lodash/map';
import concat from 'lodash/concat';
import { withRouter } from 'react-router';
import Typography from '@material-ui/core/Typography';
import { IN_WORK, WAIT_CAR_ARRIVE, WAIT_WORK_START } from '../../../../../common-code/constants/orderStatusTypes';
import { SCHEDULE_DONE, SCHEDULE_IN_WORK, SCHEDULE_WAIT_WORK_START } from '../../../../../common-code/constants/scheduleStatusTypes';
import { setLastOrderCreateTimestamp } from '../../../reduxActions/WfmPageActions';
import SetOrderFactCarArriveDateTimeMutation from '../../../relay/mutation/order/SetOrderFactCarArriveDateTimeMutation';
import SetScheduleStatusMutation from '../../../relay/mutation/schedule/SetScheduleStatusMutation';

import relaySuperContainer from '../../../../../common-code/decorators/relaySuperContainer';
import Order from './Order';
import OrderActionButton from './OrderActionButton';
import setOrderFactCarArriveDateTime from './setOrderFactCarArriveDateTime';
import setScheduleStatus from './setScheduleStatus';

const ALERT_CAR_DONT_ARRIVE = 300; // 5 минут.

const OrderScheduledStatuses = [WAIT_CAR_ARRIVE, WAIT_WORK_START, IN_WORK];
const REFETCH_INTERVAL = 60 * 1000;

@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          myFirm {
              orders(
                  boxId: $boxId, 
                  statuses: [WAIT_CAR_ARRIVE, WAIT_WORK_START, IN_WORK], 
                  withActiveSchedules: true, 
                  active: true,
                  after: null,
                  first: 100500,
              ) {
                  edges {
                      node {
                          id
                          status
                          factClientVisitDateTime {
                              timestamp
                          }
                          ${Order.getFragment('order')}
                          ${SetOrderFactCarArriveDateTimeMutation.getFragment('order')}
                          ${SetScheduleStatusMutation.getFragment('order')}
                          schedules (statuses: [SCHEDULE_IN_WORK, SCHEDULE_WAIT_WORK_START]) {
                              ${SetScheduleStatusMutation.getFragment('schedule')}
                              id
                              status
                              start {
                                  timestamp
                              }
                              finish {
                                  timestamp
                              }
                              serviceWorks {
                                  id
                                  status
                              }
                          }
                      }
                  }
              }
          }
      }
  `,
})
@connect(
  state => ({
    lastOrderCreateTimestamp: state.wfmPageStore.lastOrderCreateTimestamp,
  }),
  dispatch => ({
    setLastOrderCreateTimestamp: timestamp => dispatch(setLastOrderCreateTimestamp(timestamp)),
  }),
)
@withRouter
class BoxOrdersSchedule extends React.PureComponent {
  static propTypes = {
    openMarkWorkOnScheduleAsCompletedDialog: PropTypes.func.isRequired,
  };

  state = {
    requestPending: false,
    refreshHandler: null,
  };

  componentWillMount() {
    this.setState({
      refreshHandler: setInterval(this.props.relay.forceFetch, REFETCH_INTERVAL),
    });
  }

  componentWillUnmount = () => {
    if (this.state.refreshHandler !== null) {
      clearInterval(this.state.refreshHandler);
      this.setState({
        refreshHandler: null,
      });
    }
  };

  componentDidUpdate = (prevProps) => {
    if (prevProps.lastOrderCreateTimestamp !== this.props.lastOrderCreateTimestamp) {
      this.props.relay.forceFetch();
    } else {
      const { viewer, setLastOrderCreateTimestamp } = this.props;
      const orders = map(get(viewer, 'myFirm.orders.edges'), 'node');
      const ordersNotInNeededStatus = filter(orders, order => indexOf(OrderScheduledStatuses, order.status) >= 0);

      if (ordersNotInNeededStatus.length) {
        setLastOrderCreateTimestamp(Date.now());
        this.props.relay.forceFetch();
      }
    }
  };

  onRequestEnd = () => {
    this.setState({
      requestPending: false,
    });
  };

  onCarArriveClick = order => (event) => {
    this.setState({
      requestPending: true,
    }, this.sendOrderFactCarArriveDateTime(order));

    event.preventDefault();
    return false;
  };

  onScheduleWorkStartClick = (schedule, order) => (event) => {
    this.setState({
      requestPending: true,
    }, this.sendScheduleStatusToStart(schedule, order));

    event.preventDefault();
    return false;
  };

  onScheduleWorkFinishClick = (schedule, order) => (event) => {
    if (!schedule.serviceWorks.length) {
      this.setState({
        requestPending: true,
      }, this.sendScheduleStatusToFinish(schedule, order));
    } else {
      this.props.openMarkWorkOnScheduleAsCompletedDialog(schedule.id);
    }

    event.preventDefault();
    return false;
  };

  sendOrderFactCarArriveDateTime = order => () => {
    const date = parseInt(Date.now() / 1000, 10);

    setOrderFactCarArriveDateTime(order, { factClientVisitDateTime: date })
      .then(this.onRequestEnd, this.onRequestEnd);
  };

  sendScheduleStatusToStart = (schedule, order) => () => {
    setScheduleStatus(schedule, order, { status: SCHEDULE_IN_WORK })
      .then(this.onRequestEnd, this.onRequestEnd);
  };

  sendScheduleStatusToFinish = (schedule, order) => () => {
    setScheduleStatus(schedule, order, { status: SCHEDULE_DONE })
      .then(this.onRequestEnd, this.onRequestEnd);
  };

  render() {
    const { viewer } = this.props;
    const { requestPending } = this.state;
    const orders = map(get(viewer, 'myFirm.orders.edges'), 'node');
    let data = [];

    map(orders, (order) => {
      const temp = map(order.schedules, schedule => ({
        order,
        schedule,
      }));

      data = concat(data, temp);
    });

    if (!data.length) {
      return <Typography style={{ textAlign: 'center' }} variant="subheading">нет расписаний</Typography>;
    }

    data = sortBy(data, [item => item.schedule.start.timestamp]);

    return (
      <div>
        {data.map((item) => {
          const { order, schedule } = item;
          let overdue = false;
          if (schedule.finish.timestamp < parseInt(Date.now() / 1000, 10)) {
            overdue = true;
          }

          let scheduleInfo;
          const start = new Date().setTime(schedule.start.timestamp * 1000);
          const finish = new Date().setTime(schedule.finish.timestamp * 1000);

          if (isSameDay(start, finish)) {
            scheduleInfo = (
              <span>
                {`запланировано ${format(start, 'DD.MM.YY')}`}
                <br />
                {`${format(start, 'HH:mm')}-${format(finish, 'HH:mm')}`}
              </span>
            );
          } else {
            scheduleInfo = `запланировано ${format(start, 'DD.MM.YY HH:mm')}-${format(finish, 'DD.MM.YY HH:mm')} `;
          }

          let alert = false;
          // За 5 минут до того как работы надо начать отмечаем что надо отметить прибытие тачки
          if (
            order.status === WAIT_CAR_ARRIVE
            && ((schedule.start.timestamp - ALERT_CAR_DONT_ARRIVE) >= parseInt(Date.now() / 1000, 10))
          ) {
            alert = true;
          }

          const waitCarArrive = order.status === WAIT_CAR_ARRIVE;
          const needStartWork = order.status === WAIT_WORK_START && schedule.status === SCHEDULE_WAIT_WORK_START;
          const needFinishWork = order.status === IN_WORK && schedule.status === SCHEDULE_IN_WORK;

          return (
            <Order
              key={schedule.id}
              order={order}
              alert={overdue || alert}
              overdue={overdue}
              scheduleInfo={scheduleInfo}
            >
              {waitCarArrive && (
                <OrderActionButton
                  disabled={requestPending}
                  pending={requestPending}
                  alert={overdue || alert}
                  onClick={this.onCarArriveClick(order)}
                  title="Авто прибыло в СТО"
                />
              )}
              {needStartWork && (
                <OrderActionButton
                  disabled={requestPending}
                  pending={requestPending}
                  alert={overdue || alert}
                  onClick={this.onScheduleWorkStartClick(schedule, order)}
                  title="Начать работу"
                />
              )}
              {needFinishWork && (
                <OrderActionButton
                  disabled={requestPending}
                  pending={requestPending}
                  alert={overdue || alert}
                  onClick={this.onScheduleWorkFinishClick(schedule, order)}
                  title="Окончить работу"
                />
              )}
            </Order>
          );
        })}
      </div>
    );
  }
}

export default BoxOrdersSchedule;

