import get from 'lodash/get';
import React from 'react';
import Relay from 'react-relay/classic';
import CircularProgress from '@material-ui/core/CircularProgress';
import relayContainer from '../../../../../common-code/decorators/relayContainer';
import ConfiguredRadium from '../../../radium/ConfiguredRadium';

@relayContainer(
  {
    firm: () => Relay.QL`
        fragment on Firm {
            orderCountWithoutSchedule: orderCount(
                active: true,
                withActiveSchedules: false,
                statuses: [WAIT_CAR_ARRIVE, WAIT_WORK_ADD, WAIT_PLANING],
                officeId: $officeId
            ) @include(if: $officeSelected)
        }
    `,
  },
  {
    officeId: null,
    officeSelected: false,
  },
)
@ConfiguredRadium
class OrderCountWithoutSchedule extends React.PureComponent {
  constructor(props) {
    super(props);

    if (props.officeId) {
      props.relay.setVariables({
        officeId: props.officeId,
        officeSelected: true,
      });
    }
  }

  render() {
    const count = get(this.props, 'firm.orderCountWithoutSchedule', null);

    if (count === null) {
      return <CircularProgress size={15} />;
    }

    return (
      <span>{count}</span>
    );
  }
}

export default OrderCountWithoutSchedule;
