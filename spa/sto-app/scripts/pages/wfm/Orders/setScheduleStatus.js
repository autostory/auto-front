import SetScheduleStatusMutation from '../../../relay/mutation/schedule/SetScheduleStatusMutation';
import commitMutationPromise from '../../../../../common-code/relay/commitMutationPromise';

function setScheduleStatus(schedule, order, input) {
  return commitMutationPromise(SetScheduleStatusMutation, { schedule, order, input });
}

export default setScheduleStatus;
