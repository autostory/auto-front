import SetOrderFactCarReturnDateTimeMutation from '../../../relay/mutation/order/SetOrderFactCarReturnDateTimeMutation';
import commitMutationPromise from '../../../../../common-code/relay/commitMutationPromise';

function setOrderFactCarReturnDateTime(order, input) {
  return commitMutationPromise(SetOrderFactCarReturnDateTimeMutation, { order, input });
}

export default setOrderFactCarReturnDateTime;
