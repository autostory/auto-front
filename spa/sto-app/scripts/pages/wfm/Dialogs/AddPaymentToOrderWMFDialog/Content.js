import React, { Component } from 'react';
import withProps from 'recompose/withProps';
import Relay from 'react-relay/classic';
import CircularProgress from '@material-ui/core/CircularProgress';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';
import OrderPaymentsTable from '../../../manager/OrderPage/includes/OrderPaymentsTable';

@withProps({ renderLoading: () => <CircularProgress /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          order(orderId: $orderId) {
              ${OrderPaymentsTable.getFragment('order')}
          }
      }
  `,
})
class Content extends Component {
  render() {
    const { viewer } = this.props;
    const { order } = viewer;

    return (
      <OrderPaymentsTable order={order} />
    );
  }
}

export default Content;

