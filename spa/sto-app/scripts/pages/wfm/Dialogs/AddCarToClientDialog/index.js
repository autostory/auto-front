import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import isUndefined from 'lodash/isUndefined';

import Slide from '@material-ui/core/Slide';
import { connect } from 'react-redux';
import { isInvalid, isSubmitting, submit as ReduxFormSubmit } from 'redux-form';
import AddCarToClientForm, { FORM_NAME } from '../../../../components/Form/Car/AddCarToClientForm/AddCarToClientForm';
import submit from '../../../../components/Form/Car/AddCarToClientForm/submit';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

@connect(state => ({
  invalid: isInvalid(FORM_NAME)(state),
  submitting: isSubmitting(FORM_NAME)(state),
}))
class AddCarToClientDialog extends PureComponent {
  static defaultProps = {
    classes: {},
  };

  static propTypes = {
    onRequestClose: PropTypes.func.isRequired,
    afterCarAdd: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    clientId: PropTypes.string.isRequired,
  };

  onStartCreateClient = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onClose = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onAdd = () => {
    this.props.dispatch(ReduxFormSubmit(FORM_NAME));
  };

  onSubmit = (values) => {
    const { viewer, afterCarAdd, clientId, onRequestClose } = this.props;

    return submit(viewer, { ...values, ...{ clientId } })
      .then((result) => {
        if (!isUndefined(result) && !isUndefined(result.success) && result.success) {
          afterCarAdd();
          onRequestClose();
        }
      });
  };

  render() {
    const {
      open, onRequestClose, clientId, submitting, invalid,
    } = this.props;

    return (
      <Dialog
        fullScreen={false}
        TransitionComponent={Transition}
        onClose={onRequestClose}
        open={open}
        maxWidth="md"
      >
        <DialogTitle id="form-dialog-title">Добавление автомобиля клиенту</DialogTitle>

        <DialogContent>
          <AddCarToClientForm
            onSubmit={this.onSubmit}
            clientId={clientId}
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={this.onAdd} color="primary" disabled={invalid || submitting}>
            Добавить {submitting && <CircularProgress size={15} />}
          </Button>
          <Button onClick={this.onClose} color="primary">
            Отмена
          </Button>
        </DialogActions>

      </Dialog>
    );
  }
}

export default AddCarToClientDialog;
