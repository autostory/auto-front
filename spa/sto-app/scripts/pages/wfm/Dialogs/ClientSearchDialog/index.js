import React from 'react';
import Relay from 'react-relay/classic';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import CircularProgress from '@material-ui/core/CircularProgress';
import debounce from 'lodash/debounce';
import get from 'lodash/get';

import Slide from '@material-ui/core/Slide';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';

import ConfiguredRadium from '../../../../radium/ConfiguredRadium';
import ClientSearchTable from './ClientSearchTable';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          myFirm {
              id
              name
              clients(
                  fio: $keyword,
                  email: $keyword,
                  phone: $keyword,
                  vin: $keyword,
                  registrationSignNumber: $keyword
                  first: 20
              ) @include(if: $notEmptyKeyword) {
                  ${ClientSearchTable.getFragment('firmClients')},
              }
          }
      }
  `,
})
@ConfiguredRadium
class ClientSearchDialog extends React.PureComponent {
  static propTypes = {
    onClientSelect: PropTypes.func.isRequired,
    onRequestClose: PropTypes.func.isRequired,
    onStartCreateClient: PropTypes.func,
    open: PropTypes.bool.isRequired,
    canCreateClient: PropTypes.bool.isRequired,
    relay: PropTypes.any,
    viewer: PropTypes.any,
  };

  state = {
    recordsPending: false,
    canCreateClient: true,
    value: '',
    keyword: '',
    onStartCreateClient: () => {
    },
  };

  onStartCreateClient = () => {
    const { onRequestClose, onStartCreateClient } = this.props;

    onStartCreateClient();
    onRequestClose();
  };

  onClose = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onClientSelect = (client) => {
    const { onClientSelect, onRequestClose } = this.props;

    onClientSelect(client.id, client.fullName);
    onRequestClose();
  };

  handleKeywordChange = (event) => {
    this.setState({
      value: event.target.value,
    }, this.handleSearchDebounced);
  };

  updateRelayKeyword = () => {
    const { relay } = this.props;
    const { keyword } = this.state;

    relay.setVariables({
      keyword,
      notEmptyKeyword: keyword !== '',
    }, (state) => {
      this.setState({
        recordsPending: !state.ready,
      });
    });
  };

  handleSearchDebounced = debounce(() => {
    this.setState({
      keyword: this.state.value,
    }, this.updateRelayKeyword);
  }, 300, { maxWait: 1000 });

  render() {
    const {
      open, onRequestClose, relay, viewer, onClientSelect, onStartCreateClient, canCreateClient, ...rest
    } = this.props;

    const { keyword, value, recordsPending } = this.state;

    const firmClients = get(viewer, 'myFirm.clients', null);

    return (
      <Dialog
        fullScreen={false}
        TransitionComponent={Transition}
        onClose={onRequestClose}
        open={open}
        maxWidth="md"
        {...rest}
      >
        <DialogTitle id="form-dialog-title">Поиск клиента</DialogTitle>

        <DialogContent>
          <DialogContentText>
            Введите часть ФИО, номера телефона, email клиента. Или гос номер или VIN номер автомобиля.
          </DialogContentText>
          <TextField
            autoFocus
            value={value}
            onChange={this.handleKeywordChange}
            margin="dense"
            placeholder="Поиск..."
            fullWidth
            InputProps={{
              maxLength: 250,
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
            }}
          />
          <ClientSearchTable
            onClientSelect={this.onClientSelect}
            pending={recordsPending}
            firmClients={firmClients}
            renderLoading={() => <CircularProgress />}
            keyword={keyword}
          />
          {canCreateClient && (
            <Button variant="contained" color="primary" onClick={this.onStartCreateClient} style={{ marginTop: 20 }}>
              Добавить клиента
            </Button>
          )}
        </DialogContent>

        <DialogActions>
          <Button onClick={this.onClose} color="primary">
            Отмена
          </Button>
        </DialogActions>

      </Dialog>
    );
  }
}

export default ClientSearchDialog;
