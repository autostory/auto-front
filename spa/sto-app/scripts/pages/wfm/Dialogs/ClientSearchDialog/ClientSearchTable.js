import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';
import get from 'lodash/get';
import map from 'lodash/map';
import compact from 'lodash/compact';
import { withStyles } from '@material-ui/core/styles';
import Highlighter from 'react-highlight-words';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';

import relayContainer from '../../../../../../common-code/decorators/relayContainer';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

const Match = ({ keywordArray, text, ...rest }) => {
  return (
    <Highlighter
      highlightStyle={{ background: '#FAD65E', padding: '4px 0' }}
      searchWords={keywordArray}
      autoEscape
      textToHighlight={text || ''}
      {...rest}
    />
  );
};

@relayContainer({
  firmClients: () => Relay.QL`
      fragment on FirmClients {
          clientList {
              edges {
                  node {
                      id
                      email
                      phone
                      fullName
                      cars {
                          carList {
                              edges {
                                  node {
                                      id
                                      registrationSignNumber
                                      displayName
                                  }
                              }
                          }
                      }
                  }
              }
          }
      }
  `,
})
class ClientSearchTable extends Component {
  static defaultProps = {
    keyword: '',
    onStartCreateClient: () => {},
  };

  static propTypes = {
    keyword: PropTypes.string.isRequired,
    onClientSelect: PropTypes.func.isRequired,
    onStartCreateClient: PropTypes.func,
  };

  onChoice = client => () => {
    const { onClientSelect } = this.props;

    onClientSelect(client);
  };

  render() {
    const {
      keyword, pending, firmClients, classes,
    } = this.props;

    if (pending) {
      return <div><CircularProgress /> Идет поиск...</div>;
    }

    const clients = get(firmClients, 'clientList.edges', null);

    if (keyword === '') {
      return (
        <div>
          Введите несколько символов и появлятся подходящие результаты.
        </div>
      );
    }

    const keywordArray = compact(keyword.split(' '));

    return (
      <div>
        <Paper className={classes.root}>
          {clients === null && (
            <div style={{ padding: 24 }}>
              Ничего не найдено
            </div>
          )}
          {clients !== null && (
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell padding="dense">ФИО</TableCell>
                  <TableCell padding="dense">Телефон</TableCell>
                  <TableCell padding="dense">E-mail</TableCell>
                  <TableCell padding="dense">Авто</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {
                  map(clients, (item) => {
                    const { node: client } = item;
                    const cars = get(client, 'cars.carList.edges', null);

                    return (
                      <TableRow
                        key={client.id}
                        hover
                        style={{ cursor: 'pointer' }}
                        onClick={this.onChoice(client)}
                      >
                        <TableCell padding="dense">
                          <Match keywordArray={keywordArray} text={client.fullName} />
                        </TableCell>
                        <TableCell padding="dense">
                          <Match keywordArray={keywordArray} text={client.phone} />
                        </TableCell>
                        <TableCell padding="dense">
                          <Match keywordArray={keywordArray} text={client.email} />
                        </TableCell>
                        <TableCell padding="dense">
                          {cars === null && (
                            <i>нет автомобилей</i>
                          )}
                          {cars !== null && (
                            map(cars, (carItem) => {
                              const { node: car } = carItem;

                              return (
                                <div style={{ whiteSpace: 'nowrap' }} key={car.id}>
                                  {car.displayName}
                                  <br />
                                  <Match keywordArray={keywordArray} text={car.registrationSignNumber} />
                                </div>
                              );
                            })
                          )}
                        </TableCell>
                      </TableRow>
                    );
                  })
                }
              </TableBody>
            </Table>
          )}
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(ClientSearchTable);

