import React from 'react';
import Relay from 'react-relay/classic';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import map from 'lodash/map';
import each from 'lodash/each';
import withProps from 'recompose/withProps';
import { format } from 'date-fns';
import { getScheduleStatusTypeName } from '../../../../../../common-code/constants/scheduleStatusTypes';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';

@withProps({
  renderLoading: () => (
    <Typography component="div" style={{ width: '100%', padding: '0 24px 12px 24px' }}>
      Проверка связей работы с расписаниями...{' '}<CircularProgress size={20} />
    </Typography>
  ),
})
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          serviceWork(serviceWorkId: $serviceWorkId) {
              id
              schedules {
                  id
                  status
                  box {
                      title
                  }
                  start {
                      timestamp
                  }
                  finish {
                      timestamp
                  }
                  serviceWorks {
                      id
                  }
              }
          }
      }
  `,
})
class CarWorkRemoveNotifer extends React.PureComponent {
  getScheduleText(schedule) {
    return (
      <div>
        <b>{schedule.box.title}</b>
        {' '}
        {format(new Date().setTime(schedule.start.timestamp * 1000), 'DD.MM.YY HH:mm')}
        {' - '}
        {format(new Date().setTime(schedule.finish.timestamp * 1000), 'DD.MM.YY HH:mm')}
        {' '}
        {getScheduleStatusTypeName(schedule.status)}
      </div>
    );
  }

  getSchedulesInfo = () => {
    const { viewer } = this.props;
    const { serviceWork } = viewer;
    const { schedules } = serviceWork;

    return (
      <div style={{ paddingBottom: 12 }}>
        {map(schedules, schedule => (
          <div key={schedule.id}>
            {!this.isLastWorkInSchedule(schedule) && (
              <Typography component="div" style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
                <div>Эта работа привязана к расписанию</div>
                <div>
                  {this.getScheduleText(schedule)}
                </div>
                <div>Не забудьте перепланировать это расписание, если это необходимо.</div>
              </Typography>
            )}
            {this.isLastWorkInSchedule(schedule) && (
              <Typography component="div" style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
                <div>Это работа привязана к расписанию</div>
                <div>
                  {this.getScheduleText(schedule)}
                </div>
                <div><b>Связанное расписание будет удалено при удалении работы.</b></div>
              </Typography>
            )}
          </div>
        ))}
      </div>
    );
  };

  isLastWorkInSchedule = (schedule) => {
    let isLastWorkInSchedule = false;

    if (schedule.serviceWorks.length === 1) {
      isLastWorkInSchedule = true;
    }

    return isLastWorkInSchedule;
  };

  hasSchedule = () => {
    const { viewer } = this.props;
    const { serviceWork } = viewer;
    const { schedules } = serviceWork;

    return schedules.length > 0;
  };

  render() {
    const hasSchedule = this.hasSchedule();

    return (
      <div style={{ width: '100%', padding: '0 24px 12px 24px' }}>
        {hasSchedule && this.getSchedulesInfo()}
        {this.props.children}
      </div>
    );
  }
}

export default CarWorkRemoveNotifer;
