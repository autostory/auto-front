import * as React from 'react';
import * as PropTypes from 'prop-types';
import Input from '@material-ui/core/Input';
import { withStyles } from '@material-ui/core/styles';
import { DataTypeProvider } from '@devexpress/dx-react-grid';
import isFinite from 'lodash/isFinite';
import parseInt from 'lodash/parseInt';
import isNaN from 'lodash/isNaN';
import VMasker from 'vanilla-masker';

const styles = {
  numericInput: {
    textAlign: 'right',
    width: '100%',
  },
};
const normalizePercent = (value) => {
  if (!value || value > 100) {
    return '';
  }

  const number = VMasker.toNumber(value);

  if (isNaN(number)) {
    return '';
  }

  const abs = Math.abs(number);

  if (isNaN(abs)) {
    return '';
  }

  return abs;
};

const getInputValue = value => (value === undefined ? '' : value);

const EditorBase = ({ value, onValueChange, classes }) => {
  const handleChange = (event) => {
    const { value: targetValue } = event.target;
    if (targetValue.trim() === '') {
      onValueChange();

      return;
    }
    onValueChange(normalizePercent(targetValue));
  };

  return (
    <Input
      classes={{
        input: classes.numericInput,
      }}
      fullWidth
      value={getInputValue(value)}
      inputProps={{
        min: 0,
        placeholder: '%',
        maxLength: 4,
      }}
      onChange={handleChange}
    />
  );
};

EditorBase.propTypes = {
  value: PropTypes.number,
  onValueChange: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

EditorBase.defaultProps = {
  value: undefined,
};

const Editor = withStyles(styles)(EditorBase);

const Formatter = ({ value }) => {
  const amount = parseInt(value, 10);
  if (!value || !isFinite(amount)) {
    return '0 %';
  }

  return `${amount} %`;
};

const PercentTypeProvider = props => (
  <DataTypeProvider
    formatterComponent={Formatter}
    editorComponent={Editor}
    {...props}
  />
);

export default PercentTypeProvider;
