import { withStyles } from '@material-ui/core/styles/index';
import isUndefined from 'lodash/isUndefined';
import React from 'react';
import sortBy from 'lodash/sortBy';
import map from 'lodash/map';
import TableCell from '@material-ui/core/TableCell';
import Input from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { connect } from 'react-redux';
import {
  getServiceWorkStatusTypes,
  getServiceWorkStatusTypeName,
} from '../../../../../../common-code/constants/serviceWorkStatusTypes';

import CustomEditCell from './CustomEditCell';
import AutoComplete from '../../../../components/common/Select/AutoComplete/AutoComplete';

const styles = theme => ({
  lookupEditCell: {
    paddingTop: theme.spacing.unit * 0.875,
    paddingRight: theme.spacing.unit,
    paddingLeft: theme.spacing.unit,
  },
  inputRoot: {
    width: '100%',
  },
});

const LookupEditCellBase = ({ availableColumnValues, value, onValueChange, classes, withEmpty }) => (
  <TableCell
    className={classes.lookupEditCell}
  >
    <Select
      value={value}
      onChange={event => onValueChange(event.target.value)}
      input={
        <Input
          classes={{ root: classes.inputRoot }}
        />
      }
    >
      {withEmpty && <MenuItem value={0}>не указан</MenuItem>}
      {availableColumnValues.length !== 0 && availableColumnValues.map(item => (
        <MenuItem key={item.id} value={item.id}>{item.title}</MenuItem>
      ))}
    </Select>
  </TableCell>
);

const LookupEditCell = withStyles(styles)(LookupEditCellBase);

const CustomTextInput = ({ newRowErrors, editedRowsErrors, ...rest }) => {
  let errorMessage = '';
  if (isUndefined(rest.row.id)) {
    errorMessage = !isUndefined(newRowErrors)
      && !isUndefined(newRowErrors[rest.column.name])
      && newRowErrors[rest.column.name];
  } else {
    errorMessage = !isUndefined(editedRowsErrors) && !isUndefined(editedRowsErrors[rest.row.id])
      && !isUndefined(editedRowsErrors[rest.row.id][rest.column.name])
      && editedRowsErrors[rest.row.id][rest.column.name];
  }

  return <CustomEditCell errorMessage={errorMessage || ''} {...rest} />;
};

@connect(state => ({
  serviceWorkActionsTypes: state.carWorkEditableTableStore.serviceWorkActionsTypes,
  managers: state.carWorkEditableTableStore.managers,
  sparePartNodes: state.carWorkEditableTableStore.sparePartNodes,
}))
@withStyles(styles)
class CarWorkEditableEditCell extends React.PureComponent {
  render() {
    const { classes } = this.props;
    const {
      dispatch, managers, serviceWorkActionsTypes, sparePartNodes, newRowErrors, editedRowsErrors, container, ...rest
    } = this.props;
    if (this.props.column.name === 'masterId') {
      const availableColumnValues = map(managers, manager => ({
        id: manager.id,
        title: manager.fullName,
        active: manager.active,
      }));

      let value = rest.value;
      if (rest.value === null) {
        value = 0;
      }

      const activeManagers = availableColumnValues.filter(item => item.active);

      return (
        <LookupEditCell
          availableColumnValues={activeManagers}
          {...rest}
          value={value}
          withEmpty
        />
      );
    }

    if (this.props.column.name === 'serviceWorkActionTypeId') {
      const availableColumnValues = map(serviceWorkActionsTypes, serviceWorkActionsType => ({
        id: serviceWorkActionsType.id,
        title: serviceWorkActionsType.name,
      }));

      const sortedAvailableColumnValues = sortBy(availableColumnValues, [item => item.title]);

      return <LookupEditCell availableColumnValues={sortedAvailableColumnValues} {...rest} withEmpty={false} />;
    }

    if (rest.column.name === 'status') {
      if (rest.value === null) {
        return <TableCell />;
      }

      return <TableCell>{getServiceWorkStatusTypeName(rest.value)}</TableCell>;

      // Редактирование статуса убрали

      const allStatuses = getServiceWorkStatusTypes();

      const availableColumnValues = map(allStatuses, serviceWorkActionsType => ({
        id: serviceWorkActionsType,
        title: getServiceWorkStatusTypeName(serviceWorkActionsType),
      }));

      return <LookupEditCell availableColumnValues={availableColumnValues} {...rest} withEmpty={false} />;
    }

    if (this.props.column.name === 'sparePartNodeId') {
      const availableColumnValues = map(sparePartNodes, sparePartNode => ({
        value: sparePartNode.id,
        label: sparePartNode.name,
      }));

      const { id } = this.props.row;
      const { name } = this.props.column;

      let errorMessage = '';

      if (id && !isUndefined(editedRowsErrors[id]) && !isUndefined(editedRowsErrors[id][name])) {
        errorMessage = editedRowsErrors[id][name];
      }

      if (!id && !isUndefined(newRowErrors[name])) {
        errorMessage = newRowErrors[name];
      }

      return (
        <TableCell className={classes.lookupEditCell} {...rest}>
          <AutoComplete
            container={container}
            errorMessage={errorMessage}
            initialSelectedOptionId={rest.value}
            options={availableColumnValues}
            onSelect={value => rest.onValueChange(value)}
            placeholder="Введите название узла"
          />
        </TableCell>
      );
    }

    if (this.props.column.name === 'totalCost') {
      return (
        <TableCell className={classes.lookupEditCell}>
          {`${rest.value} руб.`}
        </TableCell>
      );
    }

    return <CustomTextInput newRowErrors={newRowErrors} editedRowsErrors={editedRowsErrors} {...rest} />;
  }
}

export default CarWorkEditableEditCell;
