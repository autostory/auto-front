import React from 'react';
import find from 'lodash/find';
import isEmpty from 'lodash/isEmpty';
import { Table } from '@devexpress/dx-react-grid-material-ui';
import { connect } from 'react-redux';
import TableCell from '@material-ui/core/TableCell';

import { getServiceWorkStatusTypeName } from '../../../../../../common-code/constants/serviceWorkStatusTypes';

@connect(state => ({
  managers: state.carWorkEditableTableStore.managers,
  serviceWorkActionsTypes: state.carWorkEditableTableStore.serviceWorkActionsTypes,
  sparePartNodes: state.carWorkEditableTableStore.sparePartNodes,
}))
class CarWorkEditableCell extends React.PureComponent {
  render() {
    const {
      managers, serviceWorkActionsTypes, sparePartNodes, dispatch, value, ...rest
    } = this.props;

    if (rest.column.name === 'masterId') {
      let formattedValue = null;
      if (!isEmpty(value)) {
        formattedValue = find(managers, { id: value }).fullName;
      } else {
        formattedValue = 'не указан';
      }

      return <Table.Cell {...rest} value={formattedValue} />;
    }

    if (rest.column.name === 'serviceWorkActionTypeId') {
      let formattedValue = null;
      if (!isEmpty(value)) {
        formattedValue = find(serviceWorkActionsTypes, { id: value }).name;
      }

      return <Table.Cell {...rest} value={formattedValue} />;
    }

    if (rest.column.name === 'sparePartNodeId') {
      let formattedValue = null;
      if (!isEmpty(value)) {
        formattedValue = find(sparePartNodes, { id: value }).name;
      }

      return <Table.Cell {...rest} value={formattedValue} />;
    }

    if (rest.column.name === 'status') {
      if (!isEmpty(value)) {
        const formattedValue = getServiceWorkStatusTypeName(value);

        return <Table.Cell {...rest} value={formattedValue} />;
      }

      return <TableCell />;
    }

    return <Table.Cell {...rest} value={value} />;
  }
}

export default CarWorkEditableCell;
