import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Relay from 'react-relay/classic';
import withProps from 'recompose/withProps';
import { Cascader } from 'antd';
import isUndefined from 'lodash/isUndefined';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';
import ConfiguredRadium from './../../../../radium/ConfiguredRadium';

import './SparePartNodeSelector.css';

const listToTree = (_list) => {
  const list = _list;
  const map = {};
  const roots = [];
  for (let i = 0; i < list.length; i += 1) {
    map[list[i].id] = i;
    list[i].children = [];
  }
  for (let i = 0; i < list.length; i += 1) {
    const node = list[i];
    if (node.parent !== null && !isUndefined(node.parent.id)) {
      list[map[node.parent.id]].children.push(node);
    } else {
      roots.push(node);
    }
  }

  return roots;
};

const filedNames = { label: 'name', value: 'id', children: 'children' };

@withProps({ renderLoading: () => <CircularProgress size={10} /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          basic {
              spareParts {
                  id
                  parent {
                      id
                  }
                  name
              }
          }
      }
  `,
})
@ConfiguredRadium
class SparePartNodeCascader extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      text: null,
      selectedOptions: [],
    };
  }

  onChange = (value, selectedOptions) => {
    this.setState({
      text: selectedOptions.map(o => o.name).join(', '),
    });
  };

  filter = (inputValue, path) =>
    path.some(option => (option.name).toLowerCase().indexOf(inputValue.toLowerCase()) > -1);

  render() {
    const { selectedOptions } = this.state;
    const options = listToTree(this.props.viewer.basic.spareParts);

    return (
      <span>
        {this.state.text}
        <Cascader
          defaultValue={selectedOptions}
          popupClassName="SparePartNodeSelector"
          size="small"
          placeholder="Укажите узел"
          options={options}
          onChange={this.onChange}
          filedNames={filedNames}
          showSearch={{ filter: this.filter }}
        />
      </span>
    );
  }
}

export default SparePartNodeCascader;
