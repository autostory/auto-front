import find from 'lodash/find';
import React from 'react';
import assign from 'lodash/assign';
import each from 'lodash/each';
import uniqBy from 'lodash/uniqBy';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';
import cloneDeep from 'lodash/cloneDeep';
import isUndefined from 'lodash/isUndefined';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';
import { Getter } from '@devexpress/dx-react-core';
import {
  SortingState,
  EditingState,
  IntegratedSorting,
  SummaryState,
  IntegratedSummary,
} from '@devexpress/dx-react-grid';
import { Grid, Table, TableHeaderRow, TableEditRow, TableSummaryRow } from '@devexpress/dx-react-grid-material-ui';
import Paper from '@material-ui/core/Paper';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import { withStyles } from '@material-ui/core/styles';
import get from 'lodash/get';
import map from 'lodash/map';
import uuid from 'uuid/v4';
import concat from 'lodash/concat';
import { connect } from 'react-redux';
import { SERVICE_WORK_DONE } from '../../../../../../common-code/constants/serviceWorkStatusTypes';

import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';
import {
  setManagers,
  setServiceWorkActionsTypes,
  setSparePartNodes,
  setAddedRows,
  setEditingRowIds,
  setRowChanges,
} from '../../../../reduxActions/CarWorkEditableTableActions';
import CarWorkRemoveNotifer from './CarWorkRemoveNotifer';
import CurrencyTypeProvider from './currency-type-provider';
import PercentTypeProvider from './percent-type-provider';
import IntegerAmountTypeProvider from './integer-amount-type-provider';
import LaborHourTypeProvider from './labor-hour-type-provider';
import EditCell from './CarWorkEditableEditCell';
import Cell from './CarWorkEditableCell';
import TableEditColumn from './TableEditColumnCustomPlugin';

const styles = theme => ({
  dialog: {
    width: 'calc(100% - 16px)',
  },
  table: {
    overflow: 'visible',
  },
});

const AddButton = ({ onExecute }) => (
  <div style={{ textAlign: 'center' }}>
    <Button
      color="primary"
      onClick={onExecute}
      title="Добавить работу"
    >
      Добавить
    </Button>
  </div>
);

const EditButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Редактировать">
    <EditIcon />
  </IconButton>
);

const DeleteButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Удалить">
    <DeleteIcon />
  </IconButton>
);

const CommitButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Сохранить">
    <SaveIcon />
  </IconButton>
);

const CancelButton = ({ onExecute }) => (
  <IconButton color="secondary" onClick={onExecute} title="Отменить изменнеия">
    <CancelIcon />
  </IconButton>
);

const commandComponents = {
  add: AddButton,
  edit: EditButton,
  delete: DeleteButton,
  commit: CommitButton,
  cancel: CancelButton,
};

const Command = ({ id, onExecute, onClick }) => {
  const CommandButton = commandComponents[id];
  return (
    <CommandButton
      onExecute={onClick(onExecute)}
    />
  );
};

const summaryCalculator = (type, rows, getValue) => {
  if (type === 'sum') {
    if (!rows.length) {
      return 0;
    }

    let sum = 0;
    each(rows, (row) => {
      sum += parseFloat(row.totalCost);
    });

    return sum.toFixed(2);
  }

  if (type === 'sumLaborHour') {
    if (!rows.length) {
      return 0;
    }

    let sum = 0;
    each(rows, (row) => {
      sum += parseFloat(row.laborHour);
    });

    return sum.toFixed(2);
  }

  return IntegratedSummary.defaultCalculator(type, rows, getValue);
};

const HeaderCell = props => <TableHeaderRow.Cell {...props} />;
const getRowId = row => row.id;

@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          office(officeId: $officeId) {
              id
              title
              managers {
                  edges {
                      node {
                          id
                          fullName
                          active
                      }
                  }
              }
          }
          basic {
              serviceWorkActionsType {
                  id
                  name
              }
              spareParts {
                  id
                  name
              }
          }
      }
  `,
})
@withStyles(styles)
@connect(
  state => ({
    editingRowIds: state.carWorkEditableTableStore.editingRowIds,
    addedRows: state.carWorkEditableTableStore.addedRows,
    rowChanges: state.carWorkEditableTableStore.rowChanges,
  }),
  dispatch => ({
    setManagers: managers => dispatch(setManagers(managers)),
    setSparePartNodes: sparePartNodes => dispatch(setSparePartNodes(sparePartNodes)),
    setServiceWorkActionsTypes: serviceWorkActionsTypes =>
      dispatch(setServiceWorkActionsTypes(serviceWorkActionsTypes)),
    setRowChanges: value => dispatch(setRowChanges(value)),
    setEditingRowIds: value => dispatch(setEditingRowIds(value)),
    setAddedRows: value => dispatch(setAddedRows(value)),
  }),
)
class CarWorkEditableTable extends React.PureComponent {
  static defaultProps = {
    onChange: () => {},
    disabled: false,
  };

  static propTypes = {
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    const managers = map(get(props, 'viewer.office.managers.edges'), 'node');
    const serviceWorkActionsTypes = get(props, 'viewer.basic.serviceWorkActionsType');
    const spareParts = get(props, 'viewer.basic.spareParts');

    props.setManagers(managers);
    props.setServiceWorkActionsTypes(serviceWorkActionsTypes);
    props.setSparePartNodes(uniqBy(spareParts, 'name'));

    const rows = this.withTotalCosts(props.serviceWorks);

    this.state = {
      columns: [
        { name: 'serviceWorkActionTypeId', title: 'Тип работы' },
        { name: 'sparePartNodeId', title: 'Узел' },
        { name: 'name', title: 'Описание' },
        { name: 'laborHour', title: 'Норма-час' },
        { name: 'masterId', title: 'Мастер' },
        { name: 'amount', title: 'Кол-во' },
        { name: 'costPerUnit', title: 'Цена за ед. работы' },
        { name: 'discountPercent', title: 'Скидка' },
        { name: 'totalCost', title: 'К оплате' },
        { name: 'status', title: 'Статус' },
      ],
      tableColumnExtensions: [
        { columnName: 'serviceWorkActionTypeId', wordWrapEnabled: true, width: 100 },
        { columnName: 'sparePartNodeId', wordWrapEnabled: true, width: 200 },
        { columnName: 'name', wordWrapEnabled: true, width: 200 },
        { columnName: 'costPerUnit', wordWrapEnabled: true, width: 90 },
        { columnName: 'amount', wordWrapEnabled: true, width: 80 },
        { columnName: 'laborHour', wordWrapEnabled: true, width: 110 },
        { columnName: 'discountPercent', wordWrapEnabled: true, width: 90 },
        { columnName: 'totalCost', wordWrapEnabled: true },
        { columnName: 'master', wordWrapEnabled: true },
        { columnName: 'status', wordWrapEnabled: true },
      ],
      editingColumnExtensions: [
        { columnName: 'totalCost', editingEnabled: false },
      ],
      rows,
      sorting: [],
      editingRowIds: [],
      addedRows: [],
      newRowErrors: {},
      editedRowsErrors: {},
      rowChanges: {},
      deletingRows: [],
      currencyColumns: ['costPerUnit', 'totalCost'],
      percentColumns: ['discountPercent'],
      deletedRowsIds: [],
      amountColumns: ['amount'],
      laborHourColumns: ['laborHour'],
      totalSummaryItems: [
        {
          columnName: 'totalCost', type: 'sum',
        },
        {
          columnName: 'laborHour', type: 'sumLaborHour',
        },
      ],
      totalSummaryMessages: {
        sum: 'Итого',
        sumLaborHour: 'Итого',
      },
      container: null,
    };

    if (props.addedRows) {
      this.state.addedRows = props.addedRows;
    }

    if (props.rowChanges) {
      this.state.rowChanges = props.rowChanges;
    }

    if (props.editingRowIds) {
      this.state.editingRowIds = props.editingRowIds;
    }

    this.changeSorting = sorting => this.setState({ sorting });
    this.changeEditingRowIds = editingRowIds => this.setState({ editingRowIds });
    this.changeAddedRows = (addedRows) => {
      const managers = map(get(props, 'viewer.office.managers.edges'), 'node');
      const serviceWorkActionsTypes = get(props, 'viewer.basic.serviceWorkActionsType');

      return this.setState({
        addedRows: addedRows.map(row => (Object.keys(row).length ? row : {
          amount: 1,
          discountPercent: 0,
          sparePartNodeId: null,
          laborHour: 0,
          costPerUnit: null,
          serviceWorkActionTypeId: serviceWorkActionsTypes[0].id,
          name: '',
          masterId: get(managers.filter(m => m.active), '[0].id', null),
          totalCost: 0,
          status: null,
        })),
      });
    };
    this.changeRowChanges = rowChanges => this.setState({ rowChanges });
    this.commitChanges = ({ added, changed, deleted }) => {
      let { rows } = this.state;
      if (added) {
        rows = [
          ...rows,
          ...added.map(row => ({
            id: `NEW_ROW_${uuid()}`,
            ...row,
          })),
        ];
      }

      if (changed) {
        rows = rows.map(row => (changed[row.id] ? { ...row, ...changed[row.id] } : row));
      }

      this.setState({
        rows,
        deletingRows: deleted || this.state.deletingRows,
      });
    };
    this.cancelDelete = () => this.setState({ deletingRows: [] });
    this.deleteRows = () => {
      const rows = this.state.rows.slice();
      let deletedRowId;
      this.state.deletingRows.forEach((rowId) => {
        const index = rows.findIndex(row => row.id === rowId);
        if (index > -1) {
          if (rowId.indexOf('NEW_ROW') === -1) {
            deletedRowId = rowId;
          }
          rows.splice(index, 1);
        }
      });

      this.setState({
        rows,
        deletingRows: [],
        deletedRowsIds: deletedRowId ? concat(this.state.deletedRowsIds, deletedRowId) : this.state.deletedRowsIds,
      });
    };
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.officeId !== this.props.officeId) {
      const managers = map(get(this.props, 'viewer.office.managers.edges'), 'node');
      this.props.setManagers(managers);
    }

    if (!isEqual(prevProps.rows, this.props.rows)) {
      const rows = this.withTotalCosts(props.serviceWorks);

      this.setState({
        rows,
      });
    }

    const {
      onChange, setRowChanges, setEditingRowIds, setAddedRows,
    } = this.props;
    const { rowChanges, editingRowIds, addedRows } = this.state;

    setRowChanges(rowChanges);
    setEditingRowIds(editingRowIds);
    setAddedRows(addedRows);

    const clearRows = map(cloneDeep(this.state.rows), (row) => {
      delete (row.totalCost);

      return row;
    });

    onChange(clearRows, this.state.deletedRowsIds);

    if (!isEqual(this.state.addedRows, prevState.addedRows)) {
      this.validateNewRows();
    }

    if (!isEqual(this.state.rowChanges, prevState.rowChanges)) {
      this.validateEditRows();
    }
  };

  withTotalCosts = rows => map(rows, (sw) => {
    const totalCost = (parseFloat(sw.costPerUnit) * parseFloat(sw.amount))
      * ((100 - parseFloat(sw.discountPercent)) / 100);

    return {
      ...sw,
      totalCost: totalCost.toFixed(2),
    };
  });

  validateNewRows = () => {
    const { addedRows } = this.state;

    const newRow = addedRows[0]; // У нас добавлять можно только по 1 за раз
    if (newRow) {
      const addedErrors = this.validateNewRow(newRow);

      this.setState({
        addedRows: [...addedRows], // чтобы сработал render
        newRowErrors: addedErrors,
      });
    }
  };

  validateEditRows = () => {
    const { editingRowIds } = this.state;
    const { rowChanges } = this.state;

    const editedRowsErrors = {};

    map(editingRowIds, (rowId) => {
      const changes = rowChanges[rowId];
      if (changes) {
        const rowErrors = this.validateEditRow(rowChanges[rowId]);
        if (!isEmpty(rowErrors)) {
          editedRowsErrors[rowId] = rowErrors;
        }
      }
    });

    this.setState({
      editedRowsErrors,
      editingRowIds: [...this.state.editingRowIds],
    });
  };

  onTryRowAdd = onExecute => () => {
    const { addedRows } = this.state;
    const newRow = addedRows[0]; // У нас добавлять можно только по 1 за раз
    if (newRow) {
      const addedErrors = this.validateNewRow(newRow);

      this.setState({
        addedRows: [...addedRows], // чтобы сработал render
        newRowErrors: addedErrors,
      });

      if (!isEmpty(addedErrors)) {
        return false;
      }
    }

    return onExecute();
  };

  onTryRowSaveChanges = (rowId, onExecute) => () => {
    const { rowChanges } = this.state;
    const editRow = rowChanges[rowId];

    if (editRow) {
      const editRowErrors = this.validateEditRow(editRow);

      const { editedRowsErrors } = this.state;
      if (!isEmpty(editRowErrors)) {
        editedRowsErrors[rowId] = editRowErrors;
      } else if (!isUndefined(editedRowsErrors[rowId])) {
        delete editedRowsErrors[rowId];
      }

      const newEditedRowsErrors = assign({}, editedRowsErrors);

      this.setState({
        editedRowsErrors: newEditedRowsErrors,
      });

      if (!isEmpty(editRowErrors)) {
        return false;
      }
    }

    return onExecute();
  };

  editCell = cellProps => {
    const { container } = this.state;

    return (
      <EditCell
        container={container}
        newRowErrors={this.state.newRowErrors}
        editedRowsErrors={this.state.editedRowsErrors}
        {...cellProps}
      />
    );
  };

  validateNewRow = (row) => {
    const errors = {};

    if (!row.amount) {
      errors.amount = 'Обязательно для заполнения';
    }

    if (row.costPerUnit === '' || row.costPerUnit === null) {
      errors.costPerUnit = 'Обязательно для заполнения';
    }

    if (row.laborHour === '' || row.laborHour === null) {
      errors.laborHour = 'Обязательно для заполнения';
    }

    if (!row.sparePartNodeId) {
      errors.sparePartNodeId = 'Обязательно для заполнения';
    }

    if (isUndefined(row.discountPercent) || (row.discountPercent !== 0 && !row.discountPercent)) {
      errors.discountPercent = 'Обязательно для заполнения';
    }

    return errors;
  };

  validateEditRow = (row) => {
    const errors = {};

    if (row.hasOwnProperty('sparePartNodeId') && !row.sparePartNodeId) {
      errors.sparePartNodeId = 'Обязательно для заполнения';
    }

    if (row.hasOwnProperty('amount') && !row.amount) {
      errors.amount = 'Обязательно для заполнения';
    }

    if (row.hasOwnProperty('laborHour') && (row.laborHour !== 0 && !row.laborHour)) {
      errors.laborHour = 'Обязательно для заполнения';
    }

    if (row.hasOwnProperty('costPerUnit') && (row.costPerUnit !== 0 && !row.costPerUnit)) {
      errors.costPerUnit = 'Обязательно для заполнения';
    }

    if (row.hasOwnProperty('discountPercent') && (row.discountPercent !== 0 && !row.discountPercent)) {
      errors.discountPercent = 'Обязательно для заполнения';
    }

    return errors;
  };

  onActionButtonClick = (id, rowIds) => (onExecute) => {
    if (!rowIds) {
      return onExecute;
    }

    const rowId = rowIds[0];
    if (id === 'commit') {
      if (rowId === 0) {
        return this.onTryRowAdd(onExecute);
      }

      return this.onTryRowSaveChanges(rowId, onExecute);
    }

    return onExecute;
  };

  commandButton = ({ id, onExecute, rowIds }) => {
    if (id === 'edit' || id === 'delete') {
      const rowId = rowIds[0];
      const row = find(this.state.rows, { id: rowId });

      if (row.status === SERVICE_WORK_DONE) {
        return null;
      }
    }

    return (
      <div>
        <Command
          id={id}
          onExecute={onExecute}
          onClick={this.onActionButtonClick(id, rowIds)}
        />
      </div>
    );
  };

  render() {
    const { classes, disabled } = this.props;

    const {
      rows,
      columns,
      tableColumnExtensions,
      editingColumnExtensions,
      sorting,
      editingRowIds,
      addedRows,
      rowChanges,
      deletingRows,
      currencyColumns,
      percentColumns,
      amountColumns,
      laborHourColumns,
      totalSummaryItems,
      totalSummaryMessages,
    } = this.state;

    return (
      <Paper>
        <div
          ref={(node) => {
            this.setState({
              container: node,
            });
          }}
        />
        <Grid
          rows={rows}
          columns={columns}
          getRowId={getRowId}
        >
          <SortingState
            sorting={sorting}
            onSortingChange={this.changeSorting}
          />
          <SummaryState
            totalItems={totalSummaryItems}
          />

          <IntegratedSorting />
          <IntegratedSummary
            calculator={summaryCalculator}
          />

          <CurrencyTypeProvider for={currencyColumns} />
          <PercentTypeProvider for={percentColumns} />
          <IntegerAmountTypeProvider for={amountColumns} />
          <LaborHourTypeProvider for={laborHourColumns} />

          <EditingState
            editingRowIds={editingRowIds}
            onEditingRowIdsChange={this.changeEditingRowIds}
            rowChanges={rowChanges}
            onRowChangesChange={this.changeRowChanges}
            addedRows={addedRows}
            onAddedRowsChange={this.changeAddedRows}
            onCommitChanges={this.commitChanges}
            columnExtensions={editingColumnExtensions}
          />

          <Table
            className={classes.table}
            columnExtensions={tableColumnExtensions}
            cellComponent={Cell}
          />

          <TableHeaderRow
            cellComponent={HeaderCell}
            showSortingControls
          />

          <TableEditRow
            cellComponent={this.editCell}
          />

          {!disabled && (
            <TableEditColumn
              width={120}
              showAddCommand={!addedRows.length}
              showEditCommand
              showDeleteCommand
              commandComponent={this.commandButton}
            />
          )}

          <TableSummaryRow
            messages={totalSummaryMessages}
          />

          {/* Для того чтобы кнопки были справа */}
          {/*<Getter*/}
            {/*name="tableColumns"*/}
            {/*computed={({ tableColumns }) => [*/}
              {/*...tableColumns.filter(c => c.type !== 'editCommand'),*/}
              {/*{ key: 'editCommand', type: 'editCommand', width: 200 }]*/}
            {/*}*/}
          {/*/>*/}
        </Grid>

        <Dialog
          open={!!deletingRows.length}
          onClose={this.cancelDelete}
          classes={{ paper: classes.dialog }}
        >
          <DialogTitle>Удаление записи</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Вы действительно хотите удалить следующую запись?
            </DialogContentText>
            <Paper>
              <Grid
                rows={rows.filter(row => deletingRows.indexOf(row.id) > -1)}
                columns={columns}
              >
                <CurrencyTypeProvider for={currencyColumns} />
                <PercentTypeProvider for={percentColumns} />
                <IntegerAmountTypeProvider for={amountColumns} />
                <LaborHourTypeProvider for={laborHourColumns} />

                <Table
                  columnExtensions={tableColumnExtensions}
                  cellComponent={Cell}
                />
                <TableHeaderRow />
              </Grid>
            </Paper>
          </DialogContent>
          <DialogActions>
            {deletingRows[0] && (
              <CarWorkRemoveNotifer
                initialVariables={{ serviceWorkId: deletingRows[0] }}
              >
                <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-end' }}>
                  <Button onClick={this.cancelDelete} color="primary">Отмена</Button>
                  <Button onClick={this.deleteRows} color="secondary">Удалить</Button>
                </div>
              </CarWorkRemoveNotifer>
            )}
          </DialogActions>
        </Dialog>
      </Paper>
    );
  }
}

export default withStyles(styles)(CarWorkEditableTable);
