import React from 'react';
import find from 'lodash/find';
import isEmpty from 'lodash/isEmpty';
import { Table } from '@devexpress/dx-react-grid-material-ui';
import { connect } from 'react-redux';

@connect(state => ({
  unitTypes: state.carWorkSparePartsEditableTableStore.unitTypes,
}))
class CarWorkSparePartsEditableCell extends React.PureComponent {
  render() {
    const { unitTypes, dispatch, value, ...rest } = this.props;

    if (rest.column.name === 'unitMeasureTypeId') {
      let formattedValue = null;
      if (!isEmpty(value)) {
        formattedValue = find(unitTypes, { id: value }).name;
      }

      return <Table.Cell {...rest} value={formattedValue} />;
    }

    return <Table.Cell {...rest} value={value} />;
  }
}

export default CarWorkSparePartsEditableCell;
