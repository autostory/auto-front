import React from 'react';
import assign from 'lodash/assign';
import find from 'lodash/find';
import isEmpty from 'lodash/isEmpty';
import isUndefined from 'lodash/isUndefined';
import map from 'lodash/map';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Relay from 'react-relay/classic';
import { Getter } from '@devexpress/dx-react-core';
import { EditingState } from '@devexpress/dx-react-grid';
import { Grid, Table, TableHeaderRow, TableEditRow } from '@devexpress/dx-react-grid-material-ui';
import Paper from '@material-ui/core/Paper';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import { withStyles } from '@material-ui/core/styles';
import uuid from 'uuid/v4';
import get from 'lodash/get';
import concat from 'lodash/concat';
import isEqual from 'lodash/isEqual';
import { SCHEDULE_DONE, SCHEDULE_WAIT_WORK_START } from '../../../../../../common-code/constants/scheduleStatusTypes';

import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import {
  setAddedRows, setEditingRowIds, setRowChanges,
} from '../../../../reduxActions/OrderScheduleEditableTableActions';
import EditCell from './ScheduleEditCell';
import Cell from './ScheduleCell';
import TableEditColumn from './TableEditColumnCustomPlugin';

const styles = theme => ({
  dialog: {
    width: 'calc(100% - 16px)',
  },
  table: {
    overflow: 'visible',
  },
});

const rawStyle = {
  addButton: {
    textAlign: 'center',
  },
};

const AddButton = ({ onExecute }) => (
  <div style={rawStyle.addButton}>
    <Button
      color="primary"
      onClick={onExecute}
      title="Добавить бронь"
    >
      Добавить
    </Button>
  </div>
);

const EditButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Редактировать">
    <EditIcon />
  </IconButton>
);

const DeleteButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Удалить">
    <DeleteIcon />
  </IconButton>
);

const CommitButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Сохранить">
    <SaveIcon />
  </IconButton>
);

const CancelButton = ({ onExecute }) => (
  <IconButton color="secondary" onClick={onExecute} title="Отменить изменнеия">
    <CancelIcon />
  </IconButton>
);

const commandComponents = {
  add: AddButton,
  edit: EditButton,
  delete: DeleteButton,
  commit: CommitButton,
  cancel: CancelButton,
};

const Command = ({ id, onExecute, onClick }) => {
  const CommandButton = commandComponents[id];
  return (
    <CommandButton
      onExecute={onClick(onExecute)}
    />
  );
};

const HeaderCell = props => <TableHeaderRow.Cell {...props} />;
const getRowId = row => row.id;

@relayContainer({
  office: () => Relay.QL`
      fragment on Office {
          ${EditCell.getFragment('office')}
          ${Cell.getFragment('office')}
          boxes(active: true) {
              id
              title
          }
      }
  `,
  order: () => Relay.QL`
      fragment on Order {
          id
          ${EditCell.getFragment('order')}
          ${Cell.getFragment('order')}
      }
  `,
})
@withStyles(styles)
@connect(
  state => ({
    editingRowIds: state.orderScheduleEditableTableStore.editingRowIds,
    addedRows: state.orderScheduleEditableTableStore.addedRows,
    rowChanges: state.orderScheduleEditableTableStore.rowChanges,
  }),
  dispatch => ({
    setRowChanges: value => dispatch(setRowChanges(value)),
    setEditingRowIds: value => dispatch(setEditingRowIds(value)),
    setAddedRows: value => dispatch(setAddedRows(value)),
  }),
)
class OrderSchedulesEditableTable extends React.PureComponent {
  static defaultProps = {
    afterScheduleStatusChange: () => {},
    onChange: () => {},
  };

  static propTypes = {
    afterScheduleStatusChange: PropTypes.func,
    onChange: PropTypes.func,
  };

  constructor(props) {
    super(props);

    const rows = props.schedules;

    this.state = {
      columns: [
        { name: 'serviceWorks', title: 'Работы' },
        { name: 'boxId', title: 'Бокс' },
        { name: 'start', title: 'начало' },
        { name: 'finish', title: 'окончание' },
        { name: 'status', title: 'статус' },
      ],
      rows,
      editingRowIds: [],
      addedRows: [],
      rowChanges: {},
      deletingRows: [],
      deletedRowsIds: [],
      editedRowsErrors: [],
      newRowErrors: [],
    };

    if (props.addedRows) {
      this.state.addedRows = props.addedRows;
    }

    if (props.rowChanges) {
      this.state.rowChanges = props.rowChanges;
    }

    if (props.editingRowIds) {
      this.state.editingRowIds = props.editingRowIds;
    }

    this.changeEditingRowIds = editingRowIds => this.setState({ editingRowIds });
    this.changeAddedRows = (addedRows) => {
      const now = new Date();
      now.setSeconds(0);
      now.setMilliseconds(0);

      this.setState({
        addedRows: addedRows.map(row => (Object.keys(row).length ? row : {
          boxId: get(this.props, 'office.boxes[0].id', null),
          start: now.getTime(),
          finish: now.getTime(),
          serviceWorks: [],
          status: SCHEDULE_WAIT_WORK_START,
        })),
      });
    };
    this.changeRowChanges = rowChanges => this.setState({ rowChanges });
    this.commitChanges = ({ added, changed, deleted }) => {
      let { rows } = this.state;
      if (added) {
        const addedRow = added.map(row => ({
          id: `NEW_ROW_${uuid()}`,
          ...row,
        }));

        if (rows) {
          rows = [
            ...rows,
            ...addedRow,
          ];
        } else {
          rows = [
            ...addedRow,
          ];
        }
      }

      if (changed) {
        rows = rows.map(row => (changed[row.id] ? { ...row, ...changed[row.id] } : row));
      }

      this.setState({
        rows,
        deletingRows: deleted || this.state.deletingRows,
      });
    };
    this.cancelDelete = () => this.setState({ deletingRows: [] });
    this.deleteRows = () => {
      const rows = this.state.rows.slice();
      let deletedRowId;
      this.state.deletingRows.forEach((rowId) => {
        const index = rows.findIndex(row => row.id === rowId);
        if (index > -1) {
          if (rowId.indexOf('NEW_ROW') === -1) {
            deletedRowId = rowId;
          }
          rows.splice(index, 1);
        }
      });

      this.setState({
        rows,
        deletingRows: [],
        deletedRowsIds: deletedRowId ? concat(this.state.deletedRowsIds, deletedRowId) : this.state.deletedRowsIds,
      });
    };
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      onChange, setRowChanges, setEditingRowIds, setAddedRows, schedules,
    } = this.props;

    if (!isEqual(prevProps.schedules, schedules)) {
      this.setState({
        rows: schedules,
      });
    }

    if (
      !isEqual(prevState.rows, this.state.rows)
      || !isEqual(prevState.deletedRowsIds, this.state.deletedRowsIds)
    ) {
      onChange(this.state.rows, this.state.deletedRowsIds);
    }

    const { rowChanges, editingRowIds, addedRows } = this.state;

    setRowChanges(rowChanges);
    setEditingRowIds(editingRowIds);
    setAddedRows(addedRows);

    onChange(this.state.rows, this.state.deletedRowsIds);

    if (!isEqual(this.state.addedRows, prevState.addedRows)) {
      this.validateNewRows();
    }

    if (!isEqual(this.state.rowChanges, prevState.rowChanges)) {
      this.validateEditRows();
    }
  }

  validateNewRows = () => {
    const { addedRows } = this.state;

    const newRow = addedRows[0]; // У нас добавлять можно только по 1 за раз
    if (newRow) {
      const addedErrors = this.validateNewRow(newRow);

      this.setState({
        addedRows: [...addedRows], // чтобы сработал render
        newRowErrors: addedErrors,
      });
    }
  };

  validateEditRows = () => {
    const { editingRowIds } = this.state;
    const { rowChanges } = this.state;

    const editedRowsErrors = {};

    map(editingRowIds, (rowId) => {
      const changes = rowChanges[rowId];
      if (changes) {
        const rowErrors = this.validateEditRow(rowChanges[rowId]);
        if (!isEmpty(rowErrors)) {
          editedRowsErrors[rowId] = rowErrors;
        }
      }
    });

    this.setState({
      editedRowsErrors,
      editingRowIds: [...this.state.editingRowIds],
    });
  };

  onTryRowAdd = onExecute => () => {
    const { addedRows } = this.state;
    const newRow = addedRows[0]; // У нас добавлять можно только по 1 за раз
    if (newRow) {
      const addedErrors = this.validateNewRow(newRow);

      this.setState({
        addedRows: [...addedRows], // чтобы сработал render
        newRowErrors: addedErrors,
      });

      if (!isEmpty(addedErrors)) {
        return false;
      }
    }

    return onExecute();
  };

  onTryRowSaveChanges = (rowId, onExecute) => () => {
    const { rowChanges } = this.state;
    const editRow = rowChanges[rowId];

    if (editRow) {
      const editRowErrors = this.validateEditRow(editRow);

      const { editedRowsErrors } = this.state;
      if (!isEmpty(editRowErrors)) {
        editedRowsErrors[rowId] = editRowErrors;
      } else if (!isUndefined(editedRowsErrors[rowId])) {
        delete editedRowsErrors[rowId];
      }

      const newEditedRowsErrors = assign({}, editedRowsErrors);

      this.setState({
        editedRowsErrors: newEditedRowsErrors,
      });

      if (!isEmpty(editRowErrors)) {
        return false;
      }
    }

    return onExecute();
  };

  validateNewRow = (row) => {
    const errors = {};

    if (!row.start) {
      errors.start = 'Обязательно для заполнения';
    }

    if (!row.finish) {
      errors.finish = 'Обязательно для заполнения';
    }

    if (row.start && row.finish && row.start > row.finish) {
      errors.start = 'Начало работ не может быть больше окончания';
      errors.finish = 'Окончание работ не может быть меньше начала работ';
    }

    if (row.start && row.finish && row.start === row.finish) {
      errors.start = 'Начало и окончание работ не должны совпадать';
      errors.finish = 'Начало и окончание работ не должны совпадать';
    }

    if (!row.serviceWorks || !row.serviceWorks.length) {
      errors.serviceWorks = 'Необходимо указать работы для расписания';
    }

    return errors;
  };

  validateEditRow = (row) => {
    const errors = {};

    if (row.hasOwnProperty('sparePartNodeId') && !row.sparePartNodeId) {
      errors.sparePartNodeId = 'Обязательно для заполнения';
    }

    if (row.hasOwnProperty('amount') && !row.amount) {
      errors.amount = 'Обязательно для заполнения';
    }

    if (row.hasOwnProperty('laborHour') && (row.laborHour !== 0 && !row.laborHour)) {
      errors.laborHour = 'Обязательно для заполнения';
    }

    if (row.hasOwnProperty('costPerUnit') && (row.costPerUnit !== 0 && !row.costPerUnit)) {
      errors.costPerUnit = 'Обязательно для заполнения';
    }

    if (row.hasOwnProperty('discountPercent') && (row.discountPercent !== 0 && !row.discountPercent)) {
      errors.discountPercent = 'Обязательно для заполнения';
    }

    return errors;
  };

  onActionButtonClick = (id, rowIds) => (onExecute) => {
    if (!rowIds) {
      return onExecute;
    }

    const rowId = rowIds[0];
    if (id === 'commit') {
      if (rowId === 0) {
        return this.onTryRowAdd(onExecute);
      }

      return this.onTryRowSaveChanges(rowId, onExecute);
    }

    return onExecute;
  };

  commandButton = ({ id, onExecute, rowIds }) => {
    if (id === 'edit' || id === 'delete') {
      const rowId = rowIds[0];
      const row = find(this.state.rows, { id: rowId });

      if (row.status === SCHEDULE_DONE) {
        return null;
      }
    }

    return (
      <div>
        <Command
          id={id}
          onExecute={onExecute}
          onClick={this.onActionButtonClick(id, rowIds)}
        />
      </div>
    );
  };

  editCell = cellProps => (
    <EditCell
      {...cellProps}
      newRowErrors={this.state.newRowErrors}
      editedRowsErrors={this.state.editedRowsErrors}
      office={this.props.office}
      rowChanges={this.state.rowChanges}
      afterScheduleStatusChange={this.props.afterScheduleStatusChange}
      order={this.props.order}
      disabled={this.props.disabled}
      serviceWorks={this.props.serviceWorks}
    />
  );

  cell = cellProps => (
    <Cell
      {...cellProps}
      afterScheduleStatusChange={this.props.afterScheduleStatusChange}
      office={this.props.office}
      order={this.props.order}
      disabled={this.props.disabled}
      serviceWorks={this.props.serviceWorks}
    />
  );

  render() {
    const { classes, disabled } = this.props;

    const {
      rows,
      columns,
      tableColumnExtensions,
      editingRowIds,
      addedRows,
      rowChanges,
      deletingRows,
    } = this.state;

    return (
      <Paper style={rawStyle.paper}>
        <Grid
          rows={rows || []}
          columns={columns}
          getRowId={getRowId}
        >
          <EditingState
            editingRowIds={editingRowIds}
            onEditingRowIdsChange={this.changeEditingRowIds}
            rowChanges={rowChanges}
            onRowChangesChange={this.changeRowChanges}
            addedRows={addedRows}
            onAddedRowsChange={this.changeAddedRows}
            onCommitChanges={this.commitChanges}
          />

          <Table
            className={classes.table}
            columnExtensions={tableColumnExtensions}
            cellComponent={this.cell}
          />

          <TableHeaderRow
            cellComponent={HeaderCell}
          />

          <TableEditRow
            cellComponent={this.editCell}
          />
          {!disabled && (
            <TableEditColumn
              width={120}
              showAddCommand={!addedRows.length}
              showEditCommand
              showDeleteCommand
              commandComponent={this.commandButton}
            />
          )}

          {/* Для того чтобы кнопки были справа */}
          {/* <Getter */}
          {/* name="tableColumns" */}
          {/* computed={({ tableColumns }) => [ */}
          {/* ...tableColumns.filter(c => c.type !== 'editCommand'), */}
          {/* { key: 'editCommand', type: 'editCommand', width: 200 }] */}
          {/* } */}
          {/* /> */}
        </Grid>

        <Dialog
          open={!!deletingRows.length}
          onClose={this.cancelDelete}
          classes={{ paper: classes.dialog }}
        >
          <DialogTitle>Удаление записи</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Вы действительно хотите удалить следующую запись?
            </DialogContentText>
            <Paper>
              {rows && deletingRows && (
                <Grid
                  rows={rows.filter(row => deletingRows.indexOf(row.id) > -1)}
                  columns={columns}
                >
                  <Table
                    columnExtensions={tableColumnExtensions}
                    cellComponent={this.cell}
                  />
                  <TableHeaderRow />
                </Grid>
              )}
            </Paper>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.cancelDelete} color="primary">Отмена</Button>
            <Button onClick={this.deleteRows} color="secondary">Удалить</Button>
          </DialogActions>
        </Dialog>
      </Paper>
    );
  }
}

export default withStyles(styles)(OrderSchedulesEditableTable);
