import cloneDeep from 'lodash/cloneDeep';
import each from 'lodash/each';
import React from 'react';
import concat from 'lodash/concat';
import PropTypes from 'prop-types';
import { Getter } from '@devexpress/dx-react-core';
import { SortingState, EditingState, IntegratedSorting, SummaryState, IntegratedSummary } from '@devexpress/dx-react-grid';
import { Grid, Table, TableHeaderRow, TableEditRow, TableSummaryRow } from '@devexpress/dx-react-grid-material-ui';
import Paper from '@material-ui/core/Paper';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import Relay from 'react-relay/classic';
import isUndefined from 'lodash/isUndefined';
import assign from 'lodash/assign';
import isEqual from 'lodash/isEqual';
import map from 'lodash/map';
import isEmpty from 'lodash/isEmpty';
import uuid from 'uuid/v4';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';
import {
  setUnitTypes, setRowChanges, setEditingRowIds, setAddedRows,
} from '../../../../reduxActions/CarWorkSparePartsEditableTableActions';
import CurrencyTypeProvider from './currency-type-provider';
import PercentTypeProvider from './percent-type-provider';
import EditCell from './CarWorkSparePartsEditableEditCell';
import Cell from './CarWorkSparePartsEditableCell';
import FloatAmountTypeProvider from './float-amount-type-provider';
import TableEditColumn from './TableEditColumnCustomPlugin';

const styles = theme => ({
  lookupEditCell: {
    paddingTop: theme.spacing.unit * 0.875,
    paddingRight: theme.spacing.unit,
    paddingLeft: theme.spacing.unit,
  },
  dialog: {
    width: 'calc(100% - 16px)',
  },
  inputRoot: {
    width: '100%',
  },
});

const rowStyle = {
  addButton: {
    textAlign: 'center',
  },
};

const AddButton = ({ onExecute }) => (
  <div style={rowStyle.addButton}>
    <Button
      color="primary"
      onClick={onExecute}
      title="Добавить работу"
    >
      Добавить
    </Button>
  </div>
);

const EditButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Редактировать">
    <EditIcon />
  </IconButton>
);

const DeleteButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Удалить">
    <DeleteIcon />
  </IconButton>
);

const CommitButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Сохранить">
    <SaveIcon />
  </IconButton>
);

const CancelButton = ({ onExecute }) => (
  <IconButton color="secondary" onClick={onExecute} title="Отменить изменнеия">
    <CancelIcon />
  </IconButton>
);

const commandComponents = {
  add: AddButton,
  edit: EditButton,
  delete: DeleteButton,
  commit: CommitButton,
  cancel: CancelButton,
};

const Command = ({ id, onExecute, onClick }) => {
  const CommandButton = commandComponents[id];
  return (
    <CommandButton
      onExecute={onClick(onExecute)}
    />
  );
};

const HeaderCell = props => <TableHeaderRow.Cell {...props} />;

const getRowId = row => row.id;

const summaryCalculator = (type, rows, getValue) => {
  if (type === 'sum') {
    if (!rows.length) {
      return 0;
    }

    let sum = 0;
    each(rows, (row) => {
      sum += parseFloat(row.totalCost);
    });

    return sum;
  }

  return IntegratedSummary.defaultCalculator(type, rows, getValue);
};

@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          basic {
              unitMeasureTypes {
                  id
                  name
              }
          }
      }
  `,
})
@connect(
  state => ({
    editingRowIds: state.carWorkSparePartsEditableTableStore.editingRowIds,
    addedRows: state.carWorkSparePartsEditableTableStore.addedRows,
    rowChanges: state.carWorkSparePartsEditableTableStore.rowChanges,
  }),
  dispatch => ({
    setUnitTypes: unitTypes => dispatch(setUnitTypes(unitTypes)),
    setRowChanges: value => dispatch(setRowChanges(value)),
    setEditingRowIds: value => dispatch(setEditingRowIds(value)),
    setAddedRows: value => dispatch(setAddedRows(value)),
  }),
)
class CarWorkSparePartsEditableTable extends React.PureComponent {
  static defaultProps = {
    onChange: () => {
    },
  };

  static propTypes = {
    onChange: PropTypes.func,
  };

  constructor(props) {
    super(props);

    const { unitMeasureTypes } = props.viewer.basic;
    props.setUnitTypes(unitMeasureTypes);

    const rows = this.withTotalCosts(props.serviceMaterials);

    this.state = {
      columns: [
        { name: 'name', title: 'Наименование' },
        { name: 'costPerUnit', title: 'Цена' },
        { name: 'unitMeasureTypeId', title: 'Ед. изм.' },
        { name: 'amount', title: 'Кол-во' },
        { name: 'discountPercent', title: 'Скидка' },
        { name: 'totalCost', title: 'К оплате' },
      ],
      tableColumnExtensions: [
        { columnName: 'name', wordWrapEnabled: true },
        { columnName: 'costPerUnit', wordWrapEnabled: true, width: 90 },
        { columnName: 'amount', wordWrapEnabled: true, width: 80 },
        { columnName: 'unitType', wordWrapEnabled: true, width: 80 },
        { columnName: 'totalCost', wordWrapEnabled: true },
        { columnName: 'discountPercent', wordWrapEnabled: true, width: 90 },
      ],
      rows,
      sorting: [],
      newRowErrors: {},
      editedRowsErrors: {},
      editingRowIds: [],
      addedRows: [],
      rowChanges: {},
      deletingRows: [],
      currencyColumns: ['costPerUnit', 'totalCost'],
      percentColumns: ['discountPercent'],
      deletedRowsIds: [],
      amountColumns: ['amount'],
      editingColumnExtensions: [
        { columnName: 'totalCost', editingEnabled: false },
      ],
      totalSummaryItems: [
        { columnName: 'totalCost', type: 'sum' },
      ],
      totalSummaryMessages: {
        sum: 'Итого',
      },
    };

    if (props.addedRows) {
      this.state.addedRows = props.addedRows;
    }

    if (props.rowChanges) {
      this.state.rowChanges = props.rowChanges;
    }

    if (props.editingRowIds) {
      this.state.editingRowIds = props.editingRowIds;
    }

    this.changeSorting = sorting => this.setState({ sorting });
    this.changeEditingRowIds = editingRowIds => this.setState({ editingRowIds });
    this.changeAddedRows = (addedRows) => {
      const { unitMeasureTypes } = props.viewer.basic;

      return this.setState({
        addedRows: addedRows.map(row => (Object.keys(row).length ? row : {
          name: '',
          costPerUnit: null,
          unitMeasureTypeId: unitMeasureTypes[0].id,
          amount: null,
          discountPercent: 0,
          totalCost: 0,
        })),
      });
    };
    this.changeRowChanges = rowChanges => this.setState({ rowChanges });
    this.commitChanges = ({ added, changed, deleted }) => {
      let { rows } = this.state;

      if (added) {
        rows = [
          ...rows,
          ...added.map(row => ({
            id: `NEW_ROW_${uuid()}`,
            ...row,
          })),
        ];
      }

      if (changed) {
        rows = rows.map(row => (changed[row.id] ? { ...row, ...changed[row.id] } : row));
      }

      this.setState({
        rows,
        deletingRows: deleted || this.state.deletingRows,
        editingRowIds: [...this.state.editingRowIds],
      });
    };
    this.cancelDelete = () => this.setState({ deletingRows: [] });
    this.deleteRows = () => {
      const rows = this.state.rows.slice();
      let deletedRowId;
      this.state.deletingRows.forEach((rowId) => {
        const index = rows.findIndex(row => row.id === rowId);
        if (index > -1) {
          if (rowId.indexOf('NEW_ROW') === -1) {
            deletedRowId = rowId;
          }
          rows.splice(index, 1);
        }
      });
      this.setState({
        rows,
        deletingRows: [],
        deletedRowsIds: deletedRowId ? concat(this.state.deletedRowsIds, deletedRowId) : this.state.deletedRowsIds,
      });
    };
  }

  withTotalCosts = rows => map(rows, (sw) => {
    const totalCost = (parseFloat(sw.costPerUnit) * parseFloat(sw.amount))
      * ((100 - parseFloat(sw.discountPercent)) / 100);

    return {
      ...sw,
      totalCost: totalCost.toFixed(2),
    };
  });

  componentDidUpdate = (prevProps, prevState) => {
    const {
      onChange, setRowChanges, setEditingRowIds, setAddedRows,
    } = this.props;
    const { rowChanges, editingRowIds, addedRows } = this.state;

    setRowChanges(rowChanges);
    setEditingRowIds(editingRowIds);
    setAddedRows(addedRows);

    if (!isEqual(prevProps.rows, this.props.rows)) {
      const rows = this.withTotalCosts(props.serviceMaterials);

      this.setState({
        rows,
      });
    }

    const clearRows = map(cloneDeep(this.state.rows), (row) => {
      delete (row.totalCost);

      return row;
    });

    onChange(clearRows, this.state.deletedRowsIds);

    if (!isEqual(this.state.addedRows, prevState.addedRows)) {
      this.validateNewRows();
    }

    if (!isEqual(this.state.rowChanges, prevState.rowChanges)) {
      this.validateEditRows();
    }
  };

  validateNewRows = () => {
    const { addedRows } = this.state;

    const newRow = addedRows[0]; // У нас добавлять можно только по 1 за раз
    if (newRow) {
      const addedErrors = this.validateNewRow(newRow);

      this.setState({
        addedRows: [...addedRows], // чтобы сработал render
        newRowErrors: addedErrors,
      });
    }
  };

  validateEditRows = () => {
    const { editingRowIds } = this.state;
    const { rowChanges } = this.state;

    const editedRowsErrors = {};

    map(editingRowIds, (rowId) => {
      const changes = rowChanges[rowId];
      if (changes) {
        const rowErrors = this.validateEditRow(rowChanges[rowId]);
        if (!isEmpty(rowErrors)) {
          editedRowsErrors[rowId] = rowErrors;
        }
      }
    });

    this.setState({
      editedRowsErrors,
      editingRowIds: [...this.state.editingRowIds],
    });
  };

  onTryRowAdd = onExecute => () => {
    console.log('onTryRowAdd call',);
    const { addedRows } = this.state;
    const newRow = addedRows[0]; // У нас добавлять можно только по 1 за раз
    if (newRow) {
      const addedErrors = this.validateNewRow(newRow);

      this.setState({
        addedRows: [...addedRows], // чтобы сработал render
        newRowErrors: addedErrors,
      });

      if (!isEmpty(addedErrors)) {
        return false;
      }
    }

    return onExecute();
  };

  onTryRowSaveChanges = (rowId, onExecute) => () => {
    console.log('onTryRowSaveChanges call rowId', rowId);
    const { rowChanges } = this.state;
    const editRow = rowChanges[rowId]; // У нас добавлять можно только по 1 за раз

    if (editRow) {
      const editRowErrors = this.validateEditRow(editRow);
      console.log('editRowErrors', editRowErrors);

      const { editedRowsErrors } = this.state;
      if (!isEmpty(editRowErrors)) {
        editedRowsErrors[rowId] = editRowErrors;
      } else if (!isUndefined(editedRowsErrors[rowId])) {
        delete editedRowsErrors[rowId];
      }

      const newEditedRowsErrors = assign({}, editedRowsErrors);

      this.setState({
        editedRowsErrors: newEditedRowsErrors,
      });

      if (!isEmpty(editRowErrors)) {
        return false;
      }
    }

    return onExecute();
  };

  editCell = cellProps => (
    <EditCell
      newRowErrors={this.state.newRowErrors}
      editedRowsErrors={this.state.editedRowsErrors}
      {...cellProps}
    />
  );

  validateNewRow = (row) => {
    const errors = {};

    if (row.name === '') {
      errors.name = 'Обязательно для заполнения';
    }

    if (row.amount === '' || row.amount === null) {
      errors.amount = 'Обязательно для заполнения';
    }

    if (row.costPerUnit === '' || row.costPerUnit === null) {
      errors.costPerUnit = 'Обязательно для заполнения';
    }

    if (isUndefined(row.discountPercent) || row.discountPercent === '' || row.discountPercent === null) {
      errors.discountPercent = 'Обязательно для заполнения';
    }

    return errors;
  };

  validateEditRow = (row) => {
    const errors = {};

    if (row.hasOwnProperty('name') && row.name === '') {
      errors.name = 'Обязательно для заполнения';
    }

    if (row.hasOwnProperty('amount') && (row.amount !== 0 && !row.amount)) {
      errors.amount = 'Обязательно для заполнения';
    }

    if (row.hasOwnProperty('costPerUnit') && (row.costPerUnit !== 0 && !row.costPerUnit)) {
      errors.costPerUnit = 'Обязательно для заполнения';
    }

    if (row.hasOwnProperty('discountPercent') && (row.discountPercent !== 0 && !row.discountPercent)) {
      errors.discountPercent = 'Обязательно для заполнения';
    }

    return errors;
  };

  onActionButtonClick = (id, rowIds) => (onExecute) => {
    if (!rowIds) {
      return onExecute;
    }

    const rowId = rowIds[0];
    if (id === 'commit') {
      if (rowId === 0) {
        return this.onTryRowAdd(onExecute);
      }

      return this.onTryRowSaveChanges(rowId, onExecute);
    }

    return onExecute;
  };

  commandButton = ({ id, onExecute, rowIds }) => (
    <Command
      id={id}
      onExecute={onExecute}
      onClick={this.onActionButtonClick(id, rowIds)}
    />
  );

  render() {
    const {
      classes, disabled,
    } = this.props;

    const {
      rows,
      columns,
      tableColumnExtensions,
      sorting,
      editingRowIds,
      addedRows,
      rowChanges,
      deletingRows,
      currencyColumns,
      percentColumns,
      amountColumns,
      editingColumnExtensions,
      totalSummaryItems,
      totalSummaryMessages,
    } = this.state;

    return (
      <Paper>
        <Grid
          rows={rows}
          columns={columns}
          getRowId={getRowId}
        >
          <SortingState
            sorting={sorting}
            onSortingChange={this.changeSorting}
          />

          <IntegratedSorting />

          <CurrencyTypeProvider for={currencyColumns} />
          <PercentTypeProvider for={percentColumns} />
          <FloatAmountTypeProvider for={amountColumns} />

          <EditingState
            editingRowIds={editingRowIds}
            onEditingRowIdsChange={this.changeEditingRowIds}
            rowChanges={rowChanges}
            onRowChangesChange={this.changeRowChanges}
            addedRows={addedRows}
            onAddedRowsChange={this.changeAddedRows}
            onCommitChanges={this.commitChanges}
            columnExtensions={editingColumnExtensions}
          />
          <SummaryState
            totalItems={totalSummaryItems}
          />

          <IntegratedSummary
            calculator={summaryCalculator}
          />

          <Table
            columnExtensions={tableColumnExtensions}
            cellComponent={Cell}
          />

          <TableHeaderRow
            cellComponent={HeaderCell}
            showSortingControls
          />

          <TableEditRow
            cellComponent={this.editCell}
          />

          {!disabled && (
            <TableEditColumn
              width={120}
              showAddCommand={!addedRows.length}
              showEditCommand
              showDeleteCommand
              commandComponent={this.commandButton}
            />
          )}

          <TableSummaryRow
            messages={totalSummaryMessages}
          />

          {/* Для того чтобы кнопки были справа */}
          {/*<Getter*/}
            {/*name="tableColumns"*/}
            {/*computed={({ tableColumns }) => [*/}
              {/*...tableColumns.filter(c => c.type !== 'editCommand'),*/}
              {/*{ key: 'editCommand', type: 'editCommand', width: 200 }]*/}
            {/*}*/}
          {/*/>*/}
        </Grid>

        <Dialog
          open={!!deletingRows.length}
          onClose={this.cancelDelete}
          classes={{ paper: classes.dialog }}
        >
          <DialogTitle>Удаление записи</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Вы действительно хотите удалить следующую запись?
            </DialogContentText>
            <Paper>
              <Grid
                rows={rows.filter(row => deletingRows.indexOf(row.id) > -1)}
                columns={columns}
              >
                <CurrencyTypeProvider for={currencyColumns} />
                <PercentTypeProvider for={percentColumns} />
                <Table
                  columnExtensions={tableColumnExtensions}
                  cellComponent={Cell}
                />
                <TableHeaderRow />
              </Grid>
            </Paper>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.cancelDelete} color="primary">Отмена</Button>
            <Button onClick={this.deleteRows} color="secondary">Удалить</Button>
          </DialogActions>
        </Dialog>
      </Paper>
    );
  }
}

export default withStyles(styles)(CarWorkSparePartsEditableTable);
