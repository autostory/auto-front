import find from 'lodash/find';
import React from 'react';
import TableCell from '@material-ui/core/TableCell';
import Relay from 'react-relay/classic';
import { getScheduleStatusTypeName } from '../../../../../../common-code/constants/scheduleStatusTypes';

import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import ScheduleChangeStatusButton from './ScheduleChangeStatusButton';

@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          status
          schedules {
              id
              status
              serviceWorks {
                  id
                  status
              }
          }
      }
  `,
})
class ScheduleStatus extends React.Component {
  render() {
    const { order, scheduleId, afterScheduleStatusChange } = this.props;
    const schedule = find(order.schedules, { id: scheduleId });

    if (!schedule) {
      return <TableCell />;
    }

    return (
      <TableCell>
        {getScheduleStatusTypeName(schedule.status)}
        <ScheduleChangeStatusButton
          scheduleStatus={schedule.status}
          afterScheduleStatusChange={afterScheduleStatusChange}
          order={order}
          schedule={schedule}
        />
      </TableCell>
    );
  }

}

export default ScheduleStatus;
