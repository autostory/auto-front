import * as React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';
import isEmpty from 'lodash/isEmpty';
import Input from '@material-ui/core/Input';
import TableCell from '@material-ui/core/TableCell';
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';

const styles = theme => ({
  cell: {
    paddingRight: theme.spacing.unit,
    paddingLeft: theme.spacing.unit,
  },
  inputRoot: {
    width: '100%',
  },
  inputRight: {
    textAlign: 'right',
  },
  inputCenter: {
    textAlign: 'center',
  },
});

const CustomEditCellBase = ({
                              column, value, onValueChange, style, classes, children,
                              row, tableRow, tableColumn, editingEnabled, className, errorMessage, ...restProps
                            }) => {
  const inputClasses = classNames({
    [classes.inputRight]: tableColumn && tableColumn.align === 'right',
    [classes.inputCenter]: tableColumn && tableColumn.align === 'center',
  });

  return (
    <TableCell
      className={classNames(classes.cell, className)}
      style={style}
      {...restProps}
    >
      <FormControl className={classes.formControl} error={!isEmpty(errorMessage)} fullWidth>
        {children || (
          <Input
            className={classes.inputRoot}
            classes={{ input: inputClasses }}
            value={value || ''}
            disabled={!editingEnabled}
            inputProps={{
              maxLength: 250,
            }}
            onChange={e => onValueChange(e.target.value)}
          />
        )}
        {!isEmpty(errorMessage) && <FormHelperText>{errorMessage}</FormHelperText>}
      </FormControl>

    </TableCell>
  );
};

CustomEditCellBase.propTypes = {
  column: PropTypes.object,
  row: PropTypes.any,
  tableRow: PropTypes.object,
  tableColumn: PropTypes.object,
  value: PropTypes.any,
  onValueChange: PropTypes.func.isRequired,
  style: PropTypes.object,
  classes: PropTypes.object.isRequired,
  editingEnabled: PropTypes.bool,
  children: PropTypes.node,
  className: PropTypes.string,
  errorMessage: PropTypes.string,
};

CustomEditCellBase.defaultProps = {
  column: undefined,
  row: undefined,
  tableRow: undefined,
  tableColumn: undefined,
  value: '',
  style: null,
  children: undefined,
  className: undefined,
  editingEnabled: true,
  errorMessage: null,
};

export default withStyles(styles, { name: 'CustomEditCell' })(CustomEditCellBase);
