import PropTypes from 'prop-types';
import React from 'react';
import Relay from 'react-relay/classic';
import { WAIT_CAR_ARRIVE } from '../../../../../../common-code/constants/orderStatusTypes';
import { SCHEDULE_DONE, SCHEDULE_IN_WORK, SCHEDULE_WAIT_WORK_START } from '../../../../../../common-code/constants/scheduleStatusTypes';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import SetScheduleStatusMutation from '../../../../relay/mutation/schedule/SetScheduleStatusMutation';
import OrderActionButton from '../../Orders/OrderActionButton';
import setScheduleStatus from '../../Orders/setScheduleStatus';
import MarkWorkOnScheduleAsCompletedDialog from '../MarkWorkOnScheduleAsCompletedDialog';

@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          ${SetScheduleStatusMutation.getFragment('order')}
          status
      }
  `,
  schedule: () => Relay.QL`
      fragment on BoxSchedule {
          id
          status
          serviceWorks {
              id
              status
          }
          ${SetScheduleStatusMutation.getFragment('schedule')}
      }
  `,
})
class ScheduleChangeStatusButton extends React.Component {
  static defaultProps = {
    afterScheduleStatusChange: () => {},
  };

  static propTypes = {
    afterScheduleStatusChange: PropTypes.func,
  };

  state = {
    requestPending: false,
    scheduleUsedInDialogWindow: null,
    markWorkOnScheduleAsCompletedDialogOpen: false,
  };

  onRequestEnd = () => {
    this.setState({
      requestPending: false,
    }, this.props.afterScheduleStatusChange);
  };

  onScheduleWorkStartClick = (event) => {
    this.setState({
      requestPending: true,
    }, this.sendScheduleStatusToStart);

    event.preventDefault();
    return false;
  };

  onScheduleWorkFinishClick = (event) => {
    const { schedule } = this.props;

    if (!schedule.serviceWorks.length) {
      this.setState({
        requestPending: true,
      }, this.sendScheduleStatusToFinish);
    } else {
      this.openMarkWorkOnScheduleAsCompletedDialog();
    }

    event.preventDefault();
    return false;
  };

  sendScheduleStatusToStart = () => {
    const { order, schedule } = this.props;

    setScheduleStatus(schedule, order, { status: SCHEDULE_IN_WORK })
      .then(this.onRequestEnd, this.onRequestEnd);
  };

  sendScheduleStatusToFinish = () => {
    const { order, schedule } = this.props;

    setScheduleStatus(schedule, order, { status: SCHEDULE_DONE })
      .then(this.onRequestEnd, this.onRequestEnd);
  };

  openMarkWorkOnScheduleAsCompletedDialog = () => {
    const { schedule } = this.props;
    const scheduleId = schedule.id;

    this.setState({
      scheduleUsedInDialogWindow: scheduleId,
      markWorkOnScheduleAsCompletedDialogOpen: true,
    });
  };

  closeMarkWorkOnScheduleAsCompletedDialog = () => {
    this.setState({
      scheduleUsedInDialogWindow: null,
      markWorkOnScheduleAsCompletedDialogOpen: false,
    }, () => {
      this.props.afterScheduleStatusChange();
      this.props.relay.forceFetch();
    });
  };

  render() {
    const { order, schedule, afterScheduleStatusChange } = this.props;

    const scheduleStatus = schedule.status;

    const { requestPending, scheduleUsedInDialogWindow, markWorkOnScheduleAsCompletedDialogOpen } = this.state;

    const waitCarArrive = order.status === WAIT_CAR_ARRIVE;

    if (waitCarArrive) {
      return false;
    }

    const needStartWork = scheduleStatus === SCHEDULE_WAIT_WORK_START;
    const needFinishWork = scheduleStatus === SCHEDULE_IN_WORK;

    if (!needStartWork && !needFinishWork) {
      return false;
    }

    return (
      <div>
        {needStartWork && (
          <OrderActionButton
            disabled={requestPending}
            pending={requestPending}
            alert={false}
            size="small"
            onClick={this.onScheduleWorkStartClick}
            title="Начать работу"
          />
        )}
        {needFinishWork && (
          <OrderActionButton
            disabled={requestPending}
            pending={requestPending}
            alert={false}
            size="small"
            onClick={this.onScheduleWorkFinishClick}
            title="Окончить работу"
          />
        )}
        <MarkWorkOnScheduleAsCompletedDialog
          afterScheduleStatusChange={afterScheduleStatusChange}
          scheduleId={scheduleUsedInDialogWindow}
          onRequestClose={this.closeMarkWorkOnScheduleAsCompletedDialog}
          open={markWorkOnScheduleAsCompletedDialogOpen}
        />
      </div>
    );
  }
}

export default ScheduleChangeStatusButton;
