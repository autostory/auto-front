import * as React from 'react';
import * as PropTypes from 'prop-types';
import Input from '@material-ui/core/Input';
import { withStyles } from '@material-ui/core/styles';
import { DataTypeProvider } from '@devexpress/dx-react-grid';
import isFinite from 'lodash/isFinite';
import isNaN from 'lodash/isNaN';

const styles = {
  numericInput: {
    textAlign: 'right',
    width: '100%',
  },
};

const normalizeCurrency = (value) => {
  if (!value) {
    return '';
  }

  const number = parseFloat(value);

  if (isNaN(number)) {
    return '';
  }

  const abs = Math.abs(number);

  if (isNaN(abs)) {
    return '';
  }

  return abs;
};

const normalizeFloatCurrency = (value) => {
  const normalizeValue = normalizeCurrency(value);

  if (value && value[value.length - 1] === '.' && value.indexOf('.') === value.length - 1) {
    return `${normalizeValue}.`;
  }

  return normalizeValue;
};

const getInputValue = value => (value === undefined ? '' : value);

const EditorBase = ({ value, onValueChange, classes }) => {
  const handleChange = (event) => {
    const { value: targetValue } = event.target;
    if (targetValue.trim() === '') {
      onValueChange();
      return;
    }
    onValueChange(normalizeFloatCurrency(targetValue));
  };
  return (
    <Input
      classes={{
        input: classes.numericInput,
      }}
      fullWidth
      value={getInputValue(value)}
      inputProps={{
        min: 0,
        placeholder: 'руб.',
        maxLength: 10,
      }}
      onChange={handleChange}
    />
  );
};

EditorBase.propTypes = {
  value: PropTypes.number,
  onValueChange: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

EditorBase.defaultProps = {
  value: undefined,
};

const Editor = withStyles(styles)(EditorBase);

const Formatter = ({ value }) => {
  const currency = parseFloat(value);
  if (!value || !isFinite(currency)) {
    return '0 руб.';
  }

  return `${currency.toFixed(2)} руб.`;
};

const availableFilterOperations = [
  'equal', 'notEqual',
  'greaterThan', 'greaterThanOrEqual',
  'lessThan', 'lessThanOrEqual',
];

const CurrencyTypeProvider = props => (
  <DataTypeProvider
    formatterComponent={Formatter}
    editorComponent={Editor}
    availableFilterOperations={availableFilterOperations}
    {...props}
  />
);

export default CurrencyTypeProvider;
