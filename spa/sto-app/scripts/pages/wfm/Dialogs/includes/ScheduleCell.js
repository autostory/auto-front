import React from 'react';
import map from 'lodash/map';
import get from 'lodash/get';
import find from 'lodash/find';
import isEmpty from 'lodash/isEmpty';
import format from 'date-fns/esm/format';
import TableCell from '@material-ui/core/TableCell';
import { Table } from '@devexpress/dx-react-grid-material-ui';
import { connect } from 'react-redux';
import Relay from 'react-relay/classic';
import { getScheduleStatusTypeName } from '../../../../../../common-code/constants/scheduleStatusTypes';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import { isRelayId } from '../../../../../../common-code/relay/globalIdUtils';
import ScheduleStatus from './ScheduleStatus';

@relayContainer({
  office: () => Relay.QL`
      fragment on Office {
          boxes(active: true) {
              id
              title
          }
      }
  `,
  order: () => Relay.QL`
      fragment on Order {
          ${ScheduleStatus.getFragment('order')}
          serviceWorks {
              id
              shortDescription
          }
      }
  `,
})
@connect(state => ({
  sparePartNodes: state.carWorkEditableTableStore.sparePartNodes,
}))
class ScheduleCell extends React.PureComponent {
  getTempServiceWorkShortDescription = (serviceWork) => {
    const { sparePartNodes } = this.props;

    const totalCost = (parseFloat(serviceWork.costPerUnit) * parseFloat(serviceWork.amount))
      * ((100 - parseFloat(serviceWork.discountPercent)) / 100);

    const sparePart = find(sparePartNodes, { id: serviceWork.sparePartNodeId });

    const parts = [
      get(sparePart, 'name', ''),
      get(serviceWork, 'name', ''),
      `${totalCost} р.`,
    ];

    return parts.join(' ');
  };

  render() {
    const {
      dispatch, office, order, value, serviceWorks: tempServiceWorks, sparePartNodes, afterScheduleStatusChange, ...rest
    } = this.props;

    const boxes = get(office, 'boxes', []);

    if (rest.column.name === 'boxId') {
      let formattedValue = 'не указано';
      if (!isEmpty(value)) {
        const box = find(boxes, { id: value });
        formattedValue = box.title;
      }

      return <Table.Cell {...rest} value={formattedValue} />;
    }

    if (rest.column.name === 'start') {
      let formattedValue = 'не указано';
      if (value) {
        const date = new Date();
        date.setTime(value);
        formattedValue = format(date, 'DD.MM.YY HH:mm');
      }

      return <Table.Cell {...rest} value={formattedValue} />;
    }

    if (rest.column.name === 'finish') {
      let formattedValue = 'не указано';
      if (value) {
        const date = new Date();
        date.setTime(value);
        formattedValue = format(value, 'DD.MM.YY HH:mm');
      }

      return <Table.Cell {...rest} value={formattedValue} />;
    }

    if (this.props.column.name === 'status') {
      const scheduleId = get(rest, 'row.id', null);

      if (!isRelayId(scheduleId)) {
        return <TableCell />;
      }

      if (this.props.disabled) {
        return (
          <TableCell>
            {getScheduleStatusTypeName(value)}
          </TableCell>
        );
      }

      return (
        <ScheduleStatus scheduleId={scheduleId} order={order} afterScheduleStatusChange={afterScheduleStatusChange} />
      );
    }

    if (rest.column.name === 'serviceWorks') {
      const persistedServiceWorks = get(this.props, 'order.serviceWorks', null);
      const serviceWorks = persistedServiceWorks || tempServiceWorks;

      let formattedValue = 'не указаны';
      const selectedServiceWorks = map(serviceWorks, (serviceWork) => {
        if (value.indexOf(serviceWork.id) > -1) {
          return {
            id: serviceWork.id,
            title: serviceWork.shortDescription || this.getTempServiceWorkShortDescription(serviceWork),
          };
        }

        return false;
      });

      if (value.length) {
        formattedValue = map(selectedServiceWorks, item => <div key={item.id}>{item.title}</div>);
      }

      return <Table.Cell {...rest} value={formattedValue} />;
    }

    return <Table.Cell {...rest} value={value} />;
  }
}

export default ScheduleCell;
