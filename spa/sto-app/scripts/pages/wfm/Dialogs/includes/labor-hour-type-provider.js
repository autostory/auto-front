import * as React from 'react';
import * as PropTypes from 'prop-types';
import Input from '@material-ui/core/Input';
import { withStyles } from '@material-ui/core/styles';
import { DataTypeProvider } from '@devexpress/dx-react-grid';
import isNaN from 'lodash/isNaN';
import isFinite from 'lodash/isFinite';

const styles = {
  numericInput: {
    textAlign: 'right',
    width: '100%',
  },
};

const normalizeLaborHour = (value) => {
  if (!value) {
    return '';
  }

  const number = parseFloat(value);

  if (isNaN(number)) {
    return '';
  }

  const abs = Math.abs(number);

  if (isNaN(abs)) {
    return '';
  }

  return abs;
};
const normalizeFloatLaborHour = (value) => {
  const normalizeValue = normalizeLaborHour(value);

  if (value && value[value.length - 1] === '.' && value.indexOf('.') === value.length - 1) {
    return `${normalizeValue}.`;
  }

  return normalizeValue;
};
const getInputValue = value => (value === undefined ? '' : value);

const EditorBase = ({ value, onValueChange, classes }) => {
  const handleChange = (event) => {
    const { value: targetValue } = event.target;
    if (targetValue.trim() === '') {
      onValueChange();

      return;
    }
    onValueChange(normalizeFloatLaborHour(targetValue));
  };

  return (
    <Input
      classes={{
        input: classes.numericInput,
      }}
      fullWidth
      value={getInputValue(value)}
      inputProps={{
        min: 0,
        maxLength: 4,
        placeholder: 'ч',
      }}
      onChange={handleChange}
    />
  );
};

EditorBase.propTypes = {
  value: PropTypes.number,
  onValueChange: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

EditorBase.defaultProps = {
  value: undefined,
};

const Editor = withStyles(styles)(EditorBase);

const Formatter = ({ value }) => {
  const laborHour = parseFloat(value);
  if (!value || !isFinite(laborHour)) {

    return '0 ч.';
  }

  return `${laborHour} ч.`;
};

const LaborHourTypeProvider = props => (
  <DataTypeProvider
    formatterComponent={Formatter}
    editorComponent={Editor}
    {...props}
  />
);

export default LaborHourTypeProvider;
