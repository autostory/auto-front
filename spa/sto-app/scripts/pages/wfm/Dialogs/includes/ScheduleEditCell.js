import { withStyles } from '@material-ui/core/styles/index';
import find from 'lodash/find';
import now from 'lodash/now';
import get from 'lodash/get';
import isUndefined from 'lodash/isUndefined';
import React from 'react';
import map from 'lodash/map';
import TableCell from '@material-ui/core/TableCell';
import Input from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { TableEditRow } from '@devexpress/dx-react-grid-material-ui';
import DateTimePicker from 'material-ui-pickers/DateTimePicker';
import { connect } from 'react-redux';
import Relay from 'react-relay/classic';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import { getScheduleStatusTypeName } from '../../../../../../common-code/constants/scheduleStatusTypes';
import { isRelayId } from '../../../../../../common-code/relay/globalIdUtils';

import CustomEditCell from './CustomEditCell';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import ScheduleStatus from './ScheduleStatus';

const styles = theme => ({
  lookupEditCell: {
    paddingTop: theme.spacing.unit * 0.875,
    paddingRight: theme.spacing.unit,
    paddingLeft: theme.spacing.unit,
  },
  menuItem: {
    maxWidth: 300,
    display: 'block',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  inputRoot: {
    width: '100%',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
    maxWidth: 300,
  },
});

const LookupEditCellBase = ({
                              availableColumnValues, value, onValueChange, classes,
                            }) => (
  <TableCell
    className={classes.lookupEditCell}
  >
    <Select
      value={value}
      onChange={event => onValueChange(event.target.value)}
      input={
        <Input classes={{ root: classes.inputRoot }} />
      }
    >
      {availableColumnValues.length !== 0 && availableColumnValues.map(item => (
        <MenuItem className={classes.menuItem} key={item.id} value={item.id}>{item.title}</MenuItem>
      ))}
    </Select>
  </TableCell>
);

const DateInput = ({
                     errorMessage, helperText, error, ...rest
                   }) => {
  let comboHelperText = helperText;
  if (errorMessage) {
    comboHelperText = errorMessage;
  }

  let comboError = error;
  if (errorMessage) {
    comboError = true;
  }

  return (
    <TextField
      helperText={comboHelperText}
      error={!!comboError}
      {...rest}
    />
  );
};

const CustomTextInput = ({ newRowErrors, editedRowsErrors, ...rest }) => {
  let errorMessage = '';
  if (isUndefined(rest.row.id)) {
    errorMessage = !isUndefined(newRowErrors)
      && !isUndefined(newRowErrors[rest.column.name])
      && newRowErrors[rest.column.name];
  } else {
    errorMessage = !isUndefined(editedRowsErrors) && !isUndefined(editedRowsErrors[rest.row.id])
      && !isUndefined(editedRowsErrors[rest.row.id][rest.column.name])
      && editedRowsErrors[rest.row.id][rest.column.name];
  }

  return <CustomEditCell errorMessage={errorMessage || ''} {...rest} />;
};

const LookupEditCell = withStyles(styles)(LookupEditCellBase);

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: (ITEM_HEIGHT * 4.5) + ITEM_PADDING_TOP,
    },
  },
};

@relayContainer({
  office: () => Relay.QL`
      fragment on Office {
          boxes(active: true) {
              id
              title
          }
      }
  `,
  order: () => Relay.QL`
      fragment on Order {
          ${ScheduleStatus.getFragment('order')}
          serviceWorks(statuses: [ SERVICE_WORK_IN_WORK, SERVICE_WORK_WAIT_WORK_START ]) {
              id
              shortDescription
          }
      }
  `,
})
@connect(state => ({
  sparePartNodes: state.carWorkEditableTableStore.sparePartNodes,
}))
@withStyles(styles)
class ScheduleEditCell extends React.PureComponent {
  onDateTimeChange = (time) => {
    const { onValueChange } = this.props;

    if (time instanceof Date) {
      time.setSeconds(0);
      time.setMilliseconds(0);
      onValueChange(time.getTime());
    } else {
      onValueChange(time);
    }
  };

  onServiceWorkChange = (event) => {
    const { onValueChange } = this.props;
    onValueChange(event.target.value);
  };

  getTempServiceWorkShortDescription = (serviceWork) => {
    const { sparePartNodes } = this.props;

    const totalCost = (parseFloat(serviceWork.costPerUnit) * parseFloat(serviceWork.amount))
      * ((100 - parseFloat(serviceWork.discountPercent)) / 100);

    const sparePart = find(sparePartNodes, { id: serviceWork.sparePartNodeId });

    const parts = [
      get(sparePart, 'name', ''),
      get(serviceWork, 'name', ''),
      `${totalCost} р.`,
    ];

    return parts.join(' ');
  };

  renderServiceWorksValue = (value) => {
    if (value.length) {
      return `Выбрано ${value.length}`;
    }

    return 'не указано';
  };

  render() {
    const { classes } = this.props;
    const {
      office, relay, value, onValueChange, newRowErrors, editedRowsErrors,
      serviceWorks: tempServiceWorks, order, viewer, rowChanges,
      afterScheduleStatusChange, serviceWorkActionsTypes, sparePartNodes, ...rest
    } = this.props;

    const boxes = get(office, 'boxes', []);

    const { id } = this.props.row;
    const { name } = this.props.column;

    let errorMessage = '';

    if (id && !isUndefined(editedRowsErrors[id]) && !isUndefined(editedRowsErrors[id][name])) {
      errorMessage = editedRowsErrors[id][name];
    }

    if (!id && !isUndefined(newRowErrors[name])) {
      errorMessage = newRowErrors[name];
    }

    if (this.props.column.name === 'boxId') {
      const availableColumnValues = map(boxes, box => ({
        id: box.id,
        title: get(box, 'title', 'Имя бокса не указано'),
      }));

      return (
        <LookupEditCell
          value={value}
          onValueChange={onValueChange}
          availableColumnValues={availableColumnValues}
          {...rest}
        />
      );
    }

    if (this.props.column.name === 'start') {
      let minStartDateTimeStamp = value;
      if (minStartDateTimeStamp > now()) {
        minStartDateTimeStamp = now();
      }

      return (
        <TableEditRow.Cell className={classes.lookupEditCell} {...rest}>
          <DateTimePicker
            ampm={false}
            keyboard
            errorMessage={errorMessage}
            format="DD.MM.YY HH:mm"
            onChange={this.onDateTimeChange}
            minDate={new Date(minStartDateTimeStamp)}
            value={new Date(value)}
            okLabel="OK"
            cancelLabel="Закрыть"
            TextFieldComponent={DateInput}
          />
        </TableEditRow.Cell>
      );
    }

    if (this.props.column.name === 'finish') {
      let minFinishDateTimeStamp = this.props.row.start;
      const currentRowChanges = rowChanges[this.props.row.id];

      if (currentRowChanges && !isUndefined(currentRowChanges.start)) {
        minFinishDateTimeStamp = currentRowChanges.start;
      }

      return (
        <TableEditRow.Cell className={classes.lookupEditCell} {...rest}>
          <DateTimePicker
            ampm={false}
            keyboard
            minDate={new Date(minFinishDateTimeStamp)}
            format="DD.MM.YY HH:mm"
            errorMessage={errorMessage}
            onChange={this.onDateTimeChange}
            value={new Date(value)}
            okLabel="OK"
            cancelLabel="Закрыть"
            TextFieldComponent={DateInput}
          />
        </TableEditRow.Cell>
      );
    }

    if (this.props.column.name === 'status') {
      const scheduleId = get(rest, 'row.id', null);

      if (!isRelayId(scheduleId)) {
        return <TableCell />;
      }

      if (this.props.disabled) {
        return (
          <TableCell>
            {getScheduleStatusTypeName(value)}
          </TableCell>
        );
      }

      return (
        <ScheduleStatus scheduleId={scheduleId} order={order} afterScheduleStatusChange={afterScheduleStatusChange} />
      );
    }

    if (this.props.column.name === 'serviceWorks') {
      const persistedServiceWorks = get(this.props, 'order.serviceWorks', null);
      const serviceWorks = persistedServiceWorks || tempServiceWorks;

      return (
        <TableEditRow.Cell {...rest}>
          <FormControl className={classes.formControl} error={!!errorMessage}>
            <Select
              multiple
              value={value}
              onChange={this.onServiceWorkChange}
              renderValue={this.renderServiceWorksValue}
              MenuProps={MenuProps}
            >
              {serviceWorks.map(serviceWork => (
                <MenuItem key={serviceWork.id} value={serviceWork.id}>
                  <Checkbox checked={value.indexOf(serviceWork.id) > -1} />
                  {serviceWork.shortDescription && <ListItemText primary={serviceWork.shortDescription} />}
                  {!serviceWork.shortDescription && (
                    <ListItemText
                      primary={this.getTempServiceWorkShortDescription(serviceWork)}
                    />
                  )}
                </MenuItem>
              ))}
            </Select>
            {errorMessage && <FormHelperText>{errorMessage}</FormHelperText>}
          </FormControl>
        </TableEditRow.Cell>
      );
    }

    return (
      <CustomTextInput
        newRowErrors={newRowErrors}
        editedRowsErrors={editedRowsErrors}
        value={value}
        onValueChange={onValueChange}
        {...rest}
      />
    );
  }
}

export default ScheduleEditCell;
