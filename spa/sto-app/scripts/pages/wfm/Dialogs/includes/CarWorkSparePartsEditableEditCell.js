import { withStyles } from '@material-ui/core/styles/index';
import React from 'react';
import map from 'lodash/map';
import isUndefined from 'lodash/isUndefined';
import TableCell from '@material-ui/core/TableCell';
import Input from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { connect } from 'react-redux';

import CustomEditCell from './CustomEditCell';

const styles = theme => ({
  lookupEditCell: {
    paddingTop: theme.spacing.unit * 0.875,
    paddingRight: theme.spacing.unit,
    paddingLeft: theme.spacing.unit,
  },
  inputRoot: {
    width: '100%',
  },
});

const LookupEditCellBase = ({ availableColumnValues, value, onValueChange, classes }) => (
  <TableCell
    className={classes.lookupEditCell}
  >
    <Select
      value={value}
      onChange={event => onValueChange(event.target.value)}
      input={
        <Input
          classes={{ root: classes.inputRoot }}
        />
      }
    >
      {availableColumnValues.map(item => (
        <MenuItem key={item.id} value={item.id}>{item.title}</MenuItem>
      ))}
    </Select>
  </TableCell>
);

const CustomTextInput = ({ newRowErrors, editedRowsErrors, ...rest }) => {
  let errorMessage = '';
  if (isUndefined(rest.row.id)) {
    errorMessage = !isUndefined(newRowErrors)
      && !isUndefined(newRowErrors[rest.column.name])
      && newRowErrors[rest.column.name];
  } else {
    errorMessage = !isUndefined(editedRowsErrors) && !isUndefined(editedRowsErrors[rest.row.id])
      && !isUndefined(editedRowsErrors[rest.row.id][rest.column.name])
      && editedRowsErrors[rest.row.id][rest.column.name];
  }

  return <CustomEditCell errorMessage={errorMessage || ''} {...rest} />;
};

const LookupEditCell = withStyles(styles)(LookupEditCellBase);

@connect(state => ({
  unitTypes: state.carWorkSparePartsEditableTableStore.unitTypes,
}))
class CarWorkSparePartsEditableEditCell extends React.PureComponent {
  render() {
    const { dispatch, unitTypes, newRowErrors, editedRowsErrors, ...rest } = this.props;

    if (this.props.column.name === 'unitMeasureTypeId') {
      const availableColumnValues = map(unitTypes, unitType => ({
        id: unitType.id,
        title: unitType.name,
      }));

      return <LookupEditCell availableColumnValues={availableColumnValues} {...rest} />;
    }

    if (this.props.column.name === 'totalCost') {
      return (
        <TableCell>
          {`${rest.value} руб.`}
        </TableCell>
      );
    }

    return <CustomTextInput newRowErrors={newRowErrors} editedRowsErrors={editedRowsErrors} {...rest} />;
  }
}

export default CarWorkSparePartsEditableEditCell;
