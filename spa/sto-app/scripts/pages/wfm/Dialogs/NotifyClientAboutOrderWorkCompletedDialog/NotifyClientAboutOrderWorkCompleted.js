import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';
import { connect } from 'react-redux';
import { formValueSelector} from 'redux-form';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import NotifyClientAboutOrderWorkCompletedForm from './NotifyClientAboutOrderWorkCompletedForm';
import { FORM_NAME } from '../MarkWorkOnScheduleAsCompletedDialog/MarkWorkOnScheduleAsCompletedForm';
import submit from './submit';
import NotifyClientAboutOrderWorkCompletedMutation
  from './../../../../relay/mutation/order/NotifyClientAboutOrderWorkCompletedMutation';

const selector = formValueSelector(FORM_NAME);

const NotifyClientAboutOrderWorkCompletedFormWithValues = connect(state => ({
  sendEmailNotification: selector(state, 'sendEmailNotification'),
  sendSMSNotification: selector(state, 'sendSMSNotification'),
}))(NotifyClientAboutOrderWorkCompletedForm);

@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          id
          ${NotifyClientAboutOrderWorkCompletedForm.getFragment('order')}
          ${NotifyClientAboutOrderWorkCompletedMutation.getFragment('order')}
          status
          client {
              email
              phone
          }
      }
  `,
})
class NotifyClientAboutOrderWorkCompleted extends Component {
  static propTypes = {
    onCancel: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
  };

  onSubmit = (values) => {
    const { order } = this.props;

    return submit(order, values)
      .then(this.props.onCancel);
  };

  render() {
    const { order, onSubmit } = this.props;

    return (
      <NotifyClientAboutOrderWorkCompletedFormWithValues
        order={order}
        onCancel={this.props.onCancel}
        buttonText={this.props.buttonText}
        onSubmit={onSubmit}
        // onSubmit={this.onSubmit}
      />
    );
  }
}

export default NotifyClientAboutOrderWorkCompleted;

