import NotifyClientAboutOrderWorkCompletedMutation
  from '../../../../relay/mutation/order/NotifyClientAboutOrderWorkCompletedMutation';
import commitMutationPromise from '../../../../../../common-code/relay/commitMutationPromise';

function submit(order, values) {
  return commitMutationPromise(NotifyClientAboutOrderWorkCompletedMutation, { order, input: values });
}

export default submit;
