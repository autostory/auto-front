import filter from 'lodash/filter';
import find from 'lodash/find';
import map from 'lodash/map';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import Slide from '@material-ui/core/Slide';
import Relay from 'react-relay/classic';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';

import NotifyClientAboutOrderWorkCompleted from './NotifyClientAboutOrderWorkCompleted';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          id
          ${NotifyClientAboutOrderWorkCompleted.getFragment('order')}
      }
  `,
})
class NotifyClientAboutOrderWorkCompletedDialog extends Component {
  static defaultProps = {
    classes: {},
  };

  static propTypes = {
    classes: PropTypes.object,
    onRequestClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
  };

  onStartCreateClient = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onClose = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  render() {
    const { open, onRequestClose, order, onSubmit } = this.props;

    return (
      <Dialog
        fullScreen={false}
        TransitionComponent={Transition}
        onClose={onRequestClose}
        open={open}
        maxWidth="md"
      >
        <DialogTitle>
          Уведомить клиента о выполнении работ по заказу?
        </DialogTitle>

        <DialogContent>
          <NotifyClientAboutOrderWorkCompleted
            buttonText={this.props.buttonText}
            order={order}
            onSubmit={onSubmit}
            onCancel={this.onClose}
          />
        </DialogContent>
      </Dialog>
    );
  }
}

export default NotifyClientAboutOrderWorkCompletedDialog;
