import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles/index';
import { Prompt } from 'react-router-dom';
import Relay from 'react-relay/classic';
import get from 'lodash/get';
import { reduxForm, Field } from 'redux-form';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Checkbox } from 'redux-form-material-ui';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import phoneFormat from '../../../../../../common-code/utils/text/phoneFormat';
import { FORM_NAME } from './../MarkWorkOnScheduleAsCompletedDialog/MarkWorkOnScheduleAsCompletedForm';

const styles = {
  paper: {
    padding: 24,
  },
  statusSelect: {
    minWidth: 400,
  },
};

const rawStyle = {
  actions: {
    marginTop: 12,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cancelButton: {
    marginLeft: 12,
  },
};

@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          id
          status
          client {
              email
              phone
          }
      }
  `,
})
@withStyles(styles)
class NotifyClientAboutOrderWorkCompletedForm extends PureComponent {
  static propTypes = {
    onCancel: PropTypes.func.isRequired,
    buttonText: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    const { order, change } = props;
    const { client } = order;
    const { email } = client;

    if (email) {
      // По умолчанию ставим галку на email уведомление, это беспалатно =)
      change('sendEmailNotification', true);
    } else {
      change('sendEmailNotification', false);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { change } = this.props;

    const nextEmail = get(nextProps, 'order.client.email');
    const currentEmail = get(this.props, 'order.client.email');

    if (nextEmail !== currentEmail && !nextEmail) {
      change('sendEmailNotification', false);
    }

    const nextPhone = get(nextProps, 'order.client.phone');
    const currentPhone = get(this.props, 'order.client.phone');

    if (nextPhone !== currentPhone && !nextPhone) {
      change('sendSMSNotification', false);
    }
  }

  onCancelClick = () => {
    this.props.onCancel();
  };

  onSaveClick = () => {
    this.props.handleSubmit();
  };

  render() {
    const { order, dirty, submitting, buttonText, sendEmailNotification, sendSMSNotification } = this.props;
    const { client } = order;
    const { email, phone } = client;

    return (
      <div>
        <Prompt
          when={dirty}
          message={() =>
            'На странице есть не сохраненные изменения. Вы действительно хотите покинуть страницу?'
          }
        />
        <div>
          {email && (
            <div>
              <FormControlLabel
                control={
                  <Field
                    name="sendEmailNotification"
                    color="primary"
                    component={Checkbox}
                  />}
                label={<span>Отправить уведомление на e-mail <b>{email}</b></span>}
              />
            </div>
          )}

          {phone && (
            <div>
              <FormControlLabel
                control={
                  <Field
                    name="sendSMSNotification"
                    color="primary"
                    component={Checkbox}
                  />}
                label={<span>Отправить SMS уведомление на номер <b>{phoneFormat(phone)}</b></span>}
              />
            </div>
          )}
          <Typography variant="caption">
            Клиент получит ссылку на приложение, а также логин и пароль от приложения если до этого он его не получал.
          </Typography>
        </div>
        <div style={rawStyle.actions}>
          <Button
            style={rawStyle.cancelButton}
            variant="flat"
            color="primary"
            disabled={submitting}
            onClick={this.onCancelClick}
          >
            Отмена
          </Button>
          <Button
            variant="contained"
            color="primary"
            disabled={submitting}
            onClick={this.onSaveClick}
          >
            {(sendEmailNotification || sendSMSNotification) && <div>Отправить
              уведомлени{(!sendEmailNotification || !sendSMSNotification) && 'е'}{(sendEmailNotification && sendSMSNotification) && 'я'} и{' '}</div>}
            {buttonText}
            {submitting && <CircularProgress size={15} />}
          </Button>
        </div>
      </div>
    );
  }
}

export default reduxForm({
  form: FORM_NAME,
  // validate,
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  initialValues: {
    sendEmailNotification: false,
    sendSMSNotification: false,
  },
})(NotifyClientAboutOrderWorkCompletedForm);
