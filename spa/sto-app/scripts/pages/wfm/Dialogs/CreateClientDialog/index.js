import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import Slide from '@material-ui/core/Slide';
import { withStyles } from '@material-ui/core/styles';
import Relay from 'react-relay/classic';
import ClientCreateForm from './../../../../components/manager/form/ClientCreateForm';
import submit from './../../../../components/manager/form/submit';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';

import ConfiguredRadium from '../../../../radium/ConfiguredRadium';

const CAR_TYPE_ID = 'Q2F0YWxvZ0NhclR5cGU6MQ=='; // легковое авто

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

const styles = theme => ({});

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          ${ClientCreateForm.getFragment('viewer')},
      }
  `,
})
@ConfiguredRadium
@withStyles(styles)
class CreateClientDialog extends Component {
  static propTypes = {
    onClientCreated: PropTypes.func.isRequired,
    onRequestClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
  };

  onSuccess = ({ newClientId }) => {
    const { onRequestClose, onClientCreated } = this.props;

    onClientCreated({ newClientId });
    onRequestClose();
  };

  onClose = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onSubmit = (values) => {
    const { viewer } = this.props;

    return submit(viewer, values);
  };

  render() {
    const {
      open, onRequestClose, onClientCreated, viewer, ...rest
    } = this.props;

    return (
      <Dialog
        fullScreen={false}
        TransitionComponent={Transition}
        onClose={onRequestClose}
        open={open}
        maxWidth="md"
        {...rest}
      >
        <DialogTitle id="form-dialog-title">Добавление клиента</DialogTitle>

        <DialogContent>
          <ClientCreateForm
            viewer={viewer}
            onSubmit={this.onSubmit}
            onSuccess={this.onSuccess}
            initialValues={{
              noCarAdd: false,
              sendEmailInvite: true,
              sendSMSInvite: false,
              catalogCarTypeId: CAR_TYPE_ID,
            }}
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={this.onClose} color="primary">
            Отмена
          </Button>
        </DialogActions>

      </Dialog>
    );
  }
}

export default CreateClientDialog;
