import React, { Component } from 'react';
import withProps from 'recompose/withProps';

import Paper from '@material-ui/core/Paper';
import Relay from 'react-relay/classic';

import CircularProgress from '@material-ui/core/CircularProgress';

import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';
import OrderServiceWorksForm from '../../../manager/OrderPage/includes/OrderServiceWorksForm';
import submitServiceWorks from '../../../manager/OrderPage/includes/submitServiceWorks';

@withProps({ renderLoading: () => <CircularProgress /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          order(orderId: $orderId) {
              id
              ${OrderServiceWorksForm.getFragment('order')}
              office {
                  id
              }
          }
      }
  `,
})
class AddMaterialsToOrderForm extends Component {
  onOrderServiceWorksFormSubmit = (values) => {
    const { order } = this.props.viewer;

    return submitServiceWorks(order, values);
  };

  render() {
    const { order } = this.props.viewer;

    return (
      <Paper>
        <OrderServiceWorksForm
          order={order}
          officeId={order.office.id}
          onSubmit={this.onOrderServiceWorksFormSubmit}
        />
      </Paper>
    );
  }
}

export default AddMaterialsToOrderForm;

