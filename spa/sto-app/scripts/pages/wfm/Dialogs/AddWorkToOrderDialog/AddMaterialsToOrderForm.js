import React, { Component } from 'react';
import withProps from 'recompose/withProps';

import Paper from '@material-ui/core/Paper';
import Relay from 'react-relay/classic';

import CircularProgress from '@material-ui/core/CircularProgress';

import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';
import OrderServiceMaterialsForm from '../../../manager/OrderPage/includes/OrderServiceMaterialsForm';
import submitServiceMaterial from '../../../manager/OrderPage/includes/submitServiceMaterial';

@withProps({ renderLoading: () => <CircularProgress /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          order(orderId: $orderId) {
              id
              ${OrderServiceMaterialsForm.getFragment('order')}
              office {
                  id
              }
          }
      }
  `,
})
class AddMaterialsToOrderForm extends Component {
  onOrderServiceMaterialFormSubmit = (values) => {
    const { order } = this.props.viewer;

    return submitServiceMaterial(order, values);
  };

  render() {
    const { order } = this.props.viewer;

    return (
      <Paper>
        <OrderServiceMaterialsForm
          order={order}
          onSubmit={this.onOrderServiceMaterialFormSubmit}
        />
      </Paper>
    );
  }
}

export default AddMaterialsToOrderForm;

