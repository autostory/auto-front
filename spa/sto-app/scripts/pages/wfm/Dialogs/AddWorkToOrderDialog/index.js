import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import Slide from '@material-ui/core/Slide';

import ConfiguredRadium from '../../../../radium/ConfiguredRadium';
import AddWorkToOrderForm from './AddWorkToOrderForm';
import AddMaterialsToOrderForm from './AddMaterialsToOrderForm';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

@ConfiguredRadium
class AddWorkToOrderDialog extends Component {
  static defaultProps = {
    classes: {},
    orderId: null,
  };

  static propTypes = {
    classes: PropTypes.object,
    onRequestClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    orderId: PropTypes.string,
  };

  onStartCreateClient = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onClose = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  render() {
    const {
      open, onRequestClose, orderId, ...rest
    } = this.props;

    return (
      <Dialog
        fullScreen
        TransitionComponent={Transition}
        onClose={onRequestClose}
        open={open}
        {...rest}
      >
        <DialogTitle id="form-dialog-title">Добавление работ заказу</DialogTitle>

        <DialogContent>
          {orderId && <AddWorkToOrderForm initialVariables={{ orderId }} />}
          <br /><br />
          {orderId && <AddMaterialsToOrderForm initialVariables={{ orderId }} />}
        </DialogContent>

        <DialogActions>
          <Button onClick={this.onClose} color="primary">
            Закрыть окно
          </Button>
        </DialogActions>

      </Dialog>
    );
  }
}

export default AddWorkToOrderDialog;
