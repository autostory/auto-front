import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import isUndefined from 'lodash/isUndefined';

import Slide from '@material-ui/core/Slide';
import EditCarForm from '../../../../components/Form/Car/EditCarForm';
import submit from '../../../../components/Form/Car/EditCarForm/submit';
import EditCarDialogActions from './EditCarDialogActions';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class EditCarDialog extends PureComponent {
  static defaultProps = {
    classes: {},
  };

  static propTypes = {
    onRequestClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    carId: PropTypes.string.isRequired,
  };

  onStartCreateClient = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onClose = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onSubmit = (car) => (values) => {
    const {
      afterCarAdd, onRequestClose,
    } = this.props;

    return submit(car, values)
      .then((result) => {
        if (!isUndefined(result) && !isUndefined(result.success) && result.success) {
          onRequestClose();
        }
      });
  };

  render() {
    const {
      open, onRequestClose, carId,
    } = this.props;

    return (
      <Dialog
        fullScreen={false}
        TransitionComponent={Transition}
        onClose={onRequestClose}
        open={open}
        maxWidth="md"
      >
        <DialogTitle>Редактирование автомобиля клиента</DialogTitle>

        <DialogContent>
          <EditCarForm
            onSubmit={this.onSubmit}
            initialVariables={{ carId }}
            carId={carId}
          />
        </DialogContent>

        <EditCarDialogActions onClose={this.onClose} />

      </Dialog>
    );
  }
}

export default EditCarDialog;
