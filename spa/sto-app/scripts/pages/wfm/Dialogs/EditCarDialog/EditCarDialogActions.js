import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import DialogActions from '@material-ui/core/DialogActions';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';

import { connect } from 'react-redux';
import { isInvalid, isSubmitting, submit as ReduxFormSubmit } from 'redux-form';
import { FORM_NAME } from '../../../../components/Form/Car/EditCarForm';

@connect(state => ({
  invalid: isInvalid(FORM_NAME)(state),
  submitting: isSubmitting(FORM_NAME)(state),
}))
class EditCarDialogActions extends PureComponent {
  static propTypes = {
    onClose: PropTypes.func.isRequired,
  };

  onClose = () => {
    const { onClose } = this.props;

    onClose();
  };

  onAdd = () => {
    this.props.dispatch(ReduxFormSubmit(FORM_NAME));
  };

  render() {
    const { submitting, invalid, } = this.props;

    return (
      <DialogActions>
        <Button onClick={this.onAdd} color="primary" disabled={invalid || submitting}>
          Сохранить {submitting && <CircularProgress size={15} />}
        </Button>
        <Button onClick={this.onClose} color="primary">
          Отмена
        </Button>
      </DialogActions>
    );
  }
}

export default EditCarDialogActions;
