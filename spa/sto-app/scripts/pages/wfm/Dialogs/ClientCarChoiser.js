import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import Typography from '@material-ui/core/Typography';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import filter from 'lodash/filter';
import map from 'lodash/map';
import get from 'lodash/get';
import Relay from 'react-relay/classic';
import relayContainer from '../../../../../common-code/decorators/relayContainer';

const styles = theme => ({
  root: {
    display: 'flex',
  },
  formControl: {},
  group: {
    margin: `${theme.spacing.unit}px 0`,
  },
});

@relayContainer({
  client: () => Relay.QL`
      fragment on Client {
          cars {
              carList {
                  edges {
                      node {
                          id
                          registrationSignNumber
                          displayName
                      }
                  }
              }
          }
      }
  `,
})
class ClientCarChoiser extends React.Component {
  state = {
    value: 'foo',
  };

  handleChange = (event) => {
    const { onChange } = this.props;

    this.setState({ value: event.target.value });

    onChange(event.target.value);
  };

  render() {
    const { classes, onChange, client } = this.props;
    const carsEdges = get(client, 'cars.carList.edges', null);

    let cars = [];
    if (carsEdges !== null) {
      cars = map(carsEdges, 'node');
    }

    return (
      <div className={classes.root}>
        {cars && (
          <FormControl component="fieldset" required className={classes.formControl}>
            <FormLabel component="legend">Выберите автомобиль клиента</FormLabel>
            <RadioGroup
              className={classes.group}
              value={this.state.value}
              onChange={this.handleChange}
            >
              {
                cars.map(car => (
                  <FormControlLabel
                    key={car.id}
                    value={car.id}
                    control={<Radio />}
                    label={`${car.displayName} ${car.registrationSignNumber}`}
                  />
                ))
              }
            </RadioGroup>
          </FormControl>
        )}
        {!cars && <Typography>У клиента нет автомобилей</Typography>}
      </div>
    );
  }
}

ClientCarChoiser.propTypes = {
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default withStyles(styles)(ClientCarChoiser);
