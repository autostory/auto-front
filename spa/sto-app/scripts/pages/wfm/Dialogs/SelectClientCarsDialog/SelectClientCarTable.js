import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Relay from 'react-relay/classic';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import ClientCarChoiser from '../ClientCarChoiser';

@relayContainer({
  client: () => Relay.QL`
      fragment on Client {
          ${ClientCarChoiser.getFragment('client')}
      }
  `,
})
class SelectClientCarTable extends Component {
  static propTypes = {
    onCarSelect: PropTypes.func.isRequired,
  };

  onCarSelect = (selectedCarId) => {
    this.props.onCarSelect(selectedCarId);
  };

  render() {
    const { client } = this.props;

    return (
      <ClientCarChoiser
        client={client}
        onChange={this.onCarSelect}
      />
    );
  }
}

export default SelectClientCarTable;

