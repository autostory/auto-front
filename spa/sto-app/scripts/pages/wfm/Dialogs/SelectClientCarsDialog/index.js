import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import Slide from '@material-ui/core/Slide';
import { withStyles } from '@material-ui/core/styles';
import Relay from 'react-relay/classic';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';

import ConfiguredRadium from '../../../../radium/ConfiguredRadium';
import AddCarToClientDialog from '../AddCarToClientDialog';
import SelectClientCarTable from './SelectClientCarTable';

const Transition = props => <Slide direction="up" {...props} />;

const styles = theme => ({});

@relayContainer({
  client: () => Relay.QL`
      fragment on Client {
          id
          ${SelectClientCarTable.getFragment('client')}
      }
  `,
})
@ConfiguredRadium
@withStyles(styles)
class SelectClientCarsDialog extends Component {
  static defaultProps = {
    classes: {},
    showCarAddDialog: false,
  };

  static propTypes = {
    classes: PropTypes.object,
    onRequestClose: PropTypes.func.isRequired,
    afterCarAdd: PropTypes.func.isRequired,
    onCarSelect: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    showCarAddDialog: PropTypes.bool,
  };

  state = {
    showCarAddDialog: false,
  };

  onClose = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onCarSelect = (selectedCarId) => {
    const { onRequestClose, onCarSelect } = this.props;

    onCarSelect(selectedCarId);
    onRequestClose();
  };

  showCarAddDialog = () => {
    this.setState({
      showCarAddDialog: true,
    });
  };

  hideCarAddDialog = () => {
    const { onRequestClose } = this.props;

    this.setState({
      showCarAddDialog: false,
    });

    onRequestClose();
  };

  afterCarAdd = () => {
    const { afterCarAdd, relay } = this.props;

    relay.forceFetch();
    afterCarAdd();
  };

  render() {
    const {
      open,
      onRequestClose,
      showCarAddDialog: propsShowCarAddDialog,
      client
    } = this.props;

    const { showCarAddDialog } = this.state;

    return (
      <Dialog
        fullScreen={false}
        TransitionComponent={Transition}
        onClose={onRequestClose}
        open={open}
        maxWidth="md"
      >
        <DialogTitle id="form-dialog-title">Выбор автомобиля клиента</DialogTitle>

        <DialogContent>
          <SelectClientCarTable
            client={client}
            onCarSelect={this.onCarSelect}
          />
          {client && (
            <AddCarToClientDialog
              clientId={client.id}
              afterCarAdd={this.afterCarAdd}
              open={showCarAddDialog || propsShowCarAddDialog}
              onRequestClose={this.hideCarAddDialog}
            />
          )}
          <Button variant="raised" color="primary" onClick={this.showCarAddDialog}>
            Добавить автомобиль клиенту
          </Button>
        </DialogContent>

        <DialogActions>
          <Button onClick={this.onClose} color="primary">
            Отмена
          </Button>
        </DialogActions>

      </Dialog>
    );
  }
}

export default SelectClientCarsDialog;
