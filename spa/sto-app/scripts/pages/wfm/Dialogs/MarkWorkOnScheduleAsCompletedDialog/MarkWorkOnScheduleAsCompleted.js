import React, { Component } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import map from 'lodash/map';
import withProps from 'recompose/withProps';
import Relay from 'react-relay/classic';
import CircularProgress from '@material-ui/core/CircularProgress';
import { formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import {
  SERVICE_WORK_DONE,
  SERVICE_WORK_WAIT_WORK_START,
} from '../../../../../../common-code/constants/serviceWorkStatusTypes';

import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';
import MarkWorkOnScheduleAsCompletedForm, { FORM_NAME } from './MarkWorkOnScheduleAsCompletedForm';
import submitScheduleServiceWorksStatus from './submitScheduleServiceWorksStatus';
import SetScheduleServiceWorksStatusMutation
  from './../../../../relay/mutation/schedule/SetScheduleServiceWorksStatusMutation';

const selector = formValueSelector(FORM_NAME);

const MarkWorkOnScheduleAsCompletedFormWithValues = connect(state => ({
  serviceWorkIds: selector(state, 'serviceWorkIds'),
  sendEmailNotification: selector(state, 'sendEmailNotification'),
  sendSMSNotification: selector(state, 'sendSMSNotification'),
}))(MarkWorkOnScheduleAsCompletedForm);

@withProps({ renderLoading: () => <CircularProgress /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          schedule(scheduleId: $scheduleId) {
              id
              serviceWorks {
                  id
                  status
              }
              ${MarkWorkOnScheduleAsCompletedForm.getFragment('schedule')}
              ${SetScheduleServiceWorksStatusMutation.getFragment('schedule')}
              order {
                  status
                  ${SetScheduleServiceWorksStatusMutation.getFragment('order')}
              }
          }
      }
  `,
})
class MarkWorkOnScheduleAsCompleted extends Component {
  static propTypes = {
    onCancel: PropTypes.func.isRequired,
    afterScheduleStatusChange: PropTypes.func.isRequired,
  };

  onScheduleServiceWorkMarkSubmit = (values) => {
    const { schedule } = this.props.viewer;

    let serviceWorksInput;

    if (values.serviceWorkIds.length > 0) {
      serviceWorksInput = map(schedule.serviceWorks, (i) => {
        if (values.serviceWorkIds.indexOf(i.id) !== -1) {
          return {
            id: i.id,
            status: SERVICE_WORK_DONE,
          };
        }

        return {
          id: i.id,
          status: i.status,
        };
      });
    } else {
      serviceWorksInput = map(schedule.serviceWorks, i => ({
        id: i.id,
        status: SERVICE_WORK_WAIT_WORK_START,
      }));
    }

    const sendSMSNotification = get(values, 'sendSMSNotification', false);
    const sendEmailNotification = get(values, 'sendEmailNotification', false);

    return submitScheduleServiceWorksStatus(
      schedule,
      schedule.order,
      serviceWorksInput,
      sendSMSNotification,
      sendEmailNotification,
    )
      .then(() => {
        this.props.afterScheduleStatusChange();
        this.props.onCancel();
      });
  };

  render() {
    const { schedule } = this.props.viewer;

    return (
      <div>
        <MarkWorkOnScheduleAsCompletedFormWithValues
          schedule={schedule}
          buttonText={this.props.buttonText}
          onCancel={this.props.onCancel}
          onSubmit={this.onScheduleServiceWorkMarkSubmit}
        />
      </div>
    );
  }
}

export default MarkWorkOnScheduleAsCompleted;

