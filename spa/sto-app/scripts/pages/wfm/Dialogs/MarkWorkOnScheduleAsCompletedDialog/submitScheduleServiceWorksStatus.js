import { SCHEDULE_DONE } from '../../../../../../common-code/constants/scheduleStatusTypes';
import SetScheduleServiceWorksStatusMutation
  from '../../../../relay/mutation/schedule/SetScheduleServiceWorksStatusMutation';
import commitMutationPromise from '../../../../../../common-code/relay/commitMutationPromise';

function submitScheduleServiceWorksStatus(schedule, order, serviceWorksInput, sendSMSNotification = false, sendEmailNotification = false) {
  const input = {
    scheduleStatus: SCHEDULE_DONE,
    serviceWorksInput,
    sendSMSNotification,
    sendEmailNotification,
  };

  return commitMutationPromise(SetScheduleServiceWorksStatusMutation, { schedule, order, input });
}

export default submitScheduleServiceWorksStatus;
