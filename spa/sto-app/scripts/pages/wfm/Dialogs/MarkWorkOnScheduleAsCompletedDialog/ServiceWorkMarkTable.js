import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import Tooltip from '@material-ui/core/Tooltip';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import Relay from 'react-relay/classic';
import map from 'lodash/map';
import isEqual from 'lodash/isEqual';
import get from 'lodash/get';
import filter from 'lodash/filter';
import { SCHEDULE_DONE } from '../../../../../../common-code/constants/scheduleStatusTypes';

import relayContainer from '../../../../../../common-code/decorators/relayContainer';

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => b[orderBy] - a[orderBy] : (a, b) => a[orderBy] - b[orderBy];
}

const rows = [
  {
    id: 'action', numeric: false, disablePadding: true, label: 'действие',
  },
  {
    id: 'node', numeric: false, disablePadding: true, label: 'узел',
  },
  {
    id: 'note', numeric: false, disablePadding: true, label: 'описание',
  },
  {
    id: 'amount', numeric: false, disablePadding: true, label: 'кол-во',
  },
  {
    id: 'laborHour', numeric: false, disablePadding: true, label: 'н.ч.',
  },
  {
    id: 'totalCost', numeric: false, disablePadding: true, label: 'стоимость позиции',
  },
  {
    id: 'master', numeric: false, disablePadding: true, label: 'мастер',
  },
];

class EnhancedTableHead extends React.Component {
  createSortHandler = property => (event) => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const {
      onSelectAllClick, order, orderBy, numSelected, rowCount,
    } = this.props;

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>
          {rows.map(row => (
            <TableCell
              key={row.id}
              numeric={row.numeric}
              padding={row.disablePadding ? 'none' : 'default'}
              sortDirection={orderBy === row.id ? order : false}
            >
              <Tooltip
                title="Sort"
                placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                enterDelay={300}
              >
                <TableSortLabel
                  active={orderBy === row.id}
                  direction={order}
                  onClick={this.createSortHandler(row.id)}
                >
                  {row.label}
                </TableSortLabel>
              </Tooltip>
            </TableCell>
          ), this)}
        </TableRow>
      </TableHead>
    );
  }
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
});

let EnhancedTableToolbar = (props) => {
  const { numSelected, classes } = props;

  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 && (
          <Typography color="inherit" variant="subheading">
            {numSelected} выбрано
          </Typography>
        )}
      </div>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object,
  numSelected: PropTypes.number.isRequired,
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const parseTableDataFromRelayData = serviceWorks => map(serviceWorks, serviceWork => ({
  id: serviceWork.id,
  action: get(serviceWork, 'action.name', 'не указано'),
  node: get(serviceWork, 'sparePartNode.name', 'не указано'),
  note: get(serviceWork, 'note', ''),
  amount: get(serviceWork, 'amount', 0),
  laborHour: `${get(serviceWork, 'laborHour', 0)} ч.`,
  master: get(serviceWork, 'master.fullName', get(serviceWork, 'master.id', 'не указан')),
  totalCost: `${get(serviceWork, 'totalCost', 0)} р.`,
  isCompleted: serviceWork.status === SCHEDULE_DONE,
}));

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 1020,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
});

@relayContainer({
  serviceWorks: () => Relay.QL`
      fragment on ServiceWork @relay(plural: true) {
          id
          status
          sparePartNode {
              name
          }
          amount
          totalCost
          action {
              name
          }
          discountPercent
          note
          laborHour
          master {
              id
              fullName
          }
      }
  `,
})
class ServiceWorkMarkTable extends React.Component {
  constructor(props) {
    super(props);

    const { serviceWorks } = props;

    const data = parseTableDataFromRelayData(serviceWorks);

    this.state = {
      order: 'desc',
      orderBy: 'totalCost',
      selected: map(filter(data, item => item.isCompleted), item => item.id),
      data,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (!isEqual(prevState.selected, this.state.selected)) {
      this.props.onMarksChange(this.state.selected);
    }
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState(state => ({ selected: state.data.map(n => n.id) }));
      return;
    }

    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { classes } = this.props;
    const {
      data, order, orderBy, selected,
    } = this.state;

    return (
      <Paper className={classes.root}>
        <EnhancedTableToolbar numSelected={selected.length} />
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
            />
            <TableBody>
              {data
                .sort(getSorting(order, orderBy))
                .map((n) => {
                  const isSelected = this.isSelected(n.id);
                  return (
                    <TableRow
                      hover
                      onClick={event => this.handleClick(event, n.id)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={n.id}
                      selected={isSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox checked={isSelected} />
                      </TableCell>

                      <TableCell padding="dense">{n.action}</TableCell>
                      <TableCell padding="dense">{n.node}</TableCell>
                      <TableCell padding="dense">{n.note}</TableCell>
                      <TableCell padding="dense">{n.amount}</TableCell>
                      <TableCell padding="dense">{n.laborHour}</TableCell>
                      <TableCell padding="dense">{n.totalCost}</TableCell>
                      <TableCell padding="dense">{n.master}</TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </div>
      </Paper>
    );
  }
}

ServiceWorkMarkTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

EnhancedTableToolbar.defaultProps = {
  classes: {},
};

export default withStyles(styles)(ServiceWorkMarkTable);
