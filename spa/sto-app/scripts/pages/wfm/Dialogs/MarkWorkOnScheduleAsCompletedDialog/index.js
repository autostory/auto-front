import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import Slide from '@material-ui/core/Slide';

import ConfiguredRadium from '../../../../radium/ConfiguredRadium';
import MarkWorkOnScheduleAsCompleted from './MarkWorkOnScheduleAsCompleted';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

@ConfiguredRadium
class MarkWorkOnScheduleAsCompletedDialog extends Component {
  static defaultProps = {
    classes: {},
    scheduleId: null,
  };

  static propTypes = {
    classes: PropTypes.object,
    onRequestClose: PropTypes.func.isRequired,
    afterScheduleStatusChange: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    scheduleId: PropTypes.string,
  };

  onStartCreateClient = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onClose = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  render() {
    const {
      open, onRequestClose, scheduleId, afterScheduleStatusChange, ...rest
    } = this.props;

    return (
      <Dialog
        fullScreen={false}
        TransitionComponent={Transition}
        onClose={onRequestClose}
        open={open}
        maxWidth="md"
        {...rest}
      >
        <DialogTitle id="form-dialog-title">
          Отметьте работы которые были выполнены в этом расписании
        </DialogTitle>

        <DialogContent>
          {scheduleId && (
            <MarkWorkOnScheduleAsCompleted
              afterScheduleStatusChange={afterScheduleStatusChange}
              initialVariables={{ scheduleId }}
              onCancel={this.onClose}
            />
          )}
        </DialogContent>

      </Dialog>
    );
  }
}

export default MarkWorkOnScheduleAsCompletedDialog;
