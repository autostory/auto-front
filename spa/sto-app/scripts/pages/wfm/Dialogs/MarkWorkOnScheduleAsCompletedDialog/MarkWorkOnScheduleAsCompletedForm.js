import PropTypes from 'prop-types';
import filter from 'lodash/filter';
import map from 'lodash/map';
import React, { PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles/index';
import { Prompt } from 'react-router-dom';
import Relay from 'react-relay/classic';
import { reduxForm, Field } from 'redux-form';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Checkbox } from 'redux-form-material-ui';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import { SCHEDULE_DONE } from '../../../../../../common-code/constants/scheduleStatusTypes';
import { SERVICE_WORK_DONE } from '../../../../../../common-code/constants/serviceWorkStatusTypes';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import phoneFormat from '../../../../../../common-code/utils/text/phoneFormat';
import ServiceWorkMarkTable from './ServiceWorkMarkTable';

export const FORM_NAME = 'MarkWorkOnScheduleAsCompletedForm';

const styles = {
  notifyRoot: {
    padding: '5px 27px',
  },
};

const rawStyle = {
  actions: {
    marginTop: 12,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cancelButton: {
    marginLeft: 12,
  },
};

@relayContainer({
  schedule: () => Relay.QL`
      fragment on BoxSchedule {
          id
          serviceWorks {
              id
              status
              ${ServiceWorkMarkTable.getFragment('serviceWorks')}
          }
          order {
              id
              client {
                  email
                  phone
              }
              serviceWorks {
                  id
                  status
                  ${ServiceWorkMarkTable.getFragment('serviceWorks')}
              }
          }
      }
  `,
})
@withStyles(styles)
class MarkWorkOnScheduleAsCompletedForm extends PureComponent {
  static propTypes = {
    onCancel: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    const initialIds = map(filter(props.serviceWorks, item => item.status === SCHEDULE_DONE), item => item.id);
    this.props.initialize({ serviceWorkIds: initialIds });
  }

  onMarksChange = (selectedServiceWorkIds) => {
    const { change } = this.props;

    change('serviceWorkIds', selectedServiceWorkIds);
  };

  onCancelClick = () => {
    this.props.onCancel();
  };

  onSaveClick = () => {
    this.props.handleSubmit();
  };

  isWillCompleteAllServiceWork = () => {
    const { schedule, serviceWorkIds } = this.props;

    if (!serviceWorkIds) {
      return false;
    }

    const allServiceWorks = schedule.order.serviceWorks;

    const serviceWorksInput = map(allServiceWorks, (i) => {
      if (serviceWorkIds.indexOf(i.id) !== -1) {
        return {
          id: i.id,
          status: SERVICE_WORK_DONE,
        };
      }

      return {
        id: i.id,
        status: i.status,
      };
    });

    const notCompletedServiceWork = filter(serviceWorksInput, sw => sw.status !== SERVICE_WORK_DONE);

    return notCompletedServiceWork.length === 0;
  };

  render() {
    const {
      schedule, dirty, submitting, serviceWorkIds, classes,
    } = this.props;
    const { order } = schedule;
    const { client } = order;
    const { serviceWorks } = schedule;
    const { email, phone } = client;

    const isWillCompleteAllServiceWork = this.isWillCompleteAllServiceWork();

    const buttonText = (serviceWorkIds && serviceWorkIds.length > 0) ? 'Сохранить отмеченные работы как выполненые и закрыть расписание' : 'Просто закрыть расписание';

    return (
      <div>
        <Prompt
          when={dirty}
          message={() =>
            'На странице есть не сохраненные изменения. Вы действительно хотите покинуть страницу?'
          }
        />
        Отметьте выполненные работы
        <ServiceWorkMarkTable
          onMarksChange={this.onMarksChange}
          disabled={submitting}
          serviceWorks={serviceWorks}
        />
        {isWillCompleteAllServiceWork && (
          <div className={classes.notifyRoot}>
            <Typography variant="subheading">
              После сохраения изменений все работы по заказу будут иметь статус выполненных.<br />
              Уведомить клиента о выполнении работ по заказу?
            </Typography>
            {email && (
              <div>
                <FormControlLabel
                  control={
                    <Field
                      name="sendEmailNotification"
                      color="primary"
                      component={Checkbox}
                    />}
                  label={<span>Отправить уведомление на e-mail <b>{email}</b></span>}
                />
              </div>
            )}
            {phone && (
              <div>
                <FormControlLabel
                  control={
                    <Field
                      name="sendSMSNotification"
                      color="primary"
                      component={Checkbox}
                    />}
                  label={<span>Отправить SMS уведомление на номер <b>{phoneFormat(phone)}</b></span>}
                />
              </div>
            )}
            <Typography variant="caption">
              Клиент получит ссылку на приложение, а также логин и пароль от приложения если до этого он его не получал.
            </Typography>
          </div>
        )}
        <div style={rawStyle.actions}>
          <Button
            style={rawStyle.cancelButton}
            variant="flat"
            color="primary"
            disabled={submitting}
            onClick={this.onCancelClick}
          >
            Отмена
          </Button>
          <Button
            variant="contained"
            color="primary"
            disabled={submitting}
            onClick={this.onSaveClick}
          >
            {buttonText} {submitting && <CircularProgress size={15} />}
          </Button>
        </div>
      </div>
    );
  }
}

export default reduxForm({
  form: FORM_NAME,
  initialValues: {
    serviceWorkIds: [],
    sendEmailNotification: false,
    sendSMSNotification: false,
  },
})(MarkWorkOnScheduleAsCompletedForm);
