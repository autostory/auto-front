import React from 'react';
import Relay from 'react-relay/classic';
import PropTypes from 'prop-types';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import CircularProgress from '@material-ui/core/CircularProgress';
import debounce from 'lodash/debounce';
import get from 'lodash/get';
import map from 'lodash/map';
import withProps from 'recompose/withProps';

import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';

import CarSearchTable from './CarSearchTable';

@withProps({ renderLoading: () => <CircularProgress /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          myFirm {
              id
              name
              clientCars(
                  registrationSignNumber: $keyword
                  vin: $keyword
                  first: 100
              ) @include(if: $notEmptyKeyword) {
                  edges {
                      node {
                          ${CarSearchTable.getFragment('cars')},
                      }
                  }
              }
          }
      }
  `,
})
class CarSearchDialogContent extends React.PureComponent {
  static propTypes = {
    onCarSelect: PropTypes.func.isRequired,
    viewer: PropTypes.any,
  };

  state = {
    recordsPending: false,
    value: '',
    keyword: '',
  };

  handleKeywordChange = (event) => {
    this.setState({
      value: event.target.value,
    }, this.handleSearchDebounced);
  };

  updateRelayKeyword = () => {
    const { relay } = this.props;
    const { keyword } = this.state;

    relay.setVariables({
      keyword,
      notEmptyKeyword: keyword !== '',
    }, (state) => {
      this.setState({
        recordsPending: !state.ready,
      });
    });
  };

  handleSearchDebounced = debounce(() => {
    this.setState({
      keyword: this.state.value,
    }, this.updateRelayKeyword);
  }, 300, { maxWait: 1000 });

  renderLoading = () => <CircularProgress />;

  render() {
    const { viewer, onCarSelect } = this.props;

    const { keyword, value, recordsPending } = this.state;

    const cars = map(get(viewer, 'myFirm.clientCars.edges'), 'node');

    return (
      <DialogContent>
        <DialogContentText>
          Введите часть гос номера или VIN номер автомобиля.
        </DialogContentText>
        <TextField
          autoFocus
          value={value}
          onChange={this.handleKeywordChange}
          margin="dense"
          placeholder="Поиск..."
          fullWidth
          InputProps={{
            maxLength: 250,
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
        />
        <CarSearchTable
          onCarSelect={onCarSelect}
          pending={recordsPending}
          cars={cars}
          renderLoading={this.renderLoading}
          keyword={keyword}
        />
      </DialogContent>
    );
  }
}

export default CarSearchDialogContent;
