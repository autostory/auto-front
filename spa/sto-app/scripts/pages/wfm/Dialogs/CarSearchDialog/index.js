import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import Slide from '@material-ui/core/Slide';

import ConfiguredRadium from '../../../../radium/ConfiguredRadium';
import CarSearchDialogContent from './CarSearchDialogContent';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

@ConfiguredRadium
class CarSearchDialog extends React.PureComponent {
  static propTypes = {
    onCarSelect: PropTypes.func.isRequired,
    onRequestClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
  };

  onClose = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onCarSelect = (carId, carTitle) => {
    const { onCarSelect, onRequestClose } = this.props;

    onCarSelect(carId, carTitle);
    onRequestClose();
  };

  render() {
    const { open, onRequestClose } = this.props;

    return (
      <Dialog
        fullScreen={false}
        TransitionComponent={Transition}
        onClose={onRequestClose}
        open={open}
        maxWidth="md"
      >
        <DialogTitle>Поиск автомобиля</DialogTitle>
        <CarSearchDialogContent
          initialVariables={{
            keyword: '',
            notEmptyKeyword: false,
          }}
          onCarSelect={this.onCarSelect}
        />
        <DialogActions>
          <Button onClick={this.onClose} color="primary">
            Отмена
          </Button>
        </DialogActions>

      </Dialog>
    );
  }
}

export default CarSearchDialog;
