import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';
import map from 'lodash/map';
import { withStyles } from '@material-ui/core/styles';
import Highlighter from 'react-highlight-words';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';

import relayContainer from '../../../../../../common-code/decorators/relayContainer';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

const Match = ({ keywordArray, text, ...rest }) => {
  return (
    <Highlighter
      highlightStyle={{ background: '#FAD65E', padding: '4px 0' }}
      searchWords={keywordArray}
      autoEscape
      textToHighlight={text || ''}
      {...rest}
    />
  );
};

@relayContainer({
  cars: () => Relay.QL`
      fragment on Car @relay(plural: true) {
          id
          vin
          registrationSignNumber
          displayName
      }
  `,
})
class CarSearchTable extends Component {
  static defaultProps = {
    keyword: '',
  };

  static propTypes = {
    keyword: PropTypes.string.isRequired,
    onCarSelect: PropTypes.func.isRequired,
  };

  onChoice = car => () => {
    const { onCarSelect } = this.props;

    const titleArray = [];
    if (car.displayName) {
      titleArray.push(car.displayName);
    }

    if (car.vin) {
      titleArray.push(car.vin);
    }

    if (car.registrationSignNumber) {
      titleArray.push(car.registrationSignNumber);
    }

    const title = titleArray.join(' ');

    onCarSelect(car.id, title);
  };

  render() {
    const {
      keyword, pending, cars, classes,
    } = this.props;

    if (pending) {
      return <div><CircularProgress /> Идет поиск...</div>;
    }

    if (keyword === '') {
      return (
        <div>
          Введите несколько символов и появлятся подходящие результаты.
        </div>
      );
    }

    return (
      <div>
        <Paper className={classes.root}>
          {cars === null && (
            <div style={{ padding: 24 }}>
              Ничего не найдено
            </div>
          )}
          {cars !== null && (
            <Table className={classes.table}>
              <TableBody>
                {map(cars, car => (
                  <TableRow
                    key={car.id}
                    hover
                    style={{ cursor: 'pointer' }}
                    onClick={this.onChoice(car)}
                  >
                    <TableCell padding="dense">
                      {car.displayName && (
                        <Typography>
                          <Match keywordArray={[keyword]} text={car.displayName} />
                        </Typography>
                      )}
                      {car.vin && (
                        <Typography>
                          VIN: <Match keywordArray={[keyword]} text={car.vin} />
                        </Typography>
                      )}
                      {car.registrationSignNumber && (
                        <Typography>
                          Гос. номер: <Match keywordArray={[keyword]} text={car.registrationSignNumber} />
                        </Typography>
                      )}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          )}
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(CarSearchTable);

