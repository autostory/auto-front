import React, { Component } from 'react';
import withProps from 'recompose/withProps';

import Paper from '@material-ui/core/Paper';
import Relay from 'react-relay/classic';

import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';
import OrderSchedulesForm from '../../../manager/OrderPage/includes/OrderSchedulesForm';
import submitServiceWorks from '../../../manager/OrderPage/includes/submitSchedules';

@withProps({ renderLoading: () => <CircularProgress /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          order(orderId: $orderId) {
              id
              ${OrderSchedulesForm.getFragment('order')}
              office {
                  id
              }
              serviceWorks {
                  id
              }
          }
      }
  `,
})
class AddSchedulesToOrderForm extends Component {
  onOrderSchedulesFormSubmit = (values) => {
    const { order } = this.props.viewer;

    return submitServiceWorks(order, values);
  };

  render() {
    const { order } = this.props.viewer;

    const { serviceWorks } = order;

    return (
      <Paper>
        {serviceWorks.length > 0 && (
          <OrderSchedulesForm
            order={order}
            onSubmit={this.onOrderSchedulesFormSubmit}
          />
        )}
        {serviceWorks.length === 0 && (
          <Typography variant="headline" color="error">
            Планировать работы можно после их добавления
          </Typography>
        )}
      </Paper>
    );
  }
}

export default AddSchedulesToOrderForm;

