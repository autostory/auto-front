import React from 'react';
import { withStyles } from '@material-ui/core/styles/index';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Relay from 'react-relay/classic';
import get from 'lodash/get';
import map from 'lodash/map';

import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import relayContainer from '../../../../../../common-code/decorators/relayContainer';

const styles = {
  formControl: {
    maxWidth: 250,
    padding: 5,
    background: '#fff',
  },
  select: {
    background: '#fff',
  },
  menuItem: {
    maxWidth: 250,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: 'block',
  },
};

@connect(state => ({
  officeId: state.wfmPageStore.officeId,
}))
@relayContainer({
  firm: () => Relay.QL`
      fragment on Firm {
          offices {
              officesList {
                  edges {
                      node {
                          id
                          title
                          active
                          managers {
                              edges {
                                  node {
                                      id
                                  }
                              }
                          }
                          boxes(active: true) {
                              id
                          }
                      }
                  }
              }
          }
      }
  `,
})
@withStyles(styles)
class ActiveOfficeSelector extends React.Component {
  static defaultProps = {
    officeId: null,
    onlyCompleted: false,
    onChange: () => {
    },
  };

  static propTypes = {
    onlyCompleted: PropTypes.bool, // Только офисы с менеджерами и боксами
    officeId: PropTypes.string,
    onChange: PropTypes.func,
  };

  constructor(props) {
    super(props);

    const offices = map(get(props, 'firm.offices.officesList.edges'), 'node');

    let officeId = offices[0].id;
    if (this.props.officeId) {
      officeId = this.props.officeId;
    }

    this.state = {
      value: officeId,
    };
  }

  handleChange = (event) => {
    const { onChange } = this.props;

    this.setState({ value: event.target.value });

    onChange(event.target.value);
  };

  render() {
    const { classes } = this.props;

    const offices = map(get(this.props, 'firm.offices.officesList.edges'), 'node');

    if (!offices.length) {
      return null;
    }

    return (
      <div>
        <FormControl className={classes.formControl}>
          <Select
            className={classes.select}
            value={this.state.value}
            onChange={this.handleChange}
          >
            {offices.map((office) => {
              if (!office.active) {
                return false;
              }

              if (this.props.onlyCompleted) {
                const managers = map(get(office, 'managers.edges'), 'node');
                if (!managers.length || !office.boxes.length) {
                  return false;
                }
              }

              return (
                <MenuItem
                  className={classes.menuItem}
                  key={office.id}
                  value={office.id}
                  title={office.title}
                >
                  {office.title}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </div>
    );
  }
}

export default ActiveOfficeSelector;
