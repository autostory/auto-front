import map from 'lodash/map';
import isUndefined from 'lodash/isUndefined';
import cloneDeep from 'lodash/cloneDeep';

import CreateClientAsManagerMutation from '../../../../relay/mutation/CreateOrderAsManagerMutation';
import commitMutationPromise from '../../../../../../common-code/relay/commitMutationPromise';

function submit(values, onOrderCreated) {
  const input = cloneDeep(values);

  const haveServiceWork = input.serviceWorks && input.serviceWorks.length;

  if (!haveServiceWork && input.isClientVisitRequired) {
    input.serviceMaterials = [];
    input.serviceWorks = [];
    input.schedules = [];
  } else {
    input.isClientVisitRequired = false;

    input.serviceMaterials = map(input.serviceMaterials, (serviceMaterial) => {
      if (!isUndefined(serviceMaterial.id)) {
        // eslint-disable-next-line no-param-reassign
        delete (serviceMaterial.id);
      }

      return serviceMaterial;
    });

    input.serviceWorks = map(input.serviceWorks, (serviceWork) => {
      if (!isUndefined(serviceWork.id)) {
        // eslint-disable-next-line no-param-reassign
        serviceWork.tempId = serviceWork.id;
        delete (serviceWork.id);
      }

      return serviceWork;
    });

    input.schedules = map(input.schedules, (schedule) => {
      if (!isUndefined(schedule.id)) {
        // eslint-disable-next-line no-param-reassign
        delete (schedule.id);
      }

      if (!isUndefined(schedule.status)) {
        // eslint-disable-next-line no-param-reassign
        delete (schedule.status);
      }

      if (schedule.start) {
        // eslint-disable-next-line no-param-reassign
        schedule.start = parseInt(schedule.start / 1000, 10);
      }

      if (schedule.finish) {
        // eslint-disable-next-line no-param-reassign
        schedule.finish = parseInt(schedule.finish / 1000, 10);
      }

      return schedule;
    });
  }

  return commitMutationPromise(CreateClientAsManagerMutation, { input })
    .then((data) => {
      onOrderCreated(data.createOrderAsManager.newOrder.id);
    });
}

export default submit;
