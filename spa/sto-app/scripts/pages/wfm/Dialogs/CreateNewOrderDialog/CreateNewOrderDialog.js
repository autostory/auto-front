import React, { Component } from 'react';
import get from 'lodash/get';
import filter from 'lodash/filter';
import map from 'lodash/map';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import Slide from '@material-ui/core/Slide';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Paper from '@material-ui/core/Paper';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';
import Relay from 'react-relay/classic';

import { reduxForm, formValueSelector, getFormError, getFormSyncWarnings } from 'redux-form';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';
import ActiveOfficeSelector from './ActiveOfficeSelector';
import validate from './validate';
import submit from './submit';

import ConfiguredRadium from '../../../../radium/ConfiguredRadium';
import CreateNewOrderStepper from './CreateNewOrderStepper';

export const FORM_NAME = 'CreateNewOrderForm';

const CreateNewOrderStepperForm = reduxForm({
  form: FORM_NAME,
  enableReinitialize: true,
  validate,
})(CreateNewOrderStepper);

const selector = formValueSelector(FORM_NAME);

const CreateNewOrderStepperFormWithValues = connect(state => ({
  isClientVisitRequired: selector(state, 'isClientVisitRequired'),
  clientId: selector(state, 'clientId'),
  orderId: selector(state, 'orderId'),
  carId: selector(state, 'carId'),
  boxId: selector(state, 'boxId'),
  serviceMaterials: selector(state, 'serviceMaterials'),
  serviceWorks: selector(state, 'serviceWorks'),
  schedules: selector(state, 'schedules'),
  phoneSameAsInProfile: selector(state, 'phoneSameAsInProfile'),
  formError: getFormError(FORM_NAME)(state),
  syncWarnings: getFormSyncWarnings(FORM_NAME)(state),
}))(CreateNewOrderStepperForm);

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

const styles = theme => ({
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '70%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  paper: {
    padding: '0 24px 24px 24px',
    marginTop: 69,
    marginBottom: 61,
    height: '100%',
    overflowY: 'auto',
    overflowX: 'hidden',
  },
});

const initialValues = {
  clientId: null,
  orderId: null,
  contactName: '',
  contactPhone: '',
  contactNote: '',
  carId: null,
  boxId: null,
  workStartDateTime: null,
  clientVisitDateTime: null,
  workFinishDateTime: null,
  phoneSameAsInProfile: true,
  serviceMaterials: [],
  serviceWorks: [],
  isClientVisitRequired: false,
  schedules: [],
};

@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          myFirm {
              id
              offices {
                  officesList {
                      edges {
                          node {
                              id
                              boxes(active: true) {
                                  id
                              }
                              managers {
                                  edges {
                                      node {
                                          id
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
              ${ActiveOfficeSelector.getFragment('firm')}
          }
          ${CreateNewOrderStepper.getFragment('viewer')}
          ${CreateNewOrderStepperFormWithValues.getFragment('viewer')}
      }
  `,
})
@ConfiguredRadium
@withStyles(styles)
class CreateNewOrderDialog extends Component {
  static defaultProps = {
    classes: {},
    onOrderCreated: () => {
    },
  };

  static propTypes = {
    classes: PropTypes.object,
    onRequestClose: PropTypes.func.isRequired,
    onOrderCreated: PropTypes.func,
    open: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    let offices = map(get(props, 'viewer.myFirm.offices.officesList.edges'), 'node');

    offices = filter(offices, (office) => {
      const managers = map(get(office, 'managers.edges'), 'node');

      return !(!managers.length || !office.boxes.length);
    });

    this.state = {
      officeId: offices[0].id,
    };
  }

  onClose = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onCancel = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onActiveOfficeChange = (officeId) => {
    this.setState({ officeId });
  };

  onSubmit = (values) => {
    const { onOrderCreated } = this.props;

    return submit(values, onOrderCreated);
  };

  render() {
    const {
      open, onRequestClose, classes, onOrderCreated, viewer, relay, ...rest
    } = this.props;

    const { officeId } = this.state;

    const firm = viewer.myFirm;
    const { basic } = viewer;

    return (
      <Dialog
        disableBackdropClick
        fullScreen
        PaperProps={{
          style: {
            overflow: 'hidden',
          },
        }}
        TransitionComponent={Transition}
        onClose={onRequestClose}
        open={open}
        maxWidth="md"
        {...rest}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton color="inherit" onClick={this.onClose} aria-label="Close">
              <CloseIcon />
            </IconButton>
            <Typography variant="title" color="inherit" className={classes.flex}>
              Создание заказа
            </Typography>
            <ActiveOfficeSelector
              onlyCompleted
              firm={firm}
              activeOfficeId={officeId}
              onChange={this.onActiveOfficeChange}
            />
            <Button color="inherit" onClick={this.onCancel}>
              Отмена
            </Button>
          </Toolbar>
        </AppBar>
        <Paper className={classes.paper}>
          <CreateNewOrderStepperFormWithValues
            officeId={officeId}
            viewer={viewer}
            firm={firm}
            basic={basic}
            initialValues={initialValues}
            onSubmit={this.onSubmit}
          />
        </Paper>
      </Dialog>
    );
  }
}

export default CreateNewOrderDialog;
