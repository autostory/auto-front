import React from 'react';
import isEqual from 'lodash/isEqual';
import Relay from 'react-relay/classic';
import { Prompt } from 'react-router-dom';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import shallowequal from 'shallowequal';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import pink from '@material-ui/core/colors/pink';
import green from '@material-ui/core/colors/green';
import map from 'lodash/map';
import find from 'lodash/find';
import get from 'lodash/get';
import isNull from 'lodash/isNull';
import isUndefined from 'lodash/isUndefined';
import FormGroup from '@material-ui/core/FormGroup';
import NoteIcon from '@material-ui/icons/Note';
import { connect } from 'react-redux';

import { Field, getFormSyncErrors } from 'redux-form';
import { TextField, Checkbox } from 'redux-form-material-ui';
import DateTimeFormInput, {
  dateToTimestamp,
  timeStampToDate,
} from '../../../../components/common/FormInput/DateTimeFormInput';
import Car from '../../../../components/Form/Order/Car';
import Client from '../../../../components/Form/Order/Client';
import Contact from '../../../../components/Form/Order/Contact';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import ViewerRoute from '../../../../../../common-code/relay/route/ViewerRoute';
import ClientSearchDialog from '../ClientSearchDialog/index';
import CreateClientDialog from '../CreateClientDialog';
import CarWorkEditableTable from '../includes/CarWorkEditableTable';
import CarWorkSparePartsEditableTable from '../includes/CarWorkSparePartsEditableTable';
import OrderSchedulesEditableTable from '../includes/OrderSchedulesEditableTable';
import { FORM_NAME } from './CreateNewOrderDialog';
import SelectClientCarsDialog from '../SelectClientCarsDialog/index';
import ExistsCarOrder from './ExistsCarOrder';

const styles = theme => ({
  root: {
    width: '100%',
  },
  stepsContainer: {
    position: 'fixed',
    top: 60,
    left: 0,
    width: '100%',
    padding: '24px 24px',
    background: '#FFF',
    borderBottom: '1px solid #E0E0E0',
    zIndex: 1,
    boxShadow: '1px -9px 21px #000',
  },
  button: {
    marginRight: theme.spacing.unit,
  },
  completed: {
    display: 'inline-block',
  },
  instructions: {
    // marginTop: 80,
    // marginBottom: 80,
  },
  actionContainer: {
    position: 'fixed',
    display: 'flex',
    justifyContent: 'flex-end',
    bottom: 0,
    left: 0,
    borderTop: '1px solid #E0E0E0',
    width: '100%',
    padding: '12px 24px',
    background: '#FFF',
    boxShadow: '1px 9px 21px #000',
  },
  listItemButton: {
    // paddingLeft: 0,
  },
  listItemRoot: {
    padding: 0,
  },
  pinkAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: pink[500],
  },
  greenAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: green[500],
  },
  formGroup: {
    paddingRight: 60,
  },
});

const stepTitles = ['Заказчик и Автомобиль', 'Детали заказа', 'Планирование'];

@relayContainer(
  {
    viewer: () => Relay.QL`
        fragment on Viewer {
            client(clientId: $clientId) @include(if: $clientSelected)  {
                ${SelectClientCarsDialog.getFragment('client')}
                ${Client.getFragment('client')}
                ${Contact.getFragment('client')}
                ${Car.getFragment('client')}
                id
                fullName
                phone
                cars {
                    itemsCount
                    carList {
                        edges {
                            node {
                                id
                                displayName
                                registrationSignNumber
                                ${Car.getFragment('car')}
                            }
                        }
                    }
                }
            }
            myFirm {
                id
                offices {
                    officesList {
                        edges {
                            node {
                                ${OrderSchedulesEditableTable.getFragment('office')}
                                id
                                title
                                boxes(active: true) {
                                    id
                                    title
                                }
                            }
                        }
                    }
                }
            }
        }
    `,
  },
  {
    clientSelected: false,
    clientId: null,
  },
)
@connect(state => ({
  formSyncErrors: getFormSyncErrors(FORM_NAME)(state),
}))
class CreateNewOrderStepper extends React.PureComponent {
  constructor(props) {
    super(props);

    props.change('officeId', props.officeId);
  }

  state = {
    activeStep: 0,
    showClientError: false,
    showCarError: false,
    showClientSearchDialog: false,
    showSelectClientCarsDialog: false,
    showCarAddDialog: false,
    showClientAddDialog: false,
  };

  componentDidUpdate = (prevProps) => {
    const { relay, clientId } = this.props;

    let prevClientId = prevProps.clientId;
    if (isUndefined(prevClientId)) {
      prevClientId = null;
    }

    let currentClientId = clientId;
    if (isUndefined(currentClientId)) {
      currentClientId = null;
    }

    if (
      (isNull(prevClientId) && !isNull(currentClientId))
      || (!isNull(prevClientId) && !isNull(currentClientId) && prevClientId !== currentClientId)
    ) {
      relay.setVariables(
        {
          clientSelected: true,
          clientId,
        },
        this.afterClientIdChange,
      );
    }

    const { viewer, carId, change } = this.props;
    const { client } = viewer;

    if (!isUndefined(client) && isNull(carId)) {
      const clientCarsEdges = get(client, 'cars.carList.edges', null);
      if (clientCarsEdges) {
        const clientCars = clientCarsEdges && map(clientCarsEdges, 'node');
        change('carId', clientCars[0].id);
      }
    }

    if (!isUndefined(client) && !(client.phone)) {
      change('phoneSameAsInProfile', false);
    }

    if (prevProps.officeId !== this.props.officeId) {
      this.props.change('officeId', this.props.officeId);

      const officesEdges = get(this.props.viewer, 'myFirm.offices.officesList.edges', null);
      const offices = officesEdges && map(officesEdges, 'node');

      const currentOffice = find(offices, { id: this.props.officeId });
      let currentOfficeBoxes = [];
      if (currentOffice) {
        currentOfficeBoxes = currentOffice.boxes;
      }

      if (currentOfficeBoxes.length) {
        this.props.change('boxId', currentOfficeBoxes[0].id);
      } else {
        this.props.change('boxId', null);
      }
    }

    const { showClientError, showCarError } = this.state;

    const clientIdError = get(this.props, 'formSyncErrors.clientId');

    if (showClientError && !clientIdError) {
      this.setState({
        showClientError: false,
      });
    }

    const carIdError = get(this.props, 'formSyncErrors.carId');

    if (showCarError && !carIdError) {
      this.setState({
        showCarError: false,
      });
    }
  };

  onClientCreated = ({ newClientId }) => {
    const { change } = this.props;

    change('carId', null);
    change('clientId', newClientId);
    this.hideClientAddDialog();
  };

  showClientAddDialog = () => {
    this.setState({
      showClientAddDialog: true,
    });
  };

  hideClientAddDialog = () => {
    this.setState({
      showClientAddDialog: false,
      showClientSearchDialog: false,
    });
  };

  onClientSelect = (selectedClientId) => {
    const { change } = this.props;

    change('carId', null);
    change('clientId', selectedClientId);
  };

  onCarSelect = (selectedCarId) => {
    this.props.change('carId', selectedCarId);
  };

  onCreateOrderButtonClick = () => {
    const { handleSubmit } = this.props;

    handleSubmit();
  };

  onServiceWorkChange = (serviceWorks) => {
    if (!shallowequal(this.props.serviceWorks, serviceWorks)) {
      this.props.change('serviceWorks', serviceWorks);
    }
  };

  onServiceMaterialChange = (serviceMaterials) => {
    if (!shallowequal(this.props.serviceMaterials, serviceMaterials)) {
      this.props.change('serviceMaterials', serviceMaterials);
    }
  };

  onSchedulesChange = (schedules) => {
    const {
      schedules: currentSchedules,
      change,
    } = this.props;

    if (!isEqual(currentSchedules, schedules)) {
      change('schedules', schedules);
    }
  };

  renderLoading = () => <CircularProgress />;

  getStepContent = (step) => {
    const {
      classes, officeId, viewer, carId, clientId, isClientVisitRequired, schedules,
    } = this.props;
    const {
      showClientError, showCarError,
    } = this.state;
    const client = viewer.client;

    const clientCarsEdges = get(client, 'cars.carList.edges', null);
    const clientCars = clientCarsEdges && map(clientCarsEdges, 'node');

    let selectedCar = null;

    if (carId && clientCars) {
      selectedCar = find(clientCars, { id: carId });
    }

    const officesEdges = get(viewer, 'myFirm.offices.officesList.edges', null);
    const offices = officesEdges && map(officesEdges, 'node');

    const currentOffice = find(offices, { id: officeId });

    const haveServiceWorks = this.props.serviceWorks && this.props.serviceWorks.length > 0;

    switch (step) {
      case 0:
        return (
          <div>
            <div>
              <Typography variant="headline">Заказчик</Typography>
              <FormGroup className={classes.formGroup}>
                <Client
                  error={showClientError}
                  onClearClick={this.clearSelectedClient}
                  client={client}
                  loading={clientId && isUndefined(client)}
                  onStartCleintSearch={this.showClientSearchDialog}
                  onStartClientAdd={this.showClientAddDialog}
                />

                <Contact
                  client={client}
                  loading={clientId && isUndefined(client)}
                  phoneSameAsInProfile={this.props.phoneSameAsInProfile}
                />

                <Typography variant="headline">Автомобиль</Typography>

                <Car
                  error={showCarError}
                  loading={(clientId && isUndefined(client)) || (carId && isUndefined(client))}
                  onCarAddClick={this.showAddClientCarsDialog}
                  onCarSelectClick={this.showSelectClientCarsDialog}
                  client={client}
                  car={selectedCar}
                  withProfileLink
                />

                {selectedCar && (
                  <ExistsCarOrder
                    initialVariables={{ carId: selectedCar.id }}
                  />
                )}

                <br />
                <Field
                  component={TextField}
                  name="contactNote"
                  label="Заметка"
                  multiline
                  rowsMax={5}
                  InputProps={{
                    maxLength: 250,
                    startAdornment: (
                      <InputAdornment position="start">
                        <NoteIcon />
                      </InputAdornment>
                    ),
                  }}
                  placeholder="Введите комментарий"
                />
              </FormGroup>
            </div>
          </div>
        );
      case 1:
        return (
          <div>
            <Field
              required
              inputProps={{
                maxLength: 250,
              }}
              component={TextField}
              name="orderReason"
              autoFocus
              label="Причина обращения"
              placeholder="Укажите причину обращения клиента"
              multiline
              rows={2}
              fullWidth
              margin="normal"
            />
            {!haveServiceWorks && (
              <React.Fragment>
                <br />
                <br />
                <FormControlLabel
                  control={
                    <Field
                      color="primary"
                      required
                      name="isClientVisitRequired"
                      component={Checkbox}
                    />
                  }
                  label="Для формирования списка работ требуется визит клиента"
                />
              </React.Fragment>
            )}
            {!isClientVisitRequired && (
              <div>
                <Typography variant="headline">Перечень выполняемых работ</Typography>
                <CarWorkEditableTable
                  serviceWorks={this.props.serviceWorks}
                  initialVariables={{ officeId }}
                  forceFetch
                  renderLoading={this.renderLoading}
                  onChange={this.onServiceWorkChange}
                  officeId={officeId}
                />
                <br />
              </div>
            )}
            {!isClientVisitRequired && (
              <div>
                <Typography variant="headline">Используемые запасные части (материалы)</Typography>
                <CarWorkSparePartsEditableTable
                  serviceMaterials={this.props.serviceMaterials}
                  onChange={this.onServiceMaterialChange}
                  forceFetch
                  renderLoading={this.renderLoading}
                />
              </div>
            )}
          </div>
        );
      case 2:
        return (
          <div>
            <div style={{ marginLeft: 20 }}>
              <Field
                name="estimatedClientArrivalDate"
                label="Когда приедет клиент"
                normalize={dateToTimestamp}
                format={timeStampToDate}
                component={DateTimeFormInput}
                disablePast
                clearable
              />
              <br />
              <br />
              <Typography variant="subheading" color="inherit">
                Планирование работ
              </Typography>
              {!isClientVisitRequired && this.props.serviceWorks && this.props.serviceWorks.length > 0 && (
                <OrderSchedulesEditableTable
                  office={currentOffice}
                  order={null}
                  serviceWorks={this.props.serviceWorks}
                  schedules={schedules}
                  onChange={this.onSchedulesChange}
                />
              )}
              {(isClientVisitRequired || (this.props.serviceWorks && this.props.serviceWorks.length === 0)) && (
                <Typography color="error">
                  На предыдущем шаге не были добавлены работы. Планирование возможно только если есть работы.
                </Typography>
              )}
            </div>
          </div>
        );
      default:
        return 'Неизвестный шаг';
    }
  };

  afterClientIdChange = () => {
    const { change } = this.props;

    const client = this.props.viewer.client;

    const clientCarsEdges = get(client, 'cars.carList.edges', null);
    if (!isNull(clientCarsEdges)) {
      const clientCars = clientCarsEdges && map(clientCarsEdges, 'node');
      change('carId', clientCars[0].id);
    } else {
      change('carId', null);
    }
  };

  clearSelectedClient = () => {
    this.setState({
      selectedCar: null,
    });

    const { change, relay } = this.props;
    change('clientId', null);
    change('carId', null);

    relay.setVariables(
      {
        clientId: null,
        clientSelected: false,
      },
      this.afterClientIdChange,
    );
  };

  showClientSearchDialog = () => {
    this.setState({
      showClientSearchDialog: true,
    });
  };

  hideClientSearchDialog = () => {
    this.setState({
      showClientSearchDialog: false,
    });
  };

  hideSelectClientCarsDialog = () => {
    this.setState({
      showSelectClientCarsDialog: false,
      showCarAddDialog: false,
    });
  };

  showSelectClientCarsDialog = () => {
    this.setState({
      showSelectClientCarsDialog: true,
    });
  };

  showAddClientCarsDialog = () => {
    this.setState({
      showSelectClientCarsDialog: true,
      showCarAddDialog: true,
    });
  };

  afterCarAdd = () => {
    this.setState({
      showSelectClientCarsDialog: true,
      showCarAddDialog: false,
    });
  };

  handleNext = () => {
    const { activeStep } = this.state;
    const { formSyncErrors, touch } = this.props;

    if (activeStep === stepTitles.length - 1) {
      this.onCreateOrderButtonClick();
    } else {
      if (this.state.activeStep === 0) {
        const { firstStepErrors } = formSyncErrors;
        if (!firstStepErrors) {
          this.setState({
            activeStep: this.state.activeStep + 1,
          });
        } else {
          const toTouch = [];

          if (formSyncErrors) {
            for (const key in firstStepErrors) {
              if (!isUndefined(firstStepErrors[key])) {
                toTouch.push(key);
              }
            }
          }

          touch(...toTouch);

          if (firstStepErrors.clientId) {
            this.setState({
              showClientError: true,
            });
          }

          if (firstStepErrors.carId) {
            this.setState({
              showCarError: true,
            });
          }
        }

        return;
      }

      if (this.state.activeStep === 1) {
        const { secondStepErrors } = formSyncErrors;
        if (!secondStepErrors) {
          this.setState({
            activeStep: this.state.activeStep + 1,
          });
        } else {
          const toTouch = [];

          if (secondStepErrors) {
            for (const key in secondStepErrors) {
              if (!isUndefined(secondStepErrors[key])) {
                toTouch.push(key);
              }
            }
          }

          touch(...toTouch);
        }
      }
    }
  };

  handleBack = () => {
    const { activeStep } = this.state;
    this.setState({
      activeStep: activeStep - 1,
    });
  };

  render() {
    const { classes } = this.props;
    const {
      activeStep, showClientSearchDialog,
      showSelectClientCarsDialog, showCarAddDialog, showClientAddDialog,
    } = this.state;

    const {
      pristine, submitting, dirty, submitSucceeded,
    } = this.props;

    const selectedClient = this.props.viewer.client;

    return (
      <div className={classes.root}>
        <Prompt
          when={dirty && !submitSucceeded}
          message={location =>
            'На странице есть не сохраненные изменения. Вы действительно хотите покинуть страницу?'
          }
        />
        <ClientSearchDialog
          onStartCreateClient={this.showClientAddDialog}
          canCreateClient
          onClientSelect={this.onClientSelect}
          initialVariables={{
            keyword: '',
            notEmptyKeyword: false,
          }}
          open={showClientSearchDialog}
          onRequestClose={this.hideClientSearchDialog}
        />
        {selectedClient && (
          <SelectClientCarsDialog
            afterCarAdd={this.afterCarAdd}
            client={selectedClient}
            onCarSelect={this.onCarSelect}
            open={showSelectClientCarsDialog}
            showCarAddDialog={showCarAddDialog}
            onRequestClose={this.hideSelectClientCarsDialog}
          />
        )}
        <Relay.RootContainer
          Component={CreateClientDialog}
          route={new ViewerRoute()}
          forceFetch
          renderFetched={data => (
            <CreateClientDialog
              onRequestClose={this.hideClientAddDialog}
              onClientCreated={this.onClientCreated}
              open={showClientAddDialog}
              viewer={data.viewer}
            />
          )}
        />
        <Stepper nonLinear activeStep={activeStep} classes={{ root: classes.stepsContainer }}>
          {stepTitles.map(label => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        <div>
          <div>
            <Typography
              className={classes.instructions}
              component="div"
            >
              {this.getStepContent(activeStep)}
            </Typography>
            <div className={classes.actionContainer}>
              <Button
                disabled={activeStep === 0 || submitting}
                onClick={this.handleBack}
                className={classes.button}
              >
                Назад
              </Button>
              <Button
                disabled={pristine || submitting}
                variant="contained"
                color="primary"
                onClick={this.handleNext}
                className={classes.button}
              >
                {activeStep === stepTitles.length - 1 ? 'Создать заказ' : 'Далее'}
              </Button>
              {submitting && <CircularProgress />}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CreateNewOrderStepper.propTypes = {
  classes: PropTypes.object,
};

export default withStyles(styles)(CreateNewOrderStepper);
