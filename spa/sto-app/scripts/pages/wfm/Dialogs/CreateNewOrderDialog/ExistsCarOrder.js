import React from 'react';
import { format } from 'date-fns';
import withProps from 'recompose/withProps';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import Relay from 'react-relay/classic';
import get from 'lodash/get';
import map from 'lodash/map';
import { Link } from 'react-router-dom';

import { getOrderStatusTypeName } from '../../../../../../common-code/constants/orderStatusTypes';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';
import { fromGlobalId } from '../../../../../../common-code/relay/globalIdUtils';

const renderLoading = () => (
  <Typography>
    <CircularProgress size={18} /> Проверка существования других заявок по этому автомобилю
  </Typography>
);

const rawStyle = {
  officeTitle: {
    maxWidth: 300,
    display: 'block',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  paper: {
    padding: 12,
    margin: 6,
  },
  container: {
    display: 'flex',
    width: '100%',
    justifyContent: 'flex-start',
  },
};

@withProps({ renderLoading })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          myFirm {
              orders(
                  active: true,
                  carId: $carId,
                  statuses: [
                      WAIT_WORK_ADD,
                      WAIT_PLANING,
                      WAIT_CAR_ARRIVE,
                      WAIT_WORK_START,
                      IN_WORK,
                      WAIT_CAR_RETURN,
                      WAIT_PAYMENT
                  ],
                  after: null,
                  first: 100,
              ) {
                  edges {
                      node {
                          id
                          status
                          office {
                              id
                              title
                          }
                          makeAt {
                              timestamp
                          }
                      }
                  }
              }
          }
      }
  `,
})
class ExistsCarOrder extends React.Component {
  render() {
    const { viewer } = this.props;
    const { myFirm } = viewer;
    const orders = map(get(myFirm, 'orders.edges'), 'node');

    if (!orders.length) {
      return null;
    }

    return (
      <div>
        <Typography>По данному автомобилю есть не закрытые заказы:</Typography>
        <div style={rawStyle.container}>
          {map(orders, order => (
            <Paper style={rawStyle.paper}>
              <Typography component={Link} to={`/order/${order.id}`} target="_blank">
                <Typography>
                  Заказ №{fromGlobalId(order.id).id}
                </Typography>
                <Typography>
                  Статус {getOrderStatusTypeName(order.status)}
                </Typography>
                <Typography>
                  Дата создания {format(order.makeAt.timestamp * 1000, 'DD.MM.YY HH:mm')}
                </Typography>
                <Typography
                  style={rawStyle.officeTitle}
                  title={order.office.title}
                >
                  Офис {order.office.title}
                </Typography>
              </Typography>
            </Paper>
          ))}
        </div>
      </div>
    );
  }
}

export default ExistsCarOrder;
