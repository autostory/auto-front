import validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export const validateStepOne = ({ clientId, carId, phoneSameAsInProfile, contactPhone, contactName }) => {
  const errors = {};

  if (!clientId) {
    errors.clientId = 'Укажите клиента.';
  }

  if (!carId) {
    errors.carId = 'Укажите автомобиль.';
  }

  if (!phoneSameAsInProfile) {
    if (!contactPhone) {
      errors.contactPhone = 'Укажите контактный номер.';
    } else {
      const phoneMatch = contactPhone.match(/\d+/g);
      if (phoneMatch === null || !validator.isMobilePhone(phoneMatch.join(''), 'ru-RU', true)) {
        errors.phoneNumber = 'Укажите контактный номер';
      }
    }

    if (!contactName) {
      errors.contactName = 'Укажите имя контактного лица.';
    }
  }

  return errors;
};

export const validateStepTwo = ({ orderReason }) => {
  const errors = {};

  if (!orderReason) {
    errors.orderReason = 'Укажите причину обращения.';
  }

  if (orderReason && !orderReason.trim()) {
    errors.orderReason = 'Укажите причину обращения.';
  }

  return errors;
};

function validate(values) {
  const firstStepErrors = validateStepOne(values);
  const secondStepErrors = validateStepTwo(values);

  let errors = {};
  if (!isEmpty(firstStepErrors)) {
    errors.firstStepErrors = firstStepErrors;
    errors = { ...errors, ...firstStepErrors };
  }

  if (!isEmpty(secondStepErrors)) {
    errors.secondStepErrors = secondStepErrors;
    errors = { ...errors, ...secondStepErrors };
  }

  return errors;
}

export default validate;
