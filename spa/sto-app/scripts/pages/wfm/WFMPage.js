import React from 'react';

import Relay from 'react-relay/classic';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import withProps from 'recompose/withProps';

import FullScreenPreLoader from '../../components/basic/FullScreenPreLoader';
import relaySuperContainer from '../../../../common-code/decorators/relaySuperContainer';
import { setPageHeader } from '../../reduxActions/ChangePageHeaderActions';
import {
  setShowActiveOfficeSelector, setShowRefreshButton, setWFMActiveOfficeId,
} from '../../reduxActions/WfmPageActions';
import WFM from './WFM';

const style = {
  wfmLoading: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
};

@withProps({ renderLoading: () => <div style={style.wfmLoading}><FullScreenPreLoader /></div> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          ${WFM.getFragment('viewer')}
      }
  `,
})
@withRouter
@connect(
  state => ({
    activeOfficeId: state.wfmPageStore.officeId,
  }),
  dispatch => ({
    setPageHeader: header => dispatch(setPageHeader(header)),
    setWFMActiveOfficeId: id => dispatch(setWFMActiveOfficeId(id)),
    setShowActiveOfficeSelector: value => dispatch(setShowActiveOfficeSelector(value)),
    setShowRefreshButton: value => dispatch(setShowRefreshButton(value)),
  }),
)
class WFMPage extends React.PureComponent {
  constructor(props) {
    super(props);

    const { match } = props;
    const { officeId } = match.params;

    if (props.activeOfficeId && officeId !== props.activeOfficeId) {
      props.history.push(`/wfm/${props.activeOfficeId}`);
    }

    if (!props.activeOfficeId && officeId) {
      props.setWFMActiveOfficeId(officeId);
    }

    props.setPageHeader('Рабочий стол');
    props.setShowActiveOfficeSelector(true);
    props.setShowRefreshButton(true);
  }

  componentDidUpdate() {
    const { match } = this.props;
    const { officeId } = match.params;

    if (this.props.activeOfficeId && officeId !== this.props.activeOfficeId) {
      this.props.history.push(`/wfm/${this.props.activeOfficeId}`);
    }

    if (!this.props.activeOfficeId && officeId) {
      this.props.setWFMActiveOfficeId(officeId);
    }
  }

  componentWillUnmount() {
    this.props.setShowActiveOfficeSelector(false);
    this.props.setShowRefreshButton(false);
  }

  render() {
    const { match, viewer } = this.props;

    const { officeId } = match.params;

    if (officeId) {
      return (
        <WFM
          viewer={viewer}
          officeId={officeId}
        />
      );
    }

    return (
      <div style={style.wfmLoading}>
        <FullScreenPreLoader />
      </div>
    );
  }
}

export default WFMPage;
