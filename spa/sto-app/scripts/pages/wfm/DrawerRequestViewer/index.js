import get from 'lodash/get';
import map from 'lodash/map';
import first from 'lodash/first';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import Relay from 'react-relay/classic';
import { reduxForm, formValueSelector } from 'redux-form';
import relayContainer from '../../../../../common-code/decorators/relayContainer';
import DrawerRequestViewerInner from './DrawerRequestViewerInner';
import submit from './submit';

const FORM_NAME = 'DrawerRequestViewerForm';

const DrawerRequestViewerForm = reduxForm({
  form: FORM_NAME, // a unique identifier for this form
  enableReinitialize: true,
})(DrawerRequestViewerInner);

const selector = formValueSelector(FORM_NAME);

const DrawerRequestViewerFormWithValues = connect(state => ({
  resultId: selector(state, 'resultId'),
  rejectReasonId: selector(state, 'rejectReasonId'),
  notifyUser: selector(state, 'notifyUser'),
  rejectReasonDescription: selector(state, 'rejectReasonDescription'),
  recallDate: selector(state, 'recallDate'),
  recallTime: selector(state, 'recallTime'),
}))(DrawerRequestViewerForm);

@relayContainer({
  firm: () => Relay.QL`
      fragment on Firm {
          offices {
              officesList {
                  edges {
                      node {
                          id
                      }
                  }
              }
          }
          ${DrawerRequestViewerInner.getFragment('firm')}
      }
  `,
})
@connect(state => ({
  request: state.wfmPageStore.request,
  activeOfficeId: state.wfmPageStore.officeId,
  requestDialogOpen: state.wfmPageStore.requestDialogOpen,
}))
class DrawerRequestViewer extends React.PureComponent {
  static defaultProps = {
    request: null,
    dispatch: () => {},
    requestDialogOpen: false,
    onDrawerClose: () => {},
    onMakeOrderFromRequest: () => {},
  };

  static propTypes = {
    dispatch: PropTypes.func,
    request: PropTypes.object,
    requestDialogOpen: PropTypes.bool,
    onDrawerClose: PropTypes.func,
    onMakeOrderFromRequest: PropTypes.func,
  };

  onSubmit = values => {
    const { request } = this.props;

    return submit(values, request);
  };

  render() {
    const {
      requestDialogOpen, request, onDrawerClose, onMakeOrderFromRequest, firm,
    } = this.props;

    if (request === null) {
      return false;
    }

    const offices = map(get(firm, 'offices.officesList.edges'), 'node');
    const firstOffice = first(offices);

    const initialTime = new Date();
    const addHours = 1;
    initialTime.setTime(initialTime.getTime() + (addHours * 60 * 60 * 1000)); // +1h

    const recallDate = new Date();

    return (
      <DrawerRequestViewerFormWithValues
        request={request}
        firm={firm}
        open={requestDialogOpen}
        onDrawerClose={onDrawerClose}
        onMakeOrderFromRequest={onMakeOrderFromRequest}
        initialValues={{
          resultId: 0,
          rejectReasonId: 0,
          officeId: this.props.activeOfficeId || firstOffice.id,
          recallDate,
          recallTime: initialTime,
          notifyUser: true,
          rejectReasonDescription: '',
        }}
        onSubmit={this.onSubmit}
      />
    );
  }
}

export default DrawerRequestViewer;
