import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Relay from 'react-relay/classic';
import { Timeline } from 'antd';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import relayContainer from '../../../../../common-code/decorators/relayContainer';

import ConfiguredRadium from './../../../radium/ConfiguredRadium';

@relayContainer({
  request: () => Relay.QL`
      fragment on Request {
          communicationHistory {
              id
              communicationDateTime {
                  formattedDateTime
              }
              nextCommunicationDateTime {
                  formattedDateTime
              }
              resultId
              rejectReasonId
              rejectReasonDescription
              manager {
                  id
                  fullName
              }
          }
      }
  `,
})
@ConfiguredRadium
class CommunicationHistory extends React.PureComponent {
  getRejectReasonText = (communicationHistory) => {
    switch (communicationHistory.rejectReasonId) {
      case 'COST':
        return 'Не устроила цена.';
      case 'TIME':
        return 'Не устроило время.';
      case 'NO_SERVICE':
        return 'Не оказываем такие услуги.';
      case 'BAD_CAR':
        return 'Не обслуживаем такой автомобиль.';
      case 'OTHER':
        return `Другое. ${communicationHistory.rejectReasonDescription}`;
      default:
        return '';
    }
  };

  getResultText = (communicationHistory) => {
    switch (communicationHistory.resultId) {
      case 'VISIT':
        return 'Договорились о визите';
      case 'RECALL':
        return `Перезвонить ${communicationHistory.nextCommunicationDateTime.formattedDateTime}`;
      case 'NO_ANSWER':
        return `Недозвон. Перезвонить ${communicationHistory.nextCommunicationDateTime.formattedDateTime}`;
      case 'REJECT':
        return `Отказ. Причина: ${this.getRejectReasonText(communicationHistory)}`;
      default:
        return '';
    }
  };

  render() {
    const { communicationHistory } = this.props.request;

    if (communicationHistory.length === 0) {
      return (
        <Paper style={{ padding: 24 }}>
          <Typography>С клиентом еще никто не связывался</Typography>
        </Paper>
      );
    }

    return (
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>История взаимодействия</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Timeline>
            {communicationHistory.map(item => (
              <Timeline.Item key={item.id}>
                {item.communicationDateTime.formattedDateTime}<br />
                  Звонок клиенту<br />
                {item.manager.fullName} (менеджер)<br />
                {this.getResultText(item)}
              </Timeline.Item>
              ))}
          </Timeline>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

export default CommunicationHistory;
