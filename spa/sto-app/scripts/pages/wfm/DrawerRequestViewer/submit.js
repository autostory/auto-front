import AddRequestCommunicationRecallResultMutation
  from './../../../relay/mutation/requestCommunications/AddRequestCommunicationRecallResultMutation';
import AddRequestCommunicationNoAnswerResultMutation
  from './../../../relay/mutation/requestCommunications/AddRequestCommunicationNoAnswerResultMutation';
import AddRequestCommunicationRejectResultMutation
  from './../../../relay/mutation/requestCommunications/AddRequestCommunicationRejectResultMutation';
import AddRequestCommunicationVisitResultMutation
  from './../../../relay/mutation/requestCommunications/AddRequestCommunicationVisitResultMutation';
import commitMutationPromise from '../../../../../common-code/relay/commitMutationPromise';
import DrawerRequestViewerInner from './DrawerRequestViewerInner';

const getTimeStamp = (recallDate, recallTime) => {
  const date = new Date(recallDate);
  date.setHours(recallTime.getHours());
  date.setMinutes(recallTime.getMinutes());
  date.setSeconds(0);
  date.setMilliseconds(0);

  return parseInt(date.getTime() / 1000, 10);
};

function submit(values, request) {
  if (values.resultId === 1) {
    return commitMutationPromise(
      AddRequestCommunicationVisitResultMutation,
      {
        request,
        input: {
          officeId: values.officeId,
          estimatedClientArrivalDate: values.estimatedClientArrivalDate,
        },
        requestFragment: DrawerRequestViewerInner.getFragment('request'),
      },
    );
  }

  if (values.resultId === 2) {
    const timestamp = getTimeStamp(values.recallDate, values.recallTime);

    return commitMutationPromise(
      AddRequestCommunicationRecallResultMutation,
      {
        request,
        input: { timestamp },
        requestFragment: DrawerRequestViewerInner.getFragment('request'),
      },
    );
  }

  if (values.resultId === 3) {
    const timestamp = getTimeStamp(values.recallDate, values.recallTime);

    return commitMutationPromise(
      AddRequestCommunicationNoAnswerResultMutation,
      {
        request,
        input: { timestamp, notifyUser: values.notyfyUser },
        requestFragment: DrawerRequestViewerInner.getFragment('request'),
      },
    );
  }

  if (values.resultId === 4) {
    return commitMutationPromise(
      AddRequestCommunicationRejectResultMutation,
      {
        request,
        input: {
          rejectReasonId: values.rejectReasonId,
          rejectReasonDescription: values.rejectReasonDescription,
        },
        requestFragment: DrawerRequestViewerInner.getFragment('request'),
      },
    );
  }

  return false;
}

export default submit;
