function validate(values) {
  const errors = {};
  if (values.resultId === 4 && values.rejectReasonId === 0) {
    errors.rejectReasonId = 'Укажите причину отказа';
  }

  if (values.resultId === 4 && values.rejectReasonId === 5 && values.cancelReasonDescription === '') {
    errors.cancelReasonDescription = 'Опишите причину отказа';
  }

  return errors;
}

export default validate;
