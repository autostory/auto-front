/* eslint-disable jsx-a11y/label-has-for,react/prop-types */
import React from 'react';
import Relay from 'react-relay/classic';
import withWidth from '@material-ui/core/withWidth';
import IconButton from '@material-ui/core/IconButton';
import TextSmsIcon from '@material-ui/icons/Textsms';
import Tooltip from '@material-ui/core/Tooltip';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Drawer from '@material-ui/core/Drawer';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import PersonIcon from '@material-ui/icons/Person';
import PhoneIcon from '@material-ui/icons/Phone';
import TimerIcon from '@material-ui/icons/Timer';
import CarIcon from '@material-ui/icons/DirectionsCar';
import CommentIcon from '@material-ui/icons/Comment';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import TimePicker from 'material-ui-pickers/TimePicker';
import DatePicker from 'material-ui-pickers/DatePicker';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import DialogContentText from '@material-ui/core/DialogContentText';

import { Field, Form } from 'redux-form';
import { Checkbox, Select, TextField } from 'redux-form-material-ui';
import get from 'lodash/get';
import map from 'lodash/map';

import ConfirmDialog from '../../../components/common/dialogs/ConfirmDialog';
import DateTimeFormInput, {
  dateToTimestamp,
  timeStampToDate,
} from '../../../components/common/FormInput/DateTimeFormInput';
import { REJECT, VISIT } from '../../../../../common-code/constants/resultRequestCommunicationTypes';
import relayContainer from '../../../../../common-code/decorators/relayContainer';
import { fromGlobalId } from '../../../../../common-code/relay/globalIdUtils';
import CarInfoWithPopover from './../../../components/Car/CarInfoWithPopover';

import ConfiguredRadium from './../../../radium/ConfiguredRadium';
import CommunicationHistory from './CommunicationHistory';

import AddRequestCommunicationRecallResultMutation
  from './../../../relay/mutation/requestCommunications/AddRequestCommunicationRecallResultMutation';
import AddRequestCommunicationNoAnswerResultMutation
  from './../../../relay/mutation/requestCommunications/AddRequestCommunicationNoAnswerResultMutation';
import AddRequestCommunicationRejectResultMutation
  from './../../../relay/mutation/requestCommunications/AddRequestCommunicationRejectResultMutation';
import AddRequestCommunicationVisitResultMutation
  from './../../../relay/mutation/requestCommunications/AddRequestCommunicationVisitResultMutation';

const style = {
  boxIcon: {
    width: 16,
    height: 16,
    position: 'relative',
    top: 3,
    color: '#909090',
  },
  boxData: {
    fontSize: 14,
    background: '#FFF',
    padding: '5px 10px',
    borderRadius: '0 0 5px 5px',
  },
  cell: {
    whiteSpace: 'normal',
    padding: 4,
  },
  listRow: {
    display: 'flex',
  },
  boxIconContainer: {
    marginRight: 16,
  },
};

@relayContainer({
  request: () => Relay.QL`
      fragment on Request {
          ${CommunicationHistory.getFragment('request')}
          ${AddRequestCommunicationRecallResultMutation.getFragment('request')}
          ${AddRequestCommunicationNoAnswerResultMutation.getFragment('request')}
          ${AddRequestCommunicationRejectResultMutation.getFragment('request')}
          ${AddRequestCommunicationVisitResultMutation.getFragment('request')}
          id
          active
          makeAt {
              formattedDateTime
          }
          car {
              id
              displayName
              ${CarInfoWithPopover.getFragment('car')}
          }
          client {
              id
              fullName
              phone
          }
          userMessage
          lastCommunication {
              resultId
          }
          order {
              id
          }
      }
  `,
  firm: () => Relay.QL`
      fragment on Firm {
          offices {
              officesList {
                  edges {
                      node {
                          id
                          title
                      }
                  }
              }
          }
      }
  `,
})
@ConfiguredRadium
class DrawerRequestViewerInner extends React.PureComponent {
  state = {
    showCloseAlert: false,
  };

  componentDidUpdate(prevProps) {
    if (!prevProps.submitSucceeded && this.props.submitSucceeded) {
      this.props.reset();
    }

    if (prevProps.request.active && !this.props.request.active) {
      if (this.props.resultId === 1) {
        this.props.onMakeOrderFromRequest(this.props.request.order.id);
      }
      this.props.relay.forceFetch();
      this.props.reset();
      this.props.onDrawerClose();
    }
  }

  onRecallDateChange = (date) => {
    this.props.change('recallDate', date);
  };

  onRecallTimeChange = (time) => {
    this.props.change('recallTime', time);
  };

  onClose = () => {
    // Если есть изменения в форме то покажем алерт
    const { pristine, onDrawerClose, reset } = this.props;
    if (!pristine) {
      this.setState({
        showCloseAlert: true,
      });

      return;
    }

    // Если измений нет, то надо просто вызвать this.props.onDrawerClose
    reset();
    onDrawerClose();
  };

  onAlertDialogCancel = () => {
    this.setState({
      showCloseAlert: false,
    });
  };

  onAlertDialogOk = () => {
    this.setState({
      showCloseAlert: false,
    });

    const { onDrawerClose, reset } = this.props;

    reset();
    onDrawerClose();
  };

  render() {
    const {
      open,
      request,
      handleSubmit,
      submitting,
      resultId,
      rejectReasonId,
      notifyUser,
      recallDate,
      recallTime,
      rejectReasonDescription,
      firm,
    } = this.props;

    const offices = map(get(firm, 'offices.officesList.edges'), 'node');

    let actionLabel = 'Сохранить';
    if (resultId === 1) {
      actionLabel = 'Сформировать заказ';
    }

    if (resultId === 4) {
      actionLabel = 'Закрыть заявку';
    }

    let actionDisabled = false;
    if (resultId === 0) {
      actionDisabled = true;
    }

    if (
      resultId === 4 &&
      (rejectReasonId === 0 ||
        (rejectReasonId === 5 && rejectReasonDescription === ''))
    ) {
      actionDisabled = true;
    }

    const { width } = this.props;

    const drawerPaperStyle = { padding: 24, width: 500 };

    if (width === 'xs' || width === 'sm') {
      drawerPaperStyle.width = '100%';
    }

    const { showCloseAlert } = this.state;

    const lastCommunicationResultId = request.lastCommunication && request.lastCommunication.resultId;
    const requestClosed = lastCommunicationResultId === VISIT || lastCommunicationResultId === REJECT;

    return (
      <Drawer
        variant="temporary"
        onClose={this.onClose}
        open={open}
        anchor="right"
      >
        <Form onSubmit={handleSubmit}>
          <Paper style={drawerPaperStyle}>
            <ConfirmDialog
              open={showCloseAlert}
              okText="Закрыть окно заявки"
              cancelText="Не закрывать"
              onOk={this.onAlertDialogOk}
              onCancel={this.onAlertDialogCancel}
              title="Форма имеет не сохраненные данные"
            >
              <DialogContentText>
                Вы уверены что хотите закрыть эту форму, все не сохраненные изменения будут утеряны.
              </DialogContentText>
            </ConfirmDialog>
            <Typography variant="headline">Заявка №{fromGlobalId(request.id).id}</Typography>
            <Paper style={{ padding: 12 }} component="div">
              <div style={style.boxData}>
                <Typography style={style.listRow} component="div">
                  <div style={style.boxIconContainer}><PersonIcon style={style.boxIcon} /></div>
                  <span>{request.client.fullName}</span>
                </Typography>
                <Typography style={style.listRow} component="div">
                  <div style={style.boxIconContainer}><PhoneIcon style={style.boxIcon} /></div>
                  <span>{request.client.phone}</span>
                </Typography>
                <Typography style={style.listRow} component="div">
                  <div style={style.boxIconContainer}><CarIcon style={style.boxIcon} /></div>
                  <CarInfoWithPopover car={request.car} />
                </Typography>
                <Typography style={style.listRow} component="div">
                  <div style={style.boxIconContainer}><TimerIcon style={style.boxIcon} /></div>
                  <span>поступила {request.makeAt.formattedDateTime}</span>
                </Typography>
                {request.userMessage && (
                  <Typography style={style.listRow} component="div">
                    <div style={style.boxIconContainer}><CommentIcon style={style.boxIcon} /></div>
                    <i>
                      {request.userMessage}
                    </i>
                  </Typography>
                )}
              </div>
            </Paper>
            <div style={{ marginTop: 12 }}>
              <CommunicationHistory request={request} />
            </div>
            {!requestClosed && (
              <Paper style={{ padding: 24, marginTop: 12 }} component="div">
                Результат взаимодействия:<br />
                <FormControl>
                  <Field
                    required
                    component={Select}
                    name="resultId"
                    style={{ minWidth: 220, maxWidth: '100%' }}
                    value={resultId}
                  >
                    <MenuItem value={0}><em>Не указано</em></MenuItem>
                    <MenuItem value={1}>Договорились о визите</MenuItem>
                    <MenuItem value={2}>Перезвонить</MenuItem>
                    <MenuItem value={3}>Недозвон</MenuItem>
                    <MenuItem value={4}>Отказ</MenuItem>
                  </Field>
                </FormControl>
                {resultId === 1 && (
                  <div>
                    <br />
                    Офис в котором будет создан заказ:<br />
                    <Field
                      required
                      component={Select}
                      name="officeId"
                      style={{ minWidth: 220, maxWidth: '100%' }}
                    >
                      {offices.map(item => (
                        <MenuItem
                          key={item.id}
                          value={item.id}
                          style={{ maxWidth: 400, overflow: 'hidden', textOverflow: 'ellipsis' }}
                          title={item.title}
                        >
                          {item.title}
                        </MenuItem>
                      ))}
                    </Field>
                    <br /><br />
                    <Field
                      name="estimatedClientArrivalDate"
                      label="Когда приедет клиент"
                      normalize={dateToTimestamp}
                      format={timeStampToDate}
                      component={DateTimeFormInput}
                      disablePast
                      clearable
                    />
                  </div>
                )}
                {(resultId === 2 || resultId === 3) && (
                  <div style={{ marginTop: 12 }}>
                    Когда перезвонить:<br />
                    <DatePicker
                      style={{ marginRight: 24 }}
                      onChange={this.onRecallDateChange}
                      autoOk
                      okLabel="OK"
                      cancelLabel="Закрыть"
                      format="DD.MM.YY"
                      disablePast
                      value={recallDate}
                      label="Дата (*)"
                    />
                    <TimePicker
                      ampm={false}
                      autoOk
                      okLabel="OK"
                      cancelLabel="Закрыть"
                      onChange={this.onRecallTimeChange}
                      value={recallTime}
                      format="HH:mm"
                      label="Время (*)"
                    />
                    <br />
                    {resultId === 3 && (
                      <div style={{ marginTop: 6 }}>
                        <FormControlLabel
                          control={
                            <Field
                              name="notifyUser"
                              component={Checkbox}
                            />
                          }
                          label="Уведомить клиента"
                        />
                        <Tooltip title="Текст уведомления">
                          <IconButton aria-label="SMS Test" disabled={!notifyUser}>
                            <TextSmsIcon />
                          </IconButton>
                        </Tooltip>
                      </div>
                    )}
                  </div>
                )}
                {resultId === 4 && (
                  <div style={{ marginTop: 12 }}>
                    <FormControl>
                      <InputLabel htmlFor="reject-cause">Причина отказа(*)</InputLabel>
                      <Field
                        component={Select}
                        style={{ minWidth: 220 }}
                        value={rejectReasonId}
                        name="rejectReasonId"
                        inputProps={{
                          name: 'rejectReasonId',
                          id: 'reject-cause',
                        }}
                      >
                        <MenuItem value={0}><em>Не указано</em></MenuItem>
                        <MenuItem value={1}>не устроила стоимость</MenuItem>
                        <MenuItem value={2}>не удобное время</MenuItem>
                        <MenuItem value={3}>не оказываем такую услугу</MenuItem>
                        <MenuItem value={4}>не обслуживается такой автомобиль</MenuItem>
                        <MenuItem value={5}>другое</MenuItem>
                      </Field>
                    </FormControl>
                    <br />
                    {rejectReasonId === 5 && (
                      <Field
                        inputProps={{
                          maxLength: 250,
                        }}
                        component={TextField}
                        name="rejectReasonDescription"
                        style={{ marginTop: 12, width: '100%' }}
                        label="Опишите причину отказа"
                        multiline
                        rows={1}
                        rowsMax={4}
                        placeholder="Введите причину отказа"
                      />
                    )}
                  </div>
                )}
                <div style={{ marginTop: 24 }}>
                  <Button
                    onClick={this.onClose}
                    variant="raised"
                    style={{ marginRight: 12 }}
                    disabled={submitting}
                  >
                    Отмена
                  </Button>
                  {!requestClosed && (
                    <Button
                      type="submit"
                      variant="raised"
                      color="primary"
                      disabled={actionDisabled || submitting}
                    >
                      {!submitting && actionLabel}
                      {submitting && <CircularProgress size={20} />}
                    </Button>
                  )}
                </div>
              </Paper>
            )}
            {requestClosed && (
              <div style={{ marginTop: 24 }}>
                <Button
                  onClick={this.onClose}
                  color="primary"
                  variant="raised"
                  style={{ marginRight: 12 }}
                >
                  Закрыть
                </Button>
              </div>
            )}
          </Paper>
        </Form>
      </Drawer>
    );
  }
}

export default withWidth()(DrawerRequestViewerInner);
