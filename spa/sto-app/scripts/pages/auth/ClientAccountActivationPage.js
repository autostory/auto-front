import React from 'react';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import Relay from 'react-relay/classic';
import isUndefined from 'lodash/isUndefined';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import grey from '@material-ui/core/colors/grey';

import themeDefault from '../../mui-theme/DefaultTheme';
import ConfiguredRadium from '../../radium/ConfiguredRadium';
import relayContainer from '../../../../common-code/decorators/relayContainer';
import AccountActivationMutation from '../../relay/mutation/registration/AccountActivationMutation';

const styles = {
  centeredContainer: {
    minWidth: 320,
    maxWidth: 400,
    height: 'auto',
    position: 'absolute',
    top: '20%',
    left: 0,
    right: 0,
    margin: 'auto',
  },
  paper: {
    padding: 20,
    overflow: 'auto',
  },
  restoreBtn: {
    textAlign: 'center',
  },
  buttonsDiv: {
    textAlign: 'center',
    padding: 10,
  },
  flatButton: {
    color: grey['500'],
  },
};

@ConfiguredRadium
@withRouter
@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id,
          ${AccountActivationMutation.getFragment('viewer')},
      }
  `,
})
class ClientAccountActivationPage extends React.Component {
  static propTypes = {
    history: ReactRouterPropTypes.history,
  };

 static defaultProps = {
    history: null,
  };

  state = {
    pendingRequest: false,
  };

  onActivation = () => {
    if (this.state.pendingRequest) {
      return;
    }

    const { match } = this.props;
    const { token } = match.params;

    if (isUndefined(token)) {
      const { history } = this.props;
      history.push('/');

      return;
    }

    const { viewer } = this.props;

    const accountActivationMutation = new AccountActivationMutation({ token, viewer });

    const onFailure = () => {
      this.setState({
        pendingRequest: false,
      });
      alert('Произошла ошибка при активации. Повторите попытку позже.');
    };

    const onSuccess = (response) => {
      this.setState({
        pendingRequest: false,
      });

      if (!response.activateAccount.activated) {
        alert('Произошла ошибка при активации. Повторите попытку позже.');

        return;
      }
      alert('Аккаунт активирован!.');
      const { history } = this.props;
      history.push('/login');
    };

    const { relay } = this.props;

    this.setState({ pendingRequest: true }, () => {
      relay.commitUpdate(accountActivationMutation, { onFailure, onSuccess });
    });
  };

  render() {
    return (
      <MuiThemeProvider theme={themeDefault}>
        <div style={styles.centeredContainer}>
          <Paper style={styles.paper}>
            <form>
              <h1>Активация аккаунта</h1>
              <p>
                Для того чтобы активировать аккаунт нажмите кнопку ниже.
              </p>

              <div style={styles.restoreBtn}>
                {!this.state.pendingRequest &&
                (<Button
                  variant="raised"
                  primary
                  onClick={this.onActivation}
                >Активировать аккаунт</Button>)
                }

                {this.state.pendingRequest &&
                (<CircularProgress size={35} />)
                }
              </div>
            </form>
          </Paper>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default ClientAccountActivationPage;
