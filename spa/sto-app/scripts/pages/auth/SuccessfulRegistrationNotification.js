import React from 'react';
import Paper from '@material-ui/core/Paper';
import MailOutline from '@material-ui/icons/MailOutline';
import Button from '@material-ui/core/Button';
import validator from 'validator';

import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import themeDefault from '../../mui-theme/DefaultTheme';
import { Link } from 'react-router-dom';
import grey from '@material-ui/core/colors/grey';

const style = {
  container: {
    minWidth: 320,
    maxWidth: 400,
    height: 'auto',
    position: 'absolute',
    top: '20%',
    left: 0,
    right: 0,
    margin: 'auto',
  },
  paper: {
    padding: 20,
    overflow: 'auto',
  },
  form: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },
  icon: {
    minWidth: 80,
    maxWidth: 100,
    height: 'auto',
  },
  buttonsDiv: {
    textAlign: 'center',
    padding: 10,
  },
  flatButton: {
    color: grey['500'],
  },
  email: {
    maxWidth: '100%',
    wordWrap: 'break-word',
    fontWeight: 'bold',
  },
};

class SuccessfulRegistrationNotification extends React.Component {
  render() {
    const searchParams = new URLSearchParams(window.location.search);
    let text = searchParams.get('email');
    if (!validator.isEmail(searchParams.get('email'))) {
      text = '';
    }

    return (
      <MuiThemeProvider theme={themeDefault}>
        <div style={style.container}>
          <Paper style={style.paper}>
            <form style={style.form}>
              <MailOutline style={style.icon} />
              <h1>Регистрация прошла успешно!</h1>
              <p>
                Мы отправили письмо с данными для авторизации<br /> на Ваш e-mail:<br />
              </p>
              <p style={style.email}>{text}</p>
              <p>
                Если сообщение не пришло в течении 10 минут,<br /> обратитесь в техническую поддержку
              </p>
            </form>
          </Paper>
          <div style={style.buttonsDiv}>
            <div style={style.buttonsDiv}>
              <Button
                to="/login"
                color="primary"
                component={Link}
                style={style.flatButton}
              >
                На страницу авторизации
              </Button>
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default SuccessfulRegistrationNotification;