import React from 'react';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import AuthenticationClient from '../../../../common-code/utils/AuthenticationClient';
import ConfiguredRadium from '../../radium/ConfiguredRadium';
import FullScreenPreLoader from './../../components/basic/FullScreenPreLoader';

@ConfiguredRadium
@withRouter
class LogoutPage extends React.Component {
  static propTypes = {
    history: ReactRouterPropTypes.history,
  };

  static defaultProps = {
    history: null,
  };

  componentDidMount = () => {
    const { history } = this.props;
    AuthenticationClient.logout();
    history.push('/login');
    window.location.reload();
  };

  render() {
    return <FullScreenPreLoader />;
  }
}

export default LogoutPage;
