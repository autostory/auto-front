/* eslint-disable react/prop-types */
import React from 'react';
import Relay from 'react-relay/classic';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import validator from 'validator';
import isFunction from 'lodash/isFunction';
import find from 'lodash/find';
import isUndefined from 'lodash/isUndefined';
import keys from 'lodash/keys';
import mapKeys from 'lodash/mapKeys';
import { withRouter } from 'react-router';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import grey from '@material-ui/core/colors/grey';
import Checkbox from '@material-ui/core/Checkbox';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import classNames from 'classnames';

import ConfiguredRadium from '../../radium/ConfiguredRadium';
import relayContainer from '../../../../common-code/decorators/relayContainer';
import ClientRegistrationMutation from '../../relay/mutation/registration/ClientRegistrationMutation';
import FirmRegistrationMutation from '../../relay/mutation/registration/FirmRegistrationMutation';
import themeDefault from '../../mui-theme/DefaultTheme';
import MaskedTextField from '../../components/basic/input/MaskedTextField';
import parseMutationInputValidationErrorMessage from '../../../../common-code/utils/parseMutationInputValidationErrorMessage';

const styles = theme => ({
  textInput: {
    marginTop: theme.spacing.unit,
  },
  requiredFieldDescription: {
    textAlign: 'left',
    marginTop: theme.spacing.unit,
  },
});

const rawStyle = {
  container: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },
  paperContainer: {
    marginTop: 30,
    maxWidth: 400,
    minWidth: '30vw',
    minHeight: '50vh',
  },
  paper: {
    padding: 20,
    overflow: 'auto',
  },
  buttonsDiv: {
    textAlign: 'center',
    padding: 10,
  },
  flatButton: {
    color: grey['500'],
  },
  checkRemember: {
    style: {
      float: 'left',
      maxWidth: 180,
      paddingTop: 5,
    },
    labelStyle: {},
    iconStyle: {},
  },
  loginBtnContainer: {
    textAlign: 'center',
    marginTop: 20,
  },
  checkBox: {
    marginTop: 10,
  },
};

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id,
          ${ClientRegistrationMutation.getFragment('viewer')},
          ${FirmRegistrationMutation.getFragment('viewer')},
      }
  `,
})
@withRouter
@ConfiguredRadium
@withStyles(styles)
class RegistrationPage extends React.Component {
  state = {
    email: '',
    password: '',
    passwordConfirm: '',
    firmName: '',
    mobilePhone: '',
    firmRegistration: true,
    managerMobilePhone: '',
    registrationFinish: false,
    requestPending: false,
    submitButtonPressed: false,
    errors: [],
    errorsFromBackend: [],
    errorSubmit: false,
    showPassword: false,
  };

  errorTexts = () => {
    const errorTexts = {
      emailErrorText: '',
      mobilePhoneErrorText: '',
      managerPhoneErrorText: '',
      passwordErrorText: '',
      passwordConfirmErrorText: '',
    };

    const emailErrorFromBackend = find(this.state.errorsFromBackend, { fieldName: 'email' });
    const phoneErrorFromBackend = find(this.state.errorsFromBackend, { fieldName: 'mobilePhone' });

    const passwordErrorFromBackend = find(this.state.errorsFromBackend, { fieldName: 'password' });
    const passwordConfirmErrorFromBackend = find(this.state.errorsFromBackend, { fieldName: 'passwordConfirm' });

    if (this.state.submitButtonPressed && this.state.errors.email) {
      errorTexts.emailErrorText = this.state.errors.email;
    }

    if (this.state.submitButtonPressed && this.state.errors.phoneNumber) {
      errorTexts.mobilePhoneErrorText = this.state.errors.phoneNumber;
    }

    if (this.state.submitButtonPressed && this.state.errors.managerPhoneNumber) {
      errorTexts.managerPhoneErrorText = this.state.errors.managerPhoneNumber;
    }

    if (!isUndefined(emailErrorFromBackend) && emailErrorFromBackend.value === this.state.email) {
      errorTexts.emailErrorText = emailErrorFromBackend.text;
    }

    if (this.state.submitButtonPressed && this.state.errors.mobilePhone) {
      errorTexts.mobilePhoneErrorText = this.state.errors.mobilePhone;
    }

    if (!isUndefined(phoneErrorFromBackend) && phoneErrorFromBackend.value === this.state.mobilePhone) {
      errorTexts.mobilePhoneErrorText = phoneErrorFromBackend.text;
    }

    if (this.state.submitButtonPressed && this.state.errors.password) {
      errorTexts.passwordErrorText = this.state.errors.password;
    }

    if (!isUndefined(passwordErrorFromBackend) && passwordErrorFromBackend.value === this.state.password) {
      errorTexts.passwordErrorText = passwordErrorFromBackend.text;
    }

    if (this.state.submitButtonPressed && this.state.errors.passwordConfirm) {
      errorTexts.passwordConfirmErrorText = this.state.errors.passwordConfirm;
    }

    if (
      !isUndefined(passwordConfirmErrorFromBackend)
      && passwordConfirmErrorFromBackend.value === this.state.passwordConfirm
    ) {
      errorTexts.passwordConfirmErrorText = passwordConfirmErrorFromBackend.text;
    }

    return errorTexts;
  };

  hideErrorMessage = () => {
    this.setState({ errorSubmit: false });
  };

  onTypeChange = (event, isInputChecked) => {
    this.setState({
      firmRegistration: isInputChecked,
    });
  };

  onEmailInput = (event) => {
    const { value } = event.target;

    this.setState({
      email: value,
    }, this.validate);
  };

  handleClickShowPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  onPhoneInput = (event) => {
    this.setState({
      mobilePhone: event.target.value,
    }, this.validate);
  };

  onManagerPhoneInput = (event, filteredValue) => {
    this.setState({
      managerMobilePhone: filteredValue,
    }, this.validate);
  };

  onPasswordInput = (event) => {
    const { value } = event.target;

    this.setState({
      password: value,
    }, this.validate);
  };

  onPasswordConfirmInput = (event) => {
    const { value } = event.target;

    this.setState({
      passwordConfirm: value,
    }, this.validate);
  };

  onFirmNameInput = (event) => {
    const { value } = event.target;

    this.setState({
      firmName: value,
    }, this.validate);
  };

  clearFormState = () => {
    this.setState({
      email: '',
      password: '',
      passwordConfirm: '',
      firmName: '',
      mobilePhone: '',
      managerMobilePhone: '',
      submitButtonPressed: false,
    });
  };

  registerAsClient = () => {
    if (this.state.requestPending) {
      return;
    }

    const {
      email, password, passwordConfirm, mobilePhone,
    } = this.state;
    const { viewer, relay } = this.props;

    const input = {
      email,
      mobilePhone,
      newPassword: password,
      newPasswordConfirm: passwordConfirm,
    };

    const clientRegistrationMutation = new ClientRegistrationMutation({ input, viewer });

    const onFailure = () => {
      this.setState({
        requestPending: false,
        errorSubmit: true,
      });
    };

    const onSuccess = (data) => {
      this.setState({
        requestPending: false,
      });
      const { messages } = data.clientRegistration.errors;

      if (messages.length) {
        const errorsFromBackend = parseMutationInputValidationErrorMessage(messages);
        const errorsFromBackendWithValues = [];
        mapKeys(errorsFromBackend, (item, fieldName) => {
          const res = item.join(' ');
          if (fieldName === 'email') {
            errorsFromBackendWithValues.push({ value: this.state.email, text: res, fieldName });
          } else if (fieldName === 'newPassword') {
            errorsFromBackendWithValues.push({ value: this.state.password, text: res, fieldName });
          } else if (fieldName === 'newPasswordConfirm') {
            errorsFromBackendWithValues.push({ value: this.state.password, text: res, fieldName });
          } else if (fieldName === 'mobilePhone') {
            errorsFromBackendWithValues.push({ value: this.state.mobilePhone, text: res, fieldName });
          }

          return false;
        });

        this.setState({ errorsFromBackend: errorsFromBackendWithValues });

        return;
      }

      this.setState({
        registrationFinish: true,
        successRegistrationEmail: this.state.email,
      });
      this.props.history.push(`/registration-successful?email=${this.state.email}`);
      this.clearFormState();
    };

    this.setState({ requestPending: true }, () => {
      relay.commitUpdate(clientRegistrationMutation, { onFailure, onSuccess });
    });
  };

  validate = (_callback) => {
    const errors = [];
    const {
      email,
      password,
      passwordConfirm,
      mobilePhone, // это контактный номер фирмы
      managerMobilePhone,
      firmName,
      firmRegistration,
      submitButtonPressed,
    } = this.state;

    if (email === '') {
      errors.email = 'Введите e-mail';
    } else if (!validator.isEmail(email)) {
      errors.email = 'Укажите E-mail';
    }

    if (mobilePhone.trim() === '') {
      errors.password = 'Укажите номер телефона для активации';
    }

    const managerMobilePhoneMatch = managerMobilePhone.match(/\d+/g);
    if (firmRegistration && (managerMobilePhoneMatch === null || !validator.isMobilePhone(managerMobilePhoneMatch.join(''), 'ru-RU', true))) {
      errors.managerPhoneNumber = 'Укажите номер мобильного телефона';
    }

    if (password.length < 8) {
      errors.password = 'Длина пароля должна быть не менее 8 символов';
    }

    if (submitButtonPressed && passwordConfirm === '') {
      errors.passwordConfirm = 'Поле обязательно для заполнения';
    }

    if (password.toLocaleLowerCase() === password) {
      errors.password = 'Пароль должен содержать заглавные и строчные символы';
    }

    if (password && password.match(/\d+/) === null) {
      errors.password = 'Пароль должен содержать хотябы одну цифру';
    }

    if (password.toUpperCase() === password) {
      errors.password = 'Пароль должен содержать заглавные и строчные символы';
    }

    if (password !== passwordConfirm) {
      errors.passwordConfirm = 'пароль и подтверждение пароля не совпадают';
      errors.password = 'пароль и подтверждение пароля не совпадают';
    }

    if (mobilePhone === '') {
      errors.phoneNumber = 'Поле обязательно для заполнения';
    }

    if (firmRegistration && firmName === '') {
      errors.firmName = 'Поле обязательно для заполнения';
    }

    let callback;
    if (isFunction(_callback)) {
      callback = _callback;
    }

    this.setState({ errors }, callback);
  };

  registerAsFirm = () => {
    if (this.state.requestPending) {
      return;
    }

    const {
      email, password, passwordConfirm, firmName, mobilePhone, managerMobilePhone,
    } = this.state;
    const { viewer, relay } = this.props;

    const input = {
      email,
      newPassword: password,
      newPasswordConfirm: passwordConfirm,
      firmName,
      phone: mobilePhone,
      managerMobilePhone,
    };

    const firmRegistrationMutation = new FirmRegistrationMutation({ input, viewer });

    const onFailure = () => {
      this.setState({
        requestPending: false,
        errorSubmit: true,
      });
    };

    const onSuccess = (data) => {
      this.setState({
        requestPending: false,
      });
      const { messages } = data.firmRegistration.errors;

      if (messages.length) {
        const errorsFromBackend = parseMutationInputValidationErrorMessage(messages);
        const errorsFromBackendWithValues = [];
        mapKeys(errorsFromBackend, (item, fieldName) => {
          const res = item.join(' ');
          if (fieldName === 'email') {
            errorsFromBackendWithValues.push({ value: this.state.email, text: res, fieldName });
          } else if (fieldName === 'newPassword') {
            errorsFromBackendWithValues.push({ value: this.state.password, text: res, fieldName });
          } else if (fieldName === 'newPasswordConfirm') {
            errorsFromBackendWithValues.push({ value: this.state.password, text: res, fieldName });
          } else if (fieldName === 'phone') {
            errorsFromBackendWithValues.push({ value: this.state.mobilePhone, text: res, fieldName });
          } else if (fieldName === 'managerMobilePhone') {
            errorsFromBackendWithValues.push({ value: this.state.managerMobilePhone, text: res, fieldName });
          }

          return false;
        });

        this.setState({ errorsFromBackend: errorsFromBackendWithValues });

        return;
      }

      this.setState({
        registrationFinish: true,
      });

      this.clearFormState();
    };

    this.setState({ requestPending: true }, () => {
      relay.commitUpdate(firmRegistrationMutation, { onFailure, onSuccess });
    });
  };

  handleRegisterButtonClick = () => {
    this.validate(() => {
      this.setState({
        submitButtonPressed: true,
      });
      if (keys(this.state.errors).length) {
        return;
      }

      this.setState({
        requestPending: true,
      });

      const { firmRegistration } = this.state;

      if (!firmRegistration) {
        this.registerAsClient();
        return;
      }

      this.registerAsFirm();
    });
  };

  render() {
    let passwordTypes = 'password';
    if (this.state.showPassword === true) {
      passwordTypes = 'text';
    }
    const {
      emailErrorText,
      mobilePhoneErrorText,
      passwordErrorText,
      passwordConfirmErrorText,
      managerPhoneErrorText,
    } = this.errorTexts();
    const { classes } = this.props;

    return (
      <MuiThemeProvider theme={themeDefault}>
        <div style={rawStyle.container}>
          <Snackbar
            open={this.state.errorSubmit}
            message="Произошла ошибка при регистрации. Повторите попытку позже."
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                className={classes.close}
                onClick={this.hideErrorMessage}
              >
                <CloseIcon />
              </IconButton>,
            ]}
            autoHideDuration={6000}
            onClose={this.hideErrorMessage}
          />
          <div style={rawStyle.paperContainer}>
            <Paper style={rawStyle.paper}>
              {!this.state.registrationFinish &&
              <form autoComplete="off">
                <Typography variant="headline">Регистрация нового автосервиса</Typography>
                {/*<FormControlLabel*/}
                {/*style={rawStyle.checkBox}*/}
                {/*control={*/}
                {/*<Checkbox*/}
                {/*color="primary"*/}
                {/*checked={this.state.firmRegistration}*/}
                {/*disabled={this.state.requestPending}*/}
                {/*onChange={this.onTypeChange}*/}
                {/*/>*/}
                {/*}*/}
                {/*label="Как владелец автосервиса"*/}
                {/*/>*/}

                <Paper style={{ padding: 24, marginBottom: 12, marginTop: 12 }} elevation={1}>
                  <Typography variant="subheading">Информация о СТО</Typography>

                  {this.state.firmRegistration &&
                  <div>
                    <TextField
                      required
                      inputProps={{
                        maxLength: 250,
                      }}
                      error={this.state.submitButtonPressed && Boolean(this.state.errors.firmName)}
                      className={classes.textInput}
                      autoComplete="off"
                      label="Название автосервиса"
                      helperText={this.state.submitButtonPressed && this.state.errors.firmName}
                      type="text"
                      disabled={this.state.requestPending}
                      fullWidth
                      value={this.state.firmName}
                      onChange={this.onFirmNameInput}
                    />
                  </div>
                  }
                  <br />
                  <Typography>
                    Укажите контактный номер телефона, по которому позвонит
                    наш менеджер для активации аккаунта фирмы и аккаунта владельца СТО
                  </Typography>
                  <TextField
                    required
                    inputProps={{
                      maxLength: 50,
                    }}
                    error={mobilePhoneErrorText !== ''}
                    className={classes.textInput}
                    autoComplete="off"
                    label="Контактный номер для активации"
                    helperText={this.state.submitButtonPressed && mobilePhoneErrorText}
                    type="text"
                    disabled={this.state.requestPending}
                    fullWidth
                    value={this.state.mobilePhone}
                    onChange={this.onPhoneInput}
                  />
                </Paper>

                <Paper style={{ padding: 24 }} elevation={1}>
                  <Typography variant="subheading">Данные для аккаунта владельца СТО</Typography>

                  <TextField
                    required
                    inputProps={{
                      maxLength: 250,
                    }}
                    error={emailErrorText !== ''}
                    className={classes.textInput}
                    autoComplete="off"
                    label="E-mail"
                    helperText={emailErrorText}
                    fullWidth
                    disabled={this.state.requestPending}
                    value={this.state.email}
                    onChange={this.onEmailInput}
                  />
                  <MaskedTextField
                    required
                    error={managerPhoneErrorText !== ''}
                    className={classes.textInput}
                    autoComplete="off"
                    label="Телефон"
                    helperText={this.state.submitButtonPressed && managerPhoneErrorText}
                    fullWidth
                    mask="(999) 999 99 99"
                    disabled={this.state.requestPending}
                    value={this.state.managerMobilePhone}
                    onChange={this.onManagerPhoneInput}
                    placeholder="(999) 999 99 99"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          {'+7 '}
                        </InputAdornment>
                      ),
                    }}
                  />
                  <br /><br />
                  <FormControl
                    required
                    className={classNames(classes.margin, classes.textField)}
                    fullWidth
                    error={this.state.submitButtonPressed && Boolean(this.state.errors.password)}
                  >
                    <InputLabel htmlFor="password">Пароль</InputLabel>
                    <Input
                      id="password"
                      inputProps={{
                        maxLength: 250,
                      }}
                      required
                      className={classes.textInput}
                      autoComplete="off"
                      label="Пароль"
                      type={passwordTypes}
                      disabled={this.state.requestPending}
                      fullWidth
                      value={this.state.password}
                      onChange={this.onPasswordInput}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="Toggle password visibility"
                            onClick={this.handleClickShowPassword}
                            onMouseDown={this.handleMouseDownPassword}
                          >
                            {this.state.showPassword && <VisibilityOff />}
                            {!this.state.showPassword && <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    <FormHelperText id="password">{passwordErrorText}</FormHelperText>
                  </FormControl>
                  <FormControl
                    required
                    className={classNames(classes.margin, classes.textField)}
                    fullWidth
                    error={this.state.submitButtonPressed && Boolean(this.state.errors.passwordConfirm)}
                  >
                    <InputLabel htmlFor="passwordConfirm">Подтверждение пароля</InputLabel>
                    <Input
                      id="passwordConfirm"
                      inputProps={{
                        maxLength: 250,
                      }}
                      required
                      className={classes.textInput}
                      autoComplete="off"
                      label="Подтверждение пароля"
                      type={passwordTypes}
                      disabled={this.state.requestPending}
                      fullWidth
                      value={this.state.passwordConfirm}
                      onChange={this.onPasswordConfirmInput}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="Toggle password visibility"
                            onClick={this.handleClickShowPassword}
                            onMouseDown={this.handleMouseDownPassword}
                          >
                            {this.state.showPassword && <VisibilityOff />}
                            {!this.state.showPassword && <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    <FormHelperText id="password">{passwordConfirmErrorText}</FormHelperText>
                  </FormControl>
                </Paper>


                {!this.state.registrationFinish &&
                <div style={rawStyle.loginBtnContainer}>
                  {
                    !this.state.requestPending && (
                      <Button
                        variant="raised"
                        color="primary"
                        onClick={this.handleRegisterButtonClick}
                      >
                        Зарегистрироваться
                      </Button>
                    )
                  }
                  {
                    this.state.requestPending &&
                    (<CircularProgress size={35} />)
                  }
                  <p className={classes.requiredFieldDescription}>* - поля обязательные для заполнения</p>
                </div>}
              </form>}
              {this.state.registrationFinish && !this.state.firmRegistration &&
              <div>
                <h1>Успешная регистрация.</h1>
                <p>
                  На указанный вами e-mail <b>{this.state.successRegistrationEmail}</b> было направлено письмо.
                  <br /><br />
                  Перейдите по ссылке из письма для активации аккаунта.
                  <br /><br />
                  После активации вы сможете авторизоваться используя указанный вами e-mail и пароль.
                </p>
              </div>
              }
              {this.state.registrationFinish && this.state.firmRegistration &&
              <div>
                <h1>Успешная регистрация.</h1>
                <p>
                  Ваша заявка на регистрацию принята.<br />
                  С вами свяжется наш менеджер для активации аккаунта.
                </p>
              </div>
              }
            </Paper>

            <div style={rawStyle.buttonsDiv}>
              <div style={rawStyle.buttonsDiv}>
                <Button
                  to="/login"
                  color="primary"
                  component={Link}
                  style={rawStyle.flatButton}
                >
                  На страницу авторизации
                </Button>
              </div>
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default RegistrationPage;
