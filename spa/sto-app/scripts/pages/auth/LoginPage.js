import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import isNull from 'lodash/isNull';
import Relay from 'react-relay/classic';
import ReactRouterPropTypes from 'react-router-prop-types';
import { withStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import PersonAdd from '@material-ui/icons/PersonAdd';
import Help from '@material-ui/icons/Help';
import TextField from '@material-ui/core/TextField';

import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import CircularProgress from '@material-ui/core/CircularProgress';
import grey from '@material-ui/core/colors/grey';

import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import classNames from 'classnames';
import InputLabel from '@material-ui/core/InputLabel';

import AuthenticationClient from '../../../../common-code/utils/AuthenticationClient';
import ConfiguredRadium from '../../radium/ConfiguredRadium';
import {
  setLogin,
  setPassword,
  setRequestPending,
  setError,
} from '../../reduxActions/LoginFormActions';
import relayContainer from '../../../../common-code/decorators/relayContainer';
import LoginMutation from '../../relay/mutation/LoginMutation';
import themeDefault from '../../mui-theme/DefaultTheme';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  textInput: {
    marginTop: theme.spacing.unit,
  },
  close: {
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4,
  },
});

const rawStyle = {
  container: {
    minWidth: 320,
    maxWidth: 400,
    height: 'auto',
    position: 'absolute',
    top: '20%',
    left: 0,
    right: 0,
    margin: 'auto',
  },
  paper: {
    padding: 20,
    overflow: 'auto',
  },
  buttonsDiv: {
    textAlign: 'center',
    padding: 10,
  },
  flatButton: {
    color: grey['500'],
  },
  checkRemember: {
    style: {
      float: 'left',
      maxWidth: 180,
      paddingTop: 5,
    },
    labelStyle: {},
    iconStyle: {},
  },
  loginBtnContainer: {
    textAlign: 'center',
    marginTop: 20,
  },
};

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id,
          ${LoginMutation.getFragment('viewer')},
      }
  `,
})
@connect(state => ({
  login: state.loginFormStore.login,
  password: state.loginFormStore.password,
  requestPending: state.loginFormStore.requestPending,
  error: state.loginFormStore.error,
}))
@ConfiguredRadium
@withRouter
@withStyles(styles)
class LoginPage extends React.Component {
  static propTypes = {
    history: ReactRouterPropTypes.history,
    location: ReactRouterPropTypes.location,
  };

  static defaultProps = {
    history: null,
    location: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      login: props.login,
      password: props.password,
      showPassword: false,
    };
  }

  onLoginInput = event => {
    this.setState({
      login: event.target.value,
    });
    this.props.dispatch(setLogin(event.target.value));
  };

  onPasswordInput = event => {
    this.setState({
      password: event.target.value,
    });
    this.props.dispatch(setPassword(event.target.value));
  };

  onInputKeyUp = (event) => {
    if (event.key === 'Enter') {
      this.handleLoginButtonClick();
    }
  };

  handleLoginButtonClick = () => {

    if (this.state.login === '' || this.state.password === '') {
      this.props.dispatch(setError(true));
      return;
    }

    this.props.dispatch(setRequestPending(true));

    const { history, location } = this.props;

    let successURL = '/';
    const query = new URLSearchParams(location.search);
    if (!isNull(query.get('redirectFrom'))) {
      successURL = query.get('redirectFrom');
    }

    // TODO: Переписать на relay
    AuthenticationClient.authenticate(
      this.state.login,
      this.state.password,
      () => {
        history.push(successURL);
      },
      () => {
        this.props.dispatch(setError(true));
        this.props.dispatch(setRequestPending(false));
      },
    );
  };

  hideErrorMessage = () => {
    this.props.dispatch(setError(false));
  };

  handleClickShowPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  render() {
    let errorMessage = 'Не правильный E-mail/телефон или пароль';
    if (this.state.login === '' || this.state.password === '') {
      errorMessage = 'Введите E-mail/телефон и пароль';
    }
    const { classes, error, requestPending } = this.props;
    const { login, password } = this.state;

    let passwordTypes = 'password';
    if (this.state.showPassword === true) {
      passwordTypes = 'text';
    }

    return (
      <MuiThemeProvider theme={themeDefault}>
        <div>
          <Snackbar
            open={error}
            message={errorMessage}
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                className={classes.close}
                onClick={this.hideErrorMessage}
              >
                <CloseIcon />
              </IconButton>,
            ]}
            autoHideDuration={2000}
            onClose={this.hideErrorMessage}
          />
          <div>
            <div style={rawStyle.container}>
              <Paper style={rawStyle.paper}>
                <form autoComplete="off">
                  <h1>Авторизация</h1>
                  <TextField
                    label="E-mail или телефон"
                    className={classes.textInput}
                    autoComplete="off"
                    fullWidth
                    inputProps={{
                      maxLength: 250,
                    }}
                    disabled={requestPending}
                    value={login}
                    onChange={this.onLoginInput}
                    onKeyUp={this.onInputKeyUp}
                  />
                  <FormControl
                    className={classNames(classes.margin, classes.textField)}
                    fullWidth
                  >
                    <InputLabel htmlFor="password">Пароль</InputLabel>
                    <Input
                      id="password"
                      required
                      inputProps={{
                        maxLength: 250,
                      }}
                      className={classes.textInput}
                      autoComplete="off"
                      label="Пароль"
                      fullWidth
                      type={passwordTypes}
                      disabled={requestPending}
                      value={password}
                      onChange={this.onPasswordInput}
                      onKeyUp={this.onInputKeyUp}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="Toggle password visibility"
                            onClick={this.handleClickShowPassword}
                            onMouseDown={this.handleMouseDownPassword}
                          >
                            {this.state.showPassword && <VisibilityOff />}
                            {!this.state.showPassword && <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                  </FormControl>
                  <div style={rawStyle.loginBtnContainer}>
                    {!requestPending && (
                      <Button
                        variant="raised"
                        color="primary"
                        onClick={this.handleLoginButtonClick}
                      >
                        Войти
                      </Button>
                    )}
                    {requestPending && (
                      <CircularProgress size={35} />
                    )}
                  </div>
                </form>
              </Paper>

              <div style={rawStyle.buttonsDiv}>
                <Button
                  component={Link}
                  to="/register"
                  className={classes.button}
                  style={rawStyle.flatButton}
                >
                  <PersonAdd className={classes.leftIcon} /> Регистрация
                </Button>

                <Button
                  color="primary"
                  component={Link}
                  to="/password-restore"
                  className={classes.button}
                  style={rawStyle.flatButton}
                >
                  <Help className={classes.leftIcon} /> Я забыл пароль
                </Button>
              </div>
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default LoginPage;
