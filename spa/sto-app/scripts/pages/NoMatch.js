import React, { Component } from 'react';
import ConfiguredRadium from './../radium/ConfiguredRadium';

@ConfiguredRadium
class NoMatch extends Component {
  render() {
    return (
      <div>
        <h1>404! Нет такой страницы.</h1>
      </div>
    );
  }
}

export default NoMatch;
