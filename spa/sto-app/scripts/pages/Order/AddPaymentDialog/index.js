import React from 'react';
import Relay from 'react-relay/classic';
import { connect } from 'react-redux';
import { submit as ReduxFormSubmit, isSubmitting, isInvalid } from 'redux-form';

import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Slide from '@material-ui/core/Slide';
import isFunction from 'lodash/isFunction';

import submit from './../../../components/Form/AddOrderPaymentForm/submit';
import AddOrderPaymentForm, { FORM_NAME } from './../../../components/Form/AddOrderPaymentForm';
import AddOrderPaymentMutation from './../../../relay/mutation/payment/AddOrderPaymentMutation';

import relayContainer from '../../../../../common-code/decorators/relayContainer';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

@connect(state => ({
  invalid: isInvalid(FORM_NAME)(state),
  submitting: isSubmitting(FORM_NAME)(state),
}))
@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          id
          totalBill
          paymentsSum
          client {
              points
          }
          ${AddOrderPaymentMutation.getFragment('order')}
      }
  `,
})
class AddPaymentDialog extends React.PureComponent {
  static defaultProps = {
    dispatch: () => {
    },
    afterAdd: () => {
    },
  };

  static propTypes = {
    dispatch: PropTypes.func,
    onRequestClose: PropTypes.func.isRequired,
    afterAdd: PropTypes.func,
    open: PropTypes.bool.isRequired,
  };

  onClose = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onAddButtonClick = () => {
    this.props.dispatch(ReduxFormSubmit(FORM_NAME));
  };

  onSubmit = (values) => {
    const { order } = this.props;

    return submit(order, values, this.afterSave);
  };

  afterSave = () => {
    const { onRequestClose, afterAdd } = this.props;

    if (isFunction(afterAdd)) {
      afterAdd();
    }

    onRequestClose();
  };

  render() {
    const {
      open, onRequestClose, submitting, invalid, order,
    } = this.props;
    const debt = order.totalBill - order.paymentsSum;

    return (
      <Dialog
        fullScreen={false}
        TransitionComponent={Transition}
        onClose={onRequestClose}
        open={open}
        maxWidth="md"
        disableBackdropClick={submitting}
        disableEscapeKeyDown={submitting}
      >
        <DialogTitle>
          Добавление оплаты заказу
        </DialogTitle>

        <DialogContent>
          <AddOrderPaymentForm
            onSubmit={this.onSubmit}
            availablePoints={order.client.points}
            totalBill={order.totalBill}
            paymentsSum={order.paymentsSum}
            initialValues={{ amount: debt }}
          />
        </DialogContent>

        <DialogActions>
          <Button
            onClick={this.onClose}
            disabled={submitting}
          >
            Отмена
          </Button>
          <Button
            onClick={this.onAddButtonClick}
            color="primary"
            variant="contained"
            disabled={submitting || invalid}
          >
            Провести платеж {submitting && <CircularProgress size={15} />}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default AddPaymentDialog;
