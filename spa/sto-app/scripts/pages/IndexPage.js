import React, { Component } from 'react';
import ConfiguredRadium from './../radium/ConfiguredRadium';
import PageBase from '../components/main/PageBase';
import Statistics from './stat/Statistics';

@ConfiguredRadium
class IndexPage extends Component {
  render() {
    return (
      <PageBase title="Статистика">
        <Statistics />
      </PageBase>
    );
  }
}

export default IndexPage;
