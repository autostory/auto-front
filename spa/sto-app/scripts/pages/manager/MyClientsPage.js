import React, { Component } from 'react';
import ConfiguredRadium from './../../radium/ConfiguredRadium';

import PageBase from '../../components/main/PageBase';
import MyClientsList from '../../components/manager/MyClientsList/MyClientsList';

@ConfiguredRadium
class MyClientsPage extends Component {
  render() {
    return (
      <PageBase title="Мои клиенты" withPaper={false}>
        <MyClientsList />
      </PageBase>
    );
  }
}

export default MyClientsPage;
