import React from 'react';
import {
  cyan600,
  pink600,
  purple600,
  orange600,
  green600,
  lime600,
  amber600,
  deepOrange600,
  red600,
} from '@material-ui/core/styles/colors';
import Assessment from '@material-ui/icons/action/assessment';
import Face from '@material-ui/icons/action/face';
import ThumbDown from '@material-ui/icons/action/thumb-down';
import ShoppingCart from '@material-ui/icons/editor/monetization-on';

import ExpandLess from '@material-ui/icons/navigation/expand-less';
import ExpandMore from '@material-ui/icons/navigation/expand-more';

import InfoBox from './../../components/manager/dashboard/InfoBox';
import NewOrders from './../../components/manager/dashboard/NewOrders';
import MonthlySales from './../../components/manager/dashboard/MonthlySales';
import BrowserUsage from './../../components/manager/dashboard/BrowserUsage';
import RecentlyProducts from './../../components/manager/dashboard/RecentlyProducts';
import globalStyles from '../../../../common-code/globalStyles';

const DashboardPage = () => (
  <div>
    <h3 style={globalStyles.navigation}>Главная / Рабочий стол</h3>

    <div className="row">
      <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
        <InfoBox
          Icon={ShoppingCart}
          color={cyan600}
          title="Выручка"
          value="+45.640р"
        />
      </div>

      <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
        <InfoBox
          Icon={ThumbDown}
          color={pink600}
          title="Негативных отзывов"
          value="5"
        />
      </div>

      <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
        <InfoBox
          Icon={Assessment}
          color={purple600}
          title="Записей на сегодня"
          value="12"
        />
      </div>

      <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
        <InfoBox
          Icon={Face}
          color={orange600}
          title="Всего клиентов"
          value="248 (+12)"
        />
      </div>
    </div>

    <div className="row">
      <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-md m-b-15">
        <NewOrders
          data={[
            { pv: 2400 },
            { pv: 1398 },
            { pv: 9800 },
            { pv: 3908 },
            { pv: 4800 },
            { pv: 3490 },
            { pv: 4300 },
          ]}
        />
      </div>

      <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 m-b-15">
        <MonthlySales
          data={[
            { name: 'янв', uv: 3700 },
            { name: 'фев', uv: 5600 },
            { name: 'мар', uv: 5800 },
            { name: 'апр', uv: 2780 },
            { name: 'май', uv: 2000 },
            { name: 'июн', uv: 1800 },
            { name: 'июл', uv: 2600 },
            { name: 'авг', uv: 2900 },
            { name: 'сен', uv: 3500 },
            { name: 'окт', uv: 3000 },
            { name: 'ноя', uv: 2400 },
            { name: 'дек', uv: 2780 },
          ]}
        />
      </div>
    </div>

    <div className="row">
      <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
        <RecentlyProducts
          data={[
            { id: 1, title: '10:00 Сергеев Иван', text: 'Лада Гранта. Замена масла.' },
            { id: 2, title: '13:20 Константин Киркоров', text: 'Honda CR-V. Стучат стойки без прогрева.' },
            { id: 3, title: '14:00 Адель Петровна', text: 'Ford Escape. Проверка всех жидкостей, замена масла.' },
            { id: 4, title: '15:00 Илья Макаров', text: 'Mazda 6. Подозрение на необходимость капиталить движку.' },
          ]}
        />
      </div>

      <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
        <BrowserUsage
          data={[
            { name: '5', value: 600, color: green600, icon: <ExpandMore /> },
            { name: '4', value: 200, color: lime600, icon: <ExpandMore /> },
            { name: '3', value: 133, color: amber600, icon: <ExpandMore /> },
            { name: '2', value: 456, color: deepOrange600, icon: <ExpandLess /> },
            { name: '1', value: 34, color: red600, icon: <ExpandLess /> },
          ]}
        />
      </div>
    </div>
  </div>
);

export default DashboardPage;
