import React, { Component } from 'react';
import ConfiguredRadium from '../../radium/ConfiguredRadium';
import CarInfo from '../../components/manager/CarInfo/CarInfo';
import PageBase from '../../components/main/PageBase';

@ConfiguredRadium
class CarInfoPage extends Component {
  render() {
    const { match } = this.props;

    return (
      <PageBase title="Карточка автомобиля" >
        <CarInfo carId={match.params.id} />
      </PageBase >
    );
  }
}

export default CarInfoPage;
