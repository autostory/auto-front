import { format } from 'date-fns';
import get from 'lodash/get';
import React from 'react';
import map from 'lodash/map';
import isEmpty from 'lodash/isEmpty';
import isString from 'lodash/isString';
import Link from 'react-router-dom/Link';
import ReactDOM from 'react-dom';
import withProps from 'recompose/withProps';

import Table from '@material-ui/core/Table';
import Typography from '@material-ui/core/Typography';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import Relay from 'react-relay/classic';

import { getPaymentTypeName } from '../../../../../../common-code/constants/paymentTypes';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';
import { fromGlobalId } from '../../../../../../common-code/relay/globalIdUtils';
import OrderPaymentFilter from './OrderPaymentFilter';

const rawStyle = {
  paper: {
    position: 'relative',
    overflow: 'auto',
    maxHeight: 'calc(100vh - 85px)',
    height: 'calc(100vh - 85px)',
  },
  center: {
    textAlign: 'center',
  },
  amount: {
    whiteSpace: 'nowrap',
  },
  note: {
    maxWidth: 400,
    wordWrap: 'break-word',
  },
  bottom: {
    display: 'flex',
    justifyContent: 'center',
    padding: 11,
    position: 'absolute',
    bottom: 20,
    right: '50%',
    marginRight: -100,
  },
};

const PER_PAGE = 20;

@withProps({ renderLoading: () => <div>Загрузка списка транзакций <CircularProgress size={25} /></div> })
@relaySuperContainer(
  {
    viewer: () => Relay.QL`
        fragment on Viewer {
            myFirm {
                ${OrderPaymentFilter.getFragment('firm')}
                orderPayments(
                    clientId: $clientId,
                    orderIdPart: $orderIdPart,
                    cashierId: $cashierId,
                    paymentType: $paymentType,
                    first: $first,
                    after: $after,
                ) {
                    edges {
                        node {
                            id
                            note
                            makeAt {
                                timestamp
                            }
                            cashier {
                                id
                                fullName
                            }
                            order {
                                id
                            }
                            client {
                                id
                                fullName
                            }
                            amount
                            paymentType
                        }
                    }
                    pageInfo {
                        startCursor
                        endCursor
                        hasNextPage
                        hasPreviousPage
                    }
                }
            }
        }
    `,
  },
  {
    clientId: null,
    cashierId: null,
    paymentType: null,
    orderIdPart: null,
    first: PER_PAGE,
    after: null,
  },
)
class OrderPaymentsTable extends React.PureComponent {
  state = {
    loading: false,
    nextPageLoading: false,
  };

  componentDidMount = () => {
    setTimeout(() => this.loadNextItemsIfNeeded(this.scrollContainerRef), 500);

    window.addEventListener('scroll', this.onScroll);
    window.addEventListener('resize', this.onScroll);
  };

  componentWillUnmount = () => {
    window.removeEventListener('scroll', this.onScroll);
    window.removeEventListener('resize', this.onScroll);
  };

  onFilterChange = (filterState) => {
    const arg = this.getArgsFromState(filterState);

    this.props.relay.setVariables(arg, ({ aborted, done, error, ready }) => {
      this.setState({
        loading: !ready,
      });
    });

    this.setState({ nextPageLoading: false });
  };

  onScroll = () => {
    if (!this.state.nextPageLoading) {
      this.loadNextItemsIfNeeded();
    }
  };

  getArgsFromState = (filterState) => {
    const args = {
      clientId: null,
      cashierId: null,
      paymentType: null,
      orderIdPart: null,
      first: PER_PAGE,
    };

    if (filterState.clientId) {
      args.clientId = filterState.clientId;
    }

    if (filterState.cashierId) {
      args.cashierId = filterState.cashierId;
    }

    if (filterState.paymentType) {
      args.paymentType = filterState.paymentType;
    }

    if (filterState.orderIdPart) {
      args.orderIdPart = filterState.orderIdPart;
    }

    return args;
  };

  loadNextItemsIfNeeded = () => {
    const scrollContainerComponent = this.scrollContainerRef;
    const tebleComponent = this.tableRef;
    if (scrollContainerComponent && tebleComponent) {
      const scrollElem = ReactDOM.findDOMNode(scrollContainerComponent);
      const tableElem = ReactDOM.findDOMNode(tebleComponent);
      const contentHeight = tableElem.offsetHeight;
      const y = scrollElem.scrollTop + scrollElem.offsetHeight;

      if (y >= (contentHeight - 200)) {
        this.loadNextItems();
      }
    }
  };

  loadNextItems = () => {
    this.setState({ nextPageLoading: true }, () => {
      const hasNextPage = get(this.props, 'viewer.myFirm.orderPayments.pageInfo.hasNextPage', false);

      if (hasNextPage) {
        this.props.relay.setVariables({
          first: this.props.relay.variables.first + PER_PAGE,
        }, (readyState) => {
          if (readyState.done || readyState.aborted) {
            this.setState({ nextPageLoading: false }, () => {
              this.loadNextItemsIfNeeded();
            });
          }
        });
      }
    });
  };

  scrollContainerRef = null;
  tableRef = null;

  render() {
    const { viewer, initialFilter } = this.props;
    const { myFirm: firm } = viewer;
    const { loading, nextPageLoading } = this.state;

    const orderPayments = map(get(firm, 'orderPayments.edges'), 'node');
    const hasNextPage = get(firm, 'orderPayments.pageInfo.hasNextPage', false);

    return (
      <div>
        <Paper
          style={rawStyle.paper}
          onScroll={this.onScroll}
          ref={(c) => {
            this.scrollContainerRef = c;
          }}
        >
          <Table
            ref={(c) => {
              this.tableRef = c;
            }}
          >
            <TableHead>
              <TableRow>
                <TableCell>Плательщик</TableCell>
                <TableCell>Заказ</TableCell>
                <TableCell>Сумма</TableCell>
                <TableCell>Тип оплаты</TableCell>
                <TableCell>Кассир</TableCell>
                <TableCell>Дата</TableCell>
              </TableRow>
              <OrderPaymentFilter
                onFilterChange={this.onFilterChange}
                initialFilter={initialFilter}
                firm={firm}
              />
            </TableHead>
            <TableBody>
              {loading && (
                <TableRow>
                  <TableCell colSpan={6} style={rawStyle.center}>
                    <br /><br /><br />
                    <CircularProgress /> Загрузка...
                    <br /><br /><br />
                  </TableCell>
                </TableRow>
              )}
              {!loading && !orderPayments.length && (
                <TableRow>
                  <TableCell colSpan={6} style={rawStyle.center}>
                    ничего не найдено
                  </TableCell>
                </TableRow>
              )}
              {!loading && !!orderPayments.length && orderPayments.map(orderPayment => (
                <TableRow key={orderPayment.id}>
                  <TableCell>
                    от <Link to={`/client/${orderPayment.client.id}`}>{orderPayment.client.fullName}</Link>
                  </TableCell>
                  <TableCell>
                    по <Link to={`/order/${orderPayment.order.id}`}>заказу
                    №{fromGlobalId(orderPayment.order.id).id}</Link>
                  </TableCell>
                  <TableCell numeric>
                    <Typography style={rawStyle.amount}>
                      {orderPayment.amount} р.
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <div>{getPaymentTypeName(orderPayment.paymentType)}</div>
                    {(!isEmpty(orderPayment.note) && isString(orderPayment.note)) && (
                      <Typography style={rawStyle.note}>
                        заметка:{' '}<i>{orderPayment.note}</i>
                      </Typography>
                    )}
                  </TableCell>
                  <TableCell>
                    {orderPayment.cashier.fullName}
                  </TableCell>
                  <TableCell>
                    {format(new Date().setTime(orderPayment.makeAt.timestamp * 1000), 'DD.MM.YY HH:mm')}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
        <div style={rawStyle.bottom}>
          {!loading && hasNextPage && nextPageLoading && (
            <div style={rawStyle.textCentered}>
              Грузим еще записи <CircularProgress thickness={4} size={20} />
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default OrderPaymentsTable;
