import PropTypes from 'prop-types';
import React from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
// import querystring from 'querystring';

import { withRouter } from 'react-router';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Relay from 'react-relay/classic';
import sortBy from 'lodash/sortBy';
import get from 'lodash/get';
import map from 'lodash/map';
import isEqual from 'lodash/isEqual';
import MaskedTextField from '../../../../components/basic/input/MaskedTextField';

import { getPaymentTypes, getPaymentTypeName } from '../../../../../../common-code/constants/paymentTypes';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import ClientSearchDialog from '../../../wfm/Dialogs/ClientSearchDialog';

const paymentsTypes = getPaymentTypes();

@relayContainer({
  firm: () => Relay.QL`
      fragment on Firm {
          managers {
              edges {
                  node {
                      id
                      email
                      fullName
                      active
                  }
              }
          }
      }
  `,
})
@withRouter
class OrderPaymentFilter extends React.PureComponent {
  static propTypes = {
    firm: PropTypes.any,
    onFilterChange: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    const { initialFilter } = props;

    this.state = {
      paymentType: get(initialFilter, 'paymentType') || 0,
      cashierId: get(initialFilter, 'cashierId') || 0,
      showClientSearchDialog: false,
      clientId: get(initialFilter, 'clientId', null),
      orderIdPart: get(initialFilter, 'orderIdPart', null),
      clientFullName: null,
    };
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (!isEqual(prevState, this.state)) {
      this.props.onFilterChange(this.state);
      const { history } = this.props;

      // TODO: При обновлении query string обновляется страница... Надо найти другой путь. например hash
      // const {
      //   paymentType, cashierId, clientId, orderIdPart
      // } = this.state;
      //
      // const query = {};
      //
      // if (paymentType) {
      //   query.paymentType = paymentType;
      // }
      // if (cashierId) {
      //   query.cashierId = cashierId;
      // }
      // if (clientId) {
      //   query.clientId = clientId;
      // }
      // if (orderIdPart !== '' && orderIdPart !== null) {
      //   query.orderIdPart = orderIdPart;
      // }
      //
      // const newLocation = {
      //   pathname: '/my-firm/payments',
      //   search: querystring.stringify(query),
      // };
      //
      // history.replace(newLocation);
    }
  };

  onClientSelect = (clientId, clientFullName) => {
    this.setState({
      clientId,
      clientFullName,
    });
  };

  handlePaymentTypeChange = (event) => {
    const { value } = event.target;

    this.setState({
      paymentType: value,
    });
  };

  handleCashierChange = (event) => {
    const { value } = event.target;

    this.setState({
      cashierId: value,
    });
  };

  onOrderIdInput = (event, filerValue) => {
    this.setState({
      orderIdPart: filerValue,
    });
  };

  showClientSearchDialog = () => {
    this.setState({
      showClientSearchDialog: true,
    });
  };

  hideClientSearchDialog = () => {
    this.setState({
      showClientSearchDialog: false,
    });
  };

  clearClient = () => {
    this.setState({
      clientId: null,
      clientFullName: null,
    });
  };

  render() {
    const { showClientSearchDialog, clientId, clientFullName, orderIdPart } = this.state;
    const { firm } = this.props;

    const managers = sortBy(map(get(firm, 'managers.edges'), 'node'), m => !m.active);

    return (
      <TableRow>
        <TableCell>
          <ClientSearchDialog
            onClientSelect={this.onClientSelect}
            canCreateClient={false}
            initialVariables={{
              keyword: '',
              notEmptyKeyword: false,
            }}
            open={showClientSearchDialog}
            onRequestClose={this.hideClientSearchDialog}
          />
          {!clientId && (
            <Button
              variant="raised"
              size="small"
              color="primary"
              onClick={this.showClientSearchDialog}
            >
              указать плательщика
            </Button>
          )}
          {clientId && (
            <div>
              {clientFullName}{' '}
              <Button
                variant="raised"
                size="small"
                color="primary"
                onClick={this.clearClient}
              >
                сбросить
              </Button>
            </div>
          )}
        </TableCell>
        <TableCell>
          <MaskedTextField
            mask="number"
            value={orderIdPart || ''}
            onChange={this.onOrderIdInput}
          />
        </TableCell>
        <TableCell />
        <TableCell>
          <FormControl fullWidth>
            <InputLabel>Тип оплаты:</InputLabel>
            <Select
              value={this.state.paymentType}
              onChange={this.handlePaymentTypeChange}
            >
              <MenuItem value={0}>Любой</MenuItem>
              {paymentsTypes.map(paymentsType => (
                <MenuItem key={paymentsType} value={paymentsType}>{getPaymentTypeName(paymentsType)}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </TableCell>
        <TableCell>
          <FormControl fullWidth>
            <InputLabel>Кассир:</InputLabel>
            <Select
              value={this.state.cashierId}
              onChange={this.handleCashierChange}
            >
              <MenuItem value={0}>Любой</MenuItem>
              {managers.map((manager) => {
                const managerDisplayName = get(manager, 'fullName', get(manager, 'email', manager.id));

                return (
                  <MenuItem key={manager.id} value={manager.id}>
                    {manager.active && managerDisplayName}
                    {!manager.active && <s>{managerDisplayName}</s>}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
        </TableCell>
        <TableCell />
      </TableRow>
    );
  }
}

export default OrderPaymentFilter;
