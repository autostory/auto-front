import React, { Component } from 'react';

import get from 'lodash/get';

import ConfiguredRadium from './../../../../radium/ConfiguredRadium';
import OrderPaymentsTable from './OrderPaymentsTable';

const PER_PAGE = 20;

@ConfiguredRadium
class MyFirmPayments extends Component {
  render() {
    const { viewer, initialFilter } = this.props;
    const firm = get(viewer, 'myFirm');
    const initialVariables = {
      first: PER_PAGE,
      after: null,
      ...initialFilter,
    };

    return (
      <OrderPaymentsTable
        firm={firm}
        initialVariables={initialVariables}
        initialFilter={initialFilter}
      />
    );
  }
}

export default MyFirmPayments;
