import React, { Component } from 'react';
import ConfiguredRadium from './../../../radium/ConfiguredRadium';

import PageBase from '../../../components/main/PageBase';
import MyFirmPayments from './includes/MyFirmPayments';

@ConfiguredRadium
class MyFirmPaymentsPage extends Component {
  getInitialFilter = () => {
    const params = new URLSearchParams(document.location.search.substring(1));

    const clientId = params.get('clientId');
    const cashierId = params.get('cashierId');
    const paymentType = params.get('paymentType');
    const orderIdPart = params.get('orderIdPart');

    const filter = {
      clientId: null,
      cashierId: null,
      paymentType: null,
      orderIdPart: null,
    };

    if (clientId) {
      filter.clientId = clientId;
    }

    if (cashierId) {
      filter.cashierId = cashierId;
    }

    if (paymentType) {
      filter.paymentType = paymentType;
    }

    if (orderIdPart !== null) {
      filter.orderIdPart = orderIdPart;
    }

    return filter;
  };

  render() {
    const initialFilter = this.getInitialFilter();

    return (
      <PageBase title="Транзакции" withPaper={false}>
        <MyFirmPayments
          initialFilter={initialFilter}
        />
      </PageBase>
    );
  }
}

export default MyFirmPaymentsPage;
