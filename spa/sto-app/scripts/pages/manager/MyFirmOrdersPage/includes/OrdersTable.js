/* eslint-disable react/no-find-dom-node */
import React from 'react';
import map from 'lodash/map';
import get from 'lodash/get';
import { format } from 'date-fns';
import ReactDOM from 'react-dom';
import Highlighter from 'react-highlight-words';

import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import Relay from 'react-relay/classic';
import { Link } from 'react-router-dom';

import CarInfoWithPopover from '../../../../components/Car/CarInfoWithPopover';
import { getOrderStatusTypeName, REJECT } from '../../../../../../common-code/constants/orderStatusTypes';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import { fromGlobalId } from '../../../../../../common-code/relay/globalIdUtils';
import OrderFilter from './OrderFilter';

const PER_PAGE = 20;

const rawStyle = {
  paper: {
    position: 'relative',
    overflow: 'auto',
    maxHeight: 'calc(100vh - 85px)',
    height: 'calc(100vh - 85px)',
  },
  textCentered: {
    textAlign: 'center',
  },
  bottom: {
    display: 'flex',
    justifyContent: 'center',
    padding: 11,
    position: 'absolute',
    bottom: 20,
    right: '50%',
    marginRight: -100,
  },
};

const Match = ({ key, text, ...rest }) => {
  return (
    <Highlighter
      highlightStyle={{ background: '#FAD65E', padding: '4px 0' }}
      searchWords={[key]}
      autoEscape
      textToHighlight={text || ''}
      {...rest}
    />
  );
};

@relayContainer(
  {
    firm: () => Relay.QL`
        fragment on Firm {
            ${OrderFilter.getFragment('firm')}
            orders (
                active: $active,
                clientId: $clientId,
                orderIdPart: $orderIdPart,
                carId: $carId,
                statuses: $statuses,
                first: $first,
                officeId: $officeId,
                after: $after,
            ) {
                edges {
                    node {
                        id
                        client {
                            id
                            fullName
                        }
                        car {
                            id
                            ${CarInfoWithPopover.getFragment('car')}
                        }
                        paymentsSum
                        rejectReasonId
                        rejectReasonDescription
                        status
                        request {
                            id
                        }
                        office {
                            id
                            title
                            active
                        }
                        totalBill
                        makeAt {
                            timestamp
                        }
                        contactName
                        contactPhone
                        contactNote
                        schedules {
                            id
                            start
                            finish
                            box {
                                id
                                title
                            }
                        }
                    }
                }
                pageInfo {
                    startCursor
                    endCursor
                    hasNextPage
                    hasPreviousPage
                }
            }
        }
    `,
  },
  {
    active: true,
    clientId: null,
    carId: null,
    officeId: null,
    statuses: [],
    orderIdPart: null,
    first: PER_PAGE,
    after: null,
  },
)
class OrdersTable extends React.PureComponent {
  state = {
    loading: false,
    nextPageLoading: false,
  };

  componentDidMount = () => {
    setTimeout(() => this.loadNextItemsIfNeeded(this.scrollContainerRef), 500);

    window.addEventListener('scroll', this.onScroll);
    window.addEventListener('resize', this.onScroll);
  };

  componentWillUnmount = () => {
    window.removeEventListener('scroll', this.onScroll);
    window.removeEventListener('resize', this.onScroll);
  };

  onFilterChange = (filterState) => {
    const arg = this.getArgsFromState(filterState);

    this.props.relay.setVariables(arg, ({ aborted, done, error, ready }) => {
      this.setState({
        loading: !ready,
      });
    });

    this.setState({ nextPageLoading: false });
  };

  onScroll = () => {
    if (!this.state.nextPageLoading) {
      this.loadNextItemsIfNeeded();
    }
  };

  getArgsFromState = (filterState) => {
    const args = {
      clientId: null,
      carId: null,
      officeId: null,
      statuses: [],
      orderIdPart: null,
      first: PER_PAGE,
    };

    if (filterState.clientId) {
      args.clientId = filterState.clientId;
    }

    if (filterState.carId) {
      args.carId = filterState.carId;
    }

    if (filterState.officeId) {
      args.officeId = filterState.officeId;
    }

    if (filterState.orderStatus) {
      args.statuses = [filterState.orderStatus];
    }

    if (filterState.orderIdPart) {
      args.orderIdPart = filterState.orderIdPart;
    }

    return args;
  };

  // TODO: Перенести по типу статусов
  getRejectReasonText = (rejectReasonId, rejectReasonDescription) => {
    switch (rejectReasonId) {
      case 'COST':
        return 'Не устроила цена.';
      case 'TIME':
        return 'Не устроило время.';
      case 'NO_SERVICE':
        return 'Не оказываем такие услуги.';
      case 'BAD_CAR':
        return 'Не обслуживаем такой автомобиль.';
      case 'OTHER':
        return `Другое. ${rejectReasonDescription}`;
      default:
        return '';
    }
  };

  loadNextItemsIfNeeded = () => {
    const scrollContainerComponent = this.scrollContainerRef;
    const tebleComponent = this.tableRef;
    if (scrollContainerComponent && tebleComponent) {
      const scrollElem = ReactDOM.findDOMNode(scrollContainerComponent);
      const tableElem = ReactDOM.findDOMNode(tebleComponent);
      const contentHeight = tableElem.offsetHeight;
      const y = scrollElem.scrollTop + scrollElem.offsetHeight;

      if (y >= (contentHeight - 200)) {
        this.loadNextItems();
      }
    }
  };

  loadNextItems = () => {
    this.setState({ nextPageLoading: true }, () => {
      const hasNextPage = get(this.props, 'firm.orders.pageInfo.hasNextPage', false);

      if (hasNextPage) {
        this.props.relay.setVariables({
          first: this.props.relay.variables.first + PER_PAGE,
        }, (readyState) => {
          if (readyState.done || readyState.aborted) {
            this.setState({ nextPageLoading: false }, () => {
              this.loadNextItemsIfNeeded();
            });
          }
        });
      }
    });
  };

  scrollContainerRef = null;
  tableRef = null;

  render() {
    const { firm } = this.props;
    const orders = map(get(firm, 'orders.edges'), 'node');

    const { loading, nextPageLoading } = this.state;

    const hasNextPage = get(this.props, 'firm.orders.pageInfo.hasNextPage', false);

    return (
      <div>
        <Paper
          style={rawStyle.paper}
          onScroll={this.onScroll}
          ref={(c) => {
            this.scrollContainerRef = c;
          }}
        >
          <Table
            ref={(c) => {
              this.tableRef = c;
            }}
          >
            <TableHead>
              <TableRow>
                <TableCell>#</TableCell>
                <TableCell>Статус</TableCell>
                <TableCell>Клиент</TableCell>
                <TableCell>Автомобиль</TableCell>
                <TableCell>Офис</TableCell>
                <TableCell>Дата создания</TableCell>
                <TableCell>Оплаты</TableCell>
                <TableCell />
              </TableRow>
              <OrderFilter
                onFilterChange={this.onFilterChange}
                firm={firm}
              />
            </TableHead>
            <TableBody>
              {loading && (
                <TableRow>
                  <TableCell colSpan={7} style={rawStyle.textCentered}>
                    <br /><br /><br />
                    <CircularProgress /> Загрузка...
                    <br /><br /><br />
                  </TableCell>
                </TableRow>
              )}
              {!loading && !orders.length && (
                <TableRow>
                  <TableCell colSpan={7} style={rawStyle.textCentered}>
                    ничего не найдено
                  </TableCell>
                </TableRow>
              )}
              {!loading && !!orders.length && orders.map(order => (
                <TableRow key={order.id}>
                  <TableCell>
                    <Match
                      key={this.props.relay.variables.orderIdPart}
                      text={fromGlobalId(order.id).id}
                    />
                  </TableCell>
                  <TableCell>
                    {getOrderStatusTypeName(order.status)}
                    {order.status === REJECT && (
                      <Typography>
                        {this.getRejectReasonText(order.rejectReasonId, order.rejectReasonDescription)}
                      </Typography>
                    )}
                  </TableCell>
                  <TableCell>
                    <Link to={`/client/${order.client.id}`}>
                      {order.client.fullName}
                    </Link>
                  </TableCell>
                  <TableCell>
                    <CarInfoWithPopover car={order.car} />
                  </TableCell>
                  <TableCell>
                    <Typography
                      style={{
                        maxWidth: 160,
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                        wordBreak: 'keep-all',
                        whiteSpace: 'nowrap',
                      }}
                      title={order.office.title}
                    >
                      {order.office.title}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    {format(new Date().setTime(order.makeAt.timestamp * 1000), 'DD.MM.YY HH:mm')}
                  </TableCell>
                  <TableCell>
                    <Typography>
                      к оплате {order.totalBill}р.<br />
                      {!order.paymentsSum && order.totalBill > 0 && '(не оплачен)'}
                      {order.paymentsSum > 0 && (
                        <Typography component="span">
                          <b>оплачено {order.paymentsSum} р.</b>{' '}
                          {order.paymentsSum >= order.totalBill && '(оплачен полностью)'}
                          {order.paymentsSum < order.totalBill && '(оплачен частично)'}
                        </Typography>
                      )}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Button
                      size="small"
                      variant="flat"
                      component={Link}
                      to={`/order/${order.id}`}
                      target="_blank"
                    >
                      Открыть
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
        <div style={rawStyle.bottom}>
          {!loading && hasNextPage && nextPageLoading && (
            <div style={rawStyle.textCentered}>
              Грузим еще записи <CircularProgress thickness={4} size={20} />
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default OrdersTable;
