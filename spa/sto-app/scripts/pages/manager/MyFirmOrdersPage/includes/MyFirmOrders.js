import React, { Component } from 'react';
import Relay from 'react-relay/classic';

import get from 'lodash/get';
import withProps from 'recompose/withProps';
import CircularProgress from '@material-ui/core/CircularProgress';

import ConfiguredRadium from './../../../../radium/ConfiguredRadium';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';
import OrdersTable from './OrdersTable';

@withProps({ renderLoading: () => <div>Загрузка заказов <CircularProgress size={25} /></div> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          myFirm {
              ${OrdersTable.getFragment('firm')}
          }
      }
  `,
})
@ConfiguredRadium
class MyFirmOrders extends Component {
  render() {
    const { viewer } = this.props;
    const firm = get(viewer, 'myFirm');

    return <OrdersTable firm={firm} />;
  }
}

export default MyFirmOrders;
