import React from 'react';
import get from 'lodash/get';
import map from 'lodash/map';
import PropTypes from 'prop-types';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import isEqual from 'lodash/isEqual';
import Relay from 'react-relay/classic';
import MaskedTextField from '../../../../components/basic/input/MaskedTextField';
import { getOrderStatusTypeName, getOrdeStatusTypes } from '../../../../../../common-code/constants/orderStatusTypes';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import CarSearchDialog from '../../../wfm/Dialogs/CarSearchDialog';

import ClientSearchDialog from '../../../wfm/Dialogs/ClientSearchDialog';

const orderStatuses = getOrdeStatusTypes();

@relayContainer({
  firm: () => Relay.QL`
      fragment on Firm {
          offices {
              officesList {
                  edges {
                      node {
                          id
                          active
                          title
                      }
                  }
              }
          }
      }
  `,
})
class OrderFilter extends React.PureComponent {
  static propTypes = {
    onFilterChange: PropTypes.func.isRequired,
  };

  state = {
    clientId: null,
    carId: null,
    officeId: null,
    orderStatus: 0,

    showClientSearchDialog: false,
    showCarSearchDialog: false,
    orderIdPart: null,
    clientFullName: null,
    carName: null,
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (!isEqual(prevState, this.state)) {
      this.props.onFilterChange(this.state);
    }
  };

  onClientSelect = (clientId, clientFullName) => {
    this.setState({
      clientId,
      clientFullName,
    });
  };

  onCarSelect = (carId, carName) => {
    this.setState({
      carId,
      carName,
    });
  };

  handleOrderStatusChange = (event) => {
    const { value } = event.target;

    this.setState({
      orderStatus: value,
    });
  };

  onOrderIdInput = (event, filerValue) => {
    this.setState({
      orderIdPart: filerValue,
    });
  };

  showClientSearchDialog = () => {
    this.setState({
      showClientSearchDialog: true,
    });
  };

  hideClientSearchDialog = () => {
    this.setState({
      showClientSearchDialog: false,
    });
  };

  clearClient = () => {
    this.setState({
      clientId: null,
      clientFullName: null,
    });
  };

  showCarSearchDialog = () => {
    this.setState({
      showCarSearchDialog: true,
    });
  };

  hideCarSearchDialog = () => {
    this.setState({
      showCarSearchDialog: false,
    });
  };

  clearCar = () => {
    this.setState({
      carId: null,
      carName: null,
    });
  };

  handleOfficeChange = (event) => {
    const { value } = event.target;

    this.setState({
      officeId: value,
    });
  };

  render() {
    const {
      showClientSearchDialog, showCarSearchDialog,
      clientId, clientFullName,
      carId, carName,
      officeId, orderIdPart, orderStatus,
    } = this.state;

    const offices = map(get(this.props, 'firm.offices.officesList.edges'), 'node');

    return (
      <TableRow>
        <TableCell>
          <MaskedTextField
            style={{ width: 50 }}
            mask="number"
            value={orderIdPart}
            onChange={this.onOrderIdInput}
          />
        </TableCell>
        <TableCell>
          <FormControl fullWidth>
            <InputLabel>Статус заказа:</InputLabel>
            <Select
              style={{ minWidth: 200 }}
              value={orderStatus}
              onChange={this.handleOrderStatusChange}
            >
              <MenuItem value={0}>Любой</MenuItem>
              {orderStatuses.map(s => (
                <MenuItem key={s} value={s}>{getOrderStatusTypeName(s)}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </TableCell>
        <TableCell>
          <ClientSearchDialog
            onClientSelect={this.onClientSelect}
            canCreateClient={false}
            initialVariables={{
              keyword: '',
              notEmptyKeyword: false,
            }}
            open={showClientSearchDialog}
            onRequestClose={this.hideClientSearchDialog}
          />
          {!clientId && (
            <Button
              variant="raised"
              size="small"
              color="primary"
              onClick={this.showClientSearchDialog}
            >
              указать заказчика
            </Button>
          )}
          {clientId && (
            <div>
              <Typography
                style={{
                  whiteSpace: 'nowrap',
                  maxWidth: 150,
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                  display: 'block',
                }}
                title={clientFullName}
              >
                {clientFullName}
              </Typography>
              <Button
                variant="raised"
                size="small"
                color="primary"
                onClick={this.clearClient}
              >
                сбросить
              </Button>
            </div>
          )}
        </TableCell>
        <TableCell>
          <CarSearchDialog
            onCarSelect={this.onCarSelect}
            open={showCarSearchDialog}
            onRequestClose={this.hideCarSearchDialog}
          />
          {!carId && (
            <Button
              variant="raised"
              size="small"
              color="primary"
              onClick={this.showCarSearchDialog}
            >
              указать автомобиль
            </Button>
          )}
          {carId && (
            <div>
              <Typography
                style={{
                  whiteSpace: 'nowrap',
                  maxWidth: 150,
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                  display: 'block',
                }}
                title={carName}
              >
                {carName}
              </Typography>
              <Button
                variant="raised"
                size="small"
                color="primary"
                onClick={this.clearCar}
              >
                сбросить
              </Button>
            </div>
          )}
        </TableCell>
        <TableCell>
          <FormControl fullWidth>
            <InputLabel>Офис:</InputLabel>
            <Select
              style={{ minWidth: 100, maxWidth: 160 }}
              value={officeId}
              onChange={this.handleOfficeChange}
            >
              <MenuItem value={0}>Любой</MenuItem>
              {offices.map(office => (
                <MenuItem
                  key={office.id}
                  value={office.id}
                  style={{
                    maxWidth: 400,
                    display: 'block',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    wordBreak: 'keep-all',
                    whiteSpace: 'nowrap',
                  }}
                  title={office.title}
                >
                  {office.active && office.title}
                  {!office.active && <s>{office.title}</s>}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </TableCell>
        <TableCell />
        <TableCell />
        <TableCell />
      </TableRow>
    );
  }
}

export default OrderFilter;
