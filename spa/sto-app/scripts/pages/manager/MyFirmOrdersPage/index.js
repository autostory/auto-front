import React, { Component } from 'react';
import ConfiguredRadium from './../../../radium/ConfiguredRadium';

import PageBase from '../../../components/main/PageBase';
import MyFirmOrders from './includes/MyFirmOrders';

@ConfiguredRadium
class MyFirmOrdersPage extends Component {
  render() {
    return (
      <PageBase title="Все заказы" withPaper={false}>
        <MyFirmOrders />
      </PageBase>
    );
  }
}

export default MyFirmOrdersPage;
