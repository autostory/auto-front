import React, { Component } from 'react';
import ConfiguredRadium from '../../../radium/ConfiguredRadium';
import PageBase from '../../../components/main/PageBase';
import AddOffice from '../../../components/manager/Office/Add/AddOffice';

@ConfiguredRadium
class AddOfficePage extends Component {
  render() {
    return (
      <PageBase title="Добавление офиса">
        <AddOffice />
      </PageBase>
    );
  }
}

export default AddOfficePage;
