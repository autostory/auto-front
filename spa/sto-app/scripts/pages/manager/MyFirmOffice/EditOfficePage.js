import React, { Component } from 'react';
import EditOffice from '../../../components/manager/Office/Edit/EditOffice';
import PageBase from '../../../components/main/PageBase';

class EditOfficePage extends Component {
  render() {
    const { match } = this.props;
    const officeId = match.params.id;

    return (
      <PageBase title="Редактирование офиса">
        <EditOffice officeId={officeId} />
      </PageBase>
    );
  }
}

export default EditOfficePage;
