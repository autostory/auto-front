import React, { Component } from 'react';
import ConfiguredRadium from '../../../radium/ConfiguredRadium';
import MyFirm from '../../../components/manager/MyFirm/MyFirm';
import PageBase from '../../../components/main/PageBase';

@ConfiguredRadium
class MyFirmPage extends Component {
  render() {
    return (
      <PageBase title="Карточка моей фирмы">
        <MyFirm />
      </PageBase>
    );
  }
}

export default MyFirmPage;
