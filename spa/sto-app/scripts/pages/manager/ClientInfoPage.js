import React, { Component } from 'react';
import ClientInfo from '../../components/manager/ClientInfo/ClientInfo';
import ConfiguredRadium from '../../radium/ConfiguredRadium';
import PageBase from '../../components/main/PageBase';

@ConfiguredRadium
class ClientInfoPage extends Component {
  render() {
    const { match } = this.props;

    return (
      <PageBase title="Карточка клиента">
        <ClientInfo clientId={match.params.id} />
      </PageBase >
    );
  }
}

export default ClientInfoPage;
