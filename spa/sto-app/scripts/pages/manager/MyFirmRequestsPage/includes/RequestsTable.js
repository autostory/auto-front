import get from 'lodash/get';
import React from 'react';
import map from 'lodash/map';

import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import Relay from 'react-relay/classic';
import { withStyles } from '@material-ui/core/styles';

import CarInfoWithPopover from '../../../../components/Car/CarInfoWithPopover';
import {
  getResultRequestCommunicationRejectReasonTypeName, OTHER,
} from '../../../../../../common-code/constants/resultRequestCommunicationRejectReasonTypes';
import {
  getResultRequestCommunicationTypeName, NO_ANSWER, RECALL, REJECT,
} from '../../../../../../common-code/constants/resultRequestCommunicationTypes';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import {
  setRequest,
  setRequestDialogOpen,
} from '../../../../reduxActions/WfmPageActions';
import { fromGlobalId } from '../../../../../../common-code/relay/globalIdUtils';
import DrawerRequestViewer from '../../../wfm/DrawerRequestViewer';
import DrawerRequestViewerInner from '../../../wfm/DrawerRequestViewer/DrawerRequestViewerInner';

const styles = {
  paper: {
    overflow: 'auto',
  },
};

@relayContainer({
  requests: () => Relay.QL`
      fragment on RequestConnection {
          edges {
              node {
                  id
                  client {
                      id
                      fullName
                  }
                  car {
                      id
                      ${CarInfoWithPopover.getFragment('car')}
                  }
                  makeAt {
                      formattedDateTime
                  }
                  order {
                      id
                  }
                  active
                  lastCommunication {
                      id
                      communicationDateTime
                      nextCommunicationDateTime {
                          formattedDateTime
                      }
                      resultId
                      rejectReasonId
                      rejectReasonDescription
                  }
                  userMessage
                  ${DrawerRequestViewerInner.getFragment('request')}
              }
          }
      }
  `,
  firm: () => Relay.QL`
      fragment on Firm {
          ${DrawerRequestViewer.getFragment('firm')}
      }
  `,
})
@connect(
  state => ({}),
  dispatch => ({
    setRequestDialogOpen: open => dispatch(setRequestDialogOpen(open)),
    setRequest: request => dispatch(setRequest(request)),
  }),
)
@withStyles(styles)
@withRouter
class RequestsTable extends React.PureComponent {
  onMakeOrderFromRequest = (orderId) => {
    this.props.history.push(`/order/${orderId}`);
  };

  showRequestDialog = request => () => {
    this.props.setRequest(request);
    this.props.setRequestDialogOpen(true);
  };

  closeRequestDialog = () => {
    this.props.setRequestDialogOpen(false);
    setTimeout(() => {
      this.props.setRequest(null);
    }, 300);
  };

  render() {
    const { classes, requests: requestsWithEdges, firm } = this.props;

    const requests = map(get(requestsWithEdges, 'edges'), 'node');

    if (!requests.length) {
      return <Typography>Тут будут заявки от клиентов. Сейчас нет ни одной заявки.</Typography>;
    }

    return (
      <Paper className={classes.paper}>
        <DrawerRequestViewer
          firm={firm}
          onDrawerClose={this.closeRequestDialog}
          onMakeOrderFromRequest={this.onMakeOrderFromRequest}
        />
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>#</TableCell>
              <TableCell>Клиент</TableCell>
              <TableCell>Автомобиль</TableCell>
              <TableCell>Сообщение клиента</TableCell>
              <TableCell>Дата создания</TableCell>
              <TableCell>Статус</TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {requests.map((request) => {
              const { lastCommunication } = request;

              let statusText = '';

              if (lastCommunication === null) {
                statusText = 'Новая. С клиентом не связывались.';
              } else {
                statusText = `${getResultRequestCommunicationTypeName(lastCommunication.resultId)}.`;

                if (lastCommunication.resultId === RECALL) {
                  statusText = `${statusText}  ${lastCommunication.nextCommunicationDateTime.formattedDateTime}.`;
                } else if (lastCommunication.resultId === NO_ANSWER) {
                  statusText = `${statusText} Перезвонить ${lastCommunication.nextCommunicationDateTime.formattedDateTime}.`;
                } else if (lastCommunication.resultId === REJECT && lastCommunication.rejectReasonId === OTHER) {
                  statusText = `${statusText} ${getResultRequestCommunicationRejectReasonTypeName(lastCommunication.rejectReasonId)}.`;
                  if (lastCommunication.rejectReasonDescription !== null) {
                    statusText = `${statusText} Причина: ${lastCommunication.rejectReasonDescription}`;
                  }
                }
              }

              return (
                <TableRow key={request.id}>
                  <TableCell>{fromGlobalId(request.id).id}</TableCell>
                  <TableCell>{request.client.fullName}</TableCell>
                  <TableCell><CarInfoWithPopover car={request.car} /></TableCell>
                  <TableCell>{request.userMessage}</TableCell>
                  <TableCell>{request.makeAt.formattedDateTime}</TableCell>
                  <TableCell>
                    <Typography
                      component="div"
                      style={{
                        maxWidth: 400,
                        wordBreak: 'break-all',
                      }}
                    >
                      {statusText}
                      {request.order && (
                        <div>
                          <Link to={`/order/${request.order.id}`} target="_blank">
                            Заказ №{fromGlobalId(request.order.id).id}
                          </Link>
                        </div>
                      )}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Button size="small" variant="flat" onClick={this.showRequestDialog(request)}>
                      Открыть
                    </Button>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

export default RequestsTable;
