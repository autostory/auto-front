import React, { Component } from 'react';
import Relay from 'react-relay/classic';

import get from 'lodash/get';
import withProps from 'recompose/withProps';
import CircularProgress from '@material-ui/core/CircularProgress';

import ConfiguredRadium from './../../../../radium/ConfiguredRadium';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';
import RequestsTable from './RequestsTable';

@withProps({ renderLoading: () => <CircularProgress /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          myFirm {
              ${RequestsTable.getFragment('firm')}
              requests {
                  ${RequestsTable.getFragment('requests')}
              }
          }
      }
  `,
})
@ConfiguredRadium
class MyFirmRequests extends Component {
  render() {
    const { viewer } = this.props;
    const requests = get(viewer, 'myFirm.requests');
    const firm = get(viewer, 'myFirm');

    return <RequestsTable requests={requests} firm={firm} />;
  }
}

export default MyFirmRequests;
