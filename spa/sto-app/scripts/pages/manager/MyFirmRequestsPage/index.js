import React, { Component } from 'react';
import ConfiguredRadium from './../../../radium/ConfiguredRadium';

import PageBase from '../../../components/main/PageBase';
import MyFirmRequests from './includes/MyFirmRequests';

@ConfiguredRadium
class MyFirmRequestsPage extends Component {
  render() {
    return (
      <PageBase title="Все заявки">
        <MyFirmRequests />
      </PageBase>
    );
  }
}

export default MyFirmRequestsPage;
