import React, { Component } from 'react';
import ConfiguredRadium from '../../radium/ConfiguredRadium';
import PageBase from '../../components/main/PageBase';
import GasEventAdd from '../../components/manager/GasEventAdd/GasEventAdd';

@ConfiguredRadium
class GasEventAddPage extends Component {
  render() {
    const { match } = this.props;

    return (
      <PageBase>
        <GasEventAdd carId={match.params.carId} />
      </PageBase >
    );
  }
}

export default GasEventAddPage;
