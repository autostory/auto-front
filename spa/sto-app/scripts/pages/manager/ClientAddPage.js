import React, { PureComponent } from 'react';
import Relay from 'react-relay/classic';
import withProps from 'recompose/withProps';
import FullScreenPreLoader from '../../components/basic/FullScreenPreLoader';

import ClientCreateForm from '../../components/manager/form/ClientCreateForm';
import submit from '../../components/manager/form/submit';
import PageBase from '../../components/main/PageBase';
import relaySuperContainer from '../../../../common-code/decorators/relaySuperContainer';
import CreateClientAsManagerMutation from '../../relay/mutation/CreateClientAsManagerMutation';

const CAR_TYPE_ID = 'Q2F0YWxvZ0NhclR5cGU6MQ=='; // легковое авто

@withProps({ renderLoading: () => <FullScreenPreLoader /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          ${ClientCreateForm.getFragment('viewer')}
          ${CreateClientAsManagerMutation.getFragment('viewer')},
      }
  `,
})
class ClientAddPage extends PureComponent {
  onSubmit = (values) => {
    const { viewer } = this.props;

    return submit(viewer, values);
  };

  render() {
    const { viewer } = this.props;

    return (
      <PageBase title="Добавление клиента">
        <ClientCreateForm
          onSubmit={this.onSubmit}
          viewer={viewer}
          initialValues={{
            noCarAdd: false,
            sendEmailInvite: true,
            sendSMSInvite: false,
            catalogCarTypeId: CAR_TYPE_ID,
          }}
        />
      </PageBase>
    );
  }
}

export default ClientAddPage;
