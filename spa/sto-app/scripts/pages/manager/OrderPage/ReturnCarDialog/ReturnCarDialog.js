import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import withMobileDialog from '@material-ui/core/withMobileDialog';

import Relay from 'react-relay/classic';
import Slide from '@material-ui/core/Slide';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import OrderActIframe from './OrderActIframe';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          id
          status
          ${OrderActIframe.getFragment('order')}
      }
  `,
})
@withMobileDialog()
class ReturnCarDialog extends Component {
  static propTypes = {
    onRequestClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
  };

  onClose = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  render() {
    const {
      open, onRequestClose, fullScreen, order,
    } = this.props;

    return (
      <Dialog
        fullScreen={fullScreen}
        TransitionComponent={Transition}
        onClose={onRequestClose}
        open={open}
        maxWidth="md"
      >
        <DialogTitle>Передача автомобиля клиенту</DialogTitle>

        <DialogContent>
          <Typography color="error">
            После отдачи автомобиля клиента нельзя будет изменить набор работ и материалов,
            а так же планировать работы.
            <br />
            Можно будет только провести оплату.
          </Typography>
          <br />
          <OrderActIframe
            order={order}
          />
        </DialogContent>

        <DialogActions>
          <Button
            onClick={this.onClose}
          >
            Я передумал
          </Button>
          {this.props.children}
        </DialogActions>

      </Dialog>
    );
  }
}

export default ReturnCarDialog;
