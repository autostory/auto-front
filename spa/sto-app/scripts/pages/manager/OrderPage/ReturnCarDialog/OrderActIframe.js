import React, { Component } from 'react';
import compose from 'recompose/compose';
import setDisplayName from 'recompose/setDisplayName';
import pdfMake from 'pdfmake/build/pdfmake';
import vfsFonts from 'pdfmake/build/vfs_fonts';

import Relay from 'react-relay/classic';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import { makePdfActParams } from '../../../../../../common-code/utils/pdf/makePdfActParams';

// const enhance = compose(
//   setDisplayName('OrderActIframe'),
// );

@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          id
          status
          totalBill
          paymentsSum
          office {
              id
              title
              address
          }
          firm {
              id
              name
          }
          office {
              id
              address
              title
          }
          makeAt {
              timestamp
          }
          serviceWorks {
              id
              amount
              discountPercent
              totalCost
              costPerUnit
              sparePartNode {
                  id
                  name
              }
              note
              action {
                  id
                  name
              }
          }
          client {
              id
              fullName
          }
          serviceMaterials {
              id
              name
              amount
              totalCost
              costPerUnit
              discountPercent
              unitMeasureType {
                  id
                  name
              }
          }
      }
  `,
})
class OrderActIframe extends Component {
  constructor(props) {
    super(props);

    this.iframeRef = null;
  }

  componentDidMount = () => {
    const { vfs } = vfsFonts.pdfMake;
    pdfMake.vfs = vfs;
    const doc = pdfMake.createPdf(makePdfActParams({ order: this.props.order }));

    doc.getBase64(this.setDocToIframe, doc);
  };

  setDocToIframe = (base64String) => {
    const url = `${URL.createObjectURL(this.b64toBlob(base64String, 'application/pdf'))}#toolbar=0&navpanes=0&scrollbar=0`;

    if (this.iframeRef) {
      this.iframeRef.setAttribute('src', url);
    }
  };

  b64toBlob = (b64Data, contentType) => {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += 512) {
      let slice = byteCharacters.slice(offset, offset + 512),
        byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, { type: contentType });
  };

  render() {
    return (
      <div>
        <iframe
          title="Акт"
          width="100%"
          height={800}
          src=""
          id="order_act"
          ref={(iframe) => {
            this.iframeRef = iframe;
          }}
        />
      </div>
    );
  }
}

export default OrderActIframe;
