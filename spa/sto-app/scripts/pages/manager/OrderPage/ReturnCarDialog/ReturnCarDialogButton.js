import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';

class ReturnCarDialogButton extends Component {
  static propTypes = {
    onClick: PropTypes.func.isRequired,
  };

  render() {
    const { onClick } = this.props;

    return (
      <Button
        color="primary"
        variant="contained"
        onClick={onClick}
      >
        Вернуть автомобиль клиенту
      </Button>
    );
  }
}

export default ReturnCarDialogButton;
