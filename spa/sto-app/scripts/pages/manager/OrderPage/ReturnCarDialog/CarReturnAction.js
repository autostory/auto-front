import React from 'react';
import Relay from 'react-relay/classic';
import { REJECT } from '../../../../../../common-code/constants/orderStatusTypes';

import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import OrderActIframe from './OrderActIframe';
import ReturnCarDialogButton from './ReturnCarDialogButton';
import ReturnCarDialog from './ReturnCarDialog';

@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          id
          status
          ${ReturnCarDialog.getFragment('order')}
          ${OrderActIframe.getFragment('order')}
      }
  `,
})
class CarReturnAction extends React.Component {
  state = {
    open: false,
  };

  onOpen = (event) => {
    this.setState({
      open: true,
    });

    event.preventDefault();
    return false;
  };

  onClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {
    const { open } = this.state;
    const { order } = this.props;

    if (order.status === REJECT) {
      return null;
    }

    return (
      <div>
        <ReturnCarDialogButton
          onClick={this.onOpen}
        />
        <ReturnCarDialog
          onRequestClose={this.onClose}
          order={order}
          open={open}
        >
          {this.props.children}
        </ReturnCarDialog>
      </div>
    );
  }
}

export default CarReturnAction;
