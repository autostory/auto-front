import React, { Component } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { Select, TextField } from 'redux-form-material-ui';
import validate from './validate';

export const FORM_NAME = 'OrderRejectForm';

const selector = formValueSelector(FORM_NAME);

const style = {
  c1: {
    marginTop: 12,
  },
  select: {
    minWidth: 220,
  },
  text: {
    marginTop: 12,
    width: '100%',
  },
};

@reduxForm({
  form: FORM_NAME,
  enableReinitialize: true,
  validate,
})
@connect(state => ({
  rejectReasonId: selector(state, 'rejectReasonId'),
}))
class OrderRejectForm extends Component {
  render() {
    const { rejectReasonId } = this.props;

    return (
      <div style={style.c1}>
        <FormControl fullWidth>
          <InputLabel htmlFor="reject-cause" required>Причина отказа</InputLabel>
          <Field
            component={Select}
            style={style.select}
            name="rejectReasonId"
            inputProps={{
              name: 'rejectReasonId',
              id: 'reject-cause',
            }}
          >
            <MenuItem value={0}><em>Не указано</em></MenuItem>
            <MenuItem value={1}>не устроила стоимость</MenuItem>
            <MenuItem value={2}>не удобное время</MenuItem>
            <MenuItem value={3}>не оказываем такую услугу</MenuItem>
            <MenuItem value={4}>не обслуживается такой автомобиль</MenuItem>
            <MenuItem value={5}>другое</MenuItem>
          </Field>
        </FormControl>
        <br />
        {rejectReasonId === 5 && (
          <Field
            inputProps={{
              maxLength: 250,
            }}
            component={TextField}
            name="rejectReasonDescription"
            style={style.text}
            label="Опишите причину отказа"
            multiline
            rows={1}
            rowsMax={4}
            placeholder="Введите причину отказа"
          />
        )}
      </div>
    );
  }
}

export default OrderRejectForm;
