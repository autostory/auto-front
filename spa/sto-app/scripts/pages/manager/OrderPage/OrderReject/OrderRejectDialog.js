import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import compose from 'recompose/compose';
import setDisplayName from 'recompose/setDisplayName';

import { connect } from 'react-redux';
import Relay from 'react-relay/classic';
import { isSubmitting, isInvalid, submit as ReduxFormSubmit } from 'redux-form';
import Slide from '@material-ui/core/Slide';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import OrderRejectForm, { FORM_NAME } from './OrderRejectForm';
import submit from './submit';
import RejectOrderMutation from './../../../../relay/mutation/order/RejectOrderMutation';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

const enhance = compose(
  setDisplayName('OrderReject'),
  relayContainer({
    order: () => Relay.QL`
        fragment on Order {
            id
            status
            ${RejectOrderMutation.getFragment('order')}
        }
    `,
  }),
  connect(state => ({
    invalid: isInvalid(FORM_NAME)(state),
    submitting: isSubmitting(FORM_NAME)(state),
  })),
  withMobileDialog(),
);

class OrderRejectDialog extends Component {
  static propTypes = {
    onRequestClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
  };

  onClose = () => {
    const { onRequestClose } = this.props;

    onRequestClose();
  };

  onOkClick = () => {
    this.props.dispatch(ReduxFormSubmit(FORM_NAME));
  };

  onSubmit = (values) => {
    const { order } = this.props;

    return submit(values, order).then(this.onClose);
  };

  render() {
    const {
      open, onRequestClose, fullScreen, invalid, submitting, order
    } = this.props;

    return (
      <Dialog
        fullScreen={fullScreen}
        TransitionComponent={Transition}
        onClose={onRequestClose}
        open={open}
        maxWidth="md"
      >
        <DialogTitle>Отмена заказа</DialogTitle>

        <DialogContent>
          <Typography color="error">
            После отмены заказа его нельзя будет больше редактировать!
          </Typography>
          <OrderRejectForm
            order={order}
            onSubmit={this.onSubmit}
          />
        </DialogContent>

        <DialogActions>
          <Button
            onClick={this.onClose}
            disabled={submitting}
          >
            Я передумал
          </Button>
          <Button
            onClick={this.onOkClick}
            color="primary"
            variant="contained"
            disabled={submitting || invalid}
          >
            {submitting && <CircularProgress size={14} />}
            {!submitting && 'Отмена заказа'}
          </Button>
        </DialogActions>

      </Dialog>
    );
  }
}

export default enhance(OrderRejectDialog);
