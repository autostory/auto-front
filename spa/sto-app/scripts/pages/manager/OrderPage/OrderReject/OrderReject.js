import React from 'react';
import Relay from 'react-relay/classic';
import compose from 'recompose/compose';
import setDisplayName from 'recompose/setDisplayName';
import { REJECT } from '../../../../../../common-code/constants/orderStatusTypes';

import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import OrderRejectButton from './OrderRejectButton';
import OrderRejectDialog from './OrderRejectDialog';

const enhance = compose(
  setDisplayName('OrderReject'),
  relayContainer({
    order: () => Relay.QL`
        fragment on Order {
            id
            status
            ${OrderRejectDialog.getFragment('order')}
        }
    `,
  }),
);

class OrderReject extends React.Component {
  state = {
    open: false,
  };

  onOpen = () => {
    this.setState({
      open: true,
    });
  };

  onClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {
    const { open } = this.state;
    const { order } = this.props;

    if (order.status === REJECT) {
      return null;
    }

    return (
      <div>
        <OrderRejectButton
          onClick={this.onOpen}
        />
        <OrderRejectDialog
          onRequestClose={this.onClose}
          order={order}
          open={open}
        />
      </div>
    );
  }
}

export default enhance(OrderReject);
