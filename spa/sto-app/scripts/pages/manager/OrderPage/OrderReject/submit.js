import RejectOrderMutation from './../../../../relay/mutation/order/RejectOrderMutation';
import commitMutationPromise from '../../../../../../common-code/relay/commitMutationPromise';

function submit(values, order) {
  return commitMutationPromise(
    RejectOrderMutation,
    {
      order,
      input: {
        rejectReasonId: values.rejectReasonId,
        rejectReasonDescription: values.rejectReasonDescription,
      },
    },
  );
}

export default submit;
