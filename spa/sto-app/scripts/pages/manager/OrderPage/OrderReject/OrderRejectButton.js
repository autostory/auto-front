import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';

class OrderRejectButton extends Component {
  static propTypes = {
    onClick: PropTypes.func.isRequired,
  };

  render() {
    const { onClick } = this.props;

    return (
      <Button
        color="secondary"
        onClick={onClick}
      >
        Отменить заказ
      </Button>
    );
  }
}

export default OrderRejectButton;
