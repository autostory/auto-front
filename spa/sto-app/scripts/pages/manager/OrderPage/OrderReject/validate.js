function validate(values) {
  const errors = {};

  if (!values.rejectReasonId) {
    errors.rejectReasonId = 'Укажите причину отказа';
  }

  if (
    values.rejectReasonId === 5
    && (
      !values.rejectReasonDescription || values.rejectReasonDescription.trim() === ''
    )
  ) {
    errors.rejectReasonDescription = 'Опишите причину отказа';
  }

  return errors;
}

export default validate;
