import React, { PureComponent } from 'react';
import isUndefined from 'lodash/isUndefined';

import { fromGlobalId } from '../../../../../common-code/relay/globalIdUtils';
import OrderFullInfoSuperContainer from './includes/OrderFullInfoSuperContainer';
import PageBase from '../../../components/main/PageBase';

class OrderPage extends PureComponent {
  render() {
    const { match } = this.props;

    const orderRelayParams = fromGlobalId(match.params.id);
    if (isUndefined(orderRelayParams.id)) {
      return 'Не правильный ID';
    }

    return (
      <PageBase title={`Заказ №${fromGlobalId(match.params.id).id}`}>
        <OrderFullInfoSuperContainer
          initialVariables={{
            orderId: match.params.id,
          }}
        />
      </PageBase>
    );
  }
}

export default OrderPage;
