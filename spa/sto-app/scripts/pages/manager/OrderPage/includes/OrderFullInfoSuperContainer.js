import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import withProps from 'recompose/withProps';
import CircularProgress from '@material-ui/core/CircularProgress';
import Relay from 'react-relay/classic';

import { setLastOrderCreateTimestamp, } from '../../../../reduxActions/WfmPageActions';
import OrderFullInfoContainer from './OrderFullInfoContainer';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';

@withProps({ renderLoading: () => <div>Загрузка информации о заказе<br /><CircularProgress /></div> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          ${OrderFullInfoContainer.getFragment('viewer')}
          order (orderId: $orderId) {
              ${OrderFullInfoContainer.getFragment('order')}
          }
      }
  `,
})
@connect(
  state => ({}),
  dispatch => ({
    setLastOrderCreateTimestamp: timestamp => dispatch(setLastOrderCreateTimestamp(timestamp)),
  }),
)
class OrderFullInfoSuperContainer extends PureComponent {
  static propTypes = {
    // eslint-disable-next-line react/no-unused-prop-types
    initialVariables: PropTypes.shape({
      orderId: PropTypes.string.isRequired,
    }).isRequired,
  };

  render() {
    const { viewer } = this.props;
    const { order } = viewer;

    return (
      <OrderFullInfoContainer
        viewer={viewer}
        order={order}
        onSubmit={this.onSubmit}
      />
    );
  }
}

export default OrderFullInfoSuperContainer;
