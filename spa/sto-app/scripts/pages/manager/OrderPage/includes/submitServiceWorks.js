import cloneDeep from 'lodash/cloneDeep';
import map from 'lodash/map';
import isUndefined from 'lodash/isUndefined';
import EditOrderServiceWorksMutation from '../../../../relay/mutation/order/EditOrderServiceWorksMutation';
import commitMutationPromise from '../../../../../../common-code/relay/commitMutationPromise';

function submitServiceWorks(order, values) {
  let input = cloneDeep(values);
  input.serviceWorks = map(input.serviceWorks, (sw) => {
    if (!isUndefined(sw.status)) {
      delete (sw.status);
    }

    return sw;
  });

  return commitMutationPromise(EditOrderServiceWorksMutation, { order, input });
}

export default submitServiceWorks;
