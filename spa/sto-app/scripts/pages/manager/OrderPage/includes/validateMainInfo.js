import validator from 'validator';

function validateMainInfo(values) {
  const errors = {};

  if (!values.clientId) {
    errors.clientId = 'Укажите клиента.';
  }

  if (!values.carId) {
    errors.carId = 'Укажите автомобиль.';
  }

  if (!values.phoneSameAsInProfile) {
    if (!values.contactPhone) {
      errors.contactPhone = 'Укажите контактный номер.';
    } else {
      const phoneMatch = values.contactPhone.match(/\d+/g);
      if (phoneMatch === null || !validator.isMobilePhone(phoneMatch.join(''), 'ru-RU', true)) {
        errors.contactPhone = 'Укажите контактный номер';
      }
    }

    if (!values.contactName) {
      errors.contactName = 'Укажите имя контактного лица.';
    }
  }

  if (!values.orderReason) {
    errors.orderReason = 'Укажите причину обращения.';
  }

  if (values.orderReason && !values.orderReason.trim()) {
    errors.orderReason = 'Укажите причину обращения.';
  }

  return errors;
}

export default validateMainInfo;
