import PropTypes from 'prop-types';
import isUndefined from 'lodash/isUndefined';

import React from 'react';
import uniq from 'lodash/uniq';
import concat from 'lodash/concat';
import map from 'lodash/map';
import get from 'lodash/get';
import isEqual from 'lodash/isEqual';
import { Prompt } from 'react-router-dom';

import Relay from 'react-relay/classic';
import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import OrderSchedulesEditableTable from '../../../wfm/Dialogs/includes/OrderSchedulesEditableTable';
import SetScheduleStatusMutation from './../../../../relay/mutation/schedule/SetScheduleStatusMutation';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';

export const FORM_NAME = 'EditOrderSchedulesForm';

const selector = formValueSelector(FORM_NAME);

const mapServiceWorksToScheduleForm = serviceWorks => map(serviceWorks, serviceWork => serviceWork.id);

const mapSchedulesToForm = schedules => map(schedules, schedule => ({
  id: schedule.id,
  boxId: get(schedule, 'box.id', null),
  start: get(schedule, 'start.timestamp', null) * 1000,
  finish: get(schedule, 'finish.timestamp', null) * 1000,
  status: get(schedule, 'status', null),
  serviceWorks: mapServiceWorksToScheduleForm(get(schedule, 'serviceWorks', [])),
}));

const parseOrderForForm = order => ({
  schedules: mapSchedulesToForm(order.schedules),
  deletingSchedulesIds: [],
});

@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          id
          schedules {
              ${SetScheduleStatusMutation.getFragment('schedule')}
              serviceWorks {
                  id
              }
              status
              id
              box {
                  id
              }
              start {
                  timestamp
              }
              finish {
                  timestamp
              }
          }
          office {
              ${OrderSchedulesEditableTable.getFragment('office')}
              boxes(active: true) {
                  id
                  title
              }
          }
          ${SetScheduleStatusMutation.getFragment('order')}
          ${OrderSchedulesEditableTable.getFragment('order')}
      }
  `,
})
@connect(state => ({
  schedules: selector(state, 'schedules'),
  deletingSchedulesIds: selector(state, 'deletingSchedulesIds'),
}))
class OrderSchedulesForm extends React.PureComponent {
  static defaultProps = {
    afterScheduleStatusChange: () => {
    },
    disabled: false,
  };

  static propTypes = {
    afterScheduleStatusChange: PropTypes.func,
    disabled: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    const { order, initialize } = props;

    initialize(parseOrderForForm(order));
  }

  componentDidUpdate = (prevProps, prevState) => {
    const {
      submitSucceeded, order, initialize, dirty,
      submitting, schedules, deletingSchedulesIds,
    } = this.props;

    if (prevProps.submitSucceeded !== this.props.submitSucceeded) {
      const newData = parseOrderForForm(order);

      this.props.initialize(newData);

      return;
    }

    const { handleSubmit } = this.props;
    let sendToSubmit = false;

    if (
      !submitSucceeded
      && !submitting
      && dirty
      && !isUndefined(prevProps.schedules)
      && !isEqual(schedules, prevProps.schedules)
    ) {
      handleSubmit();
      sendToSubmit = true;
    }

    if (
      !submitSucceeded
      && !submitting
      && dirty
      && !isUndefined(prevProps.deletingSchedulesIds)
      && !isEqual(deletingSchedulesIds, prevProps.deletingSchedulesIds)
    ) {
      handleSubmit();
      sendToSubmit = true;
    }

    if (
      !sendToSubmit && !submitting && !isUndefined(prevProps.schedules)
      && !isEqual(parseOrderForForm(order).schedules, prevProps.schedules)
    ) {
      this.props.initialize(parseOrderForForm(order));
    }
  };

  onSchedulesChange = (schedules, deletingSchedulesIds) => {
    const {
      schedules: currentSchedules,
      deletingSchedulesIds: currentDeletingSchedulesIds,
      change,
    } = this.props;

    if (!isEqual(currentSchedules, schedules)) {
      change('schedules', schedules);
    }

    if (deletingSchedulesIds.length) {
      change('deletingSchedulesIds', uniq(concat(currentDeletingSchedulesIds, deletingSchedulesIds)));
    }
  };

  afterScheduleStatusChange = () => {
    this.props.afterScheduleStatusChange();
    this.props.relay.forceFetch();
  };

  render() {
    const {
      disabled, dirty, order, schedules,
    } = this.props;

    const { office } = order;

    return (
      <div>
        <Prompt
          when={dirty}
          message={() =>
            'На странице есть не сохраненные изменения. Вы действительно хотите покинуть страницу?'
          }
        />

        {schedules && (
          <OrderSchedulesEditableTable
            office={office}
            afterScheduleStatusChange={this.afterScheduleStatusChange}
            order={order}
            disabled={disabled}
            schedules={schedules}
            onChange={this.onSchedulesChange}
          />
        )}
      </div>
    );
  }
}

export default reduxForm({
  form: FORM_NAME,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
})(OrderSchedulesForm);
