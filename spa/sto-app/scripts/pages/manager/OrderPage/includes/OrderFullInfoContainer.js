import React, { PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles/index';
import { formatRelative, format } from 'date-fns';
import { ru } from 'date-fns/esm/locale';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';
import Relay from 'react-relay/classic';
import get from 'lodash/get';
import Grid from '@material-ui/core/Grid';

import {
  getOrderStatusTypeName,
  REJECT,
  DONE,
  WAIT_CAR_RETURN,
  WAIT_PAYMENT, IN_WORK,
} from '../../../../../../common-code/constants/orderStatusTypes';
import { setPageHeader } from '../../../../reduxActions/ChangePageHeaderActions';
import { fromGlobalId } from '../../../../../../common-code/relay/globalIdUtils';
import NotifyClientAboutOrderWorkCompletedDialog from '../../../wfm/Dialogs/NotifyClientAboutOrderWorkCompletedDialog';
import OrderReject from '../OrderReject/OrderReject';
import EditOrderMainInfoMutation from './../../../../relay/mutation/order/EditOrderMainInfoMutation';
import EditOrderServiceMaterialMutation from '../../../../relay/mutation/order/EditOrderServiceMaterialMutation';
import EditOrderServiceWorksMutation from '../../../../relay/mutation/order/EditOrderServiceWorksMutation';
import EditOrderSchedulesMutation from '../../../../relay/mutation/order/EditOrderSchedulesMutation';
import DownloadActButton from './DownloadActButton';
import OrderMainInfoForm from './OrderMainInfoForm';
import OrderSchedulesForm from './OrderSchedulesForm';
import OrderServiceWorksForm from './OrderServiceWorksForm';
import OrderServiceMaterialsForm from './OrderServiceMaterialsForm';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import OrderPaymentsTable from './OrderPaymentsTable';
import submitMainInfo from './submitMainInfo';
import submitSchedules from './submitSchedules';
import submitServiceMaterial from './submitServiceMaterial';
import submitServiceWorks from './submitServiceWorks';
import CarArriveButtons from './CarArriveButtons';

const styles = {
  paper: {
    padding: 24,
    margin: '24px 0',
  },
  root: {
    padding: '0 0 24px 0',
  },
  statusSelect: {
    minWidth: 400,
  },
};

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id
          ${OrderMainInfoForm.getFragment('viewer')}
      }
  `,
  order: () => Relay.QL`
      fragment on Order {
          id
          status
          office {
              id
              title
              address
          }
          serviceWorks {
              id
          }
          makeAt {
              timestamp
          }
          factClientVisitDateTime {
              timestamp
          }
          factCarReturnDateTime {
              timestamp
          }
          rejectReasonId
          rejectReasonDescription
          ${OrderSchedulesForm.getFragment('order')}
          ${OrderPaymentsTable.getFragment('order')}
          ${OrderMainInfoForm.getFragment('order')}
          ${OrderServiceWorksForm.getFragment('order')}
          ${OrderServiceMaterialsForm.getFragment('order')}
          ${DownloadActButton.getFragment('order')}
          ${EditOrderMainInfoMutation.getFragment('order')}
          ${CarArriveButtons.getFragment('order')}
          ${EditOrderServiceWorksMutation.getFragment('order')}
          ${EditOrderServiceMaterialMutation.getFragment('order')}
          ${EditOrderSchedulesMutation.getFragment('order')}
          ${OrderReject.getFragment('order')}
          ${NotifyClientAboutOrderWorkCompletedDialog.getFragment('order')}
      }
  `,
})
@connect(
  state => ({}),
  dispatch => ({
    setPageHeader: header => dispatch(setPageHeader(header)),
  }),
)
@withStyles(styles)
class OrderFullInfoContainer extends PureComponent {
  constructor(props) {
    super(props);

    const orderId = fromGlobalId(props.order.id).id;
    const status = getOrderStatusTypeName(props.order.status);
    props.setPageHeader(`Заказ №${orderId} (${status})`);
  }

  // TODO: Перенести по типу статусов
  getRejectReasonText = (rejectReasonId, rejectReasonDescription) => {
    switch (rejectReasonId) {
      case 'COST':
        return 'Не устроила цена.';
      case 'TIME':
        return 'Не устроило время.';
      case 'NO_SERVICE':
        return 'Не оказываем такие услуги.';
      case 'BAD_CAR':
        return 'Не обслуживаем такой автомобиль.';
      case 'OTHER':
        return `Другое. ${rejectReasonDescription}`;
      default:
        return '';
    }
  };

  componentDidUpdate = (prevProps) => {
    if (prevProps.order.status !== this.props.order.status) {
      const orderId = fromGlobalId(this.props.order.id).id;
      const status = getOrderStatusTypeName(this.props.order.status);
      this.props.setPageHeader(`Заказ №${orderId} (${status})`);
    }
  };

  onOrderMainInfoFormSubmit = (values) => {
    const { order } = this.props;

    return submitMainInfo(order, values);
  };

  onOrderServiceWorksFormSubmit = (values) => {
    const { order } = this.props;

    return submitServiceWorks(order, values);
  };

  onOrderServiceMaterialsFormSubmit = (values) => {
    const { order } = this.props;

    return submitServiceMaterial(order, values);
  };

  onOrderSchedulesFormSubmit = (values) => {
    const { order } = this.props;

    return submitSchedules(order, values).then(() => {
      this.props.relay.forceFetch();
    });
  };

  afterScheduleStatusChange = () => {
    this.props.relay.forceFetch();
  };

  render() {
    const { classes, order, viewer } = this.props;

    const editEnabled = order.status !== REJECT && order.status !== DONE;
    const changeOrderWorkDisabled = order.status === WAIT_PAYMENT || order.status === WAIT_CAR_RETURN;

    const { serviceWorks } = order;

    if (order === null) {
      return <div>Такого заказа не существует.</div>;
    }

    const makeAt = new Date();
    makeAt.setTime(order.makeAt.timestamp * 1000);

    const officeId = order.office.id;

    const factCarReturnTimestamp = get(order, 'factCarReturnDateTime.timestamp', false);
    let factCarReturnDate = null;
    if (factCarReturnTimestamp) {
      factCarReturnDate = new Date();
      factCarReturnDate.setTime(factCarReturnTimestamp * 1000);
    }

    const factClientVisitTimestamp = get(order, 'factClientVisitDateTime.timestamp', false);
    let factClientVisitDate = null;
    if (factClientVisitTimestamp) {
      factClientVisitDate = new Date();
      factClientVisitDate.setTime(factClientVisitTimestamp * 1000);
    }

    // TODO: Добавить в форму пробег
    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="flex-start"
          >
            <Grid item>
              <Typography variant="headline">Основная информация</Typography>
              <Typography>
                Дата поступления:
                {' '}
                <b>{format(makeAt, 'DD.MM.YY HH:mm')}</b>
                {' '}
                (<i>{formatRelative(makeAt, new Date(), { locale: ru })}</i>)
              </Typography>
              <Typography>
                Статус:{' '}<b>{getOrderStatusTypeName(order.status)}</b>
              </Typography>
              {order.status === REJECT && (
                <Typography>
                  Причина отказа:{' '}{this.getRejectReasonText(order.rejectReasonId, order.rejectReasonDescription)}
                </Typography>
              )}
              <Typography component="div">
                Автомобиль в СТО:{' '}
                {!factClientVisitDate && <b>НЕТ</b>}
                {factClientVisitDate && <span><b>ДА</b>, прибыло {format(factClientVisitDate, 'DD.MM.YY HH:mm')}</span>}

                <CarArriveButtons
                  order={order}
                />
              </Typography>
              {factCarReturnDate && (
                <Typography>
                  Дата возврата авто клиенту:
                  {' '}
                  <b>{format(factCarReturnDate, 'DD.MM.YY HH:mm')}</b>
                </Typography>
              )}
              <Typography component="div">
                Офис:{' '}{order.office.title}{' '}{order.office.address}
              </Typography>
              <DownloadActButton
                order={order}
              />
            </Grid>
            <Grid item>
              <OrderReject order={order} />
            </Grid>
          </Grid>
        </Paper>

        <Paper className={classes.paper} id="main">
          <OrderMainInfoForm
            disabled={!editEnabled}
            order={order}
            viewer={viewer}
            onSubmit={this.onOrderMainInfoFormSubmit}
          />
        </Paper>

        <Paper className={classes.paper} id="service">
          <Typography variant="headline">Перечень выполняемых работ</Typography>
          <OrderServiceWorksForm
            disabled={!editEnabled || changeOrderWorkDisabled}
            order={order}
            officeId={officeId}
            onSubmit={this.onOrderServiceWorksFormSubmit}
          />
        </Paper>

        <Paper className={classes.paper} id="materials">
          <Typography variant="headline">Используемые запасные части (материалы)</Typography>
          <OrderServiceMaterialsForm
            disabled={!editEnabled || changeOrderWorkDisabled}
            order={order}
            onSubmit={this.onOrderServiceMaterialsFormSubmit}
          />
        </Paper>

        <Paper className={classes.paper} id="schedule">
          <Typography variant="headline">Планирование</Typography>
          {serviceWorks.length > 0 && (
            <OrderSchedulesForm
              disabled={!editEnabled || changeOrderWorkDisabled}
              order={order}
              afterScheduleStatusChange={this.afterScheduleStatusChange}
              onSubmit={this.onOrderSchedulesFormSubmit}
            />
          )}
          {serviceWorks.length === 0 && (
            <Typography variant="headline" color="error">
              Планировать работы можно после их добавления
            </Typography>
          )}
        </Paper>

        <Paper className={classes.paper} id="payments">
          <Typography variant="headline">Платежи</Typography>
          {serviceWorks.length > 0 && <OrderPaymentsTable order={order} disabled={!editEnabled} />}
          {serviceWorks.length === 0 && (
            <Typography variant="headline" color="error">
              Можно проводить оплаты после добавления работ по заказу
            </Typography>
          )}
        </Paper>
      </div>
    );
  }
}

export default OrderFullInfoContainer;
