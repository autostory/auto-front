/* eslint-disable prefer-destructuring */
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import find from 'lodash/find';
import { format } from 'date-fns';
import isUndefined from 'lodash/isUndefined';
import map from 'lodash/map';
import { withStyles } from '@material-ui/core/styles/index';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import { Prompt } from 'react-router-dom';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Relay from 'react-relay/classic';
import { Checkbox, TextField } from 'redux-form-material-ui';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import InputAdornment from '@material-ui/core/InputAdornment';
import NoteIcon from '@material-ui/icons/Note';

import CarInfoWithPopover from '../../../../components/Car/CarInfoWithPopover';
import DateTimeFormInput, {
  dateToTimestamp,
  timeStampToDate,
} from '../../../../components/common/FormInput/DateTimeFormInput';
import Car from '../../../../components/Form/Order/Car';
import Client from '../../../../components/Form/Order/Client';
import Contact from '../../../../components/Form/Order/Contact';
import ViewerRoute from '../../../../../../common-code/relay/route/ViewerRoute';
import ClientSearchDialog from '../../../wfm/Dialogs/ClientSearchDialog';
import CreateClientDialog from '../../../wfm/Dialogs/CreateClientDialog';
import validateMainInfo from './validateMainInfo';
import SelectClientCarsDialog from '../../../wfm/Dialogs/SelectClientCarsDialog';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import OrderPaymentsTable from './OrderPaymentsTable';

export const FORM_NAME = 'EditOrderMainInfoForm';

const styles = {
  paper: {
    padding: 24,
    margin: '24px 0',
  },
  statusSelect: {
    minWidth: 400,
  },
};

const selector = formValueSelector(FORM_NAME);

const parseOrderForForm = order => ({
  contactName: order.contactName,
  contactPhone: order.contactPhone,
  contactNote: order.contactNote,
  clientId: order.client.id,
  carId: order.car.id,
  orderReason: order.reason,
  phoneSameAsInProfile: order.phoneSameAsInProfile,
  estimatedClientArrivalDate: get(order, 'estimatedClientArrivalDate.timestamp', null),
  isClientVisitRequired: order.isClientVisitRequiredForPlaning,
});

@relayContainer(
  {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
            client(clientId: $clientId) @include(if: $clientChanged)  {
                ${Client.getFragment('client')}
                ${Contact.getFragment('client')}
                ${Car.getFragment('client')}
                ${SelectClientCarsDialog.getFragment('client')}
                id
                fullName
                phone
                cars {
                    itemsCount
                    carList {
                        edges {
                            node {
                                id
                                displayName
                                registrationSignNumber
                                ${CarInfoWithPopover.getFragment('car')}
                                ${Car.getFragment('car')}
                            }
                        }
                    }
                }
            }
        }
    `,
    order: () => Relay.QL`
        fragment on Order {
            ${OrderPaymentsTable.getFragment('order')}
            id
            car {
                id
                displayName
                registrationSignNumber
                ${CarInfoWithPopover.getFragment('car')}
                ${Car.getFragment('car')}
            }
            client {
                ${Client.getFragment('client')}
                ${Contact.getFragment('client')}
                ${Car.getFragment('client')}
                ${SelectClientCarsDialog.getFragment('client')}
                id
                fullName
                phone
            }
            estimatedClientArrivalDate {
                timestamp
            }
            factClientVisitDateTime {
                timestamp
            }
            serviceWorks {
                id
            }
            isClientVisitRequiredForPlaning
            contactName
            contactPhone
            phoneSameAsInProfile
            contactNote
            reason
        }
    `,
  },
  {
    clientChanged: false,
    clientId: null,
  },
)
@connect(state => ({
  clientId: selector(state, 'clientId'),
  carId: selector(state, 'carId'),
  phoneSameAsInProfile: selector(state, 'phoneSameAsInProfile'),
}))
@withStyles(styles)
class OrderMainInfoForm extends PureComponent {
  static defaultProps = {
    disabled: false,
  };

  static propTypes = {
    disabled: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    const { order } = props;
    const values = parseOrderForForm(order);
    props.initialize(values);

    props.relay.setVariables({
      clientChanged: true,
      clientId: order.client.id,
    });
  }

  state = {
    showClientSearchDialog: false,
    showSelectClientCarsDialog: false,
    showClientAddDialog: false,
    showCarAddDialog: false,
  };

  componentDidUpdate = (prevProps) => {
    if (prevProps.submitSucceeded !== this.props.submitSucceeded) {
      const { order } = this.props;

      this.props.initialize(parseOrderForForm(order));
    }
  };

  onSave = () => {
    const { handleSubmit } = this.props;

    handleSubmit();
  };

  onCarSelect = (selectedCarId) => {
    this.props.change('carId', selectedCarId);
  };

  onClientCreated = ({ newClientId }) => {
    this.props.change('clientId', newClientId);
  };

  onClientSelect = (selectedClientId) => {
    this.props.change('clientId', selectedClientId);
    this.props.change('carId', null);

    this.props.relay.setVariables({
      clientChanged: true,
      clientId: selectedClientId,
    });
  };

  showClientAddDialog = () => {
    this.setState({
      showClientAddDialog: true,
    });
  };

  hideClientAddDialog = () => {
    this.setState({
      showClientAddDialog: false,
      showClientSearchDialog: false,
    });
  };

  hideClientSearchDialog = () => {
    this.setState({
      showClientSearchDialog: false,
    });
  };

  showClientSearchDialog = () => {
    this.setState({
      showClientSearchDialog: true,
    });
  };

  showAddClientCarsDialog = () => {
    this.setState({
      showSelectClientCarsDialog: true,
      showCarAddDialog: true,
    });
  };

  afterCarAdd = () => {
    this.setState({
      showSelectClientCarsDialog: true,
      showCarAddDialog: false,
    });
  };

  hideSelectClientCarsDialog = () => {
    this.setState({
      showSelectClientCarsDialog: false,
      showCarAddDialog: false,
    });
  };

  showSelectClientCarsDialog = () => {
    this.setState({
      showSelectClientCarsDialog: true,
    });
  };

  render() {
    const {
      phoneSameAsInProfile, dirty, invalid, submitting, disabled,
    } = this.props;

    const {
      showClientSearchDialog, showClientAddDialog, showSelectClientCarsDialog, showCarAddDialog,
    } = this.state;

    const isClientArrive = get(this.props, 'order.factClientVisitDateTime.timestamp', false);
    let clientArriveDate = false;
    if (isClientArrive) {
      clientArriveDate = new Date();
      clientArriveDate.setTime(get(this.props, 'order.factClientVisitDateTime.timestamp') * 1000);
    }

    let estimatedClientArrivalDate = false;
    const estimatedClientArrivalTimestamp = get(this.props, 'order.estimatedClientArrivalDate.timestamp', false);
    if (estimatedClientArrivalTimestamp) {
      estimatedClientArrivalDate = new Date();
      estimatedClientArrivalDate.setTime(estimatedClientArrivalTimestamp * 1000);
    }

    let client;
    if (!isUndefined(this.props.viewer.client)) {
      client = this.props.viewer.client;
    }

    const { order } = this.props;

    const { serviceWorks } = order;
    const haveServiceWorks = serviceWorks && serviceWorks.length > 0;

    let car;
    if (!isUndefined(this.props.viewer.client)) {
      const clientCars = map(get(this.props.viewer.client, 'cars.carList.edges', null), 'node');
      if (!isEmpty(clientCars)) {
        car = find(clientCars, { id: this.props.carId });
      }
    }

    const inputDisabled = this.props.submitting || this.props.disabled;

    return (
      <div>
        <Prompt
          when={dirty}
          message={() =>
            'На странице есть не сохраненные изменения. Вы действительно хотите покинуть страницу?'
          }
        />
        <ClientSearchDialog
          onStartCreateClient={this.showClientAddDialog}
          canCreateClient
          onClientSelect={this.onClientSelect}
          initialVariables={{
            keyword: '',
            notEmptyKeyword: false,
          }}
          open={showClientSearchDialog}
          onRequestClose={this.hideClientSearchDialog}
        />

        <Relay.RootContainer
          Component={CreateClientDialog}
          route={new ViewerRoute()}
          renderFetched={data => (
            <CreateClientDialog
              onRequestClose={this.hideClientAddDialog}
              onClientCreated={this.onClientCreated}
              open={showClientAddDialog}
              viewer={data.viewer}
            />
          )}
        />

        <SelectClientCarsDialog
          afterCarAdd={this.afterCarAdd}
          client={client}
          onCarSelect={this.onCarSelect}
          open={showSelectClientCarsDialog}
          showCarAddDialog={showCarAddDialog}
          onRequestClose={this.hideSelectClientCarsDialog}
        />

        <div>
          <Typography variant="headline">Основное</Typography>

          <Typography variant="subheading">Клиент</Typography>

          <Client
            disabled={inputDisabled}
            loading={isUndefined(client)}
            onClearClick={this.clearSelectedClient}
            withProfileLink
            clearable={false}
            client={client}
            onStartCleintSearch={this.showClientSearchDialog}
            onStartClientAdd={this.showClientAddDialog}
          />
          <br />
          <Typography variant="subheading">Контакты</Typography>
          <Contact
            loading={isUndefined(client)}
            disabled={inputDisabled}
            client={client}
            phoneSameAsInProfile={phoneSameAsInProfile}
          />

          <Field
            disabled={inputDisabled}
            component={TextField}
            name="contactNote"
            label="Заметка"
            multiline
            rowsMax={5}
            InputProps={{
              maxLength: 250,
              startAdornment: (
                <InputAdornment position="start">
                  <NoteIcon />
                </InputAdornment>
              ),
            }}
            placeholder="Введите комментарий"
          />
          <br />
          <Typography variant="subheading">Автомобиль</Typography>
          <Car
            disabled={inputDisabled}
            loading={isUndefined(client)}
            onCarAddClick={this.showAddClientCarsDialog}
            onCarSelectClick={this.showSelectClientCarsDialog}
            client={client}
            withProfileLink
            car={car}
          />

          <Field
            inputProps={{
              maxLength: 250,
            }}
            disabled={inputDisabled}
            component={TextField}
            required
            name="orderReason"
            label="Причина обращения"
            placeholder="Укажите причину обращения клиента"
            multiline
            rows={2}
            fullWidth
            margin="normal"
          />
          <br />
          <br />
          {!isClientArrive && (
            <Field
              inputProps={{
                maxLength: 250,
              }}
              disabled={inputDisabled}
              name="estimatedClientArrivalDate"
              label="Когда приедет клиент"
              normalize={dateToTimestamp}
              format={timeStampToDate}
              component={DateTimeFormInput}
              clearable
            />
          )}
          {clientArriveDate && estimatedClientArrivalDate && (
            <Typography>
              Клиент обещал приехать <b>{format(estimatedClientArrivalDate, 'DD.MM.YY HH:mm')}</b>
            </Typography>
          )}
          {clientArriveDate && (
            <Typography>
              Фактически клиент приехал <b>{format(clientArriveDate, 'DD.MM.YY HH:mm')}</b>
            </Typography>
          )}
          {!haveServiceWorks && (
            <React.Fragment>
              <br />
              <br />
              <FormControlLabel
                disabled={inputDisabled}
                control={
                  <Field
                    color="primary"
                    name="isClientVisitRequired"
                    component={Checkbox}
                  />
                }
                label="Для формирования списка работ требуется визит клиента"
              />
            </React.Fragment>
          )}
          <br />
          <br />

          <Button
            variant="contained"
            color="primary"
            disabled={disabled || !dirty || inputDisabled || invalid}
            onClick={this.onSave}
          >
            Сохранить
          </Button>

          {invalid && (
            <Typography color="error">
              В форме есть ошибки. Исправьте их и заказ можно будет сохранить.
            </Typography>
          )}

          {submitting && <CircularProgress />}
        </div>
      </div>
    );
  }
}

export default reduxForm({
  form: FORM_NAME,
  validate: validateMainInfo,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
})(OrderMainInfoForm);
