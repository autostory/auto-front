import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import isEmpty from 'lodash/isEmpty';
import isString from 'lodash/isString';

import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Tooltip from '@material-ui/core/Tooltip';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import HelpIcon from '@material-ui/icons/Help';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Relay from 'react-relay/classic';
import { DONE, REJECT } from '../../../../../../common-code/constants/orderStatusTypes';
import { getPaymentTypeName } from '../../../../../../common-code/constants/paymentTypes';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import AddPaymentDialog from '../../../Order/AddPaymentDialog';

const rawStyle = {
  paper: {
    overflow: 'auto',
  },
  totalRow: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  addButton: {
    flex: 1,
    maxWidth: 400,
  },
  textCentered: {
    textAlign: 'center',
  },
  helpPointIcon: {
    color: '#7e7e7e',
    verticalAlign: 'bottom',
    width: 20,
    height: 20,
    marginLeft: 10,
  },
  helpPointTooltip: {
    fontSize: 14,
    padding: 7,
    lineHeight: '21px',
  },
};

@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          ${AddPaymentDialog.getFragment('order')}
          id
          client {
              points
          }
          clientWillReceivePointAmount
          payments {
              id
              note
              amount
              cashier {
                  id
                  fullName
              }
              makeAt {
                  formattedDateTime
              }
              paymentType
          }
          office {
              cashBackPercent
          }
          serviceWorkTotalBill
          paymentsSum
          totalBill
      }
  `,
})
class OrderPaymentsTable extends PureComponent {
  static defaultProps = {
    disabled: false,
  };

  static propTypes = {
    disabled: PropTypes.bool,
    order: PropTypes.any,
  };

  state = {
    showAddPaymentDialog: false,
  };

  hideAddPaymentDialog = () => {
    this.setState({
      showAddPaymentDialog: false,
    });
  };

  showAddPaymentDialog = () => {
    this.setState({
      showAddPaymentDialog: true,
    });
  };

  render() {
    const { order } = this.props;
    const { payments, paymentsSum, totalBill } = order;
    const { disabled } = this.props;
    const { showAddPaymentDialog } = this.state;

    const { clientWillReceivePointAmount } = order;

    // TODO: Указить тут нормальную формулу расчета баллов

    return (
      <div>
        <AddPaymentDialog
          order={order}
          open={showAddPaymentDialog}
          onRequestClose={this.hideAddPaymentDialog}
        />
        <Paper style={rawStyle.paper}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>#</TableCell>
                <TableCell>Сумма</TableCell>
                <TableCell>Тип оплаты</TableCell>
                <TableCell>Дата</TableCell>
                <TableCell>Кассир</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {payments.length > 0 && payments.map((orderPayment, index) => (
                <TableRow key={orderPayment.id}>
                  <TableCell>{index + 1}</TableCell>
                  <TableCell numeric>{orderPayment.amount} р.</TableCell>
                  <TableCell>
                    {getPaymentTypeName(orderPayment.paymentType)}
                    {(!isEmpty(orderPayment.note) && isString(orderPayment.note)) && (
                      <Typography
                        style={{
                          maxWidth: 400,
                          wordWrap: 'break-word',
                        }}
                      >
                        заметка:{' '}<i>{orderPayment.note}</i>
                      </Typography>
                    )}
                  </TableCell>
                  <TableCell>{orderPayment.makeAt.formattedDateTime}</TableCell>
                  <TableCell>{orderPayment.cashier.fullName}</TableCell>
                </TableRow>
              ))}
              {payments.length === 0 && (
                <TableRow>
                  <TableCell colSpan={5}>нет платежей</TableCell>
                </TableRow>
              )}
              {!disabled && (
                <TableRow>
                  <TableCell colSpan={5}>
                    <div style={rawStyle.totalRow}>
                      <Button
                        variant="contained"
                        color="primary"
                        disabled={disabled || (payments.length > 0 && (totalBill - paymentsSum <= 0))}
                        style={rawStyle.addButton}
                        onClick={this.showAddPaymentDialog}
                      >
                        Провести оплату
                      </Button>
                      <div>
                        {totalBill > 0 && <div>Счет: {totalBill} р.</div>}
                        {payments.length > 0 && <div>Всего оплачено: {paymentsSum} р.</div>}
                        {payments.length > 0 && (totalBill - paymentsSum > 0) && (
                          <div>Долг: {parseFloat((totalBill - paymentsSum).toFixed(2))} р.</div>
                        )}
                        {payments.length > 0 && (totalBill - paymentsSum <= 0) && <div><b>Оплачен полностью</b></div>}
                        {order.status !== DONE && order.status !== REJECT && clientWillReceivePointAmount > 0 && (
                          <div>
                            Клиенту будет начислено {clientWillReceivePointAmount} баллов
                            <Tooltip
                              title={(
                                <div style={rawStyle.helpPointTooltip}>
                                  <div>Баллы начисляются только со стоимости работ.</div>
                                  <div>Возращается {order.office.cashBackPercent}%</div>
                                </div>
                              )}
                            >
                              <HelpIcon style={rawStyle.helpPointIcon} />
                            </Tooltip>
                          </div>
                        )}
                      </div>
                    </div>
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </Paper>
      </div>
    );
  }
}

export default OrderPaymentsTable;
