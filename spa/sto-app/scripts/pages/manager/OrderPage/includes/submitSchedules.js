import cloneDeep from 'lodash/cloneDeep';
import map from 'lodash/map';
import isUndefined from 'lodash/isUndefined';
import EditOrderSchedulesMutation from '../../../../relay/mutation/order/EditOrderSchedulesMutation';
import commitMutationPromise from '../../../../../../common-code/relay/commitMutationPromise';

function submitSchedules(order, values) {
  const input = cloneDeep(values);
  input.schedules = map(input.schedules, schedule => {
    if (!isUndefined(schedule.status)) {
      delete (schedule.status);
    }

    return ({
      ...schedule,
      start: parseInt((schedule.start / 1000), 10),
      finish: parseInt((schedule.finish / 1000), 10),
    });
  });

  return commitMutationPromise(EditOrderSchedulesMutation, { order, input });
}

export default submitSchedules;
