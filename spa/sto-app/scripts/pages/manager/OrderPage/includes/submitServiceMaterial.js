import EditOrderServiceMaterialMutation from '../../../../relay/mutation/order/EditOrderServiceMaterialMutation';
import commitMutationPromise from '../../../../../../common-code/relay/commitMutationPromise';

function submitServiceMaterial(order, values) {
  return commitMutationPromise(EditOrderServiceMaterialMutation, { order, input: values });
}

export default submitServiceMaterial;
