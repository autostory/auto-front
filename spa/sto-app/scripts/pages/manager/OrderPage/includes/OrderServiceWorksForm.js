import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import uniq from 'lodash/uniq';
import isUndefined from 'lodash/isUndefined';
import concat from 'lodash/concat';
import isEqual from 'lodash/isEqual';
import map from 'lodash/map';
import { withStyles } from '@material-ui/core/styles/index';
import get from 'lodash/get';
import { Prompt } from 'react-router-dom';

import CircularProgress from '@material-ui/core/CircularProgress';
import Relay from 'react-relay/classic';
import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';

import SetScheduleStatusMutation from './../../../../relay/mutation/schedule/SetScheduleStatusMutation';
import CarWorkEditableTable from '../../../wfm/Dialogs/includes/CarWorkEditableTable';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';

export const FORM_NAME = 'EditOrderForm';

const styles = {
  paper: {
    padding: 24,
  },
  statusSelect: {
    minWidth: 400,
  },
};

const selector = formValueSelector(FORM_NAME);

const mapServiceWorkToForm = serviceWorks => map(serviceWorks, serviceWork => ({
  id: serviceWork.id,
  serviceWorkActionTypeId: get(serviceWork, 'action.id', null),
  sparePartNodeId: get(serviceWork, 'sparePartNode.id', null),
  masterId: get(serviceWork, 'master.id', null),
  name: serviceWork.note,
  costPerUnit: serviceWork.costPerUnit,
  amount: serviceWork.amount,
  laborHour: serviceWork.laborHour,
  discountPercent: serviceWork.discountPercent,
  status: serviceWork.status,
}));

const parseOrderForForm = order => ({
  serviceWorks: mapServiceWorkToForm(order.serviceWorks),
  deletingServiceWorksIds: [],
});

@withStyles(styles)
@connect(state => ({
  clientId: selector(state, 'clientId'),
  carId: selector(state, 'carId'),
  serviceWorks: selector(state, 'serviceWorks'),
  serviceMaterials: selector(state, 'serviceMaterials'),
  phoneSameAsInProfile: selector(state, 'phoneSameAsInProfile'),
  deletingServiceWorksIds: selector(state, 'deletingServiceWorksIds'),
  deletingServiceMaterialsIds: selector(state, 'deletingServiceMaterialsIds'),
}))
@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          ${SetScheduleStatusMutation.getFragment('order')}
          id
          serviceWorks {
              shortDescription
              status
              id
              sparePartNode {
                  id
                  name
              }
              status
              amount
              totalCost
              costPerUnit
              action {
                  id
                  name
              }
              discountPercent
              note
              laborHour
              master {
                  id
                  fullName
              }
          }
      }
  `,
})
class OrderServiceWorksForm extends PureComponent {
  static defaultProps = {
    disabled: false,
  };

  static propTypes = {
    disabled: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    const { order } = props;
    props.initialize(parseOrderForForm(order));
  }

  componentDidUpdate = (prevProps) => {
    const { order } = this.props;

    if (prevProps.submitSucceeded !== this.props.submitSucceeded) {
      this.props.initialize(parseOrderForForm(order));
    }

    const { handleSubmit, submitSucceeded, dirty } = this.props;

    if (
      !submitSucceeded
      && dirty
      && !isUndefined(prevProps.serviceWorks)
      && !isEqual(this.props.serviceWorks, prevProps.serviceWorks)
    ) {
      handleSubmit();
    }

    if (
      !submitSucceeded
      && dirty
      && !isUndefined(prevProps.deletingServiceWorksIds)
      && !isEqual(this.props.deletingServiceWorksIds, prevProps.deletingServiceWorksIds)
    ) {
      handleSubmit();
    }

    if (!isEqual(order.serviceWorks, prevProps.order.serviceWorks)) {
      this.props.initialize(parseOrderForForm(order));
    }
  };

  onServiceWorkChange = (serviceWorks, deletingServiceWorksIds) => {
    if (!isEqual(this.props.serviceWorks, serviceWorks)) {
      this.props.change('serviceWorks', serviceWorks);
    }

    if (deletingServiceWorksIds.length) {
      this.props.change('deletingServiceWorksIds', uniq(concat(this.props.deletingServiceWorksIds, deletingServiceWorksIds)));
    }
  };

  renderLoading = () => <CircularProgress />;

  render() {
    const {
      serviceWorks, dirty, disabled, officeId,
    } = this.props;

    return (
      <div>
        <Prompt
          when={dirty}
          message={() =>
            'На странице есть не сохраненные изменения. Вы действительно хотите покинуть страницу?'
          }
        />
        <CarWorkEditableTable
          disabled={disabled}
          serviceWorks={serviceWorks}
          initialVariables={{ officeId }}
          renderLoading={this.renderLoading}
          officeId={officeId}
          onChange={this.onServiceWorkChange}
        />
      </div>
    );
  }
}

export default reduxForm({
  form: FORM_NAME,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
})(OrderServiceWorksForm);
