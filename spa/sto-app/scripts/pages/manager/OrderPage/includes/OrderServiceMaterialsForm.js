import PropTypes from 'prop-types';
import isUndefined from 'lodash/isUndefined';

import React, { PureComponent } from 'react';
import uniq from 'lodash/uniq';
import concat from 'lodash/concat';
import map from 'lodash/map';
import get from 'lodash/get';
import isEqual from 'lodash/isEqual';
import { Prompt } from 'react-router-dom';

import CircularProgress from '@material-ui/core/CircularProgress';
import Relay from 'react-relay/classic';
import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';

import validateMainInfo from './validateMainInfo';
import CarWorkSparePartsEditableTable from '../../../wfm/Dialogs/includes/CarWorkSparePartsEditableTable';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';

export const FORM_NAME = 'EditOrderServiceMaterialsForm';

const selector = formValueSelector(FORM_NAME);

const mapServiceMaterialToForm = serviceMaterials => map(serviceMaterials, serviceMaterial => ({
  id: serviceMaterial.id,
  unitMeasureTypeId: get(serviceMaterial, 'unitMeasureType.id', null),
  name: serviceMaterial.name,
  amount: serviceMaterial.amount,
  costPerUnit: serviceMaterial.costPerUnit,
  discountPercent: serviceMaterial.discountPercent,
}));

const parseOrderForForm = order => ({
  serviceMaterials: mapServiceMaterialToForm(order.serviceMaterials),
  deletingServiceMaterialsIds: [],
});

@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          id
          serviceMaterials {
              id
              amount
              name
              costPerUnit
              totalCost
              discountPercent
              unitMeasureType {
                  id
                  name
              }
          }
      }
  `,
})
@connect(state => ({
  serviceMaterials: selector(state, 'serviceMaterials'),
  deletingServiceMaterialsIds: selector(state, 'deletingServiceMaterialsIds'),
}))
class OrderServiceMaterialsForm extends PureComponent {
  static defaultProps = {
    disabled: false,
  };

  static propTypes = {
    disabled: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    const { order, initialize } = props;

    initialize(parseOrderForForm(order));
  }

  componentDidUpdate = (prevProps) => {
    const { submitSucceeded, order, initialize, dirty } = this.props;
    if (prevProps.submitSucceeded !== submitSucceeded) {
      initialize(parseOrderForForm(order));
    }

    const { handleSubmit } = this.props;

    if (
      !submitSucceeded
      && dirty
      && !isUndefined(prevProps.serviceMaterials)
      && !isEqual(this.props.serviceMaterials, prevProps.serviceMaterials)
    ) {
      handleSubmit();
    }

    if (
      !submitSucceeded
      && dirty
      && !isUndefined(prevProps.deletingServiceMaterialsIds)
      && !isEqual(this.props.deletingServiceMaterialsIds, prevProps.deletingServiceMaterialsIds)
    ) {
      handleSubmit();
    }
  };

  onServiceMaterialChange = (serviceMaterials, deletingServiceMaterialsIds) => {
    const {
      serviceMaterials: currentServiceMaterials,
      deletingServiceMaterialsIds: currentDeletingServiceMaterialsIds,
      change,
    } = this.props;

    if (!isEqual(currentServiceMaterials, serviceMaterials)) {
      change('serviceMaterials', serviceMaterials);
    }

    if (deletingServiceMaterialsIds.length) {
      change('deletingServiceMaterialsIds', uniq(concat(currentDeletingServiceMaterialsIds, deletingServiceMaterialsIds)));
    }
  };

  renderLoading = () => <CircularProgress />;

  render() {
    const { serviceMaterials, dirty, disabled } = this.props;

    return (
      <div>
        <Prompt
          when={dirty}
          message={() =>
            'На странице есть не сохраненные изменения. Вы действительно хотите покинуть страницу?'
          }
        />

        <CarWorkSparePartsEditableTable
          disabled={disabled}
          serviceMaterials={serviceMaterials}
          renderLoading={this.renderLoading}
          onChange={this.onServiceMaterialChange}
        />
      </div>
    );
  }
}

export default reduxForm({
  form: FORM_NAME,
  validate: validateMainInfo,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
})(OrderServiceMaterialsForm);
