import React from 'react';
import pdfMake from 'pdfmake/build/pdfmake';
import vfsFonts from 'pdfmake/build/vfs_fonts';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';
import { WAIT_CAR_ARRIVE, WAIT_CAR_RETURN } from '../../../../../../common-code/constants/orderStatusTypes';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import { fromGlobalId } from '../../../../../../common-code/relay/globalIdUtils';
import { makePdfActParams } from '../../../../../../common-code/utils/pdf/makePdfActParams';
import MarkWorkOnScheduleAsCompletedDialog from '../../../wfm/Dialogs/MarkWorkOnScheduleAsCompletedDialog';
import OrderActionButton from '../../../wfm/Orders/OrderActionButton';
import SetOrderFactCarArriveDateTimeMutation
  from '../../../../relay/mutation/order/SetOrderFactCarArriveDateTimeMutation';
import SetOrderFactCarReturnDateTimeMutation
  from '../../../../relay/mutation/order/SetOrderFactCarReturnDateTimeMutation';
import setOrderFactCarArriveDateTime from '../../../wfm/Orders/setOrderFactCarArriveDateTime';
import setOrderFactCarReturnDateTime from '../../../wfm/Orders/setOrderFactCarReturnDateTime';
import CarReturnAction from '../ReturnCarDialog/CarReturnAction';

const ALERT_CAR_DONT_ARRIVE = 300; // 5 минут.

@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          id
          status
          totalBill
          paymentsSum
          office {
              id
          }
          makeAt {
              timestamp
          }
          factClientVisitDateTime {
              timestamp
          }
          factCarReturnDateTime {
              timestamp
          }
          office {
              id
              title
              address
          }
          firm {
              id
              name
          }
          serviceWorks {
              id
              amount
              discountPercent
              totalCost
              costPerUnit
              sparePartNode {
                  id
                  name
              }
              note
              action {
                  id
                  name
              }
          }
          client {
              id
              fullName
          }
          serviceMaterials {
              id
              name
              amount
              totalCost
              costPerUnit
              discountPercent
              unitMeasureType {
                  id
                  name
              }
          }
          ${SetOrderFactCarArriveDateTimeMutation.getFragment('order')}
          ${SetOrderFactCarReturnDateTimeMutation.getFragment('order')}
          ${CarReturnAction.getFragment('order')}
      }
  `,
})
class CarArriveButtons extends React.PureComponent {
  static defaultProps = {
    classes: {},
  };

  static propTypes = {
    classes: PropTypes.object,
  };

  state = {
    scheduleUsedInDialogWindow: null,
    markWorkOnScheduleAsCompletedDialogOpen: false,
    requestPending: false,
  };

  onRequestEnd = () => {
    this.setState({
      requestPending: false,
    });
  };

  onCarArriveClick = (event) => {
    const { order } = this.props;

    this.setState({
      requestPending: true,
    }, this.sendOrderFactCarArriveDateTime(order));

    event.preventDefault();
    return false;
  };

  onCarReturnClick = (event) => {
    const { order } = this.props;
    const orderId = fromGlobalId(order.id).id;

    const { vfs } = vfsFonts.pdfMake;
    pdfMake.vfs = vfs;
    pdfMake
      .createPdf(makePdfActParams({ order }))
      .download(`Акт выполненных работ по заказу №${orderId}`);

    this.setState({
      requestPending: true,
    }, this.setOrderFactCarReturnDateTime(order));

    event.preventDefault();
    return false;
  };

  setOrderFactCarReturnDateTime = order => () => {
    const date = parseInt(Date.now() / 1000, 10);

    setOrderFactCarReturnDateTime(order, { factCarReturnDateTime: date })
      .then(this.onRequestEnd, this.onRequestEnd);
  };

  sendOrderFactCarArriveDateTime = order => () => {
    const date = parseInt(Date.now() / 1000, 10);

    setOrderFactCarArriveDateTime(order, { factClientVisitDateTime: date })
      .then(this.onRequestEnd, this.onRequestEnd);
  };

  openMarkWorkOnScheduleAsCompletedDialog = (scheduleId) => {
    this.setState({
      scheduleUsedInDialogWindow: scheduleId,
      markWorkOnScheduleAsCompletedDialogOpen: true,
    });
  };

  closeMarkWorkOnScheduleAsCompletedDialog = () => {
    this.setState({
      scheduleUsedInDialogWindow: null,
      markWorkOnScheduleAsCompletedDialogOpen: false,
    });
  };

  render() {
    const { classes, order } = this.props;
    const {
      requestPending, scheduleUsedInDialogWindow, markWorkOnScheduleAsCompletedDialogOpen,
    } = this.state;
    const schedule = false;

    let overdue = false;
    if (schedule && schedule.finish.timestamp < parseInt(Date.now() / 1000, 10)) {
      overdue = true;
    }

    let alert = false;
    // За 5 минут до того как работы надо начать отмечаем что надо отметить прибытие тачки
    if (
      order.status === WAIT_CAR_ARRIVE
      && (schedule && (schedule.start.timestamp - ALERT_CAR_DONT_ARRIVE) >= parseInt(Date.now() / 1000, 10))
    ) {
      alert = true;
    }

    const waitCarArrive = order.status === WAIT_CAR_ARRIVE;

    return (
      <div>
        <MarkWorkOnScheduleAsCompletedDialog
          scheduleId={scheduleUsedInDialogWindow}
          onRequestClose={this.closeMarkWorkOnScheduleAsCompletedDialog}
          open={markWorkOnScheduleAsCompletedDialogOpen}
        />
        {waitCarArrive && (
          <OrderActionButton
            disabled={requestPending}
            pending={requestPending}
            alert={overdue || alert}
            onClick={this.onCarArriveClick}
            title="Авто прибыло в СТО"
          />
        )}
        {order.status === WAIT_CAR_RETURN && (
          <CarReturnAction order={order}>
            <OrderActionButton
              disabled={requestPending}
              pending={requestPending}
              alert={false}
              onClick={this.onCarReturnClick}
              title="В акте все правильно, вернуть авто клиенту и получить акт"
            />
          </CarReturnAction>
        )}
      </div>
    );
  }
}

export default CarArriveButtons;
