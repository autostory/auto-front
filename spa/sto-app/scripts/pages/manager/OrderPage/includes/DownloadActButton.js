import React from 'react';
import Relay from 'react-relay/classic';
import pdfMake from 'pdfmake/build/pdfmake';
import vfsFonts from 'pdfmake/build/vfs_fonts';
import Button from '@material-ui/core/Button';
import { DONE, WAIT_CAR_RETURN, WAIT_PAYMENT } from '../../../../../../common-code/constants/orderStatusTypes';

import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import { fromGlobalId } from '../../../../../../common-code/relay/globalIdUtils';
import { makePdfActParams } from '../../../../../../common-code/utils/pdf/makePdfActParams';

@relayContainer({
  order: () => Relay.QL`
      fragment on Order {
          id
          status
          totalBill
          paymentsSum
          office {
              id
              title
              address
          }
          firm {
              id
              name
          }
          office {
              id
              address
              title
          }
          makeAt {
              timestamp
          }
          serviceWorks {
              id
              amount
              discountPercent
              totalCost
              costPerUnit
              sparePartNode {
                  id
                  name
              }
              note
              action {
                  id
                  name
              }
          }
          client {
              id
              fullName
          }
          serviceMaterials {
              id
              name
              amount
              totalCost
              costPerUnit
              discountPercent
              unitMeasureType {
                  id
                  name
              }
          }
      }
  `,
})
class DownloadActButton extends React.PureComponent {
  downloadAct = () => {
    const { order } = this.props;
    const orderId = fromGlobalId(order.id).id;

    const { vfs } = vfsFonts.pdfMake;
    pdfMake.vfs = vfs;
    pdfMake.createPdf(makePdfActParams({ order })).download(`Акт выполненных работ по заказу SW-${orderId}`);
  };

  render() {
    const { order } = this.props;

    if (order.status !== WAIT_PAYMENT && order.status !== DONE) {
      return null;
    }

    return (
      <Button
        variant="contained"
        color="primary"
        onClick={this.downloadAct}
      >
        Скачать акт
      </Button>
    );
  }
}

export default DownloadActButton;
