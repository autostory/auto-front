import EditOrderMainInfoMutation from '../../../../relay/mutation/order/EditOrderMainInfoMutation';
import commitMutationPromise from '../../../../../../common-code/relay/commitMutationPromise';

function submitMainInfo(order, values) {
  return commitMutationPromise(EditOrderMainInfoMutation, { order, input: values });
}

export default submitMainInfo;
