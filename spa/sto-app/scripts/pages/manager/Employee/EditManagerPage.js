import React, { Component } from 'react';
import Relay from 'react-relay/classic';
import withProps from 'recompose/withProps';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withRouter } from 'react-router';
import PageBase from '../../../components/main/PageBase';
import EditManagerForm from '../../../components/manager/Employee/Edit/EditManagerForm';
import relaySuperContainer from '../../../../../common-code/decorators/relaySuperContainer';
import submit from '../../../components/manager/Employee/Edit/submit';
import EditManagerMutation from '../../../relay/mutation/employee/EditManagerMutation';

@withProps({ renderLoading: () => <CircularProgress /> })
@relaySuperContainer(
  {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
            manager (managerId: $managerId) @include(if: $managerIdNotEmpty) {
                id
                active
                email
                firstName
                surname
                patronymic
                phone
                ${EditManagerMutation.getFragment('manager')}
            }
        }
    `,
  },
  {
    managerId: null,
    managerIdNotEmpty: false,
  },
)
@withRouter
class EditManagerPage extends Component {
  constructor(props) {
    super(props);
    const { match, relay } = props;
    const managerId = match.params.id;

    relay.setVariables({
      managerId,
      managerIdNotEmpty: true,
    });
  }

  onManagerFormSubmit = (values) => {
    const { viewer } = this.props;
    const { manager } = viewer;

    return submit(manager, values)
      // .then(() => {
      //   this.props.history.push('/my-firm');
      // })
  };

  getForm = () => {
    const { viewer } = this.props;
    const { manager } = viewer;

    const initialValues = {
      firstName: manager.firstName,
      surname: manager.surname,
      patronymic: manager.patronymic,
      email: manager.email,
      phone: manager.phone,
    };

    return (
      <EditManagerForm
        onSubmit={this.onManagerFormSubmit}
        initialValues={initialValues}
      />
    );
  };

  render() {
    const { viewer } = this.props;
    const { manager } = viewer;

    return (
      <PageBase title="Редактирование сотрудника">
        <div>
          {manager && (this.getForm())}
          {!manager && (
            <div><CircularProgress /> Загрузка формы...</div>
          )}
        </div>
      </PageBase>
    );
  }
}

export default EditManagerPage;
