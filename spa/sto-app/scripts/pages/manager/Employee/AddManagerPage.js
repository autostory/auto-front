import React, { Component } from 'react';
import Relay from 'react-relay/classic';
import withProps from 'recompose/withProps';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withRouter } from 'react-router';
import PageBase from '../../../components/main/PageBase';
import AddManagerForm from '../../../components/manager/Employee/Add/AddManagerForm';
import relaySuperContainer from '../../../../../common-code/decorators/relaySuperContainer';
import submitManagerInfo from '../../../components/manager/Employee/Add/submitManagerInfo';
import CreateManagerMutation from '../../../relay/mutation/employee/CreateManagerMutation';

@withProps({ renderLoading: () => <CircularProgress /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          ${AddManagerForm.getFragment('viewer')}
          ${CreateManagerMutation.getFragment('viewer')}
      }
  `,
})
@withRouter
class AddManagerPage extends Component {
  onManagerFormSubmit = (values) => {
    const { viewer } = this.props;

    return submitManagerInfo(viewer, values)
      .then(() => {
        this.props.history.push('/my-firm');
      });
  };

  render() {
    return (
      <PageBase title="Добавление сотрудника">
        <AddManagerForm
          onSubmit={this.onManagerFormSubmit}
          viewer={this.props.viewer}
        />
      </PageBase>
    );
  }
}

export default AddManagerPage;
