import React, { Component } from 'react';
import Relay from 'react-relay/classic';
import withProps from 'recompose/withProps';
import filter from 'lodash/filter';
import each from 'lodash/each';
import map from 'lodash/map';
import get from 'lodash/get';
import CircularProgress from '@material-ui/core/CircularProgress';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';

import relaySuperContainer from '../../../../common-code/decorators/relaySuperContainer';

const rawStyle = {
  officeTitle: {
    maxWidth: 300,
    display: 'block',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  header: {
    padding: 12,
    background: '#F5F5F5',
  },
  content: {
    padding: 12,
  },
  container: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  paper: {
    margin: 6,
    minWidth: 320,
  },
  row: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between',
  },
  divider: {
    marginTop: 24,
  },
};

@withProps({ renderLoading: () => <CircularProgress /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          myFirm {
              offices {
                  officesList {
                      edges {
                          node {
                              id
                              title
                              active
                              totalOrderPaymentDebt
                              orderCountInWaitPaymentStatus
                              dailyOfficeReport {
                                  createdOrderCount
                                  doneOrderCount
                                  totalRevenues
                              }
                          }
                      }
                  }
              }
          }
      }
  `,
})
@withRouter
class Statistics extends Component {
  render() {
    const { viewer } = this.props;

    const offices = filter(map(get(viewer, 'myFirm.offices.officesList.edges'), 'node'), office => office.active);

    let allOfficesTotalRevenues = 0;
    each(offices, (office) => {
      allOfficesTotalRevenues += office.dailyOfficeReport.totalRevenues;
    });

    return (
      <div>
        <div style={rawStyle.container}>
          {map(offices, office => (
            <Paper style={rawStyle.paper} key={office.id}>
              <div style={rawStyle.header}>
                <Typography
                  variant="title"
                  style={rawStyle.officeTitle}
                  title={office.title}
                >
                  {office.title}
                </Typography>
              </div>
              <div style={rawStyle.content}>
                <Typography variant="headline">За все время</Typography>
                <Typography style={rawStyle.row} component="div">
                  <div>текущий долг по заказам</div>
                  <div>{office.totalOrderPaymentDebt} р.</div>
                </Typography>
                <Typography style={rawStyle.row} component="div">
                  <div>всего заказов ожидающих оплату</div>
                  <div>{office.orderCountInWaitPaymentStatus}</div>
                </Typography>
                <br />
                <Typography variant="headline">За сегодня</Typography>
                <Typography style={rawStyle.row} component="div">
                  <div><Link to={`/wfm/${office.id}`}>Заказов поступило</Link></div>
                  <div>{office.dailyOfficeReport.createdOrderCount}</div>
                </Typography>
                <Typography style={rawStyle.row} component="div">
                  <div><Link to="/orders">Заказов выполнено</Link></div>
                  <div>{office.dailyOfficeReport.doneOrderCount}</div>
                </Typography>
                <Typography style={rawStyle.row} component="div">
                  <div><Link to="/my-firm/payments">Выручка</Link></div>
                  <div>{office.dailyOfficeReport.totalRevenues} р.</div>
                </Typography>
              </div>
            </Paper>
          ))}
        </div>
        <Divider style={rawStyle.divider} />
        <Typography variant="headline">
          Суммарная выручка за сегодня {allOfficesTotalRevenues} р.
        </Typography>
      </div>
    );
  }
}

export default Statistics;
