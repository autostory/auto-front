import React, { Component } from 'react';
import ConfiguredRadium from '../../radium/ConfiguredRadium';
import PageBase from '../../components/main/PageBase';
import MyProfile from '../../components/myProfile/MyProfile';

@ConfiguredRadium
class ProfilePage extends Component {
  render() {
    return (
      <PageBase title="Мой профиль">
        <MyProfile />
      </PageBase>
    );
  }
}

export default ProfilePage;
