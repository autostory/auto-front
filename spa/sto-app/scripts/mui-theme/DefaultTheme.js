import { createMuiTheme } from '@material-ui/core/styles';
import { blue, grey } from '@material-ui/core/colors';
// import lightBaseTheme from '@material-ui/core/styles/baseThemes/lightBaseTheme';
// import darkBaseTheme from '@material-ui/core/styles/baseThemes/darkBaseTheme';

const themeDefault = createMuiTheme({
  palette: {
    primary: {
      light: '#757ce8',
      main: '#3f50b5',
      dark: '#002884',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ff7961',
      main: '#f44336',
      dark: '#ba000d',
      contrastText: '#000',
    },
  }, // http://www.material-ui.com/#/customization/themes
  appBar: {
    // height: 57,
    // color: blue['600'],
  },
  drawer: {
    // width: 230,
    // color: grey['900'],
  },
  raisedButton: {
    // primaryColor: blue['600'],
  },
});

export default themeDefault;
