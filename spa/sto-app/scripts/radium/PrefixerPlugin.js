import Prefixer from 'inline-style-prefixer';

// Checks the DOM for a value that sticks, given a CSS property
function getSupportedStyleValue(property, values) {
  const element = document.createElement('span');

  return values.find((value) => {
    element.style[property] = value;

    return element.style[property] === value;
  });
}

// Styles with multiple values (eg. display: 'flex') get turned into arrays
// if a single solution can't be found. Use the 'empty span' hack to determine
// which one to use
function patchMultiValues(style, key) {
  if (typeof key !== 'undefined' && Array.isArray(style[key])) {
    const supportedValue = getSupportedStyleValue(key, style[key].reverse());

    // we are mutating param during this traversal for performance
    style[key] = supportedValue;
  } else if (typeof key === 'undefined') {
    Object.keys(style).forEach((childKey) => {
      patchMultiValues(style, childKey);
    });
  } else if (typeof style[key] === 'object') {
    Object.keys(style[key]).forEach((childKey) => {
      patchMultiValues(style[key], childKey);
    });
  }

  return style;
}

export default (config) => {
  const prefixer = new Prefixer();

  return {
    style: patchMultiValues(prefixer.prefix(config.style)),
  };
};
