import Radium from 'radium';
import PrefixerPlugin from './PrefixerPlugin';

export default function ConfiguredRadium(component) {
  return Radium({
    plugins: [
      Radium.Plugins.mergeStyleArray,
      Radium.Plugins.checkProps,
      Radium.Plugins.resolveMediaQueries,
      Radium.Plugins.resolveInteractionStyles,
      Radium.Plugins.keyframes,
      Radium.Plugins.visited,
      Radium.Plugins.removeNestedStyles,
      PrefixerPlugin,
      Radium.Plugins.checkProps,
    ],
  })(component);
}
