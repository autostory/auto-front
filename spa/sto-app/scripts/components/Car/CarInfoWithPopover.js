/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import map from 'lodash/map';
import isUndefined from 'lodash/isUndefined';
import isNull from 'lodash/isNull';
import Relay from 'react-relay/classic';
import { Link } from 'react-router-dom';
import Popover from '@material-ui/core/Popover';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Highlighter from 'react-highlight-words';

import relayContainer from '../../../../common-code/decorators/relayContainer';
import ConfiguredRadium from '../../radium/ConfiguredRadium';

const style = {
  tth: {
    marginRight: 10,
  },
  actions: {
    marginTop: 10,
  },
  container: {
    padding: 18,
  },
  mainInfoContainer: {
    display: 'inline-flex',
    flexDirection: 'column',
    cursor: 'pointer',
    textDecoration: 'underline dashed',
    ':hover': {
      textDecoration: 'none',
    },
  },
  carNumber: {
    // paddingLeft: 6,
  },
};

const anchorOrigin = {
  vertical: 'bottom',
  horizontal: 'center',
};

const transformOrigin = {
  vertical: 'top',
  horizontal: 'center',
};

const rawStyle = {
  highlighter: {
    background: '#FAD65E',
    padding: '4px 0',
  },
};

const Match = ({ keywordArray, text, ...rest }) => (
  <Highlighter
    highlightStyle={rawStyle.highlighter}
    searchWords={keywordArray}
    autoEscape
    textToHighlight={text || ''}
    {...rest}
  />
);

@relayContainer({
  car: () => Relay.QL`
      fragment on Car {
          id
          displayName
          registrationSignNumber
          vin
          color
          year
          owners {
              ownersList {
                  edges {
                      node {
                          id
                          fullName
                      }
                  }
              }
          }
      }
  `,
})
@ConfiguredRadium
class CarInfoWithPopover extends React.PureComponent {
  static defaultProps = {
    match: '',
  };

  static propTypes = {
    car: PropTypes.any,
    match: PropTypes.string,
  };

  state = {
    popoverVisible: false,
    anchorEl: null,
  };

  clickHandler = (event) => {
    this.setState({
      anchorEl: event.currentTarget,
      popoverVisible: !this.state.popoverVisible,
    });
  };

  handleClose = () => {
    this.setState({
      anchorEl: null,
      popoverVisible: false,
    });
  };

  render() {
    const { anchorEl, popoverVisible } = this.state;
    const { car, match } = this.props;

    if (isUndefined(car)) {
      return (
        <i style={{ fontSize: 'smaller' }}>не указано</i>
      );
    }

    const ownersEdges = get(car, 'owners.ownersList.edges');
    const owners = map(ownersEdges, 'node');

    return (
      <div>
        <div style={style.mainInfoContainer} onClick={this.clickHandler}>
          <div>
            {car.displayName}
          </div>
          <div style={style.carNumber}>
            <Match text={car.registrationSignNumber} keywordArray={[match]} />
          </div>
        </div>
        {!isUndefined(car) && (
          <Popover
            open={popoverVisible}
            anchorEl={anchorEl}
            onClose={this.handleClose}
            anchorOrigin={anchorOrigin}
            transformOrigin={transformOrigin}
          >
            <div style={style.container}>
              <Typography>Марка, модель: {car.displayName}</Typography>
              <Typography>Гос. номер: <b>{car.registrationSignNumber}</b></Typography>
              <Typography>VIN: {car.vin}</Typography>
              <Typography>Цвет: {isNull(car.color) && <i>не указано</i>}{!isNull(car.color) && car.color}</Typography>
              <Typography>
                Собственник:{' '}
                {map(owners, owner => (
                  <span key={owner.id}>
                    <Link
                      to={`/client/${owner.id}`}
                      target="_blank"
                    >
                      {owner.fullName}
                    </Link>
                    {' '}
                  </span>
                ))}
              </Typography>
              <Typography style={style.actions}>
                <Button variant="outlined" target="_blank" component={Link} to={`/car/${car.id}`}>
                  Карточка авто
                </Button>
              </Typography>
            </div>
          </Popover>
        )}
      </div>
    );
  }
}

export default CarInfoWithPopover;
