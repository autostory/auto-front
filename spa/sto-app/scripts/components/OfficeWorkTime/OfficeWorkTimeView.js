import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';
import get from 'lodash/get';

import relayContainer from '../../../../common-code/decorators/relayContainer';
import { DAYS_OF_WEEK_NAMES_SHORT, DAYS_OF_WEEK_NAMES_FULL } from '../../../../common-code/constants/daysNames';

@relayContainer({
  office: () => Relay.QL`
      fragment on Office {
          workTimes {
              id
              start {
                  formattedTime
              }
              end {
                  formattedTime
              }
              dayOfWeek
              holiday
              roundTheClock
          }
      }
  `,
})
class OfficeWorkTimeView extends PureComponent {
  static defaultProps = {
    shortNames: false,
  };

  static propTypes = {
    shortNames: PropTypes.bool,
  };

  render() {
    const { office, shortNames } = this.props;
    const { workTimes } = office;

    if (!workTimes.length) {
      return <i>не указано</i>;
    }

    return (
      <table>
        {workTimes.map(workTime => (
          <tr>
            <td>
              {shortNames && DAYS_OF_WEEK_NAMES_SHORT[workTime.dayOfWeek]}
              {!shortNames && DAYS_OF_WEEK_NAMES_FULL[workTime.dayOfWeek]}
            </td>
            <td>
              {!workTime.holiday && !workTime.roundTheClock && (
                <span>{get(workTime, 'start.formattedTime', '?')} - {get(workTime, 'end.formattedTime', '?')}</span>
              )}
              {workTime.holiday && (
                <span>выходной</span>
              )}
              {!workTime.holiday && workTime.roundTheClock && (
                <span>весь день</span>
              )}
            </td>
          </tr>
        ))}
      </table>
    );
  }
}

export default OfficeWorkTimeView;
