import find from 'lodash/find';

const parseRelayWorkTimes = (workTimes) => {
  const result = [];

  for (let i = 1; i < 8; i += 1) {
    const workTime = find(workTimes, { dayOfWeek: i });

    const data = {
      start: {
        formattedDateTime: null,
      },
      end: {
        formattedDateTime: null,
      },
      dayOfWeek: i,
      roundTheClock: true,
      holiday: false,
    };

    if (workTime) {
      if (workTime.start) {
        data.start.formattedDateTime = new Date(workTime.start.formattedDateTime);
      }

      if (workTime.end) {
        data.end.formattedDateTime = new Date(workTime.end.formattedDateTime);
      }

      data.roundTheClock = workTime.roundTheClock;
      data.holiday = workTime.holiday;
    }

    result.push(data);
  }

  return result;
};

export default parseRelayWorkTimes;
