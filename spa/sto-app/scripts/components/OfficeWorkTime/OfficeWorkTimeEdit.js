import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';
import find from 'lodash/find';
import cloneDeep from 'lodash/cloneDeep';
import get from 'lodash/get';
import filter from 'lodash/filter';
import isEmpty from 'lodash/isEmpty';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TimePicker from 'material-ui-pickers/TimePicker';
import { DAYS_OF_WEEK_NAMES_SHORT, DAYS_OF_WEEK_NAMES_FULL } from '../../../../common-code/constants/daysNames';

import relayContainer from '../../../../common-code/decorators/relayContainer';
import parseRelayWorkTimes from './parseRelayWorkTimes';

const rawStyle = {
  timePicker: {
    width: 50,
  },
};

@relayContainer({
  office: () => Relay.QL`
      fragment on Office {
          workTimes {
              id
              start {
                  formattedDateTime
              }
              end {
                  formattedDateTime
              }
              dayOfWeek
              roundTheClock
              holiday
          }
      }
  `,
})
class OfficeWorkTimeEdit extends PureComponent {
  static defaultProps = {
    shortNames: false,
    onErrorChange: () => {
    },
    onChange: () => {
    },
  };

  static propTypes = {
    shortNames: PropTypes.bool,
    onErrorChange: PropTypes.func,
    onChange: PropTypes.func,
  };

  constructor(props) {
    super(props);

    const { office } = props;
    const { workTimes } = office;

    this.state = {
      workTimes: parseRelayWorkTimes(workTimes),
      timeErrors: {
        1: false,
        2: false,
        3: false,
        4: false,
        5: false,
        6: false,
        7: false,
      },
    };
  }

  componentDidMount = () => {
    const { timeErrors, workTimes } = this.state;
    const { onErrorChange, onChange } = this.props;

    onErrorChange(!isEmpty(filter(timeErrors, value => value)));
    onChange(workTimes);

    this.checkErrors();
  };

  componentDidUpdate = () => {
    const { timeErrors, workTimes } = this.state;
    const { onErrorChange, onChange } = this.props;

    onErrorChange(!isEmpty(filter(timeErrors, value => value)));
    onChange(workTimes);
  };

  handleChangeHoliday = dayOfWeek => (event) => {
    const { workTimes } = this.state;
    const workTime = find(workTimes, { dayOfWeek });
    workTime.holiday = event.target.checked;

    this.setState({
      workTimes: cloneDeep(workTimes),
    });
  };

  handleChangeRoundTheClock = dayOfWeek => (event) => {
    const { workTimes } = this.state;
    const workTime = find(workTimes, { dayOfWeek });
    workTime.roundTheClock = event.target.checked;

    this.setState({
      workTimes: cloneDeep(workTimes),
    });
  };

  handleChangeStart = dayOfWeek => (time) => {
    const { workTimes } = this.state;
    const workTime = find(workTimes, { dayOfWeek });
    time.setSeconds(0);
    time.setMilliseconds(0);

    workTime.start = {
      formattedDateTime: time,
    };

    this.setState({
      workTimes: cloneDeep(workTimes),
    });
  };

  handleChangeEnd = dayOfWeek => (time) => {
    const { workTimes } = this.state;
    const workTime = find(workTimes, { dayOfWeek });
    time.setSeconds(0);
    time.setMilliseconds(0);

    workTime.end = {
      formattedDateTime: time,
    };

    this.setState({
      workTimes: cloneDeep(workTimes),
    });
  };

  checkErrors = () => {
    const { timeErrors } = this.state;
    for (let i = 1; i < 8; i += 1) {
      timeErrors[i] = this.startError(i) || this.endError(i);
    }

    this.setState({
      timeErrors,
    });
  };

  startError = (dayOfWeek) => {
    const { workTimes } = this.state;
    const workTime = find(workTimes, { dayOfWeek });
    const start = get(workTime, 'start.formattedDateTime', null);
    const end = get(workTime, 'end.formattedDateTime', null);

    if (workTime.holiday || workTime.roundTheClock) {
      return false;
    }

    return !start || (end && start.getTime() >= end.getTime());
  };

  endError = (dayOfWeek) => {
    const { workTimes } = this.state;
    const workTime = find(workTimes, { dayOfWeek });
    const start = get(workTime, 'start.formattedDateTime', null);
    const end = get(workTime, 'end.formattedDateTime', null);

    if (workTime.holiday || workTime.roundTheClock) {
      return false;
    }

    return !end || (start && start.getTime() >= end.getTime());
  };

  render() {
    const { shortNames } = this.props;
    const { workTimes, timeErrors } = this.state;

    const rows = [];

    for (let i = 1; i < 8; i += 1) {
      const data = find(workTimes, { dayOfWeek: i });

      rows.push(
        <div key={i}>
          {shortNames && DAYS_OF_WEEK_NAMES_SHORT[data.dayOfWeek]}
          {!shortNames && DAYS_OF_WEEK_NAMES_FULL[data.dayOfWeek]}
          {' '}
          c
          {' '}
          <TimePicker
            style={rawStyle.timePicker}
            ampm={false}
            error={timeErrors[data.dayOfWeek]}
            disabled={data.holiday || data.roundTheClock}
            autoOk
            okLabel="OK"
            cancelLabel="Закрыть"
            onChange={this.handleChangeStart(i)}
            value={get(data, 'start.formattedDateTime', null)}
            format="HH:mm"
          />
          {' '}
          до
          {' '}
          <TimePicker
            style={rawStyle.timePicker}
            ampm={false}
            disabled={data.holiday || data.roundTheClock}
            autoOk
            error={timeErrors[data.dayOfWeek]}
            okLabel="OK"
            cancelLabel="Закрыть"
            onChange={this.handleChangeEnd(i)}
            value={get(data, 'end.formattedDateTime', null)}
            format="HH:mm"
          />
          <FormControlLabel
            control={
              <Checkbox
                checked={data.roundTheClock}
                color="primary"
                disabled={data.holiday}
                onChange={this.handleChangeRoundTheClock(i)}
              />
            }
            label="весь день"
          />
          <FormControlLabel
            control={
              <Checkbox
                checked={data.holiday}
                color="primary"
                onChange={this.handleChangeHoliday(i)}
              />
            }
            label="выходной"
          />
        </div>,
      );
    }

    return (
      <div>
        {rows}
      </div>
    );
  }
}

export default OfficeWorkTimeEdit;
