import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';
import globalStyles from '../../../../common-code/globalStyles';
import { setPageHeader } from '../../reduxActions/ChangePageHeaderActions';

@connect(
  state => ({}),
  dispatch => ({
    setPageHeader: header => dispatch(setPageHeader(header)),
  }),
)
class PageBase extends React.PureComponent {
  static defaultProps = {
    title: '',
    withPaper: true,
  };

  static propTypes = {
    children: PropTypes.any,
    title: PropTypes.string,
    withPaper: PropTypes.bool,
  };

  constructor(props) {
    super(props);
    props.setPageHeader(props.title);
  }

  componentDidUpdate = (prevProps) => {
    if (this.props.title !== prevProps.title) {
      this.props.setPageHeader(this.props.title);
    }
  };

  render() {
    const { withPaper } = this.props;

    return (
      <div>
        {withPaper && (
        <Paper style={globalStyles.paper}>
          {this.props.children}
          <div style={globalStyles.clear} />
        </Paper>
        )}
        {!withPaper && this.props.children}
      </div>
    );
  }
}

PageBase.propTypes = {
  children: PropTypes.element,
};

export default PageBase;
