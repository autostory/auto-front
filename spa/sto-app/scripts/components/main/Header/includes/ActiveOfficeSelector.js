import React from 'react';
import { connect } from 'react-redux';
import Relay from 'react-relay/classic';
import get from 'lodash/get';
import map from 'lodash/map';
import find from 'lodash/find';
import { withStyles } from '@material-ui/core/styles';

import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';
import {
  setWFMActiveOfficeId,
} from '../../../../reduxActions/WfmPageActions';

const styles = {
  formControl: {
    maxWidth: 250,
  },
  menuItem: {
    maxWidth: 250,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: 'block',
  },
};

@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          myFirm {
              id
              offices {
                  officesList {
                      edges {
                          node {
                              id
                              title
                          }
                      }
                  }
              }
          }
      }
  `,
})
@connect(
  state => ({
    officeId: state.wfmPageStore.officeId,
  }),
  dispatch => ({
    setWFMActiveOfficeId: officeId => dispatch(setWFMActiveOfficeId(officeId)),
  }),
)
@withStyles(styles)
class ActiveOfficeSelector extends React.Component {
  constructor(props) {
    super(props);
    const offices = map(get(props.viewer, 'myFirm.offices.officesList.edges'), 'node');

    let value = 0;

    if (offices.length) {
      const { officeId } = this.props;

      value = offices[0].id;
      if (officeId !== null) {
        const office = find(offices, { id: officeId });
        if (office) {
          value = officeId;
        }
      }

      if (officeId === null) {
        props.setWFMActiveOfficeId(value);
      }
    }

    this.state = {
      value,
    };
  }

  handleChange = (event) => {
    this.setState({ value: event.target.value });
    this.props.setWFMActiveOfficeId(event.target.value);
  };

  render() {
    const { viewer, classes } = this.props;
    const offices = map(get(viewer, 'myFirm.offices.officesList.edges'), 'node');

    if (!offices.length) {
      return null;
    }

    return (
      <div>
        <FormControl className={classes.formControl}>
          <Select
            value={this.state.value}
            onChange={this.handleChange}
          >
            {offices.map(office => (
              <MenuItem
                title={office.title}
                key={office.id}
                value={office.id}
                className={classes.menuItem}
              >
                {office.title}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
    );
  }
}

export default ActiveOfficeSelector;
