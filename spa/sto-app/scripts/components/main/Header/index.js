import React from 'react';
import withWidth from '@material-ui/core/withWidth/index';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import withStyles from '@material-ui/core/styles/withStyles';
import Typography from '@material-ui/core/Typography';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import CircularProgress from '@material-ui/core/CircularProgress';
import Tooltip from '@material-ui/core/Tooltip';

import RefreshIcon from '@material-ui/icons/Refresh';
import ExitIcon from '@material-ui/icons/ExitToApp';
import PersonIcon from '@material-ui/icons/AccountCircle';
import MenuIcon from '@material-ui/icons/Menu';

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { setOpen } from '../../../reduxActions/LeftDrawerActions';

import { setLastOrderCreateTimestamp } from '../../../reduxActions/WfmPageActions';
import ActiveOfficeSelector from './includes/ActiveOfficeSelector';

const styles = theme => ({
  drawerHeader: {
    background: '#fff',
    borderRight: '1px solid #E0E0E0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    position: 'relative',
    left: -24,
    borderBottom: '1px solid #E0E0E0',
    ...theme.mixins.toolbar,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  drawerHeaderHidden: {
    width: 71,
  },
  drawerHeaderVisible: {
    width: 221,
  },
  header: {
    paddingLeft: 0,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  flex: {
    flex: 1,
  },
  officeSelectorContainer: {
    background: 'white',
    padding: '5px 15px',
    marginRight: 15,
    minHeight: 42,
    minWidth: 100,
  },
  activeOfficeSelectorLoading: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 32,
  },
  menuItem: {
    color: '#FFF',
  },
});

const rawStyle = {
  refreshButton: {
    marginLeft: 12,
    color: '#FFF',
  },
};

const anchorOrigin = {
  vertical: 'top',
  horizontal: 'right',
};

const transformOrigin = {
  vertical: 'top',
  horizontal: 'right',
};

@connect(
  state => ({
    showActiveOfficeSelector: state.wfmPageStore.showActiveOfficeSelector,
    showRefreshButton: state.wfmPageStore.showRefreshButton,
    headerTitle: state.pageHeaderStore.header,
    drawerOpen: state.leftDrawerStore.open,
  }),
  dispatch => ({
    setLastOrderCreateTimestamp: timestamp => dispatch(setLastOrderCreateTimestamp(timestamp)),
    setOpen: open => dispatch(setOpen(open)),
  }),
)
@withStyles(styles)
class Header extends React.Component {
  state = {
    profileMenuOpen: false,
    anchorEl: null,
    refreshInAction: false,
  };

  onRefreshButtonClick = () => {
    this.props.setLastOrderCreateTimestamp(Date.now());
    this.setState({
      refreshInAction: true,
    });

    setTimeout(() => {
      this.setState({
        refreshInAction: false,
      });
    }, 1000);
  };

  handleProfileMenu = (event) => {
    this.setState({
      profileMenuOpen: !this.state.profileMenuOpen,
      anchorEl: event.currentTarget,
    });
  };

  handleProfileClose = () => {
    this.setState({
      profileMenuOpen: false,
      anchorEl: null,
    });
  };

  toggleDrawer = () => {
    const { setOpen, drawerOpen } = this.props;

    setOpen(!drawerOpen);
  };

  render() {
    const {
      classes, showActiveOfficeSelector, showRefreshButton, drawerOpen, headerTitle, width,
    } = this.props;
    const { profileMenuOpen, anchorEl, refreshInAction } = this.state;

    return (
      <AppBar
        position="fixed"
        classes={{
          root: classnames([
            classes.header,
          ]),
        }}
      >
        <Toolbar>
          {width !== 'xs' && (
            <div
              className={classnames(
                classes.drawerHeader,
                !drawerOpen && classes.drawerHeaderHidden,
                drawerOpen && classes.drawerHeaderVisible,
              )}
            >
              {drawerOpen && (
                <IconButton onClick={this.toggleDrawer}>
                  <ChevronLeftIcon color="#FFFFFF" />
                </IconButton>
              )}
              {!drawerOpen && (
                <IconButton onClick={this.toggleDrawer} aria-label="Меню">
                  <MenuIcon />
                </IconButton>
              )}
            </div>
          )}
          {width === 'xs' && (
            <IconButton className={classes.menuButton} onClick={this.toggleDrawer} aria-label="Меню">
              <MenuIcon className={classes.menuItem} />
            </IconButton>
          )}
          <Typography variant="title" color="inherit" className={classes.flex}>
            {headerTitle}
            {showRefreshButton && (
              <Tooltip title="Обновить">
                <IconButton
                  style={rawStyle.refreshButton}
                  onClick={this.onRefreshButtonClick}
                  disabled={refreshInAction}
                >
                  <RefreshIcon />
                </IconButton>
              </Tooltip>
            )}
          </Typography>
          {showActiveOfficeSelector && (
            <div className={classes.officeSelectorContainer}>
              <ActiveOfficeSelector
                renderLoading={() => (
                  <div className={classes.activeOfficeSelectorLoading}>
                    <CircularProgress color="secondary" size={15} />
                  </div>
                )}
              />
            </div>
          )}
          <div>
            <IconButton
              aria-owns={profileMenuOpen ? 'menu-profile' : null}
              aria-haspopup="true"
              onClick={this.handleProfileMenu}
              color="inherit"
            >
              <PersonIcon />
            </IconButton>
            <Menu
              anchorEl={anchorEl}
              id="menu-profile"
              open={profileMenuOpen}
              anchorOrigin={anchorOrigin}
              transformOrigin={transformOrigin}
              onClose={this.handleProfileClose}
            >
              <MenuItem to="/profile" onClick={this.handleClose} component={Link}>
                <ListItemIcon>
                  <PersonIcon />
                </ListItemIcon>
                <ListItemText inset primary="Мой профиль" />
              </MenuItem>
              <MenuItem to="/logout" onClick={this.handleClose} component={Link}>
                <ListItemIcon>
                  <ExitIcon />
                </ListItemIcon>
                <ListItemText inset primary="Выйти" />
              </MenuItem>
            </Menu>
          </div>
        </Toolbar>
      </AppBar>
    );
  }
}

export default withWidth()(Header);
