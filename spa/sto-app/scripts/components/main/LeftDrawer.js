import React from 'react';
import PropTypes from 'prop-types';
import Drawer from '@material-ui/core/Drawer';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import classnames from 'classnames';
import { connect } from 'react-redux';

import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth/index';
import { setOpen } from '../../reduxActions/LeftDrawerActions';

const styles = theme => ({
  desktopPaper: {
    position: 'fixed',
    top: 64,
    height: 'calc(100% - 64px)',
  },
  drawerPaper: {
    width: 221,
    paddingTop: 6,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  drawerPaperPersistentSmall: {
    width: 71,
  },
  menuItem: {
    padding: '12px 24px',
    minHeight: 24,
  },
});

@connect(
  state => ({
    drawerOpen: state.leftDrawerStore.open,
  }),
  dispatch => ({
    setOpen: open => dispatch(setOpen(open)),
  }),
)
@withStyles(styles)
class LeftDrawer extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
    menus: PropTypes.array,
    width: PropTypes.any,
  };

  static defaultProps = {
    menus: [],
    classes: {},
  };

  closeDrawer = () => {
    const { setOpen } = this.props;

    setOpen(false);
  };

  render() {
    const {
      classes, menus, width, drawerOpen,
    } = this.props;

    let variant = 'persistent';
    if (width === 'xs') {
      variant = 'temporary';
    }

    return (
      <Drawer
        variant={variant}
        open={drawerOpen || variant === 'persistent'}
        onClose={this.closeDrawer}
        classes={{
          paper: classnames([
            classes.drawerPaper,
            variant === 'persistent' && classes.desktopPaper,
            (variant === 'persistent' && !drawerOpen) && classes.drawerPaperPersistentSmall,
          ]),
        }}
      >
        {menus.map(menu => (
          <MenuItem
            title={menu.text}
            to={menu.link}
            key={menu.text}
            component={Link}
            className={classes.menuItem}
          >
            <ListItemIcon>
              {menu.icon}
            </ListItemIcon>
            <ListItemText inset primary={menu.text} />
          </MenuItem>
        ))}
      </Drawer>
    );
  }
}

export default withWidth()(LeftDrawer);
