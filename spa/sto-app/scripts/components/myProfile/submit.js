import cloneDeep from 'lodash/cloneDeep';
import isEmpty from 'lodash/isEmpty';
import { SubmissionError } from 'redux-form';
import EditMyProfileMutation from '../../relay/mutation/profile/EditMyProfileMutation';
import parseMutationInputValidationErrorMessageForForm
  from '../../../../common-code/utils/parseMutationInputValidationErrorMessageForForm';
import commitMutationPromise from '../../../../common-code/relay/commitMutationPromise';

function submit(viewer, values) {
  const onFailure = (errors) => {
    console.log('onFailure errors', errors);
    if (errors && errors instanceof SubmissionError) {
      throw errors;
    }

    alert('При редактировании профиля произошла ошибка. Обновите страницу и/или повторите попытку.');
  };

  const onSuccess = (response) => {
    if (response.editMyProfileMutation.errors.messages.length > 0) {
      const errors = response.editMyProfileMutation.errors.messages;
      const parsedErrors = parseMutationInputValidationErrorMessageForForm(errors);

      if (!isEmpty(parsedErrors.mobilePhone)) {
        parsedErrors.phone = cloneDeep(parsedErrors.mobilePhone);
        delete (parsedErrors.mobilePhone);
      }

      throw new SubmissionError(parsedErrors);
    }
  };

  return commitMutationPromise(EditMyProfileMutation, { viewer, input: values })
    .then(onSuccess)
    .catch(onFailure);
}

export default submit;
