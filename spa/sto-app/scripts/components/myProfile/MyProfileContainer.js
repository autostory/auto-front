import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Relay from 'react-relay/classic';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';

import ConfiguredRadium from './../../radium/ConfiguredRadium';
import relayContainer from '../../../../common-code/decorators/relayContainer';
import ChangeMyPasswordMutation from '../../relay/mutation/profile/ChangeMyPasswordMutation';
import EditMyProfileMutation from '../../relay/mutation/profile/EditMyProfileMutation';
import MyProfileForm from './MyProfileForm';
import submit from './submit';

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id
          firstName
          surname
          patronymic
          email
          phone
          ${ChangeMyPasswordMutation.getFragment('viewer')},
          ${EditMyProfileMutation.getFragment('viewer')},
      }
  `,
})
@ConfiguredRadium
class MyProfileContainer extends Component {
  state = {
    oldPassword: '',
    newPassword: '',
    newPasswordConfirm: '',
    passwordChangeMode: false,
    passwordChangeErrorText: '',
    pendingPasswordSave: false,
  };

  onProfileFormSubmit = (values) => {
    const { viewer } = this.props;

    return submit(viewer, values);
  };

  getForm = () => {
    const { viewer } = this.props;

    const initialValues = {
      firstName: viewer.firstName,
      surname: viewer.surname,
      patronymic: viewer.patronymic,
      email: viewer.email,
      phone: viewer.phone,
    };

    return (
      <MyProfileForm
        onSubmit={this.onProfileFormSubmit}
        initialValues={initialValues}
      />
    );
  };

  changePassword = () => {
    if (this.validateNewPassword()) {
      if (this.pendingPasswordSave) {
        return;
      }

      const onSuccess = (data) => {
        this.setState({
          pendingPasswordSave: false,
        });

        if (data.changeMyPassword.passwordChanged) {
          this.setState({
            passwordChangeMode: false,
            pendingPasswordSave: false,
          });

          alert('Новый пароль установлен.');

          return;
        }

        let errorText = '';
        if (data.changeMyPassword.errors.messages.length > 0) {
          const { messages } = data.changeMyPassword.errors;
          for (let j = 0; j < messages.length; j += 1) {
            errorText = `${messages[j].text}\n`;
          }

          alert(errorText);
        }
      };

      const onFailure = (e) => {
        this.setState({
          pendingPasswordSave: false,
        });

        console.log('onFailure e =', e);
        alert('Что-то пошло не так при смене пароля.');
      };

      const { relay, viewer } = this.props;
      const input = {
        newPassword: this.state.newPassword,
        currentPassword: this.state.oldPassword,
      };

      const changeMyPasswordMutation = new ChangeMyPasswordMutation({ input, viewer });
      this.setState({ pendingPasswordSave: true }, () => {
        relay.commitUpdate(changeMyPasswordMutation, { onFailure, onSuccess });
      });
    }
  };

  validateNewPassword = () => {
    const p1 = this.state.newPassword;
    const p2 = this.state.newPasswordConfirm;

    if (p1 !== p2) {
      this.setState({
        passwordChangeErrorText: 'Пароли не совпадают.',
      });

      return false;
    }

    if (p1.length < 8) {
      this.setState({
        passwordChangeErrorText: 'Пароли должен быть длинной минимум 8 символов.',
      });

      return false;
    }

    if (p1.toLowerCase() === p1 || p1.toUpperCase() === p1) {
      this.setState({
        passwordChangeErrorText: 'Пароль должен содержать как минимум одну заглавную и одну строчную букву.',
      });

      return false;
    }

    if (p1.match(/\d+/g) === null) {
      this.setState({
        passwordChangeErrorText: 'Пароль должен содержать как минимум одну цифру.',
      });

      return false;
    }

    this.setState({
      passwordChangeErrorText: '',
    });

    return true;
  };

  handleFieldChange = (event) => {
    const key = event.target.name;
    const { value } = event.target;

    if ((key === 'newPassword' && this.state.newPasswordConfirm !== '') || key === 'newPasswordConfirm') {
      this.setState({
        [key]: value,
      }, this.validateNewPassword);
    } else {
      this.setState({
        [key]: value,
      });
    }
  };

  togglePasswordChangeMode = () => {
    this.setState({ passwordChangeMode: !this.state.passwordChangeMode });
  };

  render() {
    return (
      <div>
        <form autoComplete="false">
          <Paper style={{ width: 500, padding: 24 }}>
            {this.getForm()}
          </Paper>
          <br />
          <br />
          <Paper style={{ width: 500, padding: 24 }}>
            <h2>Пароль</h2>
            {this.state.passwordChangeMode &&
            <div style={{ width: 400 }}>
              <TextField
                inputProps={{
                  maxLength: 30,
                }}
                fullWidth
                name="oldPassword"
                disabled={this.state.pendingPasswordSave}
                type="password"
                label="Ваш текущий пароль"
                onChange={this.handleFieldChange}
              />
              <br />
              <TextField
                fullWidth
                inputProps={{
                  maxLength: 30,
                }}
                name="newPassword"
                disabled={this.state.pendingPasswordSave}
                type="password"
                label="Новый пароль"
                onChange={this.handleFieldChange}
                error={this.state.passwordChangeErrorText}
                helperText={this.state.passwordChangeErrorText}
              />
              <br />
              <TextField
                fullWidth
                inputProps={{
                  maxLength: 30,
                }}
                name="newPasswordConfirm"
                disabled={this.state.pendingPasswordSave}
                type="password"
                label="Подтверждение нового пароля"
                error={this.state.passwordChangeErrorText}
                helperText={this.state.passwordChangeErrorText}
                onChange={this.handleFieldChange}
              />
            </div>
            }
            {this.state.passwordChangeMode && (
              <Button
                disabled={this.state.passwordChangeErrorText !== '' || this.state.pendingPasswordSave}
                color="primary"
                onClick={this.changePassword}
              >
                Сохранить
              </Button>
            )}
            {this.state.pendingPasswordSave && <CircularProgress />}
            <Button
              disabled={this.state.pendingPasswordSave}
              color="secondary"
              onClick={this.togglePasswordChangeMode}
            >
              {this.state.passwordChangeMode ? 'Отменить' : 'Сменить пароль'}
            </Button>
          </Paper>
        </form>
      </div>
    );
  }
}

export default MyProfileContainer;
