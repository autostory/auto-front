import React, { Component } from 'react';
import Relay from 'react-relay/classic';

import MyProfileRoute from './MyProfileRoute';
import MyProfileContainer from './MyProfileContainer';
import ConfiguredRadium from '../../radium/ConfiguredRadium';

@ConfiguredRadium
class MyProfile extends Component {
  getResult = ComponentWithRelay => (
    // TODO: Этот подход можно унифицировать
    <Relay.RootContainer
      Component={ComponentWithRelay}
      route={new MyProfileRoute()}
      forceFetch
      renderLoading={() => <div>Загрузка профиля...</div>}
      renderFetched={data => <ComponentWithRelay {...data} />}
      renderFailure={(error, retry) => (
        <div>
          <p>Произошла ошибка при загрузке данных.</p>
          <p>
            <button onClick={retry} >Повторить попытку.</button>
          </p>
        </div>
      )}
    />
  );

  render() {
    const container = this.getResult(MyProfileContainer);

    return (
      <div>
        {container}
      </div>
    );
  }
}

export default MyProfile;
