import Relay from 'react-relay/classic';

class MyProfileRoute extends Relay.Route {
  static queries = {
    viewer: () => Relay.QL`query { viewer }`,
  };

  static routeName = 'MyProfileRoute';
}

export default MyProfileRoute;
