import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import { Link } from 'react-router-dom';
import { reduxForm } from 'redux-form';
import ManagerForm from '../Form/Manager/ManagerForm';
import validate from './validate';

export const FORM_NAME = 'MyProfileForm';

class MyProfileForm extends React.PureComponent {
  onSave = () => {
    const { handleSubmit } = this.props;

    handleSubmit();
  };

  render() {
    const { dirty, invalid, submitting } = this.props;

    return (
      <FormGroup row={false} style={{ width: 400 }}>
        <ManagerForm
          disabled={submitting}
        />
        <div>
          <Button
            variant="contained"
            color="primary"
            disabled={!dirty || submitting || invalid}
            onClick={this.onSave}
            style={{
              marginRight: 15,
              marginTop: 15,
            }}
          >
            Сохранить
          </Button>
        </div>
        {dirty && invalid && (
          <Typography color="error">
            В форме есть ошибки. Исправьте их и изменения можно будет сохранить.
          </Typography>
        )}
        {submitting && <CircularProgress />}
      </FormGroup>
    );
  }
}

export default reduxForm({
  form: FORM_NAME,
  validate,
})(MyProfileForm);
