import React from 'react';
import compact from 'lodash/compact';
import map from 'lodash/map';
import get from 'lodash/get';
import { connect } from 'react-redux';
import { Table } from '@devexpress/dx-react-grid-material-ui';
import { Link } from 'react-router-dom';
import TableCell from '@material-ui/core/TableCell';
import InfoIcon from '@material-ui/icons/Info';
import Button from '@material-ui/core/Button';
import Highlighter from 'react-highlight-words';

import CarInfoWithPopover from '../../Car/CarInfoWithPopover';

const rawStyle = {
  highlighter: {
    background: '#FAD65E',
    padding: '4px 0',
  },
  noCar: {
    fontSize: 'smaller',
  },
};

const Match = ({ keywordArray, text, ...rest }) => (
  <Highlighter
    highlightStyle={rawStyle.highlighter}
    searchWords={keywordArray}
    autoEscape
    textToHighlight={text || ''}
    {...rest}
  />
);

const ClientCarsCell = ({ value, match }) => {
  const cars = map(get(value, 'carList.edges'), 'node');

  if (cars.length === 0) {
    return (
      <Table.Cell><i style={rawStyle.noCar}>нет автомобилей</i></Table.Cell>
    );
  }

  const carItems = map(cars, item => (
    <CarInfoWithPopover key={item.id} car={item} match={match} />
  ));

  return (
    <Table.Cell>{carItems}</Table.Cell>
  );
};

@connect(state => ({
  email: state.firmClientsFilterStore.email,
  phone: state.firmClientsFilterStore.phone,
  fio: state.firmClientsFilterStore.fio,
  registrationSignNumber: state.firmClientsFilterStore.registrationSignNumber,
}))
class Cell extends React.Component {
  render() {
    const { email, phone, fio, registrationSignNumber, dispatch, ...rest } = this.props;

    if (rest.column.name === 'cars') {
      return <ClientCarsCell {...rest} match={registrationSignNumber} />;
    }

    // if (rest.column.name === 'id') {
    //   const { value, ...rest2 } = rest;
    //   const match = <Match text={value} keywordArray={[id]} />;
    //
    //   return <Table.Cell value={match} {...rest2} />;
    // }

    if (rest.column.name === 'view') {
      const { row } = rest;
      return (
        <TableCell style={{ padding: '0 5px' }}>
          <Button
            component={Link}
            title="Карточка клиента"
            to={`/client/${row.id}`}
            target="_blank"
          >
            <InfoIcon />
          </Button>
        </TableCell>
      );
    }

    if (rest.column.name === 'fullName') {
      const { value, ...rest2 } = rest;
      const match = <Match text={value} keywordArray={compact(fio.split(' '))} />;

      return <Table.Cell value={match} {...rest2} />;
    }

    if (rest.column.name === 'phone') {
      const { value, ...rest2 } = rest;
      const match = <Match text={value} keywordArray={[phone]} />;

      return <Table.Cell value={match} {...rest2} />;
    }

    if (rest.column.name === 'email') {
      const { value, ...rest2 } = rest;
      const match = <Match text={value} keywordArray={[email]} />;

      return <Table.Cell value={match} {...rest2} />;
    }

    return <Table.Cell {...rest} />;
  }
}

export default Cell;