import React from 'react';
import get from 'lodash/get';
import map from 'lodash/map';
import debounce from 'lodash/debounce';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import {
  TableColumnVisibility,
  FilteringState,
  IntegratedFiltering,
} from '@devexpress/dx-react-grid';

import {
  Grid,
  Table,
  TableHeaderRow,
  Toolbar,
  ColumnChooser,
  TableFilterRow,
} from '@devexpress/dx-react-grid-material-ui';

import CircularProgress from '@material-ui/core/CircularProgress';
import TableCell from '@material-ui/core/TableCell';
import Input from '@material-ui/core/Input';
import { withStyles } from '@material-ui/core/styles/index';

import { connect } from 'react-redux';
import { fromGlobalId } from '../../../../../common-code/relay/globalIdUtils';

import {
  setEmailFilter, setFioFilter, setPhoneFilter, setRegistrationSignNumberFilter,
} from '../../../reduxActions/FirmClientsFilterActions';

import Cell from './Cell';

const phoneFilterStyles = theme => ({
  cell: {
    width: '100%',
    paddingLeft: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
  },
  input: {
    width: '100%',
  },
});

const PhoneFilterCellBase = ({ filter, onFilter, classes }) => (
  <TableCell className={classes.cell}>
    <Input
      className={classes.input}
      type="number"
      value={filter ? filter.value : ''}
      onChange={e => onFilter(e.target.value ? { value: e.target.value } : null)}
      placeholder="Фильтр..."
    />
  </TableCell>
);

const PhoneFilterCell = withStyles(phoneFilterStyles, { name: 'SexFilterCell' })(PhoneFilterCellBase);

const rawStyle = {
  paper: {
    position: 'relative',
  },
  noSelectedRows: {
    fontSize: 'small',
  },
};

const getRowId = row => row.id;

const NoDataCell = () => (
  <Table.Cell colSpan={5}>
    <Typography variant="subheading">
      Ничего не найдено
    </Typography>
  </Table.Cell>
);

const NoSelectedRows = () => (
  <i style={rawStyle.noSelectedRows}>не выбрано ни одной колонки для отображения</i>
);

const CustomFilterCell = ({ filteringEnabled, ...props }) => {
  if (!filteringEnabled) {
    return <th />;
  }

  const { column } = props;
  if (column.name === 'phone') {
    return <PhoneFilterCell {...props} />;
  }

  return <TableFilterRow.Cell {...props} />;
};

const idPredicate = (value, filter) => {
  const { id } = fromGlobalId(value);

  return id.indexOf(filter.value) !== -1;
};

const carPredicate = (value, filter) => {
  const cars = map(get(value, 'carList.edges'), 'node');

  if (!cars.length) {
    return false;
  }

  return cars.filter(car =>
    car.registrationSignNumber && car.registrationSignNumber.indexOf(filter.value) !== -1).length > 0;
};

@connect(
  state => ({}),
  dispatch => ({
    setEmailFilter: ({ value }) => {
      dispatch(setEmailFilter(value));
    },
    setFioFilter: ({ value }) => {
      dispatch(setFioFilter(value));
    },
    setPhoneFilter: ({ value }) => {
      dispatch(setPhoneFilter(value));
    },
    setRegistrationSignNumberFilter: ({ value }) => {
      dispatch(setRegistrationSignNumberFilter(value));
    },
    // setIdFilter: ({ value }) => {
    //   dispatch(setIdFilter(value));
    // },
    // setOrberByFilter: ({ value }) => {
    //   dispatch(setOrberByFilter(value));
    // },
    // setOrderTypeFilter: ({ value }) => {
    //   dispatch(setOrderTypeFilter(value));
    // },
  }),
)
export default class MyClientsTableNew extends React.PureComponent {
  state = {
    columns: [
      { name: 'view', title: ' ' },
      { name: 'category', title: 'Категория' },
      { name: 'fullName', title: 'ФИО' },
      { name: 'phone', title: 'Сотовый' },
      { name: 'email', title: 'E-mail' },
      { name: 'cars', title: 'Автомобили' },
      { name: 'points', title: 'Баллы' },
      { name: 'avgBill', title: 'Средний чек' },
      { name: 'sumBill', title: 'Суммарный чек' },
      { name: 'ordersCount', title: 'Количество заказов' },
    ],
    rows: [],
    // sorting: [{ columnName: 'id', direction: 'desc' }],
    integratedFilteringColumnExtensions: [
      { columnName: 'id', predicate: idPredicate },
      { columnName: 'cars', predicate: carPredicate },
    ],
    // sortingColumnExtensions: [
    //   { columnName: 'cars', sortingEnabled: false },
    //   { columnName: 'avgBill', sortingEnabled: false },
    //   { columnName: 'sumBill', sortingEnabled: false },
    //   { columnName: 'ordersCount', sortingEnabled: false },
    // ],
    filteringStateColumnExtensions: [
      // { columnName: 'id', filteringEnabled: true },
      { columnName: 'view', filteringEnabled: false },
      { columnName: 'category', filteringEnabled: false },
      { columnName: 'fullName', filteringEnabled: true },
      { columnName: 'phone', filteringEnabled: true },
      { columnName: 'email', filteringEnabled: true },
      { columnName: 'cars', filteringEnabled: true },
      { columnName: 'points', filteringEnabled: false },
      { columnName: 'avgBill', filteringEnabled: false },
      { columnName: 'sumBill', filteringEnabled: false },
      { columnName: 'ordersCount', filteringEnabled: false },
    ],
    defaultHiddenColumnNames: ['email', 'sumBill', 'ordersCount'],
    tableColumnExtensions: [
      { columnName: 'view', width: 88 },
      { columnName: 'category', width: 100 },
      { columnName: 'fullName', wordWrapEnabled: true },
      { columnName: 'phone', wordWrapEnabled: true },
      { columnName: 'email', wordWrapEnabled: true },
    ],
    loading: true,
  };

  componentDidMount() {
    this.renewData();
  }

  componentDidUpdate() {
    this.renewData();
  }

  // changeSorting = (sorting) => {
  //   this.setState({
  //     loading: true,
  //     sorting,
  //   });
  //
  //   const { setOrberByFilter, setOrderTypeFilter } = this.props;
  //
  //   setOrberByFilter({ value: sorting[0].columnName });
  //   setOrderTypeFilter({ value: sorting[0].direction });
  // };

  renewData = () => {
    const { data } = this.props;

    this.setState({
      rows: data,
      loading: false,
    });
  };

  normalizeFilters = (filters) => {
    const normFilters = {
      email: '',
      phone: '',
      fullName: '',
      cars: '',
    };

    if (!filters.length) {
      return normFilters;
    }

    filters.map((item) => {
      normFilters[item.columnName] = item.value;
    });

    return normFilters;
  };

  handleFilterChangeDebounced = (value, callback) => debounce(() => {
    callback({ value });
  }, 300, { maxWait: 1000 })();

  getCallbackByKey = (key) => {
    const {
      // setIdFilter,
      setEmailFilter, setFioFilter, setPhoneFilter, setRegistrationSignNumberFilter,
    } = this.props;

    switch (key) {
      // case 'id':
      //   return setIdFilter;
      case 'email':
        return setEmailFilter;
      case 'fullName':
        return setFioFilter;
      case 'phone':
        return setPhoneFilter;
      case 'cars':
        return setRegistrationSignNumberFilter;
      default:
        return () => {};
    }
  };

  onFiltersChange = (filters) => {
    const normFilters = this.normalizeFilters(filters);

    map(normFilters, (value, key) => {
      this.handleFilterChangeDebounced(value, this.getCallbackByKey(key));
    });
  };

  render() {
    const {
      rows,
      columns,
      // sorting,
      loading,
      integratedFilteringColumnExtensions,
      filteringStateColumnExtensions,
      defaultHiddenColumnNames,
      tableColumnExtensions,
      // sortingColumnExtensions,
    } = this.state;

    return (
      <Paper>
        <Grid
          rows={rows}
          columns={columns}
          getRowId={getRowId}
        >
          {/* <RelayIdTypeProvider for={['id']} /> */}

          <FilteringState
            onFiltersChange={this.onFiltersChange}
            columnExtensions={filteringStateColumnExtensions}
          />

          <IntegratedFiltering
            columnExtensions={integratedFilteringColumnExtensions}
          />

          {/* <SortingState */}
          {/* sorting={sorting} */}
          {/* onSortingChange={this.changeSorting} */}
          {/* columnExtensions={sortingColumnExtensions} */}
          {/* /> */}

          <Table
            columnExtensions={tableColumnExtensions}
            cellComponent={Cell}
            noDataCellComponent={NoDataCell}
            messages={{
              noData: 'нет данных',
            }}
          />

          <TableColumnVisibility
            defaultHiddenColumnNames={defaultHiddenColumnNames}
            emptyMessageComponent={NoSelectedRows}
          />

          <TableFilterRow
            cellComponent={CustomFilterCell}
            messages={{
              filterPlaceholder: 'Фильтр...',
            }}
          />

          <TableHeaderRow />

          {/* <TableHeaderRow */}
          {/* showSortingControls */}
          {/* /> */}

          <Toolbar />
          <ColumnChooser />
        </Grid>
        {loading && <CircularProgress />}
      </Paper>
    );
  }
}
