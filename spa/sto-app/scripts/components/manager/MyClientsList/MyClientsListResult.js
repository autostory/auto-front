/* eslint-disable react/prop-types */
import React, { PureComponent } from 'react';
import Relay from 'react-relay/classic';
import withProps from 'recompose/withProps';

import { connect } from 'react-redux';
import cloneDeep from 'lodash/cloneDeep';
import get from 'lodash/get';
import map from 'lodash/map';
import CircularProgress from '@material-ui/core/CircularProgress';
import { getClientCategoryTypeName } from '../../../../../common-code/constants/clientCategoryTypes';

import relaySuperContainer from '../../../../../common-code/decorators/relaySuperContainer';
import MyClientsTableNew from './MyClientsTableNew';
import CarInfoWithPopover from './../../Car/CarInfoWithPopover';

const PER_PAGE = 20;

const rawStyle = {
  textCentered: {
    textAlign: 'center',
  },
};

@withProps({
  renderLoading: () => <div>Загрузка списка клиентов <CircularProgress /></div>,
})
@relaySuperContainer(
  {
    viewer: () => Relay.QL`
        fragment on Viewer {
            myFirm{
                id
                clients(
                    email: $email,
                    phone: $phone,
                    fio: $fio,
                    registrationSignNumber: $registrationSignNumber,
                    first: $first,
                    after: $after
                    orderBy: $orderBy,
                    orderType: $orderType,
                ) {
                    id
                    clientList {
                        pageInfo {
                            startCursor
                            endCursor
                            hasNextPage
                            hasPreviousPage
                        }
                        edges {
                            node {
                                ordersCount
                                category
                                points
                                paymentStat {
                                    sumBill
                                    averageBill
                                }
                                id
                                email
                                phone
                                fullName
                                cars {
                                    carList {
                                        edges {
                                            node {
                                                ${CarInfoWithPopover.getFragment('car')}
                                                id
                                                vin
                                                registrationSignNumber
                                                catalogCarType {
                                                    id
                                                    name
                                                }
                                                catalogCarMark {
                                                    id
                                                    name
                                                }
                                                catalogCarModel {
                                                    id
                                                    name
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    `,
  },
  {
    email: '',
    phone: '',
    fio: '',
    orderBy: 'id',
    orderType: 'DESC',
    registrationSignNumber: '',
    first: PER_PAGE,
    after: null,
  },
)
@connect(state => ({
  orderBy: state.firmClientsFilterStore.orderBy,
  orderType: state.firmClientsFilterStore.orderType,
  email: state.firmClientsFilterStore.email,
  phone: state.firmClientsFilterStore.phone,
  fio: state.firmClientsFilterStore.fio,
  registrationSignNumber: state.firmClientsFilterStore.registrationSignNumber,
}))
class MyClientsListResult extends PureComponent {
  state = {
    loading: false,
  };

  componentDidMount = () => {
    setTimeout(() => this.loadNextItemsIfNeeded(this.scrollContainer), 500);

    window.addEventListener('scroll', this.onScroll);
    window.addEventListener('resize', this.onScroll);
  };

  componentDidUpdate = () => {
    const { relay } = this.props;
    const {
      email, phone, fio, registrationSignNumber, id, orderBy, orderType,
    } = this.props;

    relay.setVariables({
      email, phone, fio, registrationSignNumber, id, orderBy, orderType,
    });
  };

  componentWillUnmount = () => {
    window.removeEventListener('scroll', this.onScroll);
    window.removeEventListener('resize', this.onScroll);
  };

  onScroll = () => {
    if (!this.state.loading) {
      this.loadNextItemsIfNeeded();
    }
  };

  loadNextItemsIfNeeded = () => {
    const elem = this.scrollContainer;
    if (elem) {
      const contentHeight = elem.offsetHeight;
      const y = window.pageYOffset + window.innerHeight;

      if (y >= contentHeight) {
        this.loadNextItems();
      }
    }
  };

  loadNextItems = () => {
    this.setState({ loading: true }, () => {
      const hasNextPage = get(this.props, 'viewer.myFirm.clients.clientList.pageInfo.hasNextPage', false);

      if (hasNextPage) {
        this.props.relay.setVariables({
          first: this.props.relay.variables.first + PER_PAGE,
        }, (readyState) => { // this gets called twice https://goo.gl/ZsQ3Dy
          if (readyState.done || readyState.aborted) {
            this.setState({ loading: false }, () => {
              this.loadNextItemsIfNeeded();
            });
          }
        });
      }
    });
  };

  scrollContainer = null;

  normalizeData = (rawData) => {
    const normData = [];
    if (rawData.length === 0) {
      return normData;
    }

    return map(rawData, (item) => {
      const newItem = cloneDeep(item);
      newItem.category = getClientCategoryTypeName(newItem.category);
      newItem.avgBill = newItem.paymentStat.averageBill;
      newItem.sumBill = newItem.paymentStat.sumBill;

      return newItem;
    });
  };

  render() {
    const clients = map(get(this.props, 'viewer.myFirm.clients.clientList.edges'), 'node');
    const hasNextPage = get(this.props, 'viewer.myFirm.clients.clientList.pageInfo.hasNextPage', false);

    return (
      <div>
        <div
          onScroll={this.onScroll}
          ref={(c) => {
            this.scrollContainer = c;
          }}
        >
          <MyClientsTableNew data={this.normalizeData(clients)} />
          <br /><br />
          {
            hasNextPage && (
              <div style={rawStyle.textCentered}>
                <CircularProgress thickness={2} size={20} />
              </div>
            )
          }
        </div>
      </div>
    );
  }
}

export default MyClientsListResult;
