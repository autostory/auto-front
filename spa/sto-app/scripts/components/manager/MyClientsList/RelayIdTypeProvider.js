import * as React from 'react';
import { DataTypeProvider } from '@devexpress/dx-react-grid';
import { fromGlobalId } from '../../../../../common-code/relay/globalIdUtils';

const Formatter = ({ value }) => {
  const parts = fromGlobalId(value);
  const { id } = parts;

  return id;
};

const RelayIdTypeProvider = props => (
  <DataTypeProvider
    formatterComponent={Formatter}
    {...props}
  />
);

export default RelayIdTypeProvider;
