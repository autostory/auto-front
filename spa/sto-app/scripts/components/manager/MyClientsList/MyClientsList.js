import React, { PureComponent } from 'react';
import { withRouter } from 'react-router';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

import SingleFAB from '../../../pages/wfm/includes/SingleFAB';
import MyClientsListResult from './MyClientsListResult';

@withRouter
class MyClientsList extends PureComponent {
  onClick = () => {
    const { history } = this.props;

    history.push('/client/add');
  };

  render() {
    return (
      <div>
        <MyClientsListResult />
        <SingleFAB>
          <Tooltip title="Добавить клиента">
            <Button
              variant="fab"
              aria-label="Добавить клиента"
              color="primary"
              onClick={this.onClick}
            >
              <PersonAddIcon />
            </Button>
          </Tooltip>
        </SingleFAB>
      </div>
    );
  }
}

export default MyClientsList;
