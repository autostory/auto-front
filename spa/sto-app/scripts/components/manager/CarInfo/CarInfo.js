import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ConfiguredRadium from '../../../radium/ConfiguredRadium';

import CarInfoContainer from './CarInfoContainer';

@ConfiguredRadium
class CarInfo extends Component {
  static propTypes = {
    carId: PropTypes.string.isRequired,
  };

  render() {
    const { carId } = this.props;

    return <CarInfoContainer initialVariables={{ carId }} />;
  }
}

export default CarInfo;
