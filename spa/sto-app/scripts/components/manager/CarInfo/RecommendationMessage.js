import React from 'react';
import Paper from '@material-ui/core/Paper';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';


const style = {
  container: {
    minWidth: 80,
    maxWidth: 350,
    padding: 15,
    marginRight: 15,
    marginBottom: 15,
    wordBreak: 'break-word',
  },
};

class RecommendationMessage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      date: '07.07.2018',
      author: 'СТО - "Новэкс"',
      text: 'многобуквмногобуквмногобуквмногобуквмногобуквмногобуквмногобуквмногобуквмногобукв',
    };
  }

  render() {
    return (
      <ExpansionPanel style={style.container}>
        <ExpansionPanelSummary>
          <Typography>%Рекоммедация от <b>{this.state.author} <br />{this.state.date} %</b> </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Typography>
            %{this.state.text}%
          </Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

export default RecommendationMessage;