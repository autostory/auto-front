import get from 'lodash/get';
import React from 'react';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import DirectionsCar from '@material-ui/icons/DirectionsCar';

import EditCarDialog from '../../../pages/wfm/Dialogs/EditCarDialog';
import CarInfoMainData from './includes/CarInfoMainData';
import relaySuperContainer from '../../../../../common-code/decorators/relaySuperContainer';
import CarOwnersList from './includes/CarOwnersList';
import OrdersTable from './OrdersTable';

const style = {
  icon: {
    minWidth: 80,
    maxWidth: 40,
    height: 'auto',
    marginRight: 15,
  },
  header: {
    fontWeight: 'bold',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    height: 60,
  },
  recommendationDetails: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  editButton: {
    height: 40,
  },
  tab: {
    padding: 8 * 3,
  },
};

function TabContainer(props) {
  return (
    <Typography component="div" style={style.tab}>
      {props.children}
    </Typography>
  );
}

@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          car (carId: $carId) {
              id
              ${CarInfoMainData.getFragment('car')},
              ${OrdersTable.getFragment('car')},
              ${CarOwnersList.getFragment('car')},
              owners {
                  itemsCount
                  ownersList {
                      edges {
                          node {
                              id
                              firstName
                              surname
                              patronymic
                              email
                              phone
                          }
                      }
                  }
              }
              registrationSignNumber
              displayName
          }
      }
  `,
})
@withRouter
class CarInfoContainer extends React.PureComponent {
  static propTypes = {
    // eslint-disable-next-line react/no-unused-prop-types
    initialVariables: PropTypes.shape({
      carId: PropTypes.string.isRequired,
    }),
  };

  state = {
    activeTab: 0,
    showCarEditDialog: false,
  };

  handleTabChange = (event, value) => {
    this.setState({ activeTab: value });
  };

  showCarEditDialog = () => {
    this.setState({
      showCarEditDialog: true,
    });
  };

  hideCarEditDialog = () => {
    this.setState({
      showCarEditDialog: false,
    });
  };

  render() {
    const { car } = this.props.viewer;
    const { activeTab, showCarEditDialog } = this.state;

    return (
      <div>
        <EditCarDialog
          carId={car.id}
          open={showCarEditDialog}
          onRequestClose={this.hideCarEditDialog}
        />
        <div style={style.header}>
          <DirectionsCar style={style.icon} />
          <div>
            {car.displayName} <br />
            Регистрационный номер:{' '}
            {get(car, 'registrationSignNumber')}
            {!get(car, 'registrationSignNumber') && <i>не указан</i>}
          </div>
          <Button style={style.editButton} color="primary" variant="contained" onClick={this.showCarEditDialog}>
            Редактировать
          </Button>
        </div>
        <AppBar position="static">
          <Tabs
            centered
            value={activeTab}
            onChange={this.handleTabChange}
            fullWidth
          >
            <Tab label="Справка по автомобилю">
              <Paper />
            </Tab>
            <Tab label="История работ">
              <Paper />
            </Tab>
            <Tab label="Владельцы">
              <Paper />
            </Tab>
          </Tabs>
        </AppBar>
        {activeTab === 0 && (
          <TabContainer>
            <ExpansionPanel expanded>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography>Технические характеристики</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <CarInfoMainData car={car} />
              </ExpansionPanelDetails>
            </ExpansionPanel>
            {/*<ExpansionPanel>*/}
              {/*<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>*/}
                {/*<Typography>Рекомендации</Typography>*/}
              {/*</ExpansionPanelSummary>*/}
              {/*<ExpansionPanelDetails>*/}
                {/*<Typography style={style.recommendationDetails} component="div">*/}
                  {/*<RecommendationMessage />*/}
                  {/*<RecommendationMessage />*/}
                {/*</Typography>*/}
              {/*</ExpansionPanelDetails>*/}
            {/*</ExpansionPanel>*/}
          </TabContainer>
        )}
        {activeTab === 1 && (
          <TabContainer>
            <OrdersTable car={this.props.viewer.car} />
          </TabContainer>
        )}
        {activeTab === 2 && (
          <TabContainer>
            <CarOwnersList car={this.props.viewer.car} />
          </TabContainer>
        )}
      </div>
    );
  }
}

export default CarInfoContainer;
