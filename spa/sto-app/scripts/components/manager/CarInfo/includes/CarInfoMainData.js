import React, { Component } from 'react';
import Relay from 'react-relay/classic';
import get from 'lodash/get';

import ConfiguredRadium from '../../../../radium/ConfiguredRadium';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';


@relayContainer({
  car: () => Relay.QL`
      fragment on Car {
          id
          vin
          registrationSignNumber
          displayName
          color
          bodyType
          engineCapacity
          enginePower
          engineModel
          catalogCarType {
              id
              name
          }
          catalogCarGeneration {
              name
              yearBegin
              yearEnd
          }
          catalogCarEquipment {
              name
          }
          lastMileage {
              value
              measuredAt {
                  formattedDate
              }
          }
      }
  `,
})
@ConfiguredRadium
class CarInfoMainData extends Component {
  render() {
    const { car } = this.props;
    return (
      <div>
        <b>Пробег: </b>
        {get(car, 'lastMileage.value', 'не указан')}
        <b> км.</b> <br />
        <b>Дата последнего указания пробега: </b>
        {get(car, 'lastMileage.measuredAt.formattedDate', 'не указана')} <br />
        <b>Поколение: </b>
        {get(car, 'catalogCarGeneration.name', 'Неизвестно')}
        <b> Выпускалось c </b>
        {get(car, 'catalogCarGeneration.yearBegin', 'Неизвестно')}
        <b> по </b>
        {get(car, 'catalogCarGeneration.yearEnd', 'Неизвестно')}<br />
        <b>Комплектация: </b>
        {get(car, 'catalogCarEquipment.name', 'Неизвестна')}<br />
        <b>VIN: </b>
        {get(car, 'vin', 'Не указан')}<br /><br />
        <b>Технические характеристики:</b><br /><br />
        <b>Тип транспорта: </b>
        {get(car, 'catalogCarType.name', 'Не указан')}<br />
        <b>Модель двигателя: </b>
        {get(car, 'engineModel', 'Не указана')}<br />
        <b>Мощность двигателя: </b>
        {get(car, 'enginePower', 'Не указана')}<br />
        <b>Объем двигателя: </b>
        {get(car, 'engineCapacity', 'Не указан')}<br />
        <b>Кузов: </b>
        {get(car, 'bodyType', 'Не указан')}<br />
        <b>Цвет: </b>
        {get(car, 'color', 'Не указан')}<br />
      </div>
    );
  }
}

export default CarInfoMainData;
