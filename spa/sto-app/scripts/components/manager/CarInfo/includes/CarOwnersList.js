import React, { Component } from 'react';

import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import get from 'lodash/get';
import map from 'lodash/map';
import Relay from 'react-relay/classic';

import relayContainer from '../../../../../../common-code/decorators/relayContainer';


const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
    padding: 15,
  },
  table: {
    minWidth: 700,
  },
});

@relayContainer({
  car: () => Relay.QL`
      fragment on Car {
          owners{
              id
              ownersList {
                  edges{
                      node {
                          id
                          fullName
                          email
                          phone
                      }
                  }
              }
          }
      }
  `,
})
@withStyles(styles)
class CarOwnersList extends Component {
  render() {
    const { classes } = this.props;
    const ownersList = map(get(this.props.car, 'owners.ownersList.edges'), 'node');
    return (
      <div>
        <span>Всего владельцевв: {ownersList.length}</span>
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>ФИО</TableCell>
                <TableCell>e-mail</TableCell>
                <TableCell>телефон</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {ownersList.length > 0 && ownersList.map(client =>
                <TableRow key={client.id}>
                  <TableCell>{client.fullName}</TableCell>
                  <TableCell>{client.email}</TableCell>
                  <TableCell>{client.phone}</TableCell>
                </TableRow>,
              )}
            </TableBody>
          </Table>
        </Paper>
      </div>
    );
  }
}

export default CarOwnersList;
