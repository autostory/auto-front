import React from 'react';
import Relay from 'react-relay/classic';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles/index';

import isEmpty from 'lodash/isEmpty';
import { getOrderStatusTypeName } from '../../../../../common-code/constants/orderStatusTypes';

import relayContainer from '../../../../../common-code/decorators/relayContainer';
import { fromGlobalId } from '../../../../../common-code/relay/globalIdUtils';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
    padding: 15,
  },
  table: {
    minWidth: 700,
  },
  container: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
});

@relayContainer({
  car: () => Relay.QL`
      fragment on Car {
          orders {
              id
              totalBill
              request
              status
              makeAt {
                  formattedDateTime
              }
              serviceWorks{
                  id
                  action {
                      name
                  }
                  sparePartNode {
                      name
                  }
                  totalCost
              }
          }
      }
  `,
})
@withStyles(styles)
class OrdersTable extends React.Component {
  render() {
    const { classes } = this.props;
    const { orders } = this.props.car;

    return (
      <div>
        {orders.map(order => {
          return (
            <Paper key={order.id} className={classes.root}>
              <div className={classes.container}>
                <div><h1>Заказ №{fromGlobalId(order.id).id}</h1>
                  <p>Заявка:
                    {!isEmpty(order.request) && isEmpty}
                    {isEmpty(order.request) && ' Отсутствует'}
                  </p>
                  <p>Общая стоимость заказа: {order.totalBill} руб.</p>
                </div>
                <div>
                  <p>Статус: {getOrderStatusTypeName(order.status)}</p>
                  <p>Дата создания: {order.makeAt.formattedDateTime}</p>
                </div>
              </div>
              {order.serviceWorks.length > 0 && (
                <Table className={classes.table}>
                  <TableHead>
                    <TableRow>
                      <TableCell>Тип работ</TableCell>
                      <TableCell>Узел</TableCell>
                      <TableCell>Стоимость работы</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {order.serviceWorks.map(serviceWork =>
                      <TableRow key={serviceWork.id}>
                        <TableCell>{serviceWork.action.name}</TableCell>
                        <TableCell>{serviceWork.sparePartNode.name}</TableCell>
                        <TableCell>{serviceWork.totalCost} руб.</TableCell>
                      </TableRow>,
                    )}
                  </TableBody>
                </Table>
              )}
            </Paper>
          );
        })}
      </div>
    );
  }
}

export default OrdersTable;