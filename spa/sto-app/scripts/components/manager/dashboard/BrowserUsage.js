import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { PieChart, Pie, Cell, ResponsiveContainer } from 'recharts';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List/List';
import ListItem from '@material-ui/core/List/ListItem';
import GlobalStyles from '../../../../../common-code/globalStyles';

const BrowserUsage = (props) => {
  const styles = {
    paper: {
      minHeight: 344,
      padding: 10,
    },
    legend: {
      paddingTop: 20,
    },
    pieChartDiv: {
      height: 350,
      textAlign: 'center',
    },
  };

  return (
    <Paper style={styles.paper}>
      <span style={GlobalStyles.title}>Оценки по отзывам</span>

      <div style={GlobalStyles.clear} />

      <div className="row">

        <div className="col-xs-12 col-sm-8 col-md-8 col-lg-8">
          <div style={styles.pieChartDiv}>
            <ResponsiveContainer>
              <PieChart >
                <Pie
                  outerRadius={130}
                  data={props.data}
                  fill="#8884d8"
                  label
                >
                  {
                    props.data.map(item => <Cell key={item.name} fill={item.color} />)
                  }
                </Pie>
              </PieChart>
            </ResponsiveContainer>
          </div>
        </div>

        <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4">
          <div style={styles.legend}>
            <div style={styles.legend}>
              <List>
                {props.data.map(item =>
                  <ListItem
                    key={item.name}
                    leftAvatar={
                      <Avatar
                        icon={item.icon}
                        backgroundColor={item.color}
                      />
                    }
                  >
                    {item.name}
                  </ListItem>,
                )}
              </List>
            </div>
          </div>
        </div>
      </div>
    </Paper>
  );
};

BrowserUsage.propTypes = {
  data: PropTypes.array,
};

export default BrowserUsage;
