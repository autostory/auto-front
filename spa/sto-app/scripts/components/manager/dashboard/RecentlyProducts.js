import React, { PropTypes } from 'react';
import Avatar from '@material-ui/core/Avatar';
import { List, ListItem } from '@material-ui/core/List';
import Subheader from '@material-ui/core/Subheader';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/navigation/more-vert';
import IconMenu from '@material-ui/core/IconMenu';
import MenuItem from '@material-ui/core/MenuItem';
import { grey400, cyan600, white } from '@material-ui/core/styles/colors';
import { typography } from '@material-ui/core/styles';

const RecentlyProducts = (props) => {
  const styles = {
    subheader: {
      fontSize: 24,
      fontWeight: typography.fontWeightLight,
      backgroundColor: cyan600,
      color: white,
    },
  };

  const iconButtonElement = (
    <IconButton
      touch
      tooltipPosition="bottom-left"
    >
      <MoreVertIcon color={grey400} />
    </IconButton>
  );

  const rightIconMenu = (
    <IconMenu iconButtonElement={iconButtonElement}>
      <MenuItem>заказ-наряд</MenuItem>
      <MenuItem>контанты клиента</MenuItem>
      <MenuItem>транспорт клиента</MenuItem>
    </IconMenu>
  );

  return (
    <Paper>
      <List>
        <Subheader style={styles.subheader}>Ближайшие записи</Subheader>
        {props.data.map(item =>
          <div key={item.title}>
            <ListItem
              leftAvatar={<Avatar src={`http://i.pravatar.cc/150?u=${item.id}`} />}
              primaryText={item.title}
              secondaryText={item.text}
              rightIconButton={rightIconMenu}
            />
            <Divider inset />
          </div>,
        )}
      </List>
    </Paper>
  );
};

RecentlyProducts.propTypes = {
  data: PropTypes.array,
};

export default RecentlyProducts;
