import Button from '@material-ui/core/Button';
import React, { Component } from 'react';
import Relay from 'react-relay/classic';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import EditIcon from '@material-ui/icons/ModeEdit';
import PersonIcon from '@material-ui/icons/Person';
import RemoveIcon from '@material-ui/icons/Delete';
import RestoreIcon from '@material-ui/icons/Restore';
import Paper from '@material-ui/core/Paper';
import sortBy from 'lodash/sortBy';
import map from 'lodash/map';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import ContentAdd from '@material-ui/icons/Add';
import CircularProgress from '@material-ui/core/CircularProgress';
import commitMutationPromise from '../../../../../common-code/relay/commitMutationPromise';
import { fromGlobalId, toGlobalId } from '../../../../../common-code/relay/globalIdUtils';
import DeleteManagerMutation from '../../../relay/mutation/employee/DeleteManagerMutation';
import RestoreManagerMutation from '../../../relay/mutation/employee/RestoreManagerMutation';
import CreateManagerMutation from '../../../relay/mutation/employee/CreateManagerMutation';

import ConfiguredRadium from './../../../radium/ConfiguredRadium';
import relayContainer from '../../../../../common-code/decorators/relayContainer';

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          ${CreateManagerMutation.getFragment('viewer')}
          id
          myFirm {
              managers {
                  edges {
                      node {
                          ${DeleteManagerMutation.getFragment('manager')}
                          ${RestoreManagerMutation.getFragment('manager')}
                          id
                          fullName
                          email
                          phone
                          active
                      }
                  }
              }
          }
      }
  `,
})
@withRouter
@ConfiguredRadium
class MyFirmManagersList extends Component {
  state = {
    pending: false,
  };

  getContent = () => {
    const { viewer, history } = this.props;
    const { pending } = this.state;
    const { edges } = viewer.myFirm.managers;
    if (edges === null) {
      return <span>Ещё нет ни одного сотрудника в фирме.</span>;
    }

    const currentManagerId = toGlobalId('Manager', fromGlobalId(viewer.id).id);

    const managers = sortBy(map(edges, 'node'), [i => !i.active, 'fullName']);

    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>ФИО</TableCell>
            <TableCell>Телефон</TableCell>
            <TableCell>E-Mail</TableCell>
            <TableCell />
          </TableRow>
        </TableHead>
        <TableBody>
          {managers.map(manager => {
            const rowStyle = {};

            if (!manager.active) {
              rowStyle.textDecoration = 'line-through';
              rowStyle.background = '#f1f1f1';
            }

            return (
              <TableRow key={manager.id} style={rowStyle}>
                <TableCell>{manager.fullName}</TableCell>
                <TableCell>{manager.phone}</TableCell>
                <TableCell>{manager.email}</TableCell>
                <TableCell style={{ whiteSpace: 'nowrap' }}>
                  {currentManagerId === manager.id && (
                    <Button
                      component={Link}
                      size="small"
                      title="Мой профиль"
                      to="/profile"
                    >
                      <PersonIcon />
                    </Button>
                  )}
                  {currentManagerId !== manager.id && (
                    <Button
                      component={Link}
                      size="small"
                      title="Редактировать"
                      to={`/my-firm/manager/edit/${manager.id}`}
                      disabled={pending}
                    >
                      <EditIcon />
                    </Button>
                  )}
                  {currentManagerId !== manager.id && manager.active && (
                    <Button
                      size="small"
                      title="Удалить"
                      onClick={() => {
                        this.handleDeleteManager(manager);
                      }}
                      disabled={pending}
                    >
                      <RemoveIcon />
                    </Button>
                  )}
                  {currentManagerId !== manager.id && !manager.active && (
                    <Button
                      size="small"
                      title="Восстановить"
                      onClick={() => {
                        this.handleRestoreManager(manager);
                      }}
                      disabled={pending}
                    >
                      <RestoreIcon />
                    </Button>
                  )}
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    );
  };

  handleDeleteManager = (manager) => {
    const { pending } = this.state;
    if (pending) {
      return;
    }

    this.setState({ pending: true }, () => {
      commitMutationPromise(DeleteManagerMutation, { manager })
        .then(this.afterEditOrRestore)
        .catch(this.afterEditOrRestore);
    });
  };

  handleRestoreManager = (manager) => {
    const { pending } = this.state;
    if (pending) {
      return;
    }

    this.setState({ pending: true }, () => {
      commitMutationPromise(RestoreManagerMutation, { manager })
        .then(this.afterEditOrRestore)
        .catch(this.afterEditOrRestore);
    });
  };

  afterEditOrRestore = () => {
    this.setState({
      pending: false,
    });
  };

  render() {
    const { pending } = this.state;

    return (
      <div>
        <h3>Менеджеры фирмы {pending && <CircularProgress size={15} />}</h3>
        <div style={{ textAlign: 'right', marginBottom: 12 }}>
          <Button color="primary" variant="contained" component={Link} to="/my-firm/manager/add">
            <ContentAdd /> Добавить сотрудника
          </Button>
        </div>
        <Paper style={{ overflow: 'auto' }}>
          {this.getContent()}
        </Paper>
      </div>
    );
  }
}

export default MyFirmManagersList;
