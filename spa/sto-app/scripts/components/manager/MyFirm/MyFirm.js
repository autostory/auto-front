import React, { PureComponent } from 'react';
import Relay from 'react-relay/classic';
import CircularProgress from '@material-ui/core/CircularProgress';

import ConfiguredRadium from '../../../radium/ConfiguredRadium';
import MyFirmResult from './MyFirmResult';
import MyFirmRoute from './MyFirmRoute';

@ConfiguredRadium
class MyFirm extends PureComponent {
  getResult = () => (
    <Relay.RootContainer
      Component={MyFirmResult}
      route={new MyFirmRoute()}
      forceFetch
      renderLoading={() => <CircularProgress />}
      renderFetched={data => <MyFirmResult {...data} />}
      renderFailure={(error, retry) => (
        <div>
          <p>Произошла ошибка при загрузке данных.</p>
          <p>
            <button onClick={retry} >Повторить попытку.</button>
          </p>
        </div>
      )}
    />
  );

  render() {
    const result = this.getResult();

    return (
      <div>
        {result}
      </div>
    );
  }
}

export default MyFirm;
