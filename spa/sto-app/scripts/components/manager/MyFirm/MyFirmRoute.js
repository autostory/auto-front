import Relay from 'react-relay/classic';

class MyFirmRoute extends Relay.Route {
  static queries = {
    viewer: () => Relay.QL`query { viewer }`,
  };

  static routeName = 'MyFirmRoute';
}

export default MyFirmRoute;
