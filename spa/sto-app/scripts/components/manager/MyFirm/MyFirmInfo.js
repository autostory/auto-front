import React, { Component } from 'react';
import Relay from 'react-relay/classic';
import Typography from '@material-ui/core/Typography';

import ConfiguredRadium from './../../../radium/ConfiguredRadium';
import relayContainer from '../../../../../common-code/decorators/relayContainer';

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id
          myFirm {
              id
              points
              name
          }
      }
  `,
})
@ConfiguredRadium
class MyFirmInfo extends Component {
  render() {
    const { myFirm } = this.props.viewer;

    return (
      <div>
        <Typography variant="headline">{myFirm.name}</Typography>
        <Typography variant="headline">Баллы: {myFirm.points}</Typography>
      </div>
    );
  }
}

export default MyFirmInfo;
