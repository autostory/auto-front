import React, { Component } from 'react';
import Relay from 'react-relay/classic';

import MyFirmInfo from './MyFirmInfo';
import MyFirmManagersList from './MyFirmManagersList';
import MyFirmOfficesList from './MyFirmOfficesList';

import ConfiguredRadium from './../../../radium/ConfiguredRadium';
import relayContainer from '../../../../../common-code/decorators/relayContainer';

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id
          ${MyFirmInfo.getFragment('viewer')},
          ${MyFirmOfficesList.getFragment('viewer')},
          ${MyFirmManagersList.getFragment('viewer')},
      }
  `,
})
@ConfiguredRadium
class MyFirmResult extends Component {
  render() {
    const { viewer } = this.props;

    return (
      <div>
        <MyFirmInfo viewer={viewer} />
        <br />
        <MyFirmOfficesList viewer={viewer} />
        <br />
        <MyFirmManagersList viewer={viewer} />
      </div>
    );
  }
}

export default MyFirmResult;
