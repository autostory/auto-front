import React, { Component } from 'react';
import Relay from 'react-relay/classic';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import EditIcon from '@material-ui/icons/ModeEdit';
import RemoveIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import ContentAdd from '@material-ui/icons/Add';
import { withRouter } from 'react-router';
import isNull from 'lodash/isNull';
import isUndefined from 'lodash/isUndefined';
import OfficeWorkTimeView from '../../OfficeWorkTime/OfficeWorkTimeView';

import ConfiguredRadium from './../../../radium/ConfiguredRadium';
import relayContainer from '../../../../../common-code/decorators/relayContainer';

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id
          myFirm {
              id
              name
              canCreateOfficeInThatFirm
              offices {
                  itemsCount
                  officesList {
                      edges {
                          node {
                              id
                              managers {
                                  edges {
                                      node {
                                          id
                                      }
                                  }
                              }
                              cashBackPercent
                              title
                              phone
                              address
                              ${OfficeWorkTimeView.getFragment('office')}
                              boxes {
                                  id
                              }
                          }
                      }
                  }
              }
          }
      }
  `,
})
@ConfiguredRadium
@withRouter
class MyFirmOfficesList extends Component {
  getContent = () => {
    const { viewer } = this.props;
    if (viewer.myFirm.offices.itemsCount === 0) {
      return <div style={{ padding: 24 }}>Ещё нет ни одного офиса.</div>;
    }

    const { history } = this.props;

    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Название</TableCell>
            <TableCell>Адрес</TableCell>
            <TableCell>Время работы</TableCell>
            <TableCell>Телефон</TableCell>
            <TableCell>Кол-во боксов</TableCell>
            <TableCell>Кол-во сотрудников</TableCell>
            <TableCell>Процент cashback</TableCell>
            <TableCell />
          </TableRow>
        </TableHead>
        <TableBody>
          {viewer.myFirm.offices.officesList.edges.map((edge) => {
            const office = edge.node;

            const boxCount = office.boxes.length;

            let managerCount = 0;
            if (!isUndefined(office.managers.edges) && !isNull(office.managers.edges)) {
              managerCount = office.managers.edges.length;
            }

            return (
              <TableRow key={office.id}>
                <TableCell>{office.title}</TableCell>
                <TableCell>{office.address}</TableCell>
                <TableCell><OfficeWorkTimeView shortNames office={office} /></TableCell>
                <TableCell>{office.phone}</TableCell>
                <TableCell numeric>{boxCount}</TableCell>
                <TableCell numeric>{managerCount}</TableCell>
                <TableCell numeric>{office.cashBackPercent}%</TableCell>
                <TableCell style={{ whiteSpace: 'nowrap' }}>
                  <Button
                    size="small"
                    title="Редактировать"
                    onClick={() => {
                      history.push(`/my-firm/office/edit/${office.id}`);
                    }}
                  >
                    <EditIcon />
                  </Button>
                  <Button
                    size="small"
                    title="Удалить"
                    onClick={() => {
                      this.handleRemoveOfficeClick(office);
                    }}
                  >
                    <RemoveIcon />
                  </Button>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    );
  };

  handleRemoveOfficeClick = (officeId) => {
    alert('Для удаления офиса обратитесь к администратору портала.');
  };

  render() {
    const { viewer } = this.props;
    const { canCreateOfficeInThatFirm } = viewer.myFirm;

    return (
      <div>
        <h3>Офисы фирмы</h3>
        {/*<div style={{ textAlign: 'right', marginBottom: 12 }}>*/}
        {/*{canCreateOfficeInThatFirm && (*/}
        {/*<Button color="primary" variant="contained" component={Link} to="/my-firm/office/add">*/}
        {/*<ContentAdd /> Добавить офис*/}
        {/*</Button>*/}
        {/*)}*/}
        {/*</div>*/}
        <div style={{ textAlign: 'right', marginBottom: 12 }}>
          <Button
            color="primary"
            variant="contained"
            onClick={() => {
              alert('Для добавления офиса обратитесь к администратору портала.');
            }}
          >
            <ContentAdd /> Добавить офис
          </Button>
        </div>
        <Paper style={{ overflow: 'auto' }}>
          {this.getContent()}
        </Paper>
      </div>
    );
  }
}

export default MyFirmOfficesList;
