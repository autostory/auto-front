import validator from 'validator';
import isEmpty from 'lodash/isEmpty';

const emailRe = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const vinRe = new RegExp("^[A-HJ-NPR-Z\\d]{8}[\\dX][A-HJ-NPR-Z\\d]{2}\\d{6}$");

export const validateStepOne = ({ email, phone, surname, firstName }) => {
  const errors = {};

  if (!phone) {
    errors.phone = 'Укажите контактный номер.';
  } else {
    const phoneMatch = phone.match(/\d+/g);
    if (phoneMatch === null || !validator.isMobilePhone(phoneMatch.join(''), 'ru-RU', true)) {
      errors.phone = 'Укажите контактный номер.';
    }
  }

  if (email && !emailRe.test(email)) {
    errors.email = 'Указан не правильный email.';
  }

  if (!surname) {
    errors.surname = 'Укажите фамилию.';
  }

  if (!firstName) {
    errors.firstName = 'Укажите имя.';
  }

  return errors;
};

export const validateStepTwo = ({ noCarAdd, vin, year, registrationSignNumber }) => {
  const errors = {};

  if (noCarAdd) {
    return errors;
  }

  // if (vin && !vin.match(vinRe)) {
  //   errors.vin = 'Введен не правильный VIN код.';
  // }

  if (!vin && !registrationSignNumber) {
    errors.registrationSignNumber = 'Укажите VIN или Гос. номер авто.';
    errors.vin = 'Укажите VIN или Гос. номер авто.';
  }

  const today = new Date();
  if (year && (year < 1885 || year > today.getFullYear())) {
    errors.year = `Год должен быть в пределах от 1885 до ${today.getFullYear()}.`;
  }

  return errors;
};

function validate(values) {
  const firstStepErrors = validateStepOne(values);
  const secondStepErrors = validateStepTwo(values);

  let errors = {};
  if (!isEmpty(firstStepErrors)) {
    errors.firstStepErrors = firstStepErrors;
    errors = { ...errors, ...firstStepErrors };
  }

  if (!isEmpty(secondStepErrors)) {
    errors.secondStepErrors = secondStepErrors;
    errors = { ...errors, ...secondStepErrors };
  }

  return errors;
}

export default validate;
