import PropTypes from 'prop-types';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Checkbox } from 'redux-form-material-ui';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Field } from 'redux-form';
import phoneFormat from '../../../../../common-code/utils/text/phoneFormat';

const rawStyle = {
  noAddCarCaption: {
    marginBottom: 20,
  },
};

class ClientInviteInputGroup extends React.PureComponent {
  static propTypes = {
    phone: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequired,
    email: PropTypes.string.isRequired,
  };

  render() {
    const { phone, email } = this.props;

    return (
      <div>
        {email && (
          <FormControlLabel
            control={
              <Field
                name="sendEmailInvite"
                color="primary"
                component={Checkbox}
              />}
            label={<span>Отправить приглашение в приложение на e-mail <b>{email}</b></span>}
          />
        )}
        <br />
        {phone && (
          <FormControlLabel
            control={
              <Field
                name="sendSMSInvite"
                color="primary"
                component={Checkbox}
              />}
            label={<span>Отправить SMS приглашение в приложение на номер <b>{phoneFormat(phone)}</b></span>}
          />
        )}
        <Typography variant="caption" style={rawStyle.noAddCarCaption}>
          Клиент получит в приглашении ссылку на приложение, логин и пароль от приложения.
        </Typography>
      </div>
    );
  }
}

export default ClientInviteInputGroup;
