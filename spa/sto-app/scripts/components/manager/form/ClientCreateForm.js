import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Prompt } from 'react-router-dom';

import { TextField, Checkbox } from 'redux-form-material-ui';

import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import Typography from '@material-ui/core/Typography';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import isUndefined from 'lodash/isUndefined';
import isEmpty from 'lodash/isEmpty';
import isFunction from 'lodash/isFunction';
import Relay from 'react-relay/classic';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { reduxForm, Field, formValueSelector, getFormSyncErrors, getFormSubmitErrors } from 'redux-form';
import CarForm from '../../Form/Car/CarForm';
import ClientInviteInputGroup from './ClientInviteInputGroup';
import validate from './validate';

import relayContainer from '../../../../../common-code/decorators/relayContainer';

import CreateClientAsManagerMutation from './../../../relay/mutation/CreateClientAsManagerMutation';
import MobilePhoneFormInput from './../../Form/Client/MobilePhoneFormInput';

const FORM_NAME = 'CreateNewClientForm';
const selector = formValueSelector(FORM_NAME);

const phoneNormalize = (value) => {
  const numb = value.match(/\d/g) || [];

  return numb.join('');
};

const CarFormWithValues = connect(state => ({
  noCarAdd: selector(state, 'noCarAdd'),
  catalogCarTypeId: selector(state, 'catalogCarTypeId'),
  catalogCarMarkId: selector(state, 'catalogCarMarkId'),
  catalogCarModelId: selector(state, 'catalogCarModelId'),
  catalogCarGenerationId: selector(state, 'catalogCarGenerationId'),
  catalogCarSerieId: selector(state, 'catalogCarSerieId'),
  catalogCarModificationId: selector(state, 'catalogCarModificationId'),
  catalogCarEquipmentId: selector(state, 'catalogCarEquipmentId'),
}))(CarForm);

const ClientInviteInputGroupWithValues = connect(state => ({
  email: selector(state, 'email'),
  phone: selector(state, 'phone'),
}))(ClientInviteInputGroup);

const rawStyle = {
  noAddCarCaption: {
    marginBottom: 20,
  },
};

@withRouter
@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          accessRights {
              canCreateUserAsManager
          }
          ${CreateClientAsManagerMutation.getFragment('viewer')},
      }
  `,
})
@connect(state => ({
  noCarAdd: selector(state, 'noCarAdd'),
  formSyncErrors: getFormSyncErrors(FORM_NAME)(state),
  formSubmitErrors: getFormSubmitErrors(FORM_NAME)(state),
}))
class ClientCreateForm extends React.PureComponent {
  static propTypes = {
    onSuccess: PropTypes.func,
    history: ReactRouterPropTypes.history,
  };

  static defaultProps = {
    history: null,
    onSuccess: () => {
    },
  };

  state = {
    activeStep: 0,
    addClientId: null,
  };

  componentDidUpdate(prevProps) {
    if (this.props.submitFailed && !this.props.submitting && prevProps.submitting) {
      const fse = this.getFirstStepSubmitErrors();
      const sse = this.getSecondStepSubmitErrors();

      if (!isEmpty(fse)) {
        this.setState({
          activeStep: 0,
        });
      } else if ((!isEmpty(sse))) {
        this.setState({
          activeStep: 1,
        });
      }
    }
  }

  getFirstStepSubmitErrors = () => {
    const { phone, email, surname, firstName, patronymic, ...rest } = this.props.formSubmitErrors;

    return {
      ...phone,
      ...email,
      ...surname,
      ...firstName,
      ...patronymic,
    };
  };

  getSecondStepSubmitErrors = () => {
    const { phone, email, surname, firstName, patronymic, ...rest } = this.props.formSubmitErrors;

    return {
      ...rest,
    };
  };

  getSteps = () => ['Основные данные клиента', 'Автомобиль клиента', 'Уведомление клиента'];

  getStepContent = (step) => {
    const { viewer } = this.props;

    switch (step) {
      case 0:
        return (
          <div>
            <div>
              <form autoComplete="off">
                <div style={{ margin: 5 }}>
                  <Field
                    inputProps={{
                      maxLength: 250,
                    }}
                    component={TextField}
                    name="email"
                    label="E-mail"
                  />
                  <span style={{ margin: 5, display: 'inline-block' }} />
                  <Field
                    inputProps={{
                      maxLength: 250,
                    }}
                    component={MobilePhoneFormInput}
                    name="phone"
                    label="Сотовый"
                    normalize={phoneNormalize}
                  />
                  <br />
                  <Field
                    inputProps={{
                      maxLength: 250,
                    }}
                    required
                    component={TextField}
                    name="surname"
                    label="Фамилия"
                  />
                  <span style={{ margin: 5, display: 'inline-block' }} />
                  <Field
                    inputProps={{
                      maxLength: 250,
                    }}
                    required
                    component={TextField}
                    name="firstName"
                    label="Имя"
                  />
                  <span style={{ margin: 5, display: 'inline-block' }} />
                  <Field
                    inputProps={{
                      maxLength: 250,
                    }}
                    component={TextField}
                    name="patronymic"
                    label="Отчество"
                  />
                </div>
              </form>
            </div>
          </div>
        );
      case 1:
        return (
          <div>
            <form autoComplete="off">
              <div>
                <FormControlLabel
                  control={
                    <Field
                      name="noCarAdd"
                      color="primary"
                      component={Checkbox}
                    />}
                  label="Не добавлять авто"
                />
              </div>
              {!this.props.noCarAdd && (
                <CarFormWithValues
                  viewer={viewer}
                  initialized
                  change={this.props.change}
                />
              )}
              {this.props.noCarAdd && (
                <Typography variant="caption" style={rawStyle.noAddCarCaption}>
                  Автомобиль клиенту можно будет добавить позже из карточки клиента или при создании заказа.
                </Typography>
              )}
            </form>
          </div>
        );
      case 2:
        return (
          <div>
            <ClientInviteInputGroupWithValues />
            <br />
          </div>
        );
      default:
        return 'Unknown step';
    }
  };

  handleNext = () => {
    const { formSyncErrors, touch, handleSubmit } = this.props;
    if (this.state.activeStep === 0) {
      const { firstStepErrors } = formSyncErrors;
      const fse = this.getFirstStepSubmitErrors();
      if (!firstStepErrors && isEmpty(fse)) {
        this.setState({
          activeStep: this.state.activeStep + 1,
        });
      } else {
        const toTouch = [];

        if (formSyncErrors) {
          for (const key in firstStepErrors) {
            if (!isUndefined(firstStepErrors[key])) {
              toTouch.push(key);
            }
          }
        }

        if (!isEmpty(fse)) {
          for (const key in fse) {
            if (!isUndefined(fse[key])) {
              toTouch.push(key);
            }
          }
        }

        touch(...toTouch);
      }

      return;
    }

    if (this.state.activeStep === 1) {
      const { secondStepErrors } = formSyncErrors;
      const sse = this.getSecondStepSubmitErrors();
      if (!secondStepErrors && isEmpty(sse)) {
        this.setState({
          activeStep: this.state.activeStep + 1,
        });
      } else {
        const toTouch = [];

        if (secondStepErrors) {
          for (const key in secondStepErrors) {
            if (!isUndefined(secondStepErrors[key])) {
              toTouch.push(key);
            }
          }
        }

        if (!isEmpty(sse)) {
          for (const key in sse) {
            if (!isUndefined(sse[key])) {
              toTouch.push(key);
            }
          }
        }

        touch(...toTouch);
      }

      return;
    }

    if (this.state.activeStep === (this.getSteps().length - 1)) {
      const submit = handleSubmit();
      submit.then((data) => {
        if (!isUndefined(data.newClientId)) {
          if (isFunction(this.props.onSuccess)) {
            this.props.onSuccess({ newClientId: data.newClientId });
          }

          this.setState({
            activeStep: this.state.activeStep + 1,
            addClientId: data.newClientId,
          });
        }
      });

      return;
    }

    this.setState({
      activeStep: this.state.activeStep + 1,
    });
  };

  handleBack = () => {
    this.setState({
      activeStep: this.state.activeStep - 1,
    });
  };

  goToNewClient = () => {
    const { history } = this.props;

    history.push(`/client/${this.state.addClientId}`);
  };

  handleReset = () => {
    this.props.reset();
    this.setState({
      activeStep: 0,
      addClientId: null,
    });
  };

  render() {
    const { viewer, submitting } = this.props;

    if (!viewer || !viewer.accessRights || !viewer.accessRights.canCreateUserAsManager) {
      return <div>У вас нет прав для добавления клиентов.</div>;
    }

    const steps = this.getSteps();
    const { activeStep } = this.state;

    return (
      <div>
        <Prompt
          when={this.props.dirty && activeStep !== 3}
          message={location =>
            'На странице есть не сохраненные изменения. Вы действительно хотите покинуть страницу?'
          }
        />
        <Stepper activeStep={activeStep} orientation="vertical">
          {steps.map((label, index) => {
            const { formSyncErrors, submitFailed } = this.props;
            const { firstStepErrors, secondStepErrors } = formSyncErrors;
            const firstStepSubmitErrors = this.getFirstStepSubmitErrors();
            const secondStepSubmitErrors = this.getSecondStepSubmitErrors();

            const labelProps = {};

            if (index === 0 && submitFailed && !isEmpty(firstStepErrors) && !isEmpty(firstStepSubmitErrors)) {
              labelProps.error = true;
            }

            if (index === 1 && submitFailed && !isEmpty(secondStepErrors) && !isEmpty(secondStepSubmitErrors)) {
              labelProps.error = true;
            }

            return (
              <Step key={label}>
                <StepLabel {...labelProps}>{label}</StepLabel>
                <StepContent>
                  <div>{this.getStepContent(index)}</div>
                  <div>
                    <div>
                      <Button
                        disabled={activeStep === 0}
                        onClick={this.handleBack}
                      >
                        Назад
                      </Button>
                      <Button
                        color="primary"
                        variant="raised"
                        onClick={this.handleNext}
                        disabled={submitting}
                      >
                        {activeStep === steps.length - 1 ? 'Добавить клиента' : 'Далее'}
                      </Button>
                      {submitting && <CircularProgress size={24} />}
                    </div>
                  </div>
                </StepContent>
              </Step>
            );
          })}
        </Stepper>
        {activeStep === steps.length && (
          <Paper>
            <span>Клиент добавлен.</span>
            <br />
            <Button variant="raised" color="primary" onClick={this.goToNewClient}>Перейти к новому клиенту</Button>
            <br />
            <Button variant="raised" color="primary" onClick={this.handleReset}>Добавить ещё одного клиента</Button>
          </Paper>
        )}
      </div>
    );
  }
}

export default reduxForm({
  form: FORM_NAME,
  validate,
})(ClientCreateForm);
