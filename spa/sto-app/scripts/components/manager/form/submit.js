import isEmpty from 'lodash/isEmpty';
import isFunction from 'lodash/isFunction';
import cloneDeep from 'lodash/cloneDeep';
import { SubmissionError } from 'redux-form';

import CreateClientAsManagerMutation from '../../../relay/mutation/CreateClientAsManagerMutation';
import parseMutationInputValidationErrorMessageForForm from '../../../../../common-code/utils/parseMutationInputValidationErrorMessageForForm';
import commitMutationPromise from '../../../../../common-code/relay/commitMutationPromise';

function submit(viewer, values, onClientCreated) {
  const onFailure = (errors) => {
    console.log('onFailure errors', errors);
    if (errors && errors instanceof SubmissionError) {
      throw errors;
    }

    alert('При добавлении пользователя произошла ошибка. Обновите страницу и/или повторите попытку.');
  };

  const onSuccess = (response) => {
    if (response.createClientAsManager.errors.messages.length > 0) {
      const errors = response.createClientAsManager.errors.messages;
      const parsedErrors = parseMutationInputValidationErrorMessageForForm(errors);

      if (!isEmpty(parsedErrors.mobilePhone)) {
        parsedErrors.phone = cloneDeep(parsedErrors.mobilePhone);
        delete (parsedErrors.mobilePhone);
      }

      throw new SubmissionError(parsedErrors);
    } else {
      const { newClientId } = response.createClientAsManager;
      if (isFunction(onClientCreated)) {
        onClientCreated(newClientId);
      }
      return { newClientId };
    }
  };

  return commitMutationPromise(CreateClientAsManagerMutation, { viewer, input: values })
    .then(onSuccess)
    .catch(onFailure);
}

export default submit;
