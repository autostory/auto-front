import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import { Link } from 'react-router-dom';
import Relay from 'react-relay/classic';
import { reduxForm } from 'redux-form';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import ManagerForm from '../../../Form/Manager/ManagerForm';
import validateManager from './validateManager';

export const FORM_NAME = 'AddManagerForm';

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          myFirm {
              id
          }
      }
  `,
})
class AddManagerForm extends React.PureComponent {
  onSave = () => {
    const { handleSubmit } = this.props;

    handleSubmit();
  };

  render() {
    const { dirty, invalid, submitting } = this.props;
    const inputDisabled = this.props.submitting;

    return (
      <FormGroup row={false} style={{ width: 400 }}>
        <ManagerForm
          disabled={inputDisabled}
        />
        <div>
          <Button
            variant="contained"
            color="primary"
            disabled={!dirty || inputDisabled || invalid}
            onClick={this.onSave}
            style={{
              marginRight: 15,
              marginTop: 15,
            }}
          >
            Добавить
          </Button>
          <Button
            variant="contained"
            color="primary"
            disabled={inputDisabled}
            component={Link}
            to="/my-firm"
            style={{ marginTop: 15 }}
          >
            Отмена
          </Button>
        </div>
        {dirty && invalid && (
          <Typography color="error">
            В форме есть ошибки. Исправьте их и сотрудника можно будет добавить.
          </Typography>
        )}
        {submitting && <CircularProgress />}
      </FormGroup>
    );
  }
}

export default reduxForm({
  form: FORM_NAME,
  validate: validateManager,
  initialValues: {
    firstName: '',
    surname: '',
    patronymic: '',
    email: '',
    phone: '',
  },
})(AddManagerForm);
