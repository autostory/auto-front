import validator from 'validator';

const emailRe = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

function validateManager(values) {
  const errors = {};

  if (values.email && !emailRe.test(values.email)) {
    errors.email = 'Указан не правильный email.';
  }

  if (!values.firstName) {
    errors.firstName = 'Укажите фамилию сотрудника.';
  }

  if (!values.surname) {
    errors.surname = 'Укажите имя сотрудника.';
  }

  if (!values.phone) {
    errors.phone = 'Укажите телефон сотрудника.';
  } else {
    const phoneMatch = values.phone.match(/\d+/g);
    if (phoneMatch === null || !validator.isMobilePhone(phoneMatch.join(''), 'ru-RU', true)) {
      errors.phone = 'Укажите контактный номер';
    }

    // if (!values.email) {
    //   errors.email = 'Укажите E-mail сотрудника.';
    // }
  }

  return errors;
}

export default validateManager;
