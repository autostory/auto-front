import cloneDeep from 'lodash/cloneDeep';
import isEmpty from 'lodash/isEmpty';
import { SubmissionError } from 'redux-form';
import CreateManagerMutation from '../../../../relay/mutation/employee/CreateManagerMutation';
import parseMutationInputValidationErrorMessageForForm
  from '../../../../../../common-code/utils/parseMutationInputValidationErrorMessageForForm';
import commitMutationPromise from '../../../../../../common-code/relay/commitMutationPromise';

function submitManagerInfo(viewer, values) {
  const onFailure = (errors) => {
    console.log('onFailure errors', errors);
    if (errors && errors instanceof SubmissionError) {
      throw errors;
    }

    alert('При добавлении сотрудника произошла ошибка. Обновите страницу и/или повторите попытку.');
  };

  const onSuccess = (response) => {
    if (response.createManagerMutation.errors.messages.length > 0) {
      const errors = response.createManagerMutation.errors.messages;
      const parsedErrors = parseMutationInputValidationErrorMessageForForm(errors);

      if (!isEmpty(parsedErrors.mobilePhone)) {
        parsedErrors.phone = cloneDeep(parsedErrors.mobilePhone);
        delete (parsedErrors.mobilePhone);
      }

      throw new SubmissionError(parsedErrors);
    }
  };

  return commitMutationPromise(CreateManagerMutation, { viewer, input: values })
    .then(onSuccess)
    .catch(onFailure);
}

export default submitManagerInfo;
