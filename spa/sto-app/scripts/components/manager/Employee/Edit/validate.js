import validator from 'validator';

const emailRe = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

function validate(values) {
  const errors = {};

  if (!values.firstName) {
    errors.firstName = 'Укажите фамилию сотрудника.';
  }

  if (!values.surname) {
    errors.surname = 'Укажите имя сотрудника.';
  }

  if (values.phone && values.phone !== '') {
    const phoneMatch = values.phone.match(/\d+/g);
    if (phoneMatch === null || !validator.isMobilePhone(phoneMatch.join(''), 'ru-RU', true)) {
      errors.phone = 'Укажите контактный номер';
    }
  }

  if (!values.email && errors.phone) {
    errors.email = 'Укажите e-mail сотрудника.';
  }

  if (values.email && !emailRe.test(values.email)) {
    errors.email = 'Указан не правильный e-mail.';
  }

  return errors;
}

export default validate;
