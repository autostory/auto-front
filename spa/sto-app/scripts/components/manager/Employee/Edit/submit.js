import cloneDeep from 'lodash/cloneDeep';
import isEmpty from 'lodash/isEmpty';
import { SubmissionError } from 'redux-form';
import EditManagerMutation from '../../../../relay/mutation/employee/EditManagerMutation';
import parseMutationInputValidationErrorMessageForForm
  from '../../../../../../common-code/utils/parseMutationInputValidationErrorMessageForForm';
import commitMutationPromise from '../../../../../../common-code/relay/commitMutationPromise';

function submit(manager, values) {
  const onFailure = (errors) => {
    console.log('onFailure errors', errors);
    if (errors && errors instanceof SubmissionError) {
      throw errors;
    }

    alert('При редактировании сотрудника произошла ошибка. Обновите страницу и/или повторите попытку.');
  };

  const onSuccess = (response) => {
    if (response.editManagerMutation.errors.messages.length > 0) {
      const errors = response.editManagerMutation.errors.messages;
      const parsedErrors = parseMutationInputValidationErrorMessageForForm(errors);

      if (!isEmpty(parsedErrors.mobilePhone)) {
        parsedErrors.phone = cloneDeep(parsedErrors.mobilePhone);
        delete (parsedErrors.mobilePhone);
      }

      throw new SubmissionError(parsedErrors);
    }
  };

  return commitMutationPromise(EditManagerMutation, { manager, input: values })
    .then(onSuccess)
    .catch(onFailure);
}

export default submit;
