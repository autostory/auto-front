import React, { Component } from 'react';
import Relay from 'react-relay/classic';
import { Link } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withRouter } from 'react-router';

import ConfiguredRadium from '../../../../radium/ConfiguredRadium';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import CreateOfficeMutation from '../../../../relay/mutation/office/CreateOfficeMutation';

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id
          ${CreateOfficeMutation.getFragment('viewer')},
          myFirm {
              id
              name
              canCreateOfficeInThatFirm
          }
      }
  `,
})
@ConfiguredRadium
@withRouter
class AddOfficeResult extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      phone: '',
      address: '',
      workTimeDescription: '',
      pendingOfficeAdd: false,
    };
  }

  onFieldChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSave = () => {
    if (this.state.pendingOfficeAdd) {
      return;
    }

    const onSuccess = (data) => {
      this.setState({
        pendingOfficeAdd: false,
      });

      const officeId = data.createOffice.newOffice.id;
      const { history } = this.props;

      history.push(`/my-firm/office/edit/${officeId}`);
    };

    const onFailure = (e) => {
      this.setState({
        pendingOfficeAdd: false,
      });

      console.log('onFailure e =', e);
      alert('Что-то пошло не так при создании офиса');
    };

    const { relay, viewer } = this.props;

    const input = {
      title: this.state.title,
      workTimeDescription: this.state.workTimeDescription,
      address: this.state.address,
      phone: this.state.phone,
      firmId: viewer.myFirm.id,
    };

    const createOfficeMutation = new CreateOfficeMutation({ input, viewer });
    this.setState({ pendingOfficeAdd: true }, () => {
      relay.commitUpdate(createOfficeMutation, { onFailure, onSuccess });
    });
  };

  render() {
    if (!this.props.viewer.myFirm.canCreateOfficeInThatFirm) {
      return <h3>У вас нет прав для создания офиса для этой фирмы.</h3>;
    }
    const { myFirm } = this.props.viewer;

    const invalid = this.state.title.trim() === '' || this.state.phone === '' || this.state.address === '';

    return (
      <div>
        <h3>Добавление офиса в фирму <b>{myFirm.name}</b></h3>
        <div style={{ width: 400 }}>
          <TextField
            multiline
            inputProps={{
              maxLength: 250,
            }}
            name="title"
            required
            disabled={this.state.pendingOfficeAdd}
            label="Название офиса"
            value={this.state.title}
            fullWidth
            onChange={this.onFieldChange}
          />
          <TextField
            multiline
            inputProps={{
              maxLength: 250,
            }}
            name="phone"
            required
            disabled={this.state.pendingOfficeAdd}
            label="Контакты офиса"
            value={this.state.phone}
            fullWidth
            onChange={this.onFieldChange}
          />
          <TextField
            multiline
            inputProps={{
              maxLength: 250,
            }}
            name="address"
            required
            disabled={this.state.pendingOfficeAdd}
            label="Адрес офиса"
            value={this.state.address}
            fullWidth
            onChange={this.onFieldChange}
          />
        </div>
        <br />
        <br />
        <Button
          disabled={this.state.pendingOfficeAdd || invalid}
          color="primary"
          onClick={this.handleSave}
          variant="raised"
          style={{ marginRight: 20 }}
        >
          Добавить
        </Button>
        {this.state.pendingOfficeAdd && <CircularProgress />}
        <Button
          component={Link}
          to="/my-firm"
          disabled={this.state.pendingOfficeAdd}
        >
          Отмена
        </Button>

      </div>
    );
  }
}

export default AddOfficeResult;
