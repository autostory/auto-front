import React, { Component } from 'react';
import Relay from 'react-relay/classic';

import AddOfficeRoute from './AddOfficeRoute';
import AddOfficeResult from './AddOfficeResult';
import ConfiguredRadium from './../../../../radium/ConfiguredRadium';

@ConfiguredRadium
class AddOffice extends Component {
  getResult = ComponentWithRelay => (
    <Relay.RootContainer
      Component={ComponentWithRelay}
      route={new AddOfficeRoute()}
      forceFetch
      renderLoading={() => <div>Загрузка формы добавления офиса...</div>}
      renderFetched={data => <ComponentWithRelay {...data} />}
      renderFailure={(error, retry) => (
        <div>
          <p>Произошла ошибка при загрузке данных.</p>
          <p>
            <button onClick={retry} >Повторить попытку.</button>
          </p>
        </div>
      )}
    />
  );

  render() {
    const container = this.getResult(AddOfficeResult);

    return (
      <div>
        {container}
      </div>
    );
  }
}

export default AddOffice;
