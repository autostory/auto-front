import Relay from 'react-relay/classic';

class AddOfficeRoute extends Relay.Route {
  static queries = {
    viewer: () => Relay.QL`query { viewer }`,
  };

  static routeName = 'AddOfficeRoute';
}

export default AddOfficeRoute;
