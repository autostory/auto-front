import React, { Component } from 'react';
import Relay from 'react-relay/classic';
import PropTypes from 'prop-types';

import ConfiguredRadium from '../../../../radium/ConfiguredRadium';

import EditOfficeRoute from './EditOfficeRoute';
import EditOfficeContainer from './EditOfficeContainer';
import EditOfficeResult from './EditOfficeResult';

@ConfiguredRadium
class EditOffice extends Component {
  static propTypes = {
    officeId: PropTypes.string.isRequired,
  };

  getResult = ComponentWithRelay => (
    <Relay.RootContainer
      Component={ComponentWithRelay}
      route={new EditOfficeRoute()}
      forceFetch
      renderLoading={() => <div>Загрузка информации о офисе...</div>}
      renderFetched={data => <ComponentWithRelay {...data} />}
      renderFailure={(error, retry) => (
        <div>
          <p>Произошла ошибка при загрузке данных.</p>
          <p>
            <button onClick={retry} >Повторить попытку.</button>
          </p>
        </div>
      )}
    />
  );

  render() {
    const { officeId } = this.props;

    const EditOfficeContainerWithRelay = Relay.createContainer(EditOfficeContainer, {
      initialVariables: {
        officeId,
      },
      fragments: {
        viewer: () => Relay.QL`
            fragment on Viewer {
                office (officeId: $officeId) {
                    ${EditOfficeResult.getFragment('office')},
                }
                myFirm {
                    ${EditOfficeResult.getFragment('firm')},
                }
            }
        `,
      },
    });

    const officeEditContainer = this.getResult(EditOfficeContainerWithRelay);

    return (
      <div>
        {officeEditContainer}
      </div>
    );
  }
}

export default EditOffice;
