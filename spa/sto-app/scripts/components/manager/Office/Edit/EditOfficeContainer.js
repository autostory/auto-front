import React, { Component } from 'react';

import EditOfficeResult from './EditOfficeResult';

class EditOfficeContainer extends Component {
  render() {
    const { office, myFirm } = this.props.viewer;

    return (
      <div>
        <EditOfficeResult office={office} firm={myFirm} />
      </div>
    );
  }
}

export default EditOfficeContainer;
