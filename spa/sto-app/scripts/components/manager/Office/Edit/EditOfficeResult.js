import cloneDeep from 'lodash/cloneDeep';
import React, { Component } from 'react';
import Relay from 'react-relay/classic';
import { Link } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';

import map from 'lodash/map';
import difference from 'lodash/difference';
import isEqual from 'lodash/isEqual';

import ConfiguredRadium from '../../../../radium/ConfiguredRadium';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import EditOfficeMutation from '../../../../relay/mutation/office/EditOfficeMutation';
import OfficeWorkTimeEdit from '../../../OfficeWorkTime/OfficeWorkTimeEdit';
import parseRelayWorkTimes from '../../../OfficeWorkTime/parseRelayWorkTimes';
import OfficeBoxes from './OfficeBoxes';
import OfficeManagers from './OfficeManagers';

@relayContainer({
  office: () => Relay.QL`
      fragment on Office {
          id
          title
          phone
          address
          managers {
              edges {
                  node {
                      id
                  }
              }
          }
          workTimes {
              id
              start {
                  formattedTime
                  formattedDateTime
              }
              end {
                  formattedTime
                  formattedDateTime
              }
              dayOfWeek
              holiday
              roundTheClock
          }
          cashBackPercent
          ${EditOfficeMutation.getFragment('office')},
          ${OfficeWorkTimeEdit.getFragment('office')},
          ${OfficeBoxes.getFragment('office')},
          ${OfficeManagers.getFragment('office')},
      }
  `,
  firm: () => Relay.QL`
      fragment on Firm {
          id
          ${OfficeManagers.getFragment('firm')},
      }
  `,
})
@ConfiguredRadium
class EditOfficeResult extends Component {
  constructor(props) {
    super(props);

    const { office } = props;

    const officeManagersIds = map(office.managers.edges, 'node.id');

    const initialWorkTimes = parseRelayWorkTimes(office.workTimes);
    this.state = {
      workTimesError: false,
      officeManagersIds,
      title: office.title,
      phone: office.phone,
      address: office.address,
      pendingOfficeEdit: false,
      workTimes: cloneDeep(initialWorkTimes),
      initialWorkTimes,
    };
  }

  componentDidUpdate(prevProps) {
    if (!isEqual(prevProps.office.workTimes, this.props.office.workTimes)) {
      const initialWorkTimes = parseRelayWorkTimes(this.props.office.workTimes);

      this.setState({
        initialWorkTimes,
      });
    }
  }

  onOfficeWorkTimeErrorChange = (workTimesError) => {
    this.setState({
      workTimesError,
    });
  };

  onOfficeWorkTimeChange = (workTimes) => {
    this.setState({
      workTimes,
    });
  };

  onFieldChange = (event) => {
    const key = event.target.name;
    const { value } = event.target;

    this.setState({
      [key]: value,
    });
  };

  haveChanges = () => {
    const { office } = this.props;
    const { title, phone, address, officeManagersIds, workTimes, initialWorkTimes } = this.state;
    const initialOfficeManagersIds = [...map(office.managers.edges, 'node.id')];

    return office.title !== title
      || office.phone !== phone
      || office.address !== address
      || !isEqual(workTimes, initialWorkTimes)
      || difference(initialOfficeManagersIds, officeManagersIds).length !== 0
      || difference(officeManagersIds, initialOfficeManagersIds).length !== 0;
  };

  handleSave = () => {
    if (this.state.pendingOfficeEdit) {
      return;
    }

    const onSuccess = () => {
      this.setState({
        pendingOfficeEdit: false,
      }, () => {
        const { office } = this.props;

        this.setState({
          initialWorkTimes: parseRelayWorkTimes(office.workTimes),
        });
      });
    };

    const onFailure = (e) => {
      this.setState({
        pendingOfficeEdit: false,
      });

      alert('Что-то пошло не так при сохранении');
    };

    const { relay, office } = this.props;
    const { title, address, phone, officeManagersIds, workTimes } = this.state;

    const input = {
      title,
      address,
      workTimes,
      phone,
      officeMangers: officeManagersIds,
    };

    const editOfficeMutation = new EditOfficeMutation({ input, office });
    this.setState({ pendingOfficeEdit: true }, () => {
      relay.commitUpdate(editOfficeMutation, { onFailure, onSuccess });
    });
  };

  handleOfficeManagersChange = (selectedOfficeManagersId) => {
    this.setState({
      officeManagersIds: [...selectedOfficeManagersId],
    });
  };

  render() {
    const { office, firm } = this.props;
    const { workTimesError } = this.state;

    const haveChanges = this.haveChanges();
    const invalid = this.state.title.trim() === '' || this.state.phone === '' || this.state.address === '';

    return (
      <div style={{ display: 'flex', width: '100%' }}>
        <Grid container spacing={24}>
          <Grid item xs={12} md={6}>
            <Paper style={{ padding: 24 }}>
              <Typography variant="headline">Основная информация</Typography>
              <div style={{ width: 400 }}>
                <TextField
                  multiline
                  required
                  inputProps={{
                    maxLength: 250,
                  }}
                  error={this.state.title.trim() === ''}
                  name="title"
                  disabled={this.state.pendingOfficeEdit}
                  label="Название офиса"
                  value={this.state.title}
                  fullWidth
                  onChange={this.onFieldChange}
                />
                <TextField
                  multiline
                  required
                  inputProps={{
                    maxLength: 250,
                  }}
                  error={this.state.phone.trim() === ''}
                  name="phone"
                  disabled={this.state.pendingOfficeEdit}
                  label="Контакты офиса"
                  value={this.state.phone}
                  fullWidth
                  onChange={this.onFieldChange}
                />
                <TextField
                  multiline
                  required
                  inputProps={{
                    maxLength: 250,
                  }}
                  error={this.state.address.trim() === ''}
                  name="address"
                  disabled={this.state.pendingOfficeEdit}
                  label="Адрес офиса"
                  value={this.state.address}
                  fullWidth
                  onChange={this.onFieldChange}
                />
                <OfficeWorkTimeEdit
                  onErrorChange={this.onOfficeWorkTimeErrorChange}
                  onChange={this.onOfficeWorkTimeChange}
                  office={office}
                  shortNames
                />
              </div>
              <br />
              <Typography variant="subheading">Сотрудники офиса</Typography>
              <OfficeManagers
                onChange={this.handleOfficeManagersChange}
                office={office}
                firm={firm}
              />
              <br />
              <br />
              <div>
                <Typography>Процент cashback <b>{office.cashBackPercent}%</b></Typography>
                <i>Для изменения значения cashback - обратитесь к администраторам сайта</i>
              </div>
              <br />
              <br />
              <Button
                variant="raised"
                disabled={(workTimesError || !haveChanges) || this.state.pendingOfficeEdit || invalid}
                style={{ marginRight: 20 }}
                color="primary"
                onClick={this.handleSave}
              >
                Сохранить
              </Button>
              {this.state.pendingOfficeEdit && <CircularProgress />}
              <Button
                disabled={this.state.pendingOfficeEdit}
                component={Link}
                to="/my-firm"
              >
                Отмена
              </Button>
            </Paper>
          </Grid>
          <Grid item xs={12} md={6}>
            <Paper style={{ padding: 24 }}>
              <Typography variant="headline">Боксы офиса</Typography>
              <OfficeBoxes office={office} />
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default EditOfficeResult;
