import React from 'react';
import map from 'lodash/map';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import Relay from 'react-relay/classic';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 300,
    maxWidth: 500,
    width: '100%',
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: theme.spacing.unit / 4,
  },
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: (ITEM_HEIGHT * 4.5) + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

@relayContainer({
  office: () => Relay.QL`
      fragment on Office {
          id
          managers {
              edges {
                  node {
                      id
                      fullName
                      email
                  }
              }
          }
      }
  `,
  firm: () => Relay.QL`
      fragment on Firm {
          id
          managers {
              edges {
                  node {
                      id
                      fullName
                      email
                      active
                  }
              }
          }
      }
  `,
})
class OfficeManagers extends React.PureComponent {
  constructor(props) {
    super(props);

    const { office } = props;
    const selectedManages = map(office.managers.edges, 'node.id');

    this.state = {
      selectedManages,
    };
  }

  handleChange = (event) => {
    const { onChange } = this.props;

    this.setState({ selectedManages: event.target.value }, () => onChange(this.state.selectedManages));
  };

  render() {
    const { classes, theme, firm } = this.props;
    const allActiveManagers = map(firm.managers.edges, 'node').filter(manager => manager.active);
    const { selectedManages } = this.state;

    return (
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="select-multiple-chip">Укажите менеджеров этого для этого офиса</InputLabel>
        <Select
          multiple
          value={selectedManages}
          onChange={this.handleChange}
          input={<Input id="select-multiple-chip" />}
          renderValue={selected => (
            <div className={classes.chips}>
              {allActiveManagers.map((manager) => {
                if (selected.indexOf(manager.id) === -1) {
                  return false;
                }

                let label = manager.fullName;
                if (label === '') {
                  label = manager.email;
                }

                return <Chip key={manager.id} label={label} className={classes.chip} />;
              })}
            </div>
          )}
          MenuProps={MenuProps}
        >
          {allActiveManagers.map((manager) => {
            let label = manager.fullName;
            if (label === '') {
              label = manager.email;
            }

            return (
              <MenuItem
                key={manager.id}
                value={manager.id}
                style={{
                  fontWeight:
                    selectedManages.indexOf(manager.id) === -1
                      ? theme.typography.fontWeightRegular
                      : theme.typography.fontWeightMedium,
                }}
              >
                {label}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    );
  }
}

export default withStyles(styles, { withTheme: true })(OfficeManagers);
