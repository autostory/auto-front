import React from 'react';
import Relay from 'react-relay/classic';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import EditIcon from '@material-ui/icons/ModeEdit';
import RemoveIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import RestoreIcon from '@material-ui/icons/Restore';
import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import get from 'lodash/get';
import isUndefined from 'lodash/isUndefined';
import CircularProgress from '@material-ui/core/CircularProgress';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';
import CreateBoxMutation from '../../../../relay/mutation/box/CreateBoxMutation';
import EditBoxMutation from '../../../../relay/mutation/box/EditBoxMutation';

@relayContainer({
  office: () => Relay.QL`
      fragment on Office {
          id
          boxes {
              id
              active
              title
          }
          ${EditBoxMutation.getFragment('office')},
          ${CreateBoxMutation.getFragment('office')},
      }
  `,
})
class OfficeBoxes extends React.PureComponent {
  state = {
    editBox: [],
    editBoxValues: {},
    pending: false,
  };

  onSuccessEdit = box => () => {
    this.setState({
      pending: false,
    }, () => {
      const { relay } = this.props;
      relay.forceFetch();

      const { editBoxValues } = this.state;

      if (!isUndefined(editBoxValues[box.id])) {
        delete editBoxValues[box.id];
      }

      this.setState({
        editBoxValues: { ...editBoxValues },
      });
    });
  };

  onSuccessActiveSave = box => () => {
    this.setState({
      pending: false,
    }, () => {
      const { relay } = this.props;
      relay.forceFetch();
    });
  };

  onFailure = () => {
    this.setState({
      pending: false,
    });

    alert('Что-то пошло не так при сохранении');
  };

  onFieldChange = boxId => (event) => {
    const key = event.target.name;
    const { value } = event.target;
    const { editBoxValues } = this.state;
    if (isUndefined(editBoxValues[boxId])) {
      editBoxValues[boxId] = {};
    }
    editBoxValues[boxId][key] = value;

    this.setState({
      editBoxValues: { ...editBoxValues },
    });
  };

  finishEditBox = box => () => {
    const { editBox } = this.state;
    const newEditBox = editBox.filter(boxId => boxId !== box.id);

    this.setState({
      editBox: newEditBox,
    });
  };

  onSuccessCreate = () => {
    this.setState({
      pending: false,
    }, () => {
      const { relay } = this.props;
      relay.forceFetch();

      const { editBoxValues } = this.state;

      if (!isUndefined(editBoxValues.new)) {
        delete editBoxValues.new;
      }

      this.setState({
        editBoxValues: { ...editBoxValues },
      });
    });
  };

  createOffice = () => {
    const { pending } = this.state;

    if (pending) {
      return;
    }

    const { editBoxValues } = this.state;
    const { relay, office } = this.props;

    const title = get(editBoxValues, '[new].title', '');

    if (title === '') {
      return;
    }

    const input = {
      title,
      officeId: office.id,
    };

    const createBoxMutation = new CreateBoxMutation({ input, office });
    this.setState({ pending: true }, () => {
      relay.commitUpdate(createBoxMutation, { onFailure: this.onFailure, onSuccess: this.onSuccessCreate });
    });
  };

  saveChanges = box => () => {
    const { pending } = this.state;
    if (pending) {
      return;
    }

    const { editBoxValues } = this.state;
    const { relay, office } = this.props;

    const title = get(editBoxValues, `[${box.id}].title`, '');

    if (title === '') {
      return;
    }

    const input = {
      title,
      boxId: box.id,
    };

    const editBoxMutation = new EditBoxMutation({ input, office, box });
    this.setState({ pending: true }, () => {
      relay.commitUpdate(editBoxMutation, { onFailure: this.onFailure, onSuccess: this.onSuccessEdit(box) });
    });
  };

  saveBoxActive = box => () => {
    const { pending } = this.state;
    if (pending) {
      return;
    }

    const { relay, office } = this.props;

    const input = {
      active: !box.active,
      boxId: box.id,
    };

    const editBoxMutation = new EditBoxMutation({ input, office, box });
    this.setState({ pending: true }, () => {
      relay.commitUpdate(editBoxMutation, { onFailure: this.onFailure, onSuccess: this.onSuccessActiveSave(box) });
    });
  };

  startEditBox = box => () => {
    const { editBox } = this.state;

    this.setState({
      editBox: [...editBox, box.id],
    });
  };

  cancelChanges = box => () => {
    const { editBoxValues } = this.state;
    if (!isUndefined(editBoxValues[box.id])) {
      delete editBoxValues[box.id];
    }

    this.setState({
      editBoxValues: { ...editBoxValues },
    }, this.finishEditBox(box));
  };

  render() {
    const { boxes } = this.props.office;
    const { editBoxValues, editBox, pending } = this.state;

    return (
      <div style={{ position: 'relative' }}>
        <Paper style={{ overflow: 'auto' }}>
          {pending && (
            <div
              style={{
                position: 'absolute',
                width: '100%',
                height: '100%',
                zIndex: 1,
                background: '#00000029',
                alignItems: 'center',
                justifyContent: 'center',
                display: 'flex',
              }}
            >
              <CircularProgress />
            </div>
          )}

          <Table>
            <TableBody>
              {boxes.map(box => (
                <TableRow key={box.id}>
                  <TableCell
                    style={{
                      maxWidth: 300,
                      width: 300,
                      overflow: 'hidden',
                    }}
                  >
                    {box.active && editBox.indexOf(box.id) === -1 && (
                      <Typography
                        style={{ wordWrap: 'break-word' }}
                      >
                        {box.title}
                      </Typography>
                    )}
                    {!box.active && editBox.indexOf(box.id) === -1 && (
                      <Typography
                        style={{
                          textDecoration: 'line-through',
                          wordWrap: 'break-word',
                        }}
                      >
                        {box.title}
                      </Typography>
                    )}
                    {editBox.indexOf(box.id) !== -1 && (
                      <TextField
                        inputProps={{
                          maxLength: 35,
                        }}
                        name="title"
                        placeholder="название бокса"
                        value={get(editBoxValues, `[${box.id}].title`, box.title)}
                        fullWidth
                        onChange={this.onFieldChange(box.id)}
                      />
                    )}

                  </TableCell>
                  <TableCell style={{ whiteSpace: 'nowrap' }}>
                    {box.active && editBox.indexOf(box.id) === -1 && (
                      <Button
                        size="small"
                        title="Редактировать"
                        onClick={this.startEditBox(box)}
                      >
                        <EditIcon />
                      </Button>
                    )}
                    {box.active && editBox.indexOf(box.id) !== -1 && (
                      <Button
                        size="small"
                        title="Сохранить"
                        color="primary"
                        onClick={this.saveChanges(box)}
                      >
                        <SaveIcon />
                      </Button>
                    )}
                    {box.active && editBox.indexOf(box.id) !== -1 && (
                      <Button
                        size="small"
                        title="Отмена"
                        onClick={this.cancelChanges(box)}
                      >
                        <CloseIcon />
                      </Button>
                    )}
                    {box.active && editBox.indexOf(box.id) === -1 && (
                      <Button
                        size="small"
                        title="Удалить"
                        onClick={this.saveBoxActive(box)}
                      >
                        <RemoveIcon />
                      </Button>
                    )}
                    {!box.active && editBox.indexOf(box.id) === -1 && (
                      <Button
                        size="small"
                        title="Восстановить"
                        onClick={this.saveBoxActive(box)}
                      >
                        <RestoreIcon />
                      </Button>
                    )}
                  </TableCell>
                </TableRow>
              ))}
              <TableRow>
                <TableCell>
                  <TextField
                    inputProps={{
                      maxLength: 35,
                    }}
                    name="title"
                    placeholder="название бокса"
                    value={get(editBoxValues, '[new].title', '')}
                    fullWidth
                    onChange={this.onFieldChange('new')}
                  />
                </TableCell>
                <TableCell>
                  <Button size="small" color="primary" variant="contained" onClick={this.createOffice}>
                    <AddIcon /> Добавить бокс
                  </Button>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </Paper>
      </div>
    );
  }
}

export default OfficeBoxes;
