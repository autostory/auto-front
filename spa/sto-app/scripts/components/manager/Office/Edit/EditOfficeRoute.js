import Relay from 'react-relay/classic';

class EditOfficeRoute extends Relay.Route {
  static queries = {
    viewer: () => Relay.QL`query { viewer }`,
  };

  static routeName = 'EditOfficeRoute';
}

export default EditOfficeRoute;
