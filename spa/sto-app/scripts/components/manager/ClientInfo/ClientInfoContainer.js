import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AddCarToClientDialog from '../../../pages/wfm/Dialogs/AddCarToClientDialog';
import EditCarDialog from '../../../pages/wfm/Dialogs/EditCarDialog';

import ClientInfoMainData from './includes/ClientInfoMainData';
import relaySuperContainer from '../../../../../common-code/decorators/relaySuperContainer';
import ClientOrdersTable from './ClientOrdersTable';
import ClientCarInfo from './ClientCarInfo';

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          client (clientId: $clientId) {
              id
              ${ClientOrdersTable.getFragment('client')},
              ${ClientInfoMainData.getFragment('client')},
              ${ClientCarInfo.getFragment('client')},
          }
      }
  `,
})
class ClientInfoContainer extends Component {
  static propTypes = {
    // eslint-disable-next-line react/no-unused-prop-types
    initialVariables: PropTypes.shape({
      clientId: PropTypes.string.isRequired,
    }),
  };

  state = {
    activeTab: 0,
    editCarId: null,
    showCarAddDialog: false,
    showCarEditDialog: false,
  };

  handleTabChange = (event, value) => {
    this.setState({ activeTab: value });
  };

  showCarAddDialog = () => {
    this.setState({
      showCarAddDialog: true,
    });
  };

  hideCarAddDialog = () => {
    this.setState({
      showCarAddDialog: false,
    });
  };

  showCarEditDialog = (carId) => () => {
    this.setState({
      editCarId: carId,
      showCarEditDialog: true,
    });
  };

  hideCarEditDialog = () => {
    this.setState({
      editCarId: null,
      showCarEditDialog: false,
    });
  };

  afterCarAdd = () => {
    this.hideCarAddDialog();
    this.props.relay.forceFetch();
  };

  render() {
    const { client } = this.props.viewer;
    const {
      activeTab, showCarAddDialog, editCarId, showCarEditDialog,
    } = this.state;

    return (
      <div>
        <AddCarToClientDialog
          clientId={client.id}
          afterCarAdd={this.afterCarAdd}
          open={showCarAddDialog}
          onRequestClose={this.hideCarAddDialog}
        />
        {editCarId && (
          <EditCarDialog
            carId={editCarId}
            open={showCarEditDialog}
            onRequestClose={this.hideCarEditDialog}
          />
        )}
        <AppBar position="static">
          <Tabs
            centered
            value={activeTab}
            onChange={this.handleTabChange}
            fullWidth
          >
            <Tab label="Основное" />
            <Tab label="Заказы и заявки" />
            <Tab label="Автомобили" />
          </Tabs>
        </AppBar>
        {activeTab === 0 && (
          <TabContainer>
            <ClientInfoMainData client={client} />
          </TabContainer>
        )}
        {activeTab === 1 && (
          <TabContainer>
            <ClientOrdersTable client={client} />
          </TabContainer>
        )}
        {activeTab === 2 && (
          <TabContainer>
            <Button
              onClick={this.showCarAddDialog}
              color="primary"
              variant="raised"
            >
              Добавить автомобиль
            </Button>
            <ClientCarInfo client={client} onCarEditClick={this.showCarEditDialog} />
          </TabContainer>
        )}
      </div>
    );
  }
}

export default ClientInfoContainer;
