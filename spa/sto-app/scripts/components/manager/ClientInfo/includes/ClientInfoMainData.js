import React, { Component } from 'react';
import Relay from 'react-relay/classic';
import Typography from '@material-ui/core/Typography';

import ConfiguredRadium from '../../../../radium/ConfiguredRadium';
import relayContainer from '../../../../../../common-code/decorators/relayContainer';

@relayContainer({
  client: () => Relay.QL`
      fragment on Client {
          id
          fullName
          points
          category
          email
          phone
      }
  `,
})
@ConfiguredRadium
class ClientInfoMainData extends Component {
  render() {
    const { client } = this.props;

    return (
      <div>
        <Typography><b>ФИО:</b> {client.fullName}</Typography>
        <Typography><b>E-mail:</b> {client.email}</Typography>
        <Typography><b>Телефон:</b> {client.phone}</Typography>
        <Typography><b>Категория:</b> {client.category}</Typography>
        <Typography><b>Баллы:</b> {client.points}</Typography>
      </div>
    );
  }
}

export default ClientInfoMainData;
