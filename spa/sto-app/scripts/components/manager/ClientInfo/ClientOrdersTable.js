import React from 'react';
import map from 'lodash/map';

import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Relay from 'react-relay/classic';
import { Link } from 'react-router-dom';

import CarInfoWithPopover from '../../Car/CarInfoWithPopover';
import { getOrderStatusTypeName } from '../../../../../common-code/constants/orderStatusTypes';
import relayContainer from '../../../../../common-code/decorators/relayContainer';
import { fromGlobalId } from '../../../../../common-code/relay/globalIdUtils';

@relayContainer({
  client: () => Relay.QL`
      fragment on Client {
          orders {
              id
              status
              car {
                  displayName
                  registrationSignNumber
                  vin
                  color
                  year
              }
              makeAt {
                  formattedDateTime
              }
              paymentsSum
              totalBill
              request {
                  id
              }
          }
      }
  `,
})
class ClientOrdersTable extends React.PureComponent {
  render() {
    const clientOrders = this.props.client.orders;

    let orders = [];
    if (clientOrders !== null) {
      orders = clientOrders;
    }

    if (!orders.length) {
      return <Typography>нет заказов</Typography>;
    }

    return (
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>#</TableCell>
              <TableCell>Статус</TableCell>
              <TableCell>Автомобиль</TableCell>
              <TableCell>Дата создания</TableCell>
              <TableCell>Оплаты</TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {orders.map(order => (
              <TableRow key={order.id}>
                <TableCell>{fromGlobalId(order.id).id}</TableCell>
                <TableCell>{getOrderStatusTypeName(order.status)}</TableCell>
                <TableCell><CarInfoWithPopover car={order.car} /></TableCell>
                <TableCell>{order.makeAt.formattedDateTime}</TableCell>
                <TableCell>
                  <span>
                    к оплате {order.totalBill}р.<br />
                    {!order.paymentsSum && order.totalBill > 0 && '(не оплачен)'}
                    {order.paymentsSum > 0 && (
                      <span>
                        <b>оплачено {order.paymentsSum} р.</b>{' '}
                        {order.paymentsSum >= order.totalBill && '(оплачен полностью)'}
                        {order.paymentsSum < order.totalBill && '(оплачен частично)'}
                      </span>
                    )}
                  </span>
                </TableCell>
                <TableCell>
                  <Button
                    size="small"
                    variant="flat"
                    component={Link}
                    to={`/order/${order.id}`}
                    target="_blank"
                  >
                    Открыть
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

export default ClientOrdersTable;
