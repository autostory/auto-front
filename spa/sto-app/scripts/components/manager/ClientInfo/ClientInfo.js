import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ClientInfoContainer from './ClientInfoContainer';

class ClientInfo extends Component {
  static propTypes = {
    clientId: PropTypes.string.isRequired,
  };

  shouldComponentUpdate(nextProps) {
    return nextProps.clientId !== this.props.clientId;
  }

  render() {
    const { clientId } = this.props;

    return <ClientInfoContainer initialVariables={{ clientId }} />;
  }
}

export default ClientInfo;
