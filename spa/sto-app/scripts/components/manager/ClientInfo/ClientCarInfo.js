import PropTypes from 'prop-types';
import React from 'react';
import Paper from '@material-ui/core/Paper';
import DirectionsCar from '@material-ui/icons/DirectionsCar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Relay from 'react-relay/classic';
import map from 'lodash/map';
import get from 'lodash/get';
import { Link } from 'react-router-dom';

import CarInfoMainData from '../CarInfo/includes/CarInfoMainData';
import relayContainer from '../../../../../common-code/decorators/relayContainer';

const style = {
  icon: {
    minWidth: 80,
    maxWidth: 40,
    height: 'auto',
    marginRight: 15,
  },
  header: {
    fontWeight: 'bold',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    height: 60,
  },
  carInfoContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  carCard: {
    padding: 15,
    marginBottom: 25,
    marginRight: 25,
  },
  actions: {
    marginLeft: 25,
  },
};

@relayContainer({
  client: () => Relay.QL`
      fragment on Client {
          cars {
              id
              carList {
                  edges {
                      node {
                          id
                          displayName
                          ${CarInfoMainData.getFragment('car')},
                          registrationSignNumber
                      }
                  }
              }
          }
      }
  `,
})
class ClientCarInfo extends React.Component {
  static defaultProps = {
    onCarEditClick: () => {},
  };

  static propTypes = {
    client: PropTypes.any,
    onCarEditClick: PropTypes.func,
  };

  render() {
    const { onCarEditClick, client } = this.props;

    const cars = map(get(client, 'cars.carList.edges'), 'node');

    if (!cars.length) {
      return <Typography>У клиента нет машин</Typography>;
    }

    return (
      <div style={style.carInfoContainer}>
        {cars.map(car => (
          <Paper key={car.id} style={style.carCard}>
            <div style={style.header}>
              <DirectionsCar style={style.icon} />
              <div>
                {car.displayName} <br />
                Регистрационный номер:{' '}
                {get(car, 'registrationSignNumber')}
                {!get(car, 'registrationSignNumber') && <i>не указан</i>}
              </div>
              <Typography>
                <Button style={style.actions} variant="outlined" onClick={onCarEditClick(car.id)}>
                  Редактировать
                </Button>
                <Button style={style.actions} variant="outlined" target="_blank" component={Link} to={`/car/${car.id}`}>
                  Карточка авто
                </Button>
              </Typography>
            </div>
            <CarInfoMainData car={car} />
          </Paper>
        ))}
      </div>
    );
  }
}

export default ClientCarInfo;