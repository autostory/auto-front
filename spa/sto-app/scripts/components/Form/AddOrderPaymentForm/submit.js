import AddOrderPaymentMutation from '../../../relay/mutation/payment/AddOrderPaymentMutation';
import commitMutationPromise from '../../../../../common-code/relay/commitMutationPromise';

function submit(order, values, afterSave) {
  return commitMutationPromise(AddOrderPaymentMutation, { order, input: values })
    .then(afterSave);
}

export default submit;
