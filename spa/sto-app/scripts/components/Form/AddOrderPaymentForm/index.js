import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Select, TextField } from 'redux-form-material-ui';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import VMasker from 'vanilla-masker';
import isNaN from 'lodash/isNaN';

import validate from './validate';
import { getPaymentTypes, getPaymentTypeName, CASH } from '../../../../../common-code/constants/paymentTypes';

export const FORM_NAME = 'AddOrderPaymentForm';

const paymentsTypes = getPaymentTypes();

const rawStyle = {
  select: {
    minWidth: 400,
  },
  container: {
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'flex-start',
  },
};

const normalizeCurrency = (value) => {
  if (!value) {
    return '';
  }

  const number = parseFloat(value);

  if (isNaN(number)) {
    return '';
  }

  const abs = Math.abs(number);

  if (isNaN(abs)) {
    return '';
  }

  return abs;
};

const normalizeFloatCurrency = (value) => {
  const normalizeValue = normalizeCurrency(value);

  if (value && value[value.length - 1] === '.' && value.indexOf('.') === value.length - 1) {
    return `${normalizeValue}.`;
  }

  return normalizeValue;
};

class AddOrderPaymentForm extends React.PureComponent {
  static propTypes = {
    availablePoints: PropTypes.number.isRequired,
    totalBill: PropTypes.number.isRequired,
    paymentsSum: PropTypes.number.isRequired,
  };

  constructor(props) {
    super(props);

    let debt = props.totalBill - props.paymentsSum;

    if (props.paymentsSum >= props.totalBill) {
      // Учитываем случай если все таки заплатили больше
      debt = 0;
    }

    props.change('debt', parseFloat(debt.toFixed(2)));
    props.change('availablePoints', props.availablePoints);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.availablePoints !== this.props.availablePoints) {
      this.props.change('availablePoints', this.props.availablePoints);
    }
  }

  render() {
    const { submitting, availablePoints, totalBill, paymentsSum } = this.props;
    let debt = totalBill - paymentsSum;
    if (debt < 0) {
      debt = 0;
    }

    return (
      <div style={rawStyle.container}>
        <Typography>Счет: {totalBill} р.</Typography>
        <Typography>Всего оплачено: {paymentsSum} р.</Typography>
        {paymentsSum > 0 && (debt > 0) && (
          <Typography>Долг: {debt} р.</Typography>
        )}
        {availablePoints === 0 && <Typography>У клиента нет баллов для оплаты.</Typography>}
        {availablePoints > 0 && <Typography>Клиенту доступно <b>{availablePoints}</b> баллов</Typography>}
        <br />
        <FormControl required>
          <InputLabel>Тип оплаты</InputLabel>
          <Field
            name="paymentType"
            component={Select}
            style={rawStyle.select}
            autoFocus
            disabled={submitting}
          >
            {paymentsTypes.map(paymentType => (
              <MenuItem key={paymentType} value={paymentType}>
                {getPaymentTypeName(paymentType)}
              </MenuItem>
            ))}
          </Field>
        </FormControl>
        <FormControl required>
          <Field
            inputProps={{
              maxLength: 250,
            }}
            disabled={submitting}
            component={TextField}
            required
            name="amount"
            label="Сумма"
            fullWidth
            normalize={normalizeFloatCurrency}
            margin="normal"
          />
        </FormControl>
        <FormControl>
          <Field
            inputProps={{
              maxLength: 250,
            }}
            disabled={submitting}
            component={TextField}
            name="note"
            label="Заметка"
            multiline
            rows={2}
            fullWidth
            margin="normal"
          />
        </FormControl>
      </div>
    );
  }
}

export default reduxForm({
  form: FORM_NAME,
  validate,
  initialValues: {
    paymentType: CASH,
    amount: '',
    note: '',
  },
  enableReinitialize: true,
})(AddOrderPaymentForm);
