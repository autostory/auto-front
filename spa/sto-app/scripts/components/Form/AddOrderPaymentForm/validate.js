import isFinite from 'lodash/isFinite';
import { POINT } from '../../../../../common-code/constants/paymentTypes';

function validate(values) {
  const errors = {};
  if (!values.paymentType) {
    errors.paymentType = 'Укажите тип оплаты';
  }

  if (!values.amount) {
    errors.amount = 'Укажите сумму';
  } else if (!isFinite(values.amount)) {
    errors.amount = 'Укажите сумму';
  } else if (values.amount < 0) {
    errors.amount = 'Укажите положительную сумму';
  } else if (values.amount > values.debt) {
    errors.amount = `Нельзя внести оплату больше суммы долга ${values.debt} руб.`;
  }

  if (values.paymentType === POINT && values.availablePoints > 0 && values.availablePoints < values.amount) {
    errors.amount = `Нельзя использовать больше ${values.availablePoints} баллов.`;
  }

  if (values.paymentType === POINT && values.availablePoints === 0) {
    errors.amount = 'У клиента нет баллов, оплата баллами невозможна';
  }

  return errors;
}

export default validate;
