import isArray from 'lodash/isArray';
import React from 'react';
import { Field } from 'redux-form';
import { TextField } from 'redux-form-material-ui';
import MobilePhoneFormInput from '../Client/MobilePhoneFormInput';

const phoneNormalize = (value) => {
  const numb = value.match(/\d/g) || [];
  if (!numb || !isArray(numb)) {

    return numb;
  }

  return numb.join('');
};

class ManagerForm extends React.PureComponent {
  render() {
    const { disabled } = this.props;

    return (
      <div>
        <Field
          required
          inputProps={{
            maxLength: 250,
          }}
          fullWidth
          disabled={disabled}
          component={TextField}
          name="surname"
          label="Фамилия"
        />
        <Field
          required
          fullWidth
          inputProps={{
            maxLength: 250,
          }}
          disabled={disabled}
          component={TextField}
          name="firstName"
          label="Имя"
        />
        <Field
          inputProps={{
            maxLength: 250,
          }}
          fullWidth
          disabled={disabled}
          component={TextField}
          name="patronymic"
          label="Отчество"
        />
        <Field
          inputProps={{
            maxLength: 250,
          }}
          fullWidth
          disabled={disabled}
          component={TextField}
          name="email"
          label="E-mail"
        />
        <Field
          inputProps={{
            maxLength: 250,
          }}
          required
          fullWidth
          disabled={disabled}
          component={MobilePhoneFormInput}
          name="phone"
          label="Номер телефона"
          normalize={phoneNormalize}
        />
      </div>
    );
  }
}

export default ManagerForm;
