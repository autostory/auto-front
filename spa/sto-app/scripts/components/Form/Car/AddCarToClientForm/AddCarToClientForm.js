import React, { Component } from 'react';
import { connect } from 'react-redux';
import withProps from 'recompose/withProps';
import Paper from '@material-ui/core/Paper';

import CircularProgress from '@material-ui/core/CircularProgress';
import { formValueSelector, isSubmitting, reduxForm } from 'redux-form';
import validate from './validate';
import CarForm from '../CarForm';

export const FORM_NAME = 'AddCarToClientForm';

const selector = formValueSelector(FORM_NAME);

const CAR_TYPE_ID = 'Q2F0YWxvZ0NhclR5cGU6MQ=='; // легковое авто

const CarReduxForm = reduxForm({
  form: FORM_NAME,
  initialValues: {
    catalogCarTypeId: CAR_TYPE_ID,
  },
  validate,
})(CarForm);

const CarReduxFormWithValues = connect(state => ({
  catalogCarTypeId: selector(state, 'catalogCarTypeId'),
  catalogCarMarkId: selector(state, 'catalogCarMarkId'),
  catalogCarModelId: selector(state, 'catalogCarModelId'),
  catalogCarGenerationId: selector(state, 'catalogCarGenerationId'),
  catalogCarSerieId: selector(state, 'catalogCarSerieId'),
  catalogCarModificationId: selector(state, 'catalogCarModificationId'),
  catalogCarEquipmentId: selector(state, 'catalogCarEquipmentId'),
  disabled: isSubmitting(FORM_NAME)(state),
}))(CarReduxForm);

@withProps({ renderLoading: () => <CircularProgress /> })
class AddCarToClientForm extends Component {
  render() {
    const { onSubmit } = this.props;

    return (
      <div>
        <Paper style={{ padding: 24 }}>
          <CarReduxFormWithValues
            onSubmit={onSubmit}
          />
        </Paper>
      </div>
    );
  }
}

export default AddCarToClientForm;

