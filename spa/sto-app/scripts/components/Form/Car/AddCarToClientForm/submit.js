import { SubmissionError } from 'redux-form';
import AddCarToClientMutation from '../../../../relay/mutation/car/AddCarToClientMutation';
import parseMutationInputValidationErrorMessageForForm
  from '../../../../../../common-code/utils/parseMutationInputValidationErrorMessageForForm';
import commitMutationPromise from '../../../../../../common-code/relay/commitMutationPromise';

function submit(viewer, values) {
  const onFailure = (response) => {
    console.log('onFailure errors', response);
    if (response && response instanceof SubmissionError) {
      throw response;
    }

    alert('При добавлении автомобиля клиенту произошла ошибка. Обновите страницу и/или повторите попытку.');

    return { success: false, response };
  };

  const onSuccess = (response) => {
    if (response.addCarToClient.errors.messages.length > 0) {
      const errors = response.addCarToClient.errors.messages;
      const parsedErrors = parseMutationInputValidationErrorMessageForForm(errors);

      throw new SubmissionError(parsedErrors);
    }

    return { success: true, response };
  };

  return commitMutationPromise(AddCarToClientMutation, { viewer, input: values })
    .then(onSuccess)
    .catch(onFailure);
}

export default submit;
