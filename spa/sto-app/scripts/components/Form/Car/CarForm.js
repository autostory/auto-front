import PropTypes from 'prop-types';
import React from 'react';
import { TextField } from 'redux-form-material-ui';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import { Field } from 'redux-form';
import ru from 'convert-layout/ru';

import CatalogCarSelector from './CatalogCarSelector/CatalogCarSelector';

const rawStyle = {
  root: {
    margin: 5,
    display: 'flex',
    flexDirection: 'column',
    width: 400,
  },
  vinAndNumber: {
    display: 'flex',
    flexDirection: 'row',
    width: 400,
  },
  or: {
    display: 'inline-flex',
    alignItems: 'center',
    padding: '0 10px',
    fontStyle: 'italic',
    whiteSpace: 'nowrap',
  },
};

const inputProps = {
  year: {
    maxLength: 4,
  },
  mileage: {
    maxLength: 50,
  },
  vin: {
    maxLength: 250,
  },
  registrationSignNumber: {
    maxLength: 250,
  },
  color: {
    maxLength: 50,
  }
};

const registrationSignNumberNormalize = (value) => {
  if (!value) {
    return value;
  }

  let string = ru.toEn(value);
  string = string.replace(/[^a-z0-9 ]/ig, '');

  return string.toUpperCase();
};

const vinNormalize = (value) => {
  if (!value) {
    return value;
  }

  let string = ru.toEn(value);
  string = string.replace(/[^a-z0-9 ]/ig, '');

  return string.toUpperCase();
};

const yearNormalize = (value) => {
  if (!value) {
    return value;
  }

  if (value.length > 4) {
    return value.substring(0, 4);
  }

  return value;
};

const onYearKeyPress = (e) => {
  const regex = new RegExp('^[0-9]+$');
  let keyCode = e.charCode;
  if (!e.charCode) {
    keyCode = e.which;
  }
  const str = String.fromCharCode(keyCode);

  if (regex.test(str)) {
    return true;
  }

  e.preventDefault();

  return false;
};

const onMillageKeyPress = (e) => {
  const regex = new RegExp('^[0-9]+$');
  let keyCode = e.charCode;
  if (!e.charCode) {
    keyCode = e.which;
  }
  const str = String.fromCharCode(keyCode);

  if (regex.test(str)) {
    return true;
  }

  e.preventDefault();

  return false;
};

class CarForm extends React.PureComponent {
  static defaultProps = {
    catalogCarEquipmentId: null,
    catalogCarGenerationId: null,
    catalogCarMarkId: null,
    catalogCarModelId: null,
    catalogCarModificationId: null,
    catalogCarSerieId: null,
    catalogCarTypeId: null,
    disabled: false,
    editForm: false,
  };

  static propTypes = {
    catalogCarEquipmentId: PropTypes.string,
    catalogCarGenerationId: PropTypes.string,
    catalogCarMarkId: PropTypes.string,
    catalogCarModelId: PropTypes.string,
    catalogCarModificationId: PropTypes.string,
    catalogCarSerieId: PropTypes.string,
    catalogCarTypeId: PropTypes.string,
    change: PropTypes.func.isRequired,
    editForm: PropTypes.bool,
    disabled: PropTypes.bool,
    initialized: PropTypes.bool.isRequired,
  };

  render() {
    const {
      change, editForm,
      dispatch, disabled, initialized,
      catalogCarTypeId,
      catalogCarMarkId,
      catalogCarModelId,
      catalogCarGenerationId,
      catalogCarSerieId,
      catalogCarModificationId,
      catalogCarEquipmentId,
    } = this.props;

    return (
      <div>
        <div style={rawStyle.root}>
          <FormControl>
            <Field
              disabled={disabled}
              inputProps={inputProps.year}
              component={TextField}
              name="year"
              onKeyPress={onYearKeyPress}
              normalize={yearNormalize}
              label="Год выпуска"
            />
          </FormControl>
          {initialized && (
            <CatalogCarSelector
              disabled={disabled}
              change={change}
              catalogCarTypeId={catalogCarTypeId}
              catalogCarMarkId={catalogCarMarkId}
              catalogCarModelId={catalogCarModelId}
              catalogCarGenerationId={catalogCarGenerationId}
              catalogCarSerieId={catalogCarSerieId}
              catalogCarModificationId={catalogCarModificationId}
              catalogCarEquipmentId={catalogCarEquipmentId}
            />
          )}
        </div>
        <br />
        <div style={rawStyle.root}>
          {!editForm && (
            <Field
              inputProps={inputProps.mileage}
              disabled={disabled}
              component={TextField}
              name="mileage"
              onKeyPress={onMillageKeyPress}
              label="Пробег"
            />
          )}
          <div style={rawStyle.vinAndNumber}>
            <Field
              inputProps={inputProps.vin}
              fullWidth
              disabled={disabled}
              component={TextField}
              name="vin"
              label="VIN"
              normalize={vinNormalize}
            />
            <Typography component="div" style={rawStyle.or}>
              и/или
            </Typography>
            <Field
              inputProps={inputProps.registrationSignNumber}
              fullWidth
              disabled={disabled}
              required
              component={TextField}
              normalize={registrationSignNumberNormalize}
              name="registrationSignNumber"
              label="Гос. номер"
            />
          </div>
          <Field
            inputProps={inputProps.color}
            disabled={disabled}
            component={TextField}
            name="color"
            label="Цвет"
          />
        </div>
      </div>
    );
  }
}

export default CarForm;
