const vinRe = new RegExp("^[A-HJ-NPR-Z\\d]{8}[\\dX][A-HJ-NPR-Z\\d]{2}\\d{6}$");

const validate = ({ noCarAdd, vin, year, registrationSignNumber }) => {
  const errors = {};

  if (noCarAdd) {
    return errors;
  }

  // if (vin && !vin.match(vinRe)) {
  //   errors.vin = 'Введен не правильный VIN код.';
  // }

  if (!registrationSignNumber) {
    errors.registrationSignNumber = 'Обязательно для заполнения.';
  }

  const today = new Date();
  if (year && (year < 1885 || year > today.getFullYear())) {
    errors.year = `Год должен быть в пределах от 1885 до ${today.getFullYear()}`;
  }

  return errors;
};

export default validate;
