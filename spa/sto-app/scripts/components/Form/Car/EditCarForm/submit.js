import { SubmissionError } from 'redux-form';
import EditCarMutation from '../../../../relay/mutation/car/EditCarMutation';
import parseMutationInputValidationErrorMessageForForm
  from './../../../../../../common-code/utils/parseMutationInputValidationErrorMessageForForm';
import commitMutationPromise from './../../../../../../common-code/relay/commitMutationPromise';

function submit(car, values) {
  const onFailure = (response) => {
    console.log('onFailure errors', response);
    if (response && response instanceof SubmissionError) {
      throw response;
    }

    alert('При редактировании автомобиля произошла ошибка. Обновите страницу и/или повторите попытку.');

    return { success: false, response };
  };

  const onSuccess = (response) => {
    if (response.editCarMutation.errors.messages.length > 0) {
      const errors = response.editCarMutation.errors.messages;
      const parsedErrors = parseMutationInputValidationErrorMessageForForm(errors);

      throw new SubmissionError(parsedErrors);
    }

    return { success: true, response };
  };

  return commitMutationPromise(EditCarMutation, { car, input: values })
    .then(onSuccess)
    .catch(onFailure);
}

export default submit;
