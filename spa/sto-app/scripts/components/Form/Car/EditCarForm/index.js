import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import withProps from 'recompose/withProps';
import Paper from '@material-ui/core/Paper';
import Relay from 'react-relay/classic';
import get from 'lodash/get';

import CircularProgress from '@material-ui/core/CircularProgress';
import { formValueSelector, isSubmitting, reduxForm } from 'redux-form';
import validate from './validate';
import CarForm from '../CarForm';

import EditCarMutation from '../../../../relay/mutation/car/EditCarMutation';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';

export const FORM_NAME = 'EditCarForm';
const selector = formValueSelector(FORM_NAME);

const style = {
  paper: {
    padding: 24,
  },
};

const CarReduxForm = reduxForm({
  form: FORM_NAME,
  validate,
})(CarForm);

const CarReduxFormWithValues = connect(state => ({
  catalogCarTypeId: selector(state, 'catalogCarTypeId'),
  catalogCarMarkId: selector(state, 'catalogCarMarkId'),
  catalogCarModelId: selector(state, 'catalogCarModelId'),
  catalogCarGenerationId: selector(state, 'catalogCarGenerationId'),
  catalogCarSerieId: selector(state, 'catalogCarSerieId'),
  catalogCarModificationId: selector(state, 'catalogCarModificationId'),
  catalogCarEquipmentId: selector(state, 'catalogCarEquipmentId'),
  disabled: isSubmitting(FORM_NAME)(state),
}))(CarReduxForm);

const getCarToForm = car => ({
  vin: car.vin,
  registrationSignNumber: car.registrationSignNumber,
  color: car.color,
  year: car.year,
  catalogCarTypeId: get(car, 'catalogCarType.id', null),
  catalogCarMarkId: get(car, 'catalogCarMark.id', null),
  catalogCarModelId: get(car, 'catalogCarModel.id', null),
  catalogCarGenerationId: get(car, 'catalogCarGeneration.id', null),
  catalogCarSerieId: get(car, 'catalogCarSerie.id', null),
  catalogCarModificationId: get(car, 'catalogCarModification.id', null),
  catalogCarEquipmentId: get(car, 'catalogCarEquipment.id', null),
});

@withProps({
  renderLoading: () => <CircularProgress />,
  forceFetch: true,
})
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          car(carId: $carId) {
              ${EditCarMutation.getFragment('car')}
              vin
              registrationSignNumber
              color
              year
              catalogCarType {
                  id
              }
              catalogCarMark {
                  id
              }
              catalogCarModel {
                  id
              }
              catalogCarGeneration {
                  id
              }
              catalogCarSerie {
                  id
              }
              catalogCarModification {
                  id
              }
              catalogCarEquipment {
                  id
              }
          }
      }
  `,
})
class EditCarForm extends PureComponent {
  render() {
    const { viewer, onSubmit } = this.props;
    const { car } = viewer;

    const initialValues = getCarToForm(car);

    return (
      <div>
        <Paper style={style.paper}>
          <CarReduxFormWithValues
            editForm
            onSubmit={onSubmit(car)}
            initialValues={initialValues}
          />
        </Paper>
      </div>
    );
  }
}

export default EditCarForm;

