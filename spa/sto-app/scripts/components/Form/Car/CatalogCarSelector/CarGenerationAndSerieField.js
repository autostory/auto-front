import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';
import { Select } from 'redux-form-material-ui';
import FormControl from '@material-ui/core/FormControl';
import { Field } from 'redux-form';
import MenuItem from '@material-ui/core/MenuItem';
import find from 'lodash/find';
import get from 'lodash/get';
import CircularProgress from '@material-ui/core/CircularProgress';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';

const styles = {
  formControl: {
    position: 'relative',
    marginTop: 12,
  },
  selectLoading: {
    position: 'absolute',
    top: 20,
    left: 180,
  },
  formControlDisabled: {
    cursor: 'not-allowed',
    opacity: 0.7,
  },
};

@relaySuperContainer(
  {
    viewer: () => Relay.QL`
        fragment on Viewer {
            catalogCarGenerations(carModelId: $catalogCarModelId) @include(if: $getGenerations) {
                id
                name
                yearBegin
                yearEnd
                catalogCarSeries {
                    id
                    name
                }
            }
        }
    `,
  },
  {
    catalogCarModelId: null,
    getGenerations: false,
  },
)
@withStyles(styles)
class CarGenerationAndSerieField extends React.PureComponent {
  static defaultProps = {
    disabled: false,
    loading: false,
  };

  static propTypes = {
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
  };

  render() {
    const { viewer, classes, disabled, loading } = this.props;
    const catalogCarGenerations = get(viewer, 'catalogCarGenerations', null);
    const { relay, catalogCarGenerationId } = this.props;

    const catalogCarModelId = get(relay, 'variables.catalogCarModelId', null);

    return (
      <React.Fragment>
        <FormControl
          className={classnames([classes.formControl, (disabled || !catalogCarModelId) && classes.formControlDisabled])}
          disabled={disabled || !catalogCarModelId}
        >
          <InputLabel>Поколение</InputLabel>
          <Field
            name="catalogCarGenerationId"
            component={Select}
            disabled={!catalogCarModelId || (catalogCarModelId && !catalogCarGenerations) || disabled}
          >
            {catalogCarGenerations && <MenuItem value="">не указано</MenuItem>}
            {catalogCarGenerations && (
              catalogCarGenerations.map(item => (
                <MenuItem key={item.id} value={item.id}>{item.name} {item.yearBegin}-{item.yearEnd}</MenuItem>
              ))
            )}
          </Field>
          {loading && (
            <CircularProgress className={classes.selectLoading} size={20} />
          )}
        </FormControl>
        <FormControl
          className={classnames([
            classes.formControl,
            (disabled || !catalogCarGenerationId) && classes.formControlDisabled,
          ])}
          disabled={disabled || !catalogCarGenerationId}
        >
          <InputLabel>Серия</InputLabel>
          <Field
            name="catalogCarSerieId"
            component={Select}
            disabled={!catalogCarGenerationId || (catalogCarGenerationId && !catalogCarGenerations) || disabled}
          >
            {catalogCarGenerations && <MenuItem value="">не указано</MenuItem>}
            {catalogCarGenerationId && catalogCarGenerations
            && get(find(catalogCarGenerations, { id: catalogCarGenerationId }), 'catalogCarSeries', [])
              .map(item => <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>)}
          </Field>
          {(loading || (catalogCarGenerationId && !catalogCarGenerations)) && (
            <CircularProgress className={classes.selectLoading} size={20} />
          )}
        </FormControl>
      </React.Fragment>
    );
  }
}

export default CarGenerationAndSerieField;
