import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';
import { Select } from 'redux-form-material-ui';
import FormControl from '@material-ui/core/FormControl';
import { Field } from 'redux-form';
import MenuItem from '@material-ui/core/MenuItem';
import get from 'lodash/get';
import CircularProgress from '@material-ui/core/CircularProgress';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';

const styles = {
  formControl: {
    position: 'relative',
    marginTop: 12,
  },
  selectLoading: {
    position: 'absolute',
    top: 20,
    left: 180,
  },
  formControlDisabled: {
    cursor: 'not-allowed',
    opacity: 0.7,
  },
};

@relaySuperContainer(
  {
    viewer: () => Relay.QL`
        fragment on Viewer {
            catalogCarMarks(carTypeId: $catalogCarTypeId) @include(if: $getMarks) {
                id
                name
            }
        }
    `,
  },
  {
    catalogCarTypeId: null,
    getMarks: false,
  },
)
@withStyles(styles)
class CarMarkField extends React.PureComponent {
  static defaultProps = {
    disabled: false,
    loading: false,
  };

  static propTypes = {
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
  };

  render() {
    const { viewer, classes, disabled, loading } = this.props;
    const catalogCarMarks = get(viewer, 'catalogCarMarks', null);

    const { relay } = this.props;

    const catalogCarTypeId = get(relay, 'variables.catalogCarTypeId', null);

    return (
      <FormControl
        className={classnames([classes.formControl, (disabled || !catalogCarTypeId) && classes.formControlDisabled])}
        disabled={disabled || !catalogCarTypeId}
      >
        <InputLabel>Марка</InputLabel>
        <Field
          name="catalogCarMarkId"
          component={Select}
          disabled={(!!catalogCarTypeId && !catalogCarMarks) || disabled}
        >
          {catalogCarMarks && <MenuItem value="">не указано</MenuItem>}
          {catalogCarMarks && (
            catalogCarMarks.map(item => <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>)
          )}
        </Field>
        {loading && (
          <CircularProgress className={classes.selectLoading} size={20} />
        )}
      </FormControl>
    );
  }
}

export default CarMarkField;
