import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';
import { Select } from 'redux-form-material-ui';
import FormControl from '@material-ui/core/FormControl';
import { Field } from 'redux-form';
import MenuItem from '@material-ui/core/MenuItem';
import get from 'lodash/get';
import CircularProgress from '@material-ui/core/CircularProgress';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';

const styles = {
  formControl: {
    position: 'relative',
    marginTop: 12,
  },
  selectLoading: {
    position: 'absolute',
    top: 20,
    left: 180,
  },
  formControlDisabled: {
    cursor: 'not-allowed',
    opacity: 0.7,
  },
};

@relaySuperContainer(
  {
    viewer: () => Relay.QL`
        fragment on Viewer {
            catalogCarModifications(carSerieId: $catalogCarSerieId, carModelId: $catalogCarModelId) @include(if: $getModifications) {
                id
                name
            }
        }
    `,
  },
  {
    getModifications: false,
    catalogCarModelId: null,
    catalogCarSerieId: null,
  },
)
@withStyles(styles)
class CarModificationField extends React.PureComponent {
  static defaultProps = {
    disabled: false,
    loading: false,
  };

  static propTypes = {
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
  };

  render() {
    const { viewer, classes, disabled, loading } = this.props;
    const catalogCarModifications = get(viewer, 'catalogCarModifications', null);

    const { relay } = this.props;

    const getModifications = get(relay, 'variables.getModifications', false);

    return (
      <FormControl
        className={classnames([classes.formControl, (disabled || !getModifications) && classes.formControlDisabled])}
        disabled={disabled || !getModifications}
      >
        <InputLabel>Модификация</InputLabel>
        <Field
          name="catalogCarModificationId"
          component={Select}
          disabled={!getModifications || (getModifications && !catalogCarModifications) || disabled}
        >
          {catalogCarModifications && <MenuItem value="">не указано</MenuItem>}
          {catalogCarModifications && (
            catalogCarModifications.map(item => <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>)
          )}
        </Field>
        {loading && (
          <CircularProgress className={classes.selectLoading} size={20} />
        )}
      </FormControl>
    );
  }
}

export default CarModificationField;
