import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';
import { Select } from 'redux-form-material-ui';
import FormControl from '@material-ui/core/FormControl';
import { Field } from 'redux-form';
import MenuItem from '@material-ui/core/MenuItem';
import get from 'lodash/get';
import CircularProgress from '@material-ui/core/CircularProgress';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';

const styles = {
  formControl: {
    position: 'relative',
    marginTop: 12,
  },
  selectLoading: {
    position: 'absolute',
    top: 20,
    left: 180,
  },
  formControlDisabled: {
    cursor: 'not-allowed',
    opacity: 0.7,
  },
};

@relaySuperContainer(
  {
    viewer: () => Relay.QL`
        fragment on Viewer {
            catalogCarModels(carMarkId: $catalogCarMarkId) @include(if: $getMarkModels) {
                id
                name
            }
        }
    `,
  },
  {
    catalogCarMarkId: null,
    getMarkModels: false,
  },
)
@withStyles(styles)
class CarModelField extends React.PureComponent {
  static defaultProps = {
    disabled: false,
    loading: false,
  };

  static propTypes = {
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
  };

  render() {
    const { viewer, classes, disabled, loading } = this.props;
    const catalogCarModels = get(viewer, 'catalogCarModels', null);

    const { relay } = this.props;

    const catalogCarMarkId = get(relay, 'variables.catalogCarMarkId', null);

    return (
      <FormControl
        className={classnames([classes.formControl, (disabled || !catalogCarMarkId) && classes.formControlDisabled])}
        disabled={disabled || !catalogCarMarkId}
      >
        <InputLabel>Модель</InputLabel>
        <Field
          name="catalogCarModelId"
          component={Select}
          disabled={!catalogCarMarkId || (catalogCarMarkId && !catalogCarModels) || disabled}
        >
          {catalogCarModels && <MenuItem value="">не указано</MenuItem>}
          {catalogCarModels && (
            catalogCarModels.map(item => <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>)
          )}
        </Field>
        {loading && (
          <CircularProgress className={classes.selectLoading} size={20} />
        )}
      </FormControl>
    );
  }
}

export default CarModelField;
