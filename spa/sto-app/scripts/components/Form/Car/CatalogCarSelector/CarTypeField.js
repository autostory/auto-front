import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import classnames from 'classnames';
import Relay from 'react-relay/classic';
import { Select } from 'redux-form-material-ui';
import FormControl from '@material-ui/core/FormControl';
import get from 'lodash/get';
import { Field } from 'redux-form';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';

const styles = {
  formControl: {
    position: 'relative',
    marginTop: 12,
  },
  selectLoading: {
    position: 'absolute',
    top: 20,
    left: 180,
  },
  formControlDisabled: {
    cursor: 'not-allowed',
    opacity: 0.7,
  },
};

@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          catalogCarTypes {
              id
              name
          }
      }
  `,
})
@withStyles(styles)
class CarTypeField extends React.PureComponent {
  static defaultProps = {
    disabled: false,
    loading: false,
  };

  static propTypes = {
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
  };

  render() {
    const {
      viewer, classes, loading, disabled,
    } = this.props;
    const catalogCarTypes = get(viewer, 'catalogCarTypes', null);

    return (
      <FormControl
        className={classnames([classes.formControl, disabled && classes.formControlDisabled])}
        disabled={disabled}
      >
        <InputLabel>Тип транспорта</InputLabel>
        <Field
          name="catalogCarTypeId"
          component={Select}
          disabled={disabled}
        >
          {catalogCarTypes && (
            catalogCarTypes.map(item => <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>)
          )}
        </Field>
        {loading && (
          <CircularProgress className={classes.selectLoading} size={20} />
        )}
      </FormControl>
    );
  }
}

export default CarTypeField;
