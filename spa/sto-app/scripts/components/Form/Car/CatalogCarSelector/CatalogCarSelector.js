import PropTypes from 'prop-types';
import React from 'react';
import isEmpty from 'lodash/isEmpty';
import { withStyles } from '@material-ui/core/styles';
import CarEquipmentField from './CarEquipmentField';
import CarGenerationAndSerieField from './CarGenerationAndSerieField';
import CarModelField from './CarModelField';
import CarModificationField from './CarModificationField';
import CarTypeField from './CarTypeField';
import CarMarkField from './CarMarkField';

const styles = {
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: 400,
  },
};

@withStyles(styles)
class CatalogCarSelector extends React.PureComponent {
  static defaultProps = {
    disabled: false,
  };

  static propTypes = {
    change: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
  };

  componentDidUpdate = (prevProps) => {
    this.updateFormData(prevProps, this.props);
  };

  updateFormData = (prevProps, props) => {
    const formData = {
      catalogCarMarkId: props.catalogCarMarkId,
      catalogCarModelId: props.catalogCarModelId,
      catalogCarGenerationId: props.catalogCarGenerationId,
      catalogCarSerieId: props.catalogCarSerieId,
      catalogCarModificationId: props.catalogCarModificationId,
      catalogCarEquipmentId: props.catalogCarEquipmentId,
    };

    if (
      !isEmpty(props.catalogCarTypeId)
      && prevProps.catalogCarTypeId !== props.catalogCarTypeId
    ) {
      formData.catalogCarMarkId = null;
      formData.catalogCarModelId = null;
      formData.catalogCarGenerationId = null;
      formData.catalogCarSerieId = null;
      formData.catalogCarModificationId = null;
      formData.catalogCarEquipmentId = null;
    }

    if (prevProps.catalogCarMarkId !== props.catalogCarMarkId) {
      formData.catalogCarModelId = null;
      formData.catalogCarGenerationId = null;
      formData.catalogCarSerieId = null;
      formData.catalogCarModificationId = null;
      formData.catalogCarEquipmentId = null;
    }

    if (prevProps.catalogCarModelId !== props.catalogCarModelId) {
      formData.catalogCarGenerationId = null;
      formData.catalogCarSerieId = null;
      formData.catalogCarModificationId = null;
      formData.catalogCarEquipmentId = null;
    }

    if (prevProps.catalogCarGenerationId !== props.catalogCarGenerationId) {
      formData.catalogCarSerieId = null;
      formData.catalogCarModificationId = null;
      formData.catalogCarEquipmentId = null;
    }

    if (prevProps.catalogCarSerieId !== props.catalogCarSerieId) {
      formData.catalogCarModificationId = null;
      formData.catalogCarEquipmentId = null;
    }

    if (prevProps.catalogCarModificationId !== props.catalogCarModificationId) {
      formData.catalogCarEquipmentId = null;
    }

    const { change } = this.props;

    change('catalogCarMarkId', formData.catalogCarMarkId);
    change('catalogCarModelId', formData.catalogCarModelId);
    change('catalogCarGenerationId', formData.catalogCarGenerationId);
    change('catalogCarSerieId', formData.catalogCarSerieId);
    change('catalogCarModificationId', formData.catalogCarModificationId);
    change('catalogCarEquipmentId', formData.catalogCarEquipmentId);
  };

  render() {
    const { classes, disabled } = this.props;
    const {
      catalogCarTypeId, catalogCarMarkId, catalogCarModelId, catalogCarGenerationId,
      catalogCarSerieId, catalogCarModificationId,
    } = this.props;

    return (
      <div className={classes.root}>
        <CarTypeField
          renderLoading={() => <CarTypeField dummy loading disabled />}
          disabled={disabled}
        />
        <CarMarkField
          renderLoading={() => <CarMarkField dummy loading disabled />}
          disabled={disabled}
          initialVariables={{
            catalogCarTypeId,
            getMarks: !!catalogCarTypeId,
          }}
        />
        <CarModelField
          renderLoading={() => <CarModelField dummy loading disabled />}
          disabled={disabled}
          initialVariables={{
            catalogCarMarkId,
            getMarkModels: !!catalogCarMarkId,
          }}
        />
        <CarGenerationAndSerieField
          renderLoading={() => <CarGenerationAndSerieField dummy loading disabled />}
          disabled={disabled}
          catalogCarGenerationId={catalogCarGenerationId}
          initialVariables={{
            catalogCarModelId,
            getGenerations: !!catalogCarModelId,
          }}
        />
        <CarModificationField
          renderLoading={() => <CarModificationField dummy loading disabled />}
          disabled={disabled}
          initialVariables={{
            catalogCarModelId,
            catalogCarSerieId,
            getModifications: !!(catalogCarModelId || catalogCarSerieId),
          }}
        />
        <CarEquipmentField
          renderLoading={() => <CarEquipmentField dummy loading disabled />}
          disabled={disabled}
          initialVariables={{
            catalogCarModificationId,
            getEquipments: !!catalogCarModificationId,
          }}
        />
      </div>
    );
  }
}

export default CatalogCarSelector;
