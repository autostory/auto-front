import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import Relay from 'react-relay/classic';
import { Select } from 'redux-form-material-ui';
import FormControl from '@material-ui/core/FormControl';
import { Field } from 'redux-form';
import MenuItem from '@material-ui/core/MenuItem';
import get from 'lodash/get';
import CircularProgress from '@material-ui/core/CircularProgress';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';
import relaySuperContainer from '../../../../../../common-code/decorators/relaySuperContainer';

const styles = {
  formControl: {
    position: 'relative',
    marginTop: 12,
  },
  selectLoading: {
    position: 'absolute',
    top: 20,
    left: 180,
  },
  formControlDisabled: {
    cursor: 'not-allowed',
    opacity: 0.7,
  },
};

@relaySuperContainer(
  {
    viewer: () => Relay.QL`
        fragment on Viewer {
            catalogCarEquipments(carModificationId: $catalogCarModificationId) @include(if: $getEquipments) {
                id
                name
                year
            }
        }
    `,
  },
  {
    catalogCarModificationId: null,
    getEquipments: false,
  },
)
@withStyles(styles)
class CarEquipmentField extends React.PureComponent {
  static defaultProps = {
    disabled: false,
    loading: false,
  };

  static propTypes = {
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
  };

  render() {
    const { viewer, classes, disabled, loading } = this.props;
    const catalogCarEquipments = get(viewer, 'catalogCarEquipments', null);

    const { relay } = this.props;

    const catalogCarModificationId = get(relay, 'variables.catalogCarModificationId', null);

    return (
      <FormControl
        className={classnames([
          classes.formControl,
          (disabled || !catalogCarModificationId) && classes.formControlDisabled,
        ])}
        disabled={disabled || !catalogCarModificationId}
      >
        <InputLabel>Комплектация</InputLabel>
        <Field
          name="catalogCarEquipmentId"
          component={Select}
          disabled={!catalogCarModificationId || (catalogCarModificationId && !catalogCarEquipments) || disabled}
        >
          {catalogCarEquipments && <MenuItem>не указано</MenuItem>}
          {catalogCarEquipments && (
            catalogCarEquipments.map(item => <MenuItem key={item.id} value={item.id}>{item.name} {item.year}</MenuItem>)
          )}
        </Field>
        {loading && (
          <CircularProgress className={classes.selectLoading} size={20} />
        )}
      </FormControl>
    );
  }
}

export default CarEquipmentField;
