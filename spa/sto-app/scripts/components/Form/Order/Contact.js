import PropTypes from 'prop-types';
import React from 'react';
import isArray from 'lodash/isArray';
import { withStyles } from '@material-ui/core/styles/index';
import Relay from 'react-relay/classic';
import { Checkbox, TextField } from 'redux-form-material-ui';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Field } from 'redux-form';
import CircularProgress from '@material-ui/core/CircularProgress';

import relayContainer from '../../../../../common-code/decorators/relayContainer';
import MobilePhoneFormInput from '../Client/MobilePhoneFormInput';

const styles = {
  checkBox: {
    width: 250,
  },
  phoneFormGroup: {
    width: 500,
    marginBottom: 20,
  },
  phoneField: {
    marginLeft: 20,
  },
};

const phoneNormalize = (value) => {
  const numb = value.match(/\d/g) || [];

  if (!numb || !isArray(numb)) {
    return numb;
  }

  return numb.join('');
};

@relayContainer({
  client: () => Relay.QL`
      fragment on Client {
          phone
      }
  `,
})
@withStyles(styles)
class Contact extends React.PureComponent {
  static defaultProps = {
    client: undefined,
    classes: {},
    contactPhone: '',
    disabled: false,
    phoneSameAsInProfile: false,
    loading: false,
  };

  static propTypes = {
    classes: PropTypes.object,
    client: PropTypes.object,
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    phoneSameAsInProfile: PropTypes.bool,
  };

  render() {
    const {
      client, classes, phoneSameAsInProfile, disabled, loading,
    } = this.props;

    if (loading) {
      return <CircularProgress />;
    }

    return (
      <FormGroup row={false}>
        {client && client.phone && (
          <FormControlLabel
            className={classes.checkBox}
            control={
              <Field
                color="primary"
                required
                disabled={disabled}
                name="phoneSameAsInProfile"
                component={Checkbox}
              />
            }
            label="Контактное лицо - заказчик"
          />
        )}
        {!phoneSameAsInProfile && (
          <FormGroup row className={classes.phoneFormGroup}>
            <Field
              inputProps={{
                maxLength: 250,
              }}
              component={MobilePhoneFormInput}
              name="contactPhone"
              label="Контактный номер"
              normalize={phoneNormalize}
            />
            <Field
              required
              inputProps={{
                maxLength: 250,
              }}
              disabled={disabled}
              component={TextField}
              className={classes.phoneField}
              name="contactName"
              label="Имя"
            />
          </FormGroup>
        )}
      </FormGroup>
    );
  }
}

export default Contact;
