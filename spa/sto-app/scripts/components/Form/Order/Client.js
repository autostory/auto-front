import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import green from '@material-ui/core/colors/green';
import pink from '@material-ui/core/colors/pink';
import { withStyles } from '@material-ui/core/styles/index';
import get from 'lodash/get';
import isFunction from 'lodash/isFunction';
import isUndefined from 'lodash/isUndefined';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

import ErrorIcon from '@material-ui/icons/Error';
import PersonIcon from '@material-ui/icons/Person';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import PhoneIcon from '@material-ui/icons/Phone';
import CompareIcon from '@material-ui/icons/CompareArrows';
import CloseIcon from '@material-ui/icons/Close';
import Relay from 'react-relay/classic';
import { Link } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';

import relayContainer from '../../../../../common-code/decorators/relayContainer';

import phoneFormat from '../../../../../common-code/utils/text/phoneFormat';

const styles = {
  listItemRoot: {
    padding: 0,
  },
  pinkAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: pink[500],
  },
  greenAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: green[500],
  },
  flexCentered: {
    display: 'flex',
    alignItems: 'center',
    paddingRight: 5,
  },
  addClientButton: {
    marginLeft: 20,
  },
  phoneIcon: {
    display: 'inline-flex',
    alignItems: 'center',
    fontSize: '1rem',
  },
  error: {
    outline: '1px solid red',
  },
};

@relayContainer({
  client: () => Relay.QL`
      fragment on Client {
          id
          fullName
          phone
      }
  `,
})
@withStyles(styles)
class Client extends React.Component {
  static defaultProps = {
    classes: {},
    clearable: true,
    client: undefined,
    disabled: false,
    loading: false,
    onClearClick: () => {},
    withProfileLink: false,
  };

  static propTypes = {
    classes: PropTypes.object,
    disabled: PropTypes.bool,
    clearable: PropTypes.bool,
    client: PropTypes.object,
    loading: PropTypes.bool,
    onClearClick: PropTypes.func,
    onStartCleintSearch: PropTypes.func.isRequired,
    onStartClientAdd: PropTypes.func.isRequired,
    withProfileLink: PropTypes.bool,
  };

  clearSelectedClient = () => {
    if (isFunction(this.props.onClearClick)) {
      this.props.onClearClick();
    }
  };

  showClientSearchDialog = () => {
    if (isFunction(this.props.onStartCleintSearch)) {
      this.props.onStartCleintSearch();
    }
  };

  showClientAddDialog = () => {
    if (isFunction(this.props.onStartClientAdd)) {
      this.props.onStartClientAdd();
    }
  };

  render() {
    const {
      client, classes, clearable, withProfileLink, loading, disabled, error,
    } = this.props;

    if (loading) {
      return <CircularProgress />;
    }

    return (
      <div className={classnames([classes.flexCentered, error && classes.error])}>
        {isUndefined(client) && (
          <div className={classes.flexCentered}>
            <List>
              <ListItem
                classes={{
                  root: classes.listItemRoot,
                }}
              >
                <Avatar className={classes.pinkAvatar}>
                  <ErrorIcon />
                </Avatar>
                <ListItemText primary="Заказчик не указан" />
              </ListItem>
            </List>
            <Button
              variant="contained"
              color="primary"
              onClick={this.showClientSearchDialog}
              disabled={disabled}
            >
              <SearchIcon /> Выбрать клиента
            </Button>
            <Button
              className={classes.addClientButton}
              variant="contained"
              color="primary"
              onClick={this.showClientAddDialog}
              disabled={disabled}
            >
              <AddIcon /> Добавить клиента
            </Button>
          </div>
        )}
        {!isUndefined(client) && (
          <div className={classes.flexCentered}>
            <List>
              <ListItem
                classes={{
                  button: classes.listItemButton,
                  root: classes.listItemRoot,
                }}
              >
                <Avatar className={classes.greenAvatar}>
                  <PersonIcon />
                </Avatar>
                <ListItemText
                  primary={client.fullName}
                  secondary={
                    <span className={classes.phoneIcon}>
                      <PhoneIcon />{phoneFormat(get(client, 'phone', ''))}
                    </span>
                  }
                />
              </ListItem>
            </List>

            <Tooltip title="Выбрать другого заказчика">
              <IconButton onClick={this.showClientSearchDialog} disabled={disabled}>
                <CompareIcon />
              </IconButton>
            </Tooltip>
            {withProfileLink && (
              <Tooltip title="Открыть карточку клиента">
                <IconButton component={Link} to={`/client/${client.id}`} target="_blank" disabled={disabled}>
                  <PersonIcon />
                </IconButton>
              </Tooltip>
            )}
            {clearable && (
              <Tooltip title="Очистить поле">
                <IconButton
                  disabled={disabled}
                  onClick={this.clearSelectedClient}
                >
                  <CloseIcon />
                </IconButton>
              </Tooltip>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default Client;
