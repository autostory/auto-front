import PropTypes from 'prop-types';
import React from 'react';
import classnames from 'classnames';
import green from '@material-ui/core/colors/green';
import pink from '@material-ui/core/colors/pink';
import { withStyles } from '@material-ui/core/styles/index';
import map from 'lodash/map';
import isFunction from 'lodash/isFunction';
import get from 'lodash/get';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import CircularProgress from '@material-ui/core/CircularProgress';

import CarIcon from '@material-ui/icons/DirectionsCar';
import ErrorIcon from '@material-ui/icons/Error';
import AddIcon from '@material-ui/icons/Add';
import CompareIcon from '@material-ui/icons/CompareArrows';
import Relay from 'react-relay/classic';
import { Link } from 'react-router-dom';

import relayContainer from '../../../../../common-code/decorators/relayContainer';

const styles = {
  listItemRoot: {
    padding: 0,
  },
  pinkAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: pink[500],
  },
  greenAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: green[500],
  },
  flexCentered: {
    display: 'flex',
    alignItems: 'center',
  },
  noClientSelected: {
    marginLeft: 0,
    display: 'flex',
    alignItems: 'center',
    fontSize: '1rem',
  },
  noClientSelectedText: {
    marginLeft: 17,
  },
  error: {
    outline: '1px solid red',
  },
};

@relayContainer({
  client: () => Relay.QL`
      fragment on Client {
          id
          fullName
          phone
          cars {
              carList {
                  edges {
                      node {
                          id
                          displayName
                          registrationSignNumber
                      }
                  }
              }
          }
      }
  `,
  car: () => Relay.QL`
      fragment on Car {
          id
          displayName
          registrationSignNumber
      }
  `,
})
@withStyles(styles)
class Car extends React.Component {
  static defaultProps = {
    car: null,
    classes: {},
    error: false,
    client: null,
    loading: false,
    disabled: false,
    withProfileLink: false,
  };

  static propTypes = {
    car: PropTypes.object,
    classes: PropTypes.object,
    client: PropTypes.object,
    loading: PropTypes.bool,
    error: PropTypes.bool,
    withProfileLink: PropTypes.bool,
    disabled: PropTypes.bool,
    onCarAddClick: PropTypes.func.isRequired,
    onCarSelectClick: PropTypes.func.isRequired,
  };

  showAddClientCarsDialog = () => {
    if (isFunction(this.props.onCarAddClick)) {
      this.props.onCarAddClick();
    }
  };

  showSelectClientCarsDialog = () => {
    if (isFunction(this.props.onCarSelectClick)) {
      this.props.onCarSelectClick();
    }
  };

  render() {
    const {
      client, classes, car: selectedCar, loading, disabled, error, withProfileLink,
    } = this.props;

    const clientCars = map(get(client, 'cars.carList.edges'), 'node');

    if (loading) {
      return <CircularProgress />;
    }

    return (
      <div className={classnames([classes.flexCentered, error && classes.error])}>
        {!client && (
          <div className={classes.noClientSelected}>
            <Avatar className={classes.pinkAvatar}>
              <ErrorIcon />
            </Avatar>
            <div className={classes.noClientSelectedText}>Сначала необходимо указать заказчика</div>
          </div>
        )}
        {client && clientCars && !selectedCar && (
          <div className={classes.noClientSelected}>
            <Avatar className={classes.pinkAvatar}>
              <ErrorIcon />
            </Avatar>
            <div className={classes.noClientSelectedText}>Выберете автомобиль клиента</div>
            <Tooltip title="Выбрать автомобиль клиента">
              <IconButton onClick={this.showSelectClientCarsDialog} disabled={disabled}>
                <CompareIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Добавить автомобиль клиенту">
              <IconButton onClick={this.showAddClientCarsDialog} disabled={disabled}>
                <AddIcon />
              </IconButton>
            </Tooltip>
          </div>
        )}
        {client && clientCars && selectedCar && (
          <div className={classes.flexCentered}>
            <List>
              <ListItem
                classes={{
                  button: classes.listItemButton,
                  root: classes.listItemRoot,
                }}
              >
                <Avatar className={classes.greenAvatar}>
                  <CarIcon />
                </Avatar>
                <ListItemText
                  primary={selectedCar.displayName}
                  secondary={selectedCar.registrationSignNumber}
                />
              </ListItem>
            </List>
            <Tooltip title="Выбрать другой автомобиль">
              <IconButton onClick={this.showSelectClientCarsDialog} disabled={disabled}>
                <CompareIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Добавить автомобиль клиенту">
              <IconButton onClick={this.showAddClientCarsDialog} disabled={disabled}>
                <AddIcon />
              </IconButton>
            </Tooltip>
            {withProfileLink && (
              <Tooltip title="Открыть карточку автомобиля">
                <IconButton component={Link} to={`/car/${selectedCar.id}`} target="_blank" disabled={disabled}>
                  <CarIcon />
                </IconButton>
              </Tooltip>
            )}
          </div>
        )}
        {client && !clientCars && (
          <div className={classes.flexCentered}>
            <List>
              <ListItem
                classes={{
                  button: classes.listItemButton,
                  root: classes.listItemRoot,
                }}
              >
                <Avatar className={classes.pinkAvatar}>
                  <ErrorIcon />
                </Avatar>
                <ListItemText
                  primary="У клиента нет автомобилей"
                />
              </ListItem>
            </List>
            <Tooltip title="Добавить автомобиль клиенту">
              <IconButton onClick={this.showAddClientCarsDialog} disabled={disabled}>
                <AddIcon />
              </IconButton>
            </Tooltip>
          </div>
        )}
      </div>
    );
  }
}

export default Car;
