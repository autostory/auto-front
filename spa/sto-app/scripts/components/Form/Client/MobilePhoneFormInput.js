import PropTypes from 'prop-types';
import React from 'react';
import InputAdornment from '@material-ui/core/InputAdornment';
import MaskedTextField from '../../basic/input/MaskedTextField';

class MobilePhoneFormInput extends React.PureComponent {
  static defaultProps = {
    disabled: false,
  };

  static propTypes = {
    disabled: PropTypes.bool,
  };

  render() {
    const { input, label, meta: { touched, error }, disabled } = this.props;

    return (
      <MaskedTextField
        label={label}
        error={touched && !!error}
        helperText={touched && error}
        disabled={disabled}
        value={input.value}
        mask="(999) 999 99 99"
        placeholder="(XXX) XXX XX XX"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              {'+7 '}
            </InputAdornment>
          ),
        }}
        {...input}
      />
    );
  }
}

export default MobilePhoneFormInput;
