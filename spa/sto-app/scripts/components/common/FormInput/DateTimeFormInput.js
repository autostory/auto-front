import React from 'react';
import isNaN from 'lodash/isNaN';
import get from 'lodash/get';
import DateTimePicker from 'material-ui-pickers/DateTimePicker';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import CalendarIcon from '@material-ui/icons/Today';

export const dateToTimestamp = (value) => {
  if (!value) {
    return null;
  }

  if (value instanceof Date) {
    return parseInt(value.getTime() / 1000, 10);
  }

  return value;
};

export const timeStampToDate = (value) => {
  if (!value) {
    return null;
  }

  const date = new Date();
  date.setTime(value * 1000);

  return date;
};

class DateTimeFormInput extends React.PureComponent {
  onDateTimeChange = (time) => {
    const { input: { onChange } } = this.props;

    if (time instanceof Date) {
      time.setSeconds(0);
      time.setMilliseconds(0);
      onChange(time);
    } else {
      onChange(time);
    }
  };

  render() {
    const {
      input, label, meta: { touched, error }, ...rest
    } = this.props;

    let date = input.value;
    if (!input.value) {
      date = null;
    } else if (!(input.value instanceof Date)) {
      date = new Date(input.value);

      if (isNaN(date.getTime())) {
        date = null;
      }
    }

    return (
      <DateTimePicker
        ampm={false}
        label={label}
        onChange={this.onDateTimeChange}
        value={date}
        format="DD.MM.YY HH:mm"
        okLabel="OK"
        cancelLabel="Закрыть"
        clearLabel="Очистить"
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton disabled={get(rest, 'disabled', false)}>
                <CalendarIcon />
              </IconButton>
            </InputAdornment>
          ),
        }}
        {...rest}
      />
    );
  }
}

export default DateTimeFormInput;
