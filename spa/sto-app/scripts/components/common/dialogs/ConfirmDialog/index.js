import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import isFunction from 'lodash/isFunction';

class ConfirmDialog extends React.Component {
  static propTypes = {
    cancelText: PropTypes.string,
    children: PropTypes.any,
    fullScreen: PropTypes.bool.isRequired,
    okText: PropTypes.string,
    onCancel: PropTypes.func,
    onOk: PropTypes.func,
    open: PropTypes.bool.isRequired,
    title: PropTypes.string,
  };

  static defaultProps = {
    cancelText: 'Отмена',
    okText: 'Ok',
    onCancel: () => {},
    onOk: () => {},
    title: '',
  };

  handleOk = () => {
    const { onOk } = this.props;
    if (isFunction(onOk)) {
      onOk();
    }
  };

  handleCancel = () => {
    const { onCancel } = this.props;
    if (isFunction(onCancel)) {
      onCancel();
    }
  };

  render() {
    const {
      open, fullScreen, title, okText, cancelText,
    } = this.props;

    return (
      <div>
        <Dialog
          open={open}
          fullScreen={fullScreen}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
          <DialogContent>
            {this.props.children}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleOk} color="primary">
              {okText}
            </Button>
            <Button onClick={this.handleCancel} color="primary" autoFocus>
              {cancelText}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withMobileDialog()(ConfirmDialog);
