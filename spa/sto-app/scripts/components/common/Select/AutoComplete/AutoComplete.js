import React from 'react';

import PropTypes from 'prop-types';
import AutoSuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import TextField from '@material-ui/core/TextField';
import MuiPortal from '@material-ui/core/Portal';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import CancelIcon from '@material-ui/icons/Cancel';
import ReactDOM from 'react-dom';
import find from 'lodash/find';
import get from 'lodash/get';
import ru from 'convert-layout/ru';

const MAX_RESULTS = 20;

const rawStyle = {
  item: {
    minHeight: 24,
    whiteSpace: 'normal',
    height: 'auto',
  },
  bold: {
    fontWeight: 500,
  },
  normal: {
    fontWeight: 300,
  },
};

function renderSuggestion(suggestion, { query, isHighlighted }) {
  const matches = match(suggestion.label, query);
  const parts = parse(suggestion.label, matches);

  return (
    <MenuItem selected={isHighlighted} component="div" style={rawStyle.item}>
      <div>
        {parts.map((part, index) => (part.highlight ? (
          <span key={String(index)} style={rawStyle.bold}>
            {part.text}
          </span>
        ) : (
          <strong key={String(index)} style={rawStyle.normal}>
            {part.text}
          </strong>
        )))}
      </div>
    </MenuItem>
  );
}

function getSuggestionValue(suggestion) {
  return suggestion.label;
}

const styles = theme => ({
  container: {
    flexGrow: 1,
    position: 'relative',
    height: 'auto',
  },
  suggestionsContainerOpen: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  suggestion: {
    display: 'block',
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none',
  },
  chipRoot: {
    height: 'auto',
  },
  chipLabel: {
    maxWidth: '100%',
    whiteSpace: 'normal',
    paddingTop: 10,
    paddingBottom: 10,
  },
});

const getCoordinates = (elem) => { // crossbrowser version
  const box = elem.getBoundingClientRect();

  const body = document.body;
  const docEl = document.documentElement;

  const scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
  const scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

  const clientTop = docEl.clientTop || body.clientTop || 0;
  const clientLeft = docEl.clientLeft || body.clientLeft || 0;

  const top = box.top + scrollTop - clientTop;
  const left = box.left + scrollLeft - clientLeft;

  return {
    top: Math.round(top),
    left: Math.round(left),
  };
};

@withStyles(styles)
class AutoComplete extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    onSelect: PropTypes.func.isRequired,
    errorMessage: PropTypes.string,
    initialSelectedOptionId: PropTypes.string,
    options: PropTypes.arrayOf(PropTypes.shape({
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      label: PropTypes.string,
    })).isRequired,
  };

  static defaultProps = {
    initialSelectedOptionId: '',
    errorMessage: '',
  };

  constructor(props) {
    super(props);

    this.node = null;
    this.interval = null;

    this.state = {
      value: '',
      selectedOptionId: null,
      suggestions: [],
    };

    if (props.initialSelectedOptionId) {
      this.state.value = get(find(this.props.options, { value: props.initialSelectedOptionId }), 'label', '');
      this.state.selectedOptionId = props.initialSelectedOptionId;
    }
  }

  componentDidMount = () => {
    this.interval = setInterval(() => {
      this.forceUpdate();
    }, 100);
  };

  componentWillUnmount = () => {
    if (this.interval) {
      clearInterval(this.interval);
    }
  };

  onSuggestionSelected = (event, { suggestion }) => {
    this.setState({
      value: suggestion.label,
      selectedOptionId: suggestion.value,
    });

    this.props.onSelect(suggestion.value);
  };

  getSuggestions = (value) => {
    const suggestions = this.props.options;
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;
    let count = 0;

    if (inputLength === 0) {
      return suggestions;
    }

    return suggestions.filter((suggestion) => {
      const keep =
        count < MAX_RESULTS && suggestion.label.toLowerCase().indexOf(inputValue) !== -1;

      if (keep) {
        count += 1;
      }

      return keep;
    });
  };

  handleDelete = () => {
    this.setState({
      selectedOptionId: null,
      value: '',
    }, () => {
      this.props.onSelect(this.state.selectedOptionId);
    });
  };

  handleChange = (event, { newValue }) => {
    this.setState({
      value: ru.fromEn(newValue),
    });
  };

  handleSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value),
    });
  };

  handleSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  renderInput = (inputProps) => {
    const {
      classes, ref, errorMessage, ...other
    } = inputProps;

    if (this.state.selectedOptionId === null) {
      return (
        <TextField
          fullWidth
          helperText={errorMessage}
          error={!!errorMessage}
          InputProps={{
            maxLength: 250,
            inputRef: ref,
            classes: {
              input: classes.input,
            },
            ...other,
          }}
        />
      );
    }

    if (this.state.selectedOptionId !== null) {
      return (
        <Chip
          label={this.state.value}
          onDelete={this.handleDelete}
          classes={{
            root: classes.chipRoot,
            label: classes.chipLabel,
          }}
          deleteIcon={<CancelIcon onTouchEnd={this.handleDelete} />}
        />
      );
    }
  };

  renderSuggestionsContainer = (options) => {
    const { containerProps, children } = options;

    if (this.props.container) {
      let elem = ReactDOM.findDOMNode(this.node);
      if (!elem) {
        return null;
      }

      const { top, left } = getCoordinates(elem);

      const style = {
        width: 300,
        position: 'absolute',
        maxHeight: 500,
        overflow: 'auto',
      };

      style.top = top + 30;
      style.left = left;

      return (
        <MuiPortal container={this.props.container}>
          <Paper
            {...containerProps}
            style={style}
            square
          >
            {children}
          </Paper>
        </MuiPortal>
      );
    }
    return (
      <Paper
        {...containerProps}
        square
      >
        {children}
      </Paper>
    );
  };

  render() {
    const { classes, placeholder, errorMessage } = this.props;
    const { value } = this.state;

    return (
      <AutoSuggest
        theme={{
          container: classes.container,
          suggestionsContainerOpen: classes.suggestionsContainerOpen,
          suggestionsList: classes.suggestionsList,
          suggestion: classes.suggestion,
        }}
        ref={(node) => {
          this.node = node;
        }}
        renderInputComponent={this.renderInput}
        suggestions={this.state.suggestions}
        onSuggestionsFetchRequested={this.handleSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.handleSuggestionsClearRequested}
        renderSuggestionsContainer={this.renderSuggestionsContainer}
        onSuggestionSelected={this.onSuggestionSelected}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        inputProps={{
          classes,
          placeholder,
          value,
          onChange: this.handleChange,
          errorMessage,
        }}
      />
    );
  }
}

AutoComplete.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AutoComplete);
