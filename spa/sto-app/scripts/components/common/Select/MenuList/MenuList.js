import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import MenuListItem from './MenuListItem';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 400,
    backgroundColor: theme.palette.background.paper,
  },
});

class MenuList extends React.PureComponent {
  // static propTypes = {
  //   classes: PropTypes.object.isRequired,
  //   children: PropTypes.oneOfType([
  //     PropTypes.arrayOf(PropTypes.instanceOf(MenuListItem)),
  //     PropTypes.instanceOf(MenuListItem),
  //   ]).isRequired,
  // };

  render() {
    const { classes, children, ...rest } = this.props;

    return (
      <div className={classes.root}>
        <List component="nav" {...rest}>
          {children}
        </List>
      </div>
    );
  }
}

export default withStyles(styles)(MenuList);
