import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import isUndefined from 'lodash/isUndefined';
import find from 'lodash/find';
import uuid from 'uuid/v4';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
});

class MenuListItem extends React.PureComponent {
  static propTypes = {
    options: PropTypes.array,
    title: PropTypes.string,
  };

  static defaultProps = {
    options: [],
    title: undefined,
  };

  constructor(props) {
    super();

    let { initialSelected } = props;
    if (isUndefined(initialSelected)) {
      initialSelected = null;
    }

    this.state = {
      id: `lock-menu${uuid()}`,
      anchorEl: null,
      selectedValue: initialSelected,
    };
  }

  onMenuItemClick = value => (event) => {
    this.handleMenuItemClick(event, value);
  };

  handleClickListItem = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuItemClick = (event, value) => {
    this.setState({
      selectedOptionId: value,
      anchorEl: null,
    });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { options, title } = this.props;
    const { anchorEl, id, selectedOptionId } = this.state;

    return (
      <React.Fragment>
        <ListItem
          button
          divider
          aria-haspopup="true"
          aria-controls={id}
          aria-label={title}
          onClick={this.handleClickListItem}
        >
          {title && (
            <ListItemText
              primary={title}
              secondary={options[selectedValue]}
            />
          )}
          {isUndefined(title) && (
            <ListItemText
              primary={find(options, { value: selectedValue }).title}
            />
          )}
        </ListItem>
        <Menu
          id={id}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {options.map(option => (
            <MenuItem
              key={option.value}
              selected={option.value === selectedValue}
              onClick={this.onMenuItemClick(option.value)}
            >
              {option.title}
            </MenuItem>
          ))}
        </Menu>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(MenuListItem);
