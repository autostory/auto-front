import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import themeDefault from '../../mui-theme/DefaultTheme';

const preloadStyle = {
  container: {
    display: 'inline-flex',
    width: '100vw',
    height: '100vh',
    minHeight: '100vh',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
};

class FullScreenPreLoader extends React.Component {
  render() {
    return (
      <MuiThemeProvider theme={themeDefault}>
        <div style={preloadStyle.container}>
          <CircularProgress />
          <div>Загрузка...</div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default FullScreenPreLoader;
