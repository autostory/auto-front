import React from 'react';
import Relay from 'react-relay/classic';
import PropTypes from 'prop-types';
import ViewerRoute from '../../../../common-code/relay/route/ViewerRoute';
import GuestComponentsContainer from './GuestComponentsContainer';
import FullScreenPreLoader from '../basic/FullScreenPreLoader';

class EnsureNotLoggedInApp extends React.Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node,
    ]),
  };

  static defaultProps = {
    children: null,
  };

  render() {
    return (
      <Relay.RootContainer
        Component={GuestComponentsContainer}
        route={new ViewerRoute()}
        forceFetch
        renderFetched={data => (
          <GuestComponentsContainer viewer={data.viewer} >
            {this.props.children}
          </GuestComponentsContainer>
        )}
        renderLoading={() => (
          <FullScreenPreLoader />
        )}
      />
    );
  }
}

export default EnsureNotLoggedInApp;
