import React from 'react';
import Relay from 'react-relay/classic';
import isNull from 'lodash/isNull';
import isUndefined from 'lodash/isUndefined';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import ConfiguredRadium from '../../radium/ConfiguredRadium';
import relayContainer from '../../../../common-code/decorators/relayContainer';
import PasswordRestorePage from '../../pages/auth/PasswordRestorePage';
import RegistrationPage from '../../pages/auth/RegistrationPage';
import LoginPage from '../../pages/auth/LoginPage';
import ClientAccountActivationPage from '../../pages/auth/ClientAccountActivationPage';

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id,
          ${PasswordRestorePage.getFragment('viewer')},
          ${LoginPage.getFragment('viewer')},
          ${PasswordRestorePage.getFragment('viewer')},
          ${RegistrationPage.getFragment('viewer')},
          ${ClientAccountActivationPage.getFragment('viewer')},
      }
  `,
})
@ConfiguredRadium
@withRouter
class GuestComponentsContainer extends React.Component {
  static propTypes = {
    viewer: PropTypes.oneOfType([
      PropTypes.shape({
        id: PropTypes.string,
      }),
    ]).isRequired,
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node,
    ]),
    history: ReactRouterPropTypes.history,
  };

  static defaultProps = {
    viewer: null,
    history: null,
    children: null,
  };

  render() {
    const { history } = this.props;
    if (
      !isNull(this.props.viewer)
      && !isUndefined(this.props.viewer.id)
      && !isNull(this.props.viewer.id)
      && this.props.viewer.id !== 'Vmlld2VyOjA='
    ) {
      // TODO: render не должен иметь side effects
      history.push('/');

      return null;
    }

    return <React.Fragment>{React.cloneElement(this.props.children, { viewer: this.props.viewer })}</React.Fragment>;
  }
}

export default GuestComponentsContainer;
