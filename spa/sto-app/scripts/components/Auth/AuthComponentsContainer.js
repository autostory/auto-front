import React from 'react';
import Relay from 'react-relay/classic';
import isNull from 'lodash/isNull';
import isUndefined from 'lodash/isUndefined';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import ConfiguredRadium from '../../radium/ConfiguredRadium';
import relayContainer from '../../../../common-code/decorators/relayContainer';

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id
      }
  `,
})
@ConfiguredRadium
@withRouter
class AuthComponentsContainer extends React.Component {
  static propTypes = {
    viewer: PropTypes.oneOfType([
      PropTypes.shape({
        id: PropTypes.string,
      }),
    ]).isRequired,
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node,
    ]),
    history: ReactRouterPropTypes.history,
    location: ReactRouterPropTypes.location,
  };

  static defaultProps = {
    viewer: null,
    history: null,
    location: null,
    children: null,
  };

  shouldComponentUpdate = nextProps => this.props.viewer.id !== nextProps.viewer.id;

  render() {
    if (
      !isNull(this.props.viewer)
      && !isUndefined(this.props.viewer.id)
      && !isNull(this.props.viewer.id)
      && this.props.viewer.id !== 'Vmlld2VyOjA='
    ) {
      return <React.Fragment>{this.props.children}</React.Fragment>;
    }

    const { location, history } = this.props;

    // Перенаправляем на страницу авторизации
    const redirectFrom = location.pathname + location.search + location.hash;
    let redirectTo = '/login';
    if (redirectFrom !== '/') {
      redirectTo = `/login?redirectFrom=${redirectFrom}`;
    }

    history.push(redirectTo);

    return null;
  }
}

export default AuthComponentsContainer;
