import Relay from 'react-relay/classic';

class CreateClientAsManagerMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { createClientAsManager }`;

  getVariables = () => ({
    email: this.props.input.email,
    phone: this.props.input.phone,
    firstName: this.props.input.firstName,
    surname: this.props.input.surname,
    patronymic: this.props.input.patronymic,

    noCarAdd: this.props.input.noCarAdd,

    vin: this.props.input.vin,
    registrationSignNumber: this.props.input.registrationSignNumber,
    color: this.props.input.color,
    year: this.props.input.year,
    mileage: this.props.input.mileage,

    catalogCarTypeId: this.props.input.catalogCarTypeId,
    catalogCarMarkId: this.props.input.catalogCarMarkId,
    catalogCarModelId: this.props.input.catalogCarModelId,
    catalogCarGenerationId: this.props.input.catalogCarGenerationId,
    catalogCarSerieId: this.props.input.catalogCarSerieId,
    catalogCarModificationId: this.props.input.catalogCarModificationId,
    catalogCarEquipmentId: this.props.input.catalogCarEquipmentId,

    sendEmailInvite: this.props.input.sendEmailInvite,
    sendSMSInvite: this.props.input.sendSMSInvite,
  });

  getFatQuery = () => Relay.QL`
      fragment on createClientAsManagerMutationPayload {
          clients {
              id
              clientList {
                  edges {
                      node {
                          id
                          email
                          phone
                          firstName
                          surname
                          patronymic
                          cars {
                              itemsCount
                              carList {
                                  edges {
                                      node {
                                          id
                                          vin
                                          registrationSignNumber
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
          }
          errors {
              messages {
                  type
                  fieldName
                  text
              }
          }
          newClientId
      }
  `;

  getConfigs = () => [
    {
      type: 'RANGE_ADD',
      parentName: 'clients',
      parentID: this.props.viewer.myFirm.clients.id,
      connectionName: 'ClientConnection',
      edgeName: 'edges',
      rangeBehaviors: {
        '': 'prepend',
      },
    },
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on createClientAsManagerMutationPayload {
                newClientId
            }
        `,
      ],
    },
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on createClientAsManagerMutationPayload {
                errors {
                    messages {
                        type
                        fieldName
                        text
                    }
                }
            }
        `,
      ],
    },
  ];

  static fragments = {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
            myFirm {
                id
                clients {
                    id
                    clientList {
                        edges {
                            node {
                                id
                                email
                                phone
                                firstName
                                surname
                                patronymic
                                cars {
                                    itemsCount
                                    carList {
                                        edges {
                                            node {
                                                id
                                                vin
                                                registrationSignNumber
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    `,
  };
}

export default CreateClientAsManagerMutation;
