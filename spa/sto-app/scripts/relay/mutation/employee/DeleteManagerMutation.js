import Relay from 'react-relay/classic';

class DeleteManagerMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { deleteManagerMutation }`;

  getVariables = () => ({
    managerId: this.props.manager.id,
  });

  getFatQuery = () => Relay.QL`
      fragment on deleteManagerMutationPayload {
          manager
          myFirm
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        manager: this.props.manager.id,
      },
    },
  ];

  static fragments = {
    manager: () => Relay.QL`
        fragment on Manager {
            id
        }
    `,
  };
}

export default DeleteManagerMutation;
