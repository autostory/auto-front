import Relay from 'react-relay/classic';

class EditManagerMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { editManagerMutation }`;

  getVariables = () => ({
    firstName: this.props.input.firstName,
    surname: this.props.input.surname,
    patronymic: this.props.input.patronymic,
    email: this.props.input.email,
    phone: this.props.input.phone,
    managerId: this.props.manager.id,
  });

  getFatQuery = () => Relay.QL`
      fragment on editManagerMutationPayload {
          manager
          myFirm
          errors
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        manager: this.props.manager.id,
      },
    },
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on editManagerMutationPayload {
                errors {
                    messages {
                        type
                        fieldName
                        text
                    }
                }
            }
        `,
      ],
    },
  ];

  static fragments = {
    manager: () => Relay.QL`
        fragment on Manager {
            id
        }
    `,
  };
}

export default EditManagerMutation;
