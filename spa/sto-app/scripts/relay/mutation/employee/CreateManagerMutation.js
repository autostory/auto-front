import Relay from 'react-relay/classic';

class CreateManagerMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { createManagerMutation }`;

  getVariables = () => ({
    firstName: this.props.input.firstName,
    surname: this.props.input.surname,
    patronymic: this.props.input.patronymic,
    email: this.props.input.email,
    phone: this.props.input.phone,
    firmId: this.props.viewer.myFirm.id,
  });

  getFatQuery = () => Relay.QL`
      fragment on createManagerMutationPayload {
          manager
          myFirm
          errors
      }
  `;

  getConfigs = () => [
    {
      type: 'RANGE_ADD',
      parentName: 'myFirm',
      parentID: this.props.viewer.myFirm.id,
      connectionName: 'ManagerConnection',
      edgeName: 'edges',
      rangeBehaviors: {
        '': 'append',
      },
    },
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on createManagerMutationPayload {
                errors {
                    messages {
                        type
                        fieldName
                        text
                    }
                }
            }
        `,
      ],
    },
  ];

  static fragments = {
    viewer: () => Relay.QL`
        fragment on Viewer {
            myFirm {
                id
            }
        }
    `,
  };
}

export default CreateManagerMutation;
