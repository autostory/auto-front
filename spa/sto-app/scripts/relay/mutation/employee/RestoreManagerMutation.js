import Relay from 'react-relay/classic';

class RestoreManagerMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { restoreManagerMutation }`;

  getVariables = () => ({
    managerId: this.props.manager.id,
  });

  getFatQuery = () => Relay.QL`
      fragment on restoreManagerMutationPayload {
          manager
          myFirm
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        manager: this.props.manager.id,
      },
    },
  ];

  static fragments = {
    manager: () => Relay.QL`
        fragment on Manager {
            id
        }
    `,
  };
}

export default RestoreManagerMutation;
