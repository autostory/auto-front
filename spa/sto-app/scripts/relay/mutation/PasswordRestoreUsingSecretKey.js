import Relay from 'react-relay/classic';

// FROM https://facebook.github.io/relay/docs/en/classic/classic-guides-mutations.html

class PasswordRestoreUsingSecretKey extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { passwordRestoreUsingSecretKey }`;

  getVariables = () => ({
    token: this.props.token,
    smsCode: this.props.smsCode,
  });

  getFatQuery = () => Relay.QL`
      fragment on passwordRestoreUsingSecretKeyMutationPayload {
          passwordChanged
      }
  `;

  getConfigs = () => [{
    type: 'REQUIRED_CHILDREN',
    children: [
      Relay.QL`
          fragment on passwordRestoreUsingSecretKeyMutationPayload {
              passwordChanged
          }
      `,
    ],
  }];

  static fragments = {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
        }
    `,
  };
}

export default PasswordRestoreUsingSecretKey;
