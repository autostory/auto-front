import Relay from 'react-relay/classic';

class AddCarToClientMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { addCarToClient }`;

  getVariables = () => ({
    vin: this.props.input.vin,
    registrationSignNumber: this.props.input.registrationSignNumber,
    color: this.props.input.color,
    year: this.props.input.year,
    mileage: this.props.input.mileage,

    catalogCarTypeId: this.props.input.catalogCarTypeId,
    catalogCarMarkId: this.props.input.catalogCarMarkId,
    catalogCarModelId: this.props.input.catalogCarModelId,
    catalogCarGenerationId: this.props.input.catalogCarGenerationId,
    catalogCarSerieId: this.props.input.catalogCarSerieId,
    catalogCarModificationId: this.props.input.catalogCarModificationId,
    catalogCarEquipmentId: this.props.input.catalogCarEquipmentId,
    clientId: this.props.input.clientId,
  });

  getFatQuery = () => Relay.QL`
      fragment on addCarToClientMutationPayload {
          newCar {
              id
          }
          errors {
              messages {
                  type
                  fieldName
                  text
              }
          }
      }
  `;

  getConfigs = () => [
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on addCarToClientMutationPayload {
                errors {
                    messages {
                        type
                        fieldName
                        text
                    }
                }
            }
        `,
      ],
    },
  ];
}

export default AddCarToClientMutation;
