import Relay from 'react-relay/classic';

class EditCarMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { editCarMutation }`;

  getVariables = () => ({
    vin: this.props.input.vin,
    registrationSignNumber: this.props.input.registrationSignNumber,
    color: this.props.input.color,
    year: this.props.input.year,

    catalogCarTypeId: this.props.input.catalogCarTypeId,
    catalogCarMarkId: this.props.input.catalogCarMarkId,
    catalogCarModelId: this.props.input.catalogCarModelId,
    catalogCarGenerationId: this.props.input.catalogCarGenerationId,
    catalogCarSerieId: this.props.input.catalogCarSerieId,
    catalogCarModificationId: this.props.input.catalogCarModificationId,
    catalogCarEquipmentId: this.props.input.catalogCarEquipmentId,
    carId: this.props.car.id,
  });

  getFatQuery = () => Relay.QL`
      fragment on editCarMutationPayload {
          car
          errors {
              messages {
                  type
                  fieldName
                  text
              }
          }
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        car: this.props.car.id,
      },
    },
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on editCarMutationPayload {
                errors {
                    messages {
                        type
                        fieldName
                        text
                    }
                }
            }
        `,
      ],
    },
  ];

  static fragments = {
    car: () => Relay.QL`
        fragment on Car {
            id
        }
    `,
  };
}

export default EditCarMutation;
