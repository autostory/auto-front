import Relay from 'react-relay/classic';

class SetScheduleServiceWorksStatusMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { setScheduleServiceWorksStatusMutation }`;

  getVariables = () => ({
    scheduleId: this.props.schedule.id,
    scheduleStatus: this.props.input.scheduleStatus,
    serviceWorksInput: this.props.input.serviceWorksInput,
    sendEmailNotification: this.props.input.sendEmailNotification || false,
    sendSMSNotification: this.props.input.sendSMSNotification || false,
  });

  getFatQuery = () => Relay.QL`
      fragment on setScheduleServiceWorksStatusMutationPayload {
          schedule
          order
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        schedule: this.props.schedule.id,
      },
    },
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        order: this.props.order.id,
      },
    },
  ];

  static fragments = {
    schedule: () => Relay.QL`
        fragment on BoxSchedule {
            id
            status
            serviceWorks {
                id
                status
            }
        }
    `,
    order: () => Relay.QL`
        fragment on Order {
            id
            status
        }
    `,
  };
}

export default SetScheduleServiceWorksStatusMutation;
