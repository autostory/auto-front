import Relay from 'react-relay/classic';

class SetScheduleStatusMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { setScheduleStatusMutation }`;

  getVariables = () => ({
    scheduleId: this.props.schedule.id,
    status: this.props.input.status,
  });

  getFatQuery = () => Relay.QL`
      fragment on setScheduleStatusMutationPayload {
          schedule {
              id
              status
              serviceWorks {
                  id
                  status
              }
          }
          order {
              status
              serviceWorks {
                  id
                  status
              }
              schedules {
                  id
                  status
              }
          }
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        schedule: this.props.schedule.id,
      },
    },
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        order: this.props.order.id,
      },
    },
  ];

  static fragments = {
    schedule: () => Relay.QL`
        fragment on BoxSchedule {
            id
            status
        }
    `,
    order: () => Relay.QL`
        fragment on Order {
            id
            status
        }
    `,
  };
}

export default SetScheduleStatusMutation;
