import Relay from 'react-relay/classic';
import map from 'lodash/map';
import cloneDeep from 'lodash/cloneDeep';
import get from 'lodash/get';

class EditOfficeMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { editOffice }`;

  getVariables = () => {
    const workTimes = cloneDeep(this.props.input.workTimes);

    return ({
      officeId: this.props.office.id,
      title: this.props.input.title,
      workTimes: map(workTimes, workTime => {
        workTime.officeId = this.props.office.id;
        workTime.start = get(workTime, 'start.formattedDateTime', null);
        workTime.end = get(workTime, 'end.formattedDateTime', null);

        if (workTime.start) {
          workTime.start = `${workTime.start.getHours()}:${workTime.start.getMinutes()}`;
        }

        if (workTime.end) {
          workTime.end = `${workTime.end.getHours()}:${workTime.end.getMinutes()}`;
        }

        return workTime;
      }),
      address: this.props.input.address,
      phone: this.props.input.phone,
      officeMangers: this.props.input.officeMangers,
    });
  };

  getFatQuery = () => Relay.QL`
      fragment on editOfficeMutationPayload {
          office {
              id
              title
              address
              workTimes {
                  id
                  holiday
                  roundTheClock
                  start {
                      formattedDateTime
                  }
                  end {
                      formattedDateTime
                  }
              }
              phone
              managers {
                  edges {
                      node {
                          id
                          fullName
                      }
                  }
              }
          }
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        office: this.props.office.id,
      },
    },
  ];

  static fragments = {
    office: () => Relay.QL`
        fragment on Office {
            id
        }
    `,
  };
}

export default EditOfficeMutation;
