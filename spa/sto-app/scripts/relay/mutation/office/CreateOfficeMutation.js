import Relay from 'react-relay/classic';

class CreateOfficeMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { createOffice }`;

  getVariables = () => ({
    title: this.props.input.title,
    address: this.props.input.address,
    phone: this.props.input.phone,
    firmId: this.props.input.firmId,
  });

  getFatQuery = () => Relay.QL`
      fragment on createOfficeMutationPayload {
          newOffice {
              id
              title
              address
              phone
          }
      }
  `;

  getConfigs = () => [{
    type: 'REQUIRED_CHILDREN',
    children: [
      Relay.QL`
          fragment on createOfficeMutationPayload {
              newOffice {
                  id
                  title
                  address
                  phone
              }
          }
      `,
    ],
  }];

  // Почему-то теперь это генерит кривой запрос
  // getConfigs = () => [
  //   // {
  //   //   type: 'RANGE_ADD',
  //   //   parentName: 'newOffice',
  //   //   parentID: this.props.viewer.myFirm.id,
  //   //   connectionName: 'FirmOffices',
  //   //   edgeName: 'edge',
  //   //   rangeBehaviors: {
  //   //     // When the ships connection is not under the influence
  //   //     // of any call, append the ship to the end of the connection
  //   //     '': 'append',
  //   //   },
  //   // },
  // ];

  static fragments = {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
            myFirm {
                id
            }
        }
    `,
  };
}

export default CreateOfficeMutation;
