import Relay from 'react-relay/classic';

class ChangeMyPasswordMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { changeMyPassword }`;

  getVariables = () => ({
    newPassword: this.props.input.newPassword,
    currentPassword: this.props.input.currentPassword,
  });

  getFatQuery = () => Relay.QL`
      fragment on changeMyPasswordMutationPayload {
          errors {
              messages {
                  type
                  fieldName
                  text
              }
          }
          passwordChanged
      }
  `;

  getConfigs = () => [
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on changeMyPasswordMutationPayload {
                passwordChanged
            }
        `,
      ],
    },
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on changeMyPasswordMutationPayload {
                errors {
                    messages {
                        type
                        fieldName
                        text
                    }
                }
            }
        `,
      ],
    },
  ];

  static fragments = {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
            firstName
            surname
            patronymic
            fullName
        }
    `,
  };
}

export default ChangeMyPasswordMutation;
