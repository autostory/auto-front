import Relay from 'react-relay/classic';

class EditMyProfileMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { editMyProfileMutation }`;

  getVariables = () => ({
    firstName: this.props.input.firstName,
    surname: this.props.input.surname,
    patronymic: this.props.input.patronymic,
    email: this.props.input.email,
    phone: this.props.input.phone,
  });

  getFatQuery = () => Relay.QL`
      fragment on editMyProfileMutationPayload {
          errors {
              messages {
                  type
                  fieldName
                  text
              }
          }
          viewer
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        viewer: this.props.viewer.id,
      },
    },
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on editMyProfileMutationPayload {
                errors {
                    messages {
                        type
                        fieldName
                        text
                    }
                }
            }
        `,
      ],
    },
  ];

  static fragments = {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
        }
    `,
  };
}

export default EditMyProfileMutation;
