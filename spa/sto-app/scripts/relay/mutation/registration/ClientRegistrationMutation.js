import Relay from 'react-relay/classic';

class ClientRegistrationMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { clientRegistration }`;

  getVariables = () => ({
    email: this.props.input.email,
    mobilePhone: this.props.input.mobilePhone,
    newPassword: this.props.input.newPassword,
    newPasswordConfirm: this.props.input.newPasswordConfirm,
  });

  getFatQuery = () => Relay.QL`
      fragment on clientRegistrationMutationPayload {
          clientCreated
          errors {
              messages {
                  type
                  fieldName
                  text
              }
          }
      }
  `;

  getConfigs = () => [
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on clientRegistrationMutationPayload {
                clientCreated
            }
        `,
      ],
    },
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on clientRegistrationMutationPayload {
                errors {
                    messages {
                        type
                        fieldName
                        text
                    }
                }
            }
        `,
      ],
    },
  ];

  static fragments = {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
            isAuthenticated
        }
    `,
  };
}

export default ClientRegistrationMutation;
