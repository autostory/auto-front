import Relay from 'react-relay/classic';

// FROM https://facebook.github.io/relay/docs/en/classic/classic-guides-mutations.html

class AccountActivationMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { activateAccount }`;

  getVariables = () => ({
    token: this.props.token,
  });

  getFatQuery = () => Relay.QL`
      fragment on activateAccountMutationPayload {
          activated
      }
  `;

  getConfigs = () => [{
    type: 'REQUIRED_CHILDREN',
    children: [
      Relay.QL`
          fragment on activateAccountMutationPayload {
              activated
          }
      `,
    ],
  }];

  static fragments = {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
        }
    `,
  };
}

export default AccountActivationMutation;
