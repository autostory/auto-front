import Relay from 'react-relay/classic';

const phoneNormalize = (value) => {
  const numb = value.match(/\d/g) || [];

  return numb.join('');
};

class FirmRegistrationMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { firmRegistration }`;

  getVariables = () => ({
    email: this.props.input.email,
    newPassword: this.props.input.newPassword,
    newPasswordConfirm: this.props.input.newPasswordConfirm,
    phone: this.props.input.phone,
    managerMobilePhone: phoneNormalize(this.props.input.managerMobilePhone),
    firmName: this.props.input.firmName,
  });

  getFatQuery = () => Relay.QL`
      fragment on firmRegistrationMutationPayload {
          firmAndManagerCreated
          errors {
              messages {
                  type
                  fieldName
                  text
              }
          }
      }
  `;

  getConfigs = () => [
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on firmRegistrationMutationPayload {
                firmAndManagerCreated
            }
        `,
      ],
    },
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on firmRegistrationMutationPayload {
                errors {
                    messages {
                        type
                        fieldName
                        text
                    }
                }
            }
        `,
      ],
    },
  ];

  static fragments = {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
            isAuthenticated
        }
    `,
  };
}

export default FirmRegistrationMutation;
