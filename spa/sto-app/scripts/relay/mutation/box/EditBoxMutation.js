import Relay from 'react-relay/classic';

class EditBoxMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { editOfficeBox }`;

  getVariables = () => ({
    boxId: this.props.input.boxId,
    title: this.props.input.title,
    active: this.props.input.active,
  });

  getFatQuery = () => Relay.QL`
      fragment on editBoxMutationPayload {
          box {
              id
              title
              active
          }
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        box: this.props.box.id,
      },
    },
  ];

  static fragments = {
    office: () => Relay.QL`
        fragment on Office {
            id
        }
    `,
  };
}

export default EditBoxMutation;
