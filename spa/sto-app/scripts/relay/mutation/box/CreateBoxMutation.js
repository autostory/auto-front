import Relay from 'react-relay/classic';

class CreateBoxMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { createOfficeBox }`;

  getVariables = () => ({
    title: this.props.input.title,
    officeId: this.props.input.officeId,
  });

  getFatQuery = () => Relay.QL`
      fragment on createBoxMutationPayload {
          newOfficeBox {
              id
              title
              active
          }
      }
  `;

  getConfigs = () => [{
    type: 'REQUIRED_CHILDREN',
    children: [
      Relay.QL`
          fragment on createBoxMutationPayload {
              newOfficeBox {
                  id
                  title
                  active
              }
          }
      `,
    ],
  }];

  static fragments = {
    office: () => Relay.QL`
        fragment on Office {
            id
        }
    `,
  };
}

export default CreateBoxMutation;
