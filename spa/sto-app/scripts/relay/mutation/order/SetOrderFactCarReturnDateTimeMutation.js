import Relay from 'react-relay/classic';

class SetOrderFactCarArriveDateTimeMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { editOrderCarArriveInfoMutation }`;

  getVariables = () => ({
    orderId: this.props.order.id,
    factCarReturnDateTime: this.props.input.factCarReturnDateTime,
    sendEmailNotification: this.props.input.sendEmailNotification,
    sendSMSNotification: this.props.input.sendSMSNotification,
  });

  getFatQuery = () => Relay.QL`
      fragment on editOrderCarArriveInfoMutationPayload {
          order {
              status
              factCarReturnDateTime {
                  timestamp
                  formattedDateTime
              }
          }
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        order: this.props.order.id,
      },
    },
  ];

  static fragments = {
    order: () => Relay.QL`
        fragment on Order {
            id
            factCarReturnDateTime {
                timestamp
                formattedDateTime
            }
        }
    `,
  };
}

export default SetOrderFactCarArriveDateTimeMutation;
