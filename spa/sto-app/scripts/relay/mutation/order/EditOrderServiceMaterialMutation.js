import Relay from 'react-relay/classic';
import OrderSchedulesForm from '../../../pages/manager/OrderPage/includes/OrderSchedulesForm';
import OrderServiceMaterialsForm from '../../../pages/manager/OrderPage/includes/OrderServiceMaterialsForm';

class EditOrderServiceMaterialMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { editOrderServiceMaterialsMutation }`;

  getVariables = () => ({
    orderId: this.props.order.id,
    serviceMaterials: this.props.input.serviceMaterials,
    deletingServiceMaterialsIds: this.props.input.deletingServiceMaterialsIds,
  });

  getFatQuery = () => Relay.QL`
      fragment on editOrderServiceMaterialsMutationPayload {
          order {
              id
              status
              clientWillReceivePointAmount
              totalBill
              paymentsSum
              ${OrderServiceMaterialsForm.getFragment('order')}
              ${OrderSchedulesForm.getFragment('order')}
          }
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        order: this.props.order.id,
      },
    },
  ];

  static fragments = {
    order: () => Relay.QL`
        fragment on Order {
            id
        }
    `,
  };
}

export default EditOrderServiceMaterialMutation;
