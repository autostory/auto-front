import Relay from 'react-relay/classic';

class EditOrderServiceWorksMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { editOrderServiceWorksMutation }`;

  getVariables = () => ({
    orderId: this.props.order.id,
    serviceWorks: this.props.input.serviceWorks,
    deletingServiceWorksIds: this.props.input.deletingServiceWorksIds,
  });

  getFatQuery = () => Relay.QL`
      fragment on editOrderServiceWorksMutationPayload {
          order
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        order: this.props.order.id,
      },
    },
  ];

  static fragments = {
    order: () => Relay.QL`
        fragment on Order {
            id
        }
    `,
  };
}

export default EditOrderServiceWorksMutation;
