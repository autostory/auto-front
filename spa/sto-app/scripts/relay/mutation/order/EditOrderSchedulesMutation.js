import Relay from 'react-relay/classic';

class EditOrderSchedulesMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { editOrderSchedulesMutation }`;

  getVariables = () => ({
    orderId: this.props.order.id,
    schedules: this.props.input.schedules,
    deletingSchedulesIds: this.props.input.deletingSchedulesIds,
  });

  getFatQuery = () => Relay.QL`
      fragment on editOrderSchedulesMutationPayload {
          order
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        order: this.props.order.id,
      },
    },
  ];

  static fragments = {
    order: () => Relay.QL`
        fragment on Order {
            id
        }
    `,
  };
}

export default EditOrderSchedulesMutation;
