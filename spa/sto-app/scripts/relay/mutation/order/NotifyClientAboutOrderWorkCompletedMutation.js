import Relay from 'react-relay/classic';

class NotifyClientAboutOrderWorkCompletedMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { notifyClientAboutOrderWorkCompletedMutation }`;

  getVariables = () => ({
    orderId: this.props.order.id,
    sendEmailNotification: this.props.input.sendEmailNotification,
    sendSMSNotification: this.props.input.sendSMSNotification,
  });

  getFatQuery = () => Relay.QL`
      fragment on notifyClientAboutOrderWorkCompletedMutationPayload {
          order
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        order: this.props.order.id,
      },
    },
  ];

  static fragments = {
    order: () => Relay.QL`
        fragment on Order {
            id
        }
    `,
  };
}

export default NotifyClientAboutOrderWorkCompletedMutation;
