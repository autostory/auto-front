import Relay from 'react-relay/classic';

class SetOrderFactCarArriveDateTimeMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { editOrderCarArriveInfoMutation }`;

  getVariables = () => ({
    orderId: this.props.order.id,
    factClientVisitDateTime: this.props.input.factClientVisitDateTime,
  });

  getFatQuery = () => Relay.QL`
      fragment on editOrderCarArriveInfoMutationPayload {
          order {
              status
              factClientVisitDateTime {
                  timestamp
                  formattedDateTime
              }
          }
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        order: this.props.order.id,
      },
    },
  ];

  static fragments = {
    order: () => Relay.QL`
        fragment on Order {
            id
            factClientVisitDateTime {
                timestamp
                formattedDateTime
            }
        }
    `,
  };
}

export default SetOrderFactCarArriveDateTimeMutation;
