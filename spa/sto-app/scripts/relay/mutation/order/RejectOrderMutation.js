import Relay from 'react-relay/classic';

class RejectOrderMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { rejectOrderMutation }`;

  getReasonEmunText = (rejectReasonId) => {
    switch (rejectReasonId) {
      case 1:
        return 'COST';
      case 2:
        return 'TIME';
      case 3:
        return 'NO_SERVICE';
      case 4:
        return 'BAD_CAR';
      case 5:
        return 'OTHER';
      default:
        return '';
    }
  };

  getVariables = () => ({
    orderId: this.props.order.id,
    rejectReasonId: this.getReasonEmunText(this.props.input.rejectReasonId),
    rejectReasonDescription: this.props.input.rejectReasonDescription,
  });

  getFatQuery = () => Relay.QL`
      fragment on rejectOrderMutationPayload {
          order
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        order: this.props.order.id,
      },
    },
  ];

  static fragments = {
    order: () => Relay.QL`
        fragment on Order {
            id
        }
    `,
  };
}

export default RejectOrderMutation;
