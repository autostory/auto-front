import Relay from 'react-relay/classic';
import get from 'lodash/get';
import OrderMainInfoForm from '../../../pages/manager/OrderPage/includes/OrderMainInfoForm';

class EditOrderMainInfoMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { editOrderMainInfoMutation }`;

  getVariables = () => {
    const values = {
      orderId: this.props.order.id,
      clientId: this.props.input.clientId,
      carId: this.props.input.carId,
      contactName: this.props.input.contactName,
      contactPhone: this.props.input.contactPhone,
      contactNote: this.props.input.contactNote,
      orderReason: this.props.input.orderReason,
      isClientVisitRequired: this.props.input.isClientVisitRequired,
      phoneSameAsInProfile: this.props.input.phoneSameAsInProfile,
    };

    const isClientArrive = get(this.props, 'order.factClientVisitDateTime.timestamp', false);

    if (!isClientArrive) {
      // Разрешаем править эту дату только если клиент еще не прибыл.
      values.estimatedClientArrivalDate = this.props.input.estimatedClientArrivalDate;
    }

    return values;
  };

  getFatQuery = () => Relay.QL`
      fragment on editOrderMainInfoMutationPayload {
          order {
              id
              status
              ${OrderMainInfoForm.getFragment('order')}
          }
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        order: this.props.order.id,
      },
    },
  ];

  static fragments = {
    order: () => Relay.QL`
        fragment on Order {
            id
            status
            factClientVisitDateTime {
                timestamp
            }
        }
    `,
  };
}

export default EditOrderMainInfoMutation;
