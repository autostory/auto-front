import Relay from 'react-relay/classic';

class AddOrderPaymentMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { addOrderPaymentMutation }`;

  getVariables = () => ({
    orderId: this.props.order.id,
    note: this.props.input.note,
    amount: this.props.input.amount,
    paymentType: this.props.input.paymentType,
  });

  getFatQuery = () => Relay.QL`
      fragment on addOrderPaymentMutationPayload {
          order {
              status
              id
              client {
                  points
              }
              clientWillReceivePointAmount
              paymentsSum
              payments {
                  id
                  note
                  cashier {
                      id
                      fullName
                      email
                      phone
                  }
                  amount
                  paymentType
                  makeAt {
                      formattedDateTime
                  }
              }
          }
      }
  `;

  getConfigs = () => [
    {
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        order: this.props.order.id,
      },
    },
  ];

  static fragments = {
    order: () => Relay.QL`
        fragment on Order {
            id
        }
    `,
  };
}

export default AddOrderPaymentMutation;
