import Relay from 'react-relay/classic';

class CreateOrderAsManagerMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { createOrderAsManager }`;

  getVariables = () => ({
    officeId: this.props.input.officeId,
    clientId: this.props.input.clientId,
    carId: this.props.input.carId,
    requestId: this.props.input.requestId,
    contactName: this.props.input.contactName,
    contactPhone: this.props.input.contactPhone,
    contactNote: this.props.input.contactNote,
    orderReason: this.props.input.orderReason,
    serviceMaterials: this.props.input.serviceMaterials,
    serviceWorks: this.props.input.serviceWorks,
    boxId: this.props.input.boxId,
    estimatedClientArrivalDate: this.props.input.estimatedClientArrivalDate,
    isClientVisitRequired: this.props.input.isClientVisitRequired,
    schedules: this.props.input.schedules,
    phoneSameAsInProfile: this.props.input.phoneSameAsInProfile,
  });

  getFatQuery = () => Relay.QL`
      fragment on createOrderAsManagerMutationPayload {
          newOrder {
              id
          }
      }
  `;

  getConfigs = () => [
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on createOrderAsManagerMutationPayload {
                newOrder {
                    id
                }
            }
        `,
      ],
    },
  ];
}

export default CreateOrderAsManagerMutation;
