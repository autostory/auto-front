import Relay from 'react-relay/classic';

class addRequestCommunicationNoAnswerResultMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { addRequestCommunicationNoAnswerResult }`;

  getVariables = () => ({
    requestId: this.props.request.id,
    timestamp: this.props.input.timestamp,
    notifyClient: this.props.input.notifyClient,
  });

  getFatQuery = () => Relay.QL`
      fragment on addRequestCommunicationNoAnswerResultPayload {
          request {
              id
          }
      }
  `;

  getConfigs = () => [{
    type: 'REQUIRED_CHILDREN',
    children: [
      Relay.QL`
          fragment on addRequestCommunicationNoAnswerResultPayload {
              request {
                  id
                  ${this.props.requestFragment}
              }
          }
      `,
    ],
  }];

  static fragments = {
    request: () => Relay.QL`
        fragment on Request {
            id
        }
    `,
  };
}

export default addRequestCommunicationNoAnswerResultMutation;
