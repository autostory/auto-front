import Relay from 'react-relay/classic';

class addRequestCommunicationRejectResultMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { addRequestCommunicationRejectResult }`;

  getReasonEmunText = (rejectReasonId) => {
    switch (rejectReasonId) {
      case 1:
        return 'COST';
      case 2:
        return 'TIME';
      case 3:
        return 'NO_SERVICE';
      case 4:
        return 'BAD_CAR';
      case 5:
        return 'OTHER';
      default:
        return '';
    }
  };

  getVariables = () => ({
    requestId: this.props.request.id,
    rejectReasonId: this.getReasonEmunText(this.props.input.rejectReasonId),
    rejectReasonDescription: this.props.input.rejectReasonDescription,
  });

  getFatQuery = () => Relay.QL`
      fragment on addRequestCommunicationRejectResultPayload {
          request {
              id
          }
      }
  `;

  getConfigs = () => [{
    type: 'REQUIRED_CHILDREN',
    children: [
      Relay.QL`
          fragment on addRequestCommunicationRejectResultPayload {
              request {
                  id
                  ${this.props.requestFragment}
              }
          }
      `,
    ],
  }];

  static fragments = {
    request: () => Relay.QL`
        fragment on Request {
            id
        }
    `,
  };
}

export default addRequestCommunicationRejectResultMutation;
