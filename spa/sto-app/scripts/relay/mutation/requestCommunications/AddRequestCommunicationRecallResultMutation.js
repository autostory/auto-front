import Relay from 'react-relay/classic';

class addRequestCommunicationRecallResultMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { addRequestCommunicationRecallResult }`;

  getVariables = () => ({
    requestId: this.props.request.id,
    timestamp: this.props.input.timestamp,
  });

  getFatQuery = () => Relay.QL`
      fragment on addRequestCommunicationRecallResultPayload {
          request {
              id
          }
      }
  `;

  getConfigs = () => [{
    type: 'REQUIRED_CHILDREN',
    children: [
      Relay.QL`
          fragment on addRequestCommunicationRecallResultPayload {
              request {
                  id
                  ${this.props.requestFragment}
              }
          }
      `,
    ],
  }];

  static fragments = {
    request: () => Relay.QL`
        fragment on Request {
            id
        }
    `,
  };
}

export default addRequestCommunicationRecallResultMutation;
