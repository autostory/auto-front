import Relay from 'react-relay/classic';

class AddRequestCommunicationVisitResultMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { addRequestCommunicationVisitResult }`;

  getVariables = () => ({
    requestId: this.props.request.id,
    officeId: this.props.input.officeId,
    estimatedClientArrivalDate: this.props.input.estimatedClientArrivalDate,
  });

  getFatQuery = () => Relay.QL`
      fragment on addRequestCommunicationVisitResultPayload {
          request {
              id
          }
      }
  `;

  getConfigs = () => [{
    type: 'REQUIRED_CHILDREN',
    children: [
      Relay.QL`
          fragment on addRequestCommunicationVisitResultPayload {
              request {
                  id
                  ${this.props.requestFragment}
              }
          }
      `,
    ],
  }];

  static fragments = {
    request: () => Relay.QL`
        fragment on Request {
            id
        }
    `,
  };
}

export default AddRequestCommunicationVisitResultMutation;
