import 'babel-polyfill';

import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'

import React from 'react';
import ReactDOM from 'react-dom';
import { reducer as reduxFormReducer } from 'redux-form';
import CssBaseline from '@material-ui/core/CssBaseline';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { Switch } from 'react-router-dom';
import DateFnsUtils from 'material-ui-pickers/utils/date-fns-utils';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import ruLocale from 'date-fns/locale/ru';

import Relay from 'react-relay/classic';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { composeWithDevTools } from 'redux-devtools-extension';
import { ConnectedRouter, routerReducer, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import EditManagerPage from './scripts/pages/manager/Employee/EditManagerPage';
import reducers from './scripts/reduxStores';

import IndexPage from './scripts/pages/IndexPage';

import LoginPage from './scripts/pages/auth/LoginPage';
import LogoutPage from './scripts/pages/auth/LogoutPage';
import PasswordRestorePage from './scripts/pages/auth/PasswordRestorePage';
import RegistrationSuccessfulNotification from './scripts/pages/auth/SuccessfulRegistrationNotification';

import MyClientsPage from './scripts/pages/manager/MyClientsPage';
import ClientAddPage from './scripts/pages/manager/ClientAddPage';
import CarInfoPage from './scripts/pages/manager/CarInfoPage';
import ClientInfoPage from './scripts/pages/manager/ClientInfoPage';
import OrderPage from './scripts/pages/manager/OrderPage/OrderPage';
import MyFirmPage from './scripts/pages/manager/MyFirm/MyFirmPage';
import EditOfficePage from './scripts/pages/manager/MyFirmOffice/EditOfficePage';
import AddOfficePage from './scripts/pages/manager/MyFirmOffice/AddOfficePage';
import AddManagerPage from './scripts/pages/manager/Employee/AddManagerPage';
import ProfilePage from './scripts/pages/profile/ProfilePage';
import MyFirmPaymentsPage from './scripts/pages/manager/MyFirmPaymentsPage';
import MyFirmRequestsPage from './scripts/pages/manager/MyFirmRequestsPage';
import MyFirmOrdersPage from './scripts/pages/manager/MyFirmOrdersPage';

import NoMatch from './scripts/pages/NoMatch';
import PrivateRoute from './scripts/Route/PrivateRoute';
import OnlyForGuestRoute from './scripts/Route/OnlyForGuestRoute';
import RegistrationPage from './scripts/pages/auth/RegistrationPage';
import ClientAccountActivationPage from './scripts/pages/auth/ClientAccountActivationPage';
import FirmAndFirstManagerAccountActivationPage from './scripts/pages/manager/FirmAndFirstManagerAccountActivationPage';
import WFMPage from './scripts/pages/wfm/WFMPage';

// This one is already present in admin-on-rest Layout, but it seems it does nothing
// if called after the initial ReactDOM.render()
// @link https://github.com/callemall/material-ui/issues/4670#issuecomment-2350319175
try {
  injectTapEventPlugin();
} catch (e) {
  // do nothing
}

const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const middleware = routerMiddleware(history);

const composeEnhancers = composeWithDevTools({
  // Specify here name, actionsBlacklist, actionsCreators and other options if needed
});

// Add the reducer to your store on the `router` key
// Also apply our middleware for navigating
const store = createStore(
  combineReducers({
    ...reducers,
    router: routerReducer,
    form: reduxFormReducer,
  }),
  composeEnhancers(applyMiddleware(...middleware)),
);

Relay.injectNetworkLayer(new Relay.DefaultNetworkLayer('/graphql', {
  credentials: 'same-origin',
  fetchTimeout: 30000,
  retryDelays: [],
}));

// if (process.env.NODE_ENV !== 'production') {
//   const { whyDidYouUpdate } = require('why-did-you-update');
//   whyDidYouUpdate(React, { include: [/^CreateNewOrderStepper/] });
// }

ReactDOM.render(
  <MuiPickersUtilsProvider utils={DateFnsUtils} locale={ruLocale}>
    <Provider store={store}>
      <ConnectedRouter
        history={history}
        environment={Relay.Store}
      >
        <React.Fragment>
          <CssBaseline />
          <Switch>
            <PrivateRoute exact path="/" component={IndexPage} />
            <PrivateRoute exact path="/logout" component={LogoutPage} />

            <PrivateRoute exact path="/profile" component={ProfilePage} />
            <PrivateRoute exact path="/wfm" component={WFMPage} />
            <PrivateRoute exact path="/wfm/:officeId" component={WFMPage} />

            <PrivateRoute path="/client/add" component={ClientAddPage} />
            <PrivateRoute path="/client/:id" component={ClientInfoPage} />

            <PrivateRoute path="/car/:id" component={CarInfoPage} />

            <PrivateRoute exact path="/my-clients" component={MyClientsPage} />
            <PrivateRoute exact path="/my-firm" component={MyFirmPage} />
            <PrivateRoute exact path="/my-firm/payments" component={MyFirmPaymentsPage} />
            <PrivateRoute exact path="/requests" component={MyFirmRequestsPage} />
            <PrivateRoute exact path="/orders" component={MyFirmOrdersPage} />
            <PrivateRoute exact path="/order/:id" component={OrderPage} />
            <PrivateRoute exact path="/my-firm/office/edit/:id" component={EditOfficePage} />
            <PrivateRoute exact path="/my-firm/office/add" component={AddOfficePage} />
            <PrivateRoute exact path="/my-firm/manager/add" component={AddManagerPage} />
            <PrivateRoute exact path="/my-firm/manager/edit/:id" component={EditManagerPage} />

            <OnlyForGuestRoute exact path="/login" component={LoginPage} />
            <OnlyForGuestRoute exact path="/register" component={RegistrationPage} />
            <OnlyForGuestRoute exact path="/password-restore" component={PasswordRestorePage} />
            <OnlyForGuestRoute exact path="/registration-successful" component={RegistrationSuccessfulNotification} />

            <OnlyForGuestRoute exact path="/activate/client/:token" component={ClientAccountActivationPage} />
            <PrivateRoute
              exact
              path="/activate/firmAndFirstManager/:token"
              component={FirmAndFirstManagerAccountActivationPage}
            />

            <PrivateRoute component={NoMatch} />
          </Switch>
        </React.Fragment>
      </ConnectedRouter>
    </Provider>
  </MuiPickersUtilsProvider>,
  document.getElementById('app'),
);
