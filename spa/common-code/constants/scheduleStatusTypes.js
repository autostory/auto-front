export const SCHEDULE_WAIT_WORK_START = 'SCHEDULE_WAIT_WORK_START';
export const SCHEDULE_IN_WORK = 'SCHEDULE_IN_WORK';
export const SCHEDULE_DONE = 'SCHEDULE_DONE';

export const getScheduleStatusTypeName = (type) => {
  switch (type) {
    case SCHEDULE_WAIT_WORK_START:
      return 'Ожидает взятия в работу';
    case SCHEDULE_IN_WORK:
      return 'В работе';
    case SCHEDULE_DONE:
      return 'Выполнен';
    default:
      return '';
  }
};

export const getScheduleStatusTypes = () => [
  SCHEDULE_WAIT_WORK_START,
  SCHEDULE_IN_WORK,
  SCHEDULE_DONE,
];
