export const NEW = 'NEW';
export const PERSISTENT = 'PERSISTENT';
export const VIP = 'VIP';

export const getClientCategoryTypeName = (type) => {
  switch (type) {
    case NEW:
      return 'Новый';
    case PERSISTENT:
      return 'Постоянный';
    case VIP:
      return 'VIP';
    default:
      return '';
  }
};