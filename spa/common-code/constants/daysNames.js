export const DAYS_OF_WEEK_NAMES_FULL = {
  1: 'понедельник',
  2: 'вторник',
  3: 'среда',
  4: 'четверг',
  5: 'пятница',
  6: 'суббота',
  7: 'воскресенье',
};

export const DAYS_OF_WEEK_NAMES_SHORT = {
  1: 'пн.',
  2: 'вт.',
  3: 'ср.',
  4: 'чт.',
  5: 'пт.',
  6: 'сб.',
  7: 'вс.',
};
