export const COST = 'COST';
export const TIME = 'TIME';
export const NO_SERVICE = 'NO_SERVICE';
export const BAD_CAR = 'BAD_CAR';
export const OTHER = 'OTHER';

export const getResultRequestCommunicationRejectReasonTypeName = (type) => {
  switch (type) {
    case COST:
      return 'Не устроила стоимость';
    case TIME:
      return 'Не устроило время';
    case NO_SERVICE:
      return 'Не оказываем такую услугу';
    case BAD_CAR:
      return 'Не обслуживаем такой автомобиль';
    case OTHER:
      return 'Другое';
    default:
      return '';
  }
};