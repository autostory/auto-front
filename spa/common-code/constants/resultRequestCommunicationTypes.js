export const VISIT = 'VISIT';
export const RECALL = 'RECALL';
export const NO_ANSWER = 'NO_ANSWER';
export const REJECT = 'REJECT';

export const getResultRequestCommunicationTypeName = (type) => {
  switch (type) {
    case VISIT:
      return 'Договорились о визите';
    case RECALL:
      return 'Перезвонить';
    case NO_ANSWER:
      return 'Недозвонился';
    case REJECT:
      return 'Отказ';
    default:
      return '';
  }
};
