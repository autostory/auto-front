export const WAIT_CAR_ARRIVE = 'WAIT_CAR_ARRIVE';
export const WAIT_WORK_ADD = 'WAIT_WORK_ADD';
export const WAIT_PLANING = 'WAIT_PLANING';
export const WAIT_WORK_START = 'WAIT_WORK_START';
export const IN_WORK = 'IN_WORK';
export const WAIT_CAR_RETURN = 'WAIT_CAR_RETURN';
export const WAIT_PAYMENT = 'WAIT_PAYMENT';
export const DONE = 'DONE';
export const REJECT = 'REJECT';

export const getOrderStatusTypeName = (type) => {
  switch (type) {
    case WAIT_CAR_ARRIVE:
      return 'Ожидает прибытия авто';
    case WAIT_WORK_ADD:
      return 'Ожидает добавления работ';
    case WAIT_PLANING:
      return 'Ожидает планирования';
    case WAIT_WORK_START:
      return 'Ожидает взятия в работу';
    case IN_WORK:
      return 'В работе';
    case WAIT_CAR_RETURN:
      return 'Ожидает возврата авто';
    case WAIT_PAYMENT:
      return 'Ожидает оплаты';
    case DONE:
      return 'Выполнен';
    case REJECT:
      return 'Отказ';
    default:
      return '';
  }
};

export const getOrdeStatusTypes = () => [
  WAIT_CAR_ARRIVE,
  WAIT_WORK_ADD,
  WAIT_PLANING,
  WAIT_WORK_START,
  WAIT_CAR_RETURN,
  WAIT_PAYMENT,
  IN_WORK,
  DONE,
  REJECT,
];
