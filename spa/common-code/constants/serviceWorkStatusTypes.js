export const SERVICE_WORK_WAIT_WORK_START = 'SERVICE_WORK_WAIT_WORK_START';
export const SERVICE_WORK_IN_WORK = 'SERVICE_WORK_IN_WORK';
export const SERVICE_WORK_DONE = 'SERVICE_WORK_DONE';

export const getServiceWorkStatusTypeName = (type) => {
  switch (type) {
    case SERVICE_WORK_WAIT_WORK_START:
      return 'Ожидает взятия в работу';
    case SERVICE_WORK_IN_WORK:
      return 'В работе';
    case SERVICE_WORK_DONE:
      return 'Выполнено';
    default:
      return '';
  }
};

export const getServiceWorkStatusTypes = () => [
  SERVICE_WORK_WAIT_WORK_START,
  SERVICE_WORK_IN_WORK,
  SERVICE_WORK_DONE,
];
