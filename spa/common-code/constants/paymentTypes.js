export const CASH = 'CASH';
export const CARD = 'CARD';
export const POINT = 'POINT';

export const getPaymentTypeName = (type) => {
  switch (type) {
    case CASH:
      return 'Наличные';
    case CARD:
      return 'Безнал';
    case POINT:
      return 'Баллы';
    default:
      return '';
  }
};

export const getPaymentTypes = () => [CASH, POINT, CARD];
