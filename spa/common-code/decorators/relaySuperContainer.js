/* eslint-disable react/prop-types */
import React from 'react';
import Relay from 'react-relay/classic';
import isEmpty from 'lodash/isEmpty';
import isUndefined from 'lodash/isUndefined';

import ViewerRoute from '../relay/route/ViewerRoute';

const route = new ViewerRoute();

const relaySuperContainer = (fragments, initialVariables) => function decorator(WrappedComponent) {
  return class extends React.PureComponent {
    static defaultProps = {
      renderFailure: () => {},
      forceFetch: true,
      renderLoading: () => {},
      renderFetched: false,
      onReadyStateChange: () => {},
    };

    render() {
      const {
        dummy, renderFetched, forceFetch, renderLoading, onReadyStateChange, renderFailure,
        initialVariables: propsInitialVariables, ...rest
      } = this.props;

      if (dummy) {
        return <WrappedComponent {...rest} />;
      }

      let relayInitialVariables = initialVariables;
      if (!isEmpty(propsInitialVariables)) {
        relayInitialVariables = propsInitialVariables;
        if (!isEmpty(initialVariables)) {
          relayInitialVariables = Object.assign(initialVariables, propsInitialVariables);
        }
      }

      const containerArgs = {
        fragments,
      };

      if (!isUndefined(relayInitialVariables)) {
        containerArgs.initialVariables = relayInitialVariables;
      }

      const Container = Relay.createContainer(
        WrappedComponent,
        containerArgs,
      );

      let onRenderFetched = data => <Container {...data} {...rest} />;
      if (renderFetched) {
        onRenderFetched = renderFetched;
      }

      return (
        <Relay.RootContainer
          Component={Container}
          route={route}
          forceFetch={forceFetch}
          renderLoading={renderLoading}
          onReadyStateChange={onReadyStateChange}
          renderFetched={onRenderFetched}
          renderFailure={renderFailure}
        />
      );
    }
  };
};

export default relaySuperContainer;
