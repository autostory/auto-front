import Relay from 'react-relay/classic';

const relayContainer = (fragments, initialVariables) => function decorator(component) {
  return Relay.createContainer(component, { fragments, initialVariables });
};

export default relayContainer;
