#!/usr/bin/env babel-node --optional es7.asyncFunctions
/* eslint-disable no-console */

import fs from 'fs';
import path from 'path';
import https from 'https';
import { introspectionQuery } from 'graphql/utilities';

const options = {
  host: 'localhost',
  port: 443,
  path: '/graphql',
  method: 'POST',
  rejectUnauthorized: false,
  requestCert: true,
  agent: false,
  headers: {
    'Content-Type': 'application/json',
  },
};
const req = https.request(options, (response) => {
  const data = [];
  response.setEncoding('utf8');
  response.on('data', (chunk) => {
    data.push(chunk);
  });
  response.on('end', () => {
    const result = JSON.parse(data.join(''));
    if (result.errors) {
      console.error(
        'Error introspecting schema: ',
        JSON.stringify(result.errors, null, 2),
      );
    } else {
      fs.writeFileSync(
        path.join(__dirname, './../../graphql/schema.json'),
        JSON.stringify(result, null, 2),
      );
      console.log('GraphQL schema has been updated');
    }
  });
});

req.on('error', (e) => {
  console.log(`problem with request: ${e.message}`);
});

req.write(JSON.stringify({ query: introspectionQuery }));
req.end();
