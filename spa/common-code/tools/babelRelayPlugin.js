var babelRelayPlugin = require('babel-relay-plugin');
var schema = require('../../graphql/schema.json');

module.exports = babelRelayPlugin(schema.data, {
  abortOnError: true,
});
