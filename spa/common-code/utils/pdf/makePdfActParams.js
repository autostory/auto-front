import format from 'date-fns/esm/format';
import concat from 'lodash/concat';
import get from 'lodash/get';
import map from 'lodash/map';
import rubles from 'rubles';
import { fromGlobalId } from '../../relay/globalIdUtils';

export const makePdfActParams = ({ order }) => {
  const orderId = fromGlobalId(order.id).id;
  const orderCreateDate = new Date().setTime(order.makeAt.timestamp * 1000);

  const {
    firm, client, serviceWorks, totalBill, serviceMaterials,
  } = order;

  const firmName = firm.name;
  const clientName = client.fullName;

  let i = 0;
  const serviceWorksData = map(serviceWorks, (serviceWork) => {
    const text = `${get(serviceWork, 'action.name')} ${get(serviceWork, 'sparePartNode.name')} ${get(serviceWork, 'note')}`;

    return [
      i += 1,
      text,
      serviceWork.amount,
      serviceWork.costPerUnit,
      serviceWork.discountPercent ? `${serviceWork.discountPercent} %` : '',
      serviceWork.totalCost,
    ];
  });

  let j = 0;
  const serviceMaterialData = map(serviceMaterials, serviceMaterial => [
    j += 1,
    serviceMaterial.name,
    serviceMaterial.amount,
    get(serviceMaterial, 'unitMeasureType.name', ''),
    serviceMaterial.costPerUnit,
    serviceMaterial.discountPercent ? `${serviceMaterial.discountPercent} %` : '',
    serviceMaterial.totalCost,
  ]);

  const serviceWorksHeader = ['№', 'Наименование работ, услуг', 'Кол-во', 'Цена, руб.', 'Скидка, %', 'Сумма, руб'];
  const serviceMaterialHeader = [
    '№',
    'Наименование материалов',
    'Кол-во',
    'Ед.',
    'Цена, руб',
    'Скидка, %',
    'Сумма, руб',
  ];
  const serviceWorksTable = concat([serviceWorksHeader], serviceWorksData);
  const serviceMaterialsTable = concat([serviceMaterialHeader], serviceMaterialData);

  return {
    pageSize: 'A4',
    content: [
      {
        text: `Акт выполненных работ по заказу SW-${orderId} от ${format(orderCreateDate, 'DD.MM.YYYY')} г.`,
        style: 'header',
      },
      {
        table: {
          headerRows: 1,
          widths: ['100%'],
          body: [
            [' '],
          ],
        },
        layout: 'headerLineOnly',
      },
      `Исполнитель: ${firmName}`,
      `Заказчик: ${clientName}`,
      ' ',
      { text: 'Оказанные работы и услуги', style: 'subheader' },
      {
        table: {
          headerRows: 1,
          widths: [15, 240, 50, 50, 50, 50],
          body: serviceWorksTable,
        },
      },
      serviceMaterials.length > 0 ? ' ' : '',
      serviceMaterials.length > 0 ? { text: 'Используемые материалы', style: 'subheader' } : '',
      serviceMaterials.length > 0 ? {
        table: {
          headerRows: 1,
          widths: [15, 210, 40, 30, 50, 50, 50],
          body: serviceMaterialsTable,
        },
      } : '',
      ' ',
      {
        text: `Итого: ${totalBill} руб.`,
        bold: true,
        alignment: 'right',
      },
      ' ',
      serviceMaterials.length > 0 ?
        `Всего оказано услуг ${serviceWorks.length} и использовано материалов ${serviceMaterials.length} на сумму ${totalBill} руб.`
        :
        `Всего оказано услуг ${serviceWorks.length} на сумму ${totalBill} руб.`,
      `(${rubles.rubles(totalBill)})`,
      ' ',
      'Выше перечисленные услуги выполнены полностью и в срок. Заказчик претензий по объему, качеству и срокам оказания услуг не имеет.',
      ' ',
      ' ',
      ' ',
      {
        table: {
          headerRows: 1,
          widths: ['50%', '50%'],
          body: [
            ['Исполнитель', 'Заказчик'],
            [' ', ' '],
            ['______________________', '______________________'],
          ],
        },
        layout: 'noBorders',
      },
    ],
  };
};
