import getViewportSize from './getViewportSize';

const updateVhCSSProp = () => {
  const { viewPortHeight } = getViewportSize();
  const vh = viewPortHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
};

export default updateVhCSSProp;

