const isBrowserSupportCSSCustomProps = () => window.CSS && window.CSS.supports('color', 'var(--primary)');

export default isBrowserSupportCSSCustomProps;
