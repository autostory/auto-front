import VMasker from 'vanilla-masker';

const PHONE_MASK = '(999) 999 99 99';

export default (text) => {
  if (!text) {
    return text;
  }

  return `+7 ${VMasker.toPattern(text, PHONE_MASK)}`;
};
