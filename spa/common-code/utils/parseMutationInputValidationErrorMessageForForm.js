import map from 'lodash/map';
import isEmpty from 'lodash/isEmpty';
import parseMutationInputValidationErrorMessage from './parseMutationInputValidationErrorMessage';

export default (message) => {
  const errorsArray = parseMutationInputValidationErrorMessage(message);

  const errors = {};

  if (isEmpty(errorsArray)) {
    return errors;
  }

  map(errorsArray, (error, key) => {
    errors[key] = error.join(', ');
  });

  return errors;
};
