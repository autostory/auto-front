import isArray from 'lodash/isArray';

export default (message) => {
  const errors = {};
  // eslint-disable-next-line array-callback-return
  message.map((errorText) => {
    if (!isArray(errors[errorText.fieldName])) {
      errors[errorText.fieldName] = [];
    }

    errors[errorText.fieldName].push(errorText.text);
  });

  return errors;
};
