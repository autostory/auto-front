import cookie from 'react-cookie';
import superagent from 'superagent';
import noCache from 'superagent-no-cache';
import isUndefined from 'lodash/isUndefined';
import trim from 'lodash/trim';
import isFunction from 'lodash/isFunction';
import saveTokenInfoToCookie from './saveTokenInfoToCookie';

// TODO: Переписать на relay
class AuthenticationClient {
  constructor() {
    this.grantTypes = {
      authenticate: 'password',
      refresh: 'refresh_token',
    };
    this.clientId = 'ah-frontend';
    this.authenticateUrl = '/oauth';
    this.refreshUrl = '/oauth';
    this.checkPeriod = 5 * 60 * 1000;

    if (!isUndefined(cookie.load('refreshToken')) && !isUndefined(cookie.load('refreshTokenTime'))) {
      this.initAutoTokenRefresh(cookie.load('refreshToken'), cookie.load('refreshTokenTime'));
    }
  }

  authenticate = (username, password, onSuccess, onError) => {
    const request = superagent.post(this.authenticateUrl);
    request.set('Content-Type', 'application/json');
    request.use(noCache);
    request.send({
      grant_type: this.grantTypes.authenticate,
      client_id: this.clientId,
      username: trim(username),
      password,
    });

    request.end((err, res) => {
      if (err) {
        if (isFunction(onError)) {
          onError.apply(null, [res, err]);
        }
      } else {
        const result = JSON.parse(res.text);
        saveTokenInfoToCookie(result.access_token, result.refresh_token, result.expires_in);

        if (isFunction(onSuccess)) {
          onSuccess.apply(null, [res]);
        }
      }
    });
  };

  doRefreshToken = (refreshToken, onSuccess, onError) => {
    if (!isUndefined(refreshToken)) {
      const request = superagent.post(this.refreshUrl);
      request.set('Content-Type', 'application/json');
      request.use(noCache);
      request.send({
        grant_type: this.grantTypes.refresh,
        client_id: this.clientId,
        refresh_token: refreshToken,
      });

      request.end((err, res) => {
        if (err) {
          if (
            err.status === 400
            && (
              JSON.parse(err.response.text).detail === 'Refresh token has expired'
              || JSON.parse(err.response.text).detail === 'Invalid refresh token'
            )
          ) {
            // Refresh token has expired or somehow invalid, we need to re-login in Sweet.
            this.logout();
            window.top.location.reload(); // TODO: Делать обновление сторы релей, т.е. переполучить viewer
          }
          if (isFunction(onError)) {
            onError.apply(null, [res, err]);
          }
        } else {
          const result = JSON.parse(res.text);
          saveTokenInfoToCookie(result.access_token, result.refresh_token, result.expires_in);

          if (isFunction(onSuccess)) {
            onSuccess.apply(null, [res]);
          }
        }
      });
    }
  };

  logout = () => {
    // TODO: Нужно ещё revoke токена делать
    this.deleteAuthCookie();
  };

  deleteAuthCookie = () => {
    cookie.remove('accessToken', '/');
    cookie.remove('refreshToken', '/');
    cookie.remove('refreshTokenTime', '/');
    cookie.remove('expiresIn', '/');
  };

  initAutoTokenRefresh(refreshToken, refreshTokenTime, tokenSaver) {
    const now = new Date().getTime();
    const refreshTokenTimeStamp = new Date(refreshTokenTime).getTime();

    if (now >= refreshTokenTimeStamp) {
      this.doRefreshToken(
        refreshToken,
        this.refreshTokenTimeoutStarter.bind(this, refreshToken, refreshTokenTime, tokenSaver),
        this.refreshTokenTimeoutStarter.bind(this, refreshToken, refreshTokenTime, tokenSaver),
      );
    } else {
      this.refreshTokenTimeoutStarter(refreshToken, refreshTokenTime, tokenSaver);
    }
  }

  refreshTokenTimeoutStarter(refreshToken, refreshTokenTime, tokenSaver) {
    setTimeout(() => {
      this.initAutoTokenRefresh(refreshToken, refreshTokenTime, tokenSaver);
    }, this.checkPeriod);
  }
}

export default new AuthenticationClient();
