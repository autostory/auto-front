import cookie from 'react-cookie';

const saveTokenInfoToCookie = (accessToken, refreshToken, expiresIn) => {
  const cookieLifeTime = expiresIn * 1000;

  cookie.save(
    'accessToken', accessToken,
    {
      expires: new Date(new Date().getTime() + cookieLifeTime),
      path: '/',
    },
  );

  cookie.save(
    'refreshToken', refreshToken,
    {
      expires: new Date(new Date().getTime() + cookieLifeTime),
      path: '/',
    },
  );

  cookie.save(
    'refreshTokenTime', new Date(new Date().getTime() + (cookieLifeTime / 2)).getTime(),
    {
      expires: new Date(new Date().getTime() + cookieLifeTime),
      path: '/',
    },
  );

  cookie.save(
    'expiresIn', new Date(new Date().getTime() + cookieLifeTime),
    {
      expires: new Date(new Date().getTime() + cookieLifeTime),
      path: '/',
    },
  );
};

export default saveTokenInfoToCookie;
