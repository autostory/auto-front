// import { typography } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';

const styles = {
  navigation: {
    fontSize: 15,
    // fontWeight: typography.fontWeightLight,
    // color: grey['600'],
    paddingBottom: 8,
    display: 'block',
  },
  title: {
    fontSize: 24,
    // fontWeight: typography.fontWeightLight,
    marginBottom: 20,
  },
  paper: {
    padding: 30,
  },
  clear: {
    clear: 'both',
  },
};

export default styles;
