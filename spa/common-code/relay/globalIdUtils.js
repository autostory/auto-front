import { Base64 } from 'js-base64';
import isEmpty from 'lodash/isEmpty';

export function toGlobalId(type, id) {
  return Base64.encode([type, id].join(':'));
}

export function fromGlobalId(globalId) {
  const unbasedGlobalId = Base64.decode(globalId);
  const delimiterPos = unbasedGlobalId.indexOf(':');
  return {
    type: unbasedGlobalId.substring(0, delimiterPos),
    id: unbasedGlobalId.substring(delimiterPos + 1),
  };
}

export function isRelayId(globalId) {
  const parts = fromGlobalId(globalId);

  return !isEmpty(parts.id) && !isEmpty(parts.type);
}
