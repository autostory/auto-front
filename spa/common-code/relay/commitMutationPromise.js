import Relay from 'react-relay/classic';

const commitMutationPromise = (Mutation, data) => {
  return new Promise((resolve, reject) => {
    Relay.Store.commitUpdate(new Mutation(data), {
      onSuccess: (transaction) => {
        resolve(transaction);
      },
      onFailure: (transaction) => {
        reject(transaction);
      },
    });
  });
};

export default commitMutationPromise;
