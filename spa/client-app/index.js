import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import { reducer as reduxFormReducer } from 'redux-form';
import CssBaseline from '@material-ui/core/CssBaseline';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import DateFnsUtils from 'material-ui-pickers/utils/date-fns-utils';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import ruLocale from 'date-fns/locale/ru';

import Relay from 'react-relay/classic';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { composeWithDevTools } from 'redux-devtools-extension';
import { ConnectedRouter, routerReducer, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import isBrowserSupportCSSCustomProps from '../common-code/utils/isBrowserSupportCSSCustomProps';
import updateVhCSSProp from '../common-code/utils/updateVhCSSProp';

import asyncComponent from './../common-code/AsyncComponent';

// const Private = asyncComponent(() => import('./scripts/containers/Private'));
// const Public = asyncComponent(() => import('./scripts/containers/Public'));

import registerServiceWorker from './registerServiceWorker';
import AutoLoginPage from './scripts/pages/AutoLoginPage';
import WaitAutoLoginPage from './scripts/pages/WaitAutoLoginPage';

const ClientCarPage = asyncComponent(() => import('./scripts/pages/ClientCarPage'));
const ClientOrderPage = asyncComponent(() => import('./scripts/pages/ClientOrderPage'));
const PointLogPage = asyncComponent(() => import('./scripts/pages/PointLogPage'));

// import ClientCarPage from './scripts/pages/ClientCarPage';
// import ClientOrderPage from './scripts/pages/ClientOrderPage';
// import PointLogPage from './scripts/pages/PointLogPage';
import reducers from './scripts/reduxStores';

const IndexPage = asyncComponent(() => import('./scripts/pages/IndexPage'));

// import IndexPage from './scripts/pages/IndexPage';

const LoginPage = asyncComponent(() => import('./scripts/pages/auth/Login/LoginPage'));
const LogoutPage = asyncComponent(() => import('./scripts/pages/auth/LogoutPage'));
const PasswordRestorePage = asyncComponent(() => import('./scripts/pages/auth/PasswordRestorePage'));

// import LoginPage from './scripts/pages/auth/Login/LoginPage';
// import LogoutPage from './scripts/pages/auth/LogoutPage';
// import PasswordRestorePage from './scripts/pages/auth/PasswordRestorePage';

const NoMatch = asyncComponent(() => import('./scripts/pages/NoMatch'));

// import NoMatch from './scripts/pages/NoMatch';
import PrivateRoute from './scripts/Route/PrivateRoute';
import OnlyForGuestRoute from './scripts/Route/OnlyForGuestRoute';

// This one is already present in admin-on-rest Layout, but it seems it does nothing if called after the initial ReactDOM.render()
// @link https://github.com/callemall/material-ui/issues/4670#issuecomment-2350319175
try {
  injectTapEventPlugin();
} catch (e) {
  // do nothing
}

const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const middleware = routerMiddleware(history);

const composeEnhancers = composeWithDevTools({
  // Specify here name, actionsBlacklist, actionsCreators and other options if needed
});

// Add the reducer to your store on the `router` key
// Also apply our middleware for navigating
const store = createStore(
  combineReducers({
    ...reducers,
    router: routerReducer,
    form: reduxFormReducer,
  }),
  composeEnhancers(applyMiddleware(...middleware)),
);

Relay.injectNetworkLayer(new Relay.DefaultNetworkLayer('/graphql', {
  credentials: 'same-origin',
  fetchTimeout: 30000,
  retryDelays: [],
}));

// if (process.env.NODE_ENV !== 'production') {
//   const { whyDidYouUpdate } = require('why-did-you-update');
//   whyDidYouUpdate(React, { include: [/^CreateNewOrderStepper/] });
// }

ReactDOM.render(
  <MuiPickersUtilsProvider utils={DateFnsUtils} locale={ruLocale}>
    <Provider store={store}>
      <ConnectedRouter
        history={history}
        environment={Relay.Store}
      >
        <React.Fragment>
          <CssBaseline />
          <Switch>
            <PrivateRoute exact path="/" component={IndexPage} />
            <PrivateRoute exact path="/point-log" component={PointLogPage} />
            <PrivateRoute exact path="/car/:carId" component={ClientCarPage} />
            <PrivateRoute exact path="/order/:orderId" component={ClientOrderPage} />
            <PrivateRoute exact path="/logout" component={LogoutPage} />
            <OnlyForGuestRoute exact path="/login" component={LoginPage} />
            <OnlyForGuestRoute exact path="/password-restore" component={PasswordRestorePage} />
            <OnlyForGuestRoute exact path="/auto-login" component={AutoLoginPage} />
            <Route exact path="/wait-auto-login" component={WaitAutoLoginPage} />

            <PrivateRoute component={NoMatch} />
          </Switch>
        </React.Fragment>
      </ConnectedRouter>
    </Provider>
  </MuiPickersUtilsProvider>,
  document.getElementById('app'),
);

document.addEventListener('DOMContentLoaded', () => {
  if (isBrowserSupportCSSCustomProps()) {
    updateVhCSSProp();
    window.addEventListener('resize', updateVhCSSProp);
  }
});

if (process.env.NODE_ENV === 'production') {
  registerServiceWorker();
}
