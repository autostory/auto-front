import React from 'react';
import { Route } from 'react-router-dom';
import EnsureNotLoggedInApp from '../components/Auth/EnsureNotLoggedInApp';

const OnlyForGuestRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <EnsureNotLoggedInApp>
        <Component {...props} />
      </EnsureNotLoggedInApp>
    )}
  />
);

export default OnlyForGuestRoute;
