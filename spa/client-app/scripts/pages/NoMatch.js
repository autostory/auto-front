import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class NoMatch extends Component {
  render() {
    return (
      <div style={{ padding: 24 }}>
        <h1>404! Нет такой страницы.</h1>
        <Link to="/">На главную страницу</Link>
      </div>
    );
  }
}

export default NoMatch;
