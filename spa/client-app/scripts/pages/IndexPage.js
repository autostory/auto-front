import React, { Component } from 'react';
import MainPageContent from '../components/index/MainPageContent';
import PageBase from '../components/main/PageBase';

class IndexPage extends Component {
  render() {
    return (
      <PageBase title="Моя онлайн сервисная книжка">
        <MainPageContent />
      </PageBase>
    );
  }
}

export default IndexPage;
