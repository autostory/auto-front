import React, { Component } from 'react';
import PageBase from '../components/main/PageBase';
import ClientCarPageContent from '../components/ClientCar/ClientCarPageContent';

class PointLogPage extends Component {
  render() {
    const { match } = this.props;

    const { carId } = match.params;

    return (
      <PageBase title="Мой автомобиль" backTo="/">
        <ClientCarPageContent initialVariables={{ carId }} carId={carId} />
      </PageBase>
    );
  }
}

export default PointLogPage;
