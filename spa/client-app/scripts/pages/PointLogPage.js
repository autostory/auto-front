import React, { Component } from 'react';
import PageBase from '../components/main/PageBase';
import PointLogPageContent from '../components/PointLog/PointLogPageContent';

class PointLogPage extends Component {
  render() {
    return (
      <PageBase title="История моих баллов" backTo="/">
        <PointLogPageContent />
      </PageBase>
    );
  }
}

export default PointLogPage;
