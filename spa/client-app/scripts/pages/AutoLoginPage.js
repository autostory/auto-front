import isNull from 'lodash/isNull';
import isUndefined from 'lodash/isUndefined';
import React from 'react';
import withProps from 'recompose/withProps';
import Relay from 'react-relay/classic';
import { withRouter } from 'react-router';

import relaySuperContainer from '../../../common-code/decorators/relaySuperContainer';
import saveTokenInfoToCookie from '../../../common-code/utils/saveTokenInfoToCookie';
import FullScreenPreLoader from './../components/basic/FullScreenPreLoader';
import AutoLoginMutation from './../relay/mutation/AutoLoginMutation';
import autoLogin from './../components/autoLogin/autoLogin';

@withProps({ renderLoading: () => <FullScreenPreLoader /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id
          isAuthenticated
          ${AutoLoginMutation.getFragment('viewer')}
      }
  `,
})
@withRouter
class AutoLoginPageContent extends React.PureComponent {
  componentDidMount() {
    const { viewer, history, location } = this.props;

    const { isAuthenticated } = viewer;

    if (isAuthenticated) {
      history.replace('/');

      return;
    }

    let token;
    const query = new URLSearchParams(location.search);
    if (!isNull(query.get('token'))) {
      token = query.get('token');
    }

    if (isUndefined(token) || token === '') {
      history.replace('/');

      return;
    }

    const values = { token };

    autoLogin({ viewer, values }).then(({ accessToken, refreshToken, expiresIn }) => {
      saveTokenInfoToCookie(accessToken, refreshToken, expiresIn);
      setTimeout(() => {
        history.replace('/wait-auto-login');
      }, 1);
    }).catch(() => {
      history.replace('/');
    });
  }

  componentDidUpdate(nextProps) {
    const { viewer, history } = nextProps;

    if (viewer.isAuthenticated) {
      history.replace('/');
    }
  }

  render() {
    return <FullScreenPreLoader />;
  }
}

export default AutoLoginPageContent;
