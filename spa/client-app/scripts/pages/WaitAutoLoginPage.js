import React from 'react';
import withProps from 'recompose/withProps';
import Relay from 'react-relay/classic';
import { withRouter } from 'react-router';

import relaySuperContainer from '../../../common-code/decorators/relaySuperContainer';
import FullScreenPreLoader from './../components/basic/FullScreenPreLoader';

@withProps({
  renderLoading: () => <FullScreenPreLoader />,
  forceFetch: true,
})
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id
          isAuthenticated
          fullName #Очень важно запросить что-то иначе запрос совпадает с предыдущем и возьмется из стора что пользователь не авторизован
      }
  `,
})
@withRouter
class WaitAutoLoginPage extends React.PureComponent {
  componentDidMount() {
    const { viewer, history } = this.props;
    const { isAuthenticated } = viewer;

    if (isAuthenticated) {
      history.replace('/');

      return;
    }

    history.replace('/login');
  }

  render() {
    return <FullScreenPreLoader />;
  }
}

export default WaitAutoLoginPage;

