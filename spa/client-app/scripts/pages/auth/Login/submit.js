import { SubmissionError } from 'redux-form';
import get from 'lodash/get';
import LoginMutation from '../../../relay/mutation/LoginMutation';
import commitMutationPromise from '../../../../../common-code/relay/commitMutationPromise';

function submit({ viewer, values }) {
  const onFailure = (errors) => {
    console.log('onFailure errors', errors);
    if (errors && errors instanceof SubmissionError) {
      throw errors;
    }

    alert('Произошла ошибка. Повторите попытку позже.');
  };

  const onSuccess = (response) => {
    const tokenInfo = get(response, 'login.tokenInfo');
    const accessToken = get(tokenInfo, 'access_token', null);
    const expiresIn = get(tokenInfo, 'expires_in', null);
    const refreshToken = get(tokenInfo, 'refresh_token', null);

    if (accessToken && expiresIn && refreshToken) {
      return {
        accessToken,
        expiresIn,
        refreshToken,
      };
    }

    throw new SubmissionError({
      global: 'Не правильный E-mail/телефон или пароль',
    });
  };

  return commitMutationPromise(LoginMutation, { viewer, input: values })
    .then(onSuccess)
    .catch(onFailure);
}

export default submit;
