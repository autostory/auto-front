import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';

import { TextField } from 'redux-form-material-ui';
import { reduxForm, Field } from 'redux-form';

import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from '@material-ui/core/InputAdornment';

import validate from './validate';

export const FORM_NAME = 'LoginForm';

const styles = theme => ({
  textInput: {
    marginTop: theme.spacing.unit,
  },
});

const rawStyle = {
  container: {
    width: 320,
    minHeight: '100%',
    display: 'flex',
    flexDirection: 'column',
    minWidth: 320,
    overflow: 'auto',
    alignItems: 'center',
    justifyContent: 'space-around',
    padding: 12,
  },
  root: {
    display: 'flex',
    justifyContent: 'center',
    minHeight: '100%',
  },
  flatButton: {
    color: '#727272',
    textTransform: 'none',
  },
  loginBtnContainer: {
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 10,
  },
  loginButton: {
    color: '#383737',
  },
  header: {
    width: '100%',
    flex: 1,
    flexBasis: 82,
    display: 'inline-flex',
    alignItems: 'flex-end',
  },
  footer: {
    width: '100%',
    textAlign: 'right',
    flex: 1,
    flexBasis: 50,
    display: 'inline-flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
};

const inputProps = {
  maxLength: 250,
};

@withStyles(styles)
class LoginForm extends React.Component {
  state = {
    showPassword: false,
  };

  onInputKeyUp = (event) => {
    if (event.key === 'Enter') {
      this.handleLoginButtonClick();
    }
  };

  handleClickShowPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  handleLoginButtonClick = () => {
    const { handleSubmit } = this.props;

    handleSubmit();
  };

  render() {
    const { classes, submitting, dirty } = this.props;
    const { showPassword } = this.state;

    let passwordTypes = 'password';
    if (this.state.showPassword === true) {
      passwordTypes = 'text';
    }

    return (
      <form autoComplete="off">
        <Field
          required
          component={TextField}
          label="E-mail или телефон"
          className={classes.textInput}
          autoComplete="off"
          fullWidth
          name="login"
          InputProps={inputProps}
          disabled={submitting}
          onKeyUp={this.onInputKeyUp}
        />
        <Field
          component={TextField}
          required
          className={classes.textInput}
          autoComplete="off"
          label="Пароль"
          name="password"
          fullWidth
          type={passwordTypes}
          disabled={submitting}
          onKeyUp={this.onInputKeyUp}
          InputProps={{
            maxLength: 250,
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  aria-label="Toggle password visibility"
                  onClick={this.handleClickShowPassword}
                  onMouseDown={this.handleMouseDownPassword}
                >
                  {showPassword && <VisibilityOff />}
                  {!showPassword && <Visibility />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <div style={rawStyle.loginBtnContainer}>
          {!submitting && (
            <Button
              variant="raised"
              fullWidth
              color="primary"
              style={rawStyle.loginButton}
              onClick={this.handleLoginButtonClick}
            >
              Войти
            </Button>
          )}
          {submitting && (
            <CircularProgress size={31} />
          )}
        </div>
      </form>
    );
  }
}

export default reduxForm({
  form: FORM_NAME,
  initialValues: {
    login: '',
    password: '',
  },
  validate,
})(LoginForm);
