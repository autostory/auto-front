import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import Relay from 'react-relay/classic';
import { withStyles } from '@material-ui/core/styles';
import isEqual from 'lodash/isEqual';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import Button from '@material-ui/core/Button';
import Help from '@material-ui/icons/Help';
import Typography from '@material-ui/core/Typography';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { getFormSubmitErrors, clearSubmitErrors } from 'redux-form';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';

import saveTokenInfoToCookie from '../../../../../common-code/utils/saveTokenInfoToCookie';
import LoginMutation from '../../../relay/mutation/LoginMutation';
import relayContainer from '../../../../../common-code/decorators/relayContainer';
import themeDefault from '../../../mui-theme/DefaultTheme';
import LoginForm, { FORM_NAME } from './LoginForm';
import submit from './submit';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    color: '#ffffff',
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  textInput: {
    marginTop: theme.spacing.unit,
  },
  close: {
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4,
  },
  container: {
    width: 320,
    minHeight: 'calc(var(--vh, 1vh) * 100)',
    display: 'flex',
    flexDirection: 'column',
    minWidth: 320,
    overflow: 'auto',
    alignItems: 'center',
    justifyContent: 'space-around',
    padding: 12,
  },
});

const rawStyle = {
  root: {
    display: 'flex',
    justifyContent: 'center',
    minHeight: '100%',
  },
  flatButton: {
    color: '#727272',
    padding: 0,
    textTransform: 'none',
  },
  checkRemember: {
    style: {
      float: 'left',
      maxWidth: 180,
      paddingTop: 5,
    },
    labelStyle: {},
    iconStyle: {},
  },
  loginBtnContainer: {
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 10,
  },
  loginButton: {
    color: 'white',
  },
  header: {
    width: '100%',
    flex: 1,
    flexBasis: 82,
    display: 'inline-flex',
    alignItems: 'flex-end',
  },
  footer: {
    width: '100%',
    textAlign: 'right',
    flex: 1,
    flexBasis: 50,
    display: 'inline-flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
};

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id,
          ${LoginMutation.getFragment('viewer')},
      }
  `,
})
@connect(
  state => ({
    submitErrors: getFormSubmitErrors(FORM_NAME)(state),
  }),
  dispatch => ({
    clearSubmitErrors: () => dispatch(clearSubmitErrors(FORM_NAME)),
  }),
)
@withRouter
@withStyles(styles)
class LoginPage extends React.Component {
  state = {
    showSnackBar: false,
  };

  componentWillReceiveProps(nextProps) {
    if (!isEmpty(nextProps.submitErrors) && !isEqual(nextProps.submitErrors, this.props.submitErrors)) {
      this.setState({
        showSnackBar: true,
      });
    }
  }

  onSubmit = (values) => {
    const { viewer } = this.props;

    return submit({ viewer, values }).then(({ accessToken, expiresIn, refreshToken }) => {
      saveTokenInfoToCookie(accessToken, refreshToken, expiresIn);
      window.location.reload();
    });
  };

  hideErrorMessage = () => {
    const { clearSubmitErrors } = this.props;

    this.setState({
      showSnackBar: false,
    }, clearSubmitErrors);
  };

  render() {
    const { classes, submitErrors } = this.props;
    const { showSnackBar } = this.state;

    const error = get(submitErrors, 'global', null);

    return (
      <MuiThemeProvider theme={themeDefault}>
        <div style={rawStyle.root}>
          <div className={classes.container}>
            <Snackbar
              open={showSnackBar}
              message={error}
              action={[
                <IconButton
                  key="close"
                  aria-label="Close"
                  color="inherit"
                  className={classes.close}
                  onClick={this.hideErrorMessage}
                >
                  <CloseIcon />
                </IconButton>,
              ]}
              autoHideDuration={2000}
              onClose={this.hideErrorMessage}
            />
            <Typography variant="display1" style={rawStyle.header}>
              Моя онлайн<br />
              сервисная книжка
            </Typography>
            <div>
              <LoginForm onSubmit={this.onSubmit} />
            </div>

            <div style={rawStyle.footer}>
              <Button
                color="primary"
                component={Link}
                to="/password-restore"
                className={classes.button}
                style={rawStyle.flatButton}
              >
                <Help className={classes.leftIcon} /> я забыл пароль
              </Button>
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default LoginPage;
