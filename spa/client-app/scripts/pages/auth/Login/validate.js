function validate(values) {
  const errors = {};
  if (values.password === '') {
    errors.password = 'Введите пароль';
  }

  if (values.login === '') {
    errors.login = 'Введите E-mail или телефон';
  }

  return errors;
}

export default validate;
