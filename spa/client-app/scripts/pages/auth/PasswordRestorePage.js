import React from 'react';
import get from 'lodash/get';
import { withStyles } from '@material-ui/core/styles/index';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import Relay from 'react-relay/classic';
import isNull from 'lodash/isNull';
import isUndefined from 'lodash/isUndefined';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import grey from '@material-ui/core/colors/grey';
import { Link } from 'react-router-dom';
import phoneFormat from '../../../../common-code/utils/text/phoneFormat';

import themeDefault from '../../mui-theme/DefaultTheme';
import SendPasswordRestoreMessage from '../../relay/mutation/SendPasswordRestoreMessage';
import relayContainer from '../../../../common-code/decorators/relayContainer';
import PasswordRestoreUsingSecretKey from '../../relay/mutation/PasswordRestoreUsingSecretKey';

const styles = theme => ({
  button: {
    color: '#383737',
  },
});

const rawStyle = {
  centeredContainer: {
    minWidth: 320,
    maxWidth: 400,
    height: 'auto',
    position: 'absolute',
    top: '20%',
    left: 0,
    right: 0,
    margin: 'auto',
  },
  paper: {
    padding: 20,
    overflow: 'auto',
  },
  restoreBtn: {
    textAlign: 'center',
    marginTop: 20,
  },
  buttonsDiv: {
    textAlign: 'center',
    padding: 10,
  },
  flatButton: {
    color: grey['500'],
  },
};

@relayContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id,
          ${SendPasswordRestoreMessage.getFragment('viewer')},
          ${PasswordRestoreUsingSecretKey.getFragment('viewer')},
      }
  `,
})
@withRouter
@withStyles(styles)
class PasswordRestorePage extends React.Component {
  static propTypes = {
    history: ReactRouterPropTypes.history,
  };

  static defaultProps = {
    history: null,
  };

  state = {
    email: '',
    smsCode: '',
    pendingRequest: false,
  };

  onSMSCodeInput = (event) => {
    this.setState({
      smsCode: event.target.value,
    });
  };

  onEmailInput = (event) => {
    this.setState({
      email: event.target.value,
    });
  };

  onRestoreClick = () => {
    if (this.state.pendingRequest) {
      return;
    }

    const { email } = this.state;
    const { viewer, history } = this.props;

    const sendPasswordRestoreMessage = new SendPasswordRestoreMessage({
      emailOrPhone: email,
      viewer,
    });

    const onFailure = () => {
      this.setState({
        pendingRequest: false,
      });

      // TODO: Переделать на SnackBar
      alert('Указан не правильный e-mail или пароль.');
    };

    const onSuccess = (response) => {
      this.setState({
        pendingRequest: false,
      });

      const messageSendBy = get(response, 'sendPasswordRestoreMessage.messageSendBy', null);

      if (!messageSendBy || messageSendBy === 'email') {
        alert('Было выслано письмо на указанный email. Сделуйте инструкциям из этого письма для восстановления пароля.');
        history.replace('/login');
      }

      if (!messageSendBy || messageSendBy === 'sms') {
        const token = get(response, 'sendPasswordRestoreMessage.token', null);
        history.push(`/password-restore/?token=${token}&phone=9130650101`);
      }
    };

    this.setState({ pendingRequest: true }, () => {
      this.props.relay.commitUpdate(sendPasswordRestoreMessage, {
        onFailure,
        onSuccess,
      });
    });
  };

  onPasswordResetClick = () => {
    if (this.state.pendingRequest) {
      return;
    }

    let token;
    const query = new URLSearchParams(location.search);
    if (!isNull(query.get('token'))) {
      token = query.get('token');
    }

    if (isUndefined(token)) {
      return;
    }

    const { viewer } = this.props;

    const passwordRestoreUsingSecretKey = new PasswordRestoreUsingSecretKey({
      token,
      viewer,
    });

    const onFailure = () => {
      this.setState({
        pendingRequest: false,
      });
      alert('Произошла ошибка при сбросе пароля. Повторите попытку позже.');
    };

    const onSuccess = (response) => {
      this.setState({
        pendingRequest: false,
      });

      if (!response.passwordRestoreUsingSecretKey.passwordChanged) {
        alert('Произошла ошибка при сбросе пароля. Повторите попытку позже.');

        return;
      }
      alert('Новый пароль был выслан письмом на указанный email.');
      const { history } = this.props;
      history.replace('/login');
    };

    this.setState({ pendingRequest: true }, () => {
      this.props.relay.commitUpdate(passwordRestoreUsingSecretKey, {
        onFailure,
        onSuccess,
      });
    });
  };

  onPasswordResetBySMSClick = () => {
    if (this.state.pendingRequest) {
      return;
    }

    let token;
    const query = new URLSearchParams(location.search);
    if (!isNull(query.get('token'))) {
      token = query.get('token');
    }

    if (isUndefined(token)) {
      return;
    }

    const { viewer } = this.props;
    const { smsCode } = this.state;

    const passwordRestoreUsingSecretKey = new PasswordRestoreUsingSecretKey({
      token,
      smsCode,
      viewer,
    });

    const onFailure = () => {
      this.setState({
        pendingRequest: false,
      });
      alert('Произошла ошибка при сбросе пароля. Повторите попытку позже.');
    };

    const onSuccess = (response) => {
      this.setState({
        pendingRequest: false,
      });

      if (!response.passwordRestoreUsingSecretKey.passwordChanged) {
        alert('Произошла ошибка при сбросе пароля. Возможно введен не правильный код из смс.');

        return;
      }
      alert('Новый пароль был выслан письмом вам по смс.');
      const { history } = this.props;
      history.replace('/login');
    };

    this.setState({ pendingRequest: true }, () => {
      this.props.relay.commitUpdate(passwordRestoreUsingSecretKey, {
        onFailure,
        onSuccess,
      });
    });
  };

  render() {
    const { classes } = this.props;

    let token;
    let email;
    let phone;
    const query = new URLSearchParams(location.search);
    if (!isNull(query.get('token'))) {
      token = query.get('token');
    }

    if (!isNull(query.get('email'))) {
      email = query.get('email');
    }

    if (!isNull(query.get('phone'))) {
      phone = query.get('phone');
    }

    if (!isUndefined(token) && !isUndefined(email)) {
      // Отображаем форму в которой пользователю надо нажать лишь одну кнопку и будет выслан новый email.
      return (
        <MuiThemeProvider theme={themeDefault}>
          <div style={rawStyle.centeredContainer}>
            <Paper style={rawStyle.paper}>
              <form>
                <h1>Сброс пароля</h1>
                <p>
                  Для того чтобы сбросить пароль у аккаунта<br />
                  с e-mail <b>{email}</b> нажмите кнопку ниже.<br />
                  Новый пароль будет отправлен на этот e-mail.
                </p>
                <p>
                  <b>
                    Если вы не запрашивали восстановления пароля, то просто
                    закройте эту страницу.
                  </b>
                </p>

                <div style={rawStyle.restoreBtn}>
                  {!this.state.pendingRequest && (
                    <Button
                      variant="raised"
                      color="primary"
                      className={classes.button}
                      onClick={this.onPasswordResetClick}
                    >
                      Сбросить пароль
                    </Button>
                  )}

                  {this.state.pendingRequest && <CircularProgress size={35} />}
                </div>
              </form>
            </Paper>
            <div style={rawStyle.buttonsDiv}>
              <Button
                color="primary"
                to="/login"
                component={Link}
                style={rawStyle.flatButton}
              >
                На страницу авторизации
              </Button>
            </div>
          </div>
        </MuiThemeProvider>
      );
    }

    if (!isUndefined(token) && !isUndefined(phone)) {
      return (
        <MuiThemeProvider theme={themeDefault}>
          <div style={rawStyle.centeredContainer}>
            <Paper style={rawStyle.paper}>
              <form>
                <h1>Сброс пароля</h1>
                <p>
                  Введите код отправленный на номер {phoneFormat(phone)}.
                </p>
                <TextField
                  value={this.state.smsCode}
                  fullWidth
                  inputProps={{
                    maxLength: 5,
                  }}
                  disabled={this.state.pendingRequest}
                  label="Код из SMS"
                  onChange={this.onSMSCodeInput}
                />
                <div style={rawStyle.restoreBtn}>
                  {!this.state.pendingRequest && (
                    <Button
                      variant="raised"
                      disabled={this.state.smsCode.length !== 5}
                      color="primary"
                      className={classes.button}
                      onClick={this.onPasswordResetBySMSClick}
                    >
                      Сбросить пароль
                    </Button>
                  )}

                  {this.state.pendingRequest && <CircularProgress size={35} />}
                </div>
              </form>
            </Paper>
            <div style={rawStyle.buttonsDiv}>
              <Button
                color="primary"
                to="/login"
                component={Link}
                style={rawStyle.flatButton}
              >
                На страницу авторизации
              </Button>
            </div>
          </div>
        </MuiThemeProvider>
      );
    }

    return (
      <MuiThemeProvider theme={themeDefault}>
        <div style={rawStyle.centeredContainer}>
          <div style={rawStyle.paper}>
            <form>
              <Typography variant="display1">Восстановление пароля</Typography>
              <br />
              <TextField
                value={this.state.email}
                fullWidth
                inputProps={{
                  maxLength: 250,
                }}
                disabled={this.state.pendingRequest}
                placeholder="Введите данные указанные при регистрации"
                label="E-mail или телефон"
                onChange={this.onEmailInput}
              />
              <div style={rawStyle.restoreBtn}>
                {!this.state.pendingRequest && (
                  <Button
                    variant="raised"
                    color="primary"
                    fullWidth
                    className={classes.button}
                    onClick={this.onRestoreClick}
                  >
                    Восстановить пароль
                  </Button>
                )}

                {this.state.pendingRequest && <CircularProgress size={35} />}
              </div>
            </form>
          </div>
          <div style={rawStyle.buttonsDiv}>
            <Button
              color="primary"
              to="/login"
              component={Link}
              style={rawStyle.flatButton}
            >
              На страницу авторизации
            </Button>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default PasswordRestorePage;
