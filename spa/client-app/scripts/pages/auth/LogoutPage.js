import React from 'react';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import AuthenticationClient from '../../../../common-code/utils/AuthenticationClient';
import FullScreenPreLoader from './../../components/basic/FullScreenPreLoader';

@withRouter
class LogoutPage extends React.Component {
  static propTypes = {
    history: ReactRouterPropTypes.history,
  };

  static defaultProps = {
    history: null,
  };

  componentDidMount = () => {
    const { history } = this.props;
    const promise = new Promise((resolve) => {
      AuthenticationClient.logout();
      resolve();
    });

    promise.then(() => {
      history.push('/login');
      window.location.reload();
    });
  };

  render() {
    return <FullScreenPreLoader />;
  }
}

export default LogoutPage;
