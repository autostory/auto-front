import React, { Component } from 'react';
import { fromGlobalId } from '../../../common-code/relay/globalIdUtils';
import PageBase from '../components/main/PageBase';
import ClientOrderPageContent from '../components/ClientOrder/ClientOrderPageContent';

class ClientOrderPage extends Component {
  render() {
    const { match, viewer } = this.props;

    const { orderId } = match.params;

    return (
      <PageBase title={`Заказ SW-${fromGlobalId(orderId).id}`} backTo="/">
        <ClientOrderPageContent initialVariables={{ orderId }} orderId={orderId} />
      </PageBase>
    );
  }
}

export default ClientOrderPage;
