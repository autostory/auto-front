import Relay from 'react-relay/classic';

class SendPasswordRestoreMessage extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { sendPasswordRestoreMessage }`;

  getVariables = () => ({
    emailOrPhone: this.props.emailOrPhone,
    type: 'client',
  });

  getFatQuery = () => Relay.QL`
      fragment on sendPasswordRestoreMessageMutationPayload {
          messageSendBy
          token
      }
  `;

  getConfigs = () => [{
    type: 'REQUIRED_CHILDREN',
    children: [
      Relay.QL`
          fragment on sendPasswordRestoreMessageMutationPayload {
              messageSendBy
              token
          }
      `,
    ],
  }];

  static fragments = {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
        }
    `,
  };
}

export default SendPasswordRestoreMessage;
