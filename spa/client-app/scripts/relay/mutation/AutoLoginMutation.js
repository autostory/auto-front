import Relay from 'react-relay/classic';

class AutoLoginMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { autoLogin }`;

  getVariables = () => ({
    token: this.props.input.token,
  });

  getFatQuery = () => Relay.QL`
      fragment on autoLoginMutationPayload {
          tokenInfo {
              access_token,
              expires_in,
              refresh_token
          }
      }
  `;

  getConfigs = () => [
    {
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
            fragment on autoLoginMutationPayload {
                tokenInfo {
                    access_token,
                    expires_in,
                    refresh_token
                }
            }
        `,
      ],
    },
  ];

  static fragments = {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
            isAuthenticated
        }
    `,
  };
}

export default AutoLoginMutation;
