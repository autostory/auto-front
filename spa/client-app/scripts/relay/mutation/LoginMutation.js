import Relay from 'react-relay/classic';

class LoginMutation extends Relay.Mutation {
  getMutation = () => Relay.QL`mutation { login }`;

  getVariables = () => ({
    login: this.props.input.login,
    password: this.props.input.password,
  });

  getFatQuery = () => Relay.QL`
      fragment on loginMutationPayload {
          tokenInfo {
              access_token,
              expires_in,
              refresh_token
          }
      }
  `;

  getConfigs = () => [{
    type: 'REQUIRED_CHILDREN',
    children: [
      Relay.QL`
          fragment on loginMutationPayload {
              tokenInfo {
                  access_token,
                  expires_in,
                  refresh_token
              }
          }
      `,
    ],
  }];

  static fragments = {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
        }
    `,
  };
}

export default LoginMutation;
