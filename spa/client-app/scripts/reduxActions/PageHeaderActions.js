import { SET_PAGE_HEADER, SET_PAGE_BACK_TO } from './../reduxConstants/actionTypes';

export function setPageHeader(header) {
  return {
    type: SET_PAGE_HEADER,
    header,
  };
}

export function setPageBackTo(backTo) {
  return {
    type: SET_PAGE_BACK_TO,
    backTo,
  };
}