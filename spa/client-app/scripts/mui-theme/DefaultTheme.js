import { createMuiTheme } from '@material-ui/core/styles';
import { blue, grey } from '@material-ui/core/colors';
// import lightBaseTheme from '@material-ui/core/styles/baseThemes/lightBaseTheme';
// import darkBaseTheme from '@material-ui/core/styles/baseThemes/darkBaseTheme';

const themeDefault = createMuiTheme({
  palette: {
    primary: {
      light: '#DCEDC8',
      main: '#8BC34A',
      dark: '#689F38',
      contrastText: '#212121',
    },
    secondary: {
      light: '#BDBDBD',
      main: '#667C89',
      dark: '#757575',
      contrastText: '#212121',
    },
  }, // http://www.material-ui.com/#/customization/themes
  typography: {
    fontFamily: '"PT Sans Narrow",sans-serif',
  },
});

export default themeDefault;
