import React from 'react';
import PropTypes from 'prop-types';
import Drawer from '@material-ui/core/Drawer';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Divider from '@material-ui/core/Divider';

import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth/index';

const styles = theme => ({
  drawerHeader: {
    display: 'flex',
    width: 206,
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    [theme.breakpoints.down('md')]: {
      height: 48,
      minHeight: 48,
    },
  },
});

@withStyles(styles)
class LeftDrawer extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
    handleDrawerClose: PropTypes.func.isRequired,
    menus: PropTypes.array,
    navDrawerOpen: PropTypes.bool.isRequired,
    width: PropTypes.any,
  };

  static defaultProps = {
    menus: [],
    classes: {},
  };

  render() {
    const {
      navDrawerOpen, handleDrawerClose, classes, menus, width,
    } = this.props;

    let variant = 'persistent';
    if (width === 'xs') {
      variant = 'temporary';
    }

    return (
      <Drawer
        variant={variant}
        open={navDrawerOpen}
        onClose={handleDrawerClose}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        {menus.map(menu => (
          <MenuItem
            to={menu.link}
            key={menu.text}
            component={Link}
          >
            <ListItemIcon>
              {menu.icon}
            </ListItemIcon>
            <ListItemText inset primary={menu.text} />
          </MenuItem>
        ))}
      </Drawer>
    );
  }
}

export default withWidth()(LeftDrawer);
