import withWidth from '@material-ui/core/withWidth/index';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import withStyles from '@material-ui/core/styles/withStyles';
import Typography from '@material-ui/core/Typography';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const styles = theme => ({
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  header: {
    flex: 1,
    color: '#ffffff',
  },
  menuItem: {
    color: '#FFF',
  },
  toolbar: {
    [theme.breakpoints.down('md')]: {
      minHeight: 48,
    },
  },
});

@connect(state => ({
  header: state.pageHeaderStore.header,
  backTo: state.pageHeaderStore.backTo,
}))
@withStyles(styles)
class Header extends React.Component {
  render() {
    const {
      classes, navDrawerOpen, appBarClasses, handleMenuIconClick, width, backTo,
    } = this.props;

    return (
      <AppBar
        position="fixed"
        classes={appBarClasses}
      >
        <Toolbar className={classes.toolbar}>
          {backTo && (
            <Link to={backTo}>
              <IconButton className={classes.menuButton} aria-label="Назад">
                <ArrowBackIcon className={classes.menuItem} />
              </IconButton>
            </Link>
          )}

          {!backTo && (!navDrawerOpen || width === 'xs') && (
            <IconButton className={classes.menuButton} aria-label="Меню" onClick={handleMenuIconClick}>
              <MenuIcon className={classes.menuItem} />
            </IconButton>
          )}
          <Typography variant="title" color="inherit" className={classes.header}>
            {this.props.header}
          </Typography>
        </Toolbar>
      </AppBar>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object,
  navDrawerOpen: PropTypes.bool.isRequired,
  handleMenuIconClick: PropTypes.func.isRequired,
};

export default withWidth()(Header);
