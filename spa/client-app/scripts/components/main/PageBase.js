import withStyles from '@material-ui/core/styles/withStyles';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import globalStyles from '../../../../common-code/globalStyles';
import { setPageHeader, setPageBackTo } from '../../reduxActions/PageHeaderActions';

const styles = {
  root: {
    width: '100%',
    maxWidth: 960,
    flex: 1,
    margin: '0 auto',
  },
};

@connect(
  state => ({}),
  dispatch => ({
    setPageHeader: header => dispatch(setPageHeader(header)),
    setPageBackTo: header => dispatch(setPageBackTo(header)),
  }),
)
@withStyles(styles)
class PageBase extends React.PureComponent {
  static defaultProps = {
    backTo: null,
  };

  static propTypes = {
    backTo: PropTypes.string,
    title: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);

    props.setPageHeader(props.title);
    props.setPageBackTo(props.backTo);
  }

  componentDidUpdate = (prevProps) => {
    if (this.props.title !== prevProps.title) {
      this.props.setPageHeader(this.props.title);
    }

    if (this.props.backTo !== prevProps.backTo) {
      this.props.setPageBackTo(this.props.backTo);
    }
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        {this.props.children}
        <div style={globalStyles.clear} />
      </div>
    );
  }
}

export default PageBase;
