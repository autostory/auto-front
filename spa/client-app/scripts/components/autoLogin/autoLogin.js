import get from 'lodash/get';
import AutoLoginMutation from '../../relay/mutation/AutoLoginMutation';
import commitMutationPromise from '../../../../common-code/relay/commitMutationPromise';

function autoLogin({ viewer, values }) {
  const onSuccess = (response) => {
    const tokenInfo = get(response, 'autoLogin.tokenInfo');
    const accessToken = get(tokenInfo, 'access_token', null);
    const expiresIn = get(tokenInfo, 'expires_in', null);
    const refreshToken = get(tokenInfo, 'refresh_token', null);

    if (accessToken && expiresIn && refreshToken) {
      return {
        accessToken,
        expiresIn,
        refreshToken,
      };
    }

    throw new Error('Не успешнаяя автоматическая авторизация.');
  };

  return commitMutationPromise(AutoLoginMutation, { viewer, input: values }).then(onSuccess);
}

export default autoLogin;
