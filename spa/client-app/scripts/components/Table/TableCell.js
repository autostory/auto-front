import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles/index';

const styles = theme => ({
  root: {
    padding: '12px 6px',
    color: '#000',
    fontSize: 16,
    [theme.breakpoints.up('md')]: {
      padding: '12px 24px',
    },
  },
});

@withStyles(styles)
class TableCell extends React.PureComponent {
  static defaultProps = {
    className: null,
  };

  static propTypes = {
    className: PropTypes.string,
  };

  render() {
    const { classes, className, ...rest } = this.props;

    return (
      <td className={classNames([classes.root, className])} {...rest}>
        {this.props.children}
      </td>
    );
  }
}

export default TableCell;
