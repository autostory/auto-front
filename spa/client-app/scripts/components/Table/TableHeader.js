import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles/index';

const styles = theme => ({
  root: {
    padding: '12px 6px',
    fontWeight: 'normal',
    color: '#757575',
    fontSize: 16,
    [theme.breakpoints.up('md')]: {
      padding: '12px 24px',
    },
  },
});

@withStyles(styles)
class TableHeader extends React.PureComponent {
  static defaultProps = {
    className: null,
  };

  static propTypes = {
    className: PropTypes.string,
  };

  render() {
    const { classes, className, ...rest } = this.props;

    return (
      <th className={classNames([classes.root, className])} {...rest}>
        {this.props.children}
      </th>
    );
  }
}

export default TableHeader;
