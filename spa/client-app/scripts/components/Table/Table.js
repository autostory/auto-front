import React from 'react';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles/index';

const styles = {
  root: {
    width: '100%',
    borderCollapse: 'collapse',
    textAlign: 'left',
  },
};

@withStyles(styles)
class Table extends React.PureComponent {
  render() {
    const { classes } = this.props;

    return (
      <Paper className={classes.root} component="table">
        {this.props.children}
      </Paper>
    );
  }
}

export default Table;
