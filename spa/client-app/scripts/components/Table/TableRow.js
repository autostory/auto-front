import React from 'react';
import { withStyles } from '@material-ui/core/styles/index';

const styles = {
  root: {
    borderBottom: '1px solid #C4C4C4',
  },
};

@withStyles(styles)
class TableRow extends React.PureComponent {
  render() {
    const { classes } = this.props;

    return (
      <tr className={classes.root}>
        {this.props.children}
      </tr>
    );
  }
}

export default TableRow;
