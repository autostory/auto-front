import React from 'react';
import { withStyles } from '@material-ui/core/styles/index';
import Relay from 'react-relay/classic';
import classNames from 'classnames';

import relayContainer from '../../../../../common-code/decorators/relayContainer';
import TableCell from '../../Table/TableCell';
import TableRow from '../../Table/TableRow';

const styles = {
  cell: {
    padding: '12px 24px',
  },
  cellRightAlign: {
    textAlign: 'right',
  },
  discountPercentContainer: {
    fontSize: 14,
  },
  discountPercent: {
    color: '#67AC5B',
  },
};

@relayContainer({
  serviceWork: () => Relay.QL`
      fragment on ServiceWork {
          id
          action {
              name
          }
          sparePartNode {
              name
          }
          totalCost
          amount
          note
          discountPercent
      }
  `,
})
@withStyles(styles)
class ServiceWorkRow extends React.PureComponent {
  render() {
    const { serviceWork, classes } = this.props;

    return (
      <TableRow>
        <TableCell className={classes.cell}>
          {serviceWork.action.name}
          {' '}
          {serviceWork.sparePartNode.name}
          {' '}
          {serviceWork.amount > 1 && <span>x{serviceWork.amount}</span>}
        </TableCell>
        <TableCell className={classNames([classes.cell, classes.cellRightAlign])}>
          <div>{serviceWork.totalCost}</div>
          {serviceWork.discountPercent > 0 && (
            <div className={classes.discountPercentContainer}>
              с учетом скидки <span className={classes.discountPercent}>{serviceWork.discountPercent}%</span>
            </div>
          )}
        </TableCell>
      </TableRow>
    );
  }
}

export default ServiceWorkRow;
