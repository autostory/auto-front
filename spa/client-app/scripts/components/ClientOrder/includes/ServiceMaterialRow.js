import React from 'react';
import { withStyles } from '@material-ui/core/styles/index';
import Relay from 'react-relay/classic';
import classNames from 'classnames';

import relayContainer from '../../../../../common-code/decorators/relayContainer';
import TableCell from '../../Table/TableCell';
import TableRow from '../../Table/TableRow';

const styles = {
  cell: {
    padding: '12px 24px',
  },
  cellRightAlign: {
    textAlign: 'right',
  },
  discountPercentContainer: {
    fontSize: 14,
  },
  discountPercent: {
    color: '#67AC5B',
  },
};

@relayContainer({
  serviceMaterial: () => Relay.QL`
      fragment on ServiceMaterial {
          id
          totalCost
          amount
          name
          unitMeasureType {
              name
          }
          discountPercent
      }
  `,
})
@withStyles(styles)
class ServiceMaterialRow extends React.PureComponent {
  render() {
    const { serviceMaterial, classes } = this.props;

    return (
      <TableRow>
        <TableCell className={classes.cell}>
          {serviceMaterial.name} {serviceMaterial.amount}{serviceMaterial.unitMeasureType.name}
        </TableCell>
        <TableCell className={classNames([classes.cell, classes.cellRightAlign])}>
          <div>{serviceMaterial.totalCost}</div>
          {serviceMaterial.discountPercent > 0 && (
            <div className={classes.discountPercentContainer}>
              с учетом скидки <span className={classes.discountPercent}>{serviceMaterial.discountPercent}%</span>
            </div>
          )}
        </TableCell>
      </TableRow>
    );
  }
}

export default ServiceMaterialRow;
