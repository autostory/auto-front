import React from 'react';
import withProps from 'recompose/withProps';
import { withStyles } from '@material-ui/core/styles/index';
import Relay from 'react-relay/classic';
import Typography from '@material-ui/core/Typography';
import map from 'lodash/map';
import classNames from 'classnames';

import { getOrderStatusTypeName } from '../../../../common-code/constants/orderStatusTypes';

import relaySuperContainer from '../../../../common-code/decorators/relaySuperContainer';
import phoneFormat from '../../../../common-code/utils/text/phoneFormat';
import FullContainerSizePreLoader from '../basic/FullContainerSizePreLoader';
import Table from '../Table/Table';
import TableCell from '../Table/TableCell';
import TableHeader from '../Table/TableHeader';
import TableRow from '../Table/TableRow';
import ServiceMaterialRow from './includes/ServiceMaterialRow';
import ServiceWorkRow from './includes/ServiceWorkRow';

const styles = {
  headerSmall: {
    fontSize: 16,
    color: 'rgba(0,0,0,0.64)',
    fontWeight: 'bold',
  },
  headerBig: {
    fontSize: 20,
    color: 'rgba(0,0,0,0.64)',
    fontWeight: 'bold',
    padding: '12px 24px 12px 24px',
  },
  status: {
    color: 'rgba(0,0,0,0.87)',
    fontSize: 20,
  },
  text: {
    color: 'rgba(0,0,0,0.87)',
    fontSize: 16,
  },
  mainData: {
    padding: '12px 24px 12px 24px',
  },
  noRecords: {
    color: 'rgba(0,0,0,0.64)',
    fontSize: 20,
    maxWidth: 400,
    padding: 24,
    margin: '0 auto',
  },
  cell: {
    padding: '12px 24px',
  },
  cellRightAlign: {
    textAlign: 'right',
  },
  totalSumContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  totalSumRow: {
    display: 'flex',
    justifyContent: 'space-between',
    width: 200,
  },
};

@withProps({ renderLoading: () => <FullContainerSizePreLoader /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id
          order(orderId: $orderId) {
              id
              status
              car {
                  id
                  displayName
                  registrationSignNumber
              }
              client {
                  id
                  phone
                  fullName
              }
              serviceAddress
              contactPhone
              contactName
              phoneSameAsInProfile
              serviceMaterials {
                  id
                  ${ServiceMaterialRow.getFragment('serviceMaterial')}
              }
              serviceWorks {
                  id
                  ${ServiceWorkRow.getFragment('serviceWork')}
              }
              totalBill
              paymentsSum
          }
      }
  `,
})
@withStyles(styles)
class ClientOrderPageContent extends React.PureComponent {
  render() {
    const { viewer, classes } = this.props;
    const { order } = viewer;

    return (
      <React.Fragment>
        <div className={classes.mainData}>
          <Typography className={classes.headerSmall}>Статус</Typography>
          <Typography className={classes.status}>{getOrderStatusTypeName(order.status)}</Typography>
        </div>
        <div className={classes.mainData}>
          <Typography className={classes.headerSmall}>Автомобиль</Typography>
          <Typography className={classes.text}>{order.car.displayName} {order.car.registrationSignNumber}</Typography>
        </div>
        <div className={classes.mainData}>
          <Typography className={classes.headerSmall}>Контакты клиента</Typography>
          <Typography className={classes.text}>
            {order.phoneSameAsInProfile && (
              <React.Fragment>
                {order.client.fullName}
                <br />
                {phoneFormat(order.client.phone)}
              </React.Fragment>
            )}
            {!order.phoneSameAsInProfile && (
              <React.Fragment>
                {order.contactName}
                <br />
                {phoneFormat(order.contactPhone)}
              </React.Fragment>
            )}
          </Typography>
        </div>
        <div className={classes.mainData}>
          <Typography className={classes.headerSmall}>Автосервис</Typography>
          <Typography className={classes.text}>{order.serviceAddress}</Typography>
        </div>
        <Typography className={classes.headerBig}>Работы и материалы</Typography>

        {(order.serviceWorks.length === 0 && order.serviceMaterials.length === 0) && (
          <Typography className={classes.noRecords}>
            Работы по заказу еще не добавляли
          </Typography>
        )}

        {(order.serviceWorks.length > 0 || order.serviceMaterials.length > 0) && (
          <Table>
            <thead>
            <TableRow>
              <TableHeader className={classes.cell}>Описание</TableHeader>
              <TableHeader className={classNames([classes.cell, classes.cellRightAlign])}>Сумма</TableHeader>
            </TableRow>
            </thead>
            <tbody>
            {order.serviceWorks.length > 0 && map(order.serviceWorks, item => (
              <ServiceWorkRow serviceWork={item} key={item.id} />
            ))}

            {order.serviceMaterials.length > 0 && map(order.serviceMaterials, item => (
              <ServiceMaterialRow serviceMaterial={item} key={item.id} />
            ))}
            <TableRow>
              <TableCell colSpan={2} className={classes.cell}>
                <div className={classes.totalSumContainer}>
                  <div className={classes.totalSumRow}>
                    <div>ИТОГО</div>
                    <div>{order.totalBill.toFixed(0)} р.</div>
                  </div>
                  <div className={classes.totalSumRow}>
                    <div>ОПЛАЧЕНО</div>
                    <div>{order.paymentsSum.toFixed(0)} р.</div>
                  </div>
                  <div className={classes.totalSumRow}>
                    <div>ДОЛГ</div>
                    <div>{(order.totalBill - order.paymentsSum).toFixed(0)} р.</div>
                  </div>
                </div>
              </TableCell>
            </TableRow>

            </tbody>
          </Table>
        )}

      </React.Fragment>
    );
  }
}

export default ClientOrderPageContent;
