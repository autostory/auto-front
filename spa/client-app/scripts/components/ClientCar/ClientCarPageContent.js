import React from 'react';
import map from 'lodash/map';
import get from 'lodash/get';
import { connect } from 'react-redux';
import withProps from 'recompose/withProps';
import { withStyles } from '@material-ui/core/styles/index';
import Relay from 'react-relay/classic';
import Typography from '@material-ui/core/Typography';

import relaySuperContainer from '../../../../common-code/decorators/relaySuperContainer';
import { setPageHeader } from '../../reduxActions/PageHeaderActions';
import FullContainerSizePreLoader from '../basic/FullContainerSizePreLoader';
import Table from '../Table/Table';
import TableHeader from '../Table/TableHeader';
import TableRow from '../Table/TableRow';
import ServiceWorkRow from './includes/ServiceWorkRow';
import ServiceMaterialRow from './includes/ServiceMaterialRow';

const styles = {
  noRecords: {
    color: 'rgba(0,0,0,0.64)',
    fontSize: 20,
    maxWidth: 400,
    padding: 24,
    margin: '0 auto',
  },
};

@withProps({ renderLoading: () => <FullContainerSizePreLoader /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id
          car(carId: $carId) {
              id
              displayName
              registrationSignNumber
              serviceBookRecords {
                  __typename
                  ... on ServiceMaterial {
                      id
                      ${ServiceMaterialRow.getFragment('serviceMaterial')}
                  }
                  ... on ServiceWork {
                      id
                      ${ServiceWorkRow.getFragment('serviceWork')}
                  }
              }
          }
      }
  `,
})
@withStyles(styles)
@connect(
  state => ({}),
  dispatch => ({
    setPageHeader: header => dispatch(setPageHeader(header)),
  }),
)
class ClientCarPageContent extends React.PureComponent {
  constructor(props) {
    super(props);

    const carDisplayName = get(props, 'viewer.car.displayName', null);
    const carRegistrationSignNumber = get(props, 'viewer.car.registrationSignNumber', null);

    if (carDisplayName || carRegistrationSignNumber) {
      props.setPageHeader(`${carDisplayName} ${carRegistrationSignNumber}`);
    }
  }

  render() {
    const { viewer, classes } = this.props;
    const { car } = viewer;
    const { serviceBookRecords } = car;

    return (
      <React.Fragment>
        {serviceBookRecords.length === 0 && (
          <Typography className={classes.noRecords}>
            Еще нет записей о работах по этому автомобилю.<br />
            <br />
            Записи появятся после того как будут выполненные заказы по этому автомобилю.<br />
          </Typography>
        )}

        {serviceBookRecords.length > 0 && (
          <Table>
            <thead>
            <TableRow>
              <TableHeader>Описание</TableHeader>
              <TableHeader>Сумма</TableHeader>
              <TableHeader>Дата</TableHeader>
            </TableRow>
            </thead>
            <tbody>
            {map(serviceBookRecords, item => {
              if (item.__typename === 'ServiceWork') {
                return <ServiceWorkRow serviceWork={item} key={item.id} />;
              }

              return <ServiceMaterialRow serviceMaterial={item} key={item.id} />;
            })}
            </tbody>
          </Table>

        )}
      </React.Fragment>
    );
  }
}

export default ClientCarPageContent;
