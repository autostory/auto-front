import React from 'react';
import get from 'lodash/get';
import { format } from 'date-fns';
import { withStyles } from '@material-ui/core/styles/index';
import Relay from 'react-relay/classic';
import relayContainer from '../../../../../common-code/decorators/relayContainer';
import TableCell from '../../Table/TableCell';
import TableRow from '../../Table/TableRow';

const styles = {
  discountPercentContainer: {
    fontSize: 14,
  },
  discountPercent: {
    color: '#67AC5B',
  },
};

@relayContainer({
  serviceWork: () => Relay.QL`
      fragment on ServiceWork {
          id
          action {
              name
          }
          sparePartNode {
              name
          }
          totalCost
          amount
          note
          discountPercent
          doneAt {
              timestamp
          }
      }
  `,
})
@withStyles(styles)
class ServiceWorkRow extends React.PureComponent {
  render() {
    const { serviceWork, classes } = this.props;

    const timestamp = get(serviceWork, 'doneAt.timestamp', null);

    return (
      <TableRow>
        <TableCell>
          {serviceWork.action.name}
          {' '}
          {serviceWork.sparePartNode.name}
          {' '}
          {serviceWork.amount > 1 && <span>x{serviceWork.amount}</span>}
        </TableCell>
        <TableCell>
          <div>{serviceWork.totalCost}</div>
          {serviceWork.discountPercent > 0 && (
            <div className={classes.discountPercentContainer}>
              с учетом скидки <span className={classes.discountPercent}>{serviceWork.discountPercent}%</span>
            </div>
          )}
        </TableCell>
        <TableCell>
          {timestamp && format(new Date().setTime(timestamp * 1000), 'DD.MM.YY HH:mm')}
        </TableCell>
      </TableRow>
    );
  }
}

export default ServiceWorkRow;
