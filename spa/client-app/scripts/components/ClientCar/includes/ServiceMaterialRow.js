import React from 'react';
import get from 'lodash/get';
import { format } from 'date-fns';
import { withStyles } from '@material-ui/core/styles/index';
import Relay from 'react-relay/classic';
import relayContainer from '../../../../../common-code/decorators/relayContainer';
import TableCell from '../../Table/TableCell';
import TableRow from '../../Table/TableRow';

const styles = {
  discountPercentContainer: {
    fontSize: 14,
  },
  discountPercent: {
    color: '#67AC5B',
  },
};

@relayContainer({
  serviceMaterial: () => Relay.QL`
      fragment on ServiceMaterial {
          id
          totalCost
          amount
          name
          unitMeasureType {
              name
          }
          discountPercent
          doneAt {
              timestamp
          }
      }
  `,
})
@withStyles(styles)
class ServiceMaterialRow extends React.PureComponent {
  render() {
    const { serviceMaterial, classes } = this.props;

    const timestamp = get(serviceMaterial, 'doneAt.timestamp', null);

    return (
      <TableRow>
        <TableCell>{serviceMaterial.name} {serviceMaterial.amount}{serviceMaterial.unitMeasureType.name}</TableCell>
        <TableCell>
          <div>{serviceMaterial.totalCost}</div>
          {serviceMaterial.discountPercent > 0 && (
            <div className={classes.discountPercentContainer}>
              с учетом скидки <span className={classes.discountPercent}>{serviceMaterial.discountPercent}%</span>
            </div>
          )}
        </TableCell>
        <TableCell>
          {timestamp && format(new Date().setTime(timestamp * 1000), 'DD.MM.YY HH:mm')}
        </TableCell>
      </TableRow>
    );
  }
}

export default ServiceMaterialRow;
