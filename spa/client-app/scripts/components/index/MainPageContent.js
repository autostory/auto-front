import React from 'react';
import withProps from 'recompose/withProps';
import { withStyles } from '@material-ui/core/styles/index';
import Relay from 'react-relay/classic';

import relaySuperContainer from '../../../../common-code/decorators/relaySuperContainer';
import FullContainerSizePreLoader from '../basic/FullContainerSizePreLoader';
import ActiveOrdersList from './includes/ActiveOrdersList';
import MyCarsList from './includes/MyCarsList';
import PointListRow from './includes/PointListRow';
import UserName from './includes/UserName';

const styles = {};

@withProps({ renderLoading: () => <FullContainerSizePreLoader /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id
          me {
              ${UserName.getFragment('client')}
              ${PointListRow.getFragment('client')}
              ${ActiveOrdersList.getFragment('client')}
              ${MyCarsList.getFragment('client')}
          }
      }
  `,
})
@withStyles(styles)
class MainPageContent extends React.PureComponent {
  render() {
    const { viewer } = this.props;
    const { me } = viewer;

    return (
      <React.Fragment>
        <UserName client={me} />
        <PointListRow client={me} />
        <ActiveOrdersList client={me} />
        <MyCarsList client={me} />
      </React.Fragment>
    );
  }
}

export default MainPageContent;
