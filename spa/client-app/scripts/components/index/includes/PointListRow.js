import React from 'react';
import { withStyles } from '@material-ui/core/styles/index';
import Relay from 'react-relay/classic';
import Paper from '@material-ui/core/Paper';
import { Link } from 'react-router-dom';
import ButtonBase from '@material-ui/core/ButtonBase';


import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import withWidth from '@material-ui/core/withWidth/index';

import relayContainer from '../../../../../common-code/decorators/relayContainer';

const styles = {
  listItemRoot: {
    fontSize: 16,
  },
  listItemDense: {
    fontSize: 16,
  },
  list: {
    textDecoration: 'none',
  },
};

@relayContainer({
  client: () => Relay.QL`
      fragment on Client {
          points
      }
  `,
})
@withStyles(styles)
class PointListRow extends React.PureComponent {
  render() {
    const { client, width, classes } = this.props;
    const { points } = client;

    let primaryText = `Можно потратить ${points} баллов`;
    if (points === 0) {
      primaryText = 'У вас еще нет баллов';
    }

    return (
      <Paper>
        <List
          dense={width === 'xs'}
          component={Link}
          to="/point-log"
          className={classes.list}
        >
          <ListItem component={ButtonBase}>
            <ListItemIcon>
              <AttachMoneyIcon />
            </ListItemIcon>
            <ListItemText
              classes={{
                root: classes.listItemRoot,
                dense: classes.listItemDense,
              }}
              primary={primaryText}
              secondary="1 балл = 1 рубль"
            />
            <ListItemSecondaryAction>
              <IconButton aria-label="More">
                <ChevronRightIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        </List>
      </Paper>
    );
  }
}

export default withWidth()(PointListRow);
