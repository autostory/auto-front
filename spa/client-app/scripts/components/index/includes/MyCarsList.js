import React from 'react';
import get from 'lodash/get';
import map from 'lodash/map';
import { withStyles } from '@material-ui/core/styles/index';
import Relay from 'react-relay/classic';
import Paper from '@material-ui/core/Paper';
import ButtonBase from '@material-ui/core/ButtonBase';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import CarIcon from '@material-ui/icons/DirectionsCar';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import withWidth from '@material-ui/core/withWidth/index';
import { Link } from 'react-router-dom';

import relayContainer from '../../../../../common-code/decorators/relayContainer';

const styles = {
  header: {
    fontSize: 20,
    color: '#000',
    padding: '12px 24px 12px 24px',
  },
  listItemRoot: {
    fontSize: 16,
  },
  listItemDense: {
    fontSize: 16,
  },
  list: {
    textDecoration: 'none',
  },
};

@relayContainer({
  client: () => Relay.QL`
      fragment on Client {
          cars {
              carList {
                  edges {
                      node {
                          id
                          displayName
                          registrationSignNumber
                      }
                  }
              }
          }
      }
  `,
})
@withStyles(styles)
class MyCarsList extends React.PureComponent {
  render() {
    const { client, width, classes } = this.props;
    const cars = map(get(client, 'cars.carList.edges'), 'node');

    return (
      <div>
        <Typography variant="subheading" className={classes.header}>
          Мои автомобили
        </Typography>
        <Paper>
          {cars.length > 0 && (
            map(cars, car => (
              <List
                key={car.id}
                dense={width === 'xs'}
                component={Link}
                to={`/car/${car.id}`}
                className={classes.list}
              >
                <ListItem component={ButtonBase}>
                  <ListItemIcon>
                    <CarIcon />
                  </ListItemIcon>
                  <ListItemText
                    classes={{
                      root: classes.listItemRoot,
                      dense: classes.listItemDense,
                    }}
                    primary={car.displayName}
                    secondary={car.registrationSignNumber}
                  />
                  <ListItemSecondaryAction>
                    <IconButton aria-label="More">
                      <ChevronRightIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              </List>
            ))
          )}
        </Paper>
      </div>
    );
  }
}

export default withWidth()(MyCarsList);
