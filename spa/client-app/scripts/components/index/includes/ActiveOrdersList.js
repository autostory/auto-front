import React from 'react';
import map from 'lodash/map';
import { withStyles } from '@material-ui/core/styles/index';
import Relay from 'react-relay/classic';
import Paper from '@material-ui/core/Paper';
import ButtonBase from '@material-ui/core/ButtonBase';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import BuildIcon from '@material-ui/icons/Build';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import withWidth from '@material-ui/core/withWidth/index';
import { Link } from 'react-router-dom';

import relayContainer from '../../../../../common-code/decorators/relayContainer';
import { fromGlobalId } from '../../../../../common-code/relay/globalIdUtils';
import { getOrderStatusTypeName } from '../../../../../common-code/constants/orderStatusTypes';

const styles = {
  header: {
    fontSize: 20,
    color: '#000',
    padding: '12px 24px 12px 24px',
  },
  listItemRoot: {
    fontSize: 16,
  },
  listItemDense: {
    fontSize: 16,
  },
  typoInherit: {
    fontSize: 'inherit',
  },
  noRecords: {
    fontSize: 16,
    padding: '0 24px 12px 24px',
    color: 'rgba(117,117,117,0.87)',
  },
  list: {
    textDecoration: 'none',
  },
};

@relayContainer({
  client: () => Relay.QL`
      fragment on Client {
          activeOrders {
              id
              status
              car {
                  registrationSignNumber
                  displayName
              }
          }
      }
  `,
})
@withStyles(styles)
class ActiveOrdersList extends React.PureComponent {
  render() {
    const { client, width, classes } = this.props;
    const { activeOrders } = client;

    return (
      <div>
        <Typography variant="subheading" className={classes.header}>
          Активные заказы
        </Typography>

        {activeOrders.length === 0 && (
          <Typography className={classes.noRecords}>нет активных заказов</Typography>
        )}

        {activeOrders.length > 0 && (
          <Paper>
            {map(activeOrders, order => (
              <List
                key={order.id}
                dense={width === 'xs'}
                component={Link}
                className={classes.list}
                to={`/order/${order.id}`}
              >
                <ListItem component={ButtonBase}>
                  <ListItemIcon>
                    <BuildIcon />
                  </ListItemIcon>
                  <ListItemText
                    classes={{
                      root: classes.listItemRoot,
                      dense: classes.listItemDense,
                    }}
                    primary={
                      <Typography className={classes.typoInherit}>
                        SW-{fromGlobalId(order.id).id} <b>{getOrderStatusTypeName(order.status)}</b>
                      </Typography>
                    }
                    secondary={`${order.car.displayName} ${order.car.registrationSignNumber}`}
                  />
                  <ListItemSecondaryAction>
                    <IconButton aria-label="More">
                      <ChevronRightIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              </List>
            ))}
          </Paper>
        )}

      </div>
    );
  }
}

export default withWidth()(ActiveOrdersList);
