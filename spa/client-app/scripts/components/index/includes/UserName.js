import React from 'react';
import { withStyles } from '@material-ui/core/styles/index';
import Relay from 'react-relay/classic';
import Typography from '@material-ui/core/Typography';

import relayContainer from '../../../../../common-code/decorators/relayContainer';

const styles = {
  text: {
    fontSize: 20,
    color: 'rgba(117,117,117,0.87)',
    padding: '12px 24px 12px 24px',
  },
};

@relayContainer({
  client: () => Relay.QL`
      fragment on Client {
          fullName
      }
  `,
})
@withStyles(styles)
class UserName extends React.PureComponent {
  render() {
    const { client, classes } = this.props;

    return (
      <Typography
        className={classes.text}
      >
        {client.fullName}
      </Typography>
    );
  }
}

export default UserName;
