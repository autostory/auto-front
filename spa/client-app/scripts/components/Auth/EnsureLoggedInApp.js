import React from 'react';
import Relay from 'react-relay/classic';
import PropTypes from 'prop-types';
import ViewerRoute from '../../../../common-code/relay/route/ViewerRoute';
import AuthComponentsContainer from './AuthComponentsContainer';
import FullScreenPreLoader from '../basic/FullScreenPreLoader';

class EnsureLoggedInApp extends React.PureComponent {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node,
    ]),
  };

  static defaultProps = {
    children: null,
  };

  render() {
    return (
      <Relay.RootContainer
        Component={AuthComponentsContainer}
        route={new ViewerRoute()}
        forceFetch
        renderFetched={data => (
          <AuthComponentsContainer viewer={data.viewer}>
            {this.props.children}
          </AuthComponentsContainer>
        )}
        renderLoading={() => (
          <FullScreenPreLoader />
        )}
      />
    );
  }
}

export default EnsureLoggedInApp;
