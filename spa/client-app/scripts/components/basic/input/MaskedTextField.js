import React from 'react';
import VMasker from 'vanilla-masker';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';

class MaskedTextField extends React.PureComponent {
  static propTypes = {
    defaultValue: PropTypes.string,
    value: PropTypes.string,
    mask: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  };

  static defaultProps = {
    defaultValue: '',
    value: '',
  };

  onChange = (event) => {
    const { mask, onChange } = this.props;
    const filteredValue = this.filter(event.target.value, mask);
    event.target.value = filteredValue;

    onChange(event, filteredValue);
  };

  filter = (_value, mask) => {
    const value = String(_value);
    let filteredValue;
    if (mask === 'number') {
      filteredValue = VMasker.toNumber(value);
    } else {
      filteredValue = VMasker.toPattern(value, mask);
    }

    return filteredValue;
  };

  render() {
    const {
      defaultValue, mask, value, onChange, ...rest
    } = this.props;

    const filteredValue = this.filter(value, mask);

    return (
      <TextField
        value={filteredValue}
        onChange={this.onChange}
        {...rest}
      />
    );
  }
}

export default MaskedTextField;
