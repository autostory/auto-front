import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import themeDefault from '../../mui-theme/DefaultTheme';

const preloadStyle = {
  container: {
    display: 'inline-flex',
    width: '100%',
    height: '100%',
    minHeight: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
};

class FullContainerSizePreLoader extends React.Component {
  render() {
    return (
      <MuiThemeProvider theme={themeDefault}>
        <div style={preloadStyle.container}>
          <CircularProgress />
          <Typography>Загрузка...</Typography>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default FullContainerSizePreLoader;
