import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { withStyles } from '@material-ui/core/styles';

import themeDefault from '../../mui-theme/DefaultTheme';

const styles = {
  container: {
    display: 'inline-flex',
    width: '100%',
    height: '100%',
    minHeight: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
};

@withStyles(styles)
class FullScreenPreLoader extends React.Component {
  render() {
    const { classes } = this.props;

    return (
      <MuiThemeProvider theme={themeDefault}>
        <div className={classes.container}>
          <CircularProgress />
          <Typography>Загрузка...</Typography>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default FullScreenPreLoader;
