import { format } from 'date-fns';
import React from 'react';
import map from 'lodash/map';
import withProps from 'recompose/withProps';
import { withStyles } from '@material-ui/core/styles/index';
import Relay from 'react-relay/classic';
import Typography from '@material-ui/core/Typography';

import relaySuperContainer from '../../../../common-code/decorators/relaySuperContainer';
import { fromGlobalId } from '../../../../common-code/relay/globalIdUtils';
import FullContainerSizePreLoader from '../basic/FullContainerSizePreLoader';
import Table from '../Table/Table';
import TableCell from '../Table/TableCell';
import TableHeader from '../Table/TableHeader';
import TableRow from '../Table/TableRow';
import WriteOff from './includes/WriteOff';

const styles = {
  noRecords: {
    color: 'rgba(117,117,117,0.87)',
    fontSize: 20,
    textAlign: 'center',
    padding: 24,
  },
  date: {
    color: '#5C5C5C',
    fontSize: 14,
  },
};

@withProps({ renderLoading: () => <FullContainerSizePreLoader /> })
@relaySuperContainer({
  viewer: () => Relay.QL`
      fragment on Viewer {
          id
          me {
              pointLog {
                  __typename
                  ... on ClientPointWriteOffLog {
                      id
                      amount
                      firmTitle
                      orderPayment {
                          order {
                              id
                              makeAt {
                                  timestamp
                              }
                              car {
                                  id
                                  displayName
                                  registrationSignNumber
                              }
                          }
                      }
                  }
              }
          }
      }
  `,
})
@withStyles(styles)
class PointLogPageContent extends React.PureComponent {
  render() {
    const { viewer, classes } = this.props;
    const { me } = viewer;
    const { pointLog } = me;

    return (
      <React.Fragment>
        {pointLog.length === 0 && (
          <Typography className={classes.noRecords}>Вы еще не зарабатывали и не тратили баллы</Typography>
        )}

        {pointLog.length > 0 && (
          <Table>
            <thead>
            <TableRow>
              <TableHeader>Сумма</TableHeader>
              <TableHeader>Заказ</TableHeader>
              <TableHeader>СТО</TableHeader>
              <TableHeader>Автомобиль</TableHeader>
            </TableRow>
            </thead>
            <tbody>
            {map(pointLog, item => (
              <TableRow key={item.id}>
                <TableCell><WriteOff value={item.amount} /></TableCell>
                <TableCell>
                  SW-{fromGlobalId(item.orderPayment.order.id).id}<br />
                  <span className={classes.date}>
                    {format(new Date().setTime(item.orderPayment.order.makeAt.timestamp * 1000), 'DD.MM.YY HH:mm')}
                  </span>
                </TableCell>
                <TableCell>{item.firmTitle}</TableCell>
                <TableCell>
                  {`${item.orderPayment.order.car.displayName} ${item.orderPayment.order.car.registrationSignNumber}`}
                </TableCell>
              </TableRow>
            ))}
            </tbody>
          </Table>
        )}
      </React.Fragment>
    );
  }
}

export default PointLogPageContent;
