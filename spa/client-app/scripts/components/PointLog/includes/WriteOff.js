import PropTypes from 'prop-types';
import React from 'react';
import { withStyles } from '@material-ui/core/styles/index';

const styles = {
  root: {
    color: '#EC6237',
  },
};

@withStyles(styles)
class WriteOff extends React.PureComponent {
  static propTypes = {
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequired,
  };

  render() {
    const { classes, value } = this.props;

    return <span className={classes.root}>-{value}</span>;
  }
}

export default WriteOff;
