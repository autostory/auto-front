import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import withWidth from '@material-ui/core/withWidth';
import withStyles from '@material-ui/core/styles/withStyles';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';

import ExitIcon from '@material-ui/icons/ExitToApp';

import Header from '../components/main/Header';
import LeftDrawer from '../components/main/LeftDrawer';
import themeDefault from '../mui-theme/DefaultTheme';

const drawerWidth = 206;

const styles = theme => ({
  header: {
    paddingLeft: 0,
  },
  headerWithOpenedDrawer: {
    paddingLeft: 0,
    [theme.breakpoints.up('sm')]: {
      paddingLeft: drawerWidth,
    },
  },
  container: {
    display: 'flex',
    minHeight: 'calc(100% - 58px)',
    margin: '58px 0 0 0',
    [theme.breakpoints.up('sm')]: {
      margin: '73px 0 0 0',
      minHeight: 'calc(100% - 73px)',
    },
    width: '100%',
    height: '100%',
    paddingLeft: 0,
  },
  containerWithOpenedDrawer: {
    paddingLeft: 0,
    [theme.breakpoints.up('sm')]: {
      paddingLeft: drawerWidth,
    },
  },
});

const menus = [
  {
    text: 'Выйти',
    icon: <ExitIcon />,
    link: '/logout',
  },
];

@withStyles(styles)
class App extends React.Component {
  static propTypes = {
    width: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']).isRequired,
  };

  state = {
    navDrawerOpen: false,
  };

  handleMenuIconClick = () => {
    this.setState({
      navDrawerOpen: !this.state.navDrawerOpen,
    });
  };

  handleDrawerClose = () => {
    this.setState({
      navDrawerOpen: false,
    });
  };

  render() {
    const { navDrawerOpen } = this.state;
    const { classes } = this.props;

    return (
      <MuiThemeProvider theme={themeDefault}>
        <React.Fragment>
          <Header
            appBarClasses={{
              root: classNames([
                classes.header,
                navDrawerOpen && classes.headerWithOpenedDrawer,
              ]),
            }}
            navDrawerOpen={navDrawerOpen}
            handleMenuIconClick={this.handleMenuIconClick}
          />

          <LeftDrawer
            handleDrawerClose={this.handleDrawerClose}
            navDrawerOpen={navDrawerOpen}
            menus={menus}
          />

          <div
            className={classNames([
              classes.container,
              navDrawerOpen && classes.containerWithOpenedDrawer,
            ])}
          >
            {this.props.children}
          </div>
        </React.Fragment>
      </MuiThemeProvider>
    );
  }
}

export default withWidth()(App);
