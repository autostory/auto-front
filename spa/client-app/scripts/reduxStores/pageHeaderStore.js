import { SET_PAGE_HEADER, SET_PAGE_BACK_TO } from './../reduxConstants/actionTypes';

const initialState = {
  header: '',
  backTo: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_PAGE_HEADER: {
      return {
        ...state,
        header: action.header,
      };
    }
    case SET_PAGE_BACK_TO: {
      return {
        ...state,
        backTo: action.backTo,
      };
    }
    default:
      return state;
  }
}
