#!/bin/bash

if [ "$AH_REMOVE_OLD_BACKUPS" = "on" ] || [ "$1" = "--force" ]; then
    echo 'Job is started: removing old backups.'

    /usr/bin/rdiff-backup --remote-schema 'ssh -o StrictHostKeyChecking=no -i /root/.ssh/id_rsa %s rdiff-backup --server' --remove-older-than 3M $AH_BACKUP_PSQL_RDIFF_DESTINATION

    echo 'Job is started: removing old backups.'
else
    echo 'Removing of old files had been disabled.'
fi
