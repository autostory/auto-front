#!/bin/bash

if [ "$AH_BACKUP_PSQL" = "on" ] || [ "$1" = "--force" ]; then
    echo 'Job is started: psql backup.'

    if [ ! -d /tmp/pgdump/ ]; then
        mkdir /tmp/pgdump/
    fi

    PGPASSWORD="${AH_BACKUP_PSQL_PASSWORD}" /usr/bin/pg_dumpall -U${AH_BACKUP_PSQL_USER} -h${AH_BACKUP_PSQL_HOST} | /bin/gzip - --rsyncable > /tmp/pgdump/pgdumpall.sql.gz
    /usr/bin/rdiff-backup --remote-schema "ssh -o StrictHostKeyChecking=no -i /root/.ssh/id_rsa %s rdiff-backup --server" --print-statistic /tmp/pgdump/ ${AH_BACKUP_PSQL_RDIFF_DESTINATION}

    rm -R /tmp/pgdump/*

    echo 'Job is finished: psql backup.'
else
    echo 'Psql backup had been disabled.'
fi

