#!/bin/bash

if [ "$AH_CRON_POINTS" = "on" ] || [ "$1" = "--force" ]
then
    exec /etc/tasks/task.sh 'points' > /proc/1/fd/1 2> /proc/1/fd/2
fi