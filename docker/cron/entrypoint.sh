#!/bin/bash
set -e

if [ -d /mnt/ssh-keys/ ]; then
    cp -r /mnt/ssh-keys/ /root/.ssh/
    chmod 700 /root/.ssh/
    chmod 400 /root/.ssh/id_rsa
fi

env > /tmp/my.env

echo "
" >> /etc/cron.d/my-cron

exec cron -f
