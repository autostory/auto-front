#!/usr/bin/env bash

printf 'Cron job %s started.\n' $1
printf 'Job output: '
/usr/bin/wget http://localuser:V3Ry@nginx:81/$1.php --tries=1 -q -O -
printf '\n'
printf 'Cron job %s finished.\n' $1