module.exports = {
  compact: true,
  babelrc: false,
  cacheDirectory: true,
  presets: [
    "env",
    "react",
    "stage-0"
  ],
  plugins: [
    [
      "relay",
      {
        "schema": "./spa/graphql/schema.json"
      }
    ],
    "transform-async-to-generator",
    "transform-class-properties",
    "transform-decorators-legacy",
    "transform-es2015-block-scoping",
    [
      "transform-es2015-classes",
      {
        "loose": true,
      }
    ],
    "transform-proto-to-assign"
  ],
};
