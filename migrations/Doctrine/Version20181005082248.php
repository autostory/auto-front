<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181005082248 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('
            CREATE OR REPLACE FUNCTION calculate_office_order_debt(officeId integer) returns NUMERIC as $$
                SELECT COALESCE(SUM(debt), 0) FROM (
                    SELECT CASE WHEN debt < 0 THEN 0
                        ELSE debt
                    END
                     FROM (
                    SELECT 
                        client_order.id,
                        client_order.status,
                        sw.sw_bill,
                        sm.sm_bill,
                        op.paymentSum,
                        (sw.sw_bill + sm.sm_bill - op.paymentSum) AS debt
                        FROM client_order 
                        LEFT JOIN (
                            SELECT
                            c3_.id as order_id,
                            COALESCE(SUM(s1_.total_cost),0) as sw_bill,
                            c3_.status as status 
                            FROM "client_order" c3_ 
                            LEFT JOIN service_work s1_ ON c3_.id = s1_.order_id 
                            GROUP BY c3_.id
                            ORDER by c3_.id
                        ) as sw ON sw.order_id = client_order.id
                        LEFT JOIN (
                            SELECT 
                            COALESCE(SUM(s0_.total_cost),0) as sm_bill,
                            c3_.id as order_id,
                             c3_.status as status 
                            FROM "client_order" c3_ 
                            LEFT JOIN service_material s0_ ON c3_.id = s0_.order_id 
                            GROUP BY c3_.id
                            ORDER by c3_.id
                        ) as sm ON sm.order_id = client_order.id
                        LEFT JOIN (
                            SELECT 
                            COALESCE(SUM(o2_.amount),0) as paymentSum,
                            c3_.id as order_id,
                             c3_.status as status 
                            FROM "client_order" c3_ 
                            LEFT JOIN "order_payment" o2_ ON c3_.id = o2_.order_id 
                            GROUP BY c3_.id
                            ORDER by c3_.id
                        ) as op ON op.order_id = client_order.id
                            WHERE client_order.status NOT IN (30, 50, 21) 
                            AND client_order.office_id = officeId
                    ) as q1
                ) as q2
            $$ language sql; 
    ');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE OR REPLACE FUNCTION calculate_office_order_debt(officeId integer) returns NUMERIC as $$
                SELECT COALESCE(SUM(debt), 0) FROM (
                    SELECT CASE WHEN debt < 0 THEN 0
                        ELSE debt
                    END
                     FROM (
                    SELECT 
                    COALESCE(SUM(s0_.total_cost),0) as sm_bill,
                    COALESCE(SUM(s1_.total_cost),0) as sw_bill,
                    COALESCE(SUM(o2_.amount),0) as paymentSum,
                    ((COALESCE(SUM(s0_.total_cost),0) + COALESCE(SUM(s1_.total_cost),0)) - COALESCE(SUM(o2_.amount),0)) AS debt, 
                    c3_.id as order_id,
                     c3_.status as status 
                    FROM "client_order" c3_ 
                    LEFT JOIN service_material s0_ ON c3_.id = s0_.order_id 
                    LEFT JOIN service_work s1_ ON c3_.id = s1_.order_id 
                    LEFT JOIN "order_payment" o2_ ON c3_.id = o2_.order_id 
                    WHERE c3_.status NOT IN (30, 50, 21) 
                    AND c3_.office_id = officeId
                    GROUP BY c3_.id
                    ORDER by c3_.id
                    ) as q1
                ) as q2
            $$ language sql; ');
    }
}
