<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180202103929 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE service_work DROP CONSTRAINT fk_9d0ddc6ccf2b4154');
        $this->addSql('DROP SEQUENCE service_work_type_id_seq CASCADE');
        $this->addSql('CREATE TABLE service_work_action_type (id SERIAL NOT NULL, name VARCHAR(128) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_42BB2BF5BF396750 ON service_work_action_type (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_42BB2BF55E237E06 ON service_work_action_type (name)');
        $this->addSql('DROP TABLE service_work_type');
        $this->addSql('ALTER TABLE service_work ADD CONSTRAINT FK_9D0DDC6CCF2B4154 FOREIGN KEY (service_work_type_id) REFERENCES service_work_action_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE event_spare_part_service ADD office_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE event_spare_part_service ADD CONSTRAINT FK_1F025F1DFFA0C224 FOREIGN KEY (office_id) REFERENCES office (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1F025F1DFFA0C224 ON event_spare_part_service (office_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE service_work DROP CONSTRAINT FK_9D0DDC6CCF2B4154');
        $this->addSql('CREATE SEQUENCE service_work_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE service_work_type (id SERIAL NOT NULL, name VARCHAR(128) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_2d8cdc5bbf396750 ON service_work_type (id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_2d8cdc5b5e237e06 ON service_work_type (name)');
        $this->addSql('DROP TABLE service_work_action_type');
        $this->addSql('ALTER TABLE service_work DROP CONSTRAINT fk_9d0ddc6ccf2b4154');
        $this->addSql('ALTER TABLE service_work ADD CONSTRAINT fk_9d0ddc6ccf2b4154 FOREIGN KEY (service_work_type_id) REFERENCES service_work_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE event_spare_part_service DROP CONSTRAINT FK_1F025F1DFFA0C224');
        $this->addSql('DROP INDEX IDX_1F025F1DFFA0C224');
        $this->addSql('ALTER TABLE event_spare_part_service DROP office_id');
    }
}
