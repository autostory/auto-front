<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180627105955 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE box_schedule DROP CONSTRAINT fk_bffd35f1b986bea');
        $this->addSql('DROP INDEX idx_bffd35f1b986bea');
        $this->addSql('ALTER TABLE box_schedule RENAME COLUMN order_box_id TO order_id');
        $this->addSql('ALTER TABLE box_schedule ADD CONSTRAINT FK_BFFD35F8D9F6D38 FOREIGN KEY (order_id) REFERENCES "order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_BFFD35F8D9F6D38 ON box_schedule (order_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE box_schedule DROP CONSTRAINT FK_BFFD35F8D9F6D38');
        $this->addSql('DROP INDEX IDX_BFFD35F8D9F6D38');
        $this->addSql('ALTER TABLE box_schedule RENAME COLUMN order_id TO order_box_id');
        $this->addSql('ALTER TABLE box_schedule ADD CONSTRAINT fk_bffd35f1b986bea FOREIGN KEY (order_box_id) REFERENCES "order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_bffd35f1b986bea ON box_schedule (order_box_id)');
    }
}
