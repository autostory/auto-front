<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180917121317 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE office ALTER title SET NOT NULL');
        $this->addSql('ALTER TABLE service_material ADD creator_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_material ADD created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE service_material ADD updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE service_material ADD CONSTRAINT FK_85C82EA861220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_85C82EA861220EA6 ON service_material (creator_id)');
        $this->addSql('ALTER TABLE service_work ADD creator_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_work ADD created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE service_work ADD updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE service_work DROP name');
        $this->addSql('ALTER TABLE service_work DROP manufacturer');
        $this->addSql('ALTER TABLE service_work DROP spare_part_identifier');
        $this->addSql('ALTER TABLE service_work ADD CONSTRAINT FK_9D0DDC6C61220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9D0DDC6C61220EA6 ON service_work (creator_id)');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE office ALTER title DROP NOT NULL');
        $this->addSql('ALTER TABLE service_material DROP CONSTRAINT FK_85C82EA861220EA6');
        $this->addSql('DROP INDEX IDX_85C82EA861220EA6');
        $this->addSql('ALTER TABLE service_material DROP creator_id');
        $this->addSql('ALTER TABLE service_material DROP created_at');
        $this->addSql('ALTER TABLE service_material DROP updated_at');
        $this->addSql('ALTER TABLE service_work DROP CONSTRAINT FK_9D0DDC6C61220EA6');
        $this->addSql('DROP INDEX IDX_9D0DDC6C61220EA6');
        $this->addSql('ALTER TABLE service_work ADD name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE service_work ADD manufacturer VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE service_work ADD spare_part_identifier VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE service_work DROP creator_id');
        $this->addSql('ALTER TABLE service_work DROP created_at');
        $this->addSql('ALTER TABLE service_work DROP updated_at');
    }
}
