<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171127115842 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('
            CREATE TABLE oauth_clients
            (
              client_id character varying(80) NOT NULL,
              client_secret character varying(80) NOT NULL,
              redirect_uri character varying(2000) NOT NULL,
              grant_types character varying(80),
              scope character varying(2000),
              user_id character varying(255),
              CONSTRAINT clients_client_id_pk PRIMARY KEY (client_id)
            )
        ');

        $this->addSql('
            CREATE TABLE oauth_access_tokens
            (
              access_token character varying(40) NOT NULL,
              client_id character varying(80) NOT NULL,
              user_id character varying(255),
              expires timestamp(0) without time zone NOT NULL,
              scope character varying(2000),
              CONSTRAINT access_token_pk PRIMARY KEY (access_token)
            )
        ');

        $this->addSql('
            CREATE TABLE oauth_refresh_tokens
            (
              refresh_token character varying(40) NOT NULL,
              client_id character varying(80) NOT NULL,
              user_id character varying(255),
              expires timestamp(0) without time zone NOT NULL,
              scope character varying(2000),
              CONSTRAINT refresh_token_pk PRIMARY KEY (refresh_token)
            )
        ');

        $this->addSql('
            CREATE TABLE oauth_scopes
            (
              type character varying(255) NOT NULL DEFAULT \'supported\'::character varying,
              scope character varying(2000),
              client_id character varying(80),
              is_default smallint
            )
        ');

        $this->addSql('
            INSERT INTO oauth_clients (client_id, client_secret, redirect_uri) VALUES (\'ah-frontend\', \'\', \'\')
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE oauth_scopes');
        $this->addSql('DROP TABLE oauth_clients');
        $this->addSql('DROP TABLE oauth_access_tokens');
        $this->addSql('DROP TABLE oauth_refresh_tokens');
    }
}
