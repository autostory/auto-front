<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180708151642 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE car_mileage (id SERIAL NOT NULL, car_id INT NOT NULL, order_id INT DEFAULT NULL, creator_id INT DEFAULT NULL, value NUMERIC(10, 0) NOT NULL, measured_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_51603C8BF396750 ON car_mileage (id)');
        $this->addSql('CREATE INDEX IDX_51603C8C3C6F69F ON car_mileage (car_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_51603C88D9F6D38 ON car_mileage (order_id)');
        $this->addSql('CREATE INDEX IDX_51603C861220EA6 ON car_mileage (creator_id)');
        $this->addSql('ALTER TABLE car_mileage ADD CONSTRAINT FK_51603C8C3C6F69F FOREIGN KEY (car_id) REFERENCES car (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_mileage ADD CONSTRAINT FK_51603C88D9F6D38 FOREIGN KEY (order_id) REFERENCES "client_order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_mileage ADD CONSTRAINT FK_51603C861220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE car_mileage');
    }
}
