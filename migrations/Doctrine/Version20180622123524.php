<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180622123524 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE request (id SERIAL NOT NULL, client_id INT DEFAULT NULL, car_id INT DEFAULT NULL, creator_id INT DEFAULT NULL, make_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, userMessage VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3B978F9FBF396750 ON request (id)');
        $this->addSql('CREATE INDEX IDX_3B978F9F19EB6921 ON request (client_id)');
        $this->addSql('CREATE INDEX IDX_3B978F9FC3C6F69F ON request (car_id)');
        $this->addSql('CREATE INDEX IDX_3B978F9F61220EA6 ON request (creator_id)');
        $this->addSql('ALTER TABLE request ADD CONSTRAINT FK_3B978F9F19EB6921 FOREIGN KEY (client_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE request ADD CONSTRAINT FK_3B978F9FC3C6F69F FOREIGN KEY (car_id) REFERENCES car (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE request ADD CONSTRAINT FK_3B978F9F61220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE request');
    }
}
