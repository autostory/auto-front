<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180905085112 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE account_activate ALTER token TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE identity ALTER login TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE basic_global_service_type ALTER name TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE unit_measure_type ALTER name TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE basic_fuel_type ALTER name TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE basic_fuel_type ALTER unit_name TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE car ADD assume_vin VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE car ALTER vin TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE car ALTER registration_sign_number TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE car ALTER color TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE password_restore ALTER token TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE user_communication ALTER type TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE service_work_action_type ALTER name TYPE VARCHAR(255)');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE account_activate ALTER token TYPE VARCHAR(128)');
        $this->addSql('ALTER TABLE basic_global_service_type ALTER name TYPE VARCHAR(128)');
        $this->addSql('ALTER TABLE service_work_action_type ALTER name TYPE VARCHAR(128)');
        $this->addSql('ALTER TABLE password_restore ALTER token TYPE VARCHAR(128)');
        $this->addSql('ALTER TABLE basic_fuel_type ALTER name TYPE VARCHAR(128)');
        $this->addSql('ALTER TABLE basic_fuel_type ALTER unit_name TYPE VARCHAR(128)');
        $this->addSql('ALTER TABLE user_communication ALTER type TYPE VARCHAR(128)');
        $this->addSql('ALTER TABLE identity ALTER login TYPE VARCHAR(128)');
        $this->addSql('ALTER TABLE car DROP assume_vin');
        $this->addSql('ALTER TABLE car ALTER vin TYPE VARCHAR(128)');
        $this->addSql('ALTER TABLE car ALTER color TYPE VARCHAR(128)');
        $this->addSql('ALTER TABLE car ALTER registration_sign_number TYPE VARCHAR(128)');
        $this->addSql('ALTER TABLE unit_measure_type ALTER name TYPE VARCHAR(128)');
    }
}
