<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180201173730 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE spare_part (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, parent INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E3D09D36BF396750 ON spare_part (id)');
        $this->addSql('CREATE TABLE basic_global_service_type (id SERIAL NOT NULL, name VARCHAR(128) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9D487F29BF396750 ON basic_global_service_type (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9D487F295E237E06 ON basic_global_service_type (name)');
        $this->addSql('CREATE TABLE service_work_type (id SERIAL NOT NULL, name VARCHAR(128) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D8CDC5BBF396750 ON service_work_type (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D8CDC5B5E237E06 ON service_work_type (name)');
        $this->addSql('CREATE TABLE service_work (id SERIAL NOT NULL, service_work_type_id INT NOT NULL, spare_part_id INT NOT NULL, costPerUnit NUMERIC(10, 2) DEFAULT NULL, amount NUMERIC(10, 0) DEFAULT NULL, totalCost NUMERIC(10, 2) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, manufacturer VARCHAR(255) DEFAULT NULL, spare_part_identifier VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9D0DDC6CBF396750 ON service_work (id)');
        $this->addSql('CREATE INDEX IDX_9D0DDC6CCF2B4154 ON service_work (service_work_type_id)');
        $this->addSql('CREATE INDEX IDX_9D0DDC6C49B7A72 ON service_work (spare_part_id)');
        $this->addSql('CREATE TABLE event_spare_part_service (id SERIAL NOT NULL, initiator_id INT NOT NULL, firm_id INT DEFAULT NULL, client_id INT NOT NULL, car_id INT NOT NULL, service_work_id INT NOT NULL, global_service_type_id INT NOT NULL, creator_id INT DEFAULT NULL, mileage INT NOT NULL, date TIMESTAMP(0) WITH TIME ZONE NOT NULL, total_cost NUMERIC(10, 2) NOT NULL, self_service BOOLEAN NOT NULL, cause_of_accident BOOLEAN NOT NULL, organisation_name VARCHAR(255) DEFAULT NULL, note VARCHAR(255) DEFAULT NULL, createdAt TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, updatedAt TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1F025F1DBF396750 ON event_spare_part_service (id)');
        $this->addSql('CREATE INDEX IDX_1F025F1D7DB3B714 ON event_spare_part_service (initiator_id)');
        $this->addSql('CREATE INDEX IDX_1F025F1D89AF7860 ON event_spare_part_service (firm_id)');
        $this->addSql('CREATE INDEX IDX_1F025F1D19EB6921 ON event_spare_part_service (client_id)');
        $this->addSql('CREATE INDEX IDX_1F025F1DC3C6F69F ON event_spare_part_service (car_id)');
        $this->addSql('CREATE INDEX IDX_1F025F1D74B00147 ON event_spare_part_service (service_work_id)');
        $this->addSql('CREATE INDEX IDX_1F025F1DE26C74E9 ON event_spare_part_service (global_service_type_id)');
        $this->addSql('CREATE INDEX IDX_1F025F1D61220EA6 ON event_spare_part_service (creator_id)');
        $this->addSql('ALTER TABLE service_work ADD CONSTRAINT FK_9D0DDC6CCF2B4154 FOREIGN KEY (service_work_type_id) REFERENCES service_work_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_work ADD CONSTRAINT FK_9D0DDC6C49B7A72 FOREIGN KEY (spare_part_id) REFERENCES spare_part (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE event_spare_part_service ADD CONSTRAINT FK_1F025F1D7DB3B714 FOREIGN KEY (initiator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE event_spare_part_service ADD CONSTRAINT FK_1F025F1D89AF7860 FOREIGN KEY (firm_id) REFERENCES firm (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE event_spare_part_service ADD CONSTRAINT FK_1F025F1D19EB6921 FOREIGN KEY (client_id) REFERENCES firm (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE event_spare_part_service ADD CONSTRAINT FK_1F025F1DC3C6F69F FOREIGN KEY (car_id) REFERENCES car (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE event_spare_part_service ADD CONSTRAINT FK_1F025F1D74B00147 FOREIGN KEY (service_work_id) REFERENCES service_work (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE event_spare_part_service ADD CONSTRAINT FK_1F025F1DE26C74E9 FOREIGN KEY (global_service_type_id) REFERENCES basic_global_service_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE event_spare_part_service ADD CONSTRAINT FK_1F025F1D61220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE service_work DROP CONSTRAINT FK_9D0DDC6C49B7A72');
        $this->addSql('ALTER TABLE event_spare_part_service DROP CONSTRAINT FK_1F025F1DE26C74E9');
        $this->addSql('ALTER TABLE service_work DROP CONSTRAINT FK_9D0DDC6CCF2B4154');
        $this->addSql('ALTER TABLE event_spare_part_service DROP CONSTRAINT FK_1F025F1D74B00147');
        $this->addSql('DROP TABLE spare_part');
        $this->addSql('DROP TABLE basic_global_service_type');
        $this->addSql('DROP TABLE service_work_type');
        $this->addSql('DROP TABLE service_work');
        $this->addSql('DROP TABLE event_spare_part_service');
    }
}
