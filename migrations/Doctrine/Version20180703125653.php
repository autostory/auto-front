<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180703125653 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER INDEX uniq_e3d09d36bf396750 RENAME TO UNIQ_41CB791EBF396750');
        $this->addSql('ALTER INDEX idx_e3d09d36727aca70 RENAME TO IDX_41CB791E727ACA70');
        $this->addSql('ALTER TABLE service_work DROP CONSTRAINT fk_9d0ddc6c49b7a72');
        $this->addSql('DROP INDEX idx_9d0ddc6c49b7a72');
        $this->addSql('ALTER TABLE service_work RENAME COLUMN spare_part_id TO spare_part_node_id');
        $this->addSql('ALTER TABLE service_work ADD CONSTRAINT FK_9D0DDC6C82B2928C FOREIGN KEY (spare_part_node_id) REFERENCES spare_part_node (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9D0DDC6C82B2928C ON service_work (spare_part_node_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE service_work DROP CONSTRAINT FK_9D0DDC6C82B2928C');
        $this->addSql('DROP INDEX IDX_9D0DDC6C82B2928C');
        $this->addSql('ALTER TABLE service_work RENAME COLUMN spare_part_node_id TO spare_part_id');
        $this->addSql('ALTER TABLE service_work ADD CONSTRAINT fk_9d0ddc6c49b7a72 FOREIGN KEY (spare_part_id) REFERENCES spare_part_node (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_9d0ddc6c49b7a72 ON service_work (spare_part_id)');
        $this->addSql('ALTER INDEX idx_41cb791e727aca70 RENAME TO idx_e3d09d36727aca70');
        $this->addSql('ALTER INDEX uniq_41cb791ebf396750 RENAME TO uniq_e3d09d36bf396750');
    }
}
