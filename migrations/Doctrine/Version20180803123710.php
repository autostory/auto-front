<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180803123710 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE office_work_time ADD holiday BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE office_work_time ALTER start_time DROP NOT NULL');
        $this->addSql('ALTER TABLE office_work_time ALTER end_time DROP NOT NULL');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE office_work_time DROP holiday');
        $this->addSql('ALTER TABLE office_work_time ALTER start_time SET NOT NULL');
        $this->addSql('ALTER TABLE office_work_time ALTER end_time SET NOT NULL');
    }
}
