<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171216085951 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("INSERT INTO user_role VALUES (nextval('user_role_id_seq'), NULL, 'admin')");

        $this->addSql("INSERT INTO user_role VALUES (nextval('user_role_id_seq'), NULL, 'manager')");
        $this->addSql("INSERT INTO user_role VALUES (nextval('user_role_id_seq'), NULL, 'manager-boss')");

        $this->addSql("INSERT INTO user_role VALUES (nextval('user_role_id_seq'), NULL, 'client')");

        $this->addSql('UPDATE user_role
            SET parent_id = (SELECT id from user_role WHERE role_id = \'manager\')
            WHERE role_id = \'manager-boss\'');

        $this->addSql("INSERT INTO user_role VALUES (nextval('user_role_id_seq'), NULL, 'authenticated-user')");

        $this->addSql('UPDATE user_role
            SET parent_id = (SELECT id from user_role WHERE role_id = \'authenticated-user\')
            WHERE role_id = \'manager\' OR role_id = \'admin\' OR role_id = \'client\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        // Down миграция не требуется.
    }
}
