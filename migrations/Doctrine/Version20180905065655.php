<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180905065655 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        // Добавляем проверку что должен быть указан VIN или гос номер на уровне БД

        $this->addSql('
            create or replace function vin_or_sign_not_empty()
            returns trigger language plpgsql as $$
            declare
                vinOrSignNotEmpty boolean;
            begin
            SELECT 
                (new.registration_sign_number IS NOT NULL AND TRIM(new.registration_sign_number) <> \'\' ) 
                OR
                (new.vin IS NOT NULL AND TRIM(new.vin) <> \'\' ) 
                OR
                (new.assume_vin IS NOT NULL AND TRIM(new.assume_vin) <> \'\' ) 
                into vinOrSignNotEmpty;
            
                if not vinOrSignNotEmpty then
                    raise exception \'VIN or assumeVin or registration sign number must be provided\';
                end if;
                return new;
            end $$;
        ');
        $this->addSql('CREATE TRIGGER t_vin_or_sign_not_empty BEFORE INSERT OR UPDATE ON car FOR EACH ROW EXECUTE PROCEDURE vin_or_sign_not_empty();');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TRIGGER t_vin_or_sign_not_empty ON car;');
        $this->addSql('DROP FUNCTION vin_or_sign_not_empty();');
    }
}
