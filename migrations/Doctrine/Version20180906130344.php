<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180906130344 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX uniq_6a95e9c46838dc9c');
        $this->addSql('DROP INDEX uniq_6a95e9c4cbe53735');
        $this->addSql('CREATE UNIQUE INDEX client_email_idx ON identity (registration_email) WHERE (user_type = \'client\')');
        $this->addSql('CREATE UNIQUE INDEX client_phone_idx ON identity (registration_phone) WHERE (user_type = \'client\')');
        $this->addSql('CREATE UNIQUE INDEX manager_email_idx ON identity (registration_email) WHERE (user_type = \'manager\')');
        $this->addSql('CREATE UNIQUE INDEX manager_phone_idx ON identity (registration_phone) WHERE (user_type = \'manager\')');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX client_email_idx');
        $this->addSql('DROP INDEX client_phone_idx');
        $this->addSql('DROP INDEX manager_email_idx');
        $this->addSql('DROP INDEX manager_phone_idx');
        $this->addSql('CREATE UNIQUE INDEX uniq_6a95e9c46838dc9c ON identity (registration_email)');
        $this->addSql('CREATE UNIQUE INDEX uniq_6a95e9c4cbe53735 ON identity (registration_phone)');
    }
}
