<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180627105845 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE box_schedule ADD order_box_id INT NOT NULL');
        $this->addSql('ALTER TABLE box_schedule ADD CONSTRAINT FK_BFFD35F35A6A3BC FOREIGN KEY (office_box_id) REFERENCES office_box (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE box_schedule ADD CONSTRAINT FK_BFFD35F1B986BEA FOREIGN KEY (order_box_id) REFERENCES "order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_BFFD35F1B986BEA ON box_schedule (order_box_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE box_schedule DROP CONSTRAINT FK_BFFD35F35A6A3BC');
        $this->addSql('ALTER TABLE box_schedule DROP CONSTRAINT FK_BFFD35F1B986BEA');
        $this->addSql('DROP INDEX IDX_BFFD35F1B986BEA');
        $this->addSql('ALTER TABLE box_schedule DROP order_box_id');
    }
}
