<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180622183230 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE request_communication (id SERIAL NOT NULL, request_id INT NOT NULL, manager_id INT NOT NULL, creator_id INT DEFAULT NULL, result_id INT NOT NULL, reject_reason_id INT DEFAULT NULL, reject_reason_description INT DEFAULT NULL, communication_date_time TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, next_communication_date_time TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_71BCC554BF396750 ON request_communication (id)');
        $this->addSql('CREATE INDEX IDX_71BCC554427EB8A5 ON request_communication (request_id)');
        $this->addSql('CREATE INDEX IDX_71BCC554783E3463 ON request_communication (manager_id)');
        $this->addSql('CREATE INDEX IDX_71BCC55461220EA6 ON request_communication (creator_id)');
        $this->addSql('ALTER TABLE request_communication ADD CONSTRAINT FK_71BCC554427EB8A5 FOREIGN KEY (request_id) REFERENCES request (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE request_communication ADD CONSTRAINT FK_71BCC554783E3463 FOREIGN KEY (manager_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE request_communication ADD CONSTRAINT FK_71BCC55461220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE request_communication');
    }
}
