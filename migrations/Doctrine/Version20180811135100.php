<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180811135100 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE client_order RENAME COLUMN phonesameasinprofile TO phone_same_as_in_profile');
        $this->addSql('ALTER TABLE client_order RENAME COLUMN estimatedClientArrivalDate TO estimated_client_arrival_date');
        $this->addSql('ALTER TABLE client_order RENAME COLUMN factClientVisitDateTime TO fact_client_visit_date_time');
        $this->addSql('ALTER TABLE client_order RENAME COLUMN factCarReturnDateTime TO fact_car_return_date_time');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE client_order RENAME COLUMN phone_same_as_in_profile TO phonesameasinprofile');
        $this->addSql('ALTER TABLE client_order RENAME COLUMN estimated_client_arrival_date TO estimatedClientArrivalDate');
        $this->addSql('ALTER TABLE client_order RENAME COLUMN fact_client_visit_date_time TO factClientVisitDateTime');
        $this->addSql('ALTER TABLE client_order RENAME COLUMN fact_car_return_date_time TO factCarReturnDateTime');
    }
}
