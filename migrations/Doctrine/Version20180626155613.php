<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180626155613 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE officebox_id_seq CASCADE');
        $this->addSql('CREATE TABLE "order_payment" (id SERIAL NOT NULL, firm_id INT NOT NULL, cashier_id INT NOT NULL, order_id INT NOT NULL, creator_id INT DEFAULT NULL, payment_type INT NOT NULL, note VARCHAR(255) DEFAULT NULL, amount NUMERIC(10, 0) NOT NULL, make_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9B522D46BF396750 ON "order_payment" (id)');
        $this->addSql('CREATE INDEX IDX_9B522D4689AF7860 ON "order_payment" (firm_id)');
        $this->addSql('CREATE INDEX IDX_9B522D462EDB0489 ON "order_payment" (cashier_id)');
        $this->addSql('CREATE INDEX IDX_9B522D468D9F6D38 ON "order_payment" (order_id)');
        $this->addSql('CREATE INDEX IDX_9B522D4661220EA6 ON "order_payment" (creator_id)');
        $this->addSql('ALTER TABLE "order_payment" ADD CONSTRAINT FK_9B522D4689AF7860 FOREIGN KEY (firm_id) REFERENCES firm (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "order_payment" ADD CONSTRAINT FK_9B522D462EDB0489 FOREIGN KEY (cashier_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "order_payment" ADD CONSTRAINT FK_9B522D468D9F6D38 FOREIGN KEY (order_id) REFERENCES "order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "order_payment" ADD CONSTRAINT FK_9B522D4661220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER INDEX uniq_2bf48b6dbf396750 RENAME TO UNIQ_5F8164BABF396750');
        $this->addSql('ALTER INDEX idx_2bf48b6dffa0c224 RENAME TO IDX_5F8164BAFFA0C224');
        $this->addSql('ALTER INDEX idx_2bf48b6d61220ea6 RENAME TO IDX_5F8164BA61220EA6');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE officebox_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('DROP TABLE "order_payment"');
        $this->addSql('ALTER INDEX idx_5f8164ba61220ea6 RENAME TO idx_2bf48b6d61220ea6');
        $this->addSql('ALTER INDEX uniq_5f8164babf396750 RENAME TO uniq_2bf48b6dbf396750');
        $this->addSql('ALTER INDEX idx_5f8164baffa0c224 RENAME TO idx_2bf48b6dffa0c224');
    }
}
