<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180202033501 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE spare_part RENAME COLUMN parent TO parent_id');
        $this->addSql('ALTER TABLE spare_part ADD CONSTRAINT FK_E3D09D36727ACA70 FOREIGN KEY (parent_id) REFERENCES spare_part (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E3D09D36727ACA70 ON spare_part (parent_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE spare_part DROP CONSTRAINT FK_E3D09D36727ACA70');
        $this->addSql('DROP INDEX IDX_E3D09D36727ACA70');
        $this->addSql('ALTER TABLE spare_part RENAME COLUMN parent_id TO parent');
    }
}
