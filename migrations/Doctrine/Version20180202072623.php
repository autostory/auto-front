<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180202072623 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE spare_part_service_event_works (event_spare_part_service_id INT NOT NULL, service_work_id INT NOT NULL, PRIMARY KEY(event_spare_part_service_id, service_work_id))');
        $this->addSql('CREATE INDEX IDX_67CFA436A192D5BB ON spare_part_service_event_works (event_spare_part_service_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_67CFA43674B00147 ON spare_part_service_event_works (service_work_id)');
        $this->addSql('ALTER TABLE spare_part_service_event_works ADD CONSTRAINT FK_67CFA436A192D5BB FOREIGN KEY (event_spare_part_service_id) REFERENCES event_spare_part_service (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE spare_part_service_event_works ADD CONSTRAINT FK_67CFA43674B00147 FOREIGN KEY (service_work_id) REFERENCES service_work (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE identity ADD created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE identity ADD updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE identity DROP createdat');
        $this->addSql('ALTER TABLE identity DROP updatedat');
        $this->addSql('ALTER TABLE office ADD created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE office ADD updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE office DROP createdat');
        $this->addSql('ALTER TABLE office DROP updatedat');
        $this->addSql('ALTER TABLE car ADD created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE car ADD updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE car DROP createdat');
        $this->addSql('ALTER TABLE car DROP updatedat');
        $this->addSql('ALTER TABLE user_communication ADD created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE user_communication ADD updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE user_communication DROP createdat');
        $this->addSql('ALTER TABLE user_communication DROP updatedat');
        $this->addSql('ALTER TABLE event_gas ADD created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE event_gas ADD updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE event_gas DROP createdat');
        $this->addSql('ALTER TABLE event_gas DROP updatedat');
        $this->addSql('ALTER TABLE event_gas ALTER date DROP NOT NULL');
        $this->addSql('ALTER TABLE event_spare_part_service DROP CONSTRAINT fk_1f025f1d74b00147');
        $this->addSql('DROP INDEX idx_1f025f1d74b00147');
        $this->addSql('ALTER TABLE event_spare_part_service ADD created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE event_spare_part_service ADD updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE event_spare_part_service DROP service_work_id');
        $this->addSql('ALTER TABLE event_spare_part_service DROP createdat');
        $this->addSql('ALTER TABLE event_spare_part_service DROP updatedat');
        $this->addSql('ALTER TABLE event_spare_part_service ALTER date DROP NOT NULL');
        $this->addSql('ALTER TABLE firm ADD created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE firm ADD updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE firm DROP createdat');
        $this->addSql('ALTER TABLE firm DROP updatedat');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE spare_part_service_event_works');
        $this->addSql('ALTER TABLE office ADD createdat TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE office ADD updatedat TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE office DROP created_at');
        $this->addSql('ALTER TABLE office DROP updated_at');
        $this->addSql('ALTER TABLE car ADD createdat TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE car ADD updatedat TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE car DROP created_at');
        $this->addSql('ALTER TABLE car DROP updated_at');
        $this->addSql('ALTER TABLE identity ADD createdat TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE identity ADD updatedat TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE identity DROP created_at');
        $this->addSql('ALTER TABLE identity DROP updated_at');
        $this->addSql('ALTER TABLE event_gas ADD createdat TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE event_gas ADD updatedat TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE event_gas DROP created_at');
        $this->addSql('ALTER TABLE event_gas DROP updated_at');
        $this->addSql('ALTER TABLE event_gas ALTER date SET NOT NULL');
        $this->addSql('ALTER TABLE event_spare_part_service ADD service_work_id INT NOT NULL');
        $this->addSql('ALTER TABLE event_spare_part_service ADD createdat TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE event_spare_part_service ADD updatedat TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE event_spare_part_service DROP created_at');
        $this->addSql('ALTER TABLE event_spare_part_service DROP updated_at');
        $this->addSql('ALTER TABLE event_spare_part_service ALTER date SET NOT NULL');
        $this->addSql('ALTER TABLE event_spare_part_service ADD CONSTRAINT fk_1f025f1d74b00147 FOREIGN KEY (service_work_id) REFERENCES service_work (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_1f025f1d74b00147 ON event_spare_part_service (service_work_id)');
        $this->addSql('ALTER TABLE firm ADD createdat TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE firm ADD updatedat TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE firm DROP created_at');
        $this->addSql('ALTER TABLE firm DROP updated_at');
        $this->addSql('ALTER TABLE user_communication ADD createdat TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE user_communication ADD updatedat TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE user_communication DROP created_at');
        $this->addSql('ALTER TABLE user_communication DROP updated_at');
    }
}
