<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180726125406 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE "client_point_log" (id SERIAL NOT NULL, client_id INT NOT NULL, order_payment_id INT NOT NULL, creator_id INT DEFAULT NULL, amount NUMERIC(10, 0) NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_37D33A53BF396750 ON "client_point_log" (id)');
        $this->addSql('CREATE INDEX IDX_37D33A5319EB6921 ON "client_point_log" (client_id)');
        $this->addSql('CREATE INDEX IDX_37D33A53B7195EEE ON "client_point_log" (order_payment_id)');
        $this->addSql('CREATE INDEX IDX_37D33A5361220EA6 ON "client_point_log" (creator_id)');
        $this->addSql('ALTER TABLE "client_point_log" ADD CONSTRAINT FK_37D33A5319EB6921 FOREIGN KEY (client_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "client_point_log" ADD CONSTRAINT FK_37D33A53B7195EEE FOREIGN KEY (order_payment_id) REFERENCES "order_payment" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "client_point_log" ADD CONSTRAINT FK_37D33A5361220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE identity ADD points NUMERIC(10, 0) DEFAULT 0');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE "client_point_log"');
        $this->addSql('ALTER TABLE identity DROP points');
    }
}
