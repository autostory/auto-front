<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180903164716 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('
            CREATE OR REPLACE FUNCTION json_equals(json, json)
              RETURNS BOOLEAN
              LANGUAGE sql
              IMMUTABLE
              STRICT
            AS $function$
              SELECT CASE f1
                WHEN \'{\' THEN -- object
                  CASE f2
                    WHEN \'{\' THEN (
                      SELECT COALESCE(bool_and(k1 IS NOT NULL AND k2 IS NOT NULL AND json_equals(v1, v2)), TRUE)
                      FROM (SELECT DISTINCT ON (k1) * FROM json_each($1) AS j1(k1, v1) ORDER BY k1, row_number() OVER () DESC) AS j1
                      FULL JOIN (SELECT DISTINCT ON (k2) * FROM json_each($2) AS j2(k2, v2) ORDER BY k2, row_number() OVER () DESC) AS j2 ON j1.k1 = j2.k2
                    )
                    ELSE FALSE
                  END
                WHEN \'[\' THEN -- array
                  CASE f2
                    WHEN \'[\' THEN (
                      SELECT COALESCE(bool_and(r1 IS NOT NULL AND r2 IS NOT NULL AND json_equals(e1, e2)), TRUE)
                      FROM (SELECT e1, row_number() OVER () AS r1 FROM json_array_elements($1) AS e1) AS e1
                      FULL JOIN (SELECT e2, row_number() OVER () AS r2 FROM json_array_elements($2) AS e2) AS e2 ON e1.r1 = e2.r2
                    )
                    ELSE FALSE
                  END
                WHEN \'n\' THEN -- null
                  CASE f2
                    WHEN \'n\' THEN TRUE
                    ELSE FALSE
                  END
                WHEN \'t\' THEN -- true
                  CASE f2
                    WHEN \'t\' THEN TRUE
                    ELSE FALSE
                  END
                WHEN \'f\' THEN -- false
                  CASE f2
                    WHEN \'f\' THEN TRUE
                    ELSE FALSE
                  END
                WHEN \'"\' THEN -- string
                  CASE f2
                    WHEN \'"\' THEN (CAST(\'[\' || j1 || \']\' AS json) ->> 0)
                            = (CAST(\'[\' || j2 || \']\' AS json) ->> 0)
                    ELSE FALSE
                  END
                ELSE -- number
                  CASE f2
                    WHEN \'{\' THEN FALSE
                    WHEN \'[\' THEN FALSE
                    WHEN \'n\' THEN FALSE
                    WHEN \'t\' THEN FALSE
                    WHEN \'f\' THEN FALSE
                    WHEN \'"\' THEN FALSE
                    ELSE CAST(CAST(\'[\' || j1 || \']\' AS json) ->> 0 AS NUMERIC)
                   = CAST(CAST(\'[\' || j2 || \']\' AS json) ->> 0 AS NUMERIC)
                  END
              END
              FROM (
                SELECT TRIM(LEADING E\'\x20\x09\x0A\x0D\' FROM CAST($1 AS text)) AS j1,
                       TRIM(LEADING E\'\x20\x09\x0A\x0D\' FROM CAST($2 AS text)) AS j2
              ) AS jsons,
              LATERAL (
                SELECT SUBSTRING(j1 FROM 1 FOR 1) AS f1,
                       SUBSTRING(j2 FROM 1 FOR 1) AS f2
              ) AS firsts
            $function$;
        ');

        $this->addSql('
            CREATE OR REPLACE FUNCTION change_trigger() RETURNS trigger AS $$
               BEGIN
                 IF TG_OP = \'INSERT\'
                 THEN INSERT INTO t_history (
                        entity_id, tabname, schemaname, operation, new_val
                      ) VALUES (
                        NEW.id, TG_RELNAME, TG_TABLE_SCHEMA, TG_OP, row_to_json(NEW)
                      );
                   RETURN NEW;
                 ELSIF  TG_OP = \'UPDATE\'
                 THEN
                    INSERT INTO t_history (
                         entity_id, tabname, schemaname, operation, new_val, old_val, differ
                       )
                       VALUES (
                         OLD.id, TG_RELNAME, TG_TABLE_SCHEMA, TG_OP, row_to_json(NEW), row_to_json(OLD), OLD is distinct from NEW 
                       );
                       RETURN NEW;
                 ELSIF TG_OP = \'DELETE\'
                 THEN
                   INSERT INTO t_history (
                    entity_id, tabname, schemaname, operation, old_val
                   )
                     VALUES (
                       OLD.id, TG_RELNAME, TG_TABLE_SCHEMA, TG_OP, row_to_json(OLD)
                     );
                     RETURN OLD;
                 END IF;
               END;
            $$ LANGUAGE \'plpgsql\' SECURITY DEFINER;
        ');

        $this->addSql('
            CREATE OR REPLACE FUNCTION app_user_id() returns INT as $$
                SELECT CAST (current_setting(\'myapp.user_id\') as INT)
            $$ language sql volatile; 
        ');

        $this->addSql('CREATE TRIGGER t_update_delete BEFORE UPDATE OR DELETE ON box_schedule FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_update_delete BEFORE UPDATE OR DELETE ON car FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_update_delete BEFORE UPDATE OR DELETE ON car_mileage FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_update_delete BEFORE UPDATE OR DELETE ON firm FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_update_delete BEFORE UPDATE OR DELETE ON identity FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_update_delete BEFORE UPDATE OR DELETE ON office FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_update_delete BEFORE UPDATE OR DELETE ON office_box FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_update_delete BEFORE UPDATE OR DELETE ON office_work_time FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_update_delete BEFORE UPDATE OR DELETE ON order_payment FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_update_delete BEFORE UPDATE OR DELETE ON request FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_update_delete BEFORE UPDATE OR DELETE ON request_communication FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_update_delete BEFORE UPDATE OR DELETE ON service_material FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_update_delete BEFORE UPDATE OR DELETE ON service_work FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_update_delete BEFORE UPDATE OR DELETE ON client_order FOR EACH ROW EXECUTE PROCEDURE change_trigger();');

        $this->addSql('CREATE TRIGGER t_insert AFTER INSERT ON box_schedule FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_insert AFTER INSERT ON car FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_insert AFTER INSERT ON car_mileage FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_insert AFTER INSERT ON firm FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_insert AFTER INSERT ON identity FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_insert AFTER INSERT ON office FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_insert AFTER INSERT ON office_box FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_insert AFTER INSERT ON office_work_time FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_insert AFTER INSERT ON order_payment FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_insert AFTER INSERT ON request FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_insert AFTER INSERT ON request_communication FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_insert AFTER INSERT ON service_material FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_insert AFTER INSERT ON service_work FOR EACH ROW EXECUTE PROCEDURE change_trigger();');
        $this->addSql('CREATE TRIGGER t_insert AFTER INSERT ON client_order FOR EACH ROW EXECUTE PROCEDURE change_trigger();');

        $this->addSql('
            CREATE TABLE t_history (
                id SERIAL NOT NULL,
                tstamp TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT now(), 
                schemaname TEXT DEFAULT NULL, 
                tabname TEXT DEFAULT NULL, 
                entity_id integer DEFAULT NULL, 
                operation TEXT DEFAULT NULL,
                who TEXT DEFAULT app_user_id(), 
                new_val JSON DEFAULT NULL,
                old_val JSON DEFAULT NULL,
                differ BOOLEAN DEFAULT NULL,
                PRIMARY KEY(id)
            )
        ');

        $this->addSql('COMMENT ON COLUMN t_history.new_val IS \'(DC2Type:json_array)\'');
        $this->addSql('COMMENT ON COLUMN t_history.old_val IS \'(DC2Type:json_array)\'');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_7D96C828BF396750 ON t_history (id)');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE IF EXISTS t_history');

        $this->addSql('DROP TRIGGER IF EXISTS t_update_delete ON box_schedule');
        $this->addSql('DROP TRIGGER IF EXISTS t_update_delete ON car');
        $this->addSql('DROP TRIGGER IF EXISTS t_update_delete ON car_mileage');
        $this->addSql('DROP TRIGGER IF EXISTS t_update_delete ON firm');
        $this->addSql('DROP TRIGGER IF EXISTS t_update_delete ON identity');
        $this->addSql('DROP TRIGGER IF EXISTS t_update_delete ON office');
        $this->addSql('DROP TRIGGER IF EXISTS t_update_delete ON office_box');
        $this->addSql('DROP TRIGGER IF EXISTS t_update_delete ON office_work_time');
        $this->addSql('DROP TRIGGER IF EXISTS t_update_delete ON order_payment');
        $this->addSql('DROP TRIGGER IF EXISTS t_update_delete ON request');
        $this->addSql('DROP TRIGGER IF EXISTS t_update_delete ON request_communication');
        $this->addSql('DROP TRIGGER IF EXISTS t_update_delete ON service_material');
        $this->addSql('DROP TRIGGER IF EXISTS t_update_delete ON service_work');
        $this->addSql('DROP TRIGGER IF EXISTS t_update_delete ON client_order');

        $this->addSql('DROP TRIGGER IF EXISTS t_insert ON box_schedule');
        $this->addSql('DROP TRIGGER IF EXISTS t_insert ON car');
        $this->addSql('DROP TRIGGER IF EXISTS t_insert ON car_mileage');
        $this->addSql('DROP TRIGGER IF EXISTS t_insert ON firm');
        $this->addSql('DROP TRIGGER IF EXISTS t_insert ON identity');
        $this->addSql('DROP TRIGGER IF EXISTS t_insert ON office');
        $this->addSql('DROP TRIGGER IF EXISTS t_insert ON office_box');
        $this->addSql('DROP TRIGGER IF EXISTS t_insert ON office_work_time');
        $this->addSql('DROP TRIGGER IF EXISTS t_insert ON order_payment');
        $this->addSql('DROP TRIGGER IF EXISTS t_insert ON request');
        $this->addSql('DROP TRIGGER IF EXISTS t_insert ON request_communication');
        $this->addSql('DROP TRIGGER IF EXISTS t_insert ON service_material');
        $this->addSql('DROP TRIGGER IF EXISTS t_insert ON service_work');
        $this->addSql('DROP TRIGGER IF EXISTS t_insert ON client_order');

        $this->addSql('DROP FUNCTION IF EXISTS app_user_id()');
        $this->addSql('DROP FUNCTION IF EXISTS change_trigger()');
        $this->addSql('DROP FUNCTION IF EXISTS json_equals(json, json)');
    }
}
