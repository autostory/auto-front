<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180812122723 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE service_work_schedule_linker (boxschedule_id INT NOT NULL, servicework_id INT NOT NULL, PRIMARY KEY(boxschedule_id, servicework_id))');
        $this->addSql('CREATE INDEX IDX_83E583847841B91B ON service_work_schedule_linker (boxschedule_id)');
        $this->addSql('CREATE INDEX IDX_83E583847CC0B16E ON service_work_schedule_linker (servicework_id)');
        $this->addSql('ALTER TABLE service_work_schedule_linker ADD CONSTRAINT FK_83E583847841B91B FOREIGN KEY (boxschedule_id) REFERENCES box_schedule (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_work_schedule_linker ADD CONSTRAINT FK_83E583847CC0B16E FOREIGN KEY (servicework_id) REFERENCES service_work (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE service_work_schedule_linker');
    }
}
