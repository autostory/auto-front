<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181005082247 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TRIGGER uniq_client_car_registration_sign_number_check_on_car_insert ON car;');
        $this->addSql('DROP FUNCTION uniq_client_car_registration_sign_number_check_on_car_insert();');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('
            create or replace function uniq_client_car_registration_sign_number_check_on_car_insert()
            returns trigger language plpgsql as $$
            declare
                clientHaveCarWithSameRegistrationSignNumber boolean;
            begin
                SELECT count(car.id) <> 0 INTO clientHaveCarWithSameRegistrationSignNumber 
                FROM client_cars 
                LEFT JOIN car ON client_cars.car_id = car.id
                WHERE 
                car.registration_sign_number = new.registration_sign_number;
            
                if clientHaveCarWithSameRegistrationSignNumber then
                    raise exception \'New Car have owners with cars witch have same registration sign number as "%"\', new.registration_sign_number;
                end if;
                return new;
            end $$;
        ');

        $this->addSql('CREATE TRIGGER uniq_client_car_registration_sign_number_check_on_car_insert BEFORE INSERT ON car FOR EACH ROW EXECUTE PROCEDURE uniq_client_car_registration_sign_number_check_on_car_insert();');

    }
}
