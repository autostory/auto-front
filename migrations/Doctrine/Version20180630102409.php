<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180630102409 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER INDEX uniq_f5299398bf396750 RENAME TO UNIQ_56440F2FBF396750');
        $this->addSql('ALTER INDEX idx_f529939889af7860 RENAME TO IDX_56440F2F89AF7860');
        $this->addSql('ALTER INDEX idx_f529939819eb6921 RENAME TO IDX_56440F2F19EB6921');
        $this->addSql('ALTER INDEX idx_f5299398c3c6f69f RENAME TO IDX_56440F2FC3C6F69F');
        $this->addSql('ALTER INDEX idx_f529939861220ea6 RENAME TO IDX_56440F2F61220EA6');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER INDEX idx_56440f2fc3c6f69f RENAME TO idx_f5299398c3c6f69f');
        $this->addSql('ALTER INDEX idx_56440f2f89af7860 RENAME TO idx_f529939889af7860');
        $this->addSql('ALTER INDEX idx_56440f2f61220ea6 RENAME TO idx_f529939861220ea6');
        $this->addSql('ALTER INDEX idx_56440f2f19eb6921 RENAME TO idx_f529939819eb6921');
        $this->addSql('ALTER INDEX uniq_56440f2fbf396750 RENAME TO uniq_f5299398bf396750');
    }
}
