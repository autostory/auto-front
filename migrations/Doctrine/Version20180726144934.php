<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180726144934 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE order_payment ADD client_id INT DEFAULT NULL');

        $this->addSql('
            CREATE OR REPLACE FUNCTION migrate20180726144934() RETURNS void AS $$
            DECLARE
                data RECORD;
            BEGIN
                FOR data
                IN
                    SELECT order_payment.id as orderPaymentId, identity.id as clientId from order_payment 
                    LEFT JOIN client_order ON client_order.id = order_payment.order_id
                    LEFT JOIN identity ON client_order.client_id = identity.id
                LOOP
                    EXECUTE \'
                        UPDATE order_payment
                        SET client_id = $2
                        WHERE order_payment.id = $1;
                    \'
                    USING 
                        data.orderPaymentId,
                        data.clientId;
                END LOOP;
            END;
            $$ LANGUAGE plpgsql;
        ');

        $this->addSql('SELECT migrate20180726144934()');
        $this->addSql('DROP FUNCTION migrate20180726144934()');

        $this->addSql('ALTER TABLE order_payment ALTER client_id DROP DEFAULT');
        $this->addSql('ALTER TABLE order_payment ALTER client_id SET NOT NULL');

        $this->addSql('ALTER TABLE order_payment ADD CONSTRAINT FK_9B522D4619EB6921 FOREIGN KEY (client_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9B522D4619EB6921 ON order_payment (client_id)');
        $this->addSql('ALTER TABLE firm ADD points NUMERIC(10, 0) NOT NULL DEFAULT 0');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE firm DROP points');
        $this->addSql('ALTER TABLE "order_payment" DROP CONSTRAINT FK_9B522D4619EB6921');
        $this->addSql('DROP INDEX IDX_9B522D4619EB6921');
        $this->addSql('ALTER TABLE "order_payment" DROP client_id');
    }
}
