<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180704085426 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE unit_measure_type (id SERIAL NOT NULL, name VARCHAR(128) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1991F30ABF396750 ON unit_measure_type (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1991F30A5E237E06 ON unit_measure_type (name)');
        $this->addSql('CREATE TABLE service_material (id SERIAL NOT NULL, order_id INT NOT NULL, unit_measure_type_id INT NOT NULL, cost_per_unit NUMERIC(10, 2) NOT NULL, amount NUMERIC(10, 0) NOT NULL, discount_percent NUMERIC(10, 0) NOT NULL, total_cost NUMERIC(10, 2) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_85C82EA8BF396750 ON service_material (id)');
        $this->addSql('CREATE INDEX IDX_85C82EA88D9F6D38 ON service_material (order_id)');
        $this->addSql('CREATE INDEX IDX_85C82EA8D34F5153 ON service_material (unit_measure_type_id)');
        $this->addSql('ALTER TABLE service_material ADD CONSTRAINT FK_85C82EA88D9F6D38 FOREIGN KEY (order_id) REFERENCES "client_order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE service_material ADD CONSTRAINT FK_85C82EA8D34F5153 FOREIGN KEY (unit_measure_type_id) REFERENCES unit_measure_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE client_order ADD order_reason VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE service_work ADD order_id INT NOT NULL');
        $this->addSql('ALTER TABLE service_work ADD CONSTRAINT FK_9D0DDC6C8D9F6D38 FOREIGN KEY (order_id) REFERENCES "client_order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9D0DDC6C8D9F6D38 ON service_work (order_id)');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE service_material DROP CONSTRAINT FK_85C82EA8D34F5153');
        $this->addSql('DROP TABLE unit_measure_type');
        $this->addSql('DROP TABLE service_material');
        $this->addSql('ALTER TABLE service_work DROP CONSTRAINT FK_9D0DDC6C8D9F6D38');
        $this->addSql('DROP INDEX IDX_9D0DDC6C8D9F6D38');
        $this->addSql('ALTER TABLE service_work DROP order_id');
        $this->addSql('ALTER TABLE "client_order" DROP order_reason');
    }
}
