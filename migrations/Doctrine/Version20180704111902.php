<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180704111902 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE service_work ADD discount_percent NUMERIC(10, 0) NOT NULL');
        $this->addSql('ALTER TABLE service_work ALTER cost_per_unit SET NOT NULL');
        $this->addSql('ALTER TABLE service_work ALTER amount SET NOT NULL');
        $this->addSql('ALTER TABLE service_work ALTER total_cost SET NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE service_work DROP discount_percent');
        $this->addSql('ALTER TABLE service_work ALTER cost_per_unit DROP NOT NULL');
        $this->addSql('ALTER TABLE service_work ALTER amount DROP NOT NULL');
        $this->addSql('ALTER TABLE service_work ALTER total_cost DROP NOT NULL');
    }
}
