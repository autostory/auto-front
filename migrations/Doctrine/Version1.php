<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version1 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE user_role_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE car_serie_id_car_serie_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE car_generation_id_car_generation_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE car_option_id_car_option_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE car_mark_id_car_mark_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE car_characteristic_value_id_car_characteristic_value_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE car_model_id_car_model_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE car_modification_id_car_modification_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE car_option_value_id_car_option_value_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE car_type_id_car_type_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE car_equipment_id_car_equipment_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE car_characteristic_id_car_characteristic_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE event_log (id SERIAL NOT NULL, user_id INT DEFAULT NULL, target_id INT NOT NULL, target_type VARCHAR(255) NOT NULL, event VARCHAR(255) NOT NULL, event_description VARCHAR(255) DEFAULT NULL, date TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9EF0AD16A76ED395 ON event_log (user_id)');
        $this->addSql('CREATE TABLE account_activate (id SERIAL NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(128) NOT NULL, activated BOOLEAN NOT NULL, activatedOn TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CEA8E61BBF396750 ON account_activate (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CEA8E61B5F37A13B ON account_activate (token)');
        $this->addSql('CREATE INDEX IDX_CEA8E61BA76ED395 ON account_activate (user_id)');
        $this->addSql('CREATE TABLE identity (id SERIAL NOT NULL, creator_id INT DEFAULT NULL, firm_id INT DEFAULT NULL, login VARCHAR(128) NOT NULL, registration_phone BIGINT DEFAULT NULL, registration_email VARCHAR(255) DEFAULT NULL, surname VARCHAR(255) DEFAULT NULL, firstName VARCHAR(255) DEFAULT NULL, patronymic VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, active BOOLEAN NOT NULL, createdAt TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, updatedAt TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, user_type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6A95E9C4BF396750 ON identity (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6A95E9C4AA08CB10 ON identity (login)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6A95E9C4CBE53735 ON identity (registration_phone)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6A95E9C46838DC9C ON identity (registration_email)');
        $this->addSql('CREATE INDEX IDX_6A95E9C461220EA6 ON identity (creator_id)');
        $this->addSql('CREATE INDEX IDX_6A95E9C489AF7860 ON identity (firm_id)');
        $this->addSql('CREATE TABLE user_role_linker (user_id INT NOT NULL, role_id INT NOT NULL, PRIMARY KEY(user_id, role_id))');
        $this->addSql('CREATE INDEX IDX_61117899A76ED395 ON user_role_linker (user_id)');
        $this->addSql('CREATE INDEX IDX_61117899D60322AC ON user_role_linker (role_id)');
        $this->addSql('CREATE TABLE client_cars (client_id INT NOT NULL, car_id INT NOT NULL, PRIMARY KEY(client_id, car_id))');
        $this->addSql('CREATE INDEX IDX_445BDE7619EB6921 ON client_cars (client_id)');
        $this->addSql('CREATE INDEX IDX_445BDE76C3C6F69F ON client_cars (car_id)');
        $this->addSql('CREATE TABLE user_role (id INT NOT NULL, parent_id INT DEFAULT NULL, role_id VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2DE8C6A3D60322AC ON user_role (role_id)');
        $this->addSql('CREATE INDEX IDX_2DE8C6A3727ACA70 ON user_role (parent_id)');
        $this->addSql('CREATE TABLE car_serie (id_car_serie INT NOT NULL, id_car_generation INT DEFAULT NULL, id_car_model INT DEFAULT NULL, id_car_type INT DEFAULT NULL, name VARCHAR(510) NOT NULL, date_create INT DEFAULT NULL, date_update INT DEFAULT NULL, PRIMARY KEY(id_car_serie))');
        $this->addSql('CREATE INDEX car_serie_id_car_model_idx ON car_serie (id_car_model)');
        $this->addSql('CREATE INDEX car_serie_id_car_generation_idx ON car_serie (id_car_generation)');
        $this->addSql('CREATE INDEX car_serie_id_car_type_idx ON car_serie (id_car_type)');
        $this->addSql('CREATE TABLE car_generation (id_car_generation INT NOT NULL, id_car_model INT DEFAULT NULL, id_car_type INT DEFAULT NULL, name VARCHAR(510) NOT NULL, year_begin VARCHAR(510) DEFAULT NULL, year_end VARCHAR(510) DEFAULT NULL, date_create INT NOT NULL, date_update INT DEFAULT NULL, PRIMARY KEY(id_car_generation))');
        $this->addSql('CREATE INDEX car_generation_id_car_type_idx ON car_generation (id_car_type)');
        $this->addSql('CREATE INDEX car_generation_id_car_model_idx ON car_generation (id_car_model)');
        $this->addSql('CREATE TABLE car_option (id_car_option INT NOT NULL, id_car_type INT DEFAULT NULL, name VARCHAR(510) NOT NULL, id_parent INT DEFAULT NULL, date_create INT NOT NULL, date_update INT NOT NULL, PRIMARY KEY(id_car_option))');
        $this->addSql('CREATE INDEX car_option_id_car_type_idx ON car_option (id_car_type)');
        $this->addSql('CREATE TABLE car_mark (id_car_mark INT NOT NULL, id_car_type INT DEFAULT NULL, name VARCHAR(510) NOT NULL, date_create INT DEFAULT NULL, date_update INT DEFAULT NULL, name_rus VARCHAR(510) DEFAULT NULL, PRIMARY KEY(id_car_mark))');
        $this->addSql('CREATE INDEX car_mark_id_car_type_idx ON car_mark (id_car_type)');
        $this->addSql('CREATE TABLE car_characteristic_value (id_car_characteristic_value INT NOT NULL, id_car_characteristic INT DEFAULT NULL, id_car_modification INT DEFAULT NULL, id_car_type INT DEFAULT NULL, value VARCHAR(510) DEFAULT NULL, unit VARCHAR(510) DEFAULT NULL, date_create INT DEFAULT NULL, date_update INT DEFAULT NULL, PRIMARY KEY(id_car_characteristic_value))');
        $this->addSql('CREATE INDEX car_characteristic_value_id_car_type_idx ON car_characteristic_value (id_car_type)');
        $this->addSql('CREATE INDEX car_characteristic_value_id_car_modification_idx ON car_characteristic_value (id_car_modification)');
        $this->addSql('CREATE INDEX car_characteristic_value_id_car_characteristic_idx ON car_characteristic_value (id_car_characteristic)');
        $this->addSql('CREATE UNIQUE INDEX car_characteristic_value_id_car_characteristic_id_car_modi_key1 ON car_characteristic_value (id_car_characteristic, id_car_modification, id_car_type)');
        $this->addSql('CREATE UNIQUE INDEX car_characteristic_value_id_car_characteristic_id_car_modif_key ON car_characteristic_value (id_car_characteristic, id_car_modification)');
        $this->addSql('CREATE TABLE car_model (id_car_model INT NOT NULL, id_car_mark INT DEFAULT NULL, id_car_type INT DEFAULT NULL, name VARCHAR(510) NOT NULL, date_create INT DEFAULT NULL, date_update INT DEFAULT NULL, name_rus VARCHAR(510) DEFAULT NULL, PRIMARY KEY(id_car_model))');
        $this->addSql('CREATE INDEX car_model_id_car_type_idx ON car_model (id_car_type)');
        $this->addSql('CREATE INDEX car_model_id_car_mark_idx ON car_model (id_car_mark)');
        $this->addSql('CREATE TABLE car_modification (id_car_modification INT NOT NULL, id_car_model INT DEFAULT NULL, id_car_serie INT DEFAULT NULL, id_car_type INT DEFAULT NULL, name VARCHAR(510) NOT NULL, date_create INT DEFAULT NULL, date_update INT DEFAULT NULL, PRIMARY KEY(id_car_modification))');
        $this->addSql('CREATE INDEX car_modification_id_car_serie_idx ON car_modification (id_car_serie)');
        $this->addSql('CREATE INDEX car_modification_id_car_model_idx ON car_modification (id_car_model)');
        $this->addSql('CREATE INDEX car_modification_id_car_type_idx ON car_modification (id_car_type)');
        $this->addSql('CREATE TABLE car_option_value (id_car_option_value INT NOT NULL, id_car_equipment INT DEFAULT NULL, id_car_option INT DEFAULT NULL, id_car_type INT DEFAULT NULL, is_base BOOLEAN NOT NULL, date_create INT NOT NULL, date_update INT NOT NULL, PRIMARY KEY(id_car_option_value))');
        $this->addSql('CREATE INDEX car_option_value_id_car_type_idx ON car_option_value (id_car_type)');
        $this->addSql('CREATE INDEX car_option_value_id_car_option_idx ON car_option_value (id_car_option)');
        $this->addSql('CREATE INDEX car_option_value_id_car_equipment_idx ON car_option_value (id_car_equipment)');
        $this->addSql('CREATE UNIQUE INDEX car_option_value_id_car_option_id_car_equipment_id_car_type_key ON car_option_value (id_car_option, id_car_equipment, id_car_type)');
        $this->addSql('CREATE TABLE car_type (id_car_type INT NOT NULL, name VARCHAR(510) NOT NULL, PRIMARY KEY(id_car_type))');
        $this->addSql('CREATE TABLE car_equipment (id_car_equipment INT NOT NULL, id_car_modification INT DEFAULT NULL, id_car_type INT DEFAULT NULL, name VARCHAR(510) NOT NULL, date_create INT NOT NULL, date_update INT NOT NULL, price_min INT DEFAULT NULL, year INT DEFAULT NULL, PRIMARY KEY(id_car_equipment))');
        $this->addSql('CREATE INDEX car_equipment_id_car_modification_idx ON car_equipment (id_car_modification)');
        $this->addSql('CREATE INDEX car_equipment_id_car_type_idx ON car_equipment (id_car_type)');
        $this->addSql('CREATE TABLE car_characteristic (id_car_characteristic INT NOT NULL, id_car_type INT DEFAULT NULL, name VARCHAR(510) DEFAULT NULL, id_parent INT DEFAULT NULL, date_create INT DEFAULT NULL, date_update INT DEFAULT NULL, PRIMARY KEY(id_car_characteristic))');
        $this->addSql('CREATE INDEX car_characteristic_id_car_type_idx ON car_characteristic (id_car_type)');
        $this->addSql('CREATE TABLE office (id SERIAL NOT NULL, firm_id INT NOT NULL, creator_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, phone VARCHAR(20) DEFAULT NULL, workTimeDescription VARCHAR(255) DEFAULT NULL, active BOOLEAN NOT NULL, createdAt TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, updatedAt TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_74516B02BF396750 ON office (id)');
        $this->addSql('CREATE INDEX IDX_74516B0289AF7860 ON office (firm_id)');
        $this->addSql('CREATE INDEX IDX_74516B0261220EA6 ON office (creator_id)');
        $this->addSql('CREATE TABLE office_manager_linker (office_id INT NOT NULL, manager_id INT NOT NULL, PRIMARY KEY(office_id, manager_id))');
        $this->addSql('CREATE INDEX IDX_E1435A6AFFA0C224 ON office_manager_linker (office_id)');
        $this->addSql('CREATE INDEX IDX_E1435A6A783E3463 ON office_manager_linker (manager_id)');
        $this->addSql('CREATE TABLE car (id SERIAL NOT NULL, catalog_car_type_id INT DEFAULT NULL, catalog_car_mark_id INT DEFAULT NULL, catalog_car_model_id INT DEFAULT NULL, creator_id INT DEFAULT NULL, vin VARCHAR(128) DEFAULT NULL, registration_sign_number VARCHAR(128) NOT NULL, createdAt TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, updatedAt TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_773DE69DBF396750 ON car (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_773DE69DB1085141 ON car (vin)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_773DE69D6C96704C ON car (registration_sign_number)');
        $this->addSql('CREATE INDEX IDX_773DE69D875356E1 ON car (catalog_car_type_id)');
        $this->addSql('CREATE INDEX IDX_773DE69D8F2B59 ON car (catalog_car_mark_id)');
        $this->addSql('CREATE INDEX IDX_773DE69DC73CB82D ON car (catalog_car_model_id)');
        $this->addSql('CREATE INDEX IDX_773DE69D61220EA6 ON car (creator_id)');
        $this->addSql('CREATE TABLE password_restore (id SERIAL NOT NULL, client_id INT DEFAULT NULL, manager_id INT DEFAULT NULL, token VARCHAR(128) NOT NULL, validBefore TIMESTAMP(0) WITH TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AF587EF0BF396750 ON password_restore (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AF587EF05F37A13B ON password_restore (token)');
        $this->addSql('CREATE INDEX IDX_AF587EF019EB6921 ON password_restore (client_id)');
        $this->addSql('CREATE INDEX IDX_AF587EF0783E3463 ON password_restore (manager_id)');
        $this->addSql('CREATE TABLE user_communication (id SERIAL NOT NULL, user_id INT DEFAULT NULL, creator_id INT DEFAULT NULL, preferred BOOLEAN NOT NULL, type VARCHAR(128) NOT NULL, value VARCHAR(255) NOT NULL, active BOOLEAN NOT NULL, createdAt TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, updatedAt TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_172118BBBF396750 ON user_communication (id)');
        $this->addSql('CREATE INDEX IDX_172118BBA76ED395 ON user_communication (user_id)');
        $this->addSql('CREATE INDEX IDX_172118BB61220EA6 ON user_communication (creator_id)');
        $this->addSql('CREATE TABLE firm (id SERIAL NOT NULL, creator_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, active BOOLEAN NOT NULL, createdAt TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, updatedAt TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_560581FDBF396750 ON firm (id)');
        $this->addSql('CREATE INDEX IDX_560581FD61220EA6 ON firm (creator_id)');
        $this->addSql('CREATE TABLE firm_clients_linker (firm_id INT NOT NULL, client_id INT NOT NULL, PRIMARY KEY(firm_id, client_id))');
        $this->addSql('CREATE INDEX IDX_DE42C3A589AF7860 ON firm_clients_linker (firm_id)');
        $this->addSql('CREATE INDEX IDX_DE42C3A519EB6921 ON firm_clients_linker (client_id)');
        $this->addSql('ALTER TABLE event_log ADD CONSTRAINT FK_9EF0AD16A76ED395 FOREIGN KEY (user_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_activate ADD CONSTRAINT FK_CEA8E61BA76ED395 FOREIGN KEY (user_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE identity ADD CONSTRAINT FK_6A95E9C461220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE identity ADD CONSTRAINT FK_6A95E9C489AF7860 FOREIGN KEY (firm_id) REFERENCES firm (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_role_linker ADD CONSTRAINT FK_61117899A76ED395 FOREIGN KEY (user_id) REFERENCES identity (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_role_linker ADD CONSTRAINT FK_61117899D60322AC FOREIGN KEY (role_id) REFERENCES user_role (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE client_cars ADD CONSTRAINT FK_445BDE7619EB6921 FOREIGN KEY (client_id) REFERENCES identity (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE client_cars ADD CONSTRAINT FK_445BDE76C3C6F69F FOREIGN KEY (car_id) REFERENCES car (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3727ACA70 FOREIGN KEY (parent_id) REFERENCES user_role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_serie ADD CONSTRAINT FK_759116E3DC9F5F2C FOREIGN KEY (id_car_generation) REFERENCES car_generation (id_car_generation) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_serie ADD CONSTRAINT FK_759116E3880B4B29 FOREIGN KEY (id_car_model) REFERENCES car_model (id_car_model) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_serie ADD CONSTRAINT FK_759116E3176FAF10 FOREIGN KEY (id_car_type) REFERENCES car_type (id_car_type) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_generation ADD CONSTRAINT FK_E1F9E22A880B4B29 FOREIGN KEY (id_car_model) REFERENCES car_model (id_car_model) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_generation ADD CONSTRAINT FK_E1F9E22A176FAF10 FOREIGN KEY (id_car_type) REFERENCES car_type (id_car_type) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_option ADD CONSTRAINT FK_42EEEC42176FAF10 FOREIGN KEY (id_car_type) REFERENCES car_type (id_car_type) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_mark ADD CONSTRAINT FK_AD1EE6DD176FAF10 FOREIGN KEY (id_car_type) REFERENCES car_type (id_car_type) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_characteristic_value ADD CONSTRAINT FK_F7A5E5CE1C7495DF FOREIGN KEY (id_car_characteristic) REFERENCES car_characteristic (id_car_characteristic) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_characteristic_value ADD CONSTRAINT FK_F7A5E5CE5537CFEB FOREIGN KEY (id_car_modification) REFERENCES car_modification (id_car_modification) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_characteristic_value ADD CONSTRAINT FK_F7A5E5CE176FAF10 FOREIGN KEY (id_car_type) REFERENCES car_type (id_car_type) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_model ADD CONSTRAINT FK_83EF70EFDC50A48 FOREIGN KEY (id_car_mark) REFERENCES car_mark (id_car_mark) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_model ADD CONSTRAINT FK_83EF70E176FAF10 FOREIGN KEY (id_car_type) REFERENCES car_type (id_car_type) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_modification ADD CONSTRAINT FK_B6BD9A3A880B4B29 FOREIGN KEY (id_car_model) REFERENCES car_model (id_car_model) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_modification ADD CONSTRAINT FK_B6BD9A3AF5A4AAC4 FOREIGN KEY (id_car_serie) REFERENCES car_serie (id_car_serie) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_modification ADD CONSTRAINT FK_B6BD9A3A176FAF10 FOREIGN KEY (id_car_type) REFERENCES car_type (id_car_type) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_option_value ADD CONSTRAINT FK_7D455AB46AF2F6CA FOREIGN KEY (id_car_equipment) REFERENCES car_equipment (id_car_equipment) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_option_value ADD CONSTRAINT FK_7D455AB4E7646C95 FOREIGN KEY (id_car_option) REFERENCES car_option (id_car_option) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_option_value ADD CONSTRAINT FK_7D455AB4176FAF10 FOREIGN KEY (id_car_type) REFERENCES car_type (id_car_type) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_equipment ADD CONSTRAINT FK_D4DA27AF5537CFEB FOREIGN KEY (id_car_modification) REFERENCES car_modification (id_car_modification) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_equipment ADD CONSTRAINT FK_D4DA27AF176FAF10 FOREIGN KEY (id_car_type) REFERENCES car_type (id_car_type) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_characteristic ADD CONSTRAINT FK_9F562770176FAF10 FOREIGN KEY (id_car_type) REFERENCES car_type (id_car_type) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE office ADD CONSTRAINT FK_74516B0289AF7860 FOREIGN KEY (firm_id) REFERENCES firm (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE office ADD CONSTRAINT FK_74516B0261220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE office_manager_linker ADD CONSTRAINT FK_E1435A6AFFA0C224 FOREIGN KEY (office_id) REFERENCES office (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE office_manager_linker ADD CONSTRAINT FK_E1435A6A783E3463 FOREIGN KEY (manager_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car ADD CONSTRAINT FK_773DE69D875356E1 FOREIGN KEY (catalog_car_type_id) REFERENCES car_type (id_car_type) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car ADD CONSTRAINT FK_773DE69D8F2B59 FOREIGN KEY (catalog_car_mark_id) REFERENCES car_mark (id_car_mark) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car ADD CONSTRAINT FK_773DE69DC73CB82D FOREIGN KEY (catalog_car_model_id) REFERENCES car_model (id_car_model) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car ADD CONSTRAINT FK_773DE69D61220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE password_restore ADD CONSTRAINT FK_AF587EF019EB6921 FOREIGN KEY (client_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE password_restore ADD CONSTRAINT FK_AF587EF0783E3463 FOREIGN KEY (manager_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_communication ADD CONSTRAINT FK_172118BBA76ED395 FOREIGN KEY (user_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_communication ADD CONSTRAINT FK_172118BB61220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE firm ADD CONSTRAINT FK_560581FD61220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE firm_clients_linker ADD CONSTRAINT FK_DE42C3A589AF7860 FOREIGN KEY (firm_id) REFERENCES firm (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE firm_clients_linker ADD CONSTRAINT FK_DE42C3A519EB6921 FOREIGN KEY (client_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE event_log DROP CONSTRAINT FK_9EF0AD16A76ED395');
        $this->addSql('ALTER TABLE account_activate DROP CONSTRAINT FK_CEA8E61BA76ED395');
        $this->addSql('ALTER TABLE identity DROP CONSTRAINT FK_6A95E9C461220EA6');
        $this->addSql('ALTER TABLE user_role_linker DROP CONSTRAINT FK_61117899A76ED395');
        $this->addSql('ALTER TABLE client_cars DROP CONSTRAINT FK_445BDE7619EB6921');
        $this->addSql('ALTER TABLE office DROP CONSTRAINT FK_74516B0261220EA6');
        $this->addSql('ALTER TABLE office_manager_linker DROP CONSTRAINT FK_E1435A6A783E3463');
        $this->addSql('ALTER TABLE car DROP CONSTRAINT FK_773DE69D61220EA6');
        $this->addSql('ALTER TABLE password_restore DROP CONSTRAINT FK_AF587EF019EB6921');
        $this->addSql('ALTER TABLE password_restore DROP CONSTRAINT FK_AF587EF0783E3463');
        $this->addSql('ALTER TABLE user_communication DROP CONSTRAINT FK_172118BBA76ED395');
        $this->addSql('ALTER TABLE user_communication DROP CONSTRAINT FK_172118BB61220EA6');
        $this->addSql('ALTER TABLE firm DROP CONSTRAINT FK_560581FD61220EA6');
        $this->addSql('ALTER TABLE firm_clients_linker DROP CONSTRAINT FK_DE42C3A519EB6921');
        $this->addSql('ALTER TABLE user_role_linker DROP CONSTRAINT FK_61117899D60322AC');
        $this->addSql('ALTER TABLE user_role DROP CONSTRAINT FK_2DE8C6A3727ACA70');
        $this->addSql('ALTER TABLE car_modification DROP CONSTRAINT FK_B6BD9A3AF5A4AAC4');
        $this->addSql('ALTER TABLE car_serie DROP CONSTRAINT FK_759116E3DC9F5F2C');
        $this->addSql('ALTER TABLE car_option_value DROP CONSTRAINT FK_7D455AB4E7646C95');
        $this->addSql('ALTER TABLE car_model DROP CONSTRAINT FK_83EF70EFDC50A48');
        $this->addSql('ALTER TABLE car DROP CONSTRAINT FK_773DE69D8F2B59');
        $this->addSql('ALTER TABLE car_serie DROP CONSTRAINT FK_759116E3880B4B29');
        $this->addSql('ALTER TABLE car_generation DROP CONSTRAINT FK_E1F9E22A880B4B29');
        $this->addSql('ALTER TABLE car_modification DROP CONSTRAINT FK_B6BD9A3A880B4B29');
        $this->addSql('ALTER TABLE car DROP CONSTRAINT FK_773DE69DC73CB82D');
        $this->addSql('ALTER TABLE car_characteristic_value DROP CONSTRAINT FK_F7A5E5CE5537CFEB');
        $this->addSql('ALTER TABLE car_equipment DROP CONSTRAINT FK_D4DA27AF5537CFEB');
        $this->addSql('ALTER TABLE car_serie DROP CONSTRAINT FK_759116E3176FAF10');
        $this->addSql('ALTER TABLE car_generation DROP CONSTRAINT FK_E1F9E22A176FAF10');
        $this->addSql('ALTER TABLE car_option DROP CONSTRAINT FK_42EEEC42176FAF10');
        $this->addSql('ALTER TABLE car_mark DROP CONSTRAINT FK_AD1EE6DD176FAF10');
        $this->addSql('ALTER TABLE car_characteristic_value DROP CONSTRAINT FK_F7A5E5CE176FAF10');
        $this->addSql('ALTER TABLE car_model DROP CONSTRAINT FK_83EF70E176FAF10');
        $this->addSql('ALTER TABLE car_modification DROP CONSTRAINT FK_B6BD9A3A176FAF10');
        $this->addSql('ALTER TABLE car_option_value DROP CONSTRAINT FK_7D455AB4176FAF10');
        $this->addSql('ALTER TABLE car_equipment DROP CONSTRAINT FK_D4DA27AF176FAF10');
        $this->addSql('ALTER TABLE car_characteristic DROP CONSTRAINT FK_9F562770176FAF10');
        $this->addSql('ALTER TABLE car DROP CONSTRAINT FK_773DE69D875356E1');
        $this->addSql('ALTER TABLE car_option_value DROP CONSTRAINT FK_7D455AB46AF2F6CA');
        $this->addSql('ALTER TABLE car_characteristic_value DROP CONSTRAINT FK_F7A5E5CE1C7495DF');
        $this->addSql('ALTER TABLE office_manager_linker DROP CONSTRAINT FK_E1435A6AFFA0C224');
        $this->addSql('ALTER TABLE client_cars DROP CONSTRAINT FK_445BDE76C3C6F69F');
        $this->addSql('ALTER TABLE identity DROP CONSTRAINT FK_6A95E9C489AF7860');
        $this->addSql('ALTER TABLE office DROP CONSTRAINT FK_74516B0289AF7860');
        $this->addSql('ALTER TABLE firm_clients_linker DROP CONSTRAINT FK_DE42C3A589AF7860');
        $this->addSql('DROP SEQUENCE user_role_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE car_serie_id_car_serie_seq CASCADE');
        $this->addSql('DROP SEQUENCE car_generation_id_car_generation_seq CASCADE');
        $this->addSql('DROP SEQUENCE car_option_id_car_option_seq CASCADE');
        $this->addSql('DROP SEQUENCE car_mark_id_car_mark_seq CASCADE');
        $this->addSql('DROP SEQUENCE car_characteristic_value_id_car_characteristic_value_seq CASCADE');
        $this->addSql('DROP SEQUENCE car_model_id_car_model_seq CASCADE');
        $this->addSql('DROP SEQUENCE car_modification_id_car_modification_seq CASCADE');
        $this->addSql('DROP SEQUENCE car_option_value_id_car_option_value_seq CASCADE');
        $this->addSql('DROP SEQUENCE car_type_id_car_type_seq CASCADE');
        $this->addSql('DROP SEQUENCE car_equipment_id_car_equipment_seq CASCADE');
        $this->addSql('DROP SEQUENCE car_characteristic_id_car_characteristic_seq CASCADE');
        $this->addSql('DROP TABLE event_log');
        $this->addSql('DROP TABLE account_activate');
        $this->addSql('DROP TABLE identity');
        $this->addSql('DROP TABLE user_role_linker');
        $this->addSql('DROP TABLE client_cars');
        $this->addSql('DROP TABLE user_role');
        $this->addSql('DROP TABLE car_serie');
        $this->addSql('DROP TABLE car_generation');
        $this->addSql('DROP TABLE car_option');
        $this->addSql('DROP TABLE car_mark');
        $this->addSql('DROP TABLE car_characteristic_value');
        $this->addSql('DROP TABLE car_model');
        $this->addSql('DROP TABLE car_modification');
        $this->addSql('DROP TABLE car_option_value');
        $this->addSql('DROP TABLE car_type');
        $this->addSql('DROP TABLE car_equipment');
        $this->addSql('DROP TABLE car_characteristic');
        $this->addSql('DROP TABLE office');
        $this->addSql('DROP TABLE office_manager_linker');
        $this->addSql('DROP TABLE car');
        $this->addSql('DROP TABLE password_restore');
        $this->addSql('DROP TABLE user_communication');
        $this->addSql('DROP TABLE firm');
        $this->addSql('DROP TABLE firm_clients_linker');
    }
}
