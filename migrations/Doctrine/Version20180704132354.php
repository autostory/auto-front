<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180704132354 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE service_work ADD master_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_work ADD labor_hour NUMERIC(10, 2) NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE service_work ALTER labor_hour DROP DEFAULT');
        $this->addSql('ALTER TABLE service_work ADD CONSTRAINT FK_9D0DDC6C13B3DB11 FOREIGN KEY (master_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9D0DDC6C13B3DB11 ON service_work (master_id)');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE service_work DROP CONSTRAINT FK_9D0DDC6C13B3DB11');
        $this->addSql('DROP INDEX IDX_9D0DDC6C13B3DB11');
        $this->addSql('ALTER TABLE service_work DROP master_id');
        $this->addSql('ALTER TABLE service_work DROP labor_hour');
    }
}
