<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181001133954 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE "client_point_accrual_log" (id SERIAL NOT NULL, client_id INT NOT NULL, order_id INT NOT NULL, creator_id INT DEFAULT NULL, amount NUMERIC(10, 2) NOT NULL, accrual_date TIMESTAMP(0) WITH TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9528A633BF396750 ON "client_point_accrual_log" (id)');
        $this->addSql('CREATE INDEX IDX_9528A63319EB6921 ON "client_point_accrual_log" (client_id)');
        $this->addSql('CREATE INDEX IDX_9528A6338D9F6D38 ON "client_point_accrual_log" (order_id)');
        $this->addSql('CREATE INDEX IDX_9528A63361220EA6 ON "client_point_accrual_log" (creator_id)');
        $this->addSql('CREATE TABLE sms_log_record (id SERIAL NOT NULL, initiator_id INT DEFAULT NULL, addressee_id INT DEFAULT NULL, sms_id VARCHAR(255) NOT NULL, message VARCHAR(255) NOT NULL, sms_provider VARCHAR(255) NOT NULL, status INT NOT NULL, phone VARCHAR(255) NOT NULL, send_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, cost NUMERIC(10, 2) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DBBC8BDABF396750 ON sms_log_record (id)');
        $this->addSql('CREATE INDEX IDX_DBBC8BDA7DB3B714 ON sms_log_record (initiator_id)');
        $this->addSql('CREATE INDEX IDX_DBBC8BDA2261B4C3 ON sms_log_record (addressee_id)');
        $this->addSql('ALTER TABLE "client_point_accrual_log" ADD CONSTRAINT FK_9528A63319EB6921 FOREIGN KEY (client_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "client_point_accrual_log" ADD CONSTRAINT FK_9528A6338D9F6D38 FOREIGN KEY (order_id) REFERENCES "client_order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "client_point_accrual_log" ADD CONSTRAINT FK_9528A63361220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sms_log_record ADD CONSTRAINT FK_DBBC8BDA7DB3B714 FOREIGN KEY (initiator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sms_log_record ADD CONSTRAINT FK_DBBC8BDA2261B4C3 FOREIGN KEY (addressee_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE office ADD cash_back_percent INT NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE office ALTER cash_back_percent DROP DEFAULT');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE "client_point_accrual_log"');
        $this->addSql('DROP TABLE sms_log_record');
        $this->addSql('ALTER TABLE office DROP cash_back_percent');
    }
}
