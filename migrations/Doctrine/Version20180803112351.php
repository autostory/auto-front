<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180803112351 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DELETE FROM "request_communication"');
        $this->addSql('DELETE FROM "request"');
        $this->addSql('DELETE FROM "box_schedule"');
        $this->addSql('DELETE FROM "service_work"');
        $this->addSql('DELETE FROM "service_material"');
        $this->addSql('DELETE FROM "order_payment"');
        $this->addSql('DELETE FROM "client_order"');
        $this->addSql('ALTER TABLE client_order ADD office_id INT NOT NULL');
        $this->addSql('ALTER TABLE client_order ADD CONSTRAINT FK_56440F2FFFA0C224 FOREIGN KEY (office_id) REFERENCES office (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_56440F2FFFA0C224 ON client_order (office_id)');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE "client_order" DROP CONSTRAINT FK_56440F2FFFA0C224');
        $this->addSql('DROP INDEX IDX_56440F2FFFA0C224');
        $this->addSql('ALTER TABLE "client_order" DROP office_id');
    }
}
