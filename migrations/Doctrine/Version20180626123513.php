<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180626123513 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE "order" ADD firm_id INT NOT NULL');
        $this->addSql('ALTER TABLE "order" ADD active BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F529939889AF7860 FOREIGN KEY (firm_id) REFERENCES firm (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F529939889AF7860 ON "order" (firm_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F529939889AF7860');
        $this->addSql('DROP INDEX IDX_F529939889AF7860');
        $this->addSql('ALTER TABLE "order" DROP firm_id');
        $this->addSql('ALTER TABLE "order" DROP active');
    }
}
