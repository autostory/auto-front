<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180708133401 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE car DROP CONSTRAINT fk_773de69dbbffe291');
        $this->addSql('ALTER TABLE car DROP CONSTRAINT fk_773de69d426be9f');
        $this->addSql('DROP INDEX idx_773de69d426be9f');
        $this->addSql('DROP INDEX idx_773de69dbbffe291');
        $this->addSql('ALTER TABLE car ADD catalog_car_serie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE car ADD catalog_car_generation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE car ADD catalog_car_modification_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE car ADD catalog_car_equipment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE car DROP generation_car_model_id');
        $this->addSql('ALTER TABLE car DROP serie_car_model_id');
        $this->addSql('ALTER TABLE car ADD CONSTRAINT FK_773DE69D670A8777 FOREIGN KEY (catalog_car_serie_id) REFERENCES car_serie (id_car_serie) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car ADD CONSTRAINT FK_773DE69DABF1BB91 FOREIGN KEY (catalog_car_generation_id) REFERENCES car_generation (id_car_generation) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car ADD CONSTRAINT FK_773DE69D7B1520A6 FOREIGN KEY (catalog_car_modification_id) REFERENCES car_modification (id_car_modification) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car ADD CONSTRAINT FK_773DE69D4EB1CA66 FOREIGN KEY (catalog_car_equipment_id) REFERENCES car_equipment (id_car_equipment) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_773DE69D670A8777 ON car (catalog_car_serie_id)');
        $this->addSql('CREATE INDEX IDX_773DE69DABF1BB91 ON car (catalog_car_generation_id)');
        $this->addSql('CREATE INDEX IDX_773DE69D7B1520A6 ON car (catalog_car_modification_id)');
        $this->addSql('CREATE INDEX IDX_773DE69D4EB1CA66 ON car (catalog_car_equipment_id)');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE car DROP CONSTRAINT FK_773DE69D670A8777');
        $this->addSql('ALTER TABLE car DROP CONSTRAINT FK_773DE69DABF1BB91');
        $this->addSql('ALTER TABLE car DROP CONSTRAINT FK_773DE69D7B1520A6');
        $this->addSql('ALTER TABLE car DROP CONSTRAINT FK_773DE69D4EB1CA66');
        $this->addSql('DROP INDEX IDX_773DE69D670A8777');
        $this->addSql('DROP INDEX IDX_773DE69DABF1BB91');
        $this->addSql('DROP INDEX IDX_773DE69D7B1520A6');
        $this->addSql('DROP INDEX IDX_773DE69D4EB1CA66');
        $this->addSql('ALTER TABLE car ADD generation_car_model_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE car ADD serie_car_model_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE car DROP catalog_car_serie_id');
        $this->addSql('ALTER TABLE car DROP catalog_car_generation_id');
        $this->addSql('ALTER TABLE car DROP catalog_car_modification_id');
        $this->addSql('ALTER TABLE car DROP catalog_car_equipment_id');
        $this->addSql('ALTER TABLE car ADD CONSTRAINT fk_773de69dbbffe291 FOREIGN KEY (generation_car_model_id) REFERENCES car_generation (id_car_generation) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car ADD CONSTRAINT fk_773de69d426be9f FOREIGN KEY (serie_car_model_id) REFERENCES car_serie (id_car_serie) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_773de69d426be9f ON car (serie_car_model_id)');
        $this->addSql('CREATE INDEX idx_773de69dbbffe291 ON car (generation_car_model_id)');
    }
}
