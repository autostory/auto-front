<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180905053034 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('
            create or replace function uniq_client_car_registration_sign_number_check_linker()
            returns trigger language plpgsql as $$
            declare
                clientHaveCarWithSameRegistrationSignNumber boolean;
            begin
                SELECT count(car.id) <> 0 into clientHaveCarWithSameRegistrationSignNumber FROM client_cars 
                LEFT JOIN car ON client_cars.car_id = car.id
                WHERE 
                client_cars.client_id = new.client_id
                AND 
                car.registration_sign_number = (SELECT car.registration_sign_number FROM car WHERE car.id = new.car_id);
            
                if clientHaveCarWithSameRegistrationSignNumber then
                    raise exception \'Client "%" have car with same registration sign number\', new.client_id;
                end if;
                return new;
            end $$;
        ');
        $this->addSql('CREATE TRIGGER t_insert_uniq_client_car_registration_sign_number_check BEFORE INSERT OR UPDATE ON client_cars FOR EACH ROW EXECUTE PROCEDURE uniq_client_car_registration_sign_number_check_linker();');

        $this->addSql('
            create or replace function uniq_client_car_registration_sign_number_check_on_car_update()
            returns trigger language plpgsql as $$
            declare
                clientHaveCarWithSameRegistrationSignNumber boolean;
            begin
                SELECT count(car.id) <> 0 INTO clientHaveCarWithSameRegistrationSignNumber 
                FROM client_cars 
                LEFT JOIN car ON client_cars.car_id = car.id
                WHERE 
                client_cars.car_id <> new.id
                AND 
                car.registration_sign_number = new.registration_sign_number;
            
                if clientHaveCarWithSameRegistrationSignNumber then
                    raise exception \'Car "%" have owners with cars witch have same registration sign number as "%"\', new.id, new.registration_sign_number;
                end if;
                return new;
            end $$;
        ');
        $this->addSql('CREATE TRIGGER t_update_car_uniq_client_car_registration_sign_number_check BEFORE UPDATE ON car FOR EACH ROW EXECUTE PROCEDURE uniq_client_car_registration_sign_number_check_on_car_update();');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TRIGGER t_insert_uniq_client_car_registration_sign_number_check ON client_cars;');
        $this->addSql('DROP FUNCTION uniq_client_car_registration_sign_number_check_linker();');

        $this->addSql('DROP TRIGGER t_update_car_uniq_client_car_registration_sign_number_check ON car;');
        $this->addSql('DROP FUNCTION uniq_client_car_registration_sign_number_check_on_car_update();');
    }
}
