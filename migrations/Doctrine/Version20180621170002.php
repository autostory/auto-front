<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180621170002 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE order_status (id SERIAL NOT NULL, title VARCHAR(128) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B88F75C9BF396750 ON order_status (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B88F75C92B36786B ON order_status (title)');
        $this->addSql('CREATE TABLE box_schedule (id SERIAL NOT NULL, office_box_id INT NOT NULL, creator_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BFFD35FBF396750 ON box_schedule (id)');
        $this->addSql('CREATE INDEX IDX_BFFD35F35A6A3BC ON box_schedule (office_box_id)');
        $this->addSql('CREATE INDEX IDX_BFFD35F61220EA6 ON box_schedule (creator_id)');
        $this->addSql('ALTER TABLE box_schedule ADD CONSTRAINT FK_BFFD35F35A6A3BC FOREIGN KEY (office_box_id) REFERENCES OfficeBox (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE box_schedule ADD CONSTRAINT FK_BFFD35F61220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE order_status');
        $this->addSql('DROP TABLE box_schedule');
    }
}
