<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180927154741 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE client_point_log ALTER amount TYPE NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE identity ALTER points TYPE NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE firm ALTER points TYPE NUMERIC(10, 2)');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE firm ALTER points TYPE NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE "client_point_log" ALTER amount TYPE NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE identity ALTER points TYPE NUMERIC(10, 0)');
    }
}
