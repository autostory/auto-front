<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180201135041 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE event_gas ALTER fuel_amount TYPE NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE event_gas ALTER one_unit_cost TYPE NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE event_gas ALTER total_cost TYPE NUMERIC(10, 2)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE event_gas ALTER fuel_amount TYPE NUMERIC(2, 0)');
        $this->addSql('ALTER TABLE event_gas ALTER one_unit_cost TYPE NUMERIC(2, 0)');
        $this->addSql('ALTER TABLE event_gas ALTER total_cost TYPE NUMERIC(2, 0)');
    }
}
