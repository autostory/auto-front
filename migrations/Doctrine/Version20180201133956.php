<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180201133956 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE basic_fuel_type (id SERIAL NOT NULL, name VARCHAR(128) NOT NULL, unit_name VARCHAR(128) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_65FDE973BF396750 ON basic_fuel_type (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_65FDE9735E237E06 ON basic_fuel_type (name)');
        $this->addSql('CREATE TABLE event_gas (id SERIAL NOT NULL, initiator_id INT NOT NULL, car_id INT NOT NULL, fuel_type_id INT NOT NULL, creator_id INT DEFAULT NULL, mileage INT NOT NULL, date TIMESTAMP(0) WITH TIME ZONE NOT NULL, fuel_amount NUMERIC(2, 0) NOT NULL, full_tank BOOLEAN NOT NULL, one_unit_cost NUMERIC(2, 0) NOT NULL, total_cost NUMERIC(2, 0) NOT NULL, note VARCHAR(255) DEFAULT NULL, createdAt TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, updatedAt TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_16FCBB04BF396750 ON event_gas (id)');
        $this->addSql('CREATE INDEX IDX_16FCBB047DB3B714 ON event_gas (initiator_id)');
        $this->addSql('CREATE INDEX IDX_16FCBB04C3C6F69F ON event_gas (car_id)');
        $this->addSql('CREATE INDEX IDX_16FCBB046A70FE35 ON event_gas (fuel_type_id)');
        $this->addSql('CREATE INDEX IDX_16FCBB0461220EA6 ON event_gas (creator_id)');
        $this->addSql('ALTER TABLE event_gas ADD CONSTRAINT FK_16FCBB047DB3B714 FOREIGN KEY (initiator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE event_gas ADD CONSTRAINT FK_16FCBB04C3C6F69F FOREIGN KEY (car_id) REFERENCES car (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE event_gas ADD CONSTRAINT FK_16FCBB046A70FE35 FOREIGN KEY (fuel_type_id) REFERENCES basic_fuel_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE event_gas ADD CONSTRAINT FK_16FCBB0461220EA6 FOREIGN KEY (creator_id) REFERENCES identity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE event_gas DROP CONSTRAINT FK_16FCBB046A70FE35');
        $this->addSql('DROP TABLE basic_fuel_type');
        $this->addSql('DROP TABLE event_gas');
    }
}
