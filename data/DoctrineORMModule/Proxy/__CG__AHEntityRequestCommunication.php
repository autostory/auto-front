<?php

namespace DoctrineORMModule\Proxy\__CG__\AH\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class RequestCommunication extends \AH\Entity\RequestCommunication implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'id', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'resultId', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'rejectReasonId', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'rejectReasonDescription', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'request', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'communicationDateTime', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'nextCommunicationDateTime', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'manager', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'creator', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'createdAt', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'updatedAt'];
        }

        return ['__isInitialized__', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'id', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'resultId', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'rejectReasonId', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'rejectReasonDescription', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'request', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'communicationDateTime', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'nextCommunicationDateTime', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'manager', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'creator', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'createdAt', '' . "\0" . 'AH\\Entity\\RequestCommunication' . "\0" . 'updatedAt'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (RequestCommunication $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId(): int
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setId(int $id): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setId', [$id]);

        parent::setId($id);
    }

    /**
     * {@inheritDoc}
     */
    public function getResultId(): int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getResultId', []);

        return parent::getResultId();
    }

    /**
     * {@inheritDoc}
     */
    public function setResultId(int $resultId): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setResultId', [$resultId]);

        parent::setResultId($resultId);
    }

    /**
     * {@inheritDoc}
     */
    public function getManager(): \AH\Entity\Manager
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getManager', []);

        return parent::getManager();
    }

    /**
     * {@inheritDoc}
     */
    public function setManager(\AH\Entity\Manager $manager): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setManager', [$manager]);

        parent::setManager($manager);
    }

    /**
     * {@inheritDoc}
     */
    public function getRejectReasonId(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRejectReasonId', []);

        return parent::getRejectReasonId();
    }

    /**
     * {@inheritDoc}
     */
    public function setRejectReasonId(?int $rejectReasonId): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRejectReasonId', [$rejectReasonId]);

        parent::setRejectReasonId($rejectReasonId);
    }

    /**
     * {@inheritDoc}
     */
    public function getNextCommunicationDateTime(): ?\DateTime
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNextCommunicationDateTime', []);

        return parent::getNextCommunicationDateTime();
    }

    /**
     * {@inheritDoc}
     */
    public function setNextCommunicationDateTime(?\DateTime $nextCommunicationDateTime): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNextCommunicationDateTime', [$nextCommunicationDateTime]);

        parent::setNextCommunicationDateTime($nextCommunicationDateTime);
    }

    /**
     * {@inheritDoc}
     */
    public function getCommunicationDateTime(): \DateTime
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCommunicationDateTime', []);

        return parent::getCommunicationDateTime();
    }

    /**
     * {@inheritDoc}
     */
    public function setCommunicationDateTime(\DateTime $communicationDateTime): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCommunicationDateTime', [$communicationDateTime]);

        parent::setCommunicationDateTime($communicationDateTime);
    }

    /**
     * {@inheritDoc}
     */
    public function getRequest(): \AH\Entity\Request
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRequest', []);

        return parent::getRequest();
    }

    /**
     * {@inheritDoc}
     */
    public function setRequest(\AH\Entity\Request $request): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRequest', [$request]);

        parent::setRequest($request);
    }

    /**
     * {@inheritDoc}
     */
    public function getRejectReasonDescription(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRejectReasonDescription', []);

        return parent::getRejectReasonDescription();
    }

    /**
     * {@inheritDoc}
     */
    public function setRejectReasonDescription(?string $rejectReasonDescription): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRejectReasonDescription', [$rejectReasonDescription]);

        parent::setRejectReasonDescription($rejectReasonDescription);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreator(): ?\AH\Entity\User
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreator', []);

        return parent::getCreator();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreator(?\AH\Entity\User $creator)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreator', [$creator]);

        return parent::setCreator($creator);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt(): ?\DateTime
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreatedAt', []);

        return parent::getCreatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreatedAt', [$createdAt]);

        parent::setCreatedAt($createdAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt(): ?\DateTime
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUpdatedAt', []);

        return parent::getUpdatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUpdatedAt', [$updatedAt]);

        parent::setUpdatedAt($updatedAt);
    }

}
