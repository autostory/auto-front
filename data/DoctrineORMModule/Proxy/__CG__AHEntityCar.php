<?php

namespace DoctrineORMModule\Proxy\__CG__\AH\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Car extends \AH\Entity\Car implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'id', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'vin', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'assumeVin', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'year', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'color', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'registrationSignNumber', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'owners', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'orders', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'mileages', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'catalogCarType', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'catalogCarMark', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'catalogCarModel', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'catalogCarSerie', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'catalogCarGeneration', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'catalogCarModification', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'catalogCarEquipment', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'creator', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'createdAt', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'updatedAt'];
        }

        return ['__isInitialized__', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'id', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'vin', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'assumeVin', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'year', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'color', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'registrationSignNumber', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'owners', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'orders', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'mileages', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'catalogCarType', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'catalogCarMark', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'catalogCarModel', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'catalogCarSerie', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'catalogCarGeneration', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'catalogCarModification', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'catalogCarEquipment', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'creator', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'createdAt', '' . "\0" . 'AH\\Entity\\Car' . "\0" . 'updatedAt'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Car $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setId($id)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setId', [$id]);

        return parent::setId($id);
    }

    /**
     * {@inheritDoc}
     */
    public function getVin()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getVin', []);

        return parent::getVin();
    }

    /**
     * {@inheritDoc}
     */
    public function setVin($vin)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setVin', [$vin]);

        return parent::setVin($vin);
    }

    /**
     * {@inheritDoc}
     */
    public function getRegistrationSignNumber()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRegistrationSignNumber', []);

        return parent::getRegistrationSignNumber();
    }

    /**
     * {@inheritDoc}
     */
    public function setRegistrationSignNumber($registrationSignNumber)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRegistrationSignNumber', [$registrationSignNumber]);

        return parent::setRegistrationSignNumber($registrationSignNumber);
    }

    /**
     * {@inheritDoc}
     */
    public function getOwners(): \Doctrine\Common\Collections\Collection
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOwners', []);

        return parent::getOwners();
    }

    /**
     * {@inheritDoc}
     */
    public function addOwner(\AH\Entity\Client $owner)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addOwner', [$owner]);

        return parent::addOwner($owner);
    }

    /**
     * {@inheritDoc}
     */
    public function removeOwner(\AH\Entity\Client $owner)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeOwner', [$owner]);

        return parent::removeOwner($owner);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrders(): \Doctrine\Common\Collections\Collection
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOrders', []);

        return parent::getOrders();
    }

    /**
     * {@inheritDoc}
     */
    public function addOrder(\AH\Entity\Order $order)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addOrder', [$order]);

        return parent::addOrder($order);
    }

    /**
     * {@inheritDoc}
     */
    public function removeOrder(\AH\Entity\Order $order)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeOrder', [$order]);

        return parent::removeOrder($order);
    }

    /**
     * {@inheritDoc}
     */
    public function getYear(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getYear', []);

        return parent::getYear();
    }

    /**
     * {@inheritDoc}
     */
    public function setYear(?int $year): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setYear', [$year]);

        parent::setYear($year);
    }

    /**
     * {@inheritDoc}
     */
    public function getColor(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getColor', []);

        return parent::getColor();
    }

    /**
     * {@inheritDoc}
     */
    public function setColor(?string $color): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setColor', [$color]);

        parent::setColor($color);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatalogCarType(): ?\AH\Entity\CatalogCar\CarType
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatalogCarType', []);

        return parent::getCatalogCarType();
    }

    /**
     * {@inheritDoc}
     */
    public function setCatalogCarType(?\AH\Entity\CatalogCar\CarType $catalogCarType): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatalogCarType', [$catalogCarType]);

        parent::setCatalogCarType($catalogCarType);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatalogCarMark(): ?\AH\Entity\CatalogCar\CarMark
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatalogCarMark', []);

        return parent::getCatalogCarMark();
    }

    /**
     * {@inheritDoc}
     */
    public function setCatalogCarMark(?\AH\Entity\CatalogCar\CarMark $catalogCarMark): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatalogCarMark', [$catalogCarMark]);

        parent::setCatalogCarMark($catalogCarMark);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatalogCarModel(): ?\AH\Entity\CatalogCar\CarModel
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatalogCarModel', []);

        return parent::getCatalogCarModel();
    }

    /**
     * {@inheritDoc}
     */
    public function setCatalogCarModel(?\AH\Entity\CatalogCar\CarModel $catalogCarModel): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatalogCarModel', [$catalogCarModel]);

        parent::setCatalogCarModel($catalogCarModel);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatalogCarGeneration(): ?\AH\Entity\CatalogCar\CarGeneration
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatalogCarGeneration', []);

        return parent::getCatalogCarGeneration();
    }

    /**
     * {@inheritDoc}
     */
    public function setCatalogCarGeneration(?\AH\Entity\CatalogCar\CarGeneration $catalogCarGeneration): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatalogCarGeneration', [$catalogCarGeneration]);

        parent::setCatalogCarGeneration($catalogCarGeneration);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatalogCarSerie(): ?\AH\Entity\CatalogCar\CarSerie
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatalogCarSerie', []);

        return parent::getCatalogCarSerie();
    }

    /**
     * {@inheritDoc}
     */
    public function setCatalogCarSerie(?\AH\Entity\CatalogCar\CarSerie $catalogCarSerie): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatalogCarSerie', [$catalogCarSerie]);

        parent::setCatalogCarSerie($catalogCarSerie);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatalogCarModification(): ?\AH\Entity\CatalogCar\CarModification
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatalogCarModification', []);

        return parent::getCatalogCarModification();
    }

    /**
     * {@inheritDoc}
     */
    public function setCatalogCarModification(?\AH\Entity\CatalogCar\CarModification $catalogCarModification): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatalogCarModification', [$catalogCarModification]);

        parent::setCatalogCarModification($catalogCarModification);
    }

    /**
     * {@inheritDoc}
     */
    public function getCatalogCarEquipment(): ?\AH\Entity\CatalogCar\CarEquipment
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCatalogCarEquipment', []);

        return parent::getCatalogCarEquipment();
    }

    /**
     * {@inheritDoc}
     */
    public function setCatalogCarEquipment(?\AH\Entity\CatalogCar\CarEquipment $catalogCarEquipment): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCatalogCarEquipment', [$catalogCarEquipment]);

        parent::setCatalogCarEquipment($catalogCarEquipment);
    }

    /**
     * {@inheritDoc}
     */
    public function getMileages(): \Doctrine\Common\Collections\Collection
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMileages', []);

        return parent::getMileages();
    }

    /**
     * {@inheritDoc}
     */
    public function addMillage(\AH\Entity\CarMileage $carMileage)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addMillage', [$carMileage]);

        return parent::addMillage($carMileage);
    }

    /**
     * {@inheritDoc}
     */
    public function removeMillage(\AH\Entity\CarMileage $carMileage)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeMillage', [$carMileage]);

        return parent::removeMillage($carMileage);
    }

    /**
     * {@inheritDoc}
     */
    public function getAssumeVin(): string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAssumeVin', []);

        return parent::getAssumeVin();
    }

    /**
     * {@inheritDoc}
     */
    public function setAssumeVin(string $assumeVin): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAssumeVin', [$assumeVin]);

        parent::setAssumeVin($assumeVin);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreator(): ?\AH\Entity\User
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreator', []);

        return parent::getCreator();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreator(?\AH\Entity\User $creator)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreator', [$creator]);

        return parent::setCreator($creator);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt(): ?\DateTime
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreatedAt', []);

        return parent::getCreatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreatedAt', [$createdAt]);

        parent::setCreatedAt($createdAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt(): ?\DateTime
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUpdatedAt', []);

        return parent::getUpdatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUpdatedAt', [$updatedAt]);

        parent::setUpdatedAt($updatedAt);
    }

}
