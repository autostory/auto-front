<?php

return [
    'bjyauthorize' => [
        'default_role' => 'guest',

        // Using the authentication identity provider, which basically reads the roles from the auth service's identity
        'identity_provider' => 'AH\Core\Provider\AuthenticationIdentityProvider',

        'role_providers' => [
            // using an object repository (entity repository) to load all roles into our ACL
            'BjyAuthorize\Provider\Role\ObjectRepositoryProvider' => [
                'object_manager' => 'Doctrine\ORM\EntityManager',
                'role_entity_class' => 'AH\Entity\Role',
            ],
        ],

        'resource_providers' => [
            'BjyAuthorize\Provider\Resource\Config' => [
                'any-client' => [
                    'my-firm-client' => [],
                ],
                'any-firm' => [ // Фирма
                    'my-firm' => [],
                ],
                'any-office' => [ // Офис фирмы
                    'my-firm-office' => [],
                ],
                'any-car' => [
                    'my-client-car' => [],
                    'my-car' => [],
                ],
                'my-profile' => [],
                'any-manager' => [
                    'my-firm-manager' => [],
                ]
            ],
        ],

        'rule_providers' => [
            'BjyAuthorize\Provider\Rule\Config' => [
                'allow' => [
                    ['admin', 'any-client', ['edit', 'change-password']],
                    ['admin', 'any-manager', ['edit', 'change-password']],

                    ['manager-boss', 'my-firm-manager', ['edit', 'change-password']],

                    ['admin', 'my-profile', ['edit', 'change-password']],

                    ['manager', 'my-profile', ['change-password']],
                    ['client', 'my-profile', ['change-password']],

                    ['admin', 'any-firm', 'view'],
                    ['manager', 'my-firm', ['view']],

                    ['admin', 'any-client', ['view', 'create', 'add-car']],
                    ['manager', 'my-firm-client', ['view', 'create', 'add-car']],

                    ['admin', 'any-car', 'view'],
                    ['manager', 'my-client-car', ['view']],
                    ['client', 'my-car', ['view']],

                    ['admin', 'any-office', 'create'],
                    ['manager-boss', 'my-firm-office', ['create', 'view', 'edit']],
                    ['manager', 'my-firm-office', 'view'],

                    // ['authenticated-user', ['any-firm-name', 'any-firm-id'], 'view'],
                ],
            ],
        ],

        'cache_options' => [
            'adapter' => [
                'name' => 'memory',
            ],
            'plugins' => [
                'serializer',
            ]
        ],

        'cache_key' => 'bjyauthorize_acl',

        'guards' => [],
    ]
];