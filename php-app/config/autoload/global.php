<?php
return array(
    'mail-sender' => [
        'name' => 'Robot from AutoHistory',
        'email' => 'robot@ah.den-nsk.ru'
    ],
    'router' => [
        'routes' => [
            'oauth' => [
                'options' => [
                    'spec' => '%oauth%',
                    'regex' => '(?P<oauth>(/oauth))',
                ],
                'type' => 'regex',
            ],
        ],
    ],
    'zf-oauth2' => array(
        'access_lifetime' => 2678400, // access tokens lifetime: 31 * 24 * 60 * 60 = 2678400 seconds (31 days)
        'storage_settings' => array(
            'user_table' => 'identity',
        ),
        'options' => array(
            'always_issue_new_refresh_token' => true,
            'refresh_token_lifetime' => 2678400,
        ),
    ),
    'zf-mvc-auth' => array(
        'authentication' => array(
            'adapters' => array(
                'custom-oauth2' => array(
                    'adapter' => 'ZF\\MvcAuth\\Authentication\\OAuth2Adapter',
                    'storage' => array(
                        'storage' => 'AH\\Core\\Adapter\\CustomOAuth2PdoAdapter',
                        'route' => '/oauth',
                    ),
                ),
            ),
        ),
    ),
);
