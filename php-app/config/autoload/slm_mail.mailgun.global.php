<?php

return [
    'slm_mail' => [
        'mailgun' => [
            'domain' => getenv('AH_MAILGUN_DOMAIN'),
            'key' => getenv('AH_MAILGUN_KEY'),
        ]
    ]
];
