<?php
return [
    'modules' => [
        'SlmMail',
        'DoctrineModule',
        'DoctrineORMModule',
        'BjyAuthorize',
        'ZF\ApiProblem',
        'ZF\ContentNegotiation',
        'ZF\OAuth2',
        'ZF\MvcAuth',
        'AH',
    ],

    'module_listener_options' => [
        'module_paths' => [
            './module',
            './vendor',
        ],
        'config_glob_paths' => [
            'config/autoload/{,*.}{global,local}.php',
        ],
        'config_cache_key' => 'php-app',
        'config_cache_enabled' => true,
        'module_map_cache_key' => 'php-app',
        'module_map_cache_enabled' => true,
        'cache_dir' => '../data/php-app-cache/',
    ],
];
