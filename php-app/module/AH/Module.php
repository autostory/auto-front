<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace AH;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceManager;

class Module
{
    public function onBootstrap(?MvcEvent $e, ?ServiceManager $sm = null)
    {
        if (!empty($e)) {
            $eventManager = $e->getApplication()->getEventManager();
            $moduleRouteListener = new ModuleRouteListener();
            $moduleRouteListener->attach($eventManager);

            /** @var ServiceManager $sm */
            $sm = $e->getApplication()->getServiceManager();
            /** @var \Doctrine\ORM\EntityManager $entityManager */
            $entityManager = $sm->get('Doctrine\ORM\EntityManager');
        } else {
            /** @var \Doctrine\ORM\EntityManager $entityManager */
            $entityManager = $sm->get('Doctrine\ORM\EntityManager');
        }
        // Чтобы при генерации миграции не учитывались таблицы по указанному regexp
        $entityManager->getConfiguration()->setFilterSchemaAssetsExpression("~^(?!oauth_|weak_password)~");

        // Чтобы записывать кто и во сколько создал сущность, а таже дата последнего изменения.
        /** @var \AH\DoctrineSubscriber\BlameSubscriber\BlameSubscriber $blameListener */
        $blameListener = $sm->get('AH\DoctrineSubscriber\BlameSubscriber');
        /** @var \AH\DoctrineSubscriber\OrderStatusChangeSubscriber\OrderStatusChangeSubscriber $blameListener */
        $orderStatusChangeSubscriber = $sm->get('AH\DoctrineSubscriber\OrderStatusChangeSubscriber');
        $entityManager->getEventManager()->addEventSubscriber($blameListener);
        $entityManager->getEventManager()->addEventSubscriber($orderStatusChangeSubscriber);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
