<?php
namespace AH\DoctrineSubscriber\BlameSubscriber;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class BlameSubscriberFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Zend\Authentication\AuthenticationService $authenticationService */
        $authenticationService = $serviceLocator->get('AuthenticationService');

        return new BlameSubscriber($authenticationService);
    }
}