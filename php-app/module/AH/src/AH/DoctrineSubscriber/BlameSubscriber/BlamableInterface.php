<?php

namespace AH\DoctrineSubscriber\BlameSubscriber;

use AH\Entity\User;
use \DateTime;

interface BlamableInterface
{
    public function getCreatedAt() :?DateTime;
    public function setCreatedAt(?DateTime $createdAt);

    public function getUpdatedAt() :?DateTime;
    public function setUpdatedAt(?DateTime $updatedAt);

    public function getCreator() :?User;
    public function setCreator(?User $creator);
}