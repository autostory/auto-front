<?php
namespace AH\DoctrineSubscriber\BlameSubscriber;

use AH\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use \Zend\Authentication\AuthenticationService;

use DateTime;

class BlameSubscriber implements EventSubscriber
{
    /** @var AuthenticationService */
    protected $authenticationService;

    public function __construct(AuthenticationService $serviceManager)
    {
        $this->authenticationService = $serviceManager;
    }

    /**
     * @return AuthenticationService
     */
    protected function getAuthenticationService()
    {
        return $this->authenticationService;
    }

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'preUpdate',
        );
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof BlamableInterface) {
            $this->updatedTimestamps( $entity);
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof BlamableInterface) {
            $this->updatedTimestamps( $entity);
        }
    }

    /**
     * @param BlamableInterface $entity
     */
    protected function updatedTimestamps(BlamableInterface $entity)
    {
        $entity->setUpdatedAt(new DateTime('now'));

        if ($entity->getCreatedAt() === null && $entity->getCreator() === null) {
            $entity->setCreatedAt(new DateTime('now'));
            $authenticationService = $this->getAuthenticationService();
            /** @var User $identity */
            $identity = $authenticationService->getIdentity();
            if (!empty($identity)) {
                $entity->setCreator($identity);
            }
        }
    }
}