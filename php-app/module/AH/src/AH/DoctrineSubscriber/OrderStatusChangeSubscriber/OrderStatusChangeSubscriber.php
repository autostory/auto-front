<?php
namespace AH\DoctrineSubscriber\OrderStatusChangeSubscriber;

use AH\Entity\Order;
use AH\Graphql\Enum\OrderStatusEnumType;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

use DateTime;

class OrderStatusChangeSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'preUpdate',
        );
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof Order) {
            $this->updatedTimestamps($entity);
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof Order) {
            $this->updatedTimestamps($entity);
        }
    }

    /**
     * @param Order $entity
     */
    protected function updatedTimestamps(Order $entity)
    {
        if ($entity->getStatus() === OrderStatusEnumType::DONE_VALUE) {
            $entity->setDoneDate(new DateTime());
        }

        if ($entity->getStatus() === OrderStatusEnumType::REJECT_VALUE) {
            $entity->setRejectDate(new DateTime());
        }
    }
}