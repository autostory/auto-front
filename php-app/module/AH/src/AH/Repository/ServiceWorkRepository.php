<?php

namespace AH\Repository;

use AH\Entity\Car;
use AH\Entity\Manager;
use AH\Graphql\Enum\OrderStatusEnumType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;

class ServiceWorkRepository extends EntityRepository
{
    public function getServiceWorksByManager(Manager $manager, array $serviceWorkStatuses)
    {
        $qb = $this->createQueryBuilder('sw')
            ->where('sw.master = :master')
            ->setParameter('master', $manager);

        if (!empty($serviceWorkStatuses)) {
            $qb
                ->andWhere('sw.status IN (:statuses)')
                ->setParameter('statuses', $serviceWorkStatuses);
        }

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function getDoneOrdersServiceWorksByCar(Car $car)
    {
        $qb = $this->createQueryBuilder('sw')
            ->leftJoin('sw.order', 'o')
            ->where('o.status = :doneOrderStatus')
            ->andWhere('o.car = :car')
            ->setParameter('doneOrderStatus', OrderStatusEnumType::DONE_VALUE)
            ->setParameter('car', $car);

        $query = $qb->getQuery();

        return new ArrayCollection($query->getResult());
    }
}