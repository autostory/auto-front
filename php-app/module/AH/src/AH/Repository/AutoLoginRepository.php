<?php

namespace AH\Repository;

use AH\Entity\AutoLogin;
use AH\Entity\Client;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;

class AutoLoginRepository extends EntityRepository
{
    /**
     * @param Client $client
     * @param string $chanelType
     * @return ArrayCollection
     * @throws \Exception
     */
    public function findActiveByUser(Client $client, string $chanelType = 'email')
    {
        $qb = $this->createQueryBuilder('al');

        $qb->leftJoin('al.client', 'client')
            ->where('client.active = true')
            ->andWhere('client.id = :client')
            ->andWhere('al.validBefore > :now');

        $qb->setParameter('client', $client);

        $now = new \DateTime();
        $now->setTimezone(new \DateTimeZone('UTC'));
        $qb->setParameter('now', $now);
        $query = $qb->getQuery();

        return new ArrayCollection($query->getResult());
    }

    /**
     * @param string $token
     * @return AutoLogin|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findActiveByToken(string $token)
    {
        $qb = $this->createQueryBuilder('al')
            ->where('al.token = :token')
            ->andWhere('al.validBefore > :now');
        $qb->setParameter('token', $token);

        $now = new \DateTime();
        $now->setTimezone(new \DateTimeZone('UTC'));

        $qb->setParameter('now', $now);
        $qb->setMaxResults(1);

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }
}