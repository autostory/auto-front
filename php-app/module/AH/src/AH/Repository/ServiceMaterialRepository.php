<?php

namespace AH\Repository;

use AH\Entity\Car;
use AH\Graphql\Enum\OrderStatusEnumType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;

class ServiceMaterialRepository extends EntityRepository
{
    public function getDoneOrdersServiceMaterialByCar(Car $car)
    {
        $qb = $this->createQueryBuilder('sm')
            ->leftJoin('sm.order', 'o')
            ->where('o.status = :doneOrderStatus')
            ->andWhere('o.car = :car')
            ->setParameter('doneOrderStatus', OrderStatusEnumType::DONE_VALUE)
            ->setParameter('car', $car);

        $query = $qb->getQuery();

        return new ArrayCollection($query->getResult());
    }
}