<?php

namespace AH\Repository;

use AH\Entity\Firm;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;

class OfficeRepository extends EntityRepository
{
    /**
     * @param Firm $firm
     * @return ArrayCollection
     */
    public function getOfficesByFirm(Firm $firm) :ArrayCollection
    {
        $qb = $this->createQueryBuilder('office');
        $qFirmOffices = $qb
            ->leftJoin('office.firm', 'f')
            ->where('office.active = true')
            ->andWhere('f.id = :firm');

        $qFirmOffices->setParameter('firm', $firm->getId());

        $query = $qFirmOffices->getQuery();

        return new ArrayCollection($query->getResult());
    }
}