<?php

namespace AH\Repository;

use AH\Entity\Car;
use AH\Entity\Client;
use AH\Entity\Firm;
use AH\Entity\Office;
use AH\Entity\OfficeBox;
use AH\Graphql\Enum\OrderStatusEnumType;
use AH\Graphql\Enum\ScheduleStatusEnumType;
use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class OrderRepository extends EntityRepository
{
    /**
     * @param Firm $firm
     * @param bool|null $active
     * @param bool|null $withActiveSchedules
     * @param OfficeBox|null $box
     * @param array $statuses
     * @param Office|null $office
     * @param Car|null $car
     * @param Client|null $client
     * @param null $orderIdPart
     * @return QueryBuilder
     */
    private function _getFirmOrdersByFilterSearchQuery(
        Firm $firm,
        bool $active = null,
        bool $withActiveSchedules = null,
        OfficeBox $box = null,
        array $statuses = [],
        Office $office = null,
        Car $car = null,
        Client $client = null,
        $orderIdPart = null
    ): QueryBuilder
    {
        $qb = $this->createQueryBuilder('o');
        $scheduleActiveStatuses = [
            ScheduleStatusEnumType::SCHEDULE_IN_WORK_VALUE,
            ScheduleStatusEnumType::SCHEDULE_WAIT_WORK_START_VALUE
        ];

        $q = $qb
            ->leftJoin('o.firm', 'firm')
            ->where('firm.id = :firm');

        if ($active !== null) {
            $q->andWhere('o.active = :active');
            $q->setParameter('active', $active);
        }

        if ($withActiveSchedules !== null) {
            if ($withActiveSchedules) {
                $q->leftJoin('o.schedules', 'schedules');
                $q->andWhere('schedules.id is NOT NULL');
                $q->andWhere('schedules.status IN (:scheduleActiveStatuses)');
                $q->setParameter('scheduleActiveStatuses', $scheduleActiveStatuses);
            } else {
                $q->leftJoin('o.schedules', 'schedules');
                $q->andWhere($qb->expr()->orX(
                    $qb->expr()->notIn('schedules.status', $scheduleActiveStatuses),
                    $qb->expr()->isNull('schedules.id')
                ));
            }
        }

        if ($withActiveSchedules && $box !== null) {
            $q->andWhere('schedules.box = :box');
            $q->setParameter('box', $box);
        }

        if ($office !== null) {
            $q->andWhere('o.office = :office');
            $q->setParameter('office', $office);
        }

        if ($car !== null) {
            $q->andWhere('o.car = :car');
            $q->setParameter('car', $car);
        }

        if ($client !== null) {
            $q->andWhere('o.client = :client');
            $q->setParameter('client', $client);
        }

        if (!empty($statuses)) {
            $q->andWhere('o.status IN (:statuses)');
            $q->setParameter('statuses', $statuses);
        }

        if (!empty($orderIdPart)) {
            $q->andWhere('CAST(o.id as TEXT) LIKE :orderIdPart');
            $q->setParameter('orderIdPart', '%' . $orderIdPart . '%');
        }

        $q->setParameter('firm', $firm->getId());

        return $q;
    }

    /**
     * @param Firm $firm
     * @param null|bool $active
     * @param null|bool $withActiveSchedules
     * @param OfficeBox|null $box
     * @param array $statuses
     * @param Office $office
     * @param Car|null $car
     * @param Client|null $client
     * @param null $orderIdPart
     * @return ArrayCollection
     */
    public function getFirmOrdersByFilter(
        Firm $firm,
        bool $active = null,
        bool $withActiveSchedules = null,
        OfficeBox $box = null,
        array $statuses = [],
        Office $office = null,
        Car $car = null,
        Client $client = null,
        $orderIdPart = null
    ): ArrayCollection
    {
        $q = $this->_getFirmOrdersByFilterSearchQuery(
            $firm,
            $active,
            $withActiveSchedules,
            $box,
            $statuses,
            $office,
            $car,
            $client,
            $orderIdPart
        );
        $q->orderBy('o.id', 'DESC');

        $query = $q->getQuery();

        return new ArrayCollection($query->getResult());
    }

    /**
     * @param Firm $firm
     * @param bool|null $active
     * @param bool|null $withActiveSchedules
     * @param OfficeBox|null $box
     * @param array $statuses
     * @param Office|null $office
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getFirmOrderCountByFilter(
        Firm $firm,
        bool $active = null,
        bool $withActiveSchedules = null,
        OfficeBox $box = null,
        array $statuses = [],
        Office $office = null
    ): int
    {
        $q = $this->_getFirmOrdersByFilterSearchQuery($firm, $active, $withActiveSchedules, $box, $statuses, $office);
        $q->select('COUNT(o.id)');

        $query = $q->getQuery();

        return intval($query->getSingleScalarResult());
    }

    /**
     * @param Client $client
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOrderCountByClient(Client $client)
    {
        $qb = $this->createQueryBuilder('o');

        $q = $qb
            ->select('count(o.id)')
            ->where('o.client = :client');

        $q->setParameter('client', $client);

        $query = $q->getQuery();

        return intval($query->getSingleScalarResult());
    }

    /**
     * @param Office $office
     * @param DateTime|null $day
     * @return integer
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function getReceivedOfficeOrdersCountByDay(Office $office, DateTime $day = null)
    {
        if ($day === null) {
            $day = new DateTime();
        }

        $qb = $this->createQueryBuilder('o')
            ->select('count(o.id)')
            ->where('CAST(o.createdAt as DATE) = :day')
            ->andWhere('o.office = :office')
            ->setParameter('day', $day->format('Y-m-d'))
            ->setParameter('office', $office);

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }

    /**
     * @param Office $office
     * @param DateTime|null $day
     * @return integer
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function getDoneOfficeOrdersCountByDay(Office $office, DateTime $day = null)
    {
        if ($day === null) {
            $day = new DateTime();
        }

        $qb = $this->createQueryBuilder('o')
            ->select('count(o.id)')
            ->where('CAST(o.doneDate as date) = :day')
            ->andWhere('o.office = :office')
            ->setParameter('day', $day->format('Y-m-d'))
            ->setParameter('office', $office);

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }

    /**
     * Сумма долга клиентов по всем заказам
     *
     * @param Office $office
     * @return float
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getOrderDebtByOffice(Office $office)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT calculate_office_order_debt(:officeId) as debt';
        $stmt = $conn->prepare($sql);

        $stmt->execute([
            'officeId' => intval($office->getId()),
        ]);
        $result = $stmt->fetch();
        $sum = $result['debt'];

        return round(floatval($sum), 2);
    }

    /**
     * @param Office $office
     * @return integer
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function getOrderCountInWaitPaymentStatus(Office $office)
    {
        $qb = $this->createQueryBuilder('o')
            ->select('count(o.id)')
            ->where('o.status = :status')
            ->andWhere('o.office = :office')
            ->setParameter('status', OrderStatusEnumType::WAIT_PAYMENT_VALUE)
            ->setParameter('office', $office);

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }

    /**
     * @param Client $client
     * @return ArrayCollection
     */
    public function getClientActiveOrders(Client $client)
    {
        $qb = $this->createQueryBuilder('o')
            ->where('o.active = :active')
            ->andWhere('o.client = (:client)')
            ->andWhere('o.status NOT IN (:finStatuses)')
            ->setParameter('finStatuses', [
                OrderStatusEnumType::REJECT_VALUE,
                OrderStatusEnumType::DONE_VALUE,
            ])
            ->setParameter('active', true)
            ->setParameter('client', $client);

        $query = $qb->getQuery();

        return new ArrayCollection($query->getResult());
    }
}