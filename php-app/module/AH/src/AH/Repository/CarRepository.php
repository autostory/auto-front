<?php

namespace AH\Repository;

use AH\Entity\Client;
use AH\Entity\Firm;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;

class CarRepository extends EntityRepository
{
    /**
     * @param Firm $firm
     * @param string|null $vinFilter
     * @param string|null $registrationSignNumberFilter
     * @return ArrayCollection
     */
    public function findFirmClientCarsByFilter(
        Firm $firm,
        string $vinFilter = null,
        string $registrationSignNumberFilter = null
    )
    {
        $qb = $this->createQueryBuilder('car')
            ->leftJoin('car.owners', 'owners')
            ->leftJoin('owners.firms', 'f')
            ->where('owners.active = true')
            ->andWhere('f.id = :firm')
            ->orderBy('car.id', 'DESC')
            ->setParameter('firm', $firm->getId());

        $additionalFilter = $qb->expr()->orX();

        if ($registrationSignNumberFilter !== null && $registrationSignNumberFilter !== '') {
            $additionalFilter->add(
                $qb->expr()->andX(
                    $qb->expr()->like('LOWER(car.registrationSignNumber)', ':registrationSignNumber')
                )
            );

            $qb->setParameter(
                'registrationSignNumber',
                '%' . mb_strtolower(strval($registrationSignNumberFilter)) . '%'
            );
        }

        if ($vinFilter !== null && $vinFilter !== '') {
            $additionalFilter->add(
                $qb->expr()->andX(
                    $qb->expr()->like('LOWER(car.vin)', ':vin')
                )
            );

            $qb->setParameter(
                'vin',
                '%' . mb_strtolower(strval($vinFilter)) . '%'
            );
        }

        $qb->andWhere($additionalFilter);

        $query = $qb->getQuery();

        return new ArrayCollection($query->getResult());
    }

    /**
     * @param Client $client
     * @param string $registrationSignNumber
     * @return ArrayCollection
     */
    public function findClientCarsByRegistrationSignNumber(Client $client, string $registrationSignNumber) {
        $q = $this->createQueryBuilder('car')
            ->leftJoin('car.owners', 'owners')
            ->where('owners.id = (:client)')
            ->andWhere('lower(car.registrationSignNumber) = lower(:registrationSignNumber)')
            ->setParameter('registrationSignNumber', $registrationSignNumber)
            ->setParameter('client', $client);

        return new ArrayCollection($q->getQuery()->getResult());
    }
}