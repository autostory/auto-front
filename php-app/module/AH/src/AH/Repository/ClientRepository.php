<?php

namespace AH\Repository;

use AH\Entity\Firm;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class ClientRepository extends EntityRepository
{
    /**
     * @param QueryBuilder $qFirmClients
     * @param Firm $firm
     * @param null|string $idFilter
     * @param null|string $fio
     * @param null|string $phone
     * @param null|string $email
     * @param null|string $registrationSignNumberFilter
     * @param null|string $vin
     * @param int|null $startFromId
     * @param int|null $maxResult
     * @param string $orderType
     * @return QueryBuilder
     */
    protected function _addFilterCriteria(
        QueryBuilder $qFirmClients,
        Firm $firm,
        ?string $idFilter = null,
        ?string $fio = null,
        ?string $phone = null,
        ?string $email = null,
        ?string $registrationSignNumberFilter = null,
        ?string $vin = null,
        ?int $startFromId = null,
        ?int $maxResult = null,
        string $orderType = 'DESC'
    )
    {
        $qb = $this->createQueryBuilder('client');

        $qFirmClients->andWhere('f.id = :firm');
        $qFirmClients->setParameter('firm', $firm->getId());

        if ($maxResult > 0) {
            $qFirmClients->setMaxResults($maxResult);
            $qFirmClients->setFirstResult(0);
        }

        if ($startFromId > 0) {
            if ($orderType === 'DESC') {
                $qFirmClients->andWhere('client.id <= :startFromId');
            } else {
                $qFirmClients->andWhere('client.id >= :startFromId');
            }

            $qFirmClients->setParameter('startFromId', $startFromId);
        }

        $additionalFilter = $qb->expr()->orX();  // TODO: Если нужно чтобы было совпадение по каджому то andX для точного совпадения

        if (!empty($fio)) {
            $clearFioParts = [];
            $fioParts = explode(' ', $fio);
            if (!empty($fioParts)) {
                foreach ($fioParts as $fioPart) {
                    if (!empty($fioPart)) {
                        $clearFioParts[] = mb_strtolower(trim($fioPart));
                    }
                }
            }

            $i = 1;
            if (!empty($clearFioParts)) {
                $fioFilters = [];
                foreach ($clearFioParts as $clearFioPart) {
                    $paramName = 'fio' . $i;
                    $fioFilters[] = $qb->expr()->orX(
                        $qb->expr()->like('LOWER(client.firstName)', ':' . $paramName),
                        $qb->expr()->like('LOWER(client.surname)', ':' . $paramName),
                        $qb->expr()->like('LOWER(client.patronymic)', ':' . $paramName)
                    );

                    $qFirmClients->setParameter($paramName, '%' . $clearFioPart . '%');

                    $i = $i + 1;
                }
                if (!empty($fioFilters)) {
                    $finalFioFilterOrX = $qb->expr()->orX();
                    foreach ($fioFilters as $fioFilter) {
                        $finalFioFilterOrX->add($fioFilter);
                    }

                    $additionalFilter->add($finalFioFilterOrX);
                }
            }
        }

        if ($phone !== null && $phone !== '') {
            $additionalFilter->add(
                $qb->expr()->andX(
                    $qb->expr()->like('CAST(client.registrationPhone as TEXT)', ':phone')
                )
            );

            $qFirmClients->setParameter('phone', '%' . strval($phone) . '%');
        }

        if ($email !== null && $email !== '') {
            $additionalFilter->add(
                $qb->expr()->andX(
                    $qb->expr()->like('LOWER(client.registrationEmail)', ':email')
                )
            );
            $qFirmClients->setParameter('email', '%' . mb_strtolower(strval($email)) . '%');
        }

        if (
            ($registrationSignNumberFilter !== null && $registrationSignNumberFilter !== '')
            ||
            ($vin !== null && $vin !== '')
        ) {
            $qFirmClients
                ->leftJoin('client.cars', 'car');

            if ($registrationSignNumberFilter !== null && $registrationSignNumberFilter !== '') {
                $additionalFilter->add(
                    $qb->expr()->andX(
                        $qb->expr()->like('LOWER(car.registrationSignNumber)', ':registrationSignNumber')
                    )
                );

                $qFirmClients->setParameter(
                    'registrationSignNumber',
                    '%' . mb_strtolower(strval($registrationSignNumberFilter)) . '%'
                );
            }

            if ($vin !== null && $vin !== '') {
                $additionalFilter->add(
                    $qb->expr()->andX(
                        $qb->expr()->like('LOWER(car.vin)', ':vin')
                    )
                );

                $qFirmClients->setParameter(
                    'vin',
                    '%' . mb_strtolower(strval($vin)) . '%'
                );
            }
        }

        if ($idFilter !== null && $idFilter !== '') {
            $additionalFilter->add(
                $qb->expr()->andX(
                    $qb->expr()->like('CAST(client.id as TEXT)', ':idFilter')
                )
            );

            $qFirmClients->setParameter('idFilter', '%' . strval($idFilter) . '%');
        }

        $qFirmClients->andWhere($additionalFilter);

        return $qFirmClients;
    }

    /**
     * @param Firm $firm
     * @param null|string $idFilter
     * @param null|string $fio
     * @param null|string $phone
     * @param null|string $email
     * @param null|string $registrationSignNumberFilter
     * @param null|string $vin
     * @param int|null $afterId
     * @param int|null $first
     * @param string $orderBy
     * @param string $orderType
     * @return ArrayCollection
     * @throws \Exception
     */
    public function findFirmClientByFilter(
        Firm $firm,
        ?string $idFilter = null,
        ?string $fio = null,
        ?string $phone = null,
        ?string $email = null,
        ?string $registrationSignNumberFilter = null,
        ?string $vin = null,
        ?int $afterId = null,
        ?int $first = null,
        string $orderBy = 'id',
        string $orderType = 'DESC'
    ) :ArrayCollection
    {
        $qb = $this->createQueryBuilder('client');

        $qFirmClients = $qb
            ->leftJoin('client.firms', 'f')
            ->where('client.active = true');

        if (!in_array(strtoupper($orderType), ['ASC', 'DESC'])) {
            throw new \Exception('Wrong orderType type provided.');
        }

        if ($orderBy === 'id') {
            $qFirmClients->orderBy('client.id', $orderType);
        }

        if ($orderBy === 'category') {
            $qFirmClients->orderBy('client.category', $orderType);
        }

        if ($orderBy === 'phone') {
            $qFirmClients->orderBy('client.registrationPhone', $orderType);
        }

        if ($orderBy === 'points') {
            $qFirmClients->orderBy('client.points', $orderType);
        }

        if ($orderBy === 'email') {
            $qFirmClients->orderBy('client.registrationEmail', $orderType);
        }

        $qFirmClients = $this->_addFilterCriteria(
            $qFirmClients,
            $firm,
            $idFilter,
            $fio,
            $phone,
            $email,
            $registrationSignNumberFilter,
            $vin,
            $afterId,
            $first,
            $orderType
        );

        $query = $qFirmClients->getQuery();

        return new ArrayCollection($query->getResult());
    }

    /**
     * @param Firm $firm
     * @param null|string $idFilter
     * @param null|string $fio
     * @param null|string $phone
     * @param null|string $email
     * @param null|string $registrationSignNumberFilter
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getFirmClientsCountByFilter(
        Firm $firm,
        ?string $idFilter = null,
        ?string $fio = null,
        ?string $phone = null,
        ?string $email = null,
        ?string $registrationSignNumberFilter = null
    )
    {
        $qFirmClients = $this->createQueryBuilder('client');
        $qFirmClients->select('count(client.id)');
        $qFirmClients->leftJoin('client.firms', 'f')
            ->where('client.active = true');

        $qFirmClients = $this->_addFilterCriteria(
            $qFirmClients,
            $firm,
            $idFilter,
            $fio,
            $phone,
            $email,
            $registrationSignNumberFilter
        );

        $count = $qFirmClients->getQuery()->getSingleScalarResult();

        return $count;
    }
}