<?php

namespace AH\Repository;

use Doctrine\ORM\EntityRepository;

class CatalogCarMarkRepository extends EntityRepository
{
    public function findMarkByFilter(
        int $carTypeId = null,
        int $carMarkId = null,
        int $carModelId = null,
        int $carSubTypeId = null
    ) {
        $qb = $this->createQueryBuilder('carMark');
        $qMarks = $qb
            ->orderBy('carMark.name', 'ASC');

        if ($carTypeId) {
            $qMarks->andWhere('carMark.catalogCarType = :carTypeId');
            $qMarks->setParameter('carTypeId', $carTypeId);
        }

        if ($carMarkId) {
            $qMarks->andWhere('carMark.id = :carMarkId');
            $qMarks->setParameter('carMarkId', $carMarkId);
        }

        if ($carModelId || $carSubTypeId) {
            $qMarks->leftJoin('carMark.catalogCarModels', 'carModel');
        }

        if ($carModelId) {
            $qMarks->andWhere('carModel.id = :carModelId');
            $qMarks->setParameter('carModelId', $carModelId);
        }

        if ($carSubTypeId) {
            $qMarks->leftJoin('carModel.catalogCarModelSubTypes', 'carModelSubType');
            $qMarks->andWhere('carModelSubType.id = :carSubTypeId');
            $qMarks->setParameter('carSubTypeId', $carSubTypeId);
        }

        $query = $qMarks->getQuery();

        return $query->getResult();
    }
}