<?php

namespace AH\Repository;

use AH\Entity\Client;
use AH\Entity\Manager;
use AH\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;

class PasswordRestoreRepository extends EntityRepository
{
    /**
     * @param User $user
     * @param string $chanelType
     * @return ArrayCollection
     * @throws \Exception
     */
    public function findActiveByUser(User $user, string $chanelType = 'email')
    {
        $qb = $this->createQueryBuilder('pwdrestore');

        if ($user instanceof Client) {
            $qb->leftJoin('pwdrestore.client', 'client')
                ->where('client.active = true')
                ->andWhere('client.id = :userId');
        } else if ($user instanceof Manager) {
            $qb->leftJoin('pwdrestore.manager', 'manager')
                ->where('manager.active = true')
                ->andWhere('manager.id = :userId');
        } else {
            throw new \Exception('Wrong User Provided.');
        }

        if ($chanelType === 'email') {
            $qb->andWhere('pwdrestore.smsCode is NULL');
        }

        if ($chanelType === 'sms') {
            $qb->andWhere('pwdrestore.smsCode is NOT NULL');
        }

        $qb->andWhere('pwdrestore.validBefore > :now');

        $qb->setParameter('userId', $user->getId());

        $now = new \DateTime();
        $now->setTimezone(new \DateTimeZone('UTC'));
        $qb->setParameter('now', $now);
        $query = $qb->getQuery();

        return new ArrayCollection($query->getResult());
    }

    public function findActiveByToken(string $token, string $smsCode = null)
    {
        $qb = $this->createQueryBuilder('pwdrestore')
            ->where('pwdrestore.token = :token')
            ->andWhere('pwdrestore.validBefore > :now');
        $qb->setParameter('token', $token);

        if (empty($smsCode)) {
            $qb->andWhere('pwdrestore.smsCode IS NULL');
        } else {
            $qb->andWhere('pwdrestore.smsCode = :smsCode')
                ->setParameter('smsCode', $smsCode);
        }

        $now = new \DateTime();
        $now->setTimezone(new \DateTimeZone('UTC'));

        $qb->setParameter('now', $now);
        $qb->setMaxResults(1);

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }
}