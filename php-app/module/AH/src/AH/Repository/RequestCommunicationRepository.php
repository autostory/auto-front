<?php

namespace AH\Repository;

use AH\Entity\Request;
use Doctrine\ORM\EntityRepository;

class RequestCommunicationRepository extends EntityRepository
{
    /**
     * @param Request $request
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLastRequestCommunicationForRequest(Request $request) {
        $qb = $this->createQueryBuilder('rc');

        $q = $qb
            ->select('rc')
            ->where('rc.request = :request')
            ->setParameter('request', $request)
            ->setMaxResults(1)
            ->orderBy('rc.id', 'DESC');

        $query = $q->getQuery();

        return $query->getOneOrNullResult();
    }
}