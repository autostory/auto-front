<?php

namespace AH\Repository;

use AH\Entity\OfficeBox;
use AH\Graphql\Enum\OrderStatusEnumType;
use AH\Graphql\Enum\ScheduleStatusEnumType;
use Doctrine\ORM\EntityRepository;

class OfficeBoxRepository extends EntityRepository
{
    /**
     * @param OfficeBox $box
     * @return \DateTime|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getReleaseDateTime(OfficeBox $box)
    {
        $scheduleActiveStatuses = [
            ScheduleStatusEnumType::SCHEDULE_IN_WORK_VALUE,
            ScheduleStatusEnumType::SCHEDULE_WAIT_WORK_START_VALUE
        ];

        $orderNotInBoxStatuses = [
            OrderStatusEnumType::WAIT_PLANING_VALUE,
            OrderStatusEnumType::DONE_VALUE,
            OrderStatusEnumType::WAIT_CAR_RETURN_VALUE,
            OrderStatusEnumType::WAIT_WORK_ADD_VALUE,
            OrderStatusEnumType::REJECT_VALUE
        ];

        $qb = $this
            ->createQueryBuilder('box')
            ->select('schedules.finish')
            ->setFirstResult(0)
            ->setMaxResults(1)
            ->leftJoin('box.schedules', 'schedules')
            ->leftJoin('schedules.order', 'o')
            ->orderBy('schedules.finish', 'DESC')
            ->where('box.id = :boxId')
            ->andWhere('o.status NOT IN (:statuses)')
            ->andWhere('schedules.status IN (:scheduleActiveStatuses)')
            ->setParameter('boxId', $box->getId())
            ->setParameter('statuses', $orderNotInBoxStatuses)
            ->setParameter('scheduleActiveStatuses', $scheduleActiveStatuses);

        $query = $qb->getQuery();
        /** @var \DateTime|null $finish */
        $result = $query->getOneOrNullResult();

        if (!empty($result) && !empty($result['finish'])) {
            return $result['finish'];
        }

        return null;
    }
}