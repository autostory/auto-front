<?php

namespace AH\Repository;

use Doctrine\ORM\EntityRepository;

class CatalogCarModelSubTypeRepository extends EntityRepository
{
    public function findUniqueSubTypeByFilter(
        int $carTypeId = null
    ) {
        $qb = $this->createQueryBuilder('subType');
        $qSubTypes = $qb
            ->orderBy('subType.name', 'ASC');

        if ($carTypeId) {
            $qSubTypes
                ->leftJoin('subType.catalogCarModel', 'carModel')
                ->leftJoin('carModel.catalogCarMark', 'carMark')
                ->leftJoin('carMark.catalogCarType', 'carType');
            $qSubTypes->andWhere('carType.id = :carTypeId');
            $qSubTypes->setParameter('carTypeId', $carTypeId);
        }

        $query = $qSubTypes->getQuery();

        return $query->getResult();
    }
}