<?php

namespace AH\Repository;

use AH\Entity\Firm;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class RequestRepository extends EntityRepository
{
    /**
     * @param Firm $firm
     * @param null $active
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function _getRequestsForFirmSearchQuery(
        Firm $firm,
        $active = null
    ): QueryBuilder {
        $qb = $this->createQueryBuilder('r');

        $q = $qb
            ->leftJoin('r.firm', 'f')
            ->where('f.id = :firm');

        $q->setParameter('firm', $firm->getId());

        if ($active) {
            $q->andWhere('r.active = :active');
            $q->setParameter('active', true);

        }

        if ($active === false) {
            $q->andWhere('r.active = :active');
            $q->setParameter('active', false);
        }

        return $q;
    }

    /**
     * @param Firm $firm
     * @param null $active
     * @return ArrayCollection
     */
    public function getRequestsForFirm(
        Firm $firm,
        $active = null
    ): ArrayCollection
    {
        $q = $this->_getRequestsForFirmSearchQuery($firm, $active);
        $q->select('r');
        $q->orderBy('r.id','DESC');

        $query = $q->getQuery();

        return new ArrayCollection($query->getResult());
    }

    /**
     * @param Firm $firm
     * @param null $active
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getRequestCountForFirm(
        Firm $firm,
        $active = null
    ): int
    {
        $q = $this->_getRequestsForFirmSearchQuery($firm, $active);
        $q->select('COUNT(r.id)');

        $query = $q->getQuery();

        return intval($query->getSingleScalarResult());
    }
}