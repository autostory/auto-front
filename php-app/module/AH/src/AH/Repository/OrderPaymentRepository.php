<?php

namespace AH\Repository;

use AH\Entity\Client;
use AH\Entity\Firm;
use AH\Entity\Office;
use AH\Entity\Order;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;

class OrderPaymentRepository extends EntityRepository
{
    /**
     * @param Order $order
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPaymentSumForOrder(Order $order)
    {
        $qb = $this->createQueryBuilder('op')
            ->select('SUM(op.amount)')
            ->where('op.order = :order')
            ->setParameter('order', $order);

        $query = $qb->getQuery();

        return floatval($query->getSingleScalarResult());
    }

    /**
     * @param Client $client
     * @return float
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPaymentSumForClient(Client $client)
    {
        $qb = $this->createQueryBuilder('op')
            ->select('SUM(op.amount)')
            ->where('op.client = :client')
            ->setParameter('client', $client);

        $query = $qb->getQuery();

        return floatval($query->getSingleScalarResult());
    }

    /**
     * @param Client $client
     * @return float
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPaymentAvgForClient(Client $client)
    {
        $qb = $this->createQueryBuilder('op')
            ->select('AVG(op.amount)')
            ->where('op.client = :client')
            ->setParameter('client', $client);

        $query = $qb->getQuery();

        return floatval(number_format(floatval($query->getSingleScalarResult()), 2, '.', ''));
    }

    /**
     * @param Firm $firm
     * @return ArrayCollection
     */
    public function getPaymentForFirm(Firm $firm)
    {
        $qb = $this->createQueryBuilder('op')
            ->select('op')
            ->leftJoin('op.order', 'o')
            ->where('o.firm = :firm')
            ->orderBy('op.makeAt', 'DESC')
            ->setParameter('firm', $firm);

        $query = $qb->getQuery();

        return new ArrayCollection($query->getResult());
    }

    /**
     * @param Office $office
     * @param DateTime|null $day
     * @return float
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getDayOrderPaymentSumByOffice(Office $office, DateTime $day = null) :float
    {
        if ($day === null) {
            $day = new DateTime();
        }

        $qb = $this->createQueryBuilder('op')
            ->select('SUM(op.amount)')
            ->leftJoin('op.order', 'o')
            ->where('o.office = :office')
            ->andWhere('CAST(op.makeAt as date) = :day')
            ->setParameter('day', $day->format('Y-m-d'))
            ->setParameter('office', $office);

        $query = $qb->getQuery();
        $sum = $query->getSingleScalarResult();
        if (!$sum) {
            $sum = 0;
        }

        return round(floatval($sum),2);
    }
}