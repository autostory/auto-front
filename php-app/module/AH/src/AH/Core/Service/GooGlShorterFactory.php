<?php

namespace AH\Core\Service;

use DominikVeils\Googl\Googl;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class GooGlShorterFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');

        if (empty($config) || empty($config['googl']) || empty($config['googl']['key'])) {
            throw new \Exception('Wrong goo.gl api key was provided.');
        }

        $service = new Googl($config['googl']['key']);

        return $service;
    }
}
