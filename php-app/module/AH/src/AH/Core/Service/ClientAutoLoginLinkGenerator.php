<?php

namespace AH\Core\Service;

use AH\Entity\AutoLogin;
use AH\Entity\Client;
use Doctrine\ORM\EntityManager;
use DominikVeils\Googl\Googl;

final class ClientAutoLoginLinkGenerator
{
    /** @var EntityManager */
    protected $entityManager;
    /** @var Googl */
    protected $googl;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager, Googl $googl)
    {
        $this->entityManager = $entityManager;
        $this->googl = $googl;
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function _createToken()
    {
        return implode('', [
            bin2hex(random_bytes(4)),
            bin2hex(random_bytes(2)),
            bin2hex(chr((ord(random_bytes(1)) & 0x0F) | 0x40)) . bin2hex(random_bytes(1)),
            bin2hex(chr((ord(random_bytes(1)) & 0x3F) | 0x80)) . bin2hex(random_bytes(1)),
            bin2hex(random_bytes(6))
        ]);
    }

    /**
     * @return Googl
     */
    protected function getGoogl(): Googl
    {
        return $this->googl;
    }

    public function generate(Client $client)
    {
        $Al = new AutoLogin();

        $token = $this->_createToken();

        $Al->setClient($client);
        $Al->setToken($token);

        $em = $this->getEntityManager();
        $em->persist($Al);
        $em->flush();

        // TODO: вынести домены в конфиг сделать dev и prod
        $link = 'https://client.den-nsk.ru/auto-login/?token=' . $token;

        // TODO: Сделать сокращение ссылок
        // $shortLink = $this->getGoogl()->shorten($link);

        return $link;
    }
}