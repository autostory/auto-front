<?php

namespace AH\Core\Service;

use OAuth2\Server as OAuth2Server;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class OAuth2ServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $oauth2ServerFactory = $serviceLocator->get('ZF\OAuth2\Service\OAuth2Server');
        $server = call_user_func($oauth2ServerFactory, '/oauth');

        if (!$server instanceof OAuth2Server) {
            throw new \RuntimeException('Cant create Oauth2Server.');
        }

        /** @var AuthenticationService $authenticationService */
        $authenticationService = $serviceLocator->get('AuthenticationService');

        /** @var \Doctrine\ORM\EntityManager $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        $service = new OAuth2Service($server, $authenticationService, $entityManager);

        return $service;
    }
}
