<?php

namespace AH\Core\Service;

use AH\Core\Mail\MailService;
use AH\Core\SMS\SMSService;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mail\Transport\TransportInterface;

/**
 * @codeCoverageIgnore
 */
final class PasswordRestoreServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        /** @var MailService $mailService */
        $mailService = $serviceLocator->get(MailService::class);
        /** @var PasswordGenerator $passwordGenerator */
        $passwordGenerator = $serviceLocator->get(PasswordGenerator::class);
        /** @var SMSService $smsService */
        $smsService = $serviceLocator->get(SMSService::class);

        $service = new PasswordRestoreService(
            $entityManager,
            $mailService,
            $smsService,
            $passwordGenerator
        );

        return $service;
    }
}
