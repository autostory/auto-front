<?php

namespace AH\Core\Service;

use AH\Entity\Order;
use AH\Entity\Service\ServiceMaterial;
use AH\Entity\Service\ServiceWork;
use AH\Repository\OrderPaymentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use AH\Graphql\Enum\ScheduleStatusEnumType;
use AH\Graphql\Enum\OrderStatusEnumType;
use AH\Graphql\Enum\ServiceWorkStatusEnumType;

final class GetOrderStatusForCurrentEntityStateService
{
    protected $rOrderPayments;

    public function __construct(OrderPaymentRepository $rOrderPayments)
    {
        $this->rOrderPayments = $rOrderPayments;
    }

    protected function getROrderPayments() {
        return $this->rOrderPayments;
    }

    /**
     * @param Order $order
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getStatus(Order $order)
    {
        $isClientVisitRequired = $order->isClientVisitRequired();
        $status = $order->getStatus();

        if ($status === OrderStatusEnumType::REJECT_VALUE) {
            return OrderStatusEnumType::REJECT_VALUE;
        }

        $schedules = $order->getSchedules();
        $serviceWorks = $order->getServiceWorks();
        $haveSchedulesInWork = false;
        $isCarArrive = !empty($order->getFactClientVisitDateTime());
        $isCarReturnedToClient = !empty($order->getFactCarReturnDateTime());

        $totalBill = 0;
        if (!empty($serviceWorks)) {
            /** @var ServiceWork $serviceWork */
            foreach ($serviceWorks as $serviceWork) {
                $totalBill = $totalBill + $serviceWork->getTotalCost();
            }
        }

        $serviceMaterials = $order->getServiceMaterials();
        if (!empty($serviceMaterials)) {
            /** @var ServiceMaterial $serviceMaterial */
            foreach ($serviceMaterials as $serviceMaterial) {
                $totalBill = $totalBill + $serviceMaterial->getTotalCost();
            }
        }

        $rOrderPayments = $this->getROrderPayments();

        $paymentSum = $rOrderPayments->getPaymentSumForOrder($order);

        $isFullyPayed = true;
        if ($totalBill > 0) {
            $isFullyPayed = $paymentSum >= $totalBill;
        }

        $haveNotCompletedSchedules = false;
        if ($schedules->count()) {
            $schedulesInWorkCriteria = Criteria::create()
                ->where(Criteria::expr()->eq('status', ScheduleStatusEnumType::SCHEDULE_IN_WORK_VALUE));
            $haveSchedulesInWork = $schedules->matching($schedulesInWorkCriteria)->count() > 0;

            $notCompletedSchedulesCriteria = Criteria::create()
                ->where(Criteria::expr()->in('status', [
                    ScheduleStatusEnumType::SCHEDULE_WAIT_WORK_START_VALUE,
                    ScheduleStatusEnumType::SCHEDULE_IN_WORK_VALUE,
                ]));
            $haveNotCompletedSchedules = $schedules->matching($notCompletedSchedulesCriteria)->count() > 0;
        }

        $haveNotCompletedServiceWorks = false;
        if ($serviceWorks->count()) {
            $notCompletedServiceWorksCriteria = Criteria::create()
                ->where(Criteria::expr()->in('status', [
                    ServiceWorkStatusEnumType::SERVICE_WORK_WAIT_WORK_START_VALUE,
                    ServiceWorkStatusEnumType::SERVICE_WORK_IN_WORK_VALUE,
                ]));

            $haveNotCompletedServiceWorks = $serviceWorks->matching($notCompletedServiceWorksCriteria)->count() > 0;
        }

        $haveServiceWorks = !empty($serviceWorks->count());

        if (!$isClientVisitRequired) {
            if (!$isCarReturnedToClient && !$haveServiceWorks) {
                return OrderStatusEnumType::WAIT_WORK_ADD_VALUE;
            }

            if ($isCarArrive && !$isCarReturnedToClient && $haveNotCompletedServiceWorks && $haveSchedulesInWork) {
                return OrderStatusEnumType::IN_WORK_VALUE;
            }

            if (!$isCarReturnedToClient && $haveNotCompletedServiceWorks && !$haveNotCompletedSchedules) {
                return OrderStatusEnumType::WAIT_PLANING_VALUE;
            }

            if (!$isCarArrive && !$isCarReturnedToClient && $haveNotCompletedServiceWorks && $haveNotCompletedSchedules) {
                return OrderStatusEnumType::WAIT_CAR_ARRIVE_VALUE;
            }

            if ($isCarArrive && !$isCarReturnedToClient && $haveNotCompletedServiceWorks && $haveNotCompletedSchedules) {
                return OrderStatusEnumType::WAIT_WORK_START_VALUE;
            }

            if (!$isFullyPayed && $isCarArrive && !$haveNotCompletedServiceWorks && !$haveNotCompletedServiceWorks) {
                return OrderStatusEnumType::WAIT_PAYMENT_VALUE;
            }

            if ($isCarArrive && !$isCarReturnedToClient && !$haveNotCompletedServiceWorks && !$haveNotCompletedSchedules) {
                return OrderStatusEnumType::WAIT_CAR_RETURN_VALUE;
            }

            if ($isFullyPayed && $isCarArrive && $isCarReturnedToClient && !$haveNotCompletedServiceWorks && !$haveNotCompletedServiceWorks) {
                return OrderStatusEnumType::DONE_VALUE;
            }
        }

        if ($isClientVisitRequired) {
            if (!$isCarArrive && !$isCarReturnedToClient) {
                return OrderStatusEnumType::WAIT_CAR_ARRIVE_VALUE;
            }

            if ($isCarArrive && !$isCarReturnedToClient && !$haveServiceWorks) {
                return OrderStatusEnumType::WAIT_WORK_ADD_VALUE;
            }

            if ($isCarArrive && !$isCarReturnedToClient && $haveNotCompletedServiceWorks && $haveSchedulesInWork) {
                return OrderStatusEnumType::IN_WORK_VALUE;
            }

            if ($isCarArrive && !$isCarReturnedToClient && $haveNotCompletedServiceWorks && !$haveNotCompletedSchedules) {
                return OrderStatusEnumType::WAIT_PLANING_VALUE;
            }

            if ($isCarArrive && !$isCarReturnedToClient && $haveNotCompletedServiceWorks && $haveNotCompletedSchedules) {
                return OrderStatusEnumType::WAIT_WORK_START_VALUE;
            }

            if (!$isFullyPayed && $isCarArrive && !$haveNotCompletedServiceWorks && !$haveNotCompletedServiceWorks) {
                return OrderStatusEnumType::WAIT_PAYMENT_VALUE;
            }

            if ($isCarArrive && !$isCarReturnedToClient && !$haveNotCompletedServiceWorks && !$haveNotCompletedSchedules) {
                return OrderStatusEnumType::WAIT_CAR_RETURN_VALUE;
            }

            if ($isFullyPayed && $isCarArrive && $isCarReturnedToClient && !$haveNotCompletedServiceWorks && !$haveNotCompletedServiceWorks) {
                return OrderStatusEnumType::DONE_VALUE;
            }
        }

        return $status;
    }
}