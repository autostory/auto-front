<?php

namespace AH\Core\Service;

use AH\Entity\OrderPayment;
use AH\Repository\OrderPaymentRepository;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @codeCoverageIgnore
 */
final class GetOrderStatusForCurrentEntityStateServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        /** @var OrderPaymentRepository $rOrderPayments */
        $rOrderPayments = $entityManager->getRepository(OrderPayment::class);
        $service = new GetOrderStatusForCurrentEntityStateService($rOrderPayments);

        return $service;
    }
}
