<?php

namespace AH\Core\Service;

use Doctrine\ORM\EntityManager;
use AH\Entity;
use Zend\Authentication\AuthenticationService;

final class EventLogService
{
    protected $entityManager;
    protected $authenticationService;

    /**
     * @param AuthenticationService $authenticationService
     * @param EntityManager $entityManager
     */
    public function __construct(
        AuthenticationService $authenticationService,
        EntityManager $entityManager
    ) {
        $this->entityManager = $entityManager;
        $this->authenticationService = $authenticationService;
    }

    /**
     * @return AuthenticationService
     */
    protected function getAuthenticationService(): AuthenticationService
    {
        return $this->authenticationService;
    }

    /**
     * Get EntityManager.
     *
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * Save in DB current LogEntity.
     * @param Entity\EventLog $log
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function saveLog(Entity\EventLog $log)
    {
        if ($this->getAuthenticationService()->hasIdentity()) {
            $log->setUser($this->getAuthenticationService()->getIdentity());
        }

        $this->getEntityManager()->persist($log);
        $this->getEntityManager()->flush($log);
    }

    public function logPasswordRestoreTokenRequest(Entity\User $user, $type)
    {
        $log = new Entity\EventLog();
        $log->setTargetId($user->getId());
        $log->setTargetType(get_class($user));
        if ($type === 'email') {
            $log->setEvent('e-mail-token-request');
        } else if ($type === 'sms') {
            $log->setEvent('sms-token-request');
        } else {
            $log->setEvent('token-request');
        }
        $this->saveLog($log);
    }

    public function logPasswordRestoreMakeNewPassword(Entity\User $user)
    {
        $log = new Entity\EventLog();
        $log->setTargetId($user->getId());
        $log->setTargetType(get_class($user));
        $log->setEvent('token-request');
        $this->saveLog($log);
    }

    /**
     * Логируем создание офиса
     *
     * @param Entity\Office $office
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function logOfficeCreate(Entity\Office $office)
    {
        $log = new Entity\EventLog();
        $log->setTargetId($office->getId());
        $log->setTargetType(Entity\Office::class);
        $log->setEvent('create');
        $this->saveLog($log);
    }

    /**
     * Логируем создание бокса офиса
     *
     * @param Entity\OfficeBox $officeBox
     */
    public function logOfficeBoxCreate(Entity\OfficeBox $officeBox)
    {
        $log = new Entity\EventLog();
        $log->setTargetId($officeBox->getId());
        $log->setTargetType(Entity\OfficeBox::class);
        $log->setEvent('create');
        $this->saveLog($log);
    }

    /**
     * Логируем создание бокса офиса
     *
     * @param Entity\OfficeBox $officeBox
     */
    public function logOfficeBoxEdit(Entity\OfficeBox $officeBox)
    {
        $log = new Entity\EventLog();
        $log->setTargetId($officeBox->getId());
        $log->setTargetType(Entity\OfficeBox::class);
        $log->setEvent('edit');
        $this->saveLog($log);
    }

    /**
     * Логируем редактирование офиса
     *
     * @param Entity\Office $office
     */
    public function logOfficeEdit(Entity\Office $office)
    {
        $log = new Entity\EventLog();
        $log->setTargetId($office->getId());
        $log->setTargetType(Entity\Office::class);
        $log->setEvent('edit');
        $this->saveLog($log);
    }

    /**
     * Логируем создание заявки
     *
     * @param Entity\Order $order
     */
    public function logOrderCreate(Entity\Order $order)
    {
        $log = new Entity\EventLog();
        $log->setTargetId($order->getId());
        $log->setTargetType(Entity\Order::class);
        $log->setEvent('create');
        $this->saveLog($log);
    }
}