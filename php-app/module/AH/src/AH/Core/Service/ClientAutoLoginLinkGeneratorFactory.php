<?php

namespace AH\Core\Service;

use DominikVeils\Googl\Googl;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ClientAutoLoginLinkGeneratorFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Doctrine\ORM\EntityManager $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        /** @var Googl $google */
        $google = $serviceLocator->get('AH\Core\Service\GooGlShorter');

        $service = new ClientAutoLoginLinkGenerator($entityManager, $google);

        return $service;
    }
}
