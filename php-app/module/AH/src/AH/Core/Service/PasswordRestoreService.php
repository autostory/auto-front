<?php

namespace AH\Core\Service;

use AH\Core\Mail\MailService;
use AH\Core\SMS\SMSService;
use AH\Entity\Client;
use AH\Entity\Log\SmsLogRecord;
use AH\Entity\Manager;
use AH\Entity\PasswordRestore;
use AH\Entity\User;
use AH\Repository\PasswordRestoreRepository;
use Doctrine\ORM\EntityManager;
use Zend\Crypt\Password\Bcrypt;

final class PasswordRestoreService
{
    /** @var EntityManager */
    protected $entityManager;
    /** @var MailService */
    protected $mailService;
    /** @var SMSService */
    protected $smsService;
    /** @var PasswordGenerator PasswordGenerator */
    protected $passwordGenerator;

    /**
     * @param EntityManager $entityManager
     * @param MailService $mailService
     * @param SMSService $smsService
     * @param PasswordGenerator $passwordGenerator
     */
    public function __construct(
        EntityManager $entityManager,
        MailService $mailService,
        SMSService $smsService,
        PasswordGenerator $passwordGenerator
    )
    {
        $this->entityManager = $entityManager;
        $this->mailService = $mailService;
        $this->smsService = $smsService;
        $this->passwordGenerator = $passwordGenerator;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @return MailService
     */
    public function getMailService(): MailService
    {
        return $this->mailService;
    }

    /**
     * @return SMSService
     */
    public function getSmsService(): SMSService
    {
        return $this->smsService;
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function createToken()
    {
        return implode('', [
            bin2hex(random_bytes(4)),
            bin2hex(random_bytes(2)),
            bin2hex(chr((ord(random_bytes(1)) & 0x0F) | 0x40)) . bin2hex(random_bytes(1)),
            bin2hex(chr((ord(random_bytes(1)) & 0x3F) | 0x80)) . bin2hex(random_bytes(1)),
            bin2hex(random_bytes(6))
        ]);
    }

    public function sendNewPassword(string $token, string $smsCode = null)
    {
        /** @var PasswordRestoreRepository $rPasswordRestore */
        $rPasswordRestore = $this->getEntityManager()->getRepository(PasswordRestore::class);
        /** @var PasswordRestore $passwordRestore */
        $passwordRestore = $rPasswordRestore->findActiveByToken($token, $smsCode);
        if (empty($passwordRestore)) {
            return false;
        }

        if (!empty($passwordRestore->getClient())) {
            $user = $passwordRestore->getClient();
        }

        if (!empty($passwordRestore->getManager())) {
            /** @var User $user */
            $user = $passwordRestore->getManager();
        }

        if (empty($user)) {
            return false;
        }

        if (!$user->getActive()) {
            return false;
        }

        $newPassword = $this->getPasswordGenerator()->generateStrongPassword();

        $bcrypt = new Bcrypt();
        $bcrypt->setCost(10);
        $passwordHash = $bcrypt->create($newPassword);
        $user->setPassword($passwordHash);

        $login = $user->getRegistrationEmail();
        if (!empty($user->getRegistrationPhone())) {
            $login = $user->getRegistrationPhone();
        }

        if (empty($smsCode)) {
            $this->getMailService()->sendNewPasswordEmail(
                $user->getRegistrationEmail(),
                $user->getFullName(),
                $login,
                $newPassword
            );
        } else {
            $phone = '7' . $user->getRegistrationPhone();
            $message = $_SERVER['HTTP_HOST'] . '
логин ' . $login . '
пароль ' . $newPassword;
            $result = $this->getSmsService()->sendSms($phone, $message);

            $smsId = isset($result['smsId']) ? $result['smsId'] : null;
            $cost = isset($result['cost']) ? $result['cost'] : null;

            if (empty($smsId)) {
                throw new \Exception('Sms send error.');
            }

            $smsLog = new SmsLogRecord();
            $smsLog->setType(SmsLogRecord::PASSWORD_RESTORE_LOGIN_PASSWORD_TYPE);
            $smsLog->setAddressee($user);
            $smsLog->setSmsId($smsId);
            $smsLog->setSmsProvider($this->getSmsService()->getDefaultSmsProvider());
            $smsLog->setCost($cost);
            $smsLog->setMessage($message);
            $smsLog->setPhone($user->getRegistrationPhone());
            $this->getEntityManager()->persist($smsLog);
        }

        $this->getEntityManager()->remove($passwordRestore);
        $this->getEntityManager()->flush();

        return $user;
    }

    /**
     * @param User $user
     * @param string $chanelType
     * @return string
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function sendPasswordRestoreMessage(User $user, string $chanelType = 'email')
    {
        if (!in_array($chanelType, ['email', 'sms'])) {
            throw new \Exception('Wrong chanelType was provided');
        }

        /** @var PasswordRestoreRepository $rPasswordRestore */
        $rPasswordRestore = $this->getEntityManager()->getRepository(PasswordRestore::class);
        /** @var PasswordRestore $passwordRestore */
        $oldPasswordRestores = $rPasswordRestore->findActiveByUser($user, $chanelType);

        // Удалим все ранее запрошенные токены по нужному каналу
        if (!empty($oldPasswordRestores)) {
            foreach ($oldPasswordRestores as $oldPasswordRestore) {
                $this->getEntityManager()->remove($oldPasswordRestore);
                $this->getEntityManager()->flush();
            }
        }

        $token = $this->createToken();
        $passwordRestore = new PasswordRestore();
        $passwordRestore->setToken($token);
        if ($user instanceof Client) {
            $passwordRestore->setClient($user);
        }

        if ($user instanceof Manager) {
            $passwordRestore->setManager($user);
        }

        if ($chanelType === 'sms') {
            $key = mt_rand(11111, 99999);
            $passwordRestore->setSmsCode($key);
        }

        $this->getEntityManager()->persist($passwordRestore);
        $this->getEntityManager()->flush();

        if ($chanelType === 'email') {
            $this->getMailService()->sendPasswordRestoreEmail(
                $user->getRegistrationEmail(),
                $user->getFullName(),
                $token
            );
        }

        if ($chanelType === 'sms') {
            $phone = '7' . $user->getRegistrationPhone();
            $message = 'Код для восстановления пароля ' . $passwordRestore->getSmsCode();
            $result = $this->getSmsService()->sendSms($phone, $message);

            $smsId = isset($result['smsId']) ? $result['smsId'] : null;
            $cost = isset($result['cost']) ? $result['cost'] : null;

            if (empty($smsId)) {
                throw new \Exception('Sms send error.');
            }

            $smsLog = new SmsLogRecord();
            $smsLog->setType(SmsLogRecord::PASSWORD_RESTORE_TOKEN_TYPE);
            $smsLog->setAddressee($user);
            $smsLog->setSmsProvider($this->getSmsService()->getDefaultSmsProvider());
            $smsLog->setSmsId($smsId);
            $smsLog->setCost($cost);
            $smsLog->setMessage($message);
            $smsLog->setPhone($user->getRegistrationPhone());
            $this->getEntityManager()->persist($smsLog);
        }

        return $token;
    }

    /**
     * @return PasswordGenerator
     */
    public function getPasswordGenerator(): PasswordGenerator
    {
        return $this->passwordGenerator;
    }
}