<?php

namespace AH\Core\Service;

use AH\Entity\AutoLogin;
use AH\Entity\Client;
use AH\Repository\AutoLoginRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use OAuth2\ResponseType\AccessToken;
use OAuth2\Server as OAuth2Server;
use OAuth2\Request as OAuth2Request;
use Zend\Authentication\AuthenticationService;
use \OAuth2\Storage\Pdo as OAuth2Storage;

final class OAuth2Service
{
    protected $oauth2Server;
    protected $authenticationService;
    protected $entityManager;

    public function __construct(
        OAuth2Server $oauth2Server,
        AuthenticationService $authenticationService,
        EntityManager $entityManager
    ) {
        $this->oauth2Server = $oauth2Server;
        $this->authenticationService = $authenticationService;
        $this->entityManager = $entityManager;
    }

    /**
     * @return OAuth2Server
     */
    private function getOauth2Server(): OAuth2Server
    {
        return $this->oauth2Server;
    }

    /**
     * @return AuthenticationService
     */
    protected function getAuthenticationService(): AuthenticationService
    {
        return $this->authenticationService;
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @param string $login
     * @param string $password
     *
     * @return array
     * ['tokenInfo'][]
     *    ['access_token'] => string|null,
     *    ['expires_in'] => string|null,
     *    ['refresh_token'] => string|null,
     */
    public function login(string $login, string $password)
    {
        $server = $this->getOauth2Server();

        if (strlen($password) === 0 || strlen($login) === 0) {
            return null;
        }

        $bodyParams = [
            'grant_type' => 'password',
            'client_id' => 'ah-frontend',
            'username' => $login,
            'password' => $password,
        ];

        $content = "{\"grant_type\":\"password\",\"client_id\":\"ah-frontend\",\"username\":\"{$login}\",\"password\":\"{$password}\"}";

        $oauth2request = new OAuth2Request(
            [],
            $bodyParams,
            [],
            [],
            [],
            $_SERVER,
            $content
        );

        $response = $server->handleTokenRequest($oauth2request);

        if ($response->getStatusCode() !== 200) {
            return [
                'tokenInfo' => [
                    'access_token' => null,
                    'expires_in' => null,
                    'refresh_token' => null,
                ]
            ];
        }

        $result = $response->getParameters();

        return [
            'tokenInfo' => [
                'access_token' => isset($result['access_token']) ? $result['access_token'] : null,
                'expires_in' => isset($result['expires_in']) ? $result['expires_in'] : null,
                'refresh_token' => isset($result['refresh_token']) ? $result['refresh_token'] : null,
            ]
        ];
    }

    /**
     * @param string $autoLoginToken
     *
     * @return array
     * ['tokenInfo'][]
     *    ['access_token'] => string|null,
     *    ['expires_in'] => string|null,
     *    ['refresh_token'] => string|null,
     */
    public function loginByAutoLoginToken(string $autoLoginToken)
    {
        $em = $this->getEntityManager();

        /** @var AutoLoginRepository $rAutoLogin */
        $rAutoLogin = $em->getRepository(AutoLogin::class);
        try {
            $autoLoginTokenEntity = $rAutoLogin->findActiveByToken($autoLoginToken);
            if (!empty($autoLoginTokenEntity)) {
                $client = $autoLoginTokenEntity->getClient();
                $em->remove($autoLoginTokenEntity);
                $em->flush();
            }
        } catch (\Doctrine\ORM\NonUniqueResultException|OptimisticLockException $e) {
            $client = null;
        }

        if (empty($client) || !$client->getActive()) {
            return [
                'tokenInfo' => [
                    'access_token' => null,
                    'expires_in' => null,
                    'refresh_token' => null,
                ]
            ];
        }

        $oauthPdo = new OAuth2Storage($em->getConnection()->getWrappedConnection());
        $server = $this->getOauth2Server();
        /** @var \OAuth2\GrantType\UserCredentials $passwordGrandType */
        $passwordGrandType = $server->getGrantType('password');
        $accessToken = new AccessToken($oauthPdo, $oauthPdo);
        $result = $passwordGrandType->createAccessToken($accessToken, 'ah-frontend', $client->getId(), null);

        return [
            'tokenInfo' => [
                'access_token' => isset($result['access_token']) ? $result['access_token'] : null,
                'expires_in' => isset($result['expires_in']) ? $result['expires_in'] : null,
                'refresh_token' => isset($result['refresh_token']) ? $result['refresh_token'] : null,
            ]
        ];
    }

    /**
     * Разлогирование текущего аутентифицированного пользователя
     */
    public function logout(): void
    {
        $authenticationService = $this->getAuthenticationService();
        if (!$authenticationService->hasIdentity()) {
            return;
        }

        $server = $this->getOauth2Server();

        // Сначала отзываем access_token
        if (isset($_COOKIE['accessToken']) && !empty($_COOKIE['accessToken'])) {
            $currentAccessToken = $_COOKIE['accessToken'];

            $bodyParams = [
                'token_type_hint' => 'access_token',
                'token' => $currentAccessToken,
            ];

            $oauth2request = new OAuth2Request(
                [],
                $bodyParams,
                [],
                [],
                [],
                $_SERVER
            );

            $server->handleRevokeRequest($oauth2request);
        }

        // Потом отзываем refresh_token
        if (isset($_COOKIE['refreshToken']) && !empty($_COOKIE['refreshToken'])) {
            $currentRefreshToken = $_COOKIE['refreshToken'];

            $bodyParams = [
                'token_type_hint' => 'refresh_token',
                'token' => $currentRefreshToken,
            ];

            $oauth2request = new OAuth2Request(
                [],
                $bodyParams,
                [],
                [],
                [],
                $_SERVER
            );

            $server->handleRevokeRequest($oauth2request);
        }
    }
}