<?php

namespace AH\Core\Relay;

use Doctrine\Common\Collections\Collection;
use GraphQLRelay\Relay;

class CustomRelay
{
    public static function connectionFromArrayCollection(
        ?Collection $arrayCollection = null,
        array $args = [],
        string $edgeTypeName = 'arrayCollection'
    ) {
        $afterId = null;
        if (isset($args['after'])) {
            $afterId = intval(substr(
                base64_decode($args['after']),
                strlen($edgeTypeName . ':')
            ));
        }

        $first = null;
        if (isset($args['first']) && intval($args['first']) > 0) {
            $first = intval($args['first']);
        }

        $hasNextPage = false;
        $hasPreviousPage = false;

        if (!empty($arrayCollection) && !empty($first) && count($arrayCollection) > $first) {
            $hasNextPage = true;
        }

        if (!empty($arrayCollection) && !empty($afterId) && $arrayCollection[0]->getId() === $afterId) {
            $hasPreviousPage = true;
        }

        // Убираем лишний полученный элемент если такой есть.
        if (!empty($arrayCollection)) {
            if (!empty($afterId)) {
                $firstElement = $arrayCollection[0];
                if ($firstElement->getId() === $afterId) {
                    unset($arrayCollection[0]);
                }
            } else if (!empty($first) && count($arrayCollection) > $first) {
                unset($arrayCollection[count($arrayCollection) - 1]);
            }
        }

        $edges = null;
        if (!empty($arrayCollection)) {
            foreach ($arrayCollection as $item) {
                $edges[] = [
                    'cursor' => Relay::toGlobalId($edgeTypeName,$item->getId()),
                    'node' => $item,
                ];
            }
        }

        $firstEdge = $edges ? $edges[0] : null;
        $lastEdge = $edges ? $edges[count($edges) - 1] : null;

        return [
            'edges' => $edges,
            'pageInfo' => [
                'startCursor' => $firstEdge ? $firstEdge['cursor'] : null,
                'endCursor' => $lastEdge ? $lastEdge['cursor'] : null,
                'hasPreviousPage' => $hasPreviousPage,
                'hasNextPage' => $hasNextPage
            ]
        ];
    }
}

// https://github.com/graphql/graphql-relay-js/issues/94#issuecomment-232410564
// Start from the greedy query: SELECT * FROM table
// If the after argument is provided, add id > parsed_cursor to the WHERE clause
// If the before argument is provided, add id < parsed_cursor to the WHERE clause
// If the first argument is provided, add ORDER BY id DESC LIMIT first+1 to the query
// If the last argument is provided, add ORDER BY id ASC LIMIT last+1 to the query
// If the last argument is provided, I reverse the order of the results
// If the first argument is provided then I set hasPreviousPage: false (see spec for a description of this behavior).
// If no less than first+1 results are returned, I set hasNextPage: true, otherwise I set it to false.
// If the last argument is provided then I set hasNextPage: false (see spec for a description of this behavior).
// If no less last+1 results are returned, I set hasPreviousPage: true, otherwise I set it to false.
