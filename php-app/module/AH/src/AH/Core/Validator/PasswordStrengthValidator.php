<?php

namespace AH\Core\Validator;

use Zend\Validator\AbstractValidator;

class PasswordStrengthValidator extends AbstractValidator
{
    const NO_LETTERS = 'NO_LETTERS';
    const NO_DIGITS = 'NO_DIGITS';
    const NO_UPPER_LOWER = 'NO_UPPER_LOWER';

    protected $messageTemplates = array(
        self::NO_DIGITS => 'Пароль должен содержать хотя бы одну цифру.',
        self::NO_LETTERS => 'Пароль должен содержать хотя бы одну букву.',
        self::NO_UPPER_LOWER => 'Пароль должен содержать символы в верхнем и нижнем регистре.',
    );

    public function __construct(array $options = array())
    {
        parent::__construct($options);
    }

    public function isValid($value)
    {
        $this->setValue($value);

        if (!preg_match("#[0-9]+#", $value)) {
            $this->error(self::NO_DIGITS);
            return false;
        }

        if (!preg_match('/[[:alpha:]]+/u', $value)) {
            $this->error(self::NO_LETTERS);
            return false;
        }

        if (mb_strtolower($value) === $value || mb_strtoupper($value) === $value) {
            $this->error(self::NO_UPPER_LOWER);
            return false;
        }

        return true;
    }
}