<?php

namespace AH\Core\Adapter;

use AH\Entity\User;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result as AuthenticationResult;

class AuthenticatedIdentityAdapter implements AdapterInterface
{
    protected $entityManager;
    protected $identity;
    protected $userId;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get Entity Manager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    private function getEntityManager()
    {
        return $this->entityManager;
    }

    public function setUserId($userId) {
        $this->userId = $userId;
    }

    /**
     * Авторизовать пользователя по его идентификатору
     *
     * @return null
     * @throws \Exception
     */
    public function authenticate()
    {
        $userId = $this->userId;

        if (empty($userId)) {
            return null;
        }

        $identity = $this->getUserEntity($userId);
        if (empty($identity)) {
            throw new \Exception('Invalid user id.');
        }

        $this->identity = $identity;

        return new AuthenticationResult(AuthenticationResult::SUCCESS, $identity);
    }

    /**
     * Очистка идентификационных данных
     */
    public function clearIdentity()
    {
        $this->identity = null;
    }

    /**
     * Получить личность авторизованного пользователя
     *
     * @return User|null
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * Авторизован ли пользователь
     *
     * @return bool
     */
    public function hasIdentity()
    {
        return !empty($this->identity);
    }

    /**
     * Вытаскивание сущности авторизованного пользователя из БД
     *
     * @param $identityId
     * @return null|object
     */
    private function getUserEntity($identityId)
    {
        $rUser = $this->getEntityManager()->getRepository(User::class);
        $user = $rUser->find($identityId);

        return $user;
    }
}