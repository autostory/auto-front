<?php

namespace AH\Core\Adapter;

use ZF\OAuth2\Adapter\PdoAdapter;

/**
 * Версия адаптера OAuth2\Storage\PDO для нехешированных паролей и нестандартного столбца username в бд
 */
final class CustomOAuth2PdoAdapter extends PdoAdapter
{
    /**
     * @param string $connection
     * @param array $config
     */
    public function __construct($connection, $config = [])
    {
        parent::__construct($connection, $config);
    }

    public function getUser($identifier)
    {
        $identifier = trim($identifier);

        $userType = 'manager';
        $domain = $_SERVER['SERVER_NAME'];
        if (
            $domain === 'dev.client.ahs.local'
            || $domain === 'client.ahs.local'
            || $domain === 'client.den-nsk.ru'
        ) {
            $userType = 'client';
        }

        $stmt = $this->db->prepare(
            sprintf(
                'SELECT * FROM %s WHERE
                    (
                      lower(registration_email) = lower(:email)
                      OR
                      (registration_phone = :phone)
                    )
                    AND active IS TRUE
                    AND user_type = :userType',
                $this->config['user_table']
            )
        );

        $phone = 0;
        if (!mb_strpos($identifier, '@')) {
            $phone = intval(preg_replace('/[^0-9]/', '', $identifier));

            if (mb_strlen($phone) === 11 && intval(mb_substr($phone, 0, 1)) === 8) {
                $phone = intval(mb_substr($phone, 1));
            }
        }

        $stmt->execute([
            'email' => $identifier,
            'phone' => $phone,
            'userType' => $userType,
        ]);

        if (!$userInfo = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            return false;
        }

        // the default behavior is to use "username" as the user_id
        return array_merge(array(
            'user_id' => $userInfo['id']
        ), $userInfo);
    }
}