<?php

namespace AH\Core\Adapter;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

final class CustomOAuth2PdoAdapterFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $services)
    {
        $config = $services->get('Config');
        $em = $services->get('Doctrine\ORM\EntityManager');

        $oauth2ServerConfig = [];
        if (isset($config['zf-oauth2']['storage_settings']) && is_array($config['zf-oauth2']['storage_settings'])) {
            $oauth2ServerConfig = $config['zf-oauth2']['storage_settings'];
        }

        return new CustomOAuth2PdoAdapter($em->getConnection()->getWrappedConnection(), $oauth2ServerConfig);
    }
}
