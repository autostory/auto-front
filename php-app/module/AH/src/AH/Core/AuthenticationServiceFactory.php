<?php

namespace AH\Core;

use Zend\Authentication\Storage\NonPersistent;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Authentication\AuthenticationService;
use AH\Core\Adapter\AuthenticatedIdentityAdapter;

class AuthenticationServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $authenticationServiceAdapter = new AuthenticatedIdentityAdapter($entityManager);
        $nonPersistentStorage = new NonPersistent();

        $service = new AuthenticationService($nonPersistentStorage, $authenticationServiceAdapter);

        return $service;
    }
}
