<?php

namespace AH\Core\Relation;

use AH\Entity\Car;
use AH\Entity\Firm;
use AH\Entity\User;

class RelationDeterminer
{
    /**
     * Является ли пользователь менеджером фирмы
     *
     * @param User $user
     * @param Firm $firm
     * @return bool
     */
    public function isManagerFromFirm(User $user, Firm $firm)
    {
        return $firm->getManagers()->contains($user);
    }

    /**
     * Является ли пользователь клиентом фирмы
     *
     * @param User $user
     * @param Firm $firm
     * @return bool
     */
    public function isClientFromFirm(User $user, Firm $firm)
    {
        return $firm->getClients()->contains($user);
    }

    /**
     * Является ли пользователь клиентом фирмы
     *
     * @param User $user
     * @param Car $car
     * @return bool
     */
    public function isUserOwnedCar(User $user, Car $car)
    {
        return $car->getOwners()->contains($user);
    }
}