<?php

namespace AH\Core\Utils;

use \RuntimeException;

class WrongRelayIdException extends RuntimeException
{
    /** @var string */
    protected $relayId;

    public function __construct($relayId)
    {
        $message = sprintf('Provided wrong relayId %s.', $relayId);

        parent::__construct($message);

    }
}