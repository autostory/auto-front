<?php

namespace AH\Core\Utils;

use GraphQLRelay\Relay;

final class RelayIdUtils
{
    /**
     * @param string $relayId
     * @param string $assumeType
     * @return bool
     */
    static function isValid(string $relayId, string $assumeType): bool
    {
        $idComponents = Relay::fromGlobalId($relayId);

        if (
            !isset($idComponents['type'])
            || mb_strlen($idComponents['type']) === 0
            || !isset($idComponents['id'])
            || intval($idComponents['id']) <= 0
        ) {
            return false;
        }

        $id = intval($idComponents['id']);
        $type = $idComponents['type'];

        if ($type === $assumeType && $id > 0) {
            return true;
        }

        return false;
    }

    /**
     * @param string $relayId
     * @param string $assumeType
     * @return int
     * @throws \Exception
     */
    static function getId(string $relayId, string $assumeType): int
    {
        if (!self::isValid($relayId, $assumeType)) {
            throw new WrongRelayIdException($relayId);
        }

        $idComponents = Relay::fromGlobalId($relayId);

        return intval($idComponents['id']);
    }
}