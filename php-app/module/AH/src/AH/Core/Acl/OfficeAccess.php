<?php

namespace AH\Core\Acl;

use AH\Core\Relation\RelationDeterminer;
use AH\Entity\Firm;
use AH\Entity\Office;
use AH\Entity\User;
use BjyAuthorize\Service\Authorize;
use Zend\Authentication\AuthenticationService;

class OfficeAccess extends AbstractAccessService
{
    public function __construct(
        AuthenticationService $authenticationService,
        Authorize $authorizeService,
        RelationDeterminer $relationDeterminer
    )
    {
        parent::__construct(
            $authenticationService,
            $authorizeService,
            $relationDeterminer
        );
    }

    public function canCreateOffice(Firm $firm): bool
    {
        if ($this->getAuthorizeService()->isAllowed('any-office', 'create')) {
            return true;
        }

        /** @var User $me */
        $me = $this->getAuthenticationService()->getIdentity();
        if (!empty($me)) {
            $relationDeterminer = $this->getRelationDeterminer();

            return $relationDeterminer->isManagerFromFirm($me, $firm)
                && $this->getAuthorizeService()->isAllowed('my-firm-office', 'create');
        }

        return false;
    }

    public function canViewOffice(Office $office): bool
    {
        return $this->canViewFirmOffices($office->getFirm());
    }

    public function canViewFirmOffices(Firm $firm): bool
    {
        if ($this->getAuthorizeService()->isAllowed('any-office', 'view')) {
            return true;
        }

        /** @var User $me */
        $me = $this->getAuthenticationService()->getIdentity();
        if (!empty($me)) {
            $relationDeterminer = $this->getRelationDeterminer();

            return $relationDeterminer->isManagerFromFirm($me, $firm)
                && $this->getAuthorizeService()->isAllowed('my-firm-office', 'view');
        }

        return false;
    }

    public function canEditOffice(Office $office): bool
    {
        if ($this->getAuthorizeService()->isAllowed('any-office', 'edit')) {
            return true;
        }

        /** @var User $me */
        $me = $this->getAuthenticationService()->getIdentity();
        if (!empty($me)) {
            $relationDeterminer = $this->getRelationDeterminer();

            return $relationDeterminer->isManagerFromFirm($me, $office->getFirm())
                && $this->getAuthorizeService()->isAllowed('my-firm-office', 'edit');
        }

        return false;
    }
}
