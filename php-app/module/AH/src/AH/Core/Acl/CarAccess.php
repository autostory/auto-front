<?php

namespace AH\Core\Acl;

use AH\Core\Relation\RelationDeterminer;
use AH\Entity\Car;
use AH\Entity\Client;
use AH\Entity\Firm;
use AH\Entity\Manager;
use AH\Entity\User;
use BjyAuthorize\Service\Authorize;
use Zend\Authentication\AuthenticationService;

class CarAccess extends AbstractAccessService
{
    public function __construct(
        AuthenticationService $authenticationService,
        Authorize $authorizeService,
        RelationDeterminer $relationDeterminer
    )
    {
        parent::__construct(
            $authenticationService,
            $authorizeService,
            $relationDeterminer
        );
    }

    public function canViewCar(Car $car): bool
    {
        if ($this->getAuthorizeService()->isAllowed('any-car', 'view')) {
            return true;
        }

        $owners = $car->getOwners();
        if (empty($owners)) {
            // Т.к. к фирмам привязаны клиенты а не машины, то если нет владельцев, то и невозможно установить связь
            return false;
        }

        /** @var User $me */
        $me = $this->getAuthenticationService()->getIdentity();

        if (!empty($me) && $me instanceof Client) {
            return $this->getRelationDeterminer()->isUserOwnedCar($me, $car)
                && $this->getAuthorizeService()->isAllowed('my-car', 'view');
        }

        foreach ($owners as $owner) {
            // Есши есть хоть один владелец привязанный к этой фирме то доступ есть.
            if (!empty($me) && $me instanceof Manager) {
                return $this->getRelationDeterminer()->isClientFromFirm($owner, $me->getFirm())
                    && $this->getAuthorizeService()->isAllowed('my-client-car', 'view');
            }
        }

        return false;
    }

    public function canEditCar(Car $car): bool
    {
        return $this->canViewCar($car);
    }
}
