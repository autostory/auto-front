<?php

namespace AH\Core\Acl;

use AH\Core\Relation\RelationDeterminer;
use BjyAuthorize\Service\Authorize;
use Zend\Authentication\AuthenticationService;

abstract class AbstractAccessService
{
    /** @var AuthenticationService */
    protected $authenticationService;
    /** @var Authorize */
    protected $authorizeService;
    /** @var RelationDeterminer */
    private $relationDeterminer;

    public function __construct(
        AuthenticationService $authenticationService,
        Authorize $authorizeService,
        RelationDeterminer $relationDeterminer
    ) {
        $this->authenticationService = $authenticationService;
        $this->authorizeService = $authorizeService;
        $this->relationDeterminer = $relationDeterminer;
    }

    /**
     * @return RelationDeterminer
     */
    protected function getRelationDeterminer(): RelationDeterminer
    {
        return $this->relationDeterminer;
    }

    /**
     * @return AuthenticationService
     */
    protected function getAuthenticationService(): AuthenticationService
    {
        return $this->authenticationService;
    }

    /**
     * @return Authorize
     */
    protected function getAuthorizeService(): Authorize
    {
        return $this->authorizeService;
    }
}