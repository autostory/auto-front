<?php

namespace AH\Core\Acl;

use AH\Core\Relation\RelationDeterminer;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use BjyAuthorize\Service\Authorize;

/**
 * @codeCoverageIgnore
 */
class OfficeAccessFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var AuthenticationService $authenticationService */
        $authenticationService = $serviceLocator->get('AuthenticationService');
        /** @var Authorize $authorizeService */
        $authorizeService = $serviceLocator->get('BjyAuthorize\Service\Authorize');
        /** @var RelationDeterminer $relationDeterminer */
        $relationDeterminer = $serviceLocator->get('AH\Core\Relation\RelationDeterminer');

        $service = new OfficeAccess(
            $authenticationService,
            $authorizeService,
            $relationDeterminer
        );

        return $service;
    }
}
