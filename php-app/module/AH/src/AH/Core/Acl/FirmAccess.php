<?php

namespace AH\Core\Acl;

use AH\Core\Relation\RelationDeterminer;
use AH\Entity\Firm;
use AH\Entity\User;
use BjyAuthorize\Service\Authorize;
use Zend\Authentication\AuthenticationService;

class FirmAccess extends AbstractAccessService
{
    public function __construct(
        AuthenticationService $authenticationService,
        Authorize $authorizeService,
        RelationDeterminer $relationDeterminer
    )
    {
        parent::__construct(
            $authenticationService,
            $authorizeService,
            $relationDeterminer
        );
    }

    public function canViewFirm(Firm $firm): bool
    {
        if ($this->getAuthorizeService()->isAllowed('any-firm', 'view')) {
            return true;
        }

        /** @var User $me */
        $me = $this->getAuthenticationService()->getIdentity();
        if (!empty($me)) {
            $relationDeterminer = $this->getRelationDeterminer();

            return $relationDeterminer->isManagerFromFirm($me, $firm)
                && $this->getAuthorizeService()->isAllowed('my-firm', 'view');
        }

        return false;
    }

    public function canViewFirmClients(Firm $firm): bool
    {
        return true; // TODO: Внедрить реальную проверку прав
    }

    public function canViewFirmClientsCars(Firm $firm): bool
    {
        return true; // TODO: Внедрить реальную проверку прав
    }
}
