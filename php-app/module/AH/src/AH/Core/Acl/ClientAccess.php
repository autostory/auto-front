<?php

namespace AH\Core\Acl;

use AH\Core\Relation\RelationDeterminer;
use AH\Entity\Client;
use AH\Entity\Manager;
use AH\Entity\User;
use BjyAuthorize\Service\Authorize;
use Zend\Authentication\AuthenticationService;

class ClientAccess extends AbstractAccessService
{
    public function __construct(
        AuthenticationService $authenticationService,
        Authorize $authorizeService,
        RelationDeterminer $relationDeterminer
    )
    {
        parent::__construct(
            $authenticationService,
            $authorizeService,
            $relationDeterminer
        );
    }

    public function canViewClient(Client $client): bool
    {
        if ($this->getAuthorizeService()->isAllowed('any-client', 'view')) {
            return true;
        }

        /** @var User $me */
        $me = $this->getAuthenticationService()->getIdentity();

        if (!empty($me) && $me instanceof Client) {
            return true;
        }

        if (!empty($me) && $me instanceof Manager) {
            return $this->getRelationDeterminer()->isClientFromFirm($client, $me->getFirm())
                && $this->getAuthorizeService()->isAllowed('my-firm-client', 'view');
        }

        return false;
    }

    public function canChangeClientPassword(Client $client): bool
    {
        if ($this->getAuthorizeService()->isAllowed('any-client', 'change-password')) {
            return true;
        }

        /** @var User $me */
        $me = $this->getAuthenticationService()->getIdentity();

        if (!empty($me)) {
            if ($me->getId() === $client->getId()) {
                return $this->getAuthorizeService()->isAllowed('my-profile', 'change-password');
            }

            if (
                $me instanceof Manager
                && $this->getRelationDeterminer()->isClientFromFirm($client, $me->getFirm())
            ) {
                return $this->getAuthorizeService()->isAllowed('my-firm-client', 'change-password');
            }
        }

        return false;
    }

    public function canEditClient(Client $client): bool
    {
        if ($this->getAuthorizeService()->isAllowed('any-client', 'edit')) {
            return true;
        }

        /** @var User $me */
        $me = $this->getAuthenticationService()->getIdentity();

        if (!empty($me)) {
            if ($me->getId() === $client->getId()) {
                return $this->getAuthorizeService()->isAllowed('my-profile', 'edit');
            }

            if (
                $me instanceof Manager
                && $this->getRelationDeterminer()->isClientFromFirm($client, $me->getFirm())
            ) {
                return $this->getAuthorizeService()->isAllowed('my-firm-client', 'edit');
            }
        }

        return false;
    }

    public function canCreateClient(Client $client): bool
    {
        if ($this->getAuthorizeService()->isAllowed('any-client', 'create')) {
            return true;
        }

        /** @var User $me */
        $me = $this->getAuthenticationService()->getIdentity();

        if (!empty($me)) {
            if (
                $me instanceof Manager
                && $me->getFirm()->getActive()
                && $this->getRelationDeterminer()->isClientFromFirm($client, $me->getFirm())
            ) {
                return $this->getAuthorizeService()->isAllowed('my-firm-client', 'create');
            }
        }

        return false;
    }

    public function canAddCarToClient(Client $client): bool
    {
        if ($this->getAuthorizeService()->isAllowed('any-client', 'add-car')) {
            return true;
        }

        /** @var User $me */
        $me = $this->getAuthenticationService()->getIdentity();

        if (!empty($me)) {
            if (
                $me instanceof Manager
                && $me->getFirm()->getActive()
                && $this->getRelationDeterminer()->isClientFromFirm($client, $me->getFirm())
            ) {
                return $this->getAuthorizeService()->isAllowed('my-firm-client', 'add-car');
            }
        }

        return false;
    }
}
