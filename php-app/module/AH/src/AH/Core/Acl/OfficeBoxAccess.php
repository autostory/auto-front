<?php

namespace AH\Core\Acl;

use AH\Core\Relation\RelationDeterminer;
use AH\Entity\Office;
use AH\Entity\OfficeBox;
use BjyAuthorize\Service\Authorize;
use Zend\Authentication\AuthenticationService;

class OfficeBoxAccess extends AbstractAccessService
{
    public function __construct(
        AuthenticationService $authenticationService,
        Authorize $authorizeService,
        RelationDeterminer $relationDeterminer
    )
    {
        parent::__construct(
            $authenticationService,
            $authorizeService,
            $relationDeterminer
        );
    }

    public function canViewOfficeBox(Office $office): bool
    {
        return true;
    }

    public function canCreateOfficeBox(Office $office): bool
    {
        return true;
    }

    public function canEditOfficeBox(OfficeBox $box): bool
    {
        return true;
    }
}
