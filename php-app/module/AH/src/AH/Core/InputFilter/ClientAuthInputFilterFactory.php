<?php

namespace AH\Core\InputFilter;

use AH\Core\Validator\FieldEqValidator;
use AH\Core\Validator\PasswordStrengthValidator;
use AH\Entity\Client;
use AH\Entity\Manager;
use Zend\ServiceManager\FactoryInterface;
use Zend\InputFilter\Factory as InputFilterFactory;
use Zend\ServiceManager\ServiceLocatorInterface;
use DoctrineModule\Validator\NoObjectExists as NoObjectExistsValidator;
use \Zend\Validator\NotEmpty as NotEmptyValidator;
use \Zend\Validator\StringLength as StringLengthValidator;
use \Zend\Validator\EmailAddress as EmailAddressValidator;
use \Zend\Filter\StringTrim as StringTrimFilter;
use \Zend\Filter\StringToLower as StringToLowerFilter;

class ClientAuthInputFilterFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $services)
    {
        $entityManager = $services->get('Doctrine\ORM\EntityManager');

        $emailInvalidFormatErrorMessageText = "Указанный вами E-mail не является правильным адресом E-mail.";

        $inputFilterFactory = new InputFilterFactory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            'newPassword' => [
                'name' => 'newPassword',
                'required' => true,
                'validators' => [
                    [
                        'name' => NotEmptyValidator::class,
                        'options' => [
                            'encoding' => 'UTF-8',
                            'break_chain_on_failure' => true,
                            'messages' => [
                                NotEmptyValidator::IS_EMPTY => 'Пароль обязателен для заполнения.',
                                NotEmptyValidator::INVALID => 'Пароль обязателен для заполнения.',
                            ],
                        ],
                    ],
                    [
                        'name' => StringLengthValidator::class,
                        'options' => [
                            'min' => 8,
                            'max' => 100,
                            'break_chain_on_failure' => true,
                            'messages' => array(
                                StringLengthValidator::TOO_LONG => 'Пароль не должен быть длинной больше %max% символов.',
                                StringLengthValidator::TOO_SHORT => 'Пароль должен быть не менее %min% символов.'
                            ),
                        ],
                    ],
                    [
                        'name' => PasswordStrengthValidator::class
                    ]
                ],
            ],
            'newPasswordConfirm' => [
                'name' => 'newPasswordConfirm',
                'required' => true,
                'validators' => [
                    [
                        'name' => NotEmptyValidator::class,
                        'options' => [
                            'encoding' => 'UTF-8',
                            'break_chain_on_failure' => true,
                            'messages' => [
                                NotEmptyValidator::IS_EMPTY => 'Подтверждение пароля обязателено для заполнения.',
                                NotEmptyValidator::INVALID => 'Подтверждение пароля обязателено для заполнения.',
                            ],
                        ],
                    ],
                    [
                        'name' => FieldEqValidator::class,
                        'options' => [
                            'field' => 'newPassword', // Какому полю он должен быть идентичен
                            'break_chain_on_failure' => true,
                            'messages' => array(
                                FieldEqValidator::DIFFERENT_FROM => 'Подтверждение пароля отличается от нового пароля.',
                            ),
                        ],
                    ],
                ],
            ],
            'email' => [
                'name' => 'email',
                'required' => false,
                'filters' => [
                    [
                        'name' => StringTrimFilter::class,
                    ],
                    [
                        'name' => StringToLowerFilter::class,
                    ]
                ],
                'validators' => [
                    [
                        'name' => EmailAddressValidator::class,
                        'options' => [
                            'encoding' => 'UTF-8',
                            'break_chain_on_failure' => true,
                            'messages' => [
                                EmailAddressValidator::INVALID => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::INVALID_FORMAT => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::INVALID_HOSTNAME => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::INVALID_MX_RECORD => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::INVALID_SEGMENT => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::DOT_ATOM => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::QUOTED_STRING => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::INVALID_LOCAL_PART => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::LENGTH_EXCEEDED => $emailInvalidFormatErrorMessageText,
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        $noObjectExistsValidator = new NoObjectExistsValidator([
            'object_repository' => $entityManager->getRepository(Client::class),
            'fields' => 'registrationEmail',
            'messageTemplates' => [
                NoObjectExistsValidator::ERROR_OBJECT_FOUND => 'Кто-то уже зарегистрировался используя указанный E-mail.',
            ],
        ]);

        // Проверим что такой email не используется
        $inputFilter->get('email')->getValidatorChain()->attach($noObjectExistsValidator);

        return $inputFilter;
    }
}
