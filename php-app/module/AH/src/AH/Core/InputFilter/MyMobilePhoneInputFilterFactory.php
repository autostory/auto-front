<?php

namespace AH\Core\InputFilter;

use Zend\I18n\Validator\PhoneNumber;
use Zend\ServiceManager\FactoryInterface;
use Zend\InputFilter\Factory as InputFilterFactory;
use Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\Filter\StringTrim as StringTrimFilter;
use \Zend\Filter\Digits as DigitsFilter;
use AH\Entity\User;
use DoctrineModule\Validator\UniqueObject as UniqueObjectValidator;

class MyMobilePhoneInputFilterFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $services)
    {
        $entityManager = $services->get('Doctrine\ORM\EntityManager');

        $inputFilterFactory = new InputFilterFactory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            'mobilePhone' => [
                'name' => 'mobilePhone',
                'required' => true,
                'filters' => [
                    [
                        'name' => StringTrimFilter::class,
                    ],
                    [
                        'name' => DigitsFilter::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => PhoneNumber::class,
                        'options' => [
                            'country' => 'RU', // ISO 3611 Country Code  RU	RUS	643	ISO 3166-2:RU
                            'allowed_types' => ['mobile'], // ['general','fixed','tollfree','personal','mobile','voip','uan',];
                            'break_chain_on_failure' => true,
                            'messages' => [
                                PhoneNumber::NO_MATCH => 'Неправильный номер телефона.',
                                PhoneNumber::UNSUPPORTED => 'Неправильный номер телефона.',
                                PhoneNumber::INVALID => 'Неправильный номер телефона',
                            ],
                        ],
                    ],
                    [
                        'name' => UniqueObjectValidator::class,
                        'options' => [
                            'object_manager' => $entityManager,
                            'object_repository' => $entityManager->getRepository(User::class),
                            'use_context'       => true,
                            'fields' => 'registrationPhone',
                            'messageTemplates' => [
                                UniqueObjectValidator::ERROR_OBJECT_NOT_UNIQUE => 'Кто-то уже зарегистрировался используя указанный номер телефона.',
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        return $inputFilter;
    }
}
