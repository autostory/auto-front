<?php

namespace AH\Core\InputFilter;

use Zend\ServiceManager\FactoryInterface;
use Zend\InputFilter\Factory as InputFilterFactory;
use Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\Validator\EmailAddress as EmailAddressValidator;
use \Zend\Filter\StringTrim as StringTrimFilter;
use \Zend\Filter\StringToLower as StringToLowerFilter;
use AH\Entity\User;
use DoctrineModule\Validator\UniqueObject as UniqueObjectValidator;

class MyEmailFilterFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $services)
    {
        $emailInvalidFormatErrorMessageText = "Указанный вами E-mail не является правильным адресом E-mail.";
        $entityManager = $services->get('Doctrine\ORM\EntityManager');

        $inputFilterFactory = new InputFilterFactory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            'email' => [
                'name' => 'email',
                'required' => false,
                'filters' => [
                    [
                        'name' => StringTrimFilter::class,
                    ],
                    [
                        'name' => StringToLowerFilter::class,
                    ]
                ],
                'validators' => [
                    [
                        'name' => EmailAddressValidator::class,
                        'options' => [
                            'encoding' => 'UTF-8',
                            'break_chain_on_failure' => true,
                            'messages' => [
                                EmailAddressValidator::INVALID => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::INVALID_FORMAT => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::INVALID_HOSTNAME => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::INVALID_MX_RECORD => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::INVALID_SEGMENT => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::DOT_ATOM => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::QUOTED_STRING => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::INVALID_LOCAL_PART => $emailInvalidFormatErrorMessageText,
                                EmailAddressValidator::LENGTH_EXCEEDED => $emailInvalidFormatErrorMessageText,
                            ],
                        ],
                    ],
                    [
                        'name' => UniqueObjectValidator::class,
                        'options' => [
                            'object_manager' => $entityManager,
                            'object_repository' => $entityManager->getRepository(User::class),
                            'use_context'       => true,
                            'fields' => 'registrationEmail',
                            'messageTemplates' => [
                                UniqueObjectValidator::ERROR_OBJECT_NOT_UNIQUE => 'Кто-то уже зарегистрировался используя указанный E-mail.',
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        return $inputFilter;
    }
}
