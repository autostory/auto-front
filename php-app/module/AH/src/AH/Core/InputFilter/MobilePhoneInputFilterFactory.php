<?php

namespace AH\Core\InputFilter;

use Zend\I18n\Validator\PhoneNumber;
use Zend\ServiceManager\FactoryInterface;
use Zend\InputFilter\Factory as InputFilterFactory;
use Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\Filter\StringTrim as StringTrimFilter;
use \Zend\Filter\Digits as DigitsFilter;
use AH\Entity\User;
use DoctrineModule\Validator\NoObjectExists as NoObjectExistsValidator;

class MobilePhoneInputFilterFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $services)
    {
        $inputFilterFactory = new InputFilterFactory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            'mobilePhone' => [
                'name' => 'mobilePhone',
                'required' => true,
                'filters' => [
                    [
                        'name' => StringTrimFilter::class,
                    ],
                    [
                        'name' => DigitsFilter::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => PhoneNumber::class,
                        'options' => [
                            'country' => 'RU', // ISO 3611 Country Code  RU	RUS	643	ISO 3166-2:RU
                            'allowed_types' => ['mobile'], // ['general','fixed','tollfree','personal','mobile','voip','uan',];
                            'break_chain_on_failure' => true,
                            'messages' => [
                                PhoneNumber::NO_MATCH => 'Неправильный номер телефона.',
                                PhoneNumber::UNSUPPORTED => 'Неправильный номер телефона.',
                                PhoneNumber::INVALID => 'Неправильный номер телефона',
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        $entityManager = $services->get('Doctrine\ORM\EntityManager');

        $noObjectExistsValidator = new NoObjectExistsValidator([
            'object_repository' => $entityManager->getRepository(User::class),
            'fields' => 'registrationPhone',
            'messageTemplates' => [
                NoObjectExistsValidator::ERROR_OBJECT_FOUND => 'Кто-то уже зарегистрировался используя указанный номер телефона.',
            ],
        ]);

        // Проверим что такой phone не используется
        $inputFilter->get('mobilePhone')->getValidatorChain()->attach($noObjectExistsValidator);

        return $inputFilter;
    }
}
