<?php

namespace AH\Core\InputFilter;

use Zend\I18n\Validator\PhoneNumber;
use Zend\ServiceManager\FactoryInterface;
use Zend\InputFilter\Factory as InputFilterFactory;
use Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\Filter\StringTrim as StringTrimFilter;
use \Zend\Filter\Digits as DigitsFilter;

class PhoneInputFilterFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $services)
    {
        $inputFilterFactory = new InputFilterFactory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            'mobilePhone' => [
                'name' => 'phone',
                'required' => true,
                'filters' => [
                    [
                        'name' => StringTrimFilter::class,
                    ],
                    [
                        'name' => DigitsFilter::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => PhoneNumber::class,
                        'options' => [
                            'country' => 'RU', // ISO 3611 Country Code  RU	RUS	643	ISO 3166-2:RU
                            'allowed_types' => ['general','fixed','tollfree','personal','mobile','voip','uan',],
                            'break_chain_on_failure' => true,
                            'messages' => [
                                PhoneNumber::NO_MATCH => 'Неправильный номер телефона.',
                                PhoneNumber::UNSUPPORTED => 'Неправильный номер телефона.',
                                PhoneNumber::INVALID => 'Неправильный номер телефона',
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        return $inputFilter;
    }
}
