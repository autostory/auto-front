<?php

namespace AH\Core\InputFilter;

use Zend\ServiceManager\FactoryInterface;
use Zend\InputFilter\Factory as InputFilterFactory;
use Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\Validator\StringLength as StringLengthValidator;
use \Zend\Filter\StringTrim as StringTrimFilter;

class IdentityFullNameFilterFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $services)
    {
        $inputFilterFactory = new InputFilterFactory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            'firstName' => [
                'name' => 'firstName',
                'required' => false,
                'filters' => [
                    [
                        'name' => StringTrimFilter::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => StringLengthValidator::class,
                        'options' => [
                            'max' => 255,
                            'break_chain_on_failure' => true,
                            'messages' => array(
                                StringLengthValidator::TOO_LONG => 'Имя не должно быть длинной больше %max% символов.',
                            ),
                        ],
                    ],
                ],
            ],
            'surname' => [
                'name' => 'firstName',
                'required' => false,
                'filters' => [
                    [
                        'name' => StringTrimFilter::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => StringLengthValidator::class,
                        'options' => [
                            'max' => 255,
                            'break_chain_on_failure' => true,
                            'messages' => array(
                                StringLengthValidator::TOO_LONG => 'Фамилия не должена быть длинной больше %max% символов.',
                            ),
                        ],
                    ],
                ],
            ],
            'patronymic' => [
                'name' => 'firstName',
                'required' => false,
                'filters' => [
                    [
                        'name' => StringTrimFilter::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => StringLengthValidator::class,
                        'options' => [
                            'max' => 255,
                            'break_chain_on_failure' => true,
                            'messages' => array(
                                StringLengthValidator::TOO_LONG => 'Отчество не должно быть длинной больше %max% символов.',
                            ),
                        ],
                    ],
                ],
            ],
        ]);

        return $inputFilter;
    }
}
