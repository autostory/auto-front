<?php

namespace AH\Core\InputFilter;

use \Zend\Filter\StringTrim as StringTrimFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\InputFilter\Factory as InputFilterFactory;
use Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\Validator\NotEmpty as NotEmptyValidator;
use \Zend\Validator\StringLength as StringLengthValidator;

class NewFirmAndManagerInputFilterFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $services)
    {
        /** @var InputFilterInterface $managerAuthInputFilter */
        $managerAuthInputFilter = $services->get('ManagerAuthInputFilter');

        $inputFilterFactory = new InputFilterFactory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            'firmName' => [
                'name' => 'firmName',
                'required' => true,
                'filters' => [
                    [
                        'name' => StringTrimFilter::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => NotEmptyValidator::class,
                        'options' => [
                            'encoding' => 'UTF-8',
                            'break_chain_on_failure' => true,
                            'messages' => [
                                NotEmptyValidator::IS_EMPTY => 'Название организации обязательно для заполнения.',
                                NotEmptyValidator::INVALID => 'Название организации обязательно для заполнения.',
                            ],
                        ],
                    ],
                    [
                        'name' => StringLengthValidator::class,
                        'options' => [
                            'max' => 255,
                            'break_chain_on_failure' => true,
                            'messages' => array(
                                StringLengthValidator::TOO_LONG => 'Название организации не должно быть длинной больше %max% символов.',
                            ),
                        ],
                    ],
                ],
            ],
            'phone' => [
                'name' => 'phone',
                'required' => true,
                'filters' => [
                    [
                        'name' => StringTrimFilter::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => NotEmptyValidator::class,
                        'options' => [
                            'encoding' => 'UTF-8',
                            'break_chain_on_failure' => true,
                            'messages' => [
                                NotEmptyValidator::IS_EMPTY => 'Поле обязательно для заполнения.',
                                NotEmptyValidator::INVALID => 'Поле обязательно для заполнения.',
                            ],
                        ],
                    ],
                    [
                        'name' => StringLengthValidator::class,
                        'options' => [
                            'max' => 50,
                            'break_chain_on_failure' => true,
                            'messages' => [
                                StringLengthValidator::TOO_LONG => 'Значение не должено быть длинной больше %max% символов.',
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        foreach ($managerAuthInputFilter->getInputs() as $input) {
            // Добавляем поля email, пароль и подтверждение пароля
            $inputFilter->add($input);
        }

        return $inputFilter;
    }
}
