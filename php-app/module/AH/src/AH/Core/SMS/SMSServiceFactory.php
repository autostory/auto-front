<?php

namespace AH\Core\SMS;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @codeCoverageIgnore
 */
final class SMSServiceFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return SMSService|mixed
     * @throws \Exception
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $defaultSmsProvider = getenv('AH_SMS_DEFAULT_PROVIDER');

        $service = new SMSService($defaultSmsProvider);

        return $service;
    }
}
