<?php

namespace AH\Core\SMS;

use SmsaeroApiV2;

include 'smsc_api.php';
include 'SmsaeroApiV2.php';

final class SMSService
{
    const SMS_WAIT_SENDING = -1;
    const SMS_DELIVERED_TO_PROVIDER = 0;
    const SMS_DELIVERED = 1;
    const SMS_CANT_DELIVER = 20;
    const SMS_TIMES_IS_UP = 40;
    const SMS_FILTER_REJECT = 50;
    const SMS_WRONG_NUMBER = 418;
    const SMS_NO_MONEY_NO_HONEY = 69;
    const SMS_UNKNOWN_STATUS = 999;

    const SMSC_PROVIDER_NAME = 'smsc';
    const SMSAERO_PROVIDER_NAME = 'smsaero';

    protected $smsAeroApi;
    protected $defaultSmsProvider;

    /**
     * SMSService constructor.
     * @param string $smsProvider
     * @throws \Exception
     */
    public function __construct(string $smsProvider)
    {
        $this->checkSmsProviderName($smsProvider);

        $this->defaultSmsProvider = $smsProvider;
        $this->smsAeroApi = new SmsaeroApiV2();
    }

    /**
     * @param string $smsProvider
     * @throws \Exception
     */
    protected function checkSmsProviderName(string $smsProvider)
    {
        if (!in_array($smsProvider, [
            self::SMSC_PROVIDER_NAME,
            self::SMSAERO_PROVIDER_NAME,
        ])) {
            throw new \Exception('Wrong sms provider provided for SMSService.');
        }
    }

    /**
     * @return string
     */
    public function getDefaultSmsProvider(): string
    {
        return $this->defaultSmsProvider;
    }

    /**
     * @return SmsaeroApiV2
     */
    public function getSmsAeroApi(): SmsaeroApiV2
    {
        return $this->smsAeroApi;
    }

    /**
     * Конвертор статусов заказа
     *
     * @param string $smsProvider
     * @param string $status
     * @return int
     * @throws \Exception
     */
    private function _getStatusCode(string $status, string $smsProvider = null)
    {
        if (empty($smsProvider)) {
            $smsProvider = $this->getDefaultSmsProvider();
        }

        $this->checkSmsProviderName($smsProvider);

        if ($smsProvider === self::SMSC_PROVIDER_NAME) {
            /**
             * -1    Ожидает отправки    Если при отправке сообщения было задано время получения абонентом, то до этого времени сообщение будет находиться в данном статусе, в других случаях сообщение в этом статусе находится непродолжительное время перед отправкой на SMS-центр.
             * 0    Передано оператору    Сообщение было передано на SMS-центр оператора для доставки.
             * 1    Доставлено    Сообщение было успешно доставлено абоненту.
             * 3    Просрочено    Возникает, если время "жизни" сообщения истекло, а оно так и не было доставлено получателю, например, если абонент не был доступен в течение определенного времени или в его телефоне был переполнен буфер сообщений.
             * 20    Невозможно доставить    Попытка доставить сообщение закончилась неудачно, это может быть вызвано разными причинами, например, абонент заблокирован, не существует, находится в роуминге без поддержки обмена SMS, или на его телефоне не поддерживается прием SMS-сообщений.
             * 22    Неверный номер    Неправильный формат номера телефона.
             * 23    Запрещено    Возникает при срабатывании ограничений на отправку дублей, на частые сообщения на один номер (флуд), на номера из черного списка, на запрещенные спам фильтром тексты или имена отправителей (Sender ID).
             * 24    Недостаточно средств    На счете Клиента недостаточная сумма для отправки сообщения.
             * 25    Недоступный номер    Телефонный номер не принимает SMS-сообщения, или на этого оператора нет рабочего маршрута.
             */

            $resultStatus = self::SMS_UNKNOWN_STATUS;
            switch ($status) {
                case '-1':
                    $resultStatus = self::SMS_WAIT_SENDING;
                    break;
                case '0':
                    $resultStatus = self::SMS_DELIVERED_TO_PROVIDER;
                    break;
                case '1':
                    $resultStatus = self::SMS_DELIVERED;
                    break;
                case '3':
                    $resultStatus = self::SMS_TIMES_IS_UP;
                    break;
                case '20':
                case '25':
                    $resultStatus = self::SMS_CANT_DELIVER;
                    break;
                case '22':
                    $resultStatus = self::SMS_WRONG_NUMBER;
                    break;
                case '23':
                    $resultStatus = self::SMS_FILTER_REJECT;
                    break;
                case '24':
                    $resultStatus = self::SMS_NO_MONEY_NO_HONEY;
                    break;
            }

            return $resultStatus;
        }

        if ($smsProvider === self::SMSAERO_PROVIDER_NAME) {
            /**
             * Статус сообщения
             * 0 — в очереди
             * 4 — ожидание статуса сообщения
             * 3 — передано
             * 6 — сообщение отклонено
             * 1 — доставлено
             * 2 — не доставлено
             * 8 — на модерации
             *  */

            $resultStatus = self::SMS_UNKNOWN_STATUS;
            switch ($status) {
                case '0':
                case '4':
                case '8':
                    $resultStatus = self::SMS_WAIT_SENDING;
                    break;
                case '3':
                    $resultStatus = self::SMS_DELIVERED_TO_PROVIDER;
                    break;
                case '1':
                    $resultStatus = self::SMS_DELIVERED;
                    break;
                case '2':
                    $resultStatus = self::SMS_CANT_DELIVER;
                    break;
                case '6':
                    $resultStatus = self::SMS_FILTER_REJECT;
                    break;
            }

            return $resultStatus;
        }

        throw new \Exception('Wrong sms provider was provided');
    }

    /**
     * @param string $phone
     * @param string $message
     * @param string $smsProvider
     * @return array | false
     * [
     *      'smsId' => (string) sms id
     *      'cost' => (string) стоимость отправленного смс
     * ]
     * @throws \Exception
     */
    public function sendSms(string $phone, string $message, string $smsProvider = null)
    {
        if (empty($smsProvider)) {
            $smsProvider = $this->getDefaultSmsProvider();
        }

        $this->checkSmsProviderName($smsProvider);

        if ($smsProvider === self::SMSC_PROVIDER_NAME) {
            list($sms_id, $sms_cnt, $cost, $balance) = send_sms($phone, $message);

            return [
                'smsId' => $sms_id,
                'cost' => $cost,
            ];
        }

        if ($smsProvider === self::SMSAERO_PROVIDER_NAME) {
            $getSmsAeroApi = $this->getSmsAeroApi();

            $data = $getSmsAeroApi->send($phone, $message, 'DIRECT');

            if (empty($data) || (is_array($data) && empty($data['success']))) {
                return false;
            }

            return [
                'smsId' => $data['data']['id'],
                'cost' => $data['data']['cost'],
            ];
        }

        throw new \Exception('Wrong sms provider was provided');
    }

    /**
     * @param string $smsId
     * @param string $phone
     *
     * @param string $smsProvider
     * @return array
     * [
     *      'status' => (integer) sms status code
     *      'changeDate' => (string|null) timestamp of change sms status date
     * ]
     * @throws \Exception
     */
    public function getSmsStatus(string $smsId, string $phone, string $smsProvider = null)
    {
        if (empty($smsProvider)) {
            $smsProvider = $this->getDefaultSmsProvider();
        }

        $this->checkSmsProviderName($smsProvider);

        if ($smsProvider === self::SMSC_PROVIDER_NAME) {
            // $time время смены статуса
            list($status, $time) = get_status($smsId, $phone);

            return [
                'status' => $this->_getStatusCode(self::SMSC_PROVIDER_NAME, $status),
                'changeDate' => $time,
            ];
        }

        if ($smsProvider === self::SMSAERO_PROVIDER_NAME) {
            $getSmsAeroApi = $this->getSmsAeroApi();
            $data = $getSmsAeroApi->check_send($smsId);

            if (empty($data) || (is_array($data) && empty($data['success']))) {
                throw new \RuntimeException('Error while check sms status');
            }

            return [
                'status' => $this->_getStatusCode(self::SMSAERO_PROVIDER_NAME, $data['data']['status']),
                'changeDate' => null,
            ];
        }

        throw new \Exception('Wrong sms provider was provided');
    }
}