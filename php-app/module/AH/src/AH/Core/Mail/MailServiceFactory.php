<?php

namespace AH\Core\Mail;

use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mail\Transport\TransportInterface;

/**
 * @codeCoverageIgnore
 */
final class MailServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var TransportInterface $transport */
        $transport = $serviceLocator->get('SlmMail\Mail\Transport\MailgunTransport');

        $config = $serviceLocator->get('Config');

        if (
            !isset($config['mail-sender'])
            || !isset($config['mail-sender']['email'])
        ) {
            throw new \Exception('No mail sender was provided.');
        }

        $sender = [
          'email' => $config['mail-sender']['email'],
          'name' => isset($config['mail-sender']['name']) ? $config['mail-sender']['name'] : '',
        ];;

        $service = new MailService(
            $transport,
            $sender
        );

        return $service;
    }
}
