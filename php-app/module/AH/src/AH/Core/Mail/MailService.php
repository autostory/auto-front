<?php

namespace AH\Core\Mail;

use AH\Entity\Client;
use AH\Entity\Manager;
use AH\Entity\PasswordRestore;
use AH\Entity\User;
use AH\Repository\PasswordRestoreRepository;
use Doctrine\ORM\EntityManager;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use SlmMail\Mail\Message\Mailgun;
use Zend\Crypt\Password\Bcrypt;
use Zend\Mail\Transport\TransportInterface;

final class MailService
{
    /** @var TransportInterface */
    protected $mailTransport;
    /** @var array */
    protected $sender;

    /**
     * @param TransportInterface $mailTransport
     * @param array $sender
     */
    public function __construct(
        TransportInterface $mailTransport,
        array $sender
    )
    {
        $this->mailTransport = $mailTransport;
        $this->sender = $sender;
    }

    /**
     * @return TransportInterface
     */
    public function getMailTransport(): TransportInterface
    {
        return $this->mailTransport;
    }

    /**
     * @return array
     */
    public function getSender(): array
    {
        return $this->sender;
    }

    public function sendNewPasswordEmail(
        string $toEmail,
        string $toName,
        string $login,
        string $password
    )
    {
        $message = new Mailgun();
        $message->addTo($toEmail);

        $message->setSubject("Новый пароль на сервисе " . $_SERVER['HTTP_HOST']);

        $htmlMarkup = "
        <html>
            <body>
                Привет, %recipient.name%!<br/><br/>
                Вот новый пароль для <a href='>https://" . $_SERVER['HTTP_HOST'] . "/'>https://" . $_SERVER['HTTP_HOST'] . "/</a><br/>
                <br/>
                логин: <b>%recipient.login%</b><br/>
                пароль: <b>%recipient.password%</b><br/>
            </body>
        </html>
        ";

        $html = new MimePart($htmlMarkup);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->setParts([$html]);

        $message->setBody($body);

        $message->addTag('password-restore-send-password');

        $sender = $this->getSender();
        $message->setFrom($sender['email'], $sender['name']);

        $message->setRecipientVariables(
            $toEmail,
            [
                'name' => $toName,
                'password' => $password,
                'login' => $login,
            ]
        );

        return $this->getMailTransport()->send($message);
    }

    public function sendPasswordRestoreEmail(
        string $toEmail,
        string $toName,
        string $token
    )
    {
        $message = new Mailgun();
        $message->addTo($toEmail);

        $message->setSubject("Восстановление пароля для сервиса " . $_SERVER['HTTP_HOST']);

        $htmlMarkup = "
        <html>
            <body>
                Привет, %recipient.name%!<br/><br/>
                 
                Для восстановления пароля перейдите по  
                <a href='https://" . $_SERVER['HTTP_HOST'] . "/password-restore/?token=%recipient.token%&email=%recipient.email%'>
                    ссылке
                </a>.
            </body>
        </html>
        ";

        $html = new MimePart($htmlMarkup);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->setParts([$html]);

        $message->setBody($body);

        $message->addTag('password-restore-send-token');

        $sender = $this->getSender();
        $message->setFrom($sender['email'], $sender['name']);

        $message->setRecipientVariables(
            $toEmail,
            [
                'name' => $toName,
                'token' => $token,
                'email' => $toEmail,
            ]
        );

        return $this->getMailTransport()->send($message);
    }

    /**
     * @param string $toEmail
     * @param string $toName
     * @param string $login
     * @param string $password
     * @param string|null $autoLoginLink
     * @param string|null $activateToken
     * @return mixed
     */
    public function sendRegistrationEmailToClient(
        string $toEmail,
        string $toName,
        string $login,
        string $password,
        string $autoLoginLink = null,
        string $activateToken = null
    )
    {
        $message = new Mailgun();
        $message->addTo($toEmail);

        $message->setSubject('Новый аккаунт на сервисе client.den-nsk.ru');

        $message->addTag('new-client-registration');

        if ($activateToken) {
            $message->addTag('new-client-registration-with-token');
        }

        // TODO: Использовать twig
        $body = file_get_contents('/var/www/avto-history/php-app/module/AH/view/emailTemplates/client-registration.html');

        $html = new MimePart($body);
        $html->type = "text/html";
        $body = new MimeMessage();
        $body->addPart($html);

        $message->setBody($body);

        $sender = $this->getSender();
        $message->setFrom($sender['email'], $sender['name']);

        $needToActivateText = '';
        if ($activateToken) {
            $needToActivateText = 'Для активации аккаунта перейдите по <a href="https://%recipient.siteHost%/activate/client/' . $activateToken . '">ссылке</a>.';
        }

        $message->setRecipientVariables(
            $toEmail,
            [
                'siteHost' => 'client.den-nsk.ru',
                'autoLoginLink' => $autoLoginLink,
                'name' => $toName,
                'login' => $login,
                'password' => $password,
                'needToActivateText' => $needToActivateText,
            ]
        );

        return $this->getMailTransport()->send($message);
    }

    public function sendEmailWithAuthDataToNewManager(
        string $toEmail,
        string $toName,
        string $login,
        string $password
    )
    {
        $message = new Mailgun();
        $message->addTo($toEmail);

        $message->setSubject("Новый аккаунт на сервисе " . $_SERVER['HTTP_HOST']);

        $message->addTag('new-manager-registration');

        $body = file_get_contents('/var/www/avto-history/php-app/module/AH/view/emailTemplates/manager-created.html');

        $html = new MimePart($body);
        $html->type = "text/html";
        $body = new MimeMessage();
        $body->addPart($html);

        $message->setBody($body);

        $sender = $this->getSender();
        $message->setFrom($sender['email'], $sender['name']);

        $needToActivateText = '';

        $message->setRecipientVariables(
            $toEmail,
            [
                'siteHost' => $_SERVER['HTTP_HOST'],
                'name' => $toName,
                'login' => $login,
                'password' => $password,
                'needToActivateText' => $needToActivateText,
            ]
        );

        return $this->getMailTransport()->send($message);
    }

    public function sendRegistrationEmailToNewFirmManager(
        string $toEmail,
        string $toName,
        string $login,
        string $password,
        string $firmName,
        string $firmContact
    )
    {
        $message = new Mailgun();
        $message->addTo($toEmail);

        $message->setSubject('Вы запросили регистрацию на сервисе  client.den-nsk.ru');

        $message->addTag('new-client-registration');

        // TODO: Использовать twig
        $body = file_get_contents('/var/www/avto-history/php-app/module/AH/view/emailTemplates/firm-and-manager-registration.html');

        $html = new MimePart($body);
        $html->type = "text/html";
        $body = new MimeMessage();
        $body->addPart($html);

        $message->setBody($body);

        $sender = $this->getSender();
        $message->setFrom($sender['email'], $sender['name']);

        $message->setRecipientVariables(
            $toEmail,
            [
                'siteHost' => 'client.den-nsk.ru',
                'name' => $toName,
                'login' => $login,
                'password' => $password,
                'firmName' => $firmName,
                'firmContact' => $firmContact,
            ]
        );

        return $this->getMailTransport()->send($message);
    }

    public function sendRegistrationEmailToModeratorAboutNewFirm(
        string $toEmail,
        string $firmName,
        string $firmPhone,
        string $firmEmail,
        string $activateToken = null
    )
    {
        $message = new Mailgun();
        $message->addTo($toEmail);

        $message->setSubject("Запрос на новый аккаунт организации " . $firmName . " на сервисе " . $_SERVER['HTTP_HOST']);

        $message->addTag('new-client-registration');

        if ($activateToken) {
            $message->addTag('new-client-registration-with-token');
        }

        // TODO: Использовать twig
        $body = file_get_contents('/var/www/avto-history/php-app/module/AH/view/emailTemplates/firm-and-manager-registration-moderation.html');

        $html = new MimePart($body);
        $html->type = "text/html";
        $body = new MimeMessage();
        $body->addPart($html);

        $message->setBody($body);

        $sender = $this->getSender();
        $message->setFrom($sender['email'], $sender['name']);

        $needToActivateText = '';
        if ($activateToken) {
            $needToActivateText = 'Для активации аккаунта и организации перейдите по ссылке https://%recipient.siteHost%/activate/firmAndFirstManager/' . $activateToken;
        }

        $message->setRecipientVariables(
            $toEmail,
            [
                'siteHost' => $_SERVER['HTTP_HOST'],
                'firmName' => $firmName,
                'firmPhone' => $firmPhone,
                'firmEmail' => $firmEmail,
                'needToActivateText' => $needToActivateText,
            ]
        );

        return $this->getMailTransport()->send($message);
    }

    public function sendToClientAboutNewOrder(
        string $toEmail,
        string $toName,
        int $orderId,
        string $autoLoginLink,
        string $login = null,
        string $password = null
    )
    {
        $message = new Mailgun();
        $message->addTo($toEmail);

        $message->setSubject('Создан заказ SW-' . $orderId);

        $message->addTag('client-notify-order-created');

        if (!empty($login) && !empty($password)) {
            $body = file_get_contents('/var/www/avto-history/php-app/module/AH/view/emailTemplates/client-notify-order-created-with-login-password.html');
        } else {
            // TODO: Использовать twig
            $body = file_get_contents('/var/www/avto-history/php-app/module/AH/view/emailTemplates/client-notify-order-created.html');
        }

        $html = new MimePart($body);
        $html->type = "text/html";
        $body = new MimeMessage();
        $body->addPart($html);

        $message->setBody($body);

        $sender = $this->getSender();
        $message->setFrom($sender['email'], $sender['name']);

        $message->setRecipientVariables(
            $toEmail,
            [
                'orderNumber' => 'SW-' . $orderId,
                'name' => $toName,
                'autoLoginLink' => $autoLoginLink,
                'login' => $login,
                'password' => $password,
            ]
        );

        return $this->getMailTransport()->send($message);
    }

    public function sendToClientAboutOrderWorkCompleted(
        string $toEmail,
        string $toName,
        int $orderId,
        string $autoLoginLink,
        string $login = null,
        string $password = null
    )
    {
        $message = new Mailgun();
        $message->addTo($toEmail);

        $message->setSubject('Работы по заказу SW-' . $orderId . ' выполнены');

        $message->addTag('client-notify-order-work-completed');

        if (!empty($login) && !empty($password)) {
            $body = file_get_contents('/var/www/avto-history/php-app/module/AH/view/emailTemplates/client-notify-order-work-completed-with-login-password.html');
        } else {
            // TODO: Использовать twig
            $body = file_get_contents('/var/www/avto-history/php-app/module/AH/view/emailTemplates/client-notify-order-work-completed.html');
        }

        $html = new MimePart($body);
        $html->type = "text/html";
        $body = new MimeMessage();
        $body->addPart($html);

        $message->setBody($body);

        $sender = $this->getSender();
        $message->setFrom($sender['email'], $sender['name']);

        $message->setRecipientVariables(
            $toEmail,
            [
                'orderNumber' => 'SW-' . $orderId,
                'name' => $toName,
                'autoLoginLink' => $autoLoginLink,
                'login' => $login,
                'password' => $password,
            ]
        );

        return $this->getMailTransport()->send($message);
    }
}