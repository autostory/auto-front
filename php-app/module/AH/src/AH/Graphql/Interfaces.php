<?php

namespace AH\GraphQL;

use AH\GraphQL\TypeInterface\EventTypeInterface;
use AH\GraphQL\TypeInterface\UserTypeInterface;
use Zend\ServiceManager\ServiceManager;

class Interfaces
{
    private static $eventTypeInterface;
    private static $userTypeInterface;

    /**
     * @param ServiceManager $serviceManager
     * @return EventTypeInterface
     */
    public static function eventTypeInterface(ServiceManager $serviceManager)
    {
        return self::$eventTypeInterface ?: (self::$eventTypeInterface = new EventTypeInterface($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return UserTypeInterface
     */
    public static function userTypeInterface(ServiceManager $serviceManager)
    {
        return self::$userTypeInterface ?: (self::$userTypeInterface = new UserTypeInterface($serviceManager));
    }
}
