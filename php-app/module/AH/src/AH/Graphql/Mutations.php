<?php

namespace AH\GraphQL;

use AH\GraphQL\Mutation\Auth\ActivateAccountMutation;
use AH\GraphQL\Mutation\Auth\AutoLoginMutation;
use AH\GraphQL\Mutation\Car\AddCarToClientMutation;
use AH\GraphQL\Mutation\Car\EditCarMutation;
use AH\GraphQL\Mutation\Manager\CreateManagerMutation;
use AH\GraphQL\Mutation\Manager\CreateOrderAsManagerMutation;
use AH\GraphQL\Mutation\Manager\DeleteManagerMutation;
use AH\GraphQL\Mutation\Manager\EditManagerMutation;
use AH\GraphQL\Mutation\Manager\RestoreManagerMutation;
use AH\GraphQL\Mutation\OfficeBox\CreateBoxMutation;
use AH\GraphQL\Mutation\OfficeBox\EditBoxMutation;
use AH\GraphQL\Mutation\Order\EditOrderCarArriveInfoMutation;
use AH\GraphQL\Mutation\Order\EditOrderMainInfoMutation;
use AH\GraphQL\Mutation\Order\EditOrderSchedulesMutation;
use AH\GraphQL\Mutation\Order\EditOrderServiceMaterialsMutation;
use AH\GraphQL\Mutation\Order\EditOrderServiceWorksMutation;
use AH\GraphQL\Mutation\Order\NotifyClientAboutOrderWorkCompletedMutation;
use AH\GraphQL\Mutation\Order\RejectOrderMutation;
use AH\GraphQL\Mutation\Order\SetScheduleServiceWorksStatusMutation;
use AH\GraphQL\Mutation\Order\SetScheduleStatusMutation;
use AH\GraphQL\Mutation\Payment\AddOrderPaymentMutation;
use AH\GraphQL\Mutation\RequestCommunications\AddRequestCommunicationNoAnswerResultMutation;
use AH\GraphQL\Mutation\RequestCommunications\AddRequestCommunicationRecallResultMutation;
use AH\GraphQL\Mutation\Profile\ChangeMyPasswordMutation;
use AH\GraphQL\Mutation\Profile\EditMyProfileMutation;
use AH\GraphQL\Mutation\Registration\ClientRegistrationMutation;
use AH\GraphQL\Mutation\Auth\LoginMutation;
use AH\GraphQL\Mutation\Auth\LogoutMutation;
use AH\GraphQL\Mutation\Auth\TokenRefreshMutation;
use AH\Graphql\Mutation\Manager\CreateClientAsManagerMutation;
use AH\GraphQL\Mutation\Office\CreateOfficeMutation;
use AH\GraphQL\Mutation\Office\EditOfficeMutation;
use AH\GraphQL\Mutation\PasswordRestore\PasswordRestoreUsingSecretKeyMutation;
use AH\GraphQL\Mutation\PasswordRestore\SendPasswordRestoreMessageMutation;
use AH\GraphQL\Mutation\Registration\FirmRegistrationMutation;
use AH\GraphQL\Mutation\RequestCommunications\AddRequestCommunicationRejectResultMutation;
use AH\GraphQL\Mutation\RequestCommunications\AddRequestCommunicationVisitResultMutation;
use Zend\ServiceManager\ServiceManager;

class Mutations
{
    private static $login;
    private static $tokenRefresh;
    private static $logout;
    private static $activateAccountMutation;

    private static $clientRegistration;
    private static $firmRegistation;

    private static $createClientAsManager;

    private static $createOffice;
    private static $editOffice;

    private static $sendPasswordRestoreMessage;
    private static $passwordRestoreUsingSecretKey;

    private static $changeMyPasswordMutation;
    private static $editMyProfileMutation;
    private static $editOfficeBox;
    private static $createOfficeBox;
    private static $addRequestCommunicationRecallResult;
    private static $addRequestCommunicationVisitResult;
    private static $addRequestCommunicationNoAnswerResult;
    private static $addRequestCommunicationRejectResult;
    private static $createOrderAsManagerMutation;
    private static $addCarToClientMutation;
    private static $editOrderMainInfoMutation;
    private static $addOrderPaymentMutation;
    private static $editOrderServiceWorksMutation;
    private static $editOrderServiceMaterialsMutation;
    private static $editOrderSchedulesMutation;
    private static $createManagerMutation;
    private static $editOrderCarArriveInfoMutation;
    private static $setScheduleStatusMutation;
    private static $setScheduleServiceWorksStatusMutation;
    private static $rejectOrderMutation;
    private static $editManagerMutation;
    private static $deleteManagerMutation;
    private static $restoreManagerMutation;
    private static $editCarMutation;
    private static $autoLoginMutation;
    private static $notifyClientAboutOrderWorkCompletedMutation;

    public static function login(ServiceManager $serviceManager)
    {
        if (empty(self::$login)) {
            self::$login = LoginMutation::create($serviceManager);
        }

        return self::$login;
    }

    public static function autoLoginMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$autoLoginMutation)) {
            self::$autoLoginMutation = AutoLoginMutation::create($serviceManager);
        }

        return self::$autoLoginMutation;
    }

    public static function activateAccount(ServiceManager $serviceManager)
    {
        if (empty(self::$activateAccountMutation)) {
            self::$activateAccountMutation = ActivateAccountMutation::create($serviceManager);
        }

        return self::$activateAccountMutation;
    }

    public static function logout(ServiceManager $serviceManager)
    {
        if (empty(self::$logout)) {
            self::$logout = LogoutMutation::create($serviceManager);
        }

        return self::$logout;
    }

    public static function tokenRefresh(ServiceManager $serviceManager)
    {
        if (empty(self::$tokenRefresh)) {
            self::$tokenRefresh = TokenRefreshMutation::create($serviceManager);
        }

        return self::$tokenRefresh;
    }

    public static function clientRegistration(ServiceManager $serviceManager)
    {
        if (empty(self::$clientRegistration)) {
            self::$clientRegistration = ClientRegistrationMutation::create($serviceManager);
        }

        return self::$clientRegistration;
    }

    public static function firmRegistration(ServiceManager $serviceManager)
    {
        if (empty(self::$firmRegistation)) {
            self::$firmRegistation = FirmRegistrationMutation::create($serviceManager);
        }

        return self::$firmRegistation;
    }

    public static function createClientAsManager(ServiceManager $serviceManager)
    {
        if (empty(self::$createClientAsManager)) {
            self::$createClientAsManager = CreateClientAsManagerMutation::create($serviceManager);
        }

        return self::$createClientAsManager;
    }

    public static function sendPasswordRestoreMessage(ServiceManager $serviceManager)
    {
        if (empty(self::$sendPasswordRestoreMessage)) {
            self::$sendPasswordRestoreMessage = SendPasswordRestoreMessageMutation::create($serviceManager);
        }

        return self::$sendPasswordRestoreMessage;
    }

    public static function passwordRestoreUsingSecretKey(ServiceManager $serviceManager)
    {
        if (empty(self::$passwordRestoreUsingSecretKey)) {
            self::$passwordRestoreUsingSecretKey = PasswordRestoreUsingSecretKeyMutation::create($serviceManager);
        }

        return self::$passwordRestoreUsingSecretKey;
    }

    public static function createOffice(ServiceManager $serviceManager)
    {
        if (empty(self::$createOffice)) {
            self::$createOffice = CreateOfficeMutation::create($serviceManager);
        }

        return self::$createOffice;
    }

    public static function editOffice(ServiceManager $serviceManager)
    {
        if (empty(self::$editOffice)) {
            self::$editOffice = EditOfficeMutation::create($serviceManager);
        }

        return self::$editOffice;
    }

    public static function changeMyPassword(ServiceManager $serviceManager)
    {
        if (empty(self::$changeMyPasswordMutation)) {
            self::$changeMyPasswordMutation = ChangeMyPasswordMutation::create($serviceManager);
        }

        return self::$changeMyPasswordMutation;
    }

    public static function editMyProfile(ServiceManager $serviceManager)
    {
        if (empty(self::$editMyProfileMutation)) {
            self::$editMyProfileMutation = EditMyProfileMutation::create($serviceManager);
        }

        return self::$editMyProfileMutation;
    }

    public static function editOfficeBox(ServiceManager $serviceManager)
    {
        if (empty(self::$editOfficeBox)) {
            self::$editOfficeBox = EditBoxMutation::create($serviceManager);
        }

        return self::$editOfficeBox;
    }

    public static function createOfficeBox(ServiceManager $serviceManager)
    {
        if (empty(self::$createOfficeBox)) {
            self::$createOfficeBox = CreateBoxMutation::create($serviceManager);
        }

        return self::$createOfficeBox;
    }

    public static function addRequestCommunicationVisitResult(ServiceManager $serviceManager)
    {
        if (empty(self::$addRequestCommunicationVisitResult)) {
            self::$addRequestCommunicationVisitResult = AddRequestCommunicationVisitResultMutation::create($serviceManager);
        }

        return self::$addRequestCommunicationVisitResult;
    }

    public static function addRequestCommunicationRecallResult(ServiceManager $serviceManager)
    {
        if (empty(self::$addRequestCommunicationRecallResult)) {
            self::$addRequestCommunicationRecallResult = AddRequestCommunicationRecallResultMutation::create($serviceManager);
        }

        return self::$addRequestCommunicationRecallResult;
    }

    public static function addRequestCommunicationNoAnswerResult(ServiceManager $serviceManager)
    {
        if (empty(self::$addRequestCommunicationNoAnswerResult)) {
            self::$addRequestCommunicationNoAnswerResult = AddRequestCommunicationNoAnswerResultMutation::create($serviceManager);
        }

        return self::$addRequestCommunicationNoAnswerResult;
    }

    public static function addRequestCommunicationRejectResult(ServiceManager $serviceManager)
    {
        if (empty(self::$addRequestCommunicationRejectResult)) {
            self::$addRequestCommunicationRejectResult = AddRequestCommunicationRejectResultMutation::create($serviceManager);
        }

        return self::$addRequestCommunicationRejectResult;
    }

    public static function createOrderAsManagerMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$createOrderAsManagerMutation)) {
            self::$createOrderAsManagerMutation = CreateOrderAsManagerMutation::create($serviceManager);
        }

        return self::$createOrderAsManagerMutation;
    }

    /**
     * @param ServiceManager $serviceManager
     * @return mixed
     */
    public static function addCarToClientMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$addCarToClientMutation)) {
            self::$addCarToClientMutation = AddCarToClientMutation::create($serviceManager);
        }

        return self::$addCarToClientMutation;
    }

    public static function editCarMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$editCarMutation)) {
            self::$editCarMutation = EditCarMutation::create($serviceManager);
        }

        return self::$editCarMutation;
    }

    public static function editOrderMainInfoMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$editOrderMainInfoMutation)) {
            self::$editOrderMainInfoMutation = EditOrderMainInfoMutation::create($serviceManager);
        }

        return self::$editOrderMainInfoMutation;
    }

    public static function addOrderPaymentMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$addOrderPaymentMutation)) {
            self::$addOrderPaymentMutation = AddOrderPaymentMutation::create($serviceManager);
        }

        return self::$addOrderPaymentMutation;
    }

    public static function editOrderServiceMaterialsMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$editOrderServiceMaterialsMutation)) {
            self::$editOrderServiceMaterialsMutation = EditOrderServiceMaterialsMutation::create($serviceManager);
        }

        return self::$editOrderServiceMaterialsMutation;
    }

    public static function editOrderServiceWorksMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$editOrderServiceWorksMutation)) {
            self::$editOrderServiceWorksMutation = EditOrderServiceWorksMutation::create($serviceManager);
        }

        return self::$editOrderServiceWorksMutation;
    }

    public static function editOrderSchedulesMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$editOrderSchedulesMutation)) {
            self::$editOrderSchedulesMutation = EditOrderSchedulesMutation::create($serviceManager);
        }

        return self::$editOrderSchedulesMutation;
    }

    public static function createManagerMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$createManagerMutation)) {
            self::$createManagerMutation = CreateManagerMutation::create($serviceManager);
        }

        return self::$createManagerMutation;
    }

    public static function editManagerMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$editManagerMutation)) {
            self::$editManagerMutation = EditManagerMutation::create($serviceManager);
        }

        return self::$editManagerMutation;
    }

    public static function deleteManagerMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$deleteManagerMutation)) {
            self::$deleteManagerMutation = DeleteManagerMutation::create($serviceManager);
        }

        return self::$deleteManagerMutation;
    }

    public static function restoreManagerMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$restoreManagerMutation)) {
            self::$restoreManagerMutation = RestoreManagerMutation::create($serviceManager);
        }

        return self::$restoreManagerMutation;
    }

    public static function editOrderCarArriveInfoMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$editOrderCarArriveInfoMutation)) {
            self::$editOrderCarArriveInfoMutation = EditOrderCarArriveInfoMutation::create($serviceManager);
        }

        return self::$editOrderCarArriveInfoMutation;
    }

    public static function setScheduleStatusMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$setScheduleStatusMutation)) {
            self::$setScheduleStatusMutation = SetScheduleStatusMutation::create($serviceManager);
        }

        return self::$setScheduleStatusMutation;
    }

    public static function setScheduleServiceWorksStatusMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$setScheduleServiceWorksStatusMutation)) {
            self::$setScheduleServiceWorksStatusMutation = SetScheduleServiceWorksStatusMutation::create($serviceManager);
        }

        return self::$setScheduleServiceWorksStatusMutation;
    }

    public static function rejectOrderMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$rejectOrderMutation)) {
            self::$rejectOrderMutation = RejectOrderMutation::create($serviceManager);
        }

        return self::$rejectOrderMutation;
    }

    public static function notifyClientAboutOrderWorkCompletedMutation(ServiceManager $serviceManager)
    {
        if (empty(self::$notifyClientAboutOrderWorkCompletedMutation)) {
            self::$notifyClientAboutOrderWorkCompletedMutation = NotifyClientAboutOrderWorkCompletedMutation::create($serviceManager);
        }

        return self::$notifyClientAboutOrderWorkCompletedMutation;
    }
}
