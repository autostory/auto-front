<?php

namespace AH\GraphQL;

use AH\Graphql\Type\AccessRightsType;
use AH\Graphql\Type\Basic\GlobalServiceTypeType;
use AH\Graphql\Type\Basic\ServiceWorkActionTypeType;
use AH\Graphql\Type\Basic\SparePartNodeType;
use AH\Graphql\Type\Basic\UnitMeasureTypeType;
use AH\Graphql\Type\BasicType;
use AH\Graphql\Type\BoxScheduleType;
use AH\Graphql\Type\CarMileageType;
use AH\Graphql\Type\CarOwnersType;
use AH\Graphql\Type\CatalogCar\CatalogCarCharacteristicType;
use AH\Graphql\Type\CatalogCar\CatalogCarCharacteristicValueType;
use AH\Graphql\Type\CatalogCar\CatalogCarEquipmentType;
use AH\Graphql\Type\CatalogCar\CatalogCarGenerationType;
use AH\Graphql\Type\CatalogCar\CatalogCarMarkType;
use AH\Graphql\Type\CatalogCar\CatalogCarModelType;
use AH\Graphql\Type\CatalogCar\CatalogCarModificationType;
use AH\Graphql\Type\CatalogCar\CatalogCarOptionType;
use AH\Graphql\Type\CatalogCar\CatalogCarOptionValueType;
use AH\Graphql\Type\CatalogCar\CatalogCarSerieType;
use AH\Graphql\Type\CatalogCar\CatalogCarTypeType;
use AH\Graphql\Type\ClientCarsType;
use AH\Graphql\Type\ClientPaymentStatType;
use AH\Graphql\Type\ErrorMessageType;
use AH\Graphql\Type\Event\EventInitiatorType;
use AH\Graphql\Type\Event\GasEventType;
use AH\Graphql\Type\Event\SparePartServiceEventType;
use AH\Graphql\Type\FirmClientsType;
use AH\Graphql\Type\ClientType;
use AH\Graphql\Type\CarType;
use AH\Graphql\Type\FirmOfficesType;
use AH\Graphql\Type\FirmType;
use AH\Graphql\Type\Basic\FuelTypeType;
use AH\Graphql\Type\ManagerOfficesType;
use AH\Graphql\Type\ManagerType;
use AH\Graphql\Type\MutationType;
use AH\Graphql\Type\MutationInputValidationErrorMessagesType;
use AH\Graphql\Type\Oauth2TokenInfoType;
use AH\Graphql\Type\OfficeBoxType;
use AH\Graphql\Type\OfficeType;
use AH\Graphql\Type\OfficeWorkTimeType;
use AH\Graphql\Type\OrderPaymentType;
use AH\Graphql\Type\OrderType;
use AH\Graphql\Type\PointLog\Client\ClientPointWriteOffLog;
use AH\Graphql\Type\QueryType;
use AH\Graphql\Type\Report\DailyFirmOfficeReport;
use AH\Graphql\Type\Report\FirmReportType;
use AH\Graphql\Type\RequestCommunicationType;
use AH\Graphql\Type\RequestType;
use AH\Graphql\Type\ServiceMaterialType;
use AH\Graphql\Type\ServiceWorkType;
use AH\Graphql\Type\ViewerType;
use AH\Graphql\Type\Basic\DateType;
use GraphQL\Type\Definition\UnionType;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class Types
{
    /** @var NodeDefinition */
    private static $nodeDefinition;

    private static $oauth2TokenInfo;
    private static $client;
    private static $car;
    private static $firm;
    private static $office;
    private static $accessRights;
    private static $viewer;
    private static $basic;
    private static $manager;
    private static $firmClients;
    private static $firmOffices;
    private static $managerOffices;
    private static $clientCars;
    private static $carOwners;
    private static $catalogCarType;
    private static $catalogCarMark;
    private static $catalogCarModel;
    private static $catalogCarGeneration;
    private static $catalogCarSerie;
    private static $catalogCarModification;
    private static $catalogCarEquipment;
    private static $fuelTypeType;
    private static $spareNodePart;
    private static $dateType;
    private static $globalServiceTypeType;
    private static $serviceWorkActionTypeType;
    private static $query;
    private static $mutation;
    private static $gasEventType;
    private static $sparePartServiceEventType;
    private static $eventInitiator;
    private static $serviceWorkType;
    private static $clientConnection;
    private static $carConnection;
    private static $officeConnection;
    private static $errorMessage;
    private static $mutationValidationErrorMessages;
    private static $managerConnection;
    private static $officeBox;
    private static $request;
    private static $requestConnection;
    private static $requestCommunicationType;
    private static $order;
    private static $orderConnection;
    private static $orderPaymentType;
    private static $boxScheduleType;
    private static $orderPaymentsConnection;
    private static $unitMeasureTypeType;
    private static $serviceMaterialType;
    private static $catalogCarOptionType;
    private static $catalogCarOptionValueType;
    private static $carMileageType;
    private static $catalogCarCharacteristicValueType;
    private static $catalogCarCharacteristicType;
    private static $clientPaymentStatType;
    private static $officeWorkTimeType;
    private static $firmReportType;
    private static $dailyFirmReportType;
    private static $clientPointWriteOffLog;
    private static $clientPointLogUnionType;

    /**
     * @param ServiceManager $serviceManager
     * @return QueryType
     */
    public static function query(ServiceManager $serviceManager)
    {
        return self::$query ?: (self::$query = new QueryType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return RequestType
     */
    public static function requestType(ServiceManager $serviceManager)
    {
        return self::$request ?: (self::$request = new RequestType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return OrderType
     */
    public static function orderType(ServiceManager $serviceManager)
    {
        return self::$order ?: (self::$order = new OrderType($serviceManager));
    }

    public static function basic(ServiceManager $serviceManager)
    {
        return self::$basic ?: (self::$basic = new BasicType($serviceManager));
    }

    public static function catalogCarType(ServiceManager $serviceManager)
    {
        return self::$catalogCarType ?: (self::$catalogCarType = new CatalogCarTypeType($serviceManager));
    }

    public static function catalogCarMark(ServiceManager $serviceManager)
    {
        return self::$catalogCarMark ?: (self::$catalogCarMark = new CatalogCarMarkType($serviceManager));
    }

    public static function catalogCarModel(ServiceManager $serviceManager)
    {
        return self::$catalogCarModel ?: (self::$catalogCarModel = new CatalogCarModelType($serviceManager));
    }

    public static function catalogCarGeneration(ServiceManager $serviceManager)
    {
        return self::$catalogCarGeneration ?: (self::$catalogCarGeneration = new CatalogCarGenerationType($serviceManager));
    }

    public static function catalogCarSerie(ServiceManager $serviceManager)
    {
        return self::$catalogCarSerie ?: (self::$catalogCarSerie = new CatalogCarSerieType($serviceManager));
    }

    public static function catalogCarModificationType(ServiceManager $serviceManager)
    {
        return self::$catalogCarModification ?: (self::$catalogCarModification = new CatalogCarModificationType($serviceManager));
    }

    public static function catalogCarEquipmentType(ServiceManager $serviceManager)
    {
        return self::$catalogCarEquipment ?: (self::$catalogCarEquipment = new CatalogCarEquipmentType($serviceManager));
    }

    public static function catalogCarOptionType(ServiceManager $serviceManager)
    {
        return self::$catalogCarOptionType ?: (self::$catalogCarOptionType = new CatalogCarOptionType($serviceManager));
    }

    public static function catalogCarOptionValueType(ServiceManager $serviceManager)
    {
        return self::$catalogCarOptionValueType ?: (self::$catalogCarOptionValueType = new CatalogCarOptionValueType($serviceManager));
    }

    public static function errorMessage()
    {
        return self::$errorMessage ?: (self::$errorMessage = new ErrorMessageType());
    }

    public static function mutationValidationErrorMessages()
    {
        return self::$mutationValidationErrorMessages ?: (self::$mutationValidationErrorMessages = new MutationInputValidationErrorMessagesType());
    }

    public static function oauth2TokenInfo()
    {
        return self::$oauth2TokenInfo ?: (self::$oauth2TokenInfo = new Oauth2TokenInfoType());
    }

    /**
     * @param ServiceManager $serviceManager
     * @return MutationType
     */
    public static function mutation(ServiceManager $serviceManager)
    {
        return self::$mutation ?: (self::$mutation = new MutationType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return ViewerType
     */
    public static function viewer(ServiceManager $serviceManager)
    {
        return self::$viewer ?: (self::$viewer = new ViewerType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return NodeDefinition|array
     */
    private static function nodeDefinition(ServiceManager $serviceManager)
    {
        if (!empty(self::$nodeDefinition)) {
            return self::$nodeDefinition;
        }

        $nodeDefinition = new NodeDefinition();

        self::$nodeDefinition = $nodeDefinition::getNodeDefinition($serviceManager);

        return self::$nodeDefinition;
    }

    /**
     * @param ServiceManager $serviceManager
     * @return mixed
     */
    public static function nodeField(ServiceManager $serviceManager)
    {
        $nodeDefinition = self::nodeDefinition($serviceManager);

        return $nodeDefinition['nodeField'];
    }

    /**
     * @param ServiceManager $serviceManager
     * @return mixed
     */
    public static function nodeInterface(ServiceManager $serviceManager)
    {
        $nodeDefinition = self::nodeDefinition($serviceManager);

        return $nodeDefinition['nodeInterface'];
    }

    /**
     * @param ServiceManager $serviceManager
     * @return EventInitiatorType
     */
    public static function eventInitiatorType(ServiceManager $serviceManager)
    {
        return self::$eventInitiator ?: (self::$eventInitiator = new EventInitiatorType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return ServiceWorkType
     */
    public static function serviceWorkType(ServiceManager $serviceManager)
    {
        return self::$serviceWorkType ?: (self::$serviceWorkType = new ServiceWorkType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return ClientType
     */
    public static function clientType(ServiceManager $serviceManager)
    {
        return self::$client ?: (self::$client = new ClientType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return CarMileageType
     */
    public static function carMileageType(ServiceManager $serviceManager)
    {
        return self::$carMileageType ?: (self::$carMileageType = new CarMileageType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return CatalogCarCharacteristicValueType
     */
    public static function catalogCarCharacteristicValueType(ServiceManager $serviceManager)
    {
        return self::$catalogCarCharacteristicValueType ?: (self::$catalogCarCharacteristicValueType = new CatalogCarCharacteristicValueType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return CatalogCarCharacteristicType
     */
    public static function catalogCarCharacteristicType(ServiceManager $serviceManager)
    {
        return self::$catalogCarCharacteristicType ?: (self::$catalogCarCharacteristicType = new CatalogCarCharacteristicType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return ManagerType
     */
    public static function managerType(ServiceManager $serviceManager)
    {
        return self::$manager ?: (self::$manager = new ManagerType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return CarType
     */
    public static function carType(ServiceManager $serviceManager)
    {
        return self::$car ?: (self::$car = new CarType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return OfficeType
     */
    public static function officeType(ServiceManager $serviceManager)
    {
        return self::$office ?: (self::$office = new OfficeType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return OfficeBoxType
     */
    public static function officeBoxType(ServiceManager $serviceManager)
    {
        return self::$officeBox ?: (self::$officeBox = new OfficeBoxType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return BoxScheduleType
     */
    public static function boxScheduleType(ServiceManager $serviceManager)
    {
        return self::$boxScheduleType ?: (self::$boxScheduleType = new BoxScheduleType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return FirmType
     */
    public static function firmType(ServiceManager $serviceManager)
    {
        return self::$firm ?: (self::$firm = new FirmType($serviceManager));
    }

    /**
     * @return AccessRightsType
     */
    public static function accessRightsType()
    {
        return self::$accessRights ?: (self::$accessRights = new AccessRightsType());
    }

    /**
     * @param ServiceManager $serviceManager
     * @return FirmClientsType
     */
    public static function firmClients(ServiceManager $serviceManager)
    {
        return self::$firmClients ?: (self::$firmClients = new FirmClientsType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return FirmOfficesType
     */
    public static function firmOffices(ServiceManager $serviceManager)
    {
        return self::$firmOffices ?: (self::$firmOffices = new FirmOfficesType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return RequestCommunicationType
     */
    public static function requestCommunicationType(ServiceManager $serviceManager)
    {
        return self::$requestCommunicationType ?: (self::$requestCommunicationType = new RequestCommunicationType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return ManagerOfficesType
     */
    public static function managerOffices(ServiceManager $serviceManager)
    {
        return self::$managerOffices ?: (self::$managerOffices = new ManagerOfficesType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return OfficeWorkTimeType
     */
    public static function officeWorkTimeType(ServiceManager $serviceManager)
    {
        return self::$officeWorkTimeType ?: (self::$officeWorkTimeType = new OfficeWorkTimeType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return ClientCarsType
     */
    public static function clientCars(ServiceManager $serviceManager)
    {
        return self::$clientCars ?: (self::$clientCars = new ClientCarsType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return CarOwnersType
     */
    public static function carOwners(ServiceManager $serviceManager)
    {
        return self::$carOwners ?: (self::$carOwners = new CarOwnersType($serviceManager));
    }

    /**
     * @return FuelTypeType
     */
    public static function fuelTypeType()
    {
        return self::$fuelTypeType ?: (self::$fuelTypeType = new FuelTypeType());
    }

    /**
     * @return DateType
     */
    public static function dateType()
    {
        return self::$dateType ?: (self::$dateType = new DateType());
    }

    /**
     * @return GlobalServiceTypeType
     */
    public static function globalServiceTypeType()
    {
        return self::$globalServiceTypeType ?: (self::$globalServiceTypeType = new GlobalServiceTypeType());
    }

    /**
     * @return ServiceWorkActionTypeType
     */
    public static function serviceWorkActionTypeType()
    {
        return self::$serviceWorkActionTypeType ?: (self::$serviceWorkActionTypeType = new ServiceWorkActionTypeType());
    }

    /**
     * @param ServiceManager $serviceManager
     * @return GasEventType
     */
    public static function gasEventType(ServiceManager $serviceManager)
    {
        return self::$gasEventType ?: (self::$gasEventType = new GasEventType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return OrderPaymentType
     */
    public static function orderPaymentType(ServiceManager $serviceManager)
    {
        return self::$orderPaymentType ?: (self::$orderPaymentType = new OrderPaymentType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return SparePartServiceEventType
     */
    public static function sparePartServiceEventType(ServiceManager $serviceManager)
    {
        return self::$sparePartServiceEventType ?: (self::$sparePartServiceEventType = new SparePartServiceEventType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return ClientPaymentStatType
     */
    public static function clientPaymentStatType(ServiceManager $serviceManager)
    {
        return self::$clientPaymentStatType ?: (self::$clientPaymentStatType = new ClientPaymentStatType($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return ServiceMaterialType
     */
    public static function serviceMaterialType(ServiceManager $serviceManager)
    {
        return self::$serviceMaterialType ?: (self::$serviceMaterialType = new ServiceMaterialType($serviceManager));
    }

    /**
     * @return UnitMeasureTypeType
     */
    public static function unitMeasureTypeType()
    {
        return self::$unitMeasureTypeType ?: (self::$unitMeasureTypeType = new UnitMeasureTypeType());
    }

    /**
     * @return SparePartNodeType
     */
    public static function sparePartNodeType()
    {
        return self::$spareNodePart ?: (self::$spareNodePart = new SparePartNodeType());
    }

    /**
     * @param ServiceManager $serviceManager
     * @return FirmReportType
     */
    public static function firmReportType(ServiceManager $serviceManager)
    {
        return self::$firmReportType ?: (self::$firmReportType = new FirmReportType($serviceManager));
    }

    public static function clientPointWriteOffLog(ServiceManager $serviceManager)
    {
        return self::$clientPointWriteOffLog ?: (self::$clientPointWriteOffLog = new ClientPointWriteOffLog($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return DailyFirmOfficeReport
     */
    public static function dailyFirmOfficeReportType(ServiceManager $serviceManager)
    {
        return self::$dailyFirmReportType ?: (self::$dailyFirmReportType = new DailyFirmOfficeReport($serviceManager));
    }

    /**
     * @param ServiceManager $serviceManager
     * @return array
     */
    public static function clientConnection(ServiceManager $serviceManager)
    {
        return self::$clientConnection ?: (self::$clientConnection = Relay::connectionDefinitions([
            'nodeType' => self::clientType($serviceManager),
        ])['connectionType']);
    }

    /**
     * @param ServiceManager $serviceManager
     * @return array
     */
    public static function managerConnection(ServiceManager $serviceManager)
    {
        return self::$managerConnection ?: (self::$managerConnection = Relay::connectionDefinitions([
            'nodeType' => self::managerType($serviceManager),
        ])['connectionType']);
    }

    /**
     * @param ServiceManager $serviceManager
     * @return array
     */
    public static function carConnection(ServiceManager $serviceManager)
    {
        return self::$carConnection ?: (self::$carConnection = Relay::connectionDefinitions([
            'nodeType' => self::carType($serviceManager),
        ])['connectionType']);
    }

    /**
     * @param ServiceManager $serviceManager
     * @return array
     */
    public static function officeConnection(ServiceManager $serviceManager)
    {
        return self::$officeConnection ?: (self::$officeConnection = Relay::connectionDefinitions([
            'nodeType' => self::officeType($serviceManager),
        ])['connectionType']);
    }

    /**
     * @param ServiceManager $serviceManager
     * @return array
     */
    public static function requestConnection(ServiceManager $serviceManager)
    {
        return self::$requestConnection ?: (self::$requestConnection = Relay::connectionDefinitions([
            'nodeType' => self::requestType($serviceManager),
        ])['connectionType']);
    }

    /**
     * @param ServiceManager $serviceManager
     * @return array
     */
    public static function orderConnection(ServiceManager $serviceManager)
    {
        return self::$orderConnection ?: (self::$orderConnection = Relay::connectionDefinitions([
            'nodeType' => self::orderType($serviceManager),
        ])['connectionType']);
    }

    /**
     * @param ServiceManager $serviceManager
     * @return array
     */
    public static function orderPaymentsConnection(ServiceManager $serviceManager)
    {
        return self::$orderPaymentsConnection ?: (self::$orderPaymentsConnection = Relay::connectionDefinitions([
            'nodeType' => self::orderPaymentType($serviceManager),
        ])['connectionType']);
    }
}
