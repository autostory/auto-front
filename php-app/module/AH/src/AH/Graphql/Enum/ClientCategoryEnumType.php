<?php

namespace AH\Graphql\Enum;

use GraphQL\Type\Definition\EnumType;

class ClientCategoryEnumType extends EnumType
{
    const TYPE_NAME = 'ClientCategoryEnum';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Доступные категории клиента',
            'values' => [
                'NEW' => [
                    'value' => 0,
                    'description' => 'Новый'
                ],
                'PERSISTENT' => [
                    'value' => 1,
                    'description' => 'Постоянный'
                ],
                'VIP' => [
                    'value' => 2,
                    'description' => 'VIP'
                ],
            ]
        ];

        parent::__construct($config);
    }
}

