<?php

namespace AH\Graphql\Enum;

use GraphQL\Type\Definition\EnumType;

class PaymentTypeEnumType extends EnumType
{
    const TYPE_NAME = 'PaymentTypeEnum';

    const CASH = 'CASH';
    const CASH_VALUE = 1;
    const CARD = 'CARD';
    const CARD_VALUE = 2;
    const POINT = 'POINT';
    const POINT_VALUE = 3;

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Виды оплаты',
            'values' => [
                self::CASH => [
                    'value' => self::CASH_VALUE,
                    'description' => 'Наличные.'
                ],
                self::CARD => [
                    'value' => self::CARD_VALUE,
                    'description' => 'Безнал.'
                ],
                self::POINT => [
                    'value' => self::POINT_VALUE,
                    'description' => 'Баллы.'
                ],
            ]
        ];

        parent::__construct($config);
    }
}

