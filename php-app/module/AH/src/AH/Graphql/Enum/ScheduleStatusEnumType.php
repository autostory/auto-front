<?php

namespace AH\Graphql\Enum;

use GraphQL\Type\Definition\EnumType;

class ScheduleStatusEnumType extends EnumType
{
    const TYPE_NAME = 'ScheduleStatusEnumType';

    const SCHEDULE_WAIT_WORK_START = 'SCHEDULE_WAIT_WORK_START';
    const SCHEDULE_WAIT_WORK_START_VALUE = 10;
    const SCHEDULE_IN_WORK = 'SCHEDULE_IN_WORK';
    const SCHEDULE_IN_WORK_VALUE = 20;
    const SCHEDULE_DONE = 'SCHEDULE_DONE';
    const SCHEDULE_DONE_VALUE = 30;

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Доступные статусы расписания работ заказа',
            'values' => [
                self::SCHEDULE_WAIT_WORK_START => [
                    'value' => self::SCHEDULE_WAIT_WORK_START_VALUE,
                    'description' => 'Ждет начала работ.'
                ],
                self::SCHEDULE_IN_WORK => [
                    'value' => self::SCHEDULE_IN_WORK_VALUE,
                    'description' => 'В работе.'
                ],
                self::SCHEDULE_DONE => [
                    'value' => self::SCHEDULE_DONE_VALUE,
                    'description' => 'Выполнено.'
                ],
            ]
        ];

        parent::__construct($config);
    }
}

