<?php

namespace AH\Graphql\Enum;

use GraphQL\Type\Definition\EnumType;

class ResultRequestCommunicationEnumType extends EnumType
{
    const TYPE_NAME = 'ResultRequestCommunicationEnum';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Доступные статусы результата коммуникации по заявке',
            'values' => [
                'VISIT' => [
                    'value' => 1,
                    'description' => 'Договорились о визите.'
                ],
                'RECALL' => [
                    'value' => 2,
                    'description' => 'Перезвонить.'
                ],
                'NO_ANSWER' => [
                    'value' => 3,
                    'description' => 'Недозвонился.'
                ],
                'REJECT' => [
                    'value' => 4,
                    'description' => 'Отказ.'
                ],
            ]
        ];

        parent::__construct($config);
    }
}

