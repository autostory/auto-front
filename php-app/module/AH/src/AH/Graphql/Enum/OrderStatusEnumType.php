<?php

namespace AH\Graphql\Enum;

use GraphQL\Type\Definition\EnumType;

class OrderStatusEnumType extends EnumType
{
    const TYPE_NAME = 'OrderStatusEnum';

    const WAIT_CAR_ARRIVE = 'WAIT_CAR_ARRIVE';
    const WAIT_CAR_ARRIVE_VALUE = 10;
    const WAIT_WORK_ADD = 'WAIT_WORK_ADD';
    const WAIT_WORK_ADD_VALUE = 12;
    const WAIT_PLANING = 'WAIT_PLANING';
    const WAIT_PLANING_VALUE = 14;
    const WAIT_WORK_START = 'WAIT_WORK_START';
    const WAIT_WORK_START_VALUE = 11;
    const IN_WORK = 'IN_WORK';
    const IN_WORK_VALUE = 20;
    const WAIT_CAR_RETURN = 'WAIT_CAR_RETURN';
    const WAIT_CAR_RETURN_VALUE = 21;
    const WAIT_PAYMENT = 'WAIT_PAYMENT';
    const WAIT_PAYMENT_VALUE = 22;
    const DONE = 'DONE';
    const DONE_VALUE = 30;
    const REJECT = 'REJECT';
    const REJECT_VALUE = 50;

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Доступные статусы заказа',
            'values' => [
                self::WAIT_WORK_ADD => [
                    'value' => self::WAIT_WORK_ADD_VALUE,
                    'description' => 'Ожидает добавления работ.'
                ],
                self::WAIT_PLANING => [
                    'value' => self::WAIT_PLANING_VALUE,
                    'description' => 'Ожидает планирования работ.'
                ],
                self::WAIT_CAR_ARRIVE => [
                    'value' => self::WAIT_CAR_ARRIVE_VALUE,
                    'description' => 'Ожидает прибытия авто.'
                ],
                self::WAIT_WORK_START => [
                    'value' => self::WAIT_WORK_START_VALUE,
                    'description' => 'Ожидает взятия в работу.'
                ],
                self::IN_WORK => [
                    'value' => self::IN_WORK_VALUE,
                    'description' => 'В работе.'
                ],
                self::WAIT_CAR_RETURN => [
                    'value' => self::WAIT_CAR_RETURN_VALUE,
                    'description' => 'Ожидает возврат авто.'
                ],
                self::DONE => [
                    'value' => self::DONE_VALUE,
                    'description' => 'Выполнено.'
                ],
                self::WAIT_PAYMENT => [
                    'value' => self::WAIT_PAYMENT_VALUE,
                    'description' => 'Ожидает оплаты.'
                ],
                self::REJECT => [
                    'value' => self::REJECT_VALUE,
                    'description' => 'Отказ.'
                ],
            ]
        ];

        parent::__construct($config);
    }
}

