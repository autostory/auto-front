<?php

namespace AH\Graphql\Enum;

use GraphQL\Type\Definition\EnumType;

class ServiceWorkStatusEnumType extends EnumType
{
    const TYPE_NAME = 'ServiceWorkStatusEnumType';

    const SERVICE_WORK_WAIT_WORK_START = 'SERVICE_WORK_WAIT_WORK_START';
    const SERVICE_WORK_WAIT_WORK_START_VALUE = 10;
    const SERVICE_WORK_IN_WORK = 'SERVICE_WORK_IN_WORK';
    const SERVICE_WORK_IN_WORK_VALUE = 20;
    const SERVICE_WORK_DONE = 'SERVICE_WORK_DONE';
    const SERVICE_WORK_DONE_VALUE = 30;

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Доступные статусы работы по заказу',
            'values' => [
                self::SERVICE_WORK_WAIT_WORK_START => [
                    'value' => self::SERVICE_WORK_WAIT_WORK_START_VALUE,
                    'description' => 'Ждет начала работ.'
                ],
                self::SERVICE_WORK_IN_WORK => [
                    'value' => self::SERVICE_WORK_IN_WORK_VALUE,
                    'description' => 'В работе.'
                ],
                self::SERVICE_WORK_DONE => [
                    'value' => self::SERVICE_WORK_DONE_VALUE,
                    'description' => 'Выполнено.'
                ],
            ]
        ];

        parent::__construct($config);
    }
}

