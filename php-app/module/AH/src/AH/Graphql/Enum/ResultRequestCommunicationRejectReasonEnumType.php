<?php

namespace AH\Graphql\Enum;

use GraphQL\Type\Definition\EnumType;

class ResultRequestCommunicationRejectReasonEnumType extends EnumType
{
    const TYPE_NAME = 'ResultRequestCommunicationRejectReasonEnum';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Доступные причины отказов клиента от заявки',
            'values' => [
                'COST' => [
                    'value' => 1,
                    'description' => 'Не устроила стоимость.'
                ],
                'TIME' => [
                    'value' => 2,
                    'description' => 'Не устроило время.'
                ],
                'NO_SERVICE' => [
                    'value' => 3,
                    'description' => 'Не оказываем такую услугу.'
                ],
                'BAD_CAR' => [
                    'value' => 4,
                    'description' => 'Не обслуживаем такой автомобиль.'
                ],
                'OTHER' => [
                    'value' => 5,
                    'description' => 'Другое.'
                ],
            ]
        ];

        parent::__construct($config);
    }
}

