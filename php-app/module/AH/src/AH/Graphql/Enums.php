<?php

namespace AH\GraphQL;

use AH\Graphql\Enum\ClientCategoryEnumType;
use AH\Graphql\Enum\OrderStatusEnumType;
use AH\Graphql\Enum\PaymentTypeEnumType;
use AH\Graphql\Enum\ResultRequestCommunicationEnumType;
use AH\Graphql\Enum\ResultRequestCommunicationRejectReasonEnumType;
use AH\Graphql\Enum\ScheduleStatusEnumType;
use AH\Graphql\Enum\ServiceWorkStatusEnumType;

class Enums
{
    private static $resultRequestCommunicationEnumType;
    private static $resultRequestCommunicationRejectCauseEnumType;
    private static $orderStatusEnumType;
    private static $paymentTypeEnumType;
    private static $clientCategoryEnumType;
    private static $scheduleStatusEnumType;
    private static $serviceWorkStatusEnumType;

    /**
     * @return ResultRequestCommunicationEnumType
     */
    public static function resultRequestCommunicationEnumType()
    {
        if (self::$resultRequestCommunicationEnumType) {
            return self::$resultRequestCommunicationEnumType;
        }

        return (self::$resultRequestCommunicationEnumType = new ResultRequestCommunicationEnumType());
    }

    /**
     * @return ResultRequestCommunicationRejectReasonEnumType
     */
    public static function resultRequestCommunicationRejectReasonEnumType()
    {
        if (self::$resultRequestCommunicationRejectCauseEnumType) {
            return self::$resultRequestCommunicationRejectCauseEnumType;
        }

        return (self::$resultRequestCommunicationRejectCauseEnumType = new ResultRequestCommunicationRejectReasonEnumType());
    }

    /**
     * @return OrderStatusEnumType
     */
    public static function orderStatusEnumType()
    {
        if (self::$orderStatusEnumType) {
            return self::$orderStatusEnumType;
        }

        return (self::$orderStatusEnumType = new OrderStatusEnumType());
    }

    /**
     * @return PaymentTypeEnumType
     */
    public static function paymentTypeEnumType()
    {
        if (self::$paymentTypeEnumType) {
            return self::$paymentTypeEnumType;
        }

        return (self::$paymentTypeEnumType = new PaymentTypeEnumType());
    }

    /**
     * @return ClientCategoryEnumType
     */
    public static function clientCategoryEnumType()
    {
        if (self::$clientCategoryEnumType) {
            return self::$clientCategoryEnumType;
        }

        return (self::$clientCategoryEnumType = new ClientCategoryEnumType());
    }

    /**
     * @return ScheduleStatusEnumType
     */
    public static function scheduleStatusEnumType()
    {
        if (self::$scheduleStatusEnumType) {
            return self::$scheduleStatusEnumType;
        }

        return (self::$scheduleStatusEnumType = new ScheduleStatusEnumType());
    }

    /**
     * @return ServiceWorkStatusEnumType
     */
    public static function serviceWorkStatusEnumType()
    {
        if (self::$serviceWorkStatusEnumType) {
            return self::$serviceWorkStatusEnumType;
        }

        return (self::$serviceWorkStatusEnumType = new ServiceWorkStatusEnumType());
    }
}
