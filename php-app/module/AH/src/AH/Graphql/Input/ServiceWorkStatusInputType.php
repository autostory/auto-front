<?php

namespace AH\GraphQL\Input;

use AH\GraphQL\Enums;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\InputObjectType;

class ServiceWorkStatusInputType extends InputObjectType
{
    const TYPE_NAME = 'ServiceWorkStatusInputType';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'fields' => [
                'id' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'status' => [
                    'type' => Type::nonNull(Enums::serviceWorkStatusEnumType()),
                ],
            ]
        ];

        parent::__construct($config);
    }
}
