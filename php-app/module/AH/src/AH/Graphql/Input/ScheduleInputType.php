<?php

namespace AH\GraphQL\Input;

use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\InputObjectType;

class ScheduleInputType extends InputObjectType
{
    const TYPE_NAME = 'ScheduleInputType';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'fields' => [
                'id' => [
                    'type' => Type::id(),
                ],
                'boxId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'start' => [
                    'type' => Type::nonNull(Type::int()),
                ],
                'finish' => [
                    'type' => Type::nonNull(Type::int()),
                ],
                'serviceWorks' => [
                    'type' => Type::listOf(Type::nonNull(Type::id())),
                ],
            ]
        ];

        parent::__construct($config);
    }
}
