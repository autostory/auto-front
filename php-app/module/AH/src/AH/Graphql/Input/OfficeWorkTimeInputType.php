<?php

namespace AH\GraphQL\Input;

use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\InputObjectType;

class OfficeWorkTimeInputType extends InputObjectType
{
    const TYPE_NAME = 'OfficeWorkTimeInputType';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'fields' => [
                'officeId' => [
                  'type' => Type::nonNull(Type::id()),
                ],
                'start' => [
                    'type' => Type::string(),
                ],
                'end' => [
                    'type' => Type::string(),
                ],
                'dayOfWeek' => [
                    'type' => Type::nonNull(Type::int()),
                ],
                'holiday' => [
                    'type' => Type::nonNull(Type::boolean()),
                ],
                'roundTheClock' => [
                    'type' => Type::nonNull(Type::boolean()),
                ],
            ]
        ];

        parent::__construct($config);
    }
}
