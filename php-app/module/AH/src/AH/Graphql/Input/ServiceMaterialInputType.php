<?php

namespace AH\GraphQL\Input;

use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\InputObjectType;

class ServiceMaterialInputType extends InputObjectType
{
    const TYPE_NAME = 'ServiceMaterialInputType';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'fields' => [
                'id' => [
                    'type' => Type::id(),
                ],
                'unitMeasureTypeId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'costPerUnit' => [
                    'type' => Type::nonNull(Type::float()),
                ],
                'amount' => [
                    'type' => Type::nonNull(Type::float()),
                ],
                'discountPercent' => [
                    'type' => Type::nonNull(Type::float()),
                ],
                'name' => [
                    'type' => Type::string(),
                ],
            ]
        ];

        parent::__construct($config);
    }
}
