<?php

namespace AH\GraphQL\Input;

use AH\GraphQL\Enums;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\InputObjectType;

class ServiceWorkInputType extends InputObjectType
{
    const TYPE_NAME = 'ServiceWorkInputType';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'fields' => [
                'id' => [
                    'type' => Type::id(),
                ],
                'tempId' => [
                    'type' => Type::string(),
                ],
                'masterId' => [
                    'type' => Type::id(),
                ],
                'serviceWorkActionTypeId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'sparePartNodeId' => [
                    'type' => Type::id(),
                ],
                'costPerUnit' => [
                    'type' => Type::nonNull(Type::float()),
                ],
                'amount' => [
                    'type' => Type::nonNull(Type::float()),
                ],
                'laborHour' => [
                    'type' => Type::float(),
                ],
                'discountPercent' => [
                    'type' => Type::float(),
                ],
                'name' => [
                    'type' => Type::string(),
                ],
                'status' => [
                    'type' => Enums::serviceWorkStatusEnumType(),
                ],
            ]
        ];

        parent::__construct($config);
    }
}
