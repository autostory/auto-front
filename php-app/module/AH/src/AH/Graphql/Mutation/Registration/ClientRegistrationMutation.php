<?php

namespace AH\GraphQL\Mutation\Registration;

use AH\Core\Mail\MailService;
use AH\Entity\AccountActivate;
use AH\Entity\Client;
use AH\Entity\Role;
use AH\Entity\UserCommunication;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Crypt\Password\Bcrypt;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\ServiceManager;

class ClientRegistrationMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        /** @var InputFilterInterface $clientAuthInputFilter */
        $clientAuthInputFilter = $serviceManager->get('ClientAuthInputFilter');

        /** @var InputFilterInterface $mobilePhoneInputFilter */
        $mobilePhoneInputFilter = $serviceManager->get('ClientRegistrationPhoneInputFilter');

        /** @var \Doctrine\ORM\EntityManager $entityManager */
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

        /** @var MailService $mailService */
        $mailService = $serviceManager->get(MailService::class);

        return Relay::mutationWithClientMutationId([
            'name' => 'clientRegistrationMutation',
            'inputFields' => [
                'email' => [
                    'type' => Type::nonNull(Type::string()),
                ],
                'mobilePhone' => [
                    'type' => Type::nonNull(Type::string()),
                ],
                'newPassword' => [
                    'type' => Type::nonNull(Type::string()),
                ],
                'newPasswordConfirm' => [
                    'type' => Type::nonNull(Type::string()),
                ],
            ],
            'outputFields' => [
                'errors' => [
                    'type' => Types::mutationValidationErrorMessages(),
                    'resolve' => function ($payload) {
                        return [
                            'messages' => isset($payload['errors']) ? $payload['errors'] : []
                        ];
                    },
                ],
                'clientCreated' => [
                    'type' => Type::boolean(),
                    'resolve' => function ($payload) {
                        return isset($payload['clientCreated']) && $payload['clientCreated'];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($clientAuthInputFilter, $mobilePhoneInputFilter, $entityManager, $mailService) {
                $clientAuthInputFilter->setData($input);
                $mobilePhoneInputFilter->setData($input);

                $errors = [];
                if (!$clientAuthInputFilter->isValid()) {
                    if (!empty($clientAuthInputFilter->getMessages())) {
                        foreach ($clientAuthInputFilter->getMessages() as $fieldName => $errorMessages) {
                            foreach ($errorMessages as $errorType => $errorMessage) {
                                $errors[] = [
                                    'fieldName' => $fieldName,
                                    'type' => $errorType,
                                    'text' => $errorMessage,
                                ];
                            }
                        }
                    }
                }

                if (!$mobilePhoneInputFilter->isValid()) {
                    if (!empty($mobilePhoneInputFilter->getMessages())) {
                        foreach ($mobilePhoneInputFilter->getMessages() as $fieldName => $errorMessages) {
                            foreach ($errorMessages as $errorType => $errorMessage) {
                                $errors[] = [
                                    'fieldName' => $fieldName,
                                    'type' => $errorType,
                                    'text' => $errorMessage,
                                ];
                            }
                        }
                    }
                }

                if (!empty($errors)) {
                    return ['errors' => $errors];
                }

                $resultValues = array_merge(
                    $clientAuthInputFilter->getValues(),
                    $mobilePhoneInputFilter->getValues()
                );

                $newClient = new Client();

                $bcrypt = new Bcrypt();
                $bcrypt->setCost(10);
                $passwordHash = $bcrypt->create($resultValues['newPassword']);
                $newClient->setRegistrationEmail($resultValues['email']);
                $newClient->setRegistrationPhone($resultValues['mobilePhone']);
                $newClient->setPassword($passwordHash);
                $newClient->setActive(false);

                $userCommunication = new UserCommunication();
                $userCommunication->setActive(false);
                $userCommunication->setUser($newClient);
                $userCommunication->setPreferred(true);
                $userCommunication->setType('email');
                $userCommunication->setValue($resultValues['email']);

                $userCommunication = new UserCommunication();
                $userCommunication->setActive(false);
                $userCommunication->setUser($newClient);
                $userCommunication->setPreferred(false);
                $userCommunication->setType('mobile');
                $userCommunication->setValue($resultValues['mobilePhone']);

                $rRoles = $entityManager->getRepository(Role::class);
                /** @var Role $clientRole */
                $clientRole = $rRoles->findOneBy([
                    'roleId' => 'client',
                ]);

                if (empty($clientRole)) {
                    throw new \Exception('Can`t find role client.');
                }

                $newClient->addRole($clientRole);

                $activationTokenValue = implode('', [
                    bin2hex(random_bytes(4)),
                    bin2hex(random_bytes(2)),
                    bin2hex(chr((ord(random_bytes(1)) & 0x0F) | 0x40)) . bin2hex(random_bytes(1)),
                    bin2hex(chr((ord(random_bytes(1)) & 0x3F) | 0x80)) . bin2hex(random_bytes(1)),
                    bin2hex(random_bytes(6))
                ]);

                $activationToken = new AccountActivate();
                $activationToken->setUser($newClient);
                $activationToken->setToken($activationTokenValue);
                $activationToken->setActivated(false);

                $entityManager->persist($activationToken);
                $entityManager->persist($newClient);
                $entityManager->persist($userCommunication);
                $entityManager->flush();

                // Отсылаем письмо с просьбой активировать аккаунт
                $mailService->sendRegistrationEmailToClient(
                    $resultValues['email'],
                    'Дорогой клиент',
                    $resultValues['email'],
                    $resultValues['newPassword'],
                    'https://' . $_SERVER['HTTP_HOST'],
                    $activationTokenValue
                );

                // TODO: Записать событие регистрации в историю.

                return ['clientCreated' => true];
            }
        ]);
    }
}
