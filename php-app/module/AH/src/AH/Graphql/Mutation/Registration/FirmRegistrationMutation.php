<?php

namespace AH\GraphQL\Mutation\Registration;

use AH\Core\Mail\MailService;
use AH\Entity\AccountActivate;
use AH\Entity\Client;
use AH\Entity\Firm;
use AH\Entity\Manager;
use AH\Entity\Role;
use AH\Entity\UserCommunication;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Crypt\Password\Bcrypt;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\ServiceManager;

class FirmRegistrationMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        /** @var InputFilterInterface $newFirmAndManagerInputFilter */
        $newFirmAndManagerInputFilter = $serviceManager->get('NewFirmAndManagerInputFilter');

        /** @var \Doctrine\ORM\EntityManager $entityManager */
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

        /** @var MailService $mailService */
        $mailService = $serviceManager->get(MailService::class);

        /** @var InputFilterInterface $mobilePhoneInputFilter */
        $mobilePhoneInputFilter = $serviceManager->get('ManagerRegistrationPhoneInputFilter');

        return Relay::mutationWithClientMutationId([
            'name' => 'firmRegistrationMutation',
            'inputFields' => [
                'firmName' => [
                    'type' => Type::nonNull(Type::string()),
                ],
                'email' => [
                    'type' => Type::nonNull(Type::string()),
                ],
                'newPassword' => [
                    'type' => Type::nonNull(Type::string()),
                ],
                'newPasswordConfirm' => [
                    'type' => Type::nonNull(Type::string()),
                ],
                'phone' => [
                    'description' => 'Контактный номер для активации СТО',
                    'type' => Type::nonNull(Type::string()),
                ],
                'managerMobilePhone' => [
                    'description' => 'Сотовый первого сотрудника',
                    'type' => Type::nonNull(Type::string()),
                ],
            ],
            'outputFields' => [
                'errors' => [
                    'type' => Types::mutationValidationErrorMessages(),
                    'resolve' => function ($payload) {
                        return [
                            'messages' => isset($payload['errors']) ? $payload['errors'] : []
                        ];
                    },
                ],
                'firmAndManagerCreated' => [
                    'type' => Type::boolean(),
                    'resolve' => function ($payload) {
                        return isset($payload['firmAndManagerCreated']) && $payload['firmAndManagerCreated'];
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($newFirmAndManagerInputFilter, $entityManager, $mailService, $mobilePhoneInputFilter) {
                $mobilePhoneInputFilter->setData([
                    'mobilePhone' => $input['managerMobilePhone'],
                ]);
                $newFirmAndManagerInputFilter->setData($input);

                $errors = [];
                if (!$newFirmAndManagerInputFilter->isValid()) {
                    $validationErrors = $newFirmAndManagerInputFilter->getMessages();
                    if (!empty($validationErrors)) {
                        foreach ($validationErrors as $fieldName => $errorMessages) {
                            foreach ($errorMessages as $errorType => $errorMessage) {
                                $errors[] = [
                                    'fieldName' => $fieldName,
                                    'text' => $errorMessage,
                                ];
                            }
                        }
                    }
                }

                if (!$mobilePhoneInputFilter->isValid()) {
                    if (!empty($mobilePhoneInputFilter->getMessages())) {
                        foreach ($mobilePhoneInputFilter->getMessages() as $fieldName => $errorMessages) {
                            foreach ($errorMessages as $errorType => $errorMessage) {
                                $errors[] = [
                                    'fieldName' => $fieldName,
                                    'type' => $errorType,
                                    'text' => $errorMessage,
                                ];
                            }
                        }
                    }
                }

                if (!empty($errors)) {
                    return ['errors' => $errors];
                }

                $resultValues = $newFirmAndManagerInputFilter->getValues();

                $newFirm = new Firm();
                $newFirm->setName(trim($resultValues['firmName']));
                $newFirm->setActive(false);
                $entityManager->persist($newFirm);

                $newManager = new Manager();

                $bcrypt = new Bcrypt();
                $bcrypt->setCost(10);
                $passwordHash = $bcrypt->create($resultValues['newPassword']);
                $newManager->setRegistrationEmail($resultValues['email']);
                $mobilePhoneInputFilterValues = $mobilePhoneInputFilter->getValues();

                $newManager->setRegistrationPhone($mobilePhoneInputFilterValues['mobilePhone']);
                $newManager->setPassword($passwordHash);
                $newManager->setActive(false);

                $rRoles = $entityManager->getRepository(Role::class);

                /** @var Role $managerBossRole */
                $managerBossRole = $rRoles->findOneBy([
                    'roleId' => 'manager-boss',
                ]);

                if (empty($managerBossRole)) {
                    throw new \Exception('Can`t find role manager-boss.');
                }

                $newManager->addRole($managerBossRole);

                $activationTokenValue = implode('', [
                    bin2hex(random_bytes(4)),
                    bin2hex(random_bytes(2)),
                    bin2hex(chr((ord(random_bytes(1)) & 0x0F) | 0x40)) . bin2hex(random_bytes(1)),
                    bin2hex(chr((ord(random_bytes(1)) & 0x3F) | 0x80)) . bin2hex(random_bytes(1)),
                    bin2hex(random_bytes(6))
                ]);

                $activationToken = new AccountActivate();
                $activationToken->setUser($newManager);
                $activationToken->setToken($activationTokenValue);
                $activationToken->setActivated(false);

                $userCommunication = new UserCommunication();
                $userCommunication->setActive(false);
                $userCommunication->setUser($newManager);
                $userCommunication->setPreferred(false);
                $userCommunication->setType('email');
                $userCommunication->setValue($resultValues['email']);

                $userCommunication->setActive(false);
                $userCommunication->setUser($newManager);
                $userCommunication->setPreferred(true);
                $userCommunication->setType('mobile');
                $userCommunication->setValue($resultValues['phone']);

                $entityManager->persist($userCommunication);
                $entityManager->persist($activationToken);
                $entityManager->persist($newManager);
                $newManager->setFirm($newFirm);
                $entityManager->flush();

                $mailService->sendRegistrationEmailToNewFirmManager(
                    $resultValues['email'],
                    'Дорогой менеджер фирмы ' . $resultValues['firmName'],
                    $resultValues['email'],
                    $resultValues['newPassword'],
                    $resultValues['firmName'],
                    $resultValues['phone']
                );

                $mailService->sendRegistrationEmailToModeratorAboutNewFirm(
                    'evtuhovdo@gmail.com', // TODO: Вынести в конфиг
                    $resultValues['firmName'],
                    $resultValues['phone'],
                    $resultValues['email'],
                    $activationTokenValue
                );

                // TODO: Записать событие регистрации в историю.

                return ['firmAndManagerCreated' => true];
            }
        ]);
    }
}
