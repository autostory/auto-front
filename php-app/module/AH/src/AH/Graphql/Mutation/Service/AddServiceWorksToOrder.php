<?php

namespace AH\GraphQL\Mutation\Service;

use AH\Entity\Manager;
use AH\Entity\Request;
use AH\Entity\RequestCommunication;
use AH\Entity\User;
use AH\GraphQL\Inputs;
use AH\Graphql\Type\RequestType;
use AH\GraphQL\Types;
use DateTime;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\ServiceManager;

use GraphQL\Type\Definition\InputObjectType;

$serviceWorkInputType = new InputObjectType([
    'name' => 'ServiceWorkInputType',
    'fields' => [
        'serviceWorkActionTypeId' => [
            'type' => Type::nonNull(Type::id()),
            'description' => 'Only show popular stories (liked by several people)'
        ],
        'orderId' => [
            'type' => Type::listOf(Type::string()),
            'description' => 'Only show stories which contain all of those tags'
        ]
    ]
]);

class AddServiceWorksToOrder
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'addServiceWorksToOrder',
            'inputFields' => [
                'orderId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'serviceWorks' => [
                    'type' => Type::listOf(Inputs::serviceWorkInputType()),
                ],
            ],
            'outputFields' => [
                'request' => [
                    'type' => Types::requestType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['request'];
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                /** @var AuthenticationService $authenticationService */
                $authenticationService = $serviceManager->get('AuthenticationService');
                /** @var User $me */
                $me = $authenticationService->getIdentity();

                if (empty($me) || !($me instanceof Manager)) {
                    throw new \Exception('You must login first.');
                }

                if (!isset($input['requestId'])) {
                    return null;
                }

                if (!isset($input['dateTime']) || empty($input['dateTime'])) {
                    return null;
                }

                $format = 'Y-m-d H:i:s';
                $nextCommunicationDateTime = DateTime::createFromFormat($format, $input['dateTime']);

                if (!$nextCommunicationDateTime) {
                    return null;
                }

                $idComponents = Relay::fromGlobalId($input['requestId']);

                if (
                    !isset($idComponents['type'])
                    || mb_strlen($idComponents['type']) === 0
                    || !isset($idComponents['id'])
                    || intval($idComponents['id']) <= 0
                ) {
                    return null;
                }

                $type = $idComponents['type'];
                $id = intval($idComponents['id']);

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');
                if ($type === RequestType::TYPE_NAME && $id > 0) {
                    /** @var Request $request */
                    $request = $entityManager->find(Request::class, $id);
                    if (empty($request)) {
                        return null;
                    }
                }

                if (empty($request)) {
                    return null;
                }

                $newRC = new RequestCommunication();

                $newRC->setCommunicationDateTime(new DateTime());
                $newRC->setNextCommunicationDateTime($nextCommunicationDateTime);
                $newRC->setManager($me);
                $newRC->setRequest($request);
                $newRC->setResultId(3);

                $entityManager->persist($newRC);
                $entityManager->persist($request);
                $entityManager->flush();

                return [
                    'request' => $request,
                ];
            }
        ]);
    }
}
