<?php

namespace AH\GraphQL\Mutation\PersonalData;

use AH\Core\Acl\ClientAccess;
use AH\Entity\Client;
use AH\Graphql\Type\OfficeType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Crypt\Password\Bcrypt;
use Zend\ServiceManager\ServiceManager;
use Zend\Validator\EmailAddress;

class EditClientMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'editMyPersonalDataMutation',
            'inputFields' => [
                'clientId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'password' => [
                    'type' => Type::string(),
                ],
                'email' => [
                    'type' => Type::string(),
                ],
                'phone' => [
                    'type' => Type::string(),
                ],
                'firstName' => [
                    'type' => Type::string(),
                ],
                'surname' => [
                    'type' => Type::string(),
                ],
                'patronymic' => [
                    'type' => Type::string(),
                ],
            ],
            'outputFields' => [
                'passwordChanged' => [
                    'type' => Type::boolean(),
                    'resolve' => function ($payload) {
                        return $payload['passwordChanged'];
                    },
                ],
                'user' => [
                    'type' => Types::clientType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['client'];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                if (!isset($input['clientId'])) {
                    return null;
                }

                $idComponents = Relay::fromGlobalId($input['clientId']);

                if (
                    !isset($idComponents['type'])
                    || mb_strlen($idComponents['type']) === 0
                    || !isset($idComponents['id'])
                    || intval($idComponents['id']) <= 0
                ) {
                    return null;
                }

                $type = $idComponents['type'];
                $id = intval($idComponents['id']);

                /** @var ClientAccess $clientAccess */
                $clientAccess = $serviceManager->get(ClientAccess::class);

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');
                if ($type === OfficeType::TYPE_NAME && $id > 0) {
                    /** @var Client $client */
                    $client = $entityManager->find(Client::class, $id);
                    if (empty($client) || !$clientAccess->canEditClient($client)) {
                        return null;
                    }
                }

                if (empty($client)) {
                    return null;
                }

                if (isset($input['firstName'])) {
                    $client->setFirstName(trim($input['firstName']));
                }

                if (isset($input['surname'])) {
                    $client->setSurname(trim($input['surname']));
                }

                if (isset($input['patronymic'])) {
                    $client->setPatronymic(trim($input['patronymic']));
                }

                if (isset($input['email'])) {
                    $email = mb_strtolower(trim($input['email']));
                    $validator = new EmailAddress();
                    if (!$validator->isValid($email)) {
                       throw new \Exception('Provided email invalid');
                    }

                    $client->setRegistrationEmail(trim($input['email']));
                }

                if (isset($input['phone'])) {
                    $phone = preg_replace('/[^0-9]/', '', $input['phone']);
                    if (!empty($phone)) {
                        $client->setRegistrationPhone(trim($input['phone']));
                    }
                }

                $passwordChanged = false;
                if (isset($input['password']) && $clientAccess->canChangeClientPassword($client)) {
                    $bcrypt = new Bcrypt();
                    $bcrypt->setCost(10);
                    $passwordHash = $bcrypt->create($input['password']);
                    $client->setPassword($passwordHash);
                }

                $entityManager->persist($client);
                $entityManager->flush();

                return [
                    'client' => $client,
                    'passwordChanged' => $passwordChanged,
                ];
            }
        ]);
    }
}
