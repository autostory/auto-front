<?php

namespace AH\GraphQL\Mutation\Profile;

use AH\Entity\User;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Authentication\AuthenticationService;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\ServiceManager;

class EditMyProfileMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'editMyProfileMutation',
            'inputFields' => [
                'firstName' => [
                    'type' => Type::string(),
                ],
                'surname' => [
                    'type' => Type::string(),
                ],
                'patronymic' => [
                    'type' => Type::string(),
                ],
                'email' => [
                    'type' => Type::string(),
                ],
                'phone' => [
                    'type' => Type::string(),
                ],
            ],
            'outputFields' => [
                'viewer' => [
                    'type' => Types::viewer($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['viewer'];
                    },
                ],
                'errors' => [
                    'type' => Types::mutationValidationErrorMessages(),
                    'resolve' => function ($payload) {
                        return [
                            'messages' => isset($payload['errors']) ? $payload['errors'] : []
                        ];
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                /** @var AuthenticationService $authenticationService */
                $authenticationService = $serviceManager->get('AuthenticationService');
                /** @var User $me */
                $me = $authenticationService->getIdentity();

                if (empty($me)) {
                    throw new \Exception('You must login first.');
                }

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                /** @var InputFilterInterface $identityFullNameFilter */
                $identityFullNameFilter = $serviceManager->get('IdentityFullNameFilter');

                /** @var InputFilterInterface $mobilePhoneInputFilter */
                $mobilePhoneInputFilter = $serviceManager->get('MyMobilePhoneInputFilter');

                $validationErrors = [];
                $validationErrorMessages = [];

                $identityFullNameFilter->setData($input);
                if (!$identityFullNameFilter->isValid()) {
                    $validationErrorMessages = array_merge(
                        $validationErrorMessages,
                        $identityFullNameFilter->getMessages()
                    );
                }

                if (isset($input['phone']) && strlen($input['phone']) > 0) {
                    $mobilePhoneInputFilter->setData(['mobilePhone' => $input['phone'],]);
                    if (!$mobilePhoneInputFilter->isValid(['id' => $me->getId()])) {
                        $validationErrorMessages = array_merge(
                            $validationErrorMessages,
                            [
                                'phone' => $mobilePhoneInputFilter->getMessages()['mobilePhone']
                            ]
                        );
                    }
                }

                /** @var InputFilterInterface $myEmailFilter */
                $myEmailFilter = $serviceManager->get('MyEmailFilter');
                $myEmailFilter->setData([
                    'email' => $input['email'],
                ]);

                if (!$myEmailFilter->isValid(['id' => $me->getId()])) {
                    $validationErrorMessages = array_merge(
                        $validationErrorMessages,
                        $myEmailFilter->getMessages()
                    );
                }

                if (!empty($validationErrorMessages)) {
                    foreach ($validationErrorMessages as $fieldName => $errorMessages) {
                        foreach ($errorMessages as $errorType => $errorMessage) {
                            $validationErrors[] = [
                                'fieldName' => $fieldName,
                                'type' => $errorType,
                                'text' => $errorMessage,
                            ];
                        }
                    }

                    return [
                        'viewer' => $me,
                        'errors' => $validationErrors
                    ];
                }

                if (!empty($identityAuthData['email'])) {
                    $me->setRegistrationEmail($identityAuthData['email']);
                }

                if (isset($input['phone']) && strlen($input['phone']) > 0) {
                    $mobilePhoneData = $mobilePhoneInputFilter->getValues();
                    $me->setRegistrationPhone($mobilePhoneData['mobilePhone']);
                }

                $identityFullNameData = $identityFullNameFilter->getValues();

                $me->setFirstName($identityFullNameData['firstName']);
                $me->setSurname($identityFullNameData['surname']);
                $me->setPatronymic($identityFullNameData['patronymic']);

                $entityManager->persist($me);
                $entityManager->flush();

                return [
                    'viewer' => $me,
                ];
            }
        ]);
    }
}
