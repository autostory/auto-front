<?php

namespace AH\GraphQL\Mutation\Profile;

use AH\Entity\Client;
use AH\Entity\Manager;
use AH\Entity\User;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Authentication\AuthenticationService;
use Zend\Crypt\Password\Bcrypt;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\ServiceManager;
use \OAuth2\Storage\Pdo as OAuth2Storage;

class ChangeMyPasswordMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'changeMyPasswordMutation',
            'inputFields' => [
                'newPassword' => [
                    'type' => Type::string(),
                ],
                'currentPassword' => [
                    'type' => Type::string(),
                ],
            ],
            'outputFields' => [
                'passwordChanged' => [
                    'type' => Type::boolean(),
                    'resolve' => function ($payload) {
                        return $payload['passwordChanged'];
                    },
                ],
                'errors' => [
                    'type' => Types::mutationValidationErrorMessages(),
                    'resolve' => function ($payload) {
                        return [
                            'messages' => isset($payload['errors']) ? $payload['errors'] : []
                        ];
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                /** @var AuthenticationService $authenticationService */
                $authenticationService = $serviceManager->get('AuthenticationService');
                /** @var User $me */
                $me = $authenticationService->getIdentity();

                if ($me instanceof Manager) {
                    /** @var InputFilterInterface $inputFilter */
                    $inputFilter = $serviceManager->get('ManagerAuthInputFilter');
                } else if ($me instanceof Client) {
                    /** @var InputFilterInterface $inputFilter */
                    $inputFilter = $serviceManager->get('ClientAuthInputFilter');
                } else {
                    throw new \Exception('Who are you mf?');
                }

                $passwordInput = $inputFilter->get('newPassword');

                $passwordInput->setValue($input['newPassword']);
                if (!$passwordInput->isValid()) {
                    $errors = [];
                    if (!empty($passwordInput->getMessages())) {
                        foreach ($passwordInput->getMessages() as $fieldName => $errorMessages) {
                            foreach ($errorMessages as $errorType => $errorMessage) {
                                $errors[] = [
                                    'fieldName' => $fieldName,
                                    'type' => $errorType,
                                    'text' => $errorMessage,
                                ];
                            }
                        }
                    }
                }

                $bcrypt = new Bcrypt();
                $bcrypt->setCost(10);

                if (empty($me)) {
                    throw new \Exception('You must login first.');
                }

                $oldPasswordValid = $bcrypt->verify($input['currentPassword'], $me->getPassword());

                if (!$oldPasswordValid) {
                    $errors[] = [
                        'fieldName' => 'currentPassword',
                        'type' => 'notMatch',
                        'text' => 'Указан не правильный текущий пароль.',
                    ];
                }

                if (!empty($errors)) {
                    return [
                        'passwordChanged' => false,
                        'errors' => $errors
                    ];
                }

                $newPasswordHash = $bcrypt->create($input['newPassword']);

                $me->setPassword($newPasswordHash);
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                $entityManager->persist($me);
                $entityManager->flush();

                // TODO: Отсылать уведомление о смене пароля.

                return [
                    'passwordChanged' => true,
                ];
            }
        ]);
    }
}
