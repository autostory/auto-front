<?php

namespace AH\GraphQL\Mutation\Car;

use AH\Core\Acl\CarAccess;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\Car;
use AH\Entity\CatalogCar;
use AH\Graphql\Type\CarType;
use AH\GraphQL\Type\CatalogCar as CatalogCarTypes;
use AH\GraphQL\Types;
use AH\Repository\CarRepository;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class EditCarMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        /** @var CarAccess $carAccess */
        $carAccess = $serviceManager->get(CarAccess::class);

        return Relay::mutationWithClientMutationId([
            'name' => 'editCarMutation',
            'inputFields' => [
                'carId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'vin' => [
                    'type' => Type::string(),
                ],
                'registrationSignNumber' => [
                    'type' => Type::string(),
                ],
                'color' => [
                    'type' => Type::string(),
                ],
                'year' => [
                    'type' => Type::int(),
                ],
                'catalogCarTypeId' => [
                    'type' => Type::id(),
                ],
                'catalogCarMarkId' => [
                    'type' => Type::id(),
                ],
                'catalogCarModelId' => [
                    'type' => Type::id(),
                ],
                'catalogCarGenerationId' => [
                    'type' => Type::id(),
                ],
                'catalogCarSerieId' => [
                    'type' => Type::id(),
                ],
                'catalogCarModificationId' => [
                    'type' => Type::id(),
                ],
                'catalogCarEquipmentId' => [
                    'type' => Type::id(),
                ],
            ],
            'outputFields' => [
                'errors' => [
                    'type' => Types::mutationValidationErrorMessages(),
                    'resolve' => function ($payload) {
                        return [
                            'messages' => isset($payload['errors']) ? $payload['errors'] : []
                        ];
                    },
                ],
                'car' => [
                    'type' => Types::carType($serviceManager),
                    'resolve' => function ($payload) {
                        return !empty($payload['car']) ? $payload['car'] : null;
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager, $carAccess) {
                $carId = RelayIdUtils::getId($input['carId'], CarType::TYPE_NAME);

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');
                $rCar = $entityManager->getRepository(Car::class);

                /** @var Car $car */
                $car = $rCar->find($carId);
                if (empty($car)) {
                    throw new \Exception('Can`t find car by provided id.');
                }

                if (!$carAccess->canEditCar($car)) {
                    throw new \Exception('For you not allowed edit this car.');
                }

                if (isset($input['color'])) {
                    $car->setColor(trim(strtolower($input['color'])));
                }

                if (isset($input['year'])) {
                    $car->setYear(intval($input['year']));
                }

                if (!empty($input['vin'])) {
                    $carWithThisVin = $rCar->findOneBy([
                        'vin' => trim(strtolower($input['vin'])),
                    ]);

                    if (!empty($carWithThisVin) && $carWithThisVin->getId() !== $car->getId()) {
                        return [
                            'errors' => [[
                                'fieldName' => 'vin',
                                'type' => 'Shit',
                                'text' => 'Кто-то уже добавил автомобиль с таким VIN номером.',
                            ]]
                        ];
                    }

                    $car->setVin(trim(strtolower($input['vin'])));
                }

                if (empty(trim(strtolower($input['registrationSignNumber'])))) {
                    return [
                        'errors' => [[
                            'fieldName' => 'registrationSignNumber',
                            'type' => 'Shit',
                            'text' => 'Обязательно для заполнения.',
                        ]]
                    ];
                }

                $registrationSignNumber = null;
                if (isset($input['registrationSignNumber'])) {
                    $registrationSignNumber = trim(strtolower($input['registrationSignNumber']));
                }

                if (!empty($registrationSignNumber)) {
                    /** @var CarRepository $rCar */
                    $rCar = $entityManager->getRepository(Car::class);

                    $owners = $car->getOwners();
                    foreach ($owners as $client) {
                        $clientCarsWithThisRegistrationSignNumber = $rCar
                            ->findClientCarsByRegistrationSignNumber($client, $registrationSignNumber);

                        if ($clientCarsWithThisRegistrationSignNumber->count() > 0) {

                            /** @var Car $carWithSN */
                            foreach ($clientCarsWithThisRegistrationSignNumber as $carWithSN) {
                                if ($carWithSN->getId() !== $car->getId()) {
                                    return [
                                        'errors' => [[
                                            'fieldName' => 'registrationSignNumber',
                                            'type' => 'Shit',
                                            'text' => sprintf(
                                                'У владельцев этого автомобиля уже есть автомобиль с гос номером %s.',
                                                strtoupper($registrationSignNumber)
                                            ),
                                        ]]
                                    ];
                                }
                            }
                        }
                    }
                }

                $car->setRegistrationSignNumber($registrationSignNumber);

                // Либо гос номер либо вин номер должен быть обязательно
                if (empty($registrationSignNumber) && empty($vin)) {
                    return [
                        'errors' => [
                            [
                                'fieldName' => 'registrationSignNumber',
                                'type' => 'Shit',
                                'text' => 'Укажите VIN или Гос. номер авто.',
                            ],
                            [
                                'fieldName' => 'vin',
                                'type' => 'Shit',
                                'text' => 'Укажите VIN или Гос. номер авто.',
                            ],
                        ],
                    ];
                }

                if (!empty($input['catalogCarTypeId'])) {
                    $carTypeId = RelayIdUtils::getId($input['catalogCarTypeId'], CatalogCarTypes\CatalogCarTypeType::TYPE_NAME);

                    /** @var CatalogCar\CarType $carType */
                    $carType = $entityManager->find(CatalogCar\CarType::class, $carTypeId);
                    $car->setCatalogCarType($carType);
                }

                if (!empty($input['catalogCarMarkId'])) {
                    $carMarkId = RelayIdUtils::getId($input['catalogCarMarkId'], CatalogCarTypes\CatalogCarMarkType::TYPE_NAME);

                    /** @var CatalogCar\CarMark $carMark */
                    $carMark = $entityManager->find(CatalogCar\CarMark::class, $carMarkId);
                    $car->setCatalogCarMark($carMark);
                }

                if (!empty($input['catalogCarModelId'])) {
                    $carModelId = RelayIdUtils::getId($input['catalogCarModelId'], CatalogCarTypes\CatalogCarModelType::TYPE_NAME);

                    /** @var CatalogCar\CarModel $carModel */
                    $carModel = $entityManager->find(CatalogCar\CarModel::class, $carModelId);
                    $car->setCatalogCarModel($carModel);
                }

                if (!empty($input['catalogCarSerieId'])) {
                    $carSerieId = RelayIdUtils::getId($input['catalogCarSerieId'], CatalogCarTypes\CatalogCarSerieType::TYPE_NAME);

                    /** @var CatalogCar\CarSerie $carSerie */
                    $carSerie = $entityManager->find(CatalogCar\CarSerie::class, $carSerieId);

                    $car->setCatalogCarSerie($carSerie);
                }

                if (!empty($input['catalogCarGenerationId'])) {
                    $catalogCarGenerationId = RelayIdUtils::getId($input['catalogCarGenerationId'], CatalogCarTypes\CatalogCarGenerationType::TYPE_NAME);

                    /** @var CatalogCar\CarGeneration $carGeneration */
                    $carGeneration = $entityManager->find(CatalogCar\CarGeneration::class, $catalogCarGenerationId);

                    $car->setCatalogCarGeneration($carGeneration);
                }

                if (!empty($input['catalogCarModificationId'])) {
                    $catalogCarModificationId = RelayIdUtils::getId($input['catalogCarModificationId'], CatalogCarTypes\CatalogCarModificationType::TYPE_NAME);

                    /** @var CatalogCar\CarModification $carModification */
                    $carModification = $entityManager->find(CatalogCar\CarModification::class, $catalogCarModificationId);

                    $car->setCatalogCarModification($carModification);
                }

                if (!empty($input['catalogCarEquipmentId'])) {
                    $catalogCarEquipmentId = RelayIdUtils::getId($input['catalogCarEquipmentId'], CatalogCarTypes\CatalogCarEquipmentType::TYPE_NAME);

                    /** @var CatalogCar\CarEquipment $catalogCarEquipment */
                    $catalogCarEquipment = $entityManager->find(CatalogCar\CarEquipment::class, $catalogCarEquipmentId);

                    $car->setCatalogCarEquipment($catalogCarEquipment);
                }

                $entityManager->persist($car);
                $entityManager->flush();

                return [
                    'car' => $car,
                ];
            }
        ]);
    }
}
