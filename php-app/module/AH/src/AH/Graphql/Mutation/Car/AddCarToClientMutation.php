<?php

namespace AH\GraphQL\Mutation\Car;

use AH\Core\Acl\ClientAccess;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\Car;
use AH\Entity\CarMileage;
use AH\Entity\Client;
use AH\Entity\CatalogCar;
use AH\Graphql\Type\ClientType;
use AH\GraphQL\Type\CatalogCar as CatalogCarTypes;
use AH\GraphQL\Types;
use AH\Repository\CarRepository;
use AH\Repository\ClientRepository;
use Doctrine\Common\Collections\Criteria;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class AddCarToClientMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'addCarToClientMutation',
            'inputFields' => [
                'clientId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'vin' => [
                    'type' => Type::string(),
                ],
                'registrationSignNumber' => [
                    'type' => Type::string(),
                ],
                'color' => [
                    'type' => Type::string(),
                ],
                'year' => [
                    'type' => Type::int(),
                ],
                'mileage' => [
                    'type' => Type::int(),
                ],
                'catalogCarTypeId' => [
                    'type' => Type::id(),
                ],
                'catalogCarMarkId' => [
                    'type' => Type::id(),
                ],
                'catalogCarModelId' => [
                    'type' => Type::id(),
                ],
                'catalogCarGenerationId' => [
                    'type' => Type::id(),
                ],
                'catalogCarSerieId' => [
                    'type' => Type::id(),
                ],
                'catalogCarModificationId' => [
                    'type' => Type::id(),
                ],
                'catalogCarEquipmentId' => [
                    'type' => Type::id(),
                ],
            ],
            'outputFields' => [
                'errors' => [
                    'type' => Types::mutationValidationErrorMessages(),
                    'resolve' => function ($payload) {
                        return [
                            'messages' => isset($payload['errors']) ? $payload['errors'] : []
                        ];
                    },
                ],
                'newCar' => [
                    'type' => Types::carType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['newCar'];
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                $clientId = RelayIdUtils::getId($input['clientId'], ClientType::TYPE_NAME);

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                /** @var Client $client */
                $client = $entityManager->getReference(Client::class, $clientId);
                if (empty($client)) {
                    return null;
                }

                /** @var ClientAccess $clientAccess */
                $clientAccess = $serviceManager->get(ClientAccess::class);

                if (!$clientAccess->canAddCarToClient($client)) {
                    return null;
                }

                $newCar = new Car();
                $newCar->addOwner($client);
                $client->addCar($newCar);

                if (isset($input['color'])) {
                    $newCar->setColor(trim(strtolower($input['color'])));
                }

                if (isset($input['year'])) {
                    $newCar->setYear(intval($input['year']));
                }

                if (isset($input['mileage'])) {
                    $carMileage = new CarMileage();
                    $carMileage->setCar($newCar);
                    $carMileage->setMeasuredAt(new \DateTime());
                    $carMileage->setValue(intval($input['mileage']));
                    $newCar->addMillage($carMileage);
                    $entityManager->persist($carMileage);
                }

                $rCar = $entityManager->getRepository('AH\Entity\Car');

                $vin = null;
                if (isset($input['vin'])) {
                    $vin = trim(strtolower($input['vin']));
                }

                if (!empty($vin)) {
                    $carWithThisVin = $rCar->findOneBy([
                        'vin' => $vin,
                    ]);

                    if (!empty($carWithThisVin)) {
                        return [
                            'errors' => [[
                                'fieldName' => 'vin',
                                'type' => 'Shit',
                                'text' => 'Кто-то уже добавил автомобиль с таким VIN номером.',
                            ]]
                        ];
                    }

                    $newCar->setVin($vin);
                }

                $registrationSignNumber = null;
                if (isset($input['registrationSignNumber'])) {
                    $registrationSignNumber = trim(strtolower($input['registrationSignNumber']));
                }

                // Либо гос номер либо вин номер должен быть обязательно
                if (empty($registrationSignNumber) && empty($vin)) {
                    return [
                        'errors' => [
                            [
                                'fieldName' => 'registrationSignNumber',
                                'type' => 'Shit',
                                'text' => 'Укажите VIN или Гос. номер авто.',
                            ],
                            [
                                'fieldName' => 'vin',
                                'type' => 'Shit',
                                'text' => 'Укажите VIN или Гос. номер авто.',
                            ],
                        ],
                    ];
                }

                if (!empty($registrationSignNumber)) {
                    /** @var CarRepository $rCar */
                    $rCar = $entityManager->getRepository(Car::class);

                    $clientCarsWithThisRegistrationSignNumber = $rCar
                        ->findClientCarsByRegistrationSignNumber($client, $registrationSignNumber);

                    if ($clientCarsWithThisRegistrationSignNumber->count() > 0) {
                        return [
                            'errors' => [[
                                'fieldName' => 'registrationSignNumber',
                                'type' => 'Shit',
                                'text' => sprintf(
                                    'У клиента уже есть автомобиль с гос номером %s.',
                                    strtoupper($registrationSignNumber)
                                ),
                            ]]
                        ];
                    }
                }

                $newCar->setRegistrationSignNumber($registrationSignNumber);

                if (!empty($input['catalogCarTypeId'])) {
                    $carTypeId = RelayIdUtils::getId($input['catalogCarTypeId'], CatalogCarTypes\CatalogCarTypeType::TYPE_NAME);

                    $carType = $entityManager->find(CatalogCar\CarType::class, $carTypeId);
                    $newCar->setCatalogCarType($carType);
                }

                if (!empty($input['catalogCarMarkId'])) {
                    $carMarkId = RelayIdUtils::getId($input['catalogCarMarkId'], CatalogCarTypes\CatalogCarMarkType::TYPE_NAME);

                    $carMark = $entityManager->find(CatalogCar\CarMark::class, $carMarkId);
                    $newCar->setCatalogCarMark($carMark);
                }

                if (!empty($input['catalogCarModelId'])) {
                    $carModelId = RelayIdUtils::getId($input['catalogCarModelId'], CatalogCarTypes\CatalogCarModelType::TYPE_NAME);

                    $carModel = $entityManager->find(CatalogCar\CarModel::class, $carModelId);
                    $newCar->setCatalogCarModel($carModel);
                }

                if (!empty($input['catalogCarSerieId'])) {
                    $carSerieId = RelayIdUtils::getId($input['catalogCarSerieId'], CatalogCarTypes\CatalogCarSerieType::TYPE_NAME);

                    /** @var CatalogCar\CarSerie $carSerie */
                    $carSerie = $entityManager->find(CatalogCar\CarSerie::class, $carSerieId);

                    $newCar->setCatalogCarSerie($carSerie);
                }

                if (!empty($input['catalogCarGenerationId'])) {
                    $catalogCarGenerationId = RelayIdUtils::getId($input['catalogCarGenerationId'], CatalogCarTypes\CatalogCarGenerationType::TYPE_NAME);

                    /** @var CatalogCar\CarGeneration $carGeneration */
                    $carGeneration = $entityManager->find(CatalogCar\CarGeneration::class, $catalogCarGenerationId);

                    $newCar->setCatalogCarGeneration($carGeneration);
                }

                if (!empty($input['catalogCarModificationId'])) {
                    $catalogCarModificationId = RelayIdUtils::getId($input['catalogCarModificationId'], CatalogCarTypes\CatalogCarModificationType::TYPE_NAME);

                    /** @var CatalogCar\CarModification $carModification */
                    $carModification = $entityManager->find(CatalogCar\CarModification::class, $catalogCarModificationId);

                    $newCar->setCatalogCarModification($carModification);
                }

                if (!empty($input['catalogCarEquipmentId'])) {
                    $catalogCarEquipmentId = RelayIdUtils::getId($input['catalogCarEquipmentId'], CatalogCarTypes\CatalogCarEquipmentType::TYPE_NAME);

                    /** @var CatalogCar\CarEquipment $catalogCarEquipment */
                    $catalogCarEquipment = $entityManager->find(CatalogCar\CarEquipment::class, $catalogCarEquipmentId);

                    $newCar->setCatalogCarEquipment($catalogCarEquipment);
                }

                $entityManager->persist($newCar);
                $entityManager->persist($client);
                $entityManager->flush();

                return [
                    'newCar' => $newCar,
                ];
            }
        ]);
    }
}
