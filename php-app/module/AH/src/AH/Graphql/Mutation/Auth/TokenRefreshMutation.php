<?php

namespace AH\GraphQL\Mutation\Auth;

use AH\GraphQL\Types;
use OAuth2\Server as OAuth2Server;
use OAuth2\Request as OAuth2Request;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Crypt\Password\Bcrypt;
use Zend\ServiceManager\ServiceManager;

class TokenRefreshMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'tokenRefreshMutation',
            'inputFields' => [],
            'outputFields' => [
                'tokenInfo' => [
                    'type' => Types::oauth2TokenInfo(),
                    'resolve' => function ($payload) {
                        if (!isset($payload['tokenInfo'])) {
                            return [
                                'access_token' => null,
                                'expires_in' => null,
                                'refresh_token' => null,
                            ];
                        }

                        $tokenInfo = $payload['tokenInfo'];

                        return [
                            'access_token' => isset($tokenInfo['access_token']) ? $tokenInfo['access_token'] : null,
                            'expires_in' => isset($tokenInfo['expires_in']) ? $tokenInfo['expires_in'] : null,
                            'refresh_token' => isset($tokenInfo['refresh_token']) ? $tokenInfo['refresh_token'] : null,
                        ];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                $oauth2ServerFactory = $serviceManager->get('ZF\OAuth2\Service\OAuth2Server');
                $server = call_user_func($oauth2ServerFactory, '/oauth');
                if (!$server instanceof OAuth2Server) {
                    throw new \RuntimeException('Cant create Oauth2Server.');
                }

                if (!isset($_COOKIE['refreshToken']) || empty($_COOKIE['refreshToken'])) {
                    return null;
                }

                $bodyParams = [
                    'grant_type' => 'refresh_token',
                    'client_id' => 'ah-frontend',
                    'refresh_token' => $_COOKIE['refreshToken'],
                ];

                $oauth2request = new OAuth2Request(
                    [],
                    $bodyParams,
                    [],
                    [],
                    [],
                    $_SERVER
                );

                $response = $server->handleTokenRequest($oauth2request);

                if ($response->getStatusCode() !== 200) {
                    return null;
                }

                $result = $response->getParameters();

                return [
                    'tokenInfo' => [
                        'access_token' => isset($result['access_token']) ? $result['access_token'] : null,
                        'expires_in' => isset($result['expires_in']) ? $result['expires_in'] : null,
                        'refresh_token' => isset($result['refresh_token']) ? $result['refresh_token'] : null,
                    ]
                ];
            }
        ]);
    }
}
