<?php

namespace AH\GraphQL\Mutation\Auth;

use AH\Core\Service\OAuth2Service;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class LoginMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'loginMutation',
            'inputFields' => [
                'login' => [
                    'type' => Type::nonNull(Type::string()),
                ],
                'password' => [
                    'type' => Type::nonNull(Type::string()),
                ],
            ],
            'outputFields' => [
                'tokenInfo' => [
                    'type' => Types::oauth2TokenInfo(),
                    'resolve' => function ($payload) {
                        if (!isset($payload['tokenInfo'])) {
                            return [
                                'access_token' => null,
                                'expires_in' => null,
                                'refresh_token' => null,
                            ];
                        }

                        $tokenInfo = $payload['tokenInfo'];

                        return [
                            'access_token' => isset($tokenInfo['access_token']) ? $tokenInfo['access_token'] : null,
                            'expires_in' => isset($tokenInfo['expires_in']) ? $tokenInfo['expires_in'] : null,
                            'refresh_token' => isset($tokenInfo['refresh_token']) ? $tokenInfo['refresh_token'] : null,
                        ];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                /** @var OAuth2Service $oAuth2Service */
                $oAuth2Service = $serviceManager->get(OAuth2Service::class);

                $login = trim(strval($input['login']));
                $password = strval($input['password']);

                $result = $oAuth2Service->login($login, $password);

                return $result;
            }
        ]);
    }
}
