<?php

namespace AH\GraphQL\Mutation\Auth;

use AH\Core\Service\OAuth2Service;
use AH\GraphQL\Types;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class LogoutMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'logoutMutation',
            'inputFields' => [],
            'outputFields' => [
                'tokenInfo' => [
                    'type' => Types::oauth2TokenInfo(),
                    'resolve' => function () {
                        return [
                            'access_token' => null,
                            'expires_in' => null,
                            'refresh_token' => null,
                        ];
                    },
                ],
            ],
            'mutateAndGetPayload' => function () use ($serviceManager) {
                /** @var OAuth2Service $oAuth2Service */
                $oAuth2Service = $serviceManager->get(OAuth2Service::class);

                $oAuth2Service->logout();
            }
        ]);
    }
}
