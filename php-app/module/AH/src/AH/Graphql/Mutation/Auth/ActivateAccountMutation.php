<?php

namespace AH\GraphQL\Mutation\Auth;

use AH\Entity\AccountActivate;
use AH\Entity\Manager;
use AH\Entity\UserCommunication;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class ActivateAccountMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        /** @var \Doctrine\ORM\EntityManager $entityManager */
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

        return Relay::mutationWithClientMutationId([
            'name' => 'activateAccountMutation',
            'inputFields' => [
                'token' => [
                    'type' => Type::nonNull(Type::string()),
                ],
            ],
            'outputFields' => [
                'activated' => [
                    'type' => Type::nonNull(Type::boolean()),
                    'resolve' => function ($payload) {
                        return isset($payload['activated']) && !empty($payload['activated']);
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($entityManager) {
                $rAccountActivate = $entityManager->getRepository(AccountActivate::class);

                // TODO: Защитится от перебора активационных токенов
                // TODO: Добавить проверку прав на доступ к активации акккаунтов.

                /** @var AccountActivate $accountActivate */
                $accountActivate = $rAccountActivate->findOneBy([
                    'token' => $input['token'],
                    'activated' => false,
                ]);

                if (empty($accountActivate)) {
                    return null;
                }

                $userToActivate = $accountActivate->getUser();

                if ($userToActivate instanceof Manager) {
                    $firmToActivate = $userToActivate->getFirm();
                    if (!$firmToActivate->getActive()) {
                        $firmToActivate->setActive(true);
                        $entityManager->persist($firmToActivate);
                    }
                }

                $userToActivate->setActive(true);

                // Активируем контакты с пользователем
                $userCommunications = $userToActivate->getCommunications();
                if (!empty($userCommunications)) {
                    /** @var UserCommunication $userCommunication */
                    foreach ($userCommunications as $userCommunication) {
                        if (
                            $userCommunication->getType() === 'email'
                            && !empty($userCommunication->getValue())
                            && $userCommunication->getValue() === $userToActivate->getRegistrationEmail()
                        ) {
                            $userCommunication->setActive(true);
                            $entityManager->persist($userCommunication);
                        }

                        if (
                            $userCommunication->getType() === 'mobile'
                            && !empty($userCommunication->getValue())
                            && $userCommunication->getValue() === $userToActivate->getRegistrationPhone()
                        ) {
                            $userCommunication->setActive(true);
                            $entityManager->persist($userCommunication);
                        }
                    }
                }

                $accountActivate->setActivated(true);

                $entityManager->persist($accountActivate);
                $entityManager->persist($userToActivate);
                $entityManager->flush();

                return [
                    'activated' => true,
                ];
            }
        ]);
    }
}
