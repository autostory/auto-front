<?php

namespace AH\GraphQL\Mutation\PasswordRestore;

use AH\Core\Service\EventLogService;
use AH\Core\Service\PasswordRestoreService;
use AH\Core\SMS\SMSService;
use AH\Entity\Client;
use AH\Entity\Manager;
use AH\Entity\User;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;
use Zend\Validator\EmailAddress;

class SendPasswordRestoreMessageMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'sendPasswordRestoreMessageMutation',
            'inputFields' => [
                'emailOrPhone' => [
                    'type' => Type::nonNull(Type::string()),
                ],
                'type' => [
                    'type' => Type::nonNull(Type::string()),
                ],
            ],
            'outputFields' => [
                'messageSendBy' => [
                    'type' => Type::string(),
                    'resolve' => function ($payload) {
                        return isset($payload['messageSendBy']) ? $payload['messageSendBy'] : null;
                    },
                ],
                'token' => [
                    'type' => Type::string(),
                    'resolve' => function ($payload) {
                        return isset($payload['token']) ? $payload['token'] : null;
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                $emailOrPhone = mb_strtolower(trim($input['emailOrPhone']));

                $phone = null;
                if (!mb_strpos($emailOrPhone, '@')) {
                    $phone = intval(preg_replace('/[^0-9]/', '', $emailOrPhone));

                    if (mb_strlen($phone) === 11 && intval(mb_substr($phone, 0, 1)) === 8) {
                        $phone = intval(mb_substr($phone, 1));
                    }
                }

                $email = null;
                $validator = new EmailAddress();
                if ($validator->isValid($emailOrPhone)) {
                    $email = $emailOrPhone;
                }

                if (empty($email) && empty($phone)) {
                    throw new \Exception('Provided wrong email or password.');
                }

                $criteria = [
                    'active' => true,
                ];

                $type = $input['type'];
                if ($type !== 'client' && $type !== 'manager') {
                    throw new \Exception('Provided wrong user type.');
                }

                if (!empty($email)) {
                    $criteria['registrationEmail'] = $email;
                } else {
                    $criteria['registrationPhone'] = $phone;
                }

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                if ($type === 'client') {
                    $rUser = $entityManager->getRepository(Client::class);
                } else if  ($type === 'manager') {
                    $rUser = $entityManager->getRepository(Manager::class);
                }

                /** @var User $user */
                $user = $rUser->findOneBy($criteria);

                if (empty($user)) {
                    return null;
                }

                /** @var PasswordRestoreService $passwordRestoreService */
                $passwordRestoreService = $serviceManager->get('AH\Core\Service\PasswordRestoreService');

                /** @var EventLogService $eventLogService */
                $eventLogService = $serviceManager->get(EventLogService::class);

                if (!empty($email)) {
                    $passwordRestoreService->sendPasswordRestoreMessage($user, 'email');
                    $eventLogService->logPasswordRestoreTokenRequest($user, 'email');

                    return [
                        'messageSendBy' => 'email',
                        'token' => null,
                    ];
                }

                $token = $passwordRestoreService->sendPasswordRestoreMessage($user, 'sms');
                $eventLogService->logPasswordRestoreTokenRequest($user, 'sms');

                return [
                    'messageSendBy' => 'sms',
                    'token' => $token,
                ];
            }
        ]);
    }
}
