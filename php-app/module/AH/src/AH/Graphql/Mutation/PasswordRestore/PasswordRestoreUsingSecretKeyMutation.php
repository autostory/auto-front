<?php

namespace AH\GraphQL\Mutation\PasswordRestore;

use AH\Core\Service\EventLogService;
use AH\Core\Service\PasswordRestoreService;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class PasswordRestoreUsingSecretKeyMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'passwordRestoreUsingSecretKeyMutation',
            'inputFields' => [
                'token' => [
                    'type' => Type::nonNull(Type::string()),
                ],
                'smsCode' => [
                    'type' => Type::string(),
                    'defaultValue' => null,
                ],
            ],
            'outputFields' => [
                'passwordChanged' => [
                    'type' => Type::boolean(),
                    'resolve' => function ($payload) {
                        return isset($payload['success']) ? $payload['success'] : null;
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                /** @var PasswordRestoreService $passwordRestoreService */
                $passwordRestoreService = $serviceManager->get('AH\Core\Service\PasswordRestoreService');

                $user = $passwordRestoreService->sendNewPassword($input['token'], $input['smsCode']);

                if (!empty($user)) {
                    /** @var EventLogService $eventLogService */
                    $eventLogService = $serviceManager->get(EventLogService::class);
                    $eventLogService->logPasswordRestoreMakeNewPassword($user);

                    /** @var \Doctrine\ORM\EntityManager $entityManager */
                    $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                    // Чтобы потом случайно не сменить пароль клиента при отпрвки сообщения о создании заказа.
                    $user->setInviteSent(true);
                    $entityManager->persist($user);
                    $entityManager->flush();

                    return [
                        'success' => true,
                    ];
                }

                return [
                    'success' => false,
                ];
            }
        ]);
    }
}
