<?php

namespace AH\GraphQL\Mutation\Payment;

use AH\Core\Service\GetOrderStatusForCurrentEntityStateService;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\ClientPointWriteOffLog;
use AH\Entity\Manager;
use AH\Entity\Order;
use AH\Entity\OrderPayment;
use AH\Entity\User;
use AH\Graphql\Enum\PaymentTypeEnumType;
use AH\GraphQL\Enums;
use AH\Graphql\Type\OrderType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\ServiceManager;

class AddOrderPaymentMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'addOrderPaymentMutation',
            'inputFields' => [
                'orderId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'note' => [
                    'type' => Type::string(),
                ],
                'amount' => [
                    'type' => Type::nonNull(Type::float()),
                ],
                'paymentType' => [
                    'type' => Type::nonNull(Enums::paymentTypeEnumType()),
                ],
            ],
            'outputFields' => [
                'order' => [
                    'type' => Types::orderType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['order'];
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                /** @var AuthenticationService $authenticationService */
                $authenticationService = $serviceManager->get('AuthenticationService');
                /** @var User $me */
                $me = $authenticationService->getIdentity();

                if (empty($me) || !($me instanceof Manager)) {
                    throw new \Exception('You must login first.');
                }

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                $orderId = RelayIdUtils::getId($input['orderId'], OrderType::TYPE_NAME);

                if ($orderId <= 0) {
                    throw new \Exception('Wrong orderId was provided.');
                }

                /** @var Order $order */
                $order = $entityManager->find(Order::class, $orderId);
                if (empty($order)) {
                    throw new \Exception('Wrong order was provided.');
                }

                $amount = floatval($input['amount']);
                if ($amount <= 0) {
                    throw new \Exception('Amount should be positive an gt zero.');
                }

                $client = $order->getClient();

                $payment = new OrderPayment();
                $payment->setOrder($order);
                $payment->setClient($client);
                $payment->setFirm($me->getFirm());
                $payment->setCashier($me);
                $payment->setPaymentType($input['paymentType']);
                $payment->setAmount($input['amount']);
                if (isset($input['note'])) {
                    $payment->setNote(trim($input['note']));
                }
                $payment->setMakeAt(new \DateTime());
                $order->addPayment($payment);

                if ($input['paymentType'] === PaymentTypeEnumType::POINT_VALUE) {
                    $availablePoints = $client->getPoints();
                    if ($availablePoints < $amount) {
                        throw new \Exception('Client don`t have enough point.');
                    }

                    $firm = $order->getFirm();
                    $firm->setPoints($firm->getPoints() + $amount);
                    $entityManager->persist($firm);

                    $client->setPoints($client->getPoints() - $amount);
                    $entityManager->persist($client);

                    $clientPointLog = new ClientPointWriteOffLog();
                    $clientPointLog->setClient($client);
                    $clientPointLog->setAmount($amount);
                    $clientPointLog->setOrderPayment($payment);
                    $entityManager->persist($clientPointLog);
                }

                $entityManager->persist($order);
                $entityManager->persist($payment);
                $entityManager->flush();

                /** @var GetOrderStatusForCurrentEntityStateService $getOrderStatusForCurrentEntityStateService */
                $getOrderStatusForCurrentEntityStateService = $serviceManager->get('GetOrderStatusForCurrentEntityStateService');
                $order->setStatus($getOrderStatusForCurrentEntityStateService->getStatus($order));

                $entityManager->persist($order);
                $entityManager->flush();

                return [
                    'order' => $order,
                ];
            }
        ]);
    }
}
