<?php

namespace AH\GraphQL\Mutation\Manager;

use AH\Core\Acl\FirmAccess;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\Firm;
use AH\Entity\Manager;
use AH\Entity\Service\ServiceWork;
use AH\Graphql\Enum\ServiceWorkStatusEnumType;
use AH\Graphql\Type\ManagerType;
use AH\GraphQL\Types;
use AH\Repository\ServiceWorkRepository;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\ServiceManager;

class DeleteManagerMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        /** @var FirmAccess $firmAccessService */
        $firmAccessService = $serviceManager->get(FirmAccess::class);

        /** @var AuthenticationService $authenticationService */
        $authenticationService = $serviceManager->get('AuthenticationService');

        return Relay::mutationWithClientMutationId([
            'name' => 'deleteManagerMutation',
            'inputFields' => [
                'managerId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
            ],
            'outputFields' => [
                'manager' => [
                    'type' => Types::managerType($serviceManager),
                    'resolve' => function ($payload) {
                        return isset($payload['manager']) ? $payload['manager'] : null;
                    },
                ],
                'myFirm' => [
                    'type' => Types::firmType($serviceManager),
                    'description' => 'Моя фирма.',
                    'resolve' => function ($payload) use ($firmAccessService, $authenticationService) {
                        $me = $authenticationService->getIdentity();

                        if (
                            !empty($me)
                            && $me instanceof Manager
                            && $firmAccessService->canViewFirm($me->getFirm())
                        ) {
                            return $me->getFirm();
                        }

                        return null;
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                /** @var \Zend\Authentication\AuthenticationService $authenticationService */
                $authenticationService = $serviceManager->get('AuthenticationService');
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                /** @var Manager $authUser */
                $authUser = $authenticationService->getIdentity();
                if (empty($authUser) || !($authUser instanceof Manager)) {
                    return null;
                }

                $managerId = RelayIdUtils::getId($input['managerId'], ManagerType::TYPE_NAME);
                /** @var Manager $manager */
                $manager = $entityManager->find(Manager::class, $managerId);

                if (empty($manager)) {
                    throw new \Exception('Cant find manager.');
                }

                /** @var Firm $firm */
                $firm = $authUser->getFirm();
                if (empty($firm)) {
                    return null;
                }

                if ($firm->getId() !== $manager->getFirm()->getId()) {
                    throw new \Exception('У вас нет доступа до этой фирмы.');
                }

                $manager->setActive(false);

                /*
                 * если мы удаляем менеджера который привязан к расписанию
                 * которое ожидает взятия в работу, в работе (в общем все кроме выполненных).
                 * то ставим вместо того кто был значение "не указан".
                 */

                /** @var ServiceWorkRepository $rServiceWork */
                $rServiceWork = $entityManager->getRepository(ServiceWork::class);
                $sws = $rServiceWork->getServiceWorksByManager(
                    $manager,
                    [
                        ServiceWorkStatusEnumType::SERVICE_WORK_IN_WORK_VALUE,
                        ServiceWorkStatusEnumType::SERVICE_WORK_WAIT_WORK_START_VALUE,
                    ]
                );

                if (!empty($sws)) {
                    /** @var ServiceWork $sw */
                    foreach ($sws as $sw) {
                        $sw->setMaster(null);
                        $entityManager->persist($sw);
                    }
                }

                $entityManager->persist($manager);
                $entityManager->flush();

                return [
                    'manager' => $manager,
                ];
            }
        ]);
    }
}
