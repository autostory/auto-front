<?php

namespace AH\GraphQL\Mutation\Manager;

use AH\Core\Acl\FirmAccess;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\Firm;
use AH\Entity\Manager;
use AH\Graphql\Type\ManagerType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Authentication\AuthenticationService;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\ServiceManager;

class EditManagerMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        /** @var FirmAccess $firmAccessService */
        $firmAccessService = $serviceManager->get(FirmAccess::class);

        /** @var AuthenticationService $authenticationService */
        $authenticationService = $serviceManager->get('AuthenticationService');

        return Relay::mutationWithClientMutationId([
            'name' => 'editManagerMutation',
            'inputFields' => [
                'managerId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'email' => [
                    'type' => Type::string(),
                ],
                'phone' => [
                    'type' => Type::string(),
                ],
                'firstName' => [
                    'type' => Type::string(),
                ],
                'surname' => [
                    'type' => Type::string(),
                ],
                'patronymic' => [
                    'type' => Type::string(),
                ],
            ],
            'outputFields' => [
                'errors' => [
                    'type' => Types::mutationValidationErrorMessages(),
                    'resolve' => function ($payload) {
                        return [
                            'messages' => isset($payload['errors']) ? $payload['errors'] : []
                        ];
                    },
                ],
                'manager' => [
                    'type' => Types::managerType($serviceManager),
                    'resolve' => function ($payload) {
                        return isset($payload['manager']) ? $payload['manager'] : null;
                    },
                ],
                'myFirm' => [
                    'type' => Types::firmType($serviceManager),
                    'description' => 'Моя фирма.',
                    'resolve' => function ($payload) use ($firmAccessService, $authenticationService) {
                        $me = $authenticationService->getIdentity();

                        if (
                            !empty($me)
                            && $me instanceof Manager
                            && $firmAccessService->canViewFirm($me->getFirm())
                        ) {
                            return $me->getFirm();
                        }

                        return null;
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                /** @var \Zend\Authentication\AuthenticationService $authenticationService */
                $authenticationService = $serviceManager->get('AuthenticationService');

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                /** @var InputFilterInterface $identityFullNameFilter */
                $identityFullNameFilter = $serviceManager->get('IdentityFullNameFilter');

                /** @var InputFilterInterface $mobilePhoneInputFilter */
                $mobilePhoneInputFilter = $serviceManager->get('MyMobilePhoneInputFilter');

                /** @var Manager $authUser */
                $authUser = $authenticationService->getIdentity();
                if (empty($authUser) || !($authUser instanceof Manager)) {
                    return null;
                }

                $managerId = RelayIdUtils::getId($input['managerId'], ManagerType::TYPE_NAME);
                /** @var Manager $manager */
                $manager = $entityManager->find(Manager::class, $managerId);

                if (empty($manager)) {
                    throw new \Exception('Cant find manager.');
                }

                /** @var Firm $firm */
                $firm = $authUser->getFirm();
                if (empty($firm)) {
                    return null;
                }

                if ($firm->getId() !== $manager->getFirm()->getId()) {
                    throw new \Exception('У вас нет доступа до этой фирмы.');
                }

                $validationErrors = [];
                $validationErrorMessages = [];

                $identityFullNameFilter->setData($input);
                if (!$identityFullNameFilter->isValid()) {
                    $validationErrorMessages = array_merge(
                        $validationErrorMessages,
                        $identityFullNameFilter->getMessages()
                    );
                }

                /** @var InputFilterInterface $myEmailFilter */
                $myEmailFilter = $serviceManager->get('MyEmailFilter');
                $myEmailFilter->setData([
                    'email' => $input['email'],
                ]);

                if (!$myEmailFilter->isValid(['id' => $manager->getId()])) {
                    $validationErrorMessages = array_merge(
                        $validationErrorMessages,
                        $myEmailFilter->getMessages()
                    );
                }

                if (isset($input['phone']) && strlen($input['phone']) > 0) {
                    $mobilePhoneInputFilter->setData(['mobilePhone' => $input['phone']]);
                    if (!$mobilePhoneInputFilter->isValid(['id' => $manager->getId()])) {
                        $validationErrorMessages = array_merge(
                            $validationErrorMessages,
                            $mobilePhoneInputFilter->getMessages()
                        );
                    }
                }

                if (!empty($validationErrorMessages)) {
                    foreach ($validationErrorMessages as $fieldName => $errorMessages) {
                        foreach ($errorMessages as $errorType => $errorMessage) {
                            $validationErrors[] = [
                                'fieldName' => $fieldName,
                                'type' => $errorType,
                                'text' => $errorMessage,
                            ];
                        }
                    }

                    return ['errors' => $validationErrors];
                }

                $identityAuthData = $myEmailFilter->getValues();
                $identityFullNameData = $identityFullNameFilter->getValues();

                if (!empty($identityAuthData['email'])) {
                    $manager->setRegistrationEmail($identityAuthData['email']);
                }

                if (isset($input['phone']) && strlen($input['phone']) > 0) {
                    $mobilePhoneData = $mobilePhoneInputFilter->getValues();
                    $manager->setRegistrationPhone($mobilePhoneData['mobilePhone']);
                }

                if (!isset($input['phone']) || empty($input['phone'])) {
                    $manager->setRegistrationPhone(null);
                }

                $manager->setFirstName($identityFullNameData['firstName']);
                $manager->setSurname($identityFullNameData['surname']);
                $manager->setPatronymic($identityFullNameData['patronymic']);

                $entityManager->persist($manager);
                $entityManager->flush();

                return [
                    'manager' => $manager,
                ];
            }
        ]);
    }
}
