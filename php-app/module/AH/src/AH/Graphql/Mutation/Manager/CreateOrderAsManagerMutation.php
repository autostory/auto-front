<?php

namespace AH\GraphQL\Mutation\Manager;

use AH\Core\Mail\MailService;
use AH\Core\Service\ClientAutoLoginLinkGenerator;
use AH\Core\Service\EventLogService;
use AH\Core\Service\GetOrderStatusForCurrentEntityStateService;
use AH\Core\Service\PasswordGenerator;
use AH\Core\SMS\SMSService;
use AH\Core\Utils\RelayIdUtils;
use AH\Core\Utils\WrongRelayIdException;
use AH\Entity\Basic\SparePartNode;
use AH\Entity\Basic\UnitMeasureType;
use AH\Entity\BoxSchedule;
use AH\Entity\Car;
use AH\Entity\Client;
use AH\Entity\Log\SmsLogRecord;
use AH\Entity\Manager;
use AH\Entity\Office;
use AH\Entity\OfficeBox;
use AH\Entity\Order;
use AH\Entity\Request;
use AH\Entity\Service\ServiceMaterial;
use AH\Entity\Service\ServiceWork;
use AH\Entity\Service\ServiceWorkActionType;
use AH\Graphql\Enum\OrderStatusEnumType;
use AH\Graphql\Enum\ScheduleStatusEnumType;
use AH\Graphql\Enum\ServiceWorkStatusEnumType;
use AH\GraphQL\Inputs;
use AH\Graphql\Type\Basic\ServiceWorkActionTypeType;
use AH\Graphql\Type\Basic\SparePartNodeType;
use AH\Graphql\Type\Basic\UnitMeasureTypeType;
use AH\Graphql\Type\BoxScheduleType;
use AH\Graphql\Type\CarType;
use AH\Graphql\Type\ClientType;
use AH\Graphql\Type\ManagerType;
use AH\Graphql\Type\OfficeBoxType;
use AH\Graphql\Type\OfficeType;
use AH\Graphql\Type\RequestType;
use AH\Graphql\Type\ServiceWorkType;
use AH\GraphQL\Types;
use DateTime;
use Exception;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Authentication\AuthenticationService;
use Zend\Crypt\Password\Bcrypt;
use Zend\ServiceManager\ServiceManager;

class CreateOrderAsManagerMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'createOrderAsManagerMutation',
            'inputFields' => [
                'clientId' => [
                    'type' => Type::nonNull(Type::Id()),
                ],
                'carId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'officeId' => [
                    'type' => Type::id(),
                ],
                'boxId' => [
                    'type' => Type::id(),
                ],
                'requestId' => [
                    'type' => Type::id(),
                ],
                'contactName' => [
                    'type' => Type::string(),
                ],
                'contactPhone' => [
                    'type' => Type::string(),
                ],
                'phoneSameAsInProfile' => [
                    'type' => Type::boolean(),
                ],
                'contactNote' => [
                    'type' => Type::string(),
                ],
                'orderReason' => [
                    'type' => Type::nonNull(Type::string()),
                ],
                'serviceWorks' => [
                    'type' => Type::listOf(Inputs::serviceWorkInputType()),
                ],
                'serviceMaterials' => [
                    'type' => Type::listOf(Inputs::serviceMaterialInputType()),
                ],
                'estimatedClientArrivalDate' => [
                    'type' => Type::string(),
                ],
                'isClientVisitRequired' => [
                    'type' => Type::nonNull(Type::boolean()),
                ],
                'schedules' => [
                    'type' => Type::listOf(Inputs::scheduleInputType()),
                ],
            ],
            'outputFields' => [
                'newOrder' => [
                    'type' => Types::orderType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['newOrder'];
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                /** @var AuthenticationService $authenticationService */
                $authenticationService = $serviceManager->get('AuthenticationService');
                $me = $authenticationService->getIdentity();
                if (!($me instanceof Manager)) {
                    return null;
                }

                $clientId = RelayIdUtils::getId($input['clientId'], ClientType::TYPE_NAME);
                $carId = RelayIdUtils::getId($input['carId'], CarType::TYPE_NAME);

                if (!$clientId || !$carId) {
                    return null;
                }

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                if (isset($input['requestId'])) {
                    $requestId = RelayIdUtils::getId($input['requestId'], RequestType::TYPE_NAME);
                }

                if (!empty($requestId)) {
                    $request = $entityManager->getReference(Request::class, $requestId);
                }

                if (empty($clientId)) {
                    return null;
                }

                if (isset($input['officeId'])) {
                    $officeId = RelayIdUtils::getId($input['officeId'], OfficeType::TYPE_NAME);
                }

                if (!empty($officeId)) {
                    $office = $entityManager->getReference(Office::class, $officeId);
                }

                if (empty($office)) {
                    return null;
                }

                /** @var Client $client */
                $client = $entityManager->find(Client::class, $clientId);

                if (empty($client)) {
                    return null;
                }

                if (empty($carId)) {
                    return null;
                }

                $car = $entityManager->getReference(Car::class, $carId);

                if (empty($car)) {
                    return null;
                }

                $isClientVisitRequired = $input['isClientVisitRequired'];

                $newOrder = new Order();
                $newOrder->setFirm($me->getFirm());
                $newOrder->setClient($client);
                $newOrder->setCar($car);
                $newOrder->setOffice($office);
                $newOrder->setClientVisitRequired($isClientVisitRequired);
                $newOrder->setActive(true);
                if (!empty($request)) {
                    $newOrder->setRequest($request);
                }

                if (isset($input['phoneSameAsInProfile']) && $input['phoneSameAsInProfile'] === true) {
                    $newOrder->setContactName($client->getFullName());
                    $newOrder->setContactPhone($client->getRegistrationPhone());
                    $newOrder->setPhoneSameAsInProfile(true);
                    $newOrder->setContactNote(null);
                } else {
                    $newOrder->setPhoneSameAsInProfile(false);
                    $contactName = strval($input['contactName']);
                    $contactPhone = strval($input['contactPhone']);
                    $contactNote = strval($input['contactNote']);

                    $newOrder->setContactName($contactName);
                    $newOrder->setContactPhone($contactPhone);
                    $newOrder->setContactNote($contactNote);
                }

                $orderReason = strval($input['orderReason']);
                if ($orderReason === '') {
                    throw new \Exception('Wrong orderReason was provided.');
                }

                $newOrder->setOrderReason($orderReason);

                $createdServiceWorks = [];

                if (!$isClientVisitRequired && isset($input['serviceWorks'])) {
                    $serviceWorksInput = $input['serviceWorks'];
                    if (!empty($serviceWorksInput)) {
                        foreach ($serviceWorksInput as $serviceWorkInput) {
                            $serviceWork = new ServiceWork();
                            $serviceWork->setOrder($newOrder);

                            $serviceWorkActionTypeId = RelayIdUtils::getId($serviceWorkInput['serviceWorkActionTypeId'], ServiceWorkActionTypeType::TYPE_NAME);
                            $serviceWorkActionType = $entityManager->getReference(ServiceWorkActionType::class, $serviceWorkActionTypeId);
                            if (empty($serviceWorkActionType)) {
                                return null;
                            }
                            $serviceWork->setServiceWorkActionType($serviceWorkActionType);

                            $sparePartNodeId = RelayIdUtils::getId($serviceWorkInput['sparePartNodeId'], SparePartNodeType::TYPE_NAME);
                            $sparePartNode = $entityManager->getReference(SparePartNode::class, $sparePartNodeId);
                            if (empty($sparePartNode)) {
                                return null;
                            }
                            $serviceWork->setSparePartNode($sparePartNode);

                            $serviceWork->setAmount($serviceWorkInput['amount']);
                            $serviceWork->setCostPerUnit($serviceWorkInput['costPerUnit']);
                            $serviceWork->setDiscountPercent(floatval($serviceWorkInput['discountPercent']));
                            $serviceWork->setNote($serviceWorkInput['name']);

                            $totalCost = (floatval($serviceWorkInput['costPerUnit']) * floatval($serviceWorkInput['amount'])) * ((100 - floatval($serviceWorkInput['discountPercent'])) / 100);
                            $serviceWork->setTotalCost($totalCost);

                            $serviceWork->setStatus(ServiceWorkStatusEnumType::SERVICE_WORK_WAIT_WORK_START_VALUE);

                            if (isset($serviceWorkInput['masterId']) && !empty($serviceWorkInput['masterId'])) {
                                $masterId = RelayIdUtils::getId($serviceWorkInput['masterId'], ManagerType::TYPE_NAME);
                                $master = $entityManager->getReference(Manager::class, $masterId);
                                if (empty($master)) {
                                    throw new \Exception('Cant find provided master');
                                }
                                $serviceWork->setMaster($master);
                            } else {
                                $serviceWork->setMaster(null);
                            }

                            $serviceWork->setLaborHour(floatval($serviceWorkInput['laborHour']));
                            $newOrder->addServiceWork($serviceWork);
                            $entityManager->persist($serviceWork);
                            $createdServiceWorks[$serviceWorkInput['tempId']] = $serviceWork;
                        }
                    }
                }

                if (!$isClientVisitRequired && isset($input['serviceMaterials'])) {
                    $serviceMaterialsInput = $input['serviceMaterials'];
                    if (!empty($serviceMaterialsInput)) {
                        foreach ($serviceMaterialsInput as $serviceMaterialInput) {
                            $serviceMaterial = new ServiceMaterial();
                            $serviceMaterial->setOrder($newOrder);

                            $unitMeasureTypeId = RelayIdUtils::getId($serviceMaterialInput['unitMeasureTypeId'], UnitMeasureTypeType::TYPE_NAME);
                            $unitMeasureType = $entityManager->getReference(UnitMeasureType::class, $unitMeasureTypeId);
                            if (empty($unitMeasureType)) {
                                return null;
                            }
                            $serviceMaterial->setUnitMeasureType($unitMeasureType);

                            $totalCost = (floatval($serviceMaterialInput['costPerUnit']) * floatval($serviceMaterialInput['amount'])) * ((100 - floatval($serviceMaterialInput['discountPercent'])) / 100);

                            $serviceMaterial->setAmount($serviceMaterialInput['amount']);
                            $serviceMaterial->setCostPerUnit($serviceMaterialInput['costPerUnit']);
                            $serviceMaterial->setDiscountPercent(floatval($serviceMaterialInput['discountPercent']));
                            $serviceMaterial->setName($serviceMaterialInput['name']);
                            $serviceMaterial->setTotalCost($totalCost);

                            $newOrder->addServiceMaterial($serviceMaterial);
                            $entityManager->persist($serviceMaterial);
                        }
                    }
                }

                if (isset($input['estimatedClientArrivalDate']) && !empty($input['estimatedClientArrivalDate'])) {
                    $estimatedClientArrivalDate = \DateTime::createFromFormat('U', $input['estimatedClientArrivalDate']);

                    if ($estimatedClientArrivalDate) {
                        $newOrder->setEstimatedClientArrivalDate($estimatedClientArrivalDate);
                    }
                }

                if (isset($input['boxId']) && !empty($input['boxId'])) {
                    $boxId = RelayIdUtils::getId($input['boxId'], OfficeBoxType::TYPE_NAME);
                }

                if (!empty($boxId)) {
                    /** @var OfficeBox $box */
                    $box = $entityManager->find(OfficeBox::class, $boxId);

                    if (empty($box)) {
                        return null;
                    }
                }

                if (isset($input['schedules'])) {
                    $schedulesInputs = $input['schedules'];
                    if (!empty($schedulesInputs)) {
                        foreach ($schedulesInputs as $schedulesInput) {
                            $schedule = new BoxSchedule();

                            $schedule->setOrder($newOrder);

                            $boxId = RelayIdUtils::getId($schedulesInput['boxId'], OfficeBoxType::TYPE_NAME);
                            $box = $entityManager->getReference(OfficeBox::class, $boxId);
                            if (empty($box)) {
                                throw new \Exception('Wrong box was provided');
                            }
                            $schedule->setBox($box);

                            $startInput = $schedulesInput['start'];
                            $start = DateTime::createFromFormat('U', $startInput);
                            $schedule->setStart($start);

                            $finishInput = $schedulesInput['finish'];
                            $finish = DateTime::createFromFormat('U', $finishInput);
                            $schedule->setFinish($finish);

                            $schedule->setStatus(ScheduleStatusEnumType::SCHEDULE_WAIT_WORK_START_VALUE);
                            $newOrder->addSchedule($schedule);
                            if (isset($schedulesInput['serviceWorks'])) {
                                $serviceWorks = $schedulesInput['serviceWorks'];
                                foreach ($serviceWorks as $serviceWorkTempId) {
                                    /** @var ServiceWork $serviceWork */
                                    $serviceWork = $createdServiceWorks[$serviceWorkTempId];
                                    if (!empty($serviceWork)) {
                                        $schedule->addServiceWork($serviceWork);
                                        $newOrder->addServiceWork($serviceWork);
                                    }
                                }
                            }

                            $entityManager->persist($schedule);
                        }
                    }
                }

                $entityManager->getConnection()->beginTransaction();
                try {
                    $newOrder->setStatus(OrderStatusEnumType::WAIT_CAR_ARRIVE_VALUE);
                    $entityManager->persist($newOrder);
                    $entityManager->flush();

                    /** @var GetOrderStatusForCurrentEntityStateService $getOrderStatusForCurrentEntityStateService */
                    $getOrderStatusForCurrentEntityStateService = $serviceManager->get('GetOrderStatusForCurrentEntityStateService');
                    $newOrder->setStatus($getOrderStatusForCurrentEntityStateService->getStatus($newOrder));
                    $entityManager->persist($newOrder);
                    $entityManager->flush();
                    $entityManager->getConnection()->commit();
                } catch (Exception $e) {
                    $entityManager->getConnection()->rollBack();
                    throw $e;
                }

                /** @var EventLogService $eventLogService */
                $eventLogService = $serviceManager->get(EventLogService::class);
                $eventLogService->logOrderCreate($newOrder);

                if (!empty($client->getRegistrationEmail()) || !empty($client->getRegistrationPhone())) {
                    /** @var ClientAutoLoginLinkGenerator $clientAutoLoginLinkGenerator */
                    $clientAutoLoginLinkGenerator = $serviceManager->get(ClientAutoLoginLinkGenerator::class);

                    $link = $clientAutoLoginLinkGenerator->generate($client);

                    $login = null;
                    $newPassword = null;
                    if (!$client->getInviteSent()) {
                        $login = $client->getRegistrationEmail();
                        if (!empty($client->getRegistrationPhone())) {
                            $login = $client->getRegistrationPhone();
                        }

                        /** @var PasswordGenerator $passwordGenerator */
                        $passwordGenerator = $serviceManager->get(PasswordGenerator::class);
                        $newPassword = $passwordGenerator->generateStrongPassword();

                        $bcrypt = new Bcrypt();
                        $bcrypt->setCost(10);
                        $passwordHash = $bcrypt->create($newPassword);
                        $client->setPassword($passwordHash);
                        $entityManager->persist($client);
                        $entityManager->flush();
                    }

                    /** @var MailService $mailService */
                    $mailService = $serviceManager->get(MailService::class);

                    if (!empty($client->getRegistrationEmail())) {
                        $mailService->sendToClientAboutNewOrder(
                            $client->getRegistrationEmail(),
                            $client->getFullName(),
                            $newOrder->getId(),
                            $link,
                            $login,
                            $newPassword
                        );

                        if (!$client->getInviteSent()) {
                            $client->setInviteSent(true);
                            $entityManager->persist($client);
                            $entityManager->flush();
                        }
                    }

                    if (!empty($client->getRegistrationPhone())) {
                        /** @var SMSService $smsService */
                        $smsService = $serviceManager->get(SMSService::class);

                        $message = 'Создан заказ SW-' . $newOrder->getId() . '
Для отслеживания перейдите по ссылке ' . $link;
                        $phone = '7' . $client->getRegistrationPhone();
                        if (!empty($login) && !empty($newPassword)) {
                            $message = $message . '
логин ' . $login . '
пароль ' . $newPassword;
                        }
                        $result = $smsService->sendSms($phone, $message);

                        $smsId = isset($result['smsId']) ? $result['smsId'] : null;
                        $cost = isset($result['cost']) ? $result['cost'] : null;

                        if (empty($smsId)) {
                            throw new \Exception('Sms send error.');
                        }

                        /** @var \Zend\Authentication\AuthenticationService $authenticationService */
                        $authenticationService = $serviceManager->get('AuthenticationService');
                        $authUser = $authenticationService->getIdentity();

                        $smsLog = new SmsLogRecord();
                        $smsLog->setType(SmsLogRecord::CLIENT_NOTIFY_ORDER_CREATED);
                        $smsLog->setAddressee($client);
                        $smsLog->setInitiator($authUser);
                        $smsLog->setSmsId($smsId);
                        $smsLog->setSmsProvider($smsService->getDefaultSmsProvider());
                        $smsLog->setCost($cost);
                        $smsLog->setMessage($message);
                        $smsLog->setPhone($client->getRegistrationPhone());

                        if (!$client->getInviteSent()) {
                            $client->setInviteSent(true);
                            $entityManager->persist($client);
                            $entityManager->flush();

                        }

                        $entityManager->persist($smsLog);

                        $entityManager->flush();
                    }
                };

                return [
                    'newOrder' => $newOrder,
                ];
            }
        ]);
    }
}
