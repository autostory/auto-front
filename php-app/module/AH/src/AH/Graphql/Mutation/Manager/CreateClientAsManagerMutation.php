<?php

namespace AH\GraphQL\Mutation\Manager;

use AH\Core\Acl\ClientAccess;
use AH\Core\Mail\MailService;
use AH\Core\Service\ClientAutoLoginLinkGenerator;
use AH\Core\Service\PasswordGenerator;
use AH\Core\SMS\SMSService;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\Car;
use AH\Entity\CarMileage;
use AH\Entity\Client;
use AH\Entity\Firm;
use AH\Entity\Log\SmsLogRecord;
use AH\Entity\Manager;
use AH\Entity\Role;
use AH\Entity\CatalogCar;
use AH\Entity\UserCommunication;
use AH\Graphql\Type\ClientType;
use AH\GraphQL\Type\CatalogCar as CatalogCarTypes;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Crypt\Password\Bcrypt;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\ServiceManager;

class CreateClientAsManagerMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'createClientAsManagerMutation',
            'inputFields' => [
                'email' => [
                    'type' => Type::string(),
                ],
                'phone' => [
                    'type' => Type::string(),
                ],
                'firstName' => [
                    'type' => Type::string(),
                ],
                'surname' => [
                    'type' => Type::string(),
                ],
                'patronymic' => [
                    'type' => Type::string(),
                ],
                'noCarAdd' => [
                    'type' => Type::nonNull(Type::boolean()),
                ],
                'sendEmailInvite' => [
                    'type' => Type::nonNull(Type::boolean()),
                ],
                'sendSMSInvite' => [
                    'type' => Type::nonNull(Type::boolean()),
                ],
                'vin' => [
                    'type' => Type::string(),
                ],
                'registrationSignNumber' => [
                    'type' => Type::string(),
                ],
                'color' => [
                    'type' => Type::string(),
                ],
                'year' => [
                    'type' => Type::int(),
                ],
                'mileage' => [
                    'type' => Type::int(),
                ],
                'catalogCarTypeId' => [
                    'type' => Type::id(),
                ],
                'catalogCarMarkId' => [
                    'type' => Type::id(),
                ],
                'catalogCarModelId' => [
                    'type' => Type::id(),
                ],
                'catalogCarGenerationId' => [
                    'type' => Type::id(),
                ],
                'catalogCarSerieId' => [
                    'type' => Type::id(),
                ],
                'catalogCarModificationId' => [
                    'type' => Type::id(),
                ],
                'catalogCarEquipmentId' => [
                    'type' => Type::id(),
                ],
            ],
            'outputFields' => [
                'errors' => [
                    'type' => Types::mutationValidationErrorMessages(),
                    'resolve' => function ($payload) {
                        return [
                            'messages' => isset($payload['errors']) ? $payload['errors'] : []
                        ];
                    },
                ],
                'clients' => [
                    'type' => Types::firmClients($serviceManager),
                    'resolve' => function ($payload) {
                        return isset($payload['myFirmClients']) ? ['clientsArray' => $payload['myFirmClients']] : null;
                    },
                ],
                'newClientId' => [
                    'type' => Type::nonNull(Type::id()),
                    'name' => 'newClientId',
                    'description' => 'Идентификатор нового клиента.',
                    'resolve' => function ($payload) {
                        if (!isset($payload['newClientEntity'])) {
                            return Relay::toGlobalId(ClientType::TYPE_NAME, 0);
                        }

                        /** @var Client $newClientEntity */
                        $newClientEntity = $payload['newClientEntity'];

                        return Relay::toGlobalId(ClientType::TYPE_NAME, $newClientEntity->getId());
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                /** @var \Zend\Authentication\AuthenticationService $authenticationService */
                $authenticationService = $serviceManager->get('AuthenticationService');

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                /** @var InputFilterInterface $identityFullNameFilter */
                $identityFullNameFilter = $serviceManager->get('IdentityFullNameFilter');

                /** @var InputFilterInterface $mobilePhoneInputFilter */
                $mobilePhoneInputFilter = $serviceManager->get('ClientRegistrationPhoneInputFilter');

                /** @var Manager $authUser */
                $authUser = $authenticationService->getIdentity();
                if (empty($authUser) || !($authUser instanceof Manager)) {
                    return null;
                }

                /** @var Firm $firm */
                $firm = $authUser->getFirm();
                if (empty($firm)) {
                    return null;
                }

                $newClient = new Client();
                $newClient->addFirm($firm);
                $firm->addClient($newClient);

                /** @var ClientAccess $clientAccess */
                $clientAccess = $serviceManager->get(ClientAccess::class);

                if (!$clientAccess->canCreateClient($newClient)) {
                    return null;
                }

                $validationErrors = [];
                $validationErrorMessages = [];

                $identityFullNameFilter->setData($input);
                if (!$identityFullNameFilter->isValid()) {
                    $validationErrorMessages = array_merge(
                        $validationErrorMessages,
                        $identityFullNameFilter->getMessages()
                    );
                }

                /** @var PasswordGenerator $passwordGenerator */
                $passwordGenerator = $serviceManager->get(PasswordGenerator::class);

                $newPassword = $passwordGenerator->generateStrongPassword();

                /** @var InputFilterInterface $clientAuthInputFilter */
                $clientAuthInputFilter = $serviceManager->get('ClientAuthInputFilter');
                $clientAuthInputFilter->setData(array_merge(
                    $input,
                    [
                        'newPassword' => $newPassword,
                        'newPasswordConfirm' => $newPassword,
                    ]
                ));

                if (isset($input['phone']) && strlen($input['phone']) > 0) {
                    $mobilePhoneInputFilter->setData(['mobilePhone' => $input['phone']]);
                    if (!$mobilePhoneInputFilter->isValid()) {
                        $validationErrorMessages = array_merge(
                            $validationErrorMessages,
                            $mobilePhoneInputFilter->getMessages()
                        );
                    }
                }

                if (!$clientAuthInputFilter->isValid()) {
                    $validationErrorMessages = $clientAuthInputFilter->getMessages();
                    if (!$clientAuthInputFilter->isValid()) {
                        $validationErrorMessages = array_merge(
                            $validationErrorMessages,
                            $mobilePhoneInputFilter->getMessages()
                        );
                    }
                }

                if (!empty($validationErrorMessages)) {
                    foreach ($validationErrorMessages as $fieldName => $errorMessages) {
                        foreach ($errorMessages as $errorType => $errorMessage) {
                            $validationErrors[] = [
                                'fieldName' => $fieldName,
                                'type' => $errorType,
                                'text' => $errorMessage,
                            ];
                        }
                    }

                    return ['errors' => $validationErrors];
                }

                $identityAuthData = $clientAuthInputFilter->getValues();
                $identityFullNameData = $identityFullNameFilter->getValues();

                if (!empty($identityAuthData['email'])) {
                    $newClient->setRegistrationEmail($identityAuthData['email']);
                }
                if (isset($input['phone']) && strlen($input['phone']) > 0) {
                    $mobilePhoneData = $mobilePhoneInputFilter->getValues();
                    $newClient->setRegistrationPhone($mobilePhoneData['mobilePhone']);
                }

                $newClient->setFirstName($identityFullNameData['firstName']);
                $newClient->setSurname($identityFullNameData['surname']);
                $newClient->setPatronymic($identityFullNameData['patronymic']);

                $bcrypt = new Bcrypt();
                $bcrypt->setCost(10);
                $passwordHash = $bcrypt->create($newPassword);

                $newClient->setPassword($passwordHash);

                $newClient->setActive(true);

                // А контактные данные надо подтвержить

                if (!empty($identityAuthData['email'])) {
                    $clientContactEmail = new UserCommunication();
                    // TODO: Надо как-то подтвержать эти контакты, а пока что просто true
                    $clientContactEmail->setActive(true);
                    $clientContactEmail->setValue($identityAuthData['email']);
                    $clientContactEmail->setType('email');
                    if (empty($mobilePhoneData['mobilePhone'])) {
                        // TODO: Надо принимать запрос какой способ связи предпочитаемый
                        $clientContactEmail->setPreferred(true);
                    }

                    $newClient->addCommunication($clientContactEmail);
                    $entityManager->persist($clientContactEmail);
                    $entityManager->persist($newClient);
                }

                if (!empty($mobilePhoneData['mobilePhone'])) {
                    $clientContactPhone = new UserCommunication();
                    // TODO: Надо как-то подтвержать эти контакты, а пока что просто true
                    $clientContactPhone->setActive(true);
                    $clientContactPhone->setValue($mobilePhoneData['mobilePhone']);
                    $clientContactPhone->setType('mobile');
                    $clientContactPhone->setPreferred(true);

                    $newClient->addCommunication($clientContactPhone);
                    $entityManager->persist($clientContactPhone);
                    $entityManager->persist($newClient);
                }

                if (!$input['noCarAdd']) {
                    if (!$clientAccess->canAddCarToClient($newClient)) {
                        return null;
                    }

                    $newCar = new Car();
                    $newCar->addOwner($newClient);
                    $newClient->addCar($newCar);

                    if (isset($input['color'])) {
                        $newCar->setColor(trim(strtolower($input['color'])));
                    }

                    if (isset($input['year'])) {
                        $newCar->setYear(intval($input['year']));
                    }

                    if (isset($input['mileage'])) {
                        $carMileage = new CarMileage();
                        $carMileage->setCar($newCar);
                        $carMileage->setMeasuredAt(new \DateTime());
                        $carMileage->setValue(intval($input['mileage']));
                        $newCar->addMillage($carMileage);
                        $entityManager->persist($carMileage);
                    }

                    $rCar = $entityManager->getRepository(Car::class);

                    $vin = null;
                    if (isset($input['vin'])) {
                        $vin = trim(strtolower($input['vin']));
                    }
                    if (!empty($vin)) {
                        $carWithThisVin = $rCar->findOneBy([
                            'vin' => $vin,
                        ]);

                        if (!empty($carWithThisVin)) {
                            return [
                                'errors' => [[
                                    'fieldName' => 'vin',
                                    'type' => 'Shit',
                                    'text' => 'Кто-то уже добавил автомобиль с таким VIN номером.',
                                ]]
                            ];
                        }

                        $newCar->setVin(trim(strtolower($input['vin'])));
                    }

                    $registrationSignNumber = null;
                    if (isset($input['registrationSignNumber'])) {
                        $registrationSignNumber = trim(strtolower($input['registrationSignNumber']));
                    }
                    if (empty($registrationSignNumber)) {
                        return [
                            'errors' => [[
                                'fieldName' => 'registrationSignNumber',
                                'type' => 'Shit',
                                'text' => 'Обязательно для заполнения.',
                            ]]
                        ];
                    }

                    $newCar->setRegistrationSignNumber($registrationSignNumber);

                    if (!empty($input['catalogCarTypeId'])) {
                        $carTypeId = RelayIdUtils::getId($input['catalogCarTypeId'], CatalogCarTypes\CatalogCarTypeType::TYPE_NAME);

                        $carType = $entityManager->find(CatalogCar\CarType::class, $carTypeId);
                        $newCar->setCatalogCarType($carType);
                    }

                    if (!empty($input['catalogCarMarkId'])) {
                        $carMarkId = RelayIdUtils::getId($input['catalogCarMarkId'], CatalogCarTypes\CatalogCarMarkType::TYPE_NAME);

                        $carMark = $entityManager->find(CatalogCar\CarMark::class, $carMarkId);
                        $newCar->setCatalogCarMark($carMark);
                    }

                    if (!empty($input['catalogCarModelId'])) {
                        $carModelId = RelayIdUtils::getId($input['catalogCarModelId'], CatalogCarTypes\CatalogCarModelType::TYPE_NAME);

                        $carModel = $entityManager->find(CatalogCar\CarModel::class, $carModelId);
                        $newCar->setCatalogCarModel($carModel);
                    }

                    if (!empty($input['catalogCarSerieId'])) {
                        $carSerieId = RelayIdUtils::getId($input['catalogCarSerieId'], CatalogCarTypes\CatalogCarSerieType::TYPE_NAME);

                        /** @var CatalogCar\CarSerie $carSerie */
                        $carSerie = $entityManager->find(CatalogCar\CarSerie::class, $carSerieId);

                        $newCar->setCatalogCarSerie($carSerie);
                    }

                    if (!empty($input['catalogCarGenerationId'])) {
                        $catalogCarGenerationId = RelayIdUtils::getId($input['catalogCarGenerationId'], CatalogCarTypes\CatalogCarGenerationType::TYPE_NAME);

                        /** @var CatalogCar\CarGeneration $carGeneration */
                        $carGeneration = $entityManager->find(CatalogCar\CarGeneration::class, $catalogCarGenerationId);

                        $newCar->setCatalogCarGeneration($carGeneration);
                    }

                    if (!empty($input['catalogCarModificationId'])) {
                        $catalogCarModificationId = RelayIdUtils::getId($input['catalogCarModificationId'], CatalogCarTypes\CatalogCarModificationType::TYPE_NAME);

                        /** @var CatalogCar\CarModification $carModification */
                        $carModification = $entityManager->find(CatalogCar\CarModification::class, $catalogCarModificationId);

                        $newCar->setCatalogCarModification($carModification);
                    }

                    if (!empty($input['catalogCarEquipmentId'])) {
                        $catalogCarEquipmentId = RelayIdUtils::getId($input['catalogCarEquipmentId'], CatalogCarTypes\CatalogCarEquipmentType::TYPE_NAME);

                        /** @var CatalogCar\CarEquipment $catalogCarEquipment */
                        $catalogCarEquipment = $entityManager->find(CatalogCar\CarEquipment::class, $catalogCarEquipmentId);

                        $newCar->setCatalogCarEquipment($catalogCarEquipment);
                    }

                    $entityManager->persist($newCar);
                }

                $rRole = $entityManager->getRepository(Role::class);
                /** @var Role $clientRole */
                $clientRole = $rRole->findOneBy(['roleId' => 'client']);
                $newClient->addRole($clientRole);

                $entityManager->persist($newClient);
                $entityManager->persist($firm);
                $entityManager->flush();

                $login = $newClient->getRegistrationEmail();
                if (!empty($newClient->getRegistrationPhone())) {
                    $login = $newClient->getRegistrationPhone();
                }

                // Отсылаем клиенту уведомление о созданном аккаунте если такое запросили
                /** @var MailService $mailService */
                $mailService = $serviceManager->get(MailService::class);

                $sendEmailInvite = $input['sendEmailInvite'];
                $sendSMSInvite = $input['sendSMSInvite'];

                if (
                    (!empty($newClient->getRegistrationEmail()) && $sendEmailInvite)
                    || (!empty($newClient->getRegistrationPhone()) && $sendSMSInvite)
                ) {
                    /** @var ClientAutoLoginLinkGenerator $clientAutoLoginLinkGenerator */
                    $clientAutoLoginLinkGenerator = $serviceManager->get(ClientAutoLoginLinkGenerator::class);

                    $link = $clientAutoLoginLinkGenerator->generate($newClient);
                }

                if (!empty($newClient->getRegistrationEmail()) && $sendEmailInvite) {
                    $mailService->sendRegistrationEmailToClient(
                        $newClient->getRegistrationEmail(),
                        $newClient->getFullName(),
                        $login,
                        $newPassword,
                        $link
                    );

                    $newClient->setInviteSent(true);
                    $entityManager->persist($newClient);
                    $entityManager->flush();
                }

                if (!empty($newClient->getRegistrationPhone()) && $sendSMSInvite) {
                    /** @var SMSService $smsService */
                    $smsService = $serviceManager->get(SMSService::class);

                    $phone = '7' . $newClient->getRegistrationPhone();
                    $message = $link . '
логин ' . $login . '
пароль ' . $newPassword;
                    $result = $smsService->sendSms($phone, $message);

                    $smsId = isset($result['smsId']) ? $result['smsId'] : null;
                    $cost = isset($result['cost']) ? $result['cost'] : null;

                    if (empty($smsId)) {
                        throw new \Exception('Sms send error.');
                    }

                    $smsLog = new SmsLogRecord();
                    $smsLog->setType(SmsLogRecord::CLIENT_REGISTRATION_TYPE);
                    $smsLog->setAddressee($newClient);
                    $smsLog->setInitiator($authUser);
                    $smsLog->setSmsId($smsId);
                    $smsLog->setSmsProvider($smsService->getDefaultSmsProvider());
                    $smsLog->setCost($cost);
                    $smsLog->setMessage($message);
                    $smsLog->setPhone($newClient->getRegistrationPhone());

                    $newClient->setInviteSent(true);

                    $entityManager->persist($smsLog);
                    $entityManager->persist($newClient);
                    $entityManager->flush();
                }

                /** @var \AH\Repository\ClientRepository $rClient */
                $rClient = $entityManager->getRepository('AH\Entity\Client');
                // TODO: Передать сюда агрументы, иначе список пользователь потличается от текущего.
                $clientsArray = $rClient->findFirmClientByFilter($firm);

                return [
                    'myFirmClients' => $clientsArray,
                    'newClientEntity' => $newClient,
                ];
            }
        ]);
    }
}
