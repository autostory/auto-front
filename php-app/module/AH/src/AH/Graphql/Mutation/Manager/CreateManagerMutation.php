<?php

namespace AH\GraphQL\Mutation\Manager;

use AH\Core\Acl\FirmAccess;
use AH\Core\Mail\MailService;
use AH\Core\Service\PasswordGenerator;
use AH\Core\SMS\SMSService;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\Firm;
use AH\Entity\Log\SmsLogRecord;
use AH\Entity\Manager;
use AH\Entity\Role;
use AH\Graphql\Type\FirmType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Authentication\AuthenticationService;
use Zend\Crypt\Password\Bcrypt;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\ServiceManager;

class CreateManagerMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        /** @var FirmAccess $firmAccessService */
        $firmAccessService = $serviceManager->get(FirmAccess::class);

        /** @var AuthenticationService $authenticationService */
        $authenticationService = $serviceManager->get('AuthenticationService');

        return Relay::mutationWithClientMutationId([
            'name' => 'createManagerMutation',
            'inputFields' => [
                'email' => [
                    'type' => Type::string(),
                ],
                'phone' => [
                    'type' => Type::string(),
                ],
                'firmId' => [
                    'type' => Type::nonNull(Type::string()),
                ],
                'firstName' => [
                    'type' => Type::string(),
                ],
                'surname' => [
                    'type' => Type::string(),
                ],
                'patronymic' => [
                    'type' => Type::string(),
                ],
            ],
            'outputFields' => [
                'errors' => [
                    'type' => Types::mutationValidationErrorMessages(),
                    'resolve' => function ($payload) {
                        return [
                            'messages' => isset($payload['errors']) ? $payload['errors'] : []
                        ];
                    },
                ],
                'manager' => [
                    'type' => Types::managerType($serviceManager),
                    'description' => 'Созданный менеджер.',
                    'resolve' => function ($payload) {
                        return isset($payload['manager']) ? $payload['manager'] : null;
                    },
                ],
                'myFirm' => [
                    'type' => Types::firmType($serviceManager),
                    'description' => 'Моя фирма.',
                    'resolve' => function ($payload) use ($firmAccessService, $authenticationService) {
                        $me = $authenticationService->getIdentity();

                        if (
                            !empty($me)
                            && $me instanceof Manager
                            && $firmAccessService->canViewFirm($me->getFirm())
                        ) {
                            return $me->getFirm();
                        }

                        return null;
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                /** @var \Zend\Authentication\AuthenticationService $authenticationService */
                $authenticationService = $serviceManager->get('AuthenticationService');

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                /** @var InputFilterInterface $identityFullNameFilter */
                $identityFullNameFilter = $serviceManager->get('IdentityFullNameFilter');

                /** @var InputFilterInterface $mobilePhoneInputFilter */
                $mobilePhoneInputFilter = $serviceManager->get('ManagerRegistrationPhoneInputFilter');

                /** @var SMSService $smsService */
                $smsService = $serviceManager->get(SMSService::class);

                /** @var Manager $authUser */
                $authUser = $authenticationService->getIdentity();
                if (empty($authUser) || !($authUser instanceof Manager)) {
                    return null;
                }

                /** @var Firm $firm */
                $firm = $authUser->getFirm();
                if (empty($firm)) {
                    return null;
                }

                $firmId = RelayIdUtils::getId($input['firmId'], FirmType::TYPE_NAME);
                if ($firm->getId() !== $firmId) {
                    throw new \Exception('У вас нет доступа до этой фирмы.');
                }

                $newManager = new Manager();
                $newManager->setFirm($firm);
                $firm->addManager($newManager);

                $validationErrors = [];
                $validationErrorMessages = [];

                $identityFullNameFilter->setData($input);
                if (!$identityFullNameFilter->isValid()) {
                    $validationErrorMessages = array_merge(
                        $validationErrorMessages,
                        $identityFullNameFilter->getMessages()
                    );
                }

                /** @var PasswordGenerator $passwordGenerator */
                $passwordGenerator = $serviceManager->get(PasswordGenerator::class);

                $newPassword = $passwordGenerator->generateStrongPassword();

                /** @var InputFilterInterface $managerAuthInputFilter */
                $managerAuthInputFilter = $serviceManager->get('ManagerAuthInputFilter');
                $managerAuthInputFilter->setData(array_merge(
                    $input,
                    [
                        'newPassword' => $newPassword,
                        'newPasswordConfirm' => $newPassword,
                    ]
                ));

                if (isset($input['phone']) && strlen($input['phone']) > 0) {
                    $mobilePhoneInputFilter->setData(['mobilePhone' => $input['phone']]);
                    if (!$mobilePhoneInputFilter->isValid()) {
                        $validationErrorMessages = array_merge(
                            $validationErrorMessages,
                            $mobilePhoneInputFilter->getMessages()
                        );
                    }
                }

                if (!$managerAuthInputFilter->isValid()) {
                    $validationErrorMessages = $managerAuthInputFilter->getMessages();
                    if (!$managerAuthInputFilter->isValid()) {
                        $validationErrorMessages = array_merge(
                            $validationErrorMessages,
                            $mobilePhoneInputFilter->getMessages()
                        );
                    }
                }

                if (!empty($validationErrorMessages)) {
                    foreach ($validationErrorMessages as $fieldName => $errorMessages) {
                        foreach ($errorMessages as $errorType => $errorMessage) {
                            $validationErrors[] = [
                                'fieldName' => $fieldName,
                                'type' => $errorType,
                                'text' => $errorMessage,
                            ];
                        }
                    }

                    return ['errors' => $validationErrors];
                }

                $identityAuthData = $managerAuthInputFilter->getValues();
                $identityFullNameData = $identityFullNameFilter->getValues();

                if (!empty($identityAuthData['email'])) {
                    $newManager->setRegistrationEmail($identityAuthData['email']);
                }

                if (isset($input['phone']) && strlen($input['phone']) > 0) {
                    $mobilePhoneData = $mobilePhoneInputFilter->getValues();
                    $newManager->setRegistrationPhone($mobilePhoneData['mobilePhone']);
                }

                $newManager->setFirstName($identityFullNameData['firstName']);
                $newManager->setSurname($identityFullNameData['surname']);
                $newManager->setPatronymic($identityFullNameData['patronymic']);

                $bcrypt = new Bcrypt();
                $bcrypt->setCost(10);
                $passwordHash = $bcrypt->create($newPassword);

                $newManager->setPassword($passwordHash);

                $newManager->setActive(true);

                $rRole = $entityManager->getRepository(Role::class);
                /** @var Role $managerRole */
                $managerRole = $rRole->findOneBy(['roleId' => 'manager']);
                $newManager->addRole($managerRole);

                $newManager->setInviteSent(true);

                $entityManager->persist($newManager);
                $entityManager->persist($firm);
                $entityManager->flush();

                $login = $newManager->getRegistrationEmail();
                if (!empty($newManager->getRegistrationPhone())) {
                    $login = $newManager->getRegistrationPhone();
                }

                // Отсылаем менеджеру уведомление о созданном аккаунте
                /** @var MailService $mailService */
                $mailService = $serviceManager->get(MailService::class);
                if (!empty($newManager->getRegistrationEmail())) {
                    $mailService->sendEmailWithAuthDataToNewManager(
                        $newManager->getRegistrationEmail(),
                        $newManager->getFullName(),
                        $login,
                        $newPassword
                    );
                } else if (!empty($newManager->getRegistrationPhone())) {
                    $phone = '7' . $newManager->getRegistrationPhone();
                    $message = 'сайт ' . $_SERVER['HTTP_HOST'] . '
логин ' . $login . '
пароль ' . $newPassword;
                    $result = $smsService->sendSms($phone, $message);

                    $smsId = isset($result['smsId']) ? $result['smsId'] : null;
                    $cost = isset($result['cost']) ? $result['cost'] : null;

                    if (empty($smsId)) {
                        throw new \Exception('Sms send error.');
                    }

                    $smsLog = new SmsLogRecord();
                    $smsLog->setType(SmsLogRecord::MANAGER_REGISTRATION_TYPE);
                    $smsLog->setAddressee($newManager);
                    $smsLog->setInitiator($authUser);
                    $smsLog->setSmsId($smsId);
                    $smsLog->setSmsProvider($smsService->getDefaultSmsProvider());
                    $smsLog->setCost($cost);
                    $smsLog->setMessage($message);
                    $smsLog->setPhone($newManager->getRegistrationPhone());
                    $entityManager->persist($smsLog);
                    $entityManager->flush($smsLog);
                }

                return [
                    'manager' => $newManager,
                ];
            }
        ]);
    }
}
