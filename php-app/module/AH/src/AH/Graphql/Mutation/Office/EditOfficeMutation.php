<?php

namespace AH\GraphQL\Mutation\Office;

use AH\Core\Acl\OfficeAccess;
use AH\Core\Service\EventLogService;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\Manager;
use AH\Entity\Office;
use AH\Entity\OfficeWorkTime;
use AH\GraphQL\Inputs;
use AH\Graphql\Type\ManagerType;
use AH\Graphql\Type\OfficeType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class EditOfficeMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'editOfficeMutation',
            'inputFields' => [
                'officeId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'title' => [
                    'type' => Type::string(),
                ],
                'workTimes' => [
                    'type' => Type::listOf(Inputs::officeWorkTimeInputType()),
                ],
                'address' => [
                    'type' => Type::string(),
                ],
                'phone' => [
                    'type' => Type::string(),
                ],
                'active' => [
                    'type' => Type::boolean(),
                ],
                'officeMangers' => [
                    'type' => Type::listOf(Type::nonNull(Type::ID()))
                ],
            ],
            'outputFields' => [
                'office' => [
                    'type' => Types::officeType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['office'];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                if (!isset($input['officeId'])) {
                    return null;
                }

                $officeId = RelayIdUtils::getId($input['officeId'], OfficeType::TYPE_NAME);
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                $office = $entityManager->find(Office::class, $officeId);

                if (empty($office)) {
                    throw new \Exception('Can`t find office.');
                }

                /** @var OfficeAccess $officeAccess */
                $officeAccess = $serviceManager->get(OfficeAccess::class);

                /** @var Office $office */
                if (!$officeAccess->canEditOffice($office)) {
                    throw new \Exception('Access denied for this office.');
                }

                if (isset($input['title'])) {
                    $office->setTitle(trim($input['title']));
                }

                if (isset($input['workTimeDescription'])) {
                    $office->setWorkTimeDescription(trim($input['workTimeDescription']));
                }

                if (isset($input['address'])) {
                    $office->setAddress(trim($input['address']));
                }

                if (isset($input['phone'])) {
                    $office->setPhone(trim($input['phone']));
                }

                if (isset($input['active'])) {
                    $office->setActive(boolval($input['active']));
                }

                if (isset($input['officeMangers'])) {
                    $currentManagers = $office->getManagers();
                    if (!empty($currentManagers)) {
                        /** @var Manager $currentManager */
                        foreach ($currentManagers as $currentManager) {
                            $currentManager->removeOffice($office);
                            $office->removeManager($currentManager);
                            $entityManager->persist($currentManager);
                        }
                    }

                    if (!empty($input['officeMangers'])) {
                        $rManagers = $entityManager->getRepository(Manager::class);
                        foreach ($input['officeMangers'] as $newManagerRelayId) {
                            $idComponents = Relay::fromGlobalId($newManagerRelayId);

                            if (
                                !isset($idComponents['type'])
                                || mb_strlen($idComponents['type']) === 0
                                || !isset($idComponents['id'])
                                || intval($idComponents['id']) <= 0
                                || $idComponents['type'] !== ManagerType::TYPE_NAME
                            ) {
                                throw new \Exception('Wrong id was provided.');
                            }

                            $managerId = intval($idComponents['id']);

                            /** @var Manager $newManager */
                            $newManager = $rManagers->find($managerId);

                            if (empty($newManager) || $newManager->getFirm()->getId() !== $office->getFirm()->getId()) {
                                throw new \Exception('Wrong manager was provided.');
                            }

                            $newManager->addOffice($office);
                            $office->addManager($newManager);
                            $entityManager->persist($newManager);
                        }
                    }
                }

                if (isset($input['workTimes']) && !empty($input['workTimes'])) {
                    $workTimes = $input['workTimes'];
                    $rOfficeWorkTime = $entityManager->getRepository(OfficeWorkTime::class);

                    foreach ($workTimes as $workTimeInput) {
                        $officeId = RelayIdUtils::getId($workTimeInput['officeId'], OfficeType::TYPE_NAME);
                        if ($officeId !== $office->getId()) {
                            throw new \Exception('Wrong officeId was provided.');
                        }

                        $workTime = $rOfficeWorkTime->findOneBy([
                            'office' => $office,
                            'dayOfWeek' => $workTimeInput['dayOfWeek'],
                        ]);

                        if (empty($workTime)) {
                            $workTime = new OfficeWorkTime();
                            $workTime->setDayOfWeek($workTimeInput['dayOfWeek']);
                            $office->addWorkTime($workTime);
                            $workTime->setOffice($office);
                        }

                        $workTime->setStart(null);
                        if (!empty($workTimeInput['start'])) {
                            $start = new \DateTime();
                            $parts = explode(':', $workTimeInput['start']);
                            $start->setTime($parts[0], $parts[1]);
                            $workTime->setStart($start);
                        }

                        $workTime->setEnd(null);
                        if (!empty($workTimeInput['end'])) {
                            $end = new \DateTime();
                            $parts = explode(':', $workTimeInput['end']);
                            $end->setTime($parts[0], $parts[1]);
                            $workTime->setEnd($end);
                        }

                        $workTime->setHoliday($workTimeInput['holiday']);
                        $workTime->setRoundTheClock($workTimeInput['roundTheClock']);

                        $entityManager->persist($workTime);
                    }
                }

                $entityManager->persist($office);
                $entityManager->flush();

                /** @var EventLogService $eventLogService */
                $eventLogService = $serviceManager->get(EventLogService::class);
                $eventLogService->logOfficeEdit($office);

                return [
                    'office' => $office,
                ];
            }
        ]);
    }
}
