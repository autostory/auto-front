<?php

namespace AH\GraphQL\Mutation\Office;

use AH\Core\Acl\OfficeAccess;
use AH\Core\Service\EventLogService;
use AH\Entity\Firm;
use AH\Entity\Office;
use AH\Graphql\Type\FirmType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class CreateOfficeMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'createOfficeMutation',
            'inputFields' => [
                'title' => [
                    'type' => Type::string(),
                ],
                'address' => [
                    'type' => Type::string(),
                ],
                'phone' => [
                    'type' => Type::string(),
                ],
                'firmId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
            ],
            'outputFields' => [
                'newOffice' => [
                    'type' => Types::officeType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['newOffice'];
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                if (!isset($input['firmId'])) {
                    return null;
                }

                $idComponents = Relay::fromGlobalId($input['firmId']);

                if (
                    !isset($idComponents['type'])
                    || mb_strlen($idComponents['type']) === 0
                    || !isset($idComponents['id'])
                    || intval($idComponents['id']) <= 0
                ) {
                    return null;
                }

                $type = $idComponents['type'];
                $id = intval($idComponents['id']);

                /** @var OfficeAccess $officeAccess */
                $officeAccess = $serviceManager->get(OfficeAccess::class);

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');
                if ($type === FirmType::TYPE_NAME && $id > 0) {
                    /** @var Firm $firm */
                    $firm = $entityManager->find(Firm::class, $id);
                    if (empty($firm) || !$officeAccess->canCreateOffice($firm)) {
                        return null;
                    }
                }

                if (empty($firm)) {
                    return null;
                }

                $newOffice = new Office();
                $newOffice->setFirm($firm);

                if (isset($input['title'])) {
                    $newOffice->setTitle(trim($input['title']));
                }

                if (isset($input['address'])) {
                    $newOffice->setAddress(trim($input['address']));
                }

                if (isset($input['phone'])) {
                    $newOffice->setPhone(trim($input['phone']));
                }

                $entityManager->persist($newOffice);
                $entityManager->flush();

                /** @var EventLogService $eventLogService */
                $eventLogService = $serviceManager->get(EventLogService::class);
                $eventLogService->logOfficeCreate($newOffice);

                return [
                    'newOffice' => $newOffice,
                ];
            }
        ]);
    }
}
