<?php

namespace AH\GraphQL\Mutation\RequestCommunications;

use AH\Core\Acl\ClientAccess;
use AH\Entity\Client;
use AH\Entity\Manager;
use AH\Entity\Request;
use AH\Entity\RequestCommunication;
use AH\Entity\User;
use AH\Graphql\Type\OfficeType;
use AH\Graphql\Type\RequestType;
use AH\GraphQL\Types;
use DateTime;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Authentication\AuthenticationService;
use Zend\Crypt\Password\Bcrypt;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Validator\EmailAddress;

class AddRequestCommunicationNoAnswerResultMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'addRequestCommunicationNoAnswerResult',
            'inputFields' => [
                'requestId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'timestamp' => [
                    'type' => Type::nonNull(Type::string()),
                    'description' => 'Дата и время следующего созвона в формате timestamp',
                ],
                'notifyClient' => [ // TODO: Пока что не используется
                    'type' => Type::boolean(),
                    'description' => 'Уведомить клиента о том что до него не дозвонились',
                ],
            ],
            'outputFields' => [
                'request' => [
                    'type' => Types::requestType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['request'];
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                /** @var AuthenticationService $authenticationService */
                $authenticationService = $serviceManager->get('AuthenticationService');
                /** @var User $me */
                $me = $authenticationService->getIdentity();

                if (empty($me) || !($me instanceof Manager)) {
                    throw new \Exception('You must login first.');
                }

                if (!isset($input['requestId'])) {
                    return null;
                }

                if (!isset($input['timestamp']) || empty($input['timestamp'])) {
                    return null;
                }

                $nextCommunicationDateTime = DateTime::createFromFormat('U', $input['timestamp']);

                if (!$nextCommunicationDateTime) {
                    return null;
                }

                $idComponents = Relay::fromGlobalId($input['requestId']);

                if (
                    !isset($idComponents['type'])
                    || mb_strlen($idComponents['type']) === 0
                    || !isset($idComponents['id'])
                    || intval($idComponents['id']) <= 0
                ) {
                    return null;
                }

                $type = $idComponents['type'];
                $id = intval($idComponents['id']);

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');
                if ($type === RequestType::TYPE_NAME && $id > 0) {
                    /** @var Request $request */
                    $request = $entityManager->find(Request::class, $id);
                    if (empty($request)) {
                        return null;
                    }
                }

                if (empty($request)) {
                    return null;
                }

                $newRC = new RequestCommunication();

                $newRC->setCommunicationDateTime(new DateTime());
                $newRC->setNextCommunicationDateTime($nextCommunicationDateTime);
                $newRC->setManager($me);
                $newRC->setRequest($request);
                $newRC->setResultId(3);

                $entityManager->persist($newRC);
                $entityManager->persist($request);
                $entityManager->flush();

                return [
                    'request' => $request,
                ];
            }
        ]);
    }
}
