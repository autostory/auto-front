<?php

namespace AH\GraphQL\Mutation\RequestCommunications;

use AH\Core\Service\GetOrderStatusForCurrentEntityStateService;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\Manager;
use AH\Entity\Office;
use AH\Entity\Order;
use AH\Entity\Request;
use AH\Entity\RequestCommunication;
use AH\Entity\User;
use AH\Graphql\Enum\OrderStatusEnumType;
use AH\Graphql\Type\OfficeType;
use AH\Graphql\Type\RequestType;
use AH\GraphQL\Types;
use DateTime;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\ServiceManager;

class AddRequestCommunicationVisitResultMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'addRequestCommunicationVisitResult',
            'inputFields' => [
                'requestId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'officeId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'estimatedClientArrivalDate' => [
                    'description' => 'timestamp',
                    'type' => Type::string(),
                ],
            ],
            'outputFields' => [
                'request' => [
                    'type' => Types::requestType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['request'];
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                /** @var AuthenticationService $authenticationService */
                $authenticationService = $serviceManager->get('AuthenticationService');
                /** @var User $me */
                $me = $authenticationService->getIdentity();

                if (empty($me) || !($me instanceof Manager)) {
                    throw new \Exception('You must login first.');
                }

                if (!isset($input['requestId'])) {
                    return null;
                }

                $requestId = RelayIdUtils::getId($input['requestId'], RequestType::TYPE_NAME);
                $officeId = RelayIdUtils::getId($input['officeId'], OfficeType::TYPE_NAME);

                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');
                /** @var Request $request */
                $request = $entityManager->find(Request::class, $requestId);
                $office = $entityManager->find(Office::class, $officeId);

                if (empty($request) || empty($office)) {
                    return null;
                }

                $newRC = new RequestCommunication();

                $newRC->setCommunicationDateTime(new DateTime());
                $newRC->setManager($me);
                $newRC->setRequest($request);
                $newRC->setResultId(1); // TODO: Тут должет быть Enum
                $request->setActive(false);

                $newOrder = new Order();
                $newOrder->setRequest($request);
                $newOrder->setStatus(OrderStatusEnumType::WAIT_WORK_ADD_VALUE);
                $newOrder->setActive(true);
                $newOrder->setPhoneSameAsInProfile(true);
                $newOrder->setOrderReason($request->getUserMessage());
                $newOrder->setCar($request->getCar());
                $newOrder->setClient($request->getClient());
                $newOrder->setFirm($request->getFirm());
                $newOrder->setOffice($office);
                $request->setOrder($newOrder);

                if (isset($input['estimatedClientArrivalDate']) && $input['estimatedClientArrivalDate'] !== null) {
                    if (!empty($input['estimatedClientArrivalDate'])) {
                        $estimatedClientArrivalDate = \DateTime::createFromFormat('U', $input['estimatedClientArrivalDate']);

                        if ($estimatedClientArrivalDate) {
                            $newOrder->setEstimatedClientArrivalDate($estimatedClientArrivalDate);
                        }
                    }
                }

                $entityManager->persist($newRC);
                $entityManager->persist($newOrder);
                $entityManager->persist($request);
                $entityManager->flush();

                /** @var GetOrderStatusForCurrentEntityStateService $getOrderStatusForCurrentEntityStateService */
                $getOrderStatusForCurrentEntityStateService = $serviceManager->get('GetOrderStatusForCurrentEntityStateService');
                $newOrder->setStatus($getOrderStatusForCurrentEntityStateService->getStatus($newOrder));

                $entityManager->persist($newOrder);
                $entityManager->flush();

                return [
                    'request' => $request,
                ];
            }
        ]);
    }
}
