<?php

namespace AH\GraphQL\Mutation\OfficeBox;

use AH\Core\Acl\OfficeBoxAccess;
use AH\Core\Service\EventLogService;
use AH\Entity\OfficeBox;
use AH\Graphql\Type\OfficeBoxType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class EditBoxMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'editBoxMutation',
            'inputFields' => [
                'boxId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'title' => [
                    'type' => Type::string(),
                ],
                'active' => [
                    'type' => Type::boolean(),
                ],
            ],
            'outputFields' => [
                'box' => [
                    'type' => Types::officeBoxType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['box'];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                if (!isset($input['boxId'])) {
                    return null;
                }

                $idComponents = Relay::fromGlobalId($input['boxId']);

                if (
                    !isset($idComponents['type'])
                    || mb_strlen($idComponents['type']) === 0
                    || !isset($idComponents['id'])
                    || intval($idComponents['id']) <= 0
                ) {
                    return null;
                }

                $type = $idComponents['type'];
                $id = intval($idComponents['id']);

                /** @var OfficeBoxAccess $officeBoxAccess */
                $officeBoxAccess = $serviceManager->get(OfficeBoxAccess::class);

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');
                if ($type === OfficeBoxType::TYPE_NAME && $id > 0) {
                    /** @var OfficeBox $box */
                    $box = $entityManager->find(OfficeBox::class, $id);
                    if (empty($box) || !$officeBoxAccess->canEditOfficeBox($box)) {
                        return null;
                    }
                }

                if (empty($box)) {
                    return null;
                }

                if (isset($input['title'])) {
                    $box->setTitle(trim($input['title']));
                }

                if (isset($input['active'])) {
                    $box->setActive(boolval($input['active']));
                }

                $entityManager->persist($box);
                $entityManager->flush();

                /** @var EventLogService $eventLogService */
                $eventLogService = $serviceManager->get(EventLogService::class);
                $eventLogService->logOfficeBoxEdit($box);

                return [
                    'box' => $box,
                ];
            }
        ]);
    }
}
