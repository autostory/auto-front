<?php

namespace AH\GraphQL\Mutation\OfficeBox;

use AH\Core\Acl\OfficeBoxAccess;
use AH\Core\Service\EventLogService;
use AH\Entity\Firm;
use AH\Entity\Office;
use AH\Entity\OfficeBox;
use AH\Graphql\Type\OfficeType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class CreateBoxMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'createBoxMutation',
            'inputFields' => [
                'title' => [
                    'type' => Type::nonNull(Type::string()),
                ],
                'officeId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
            ],
            'outputFields' => [
                'newOfficeBox' => [
                    'type' => Types::officeBoxType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['newOfficeBox'];
                    },
                ],
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                if (!isset($input['officeId'])) {
                    return null;
                }

                $idComponents = Relay::fromGlobalId($input['officeId']);

                if (
                    !isset($idComponents['type'])
                    || mb_strlen($idComponents['type']) === 0
                    || !isset($idComponents['id'])
                    || intval($idComponents['id']) <= 0
                ) {
                    return null;
                }

                $type = $idComponents['type'];
                $id = intval($idComponents['id']);

                /** @var OfficeBoxAccess $officeBoxAccess */
                $officeBoxAccess = $serviceManager->get(OfficeBoxAccess::class);

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                /** @var Office $office */
                $office = $entityManager->find(Office::class, $id);

                if (empty($office)) {
                    return null;
                }

                if ($type === OfficeType::TYPE_NAME && $id > 0) {
                    /** @var Firm $firm */
                    $firm = $office->getFirm();
                    if (empty($firm) || !$officeBoxAccess->canCreateOfficeBox($office)) {
                        return null;
                    }
                }

                if (!isset($input['title'])) {
                    return null;
                }

                $newOfficeBox = new OfficeBox();
                $newOfficeBox->setOffice($office);
                $newOfficeBox->setActive(true);

                if (isset($input['title'])) {
                    $boxTitle = trim($input['title']);
                    if ($boxTitle === '') {
                        return null;
                    }

                    $newOfficeBox->setTitle($boxTitle);
                }

                $entityManager->persist($newOfficeBox);
                $entityManager->flush();

                /** @var EventLogService $eventLogService */
                $eventLogService = $serviceManager->get(EventLogService::class);
                $eventLogService->logOfficeBoxCreate($newOfficeBox);

                return [
                    'newOfficeBox' => $newOfficeBox,
                ];
            }
        ]);
    }
}
