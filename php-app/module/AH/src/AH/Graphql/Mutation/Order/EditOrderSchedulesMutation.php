<?php

namespace AH\GraphQL\Mutation\Order;

use AH\Core\Service\GetOrderStatusForCurrentEntityStateService;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\BoxSchedule;
use AH\Entity\OfficeBox;
use AH\Entity\Order;
use AH\Entity\Service\ServiceWork;
use AH\Graphql\Enum\ScheduleStatusEnumType;
use AH\Graphql\Enum\ServiceWorkStatusEnumType;
use AH\GraphQL\Inputs;
use AH\Graphql\Type\BoxScheduleType;
use AH\Graphql\Type\OfficeBoxType;
use AH\Graphql\Type\OrderType;
use AH\Graphql\Type\ServiceWorkType;
use AH\GraphQL\Types;
use DateTime;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class EditOrderSchedulesMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'editOrderSchedulesMutation',
            'inputFields' => [
                'orderId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'deletingSchedulesIds' => [
                    'type' => Type::listOf(Type::nonNull(Type::id())),
                ],
                'schedules' => [
                    'type' => Type::listOf(Inputs::scheduleInputType()),
                ],
            ],
            'outputFields' => [
                'order' => [
                    'type' => Types::orderType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['order'];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                if (!isset($input['orderId'])) {
                    return null;
                }

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                $orderId = intval(RelayIdUtils::getId($input['orderId'], OrderType::TYPE_NAME));

                if ($orderId <= 0) {
                    throw new \Exception('Wrong orderId was provided.');
                }

                /** @var Order $order */
                $order = $entityManager->find(Order::class, $orderId);
                if (empty($order)) {
                    throw new \Exception('Wrong order was provided.');
                }

                $deletedSchedulesServiceWorks = [];

                if (isset($input['deletingSchedulesIds']) && !empty($input['deletingSchedulesIds'])) {
                    foreach ($input['deletingSchedulesIds'] as $idToDelete) {
                        $scheduleId = RelayIdUtils::getId($idToDelete, BoxScheduleType::TYPE_NAME);
                        /** @var BoxSchedule $schedule */
                        $schedule = $entityManager->find(BoxSchedule::class, $scheduleId);
                        if (!empty($schedule)) {
                            $deletedSchedulesServiceWorks[] = $schedule->getServiceWorks();
                            $entityManager->remove($schedule);
                        }
                    }
                }

                if (isset($input['schedules'])) {
                    $schedulesInputs = $input['schedules'];
                    if (!empty($schedulesInputs)) {
                        foreach ($schedulesInputs as $schedulesInput) {
                            $isNewSchedule = false;
                            if (mb_strpos($schedulesInput['id'], 'NEW_ROW') === 0) {
                                $schedule = new BoxSchedule();
                                $isNewSchedule = true;
                            } else {
                                $scheduleId = RelayIdUtils::getId($schedulesInput['id'], BoxScheduleType::TYPE_NAME);
                                $schedule = $entityManager->find(BoxSchedule::class, $scheduleId);
                                if (empty($schedule)) {
                                    $schedule = new BoxSchedule();
                                }
                            }

                            $schedule->setOrder($order);

                            $boxId = RelayIdUtils::getId($schedulesInput['boxId'], OfficeBoxType::TYPE_NAME);
                            $box = $entityManager->getReference(OfficeBox::class, $boxId);
                            if (empty($box)) {
                                throw new \Exception('Wrong box was provided');
                            }
                            $schedule->setBox($box);

                            $startInput = $schedulesInput['start'];
                            $start = DateTime::createFromFormat('U', $startInput);
                            $schedule->setStart($start);

                            $finishInput = $schedulesInput['finish'];
                            $finish = DateTime::createFromFormat('U', $finishInput);
                            $schedule->setFinish($finish);

                            if ($isNewSchedule) {
                                $schedule->setStatus(ScheduleStatusEnumType::SCHEDULE_WAIT_WORK_START_VALUE);
                            }

                            if (isset($schedulesInput['serviceWorks'])) {
                                $serviceWorks = $schedulesInput['serviceWorks'];
                                $schedule->clearServiceWorks();
                                foreach ($serviceWorks as $serviceWorkRelayId) {
                                    $serviceWorkId = RelayIdUtils::getId($serviceWorkRelayId, ServiceWorkType::TYPE_NAME);
                                    /** @var ServiceWork $serviceWork */
                                    $serviceWork = $entityManager->find(ServiceWork::class, $serviceWorkId);
                                    if (!empty($serviceWork)) {
                                        $schedule->addServiceWork($serviceWork);
                                        if ($schedule->getStatus() === ScheduleStatusEnumType::SCHEDULE_IN_WORK_VALUE) {
                                            $serviceWork->setStatus(ServiceWorkStatusEnumType::SERVICE_WORK_IN_WORK_VALUE);
                                        }
                                    }
                                }
                            }

                            $entityManager->persist($schedule);
                        }
                    }
                }

                $entityManager->persist($order);
                $entityManager->flush();

                if (!empty($deletedSchedulesServiceWorks)) {
                    foreach ($deletedSchedulesServiceWorks as $dssArray) {
                        /** @var ServiceWork $sw */
                        foreach ($dssArray as $sw) {
                            if ($sw->getSchedules()->count() === 0) {
                                $sw->setStatus(ServiceWorkStatusEnumType::SERVICE_WORK_WAIT_WORK_START_VALUE);
                            }
                        }
                    }
                }

                /** @var GetOrderStatusForCurrentEntityStateService $getOrderStatusForCurrentEntityStateService */
                $getOrderStatusForCurrentEntityStateService = $serviceManager->get('GetOrderStatusForCurrentEntityStateService');
                $order->setStatus($getOrderStatusForCurrentEntityStateService->getStatus($order));

                $entityManager->persist($order);
                $entityManager->flush();

                return [
                    'order' => $order,
                ];
            }
        ]);
    }
}
