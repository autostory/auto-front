<?php

namespace AH\GraphQL\Mutation\Order;

use AH\Core\Service\GetOrderStatusForCurrentEntityStateService;
use AH\Core\Utils\RelayIdUtils;
use AH\Core\Utils\WrongRelayIdException;
use AH\Entity\Basic\SparePartNode;
use AH\Entity\BoxSchedule;
use AH\Entity\Manager;
use AH\Entity\Order;
use AH\Entity\Service\ServiceWork;
use AH\Entity\Service\ServiceWorkActionType;
use AH\Graphql\Enum\OrderStatusEnumType;
use AH\Graphql\Enum\ServiceWorkStatusEnumType;
use AH\GraphQL\Inputs;
use AH\Graphql\Type\Basic\ServiceWorkActionTypeType;
use AH\Graphql\Type\Basic\SparePartNodeType;
use AH\Graphql\Type\ManagerType;
use AH\Graphql\Type\OrderType;
use AH\Graphql\Type\ServiceWorkType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class EditOrderServiceWorksMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'editOrderServiceWorksMutation',
            'inputFields' => [
                'orderId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'serviceWorks' => [
                    'type' => Type::listOf(Inputs::serviceWorkInputType()),
                ],
                'deletingServiceWorksIds' => [
                    'type' => Type::listOf(Type::nonNull(Type::id())),
                ],
            ],
            'outputFields' => [
                'order' => [
                    'type' => Types::orderType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['order'];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                if (!isset($input['orderId'])) {
                    return null;
                }

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                $orderId = intval(RelayIdUtils::getId($input['orderId'], OrderType::TYPE_NAME));

                if ($orderId <= 0) {
                    throw new \Exception('Wrong orderId was provided.');
                }

                /** @var Order $order */
                $order = $entityManager->find(Order::class, $orderId);
                if (empty($order)) {
                    throw new \Exception('Wrong order was provided.');
                }

                if (isset($input['deletingServiceWorksIds']) && !empty($input['deletingServiceWorksIds'])) {
                    foreach ($input['deletingServiceWorksIds'] as $deletingServiceWorksId) {
                        $serviceWorkId = RelayIdUtils::getId($deletingServiceWorksId, ServiceWorkType::TYPE_NAME);
                        /** @var ServiceWork $serviceWork */
                        $serviceWork = $entityManager->find(ServiceWork::class, $serviceWorkId);
                        if (!empty($serviceWork)) {
                            $serviceWorkSchedules = $serviceWork->getSchedules();

                            if (!empty($serviceWorkSchedules)) {
                                /** @var BoxSchedule $serviceWorkSchedule */
                                foreach ($serviceWorkSchedules as $serviceWorkSchedule) {
                                    $sws = $serviceWorkSchedule->getServiceWorks();

                                    if ($sws->count() === 1) {
                                        // Удалили работу которая была привязана к расписанию у которого больше нет работ кроме удаляемой работы.
                                        $entityManager->remove($serviceWorkSchedule);
                                    }
                                }
                            }
                            $entityManager->remove($serviceWork);
                        }
                    }
                }

                if (isset($input['serviceWorks'])) {
                    $serviceWorksInput = $input['serviceWorks'];
                    if (!empty($serviceWorksInput)) {
                        foreach ($serviceWorksInput as $serviceWorkInput) {
                            $isNewServiceWork = false;
                            if (mb_strpos($serviceWorkInput['id'], 'NEW_ROW') === 0) {
                                $isNewServiceWork = true;
                                $serviceWork = new ServiceWork();
                            } else {
                                $serviceWorkId = RelayIdUtils::getId($serviceWorkInput['id'], ServiceWorkType::TYPE_NAME);
                                $serviceWork = $entityManager->find(ServiceWork::class, $serviceWorkId);
                                if (empty($serviceWork)) {
                                    $serviceWork = new ServiceWork();
                                }
                            }

                            $serviceWork->setOrder($order);
                            $serviceWorkActionTypeId = RelayIdUtils::getId($serviceWorkInput['serviceWorkActionTypeId'], ServiceWorkActionTypeType::TYPE_NAME);
                            $serviceWorkActionType = $entityManager->getReference(ServiceWorkActionType::class, $serviceWorkActionTypeId);
                            if (empty($serviceWorkActionType)) {
                                return null;
                            }
                            $serviceWork->setServiceWorkActionType($serviceWorkActionType);

                            $sparePartNodeId = RelayIdUtils::getId($serviceWorkInput['sparePartNodeId'], SparePartNodeType::TYPE_NAME);
                            $sparePartNode = $entityManager->getReference(SparePartNode::class, $sparePartNodeId);
                            if (empty($sparePartNode)) {
                                return null;
                            }
                            $serviceWork->setSparePartNode($sparePartNode);

                            $serviceWork->setAmount($serviceWorkInput['amount']);
                            $serviceWork->setCostPerUnit($serviceWorkInput['costPerUnit']);
                            $serviceWork->setDiscountPercent($serviceWorkInput['discountPercent']);
                            $serviceWork->setNote(trim($serviceWorkInput['name']));

                            $totalCost = (floatval($serviceWorkInput['costPerUnit']) * floatval($serviceWorkInput['amount'])) * ((100 - floatval($serviceWorkInput['discountPercent'])) / 100);
                            $serviceWork->setTotalCost($totalCost);

                            if (isset($serviceWorkInput['masterId']) && !empty($serviceWorkInput['masterId'])) {
                                $masterId = RelayIdUtils::getId($serviceWorkInput['masterId'], ManagerType::TYPE_NAME);
                                $master = $entityManager->getReference(Manager::class, $masterId);
                                if (empty($master)) {
                                    throw new \Exception('Cant find provided master');
                                }
                                $serviceWork->setMaster($master);
                            } else {
                                $serviceWork->setMaster(null);
                            }

                            $serviceWork->setLaborHour($serviceWorkInput['laborHour']);

                            if ($isNewServiceWork) {
                                $serviceWork->setStatus(ServiceWorkStatusEnumType::SERVICE_WORK_WAIT_WORK_START_VALUE);
                            } else {
                                if (isset($serviceWorkInput['status'])) {
                                    $serviceWork->setStatus($serviceWorkInput['status']);
                                }
                            }
                            $order->addServiceWork($serviceWork);
                            $entityManager->persist($serviceWork);
                        }
                    }
                }

                $entityManager->persist($order);
                $entityManager->flush();

                /** @var GetOrderStatusForCurrentEntityStateService $getOrderStatusForCurrentEntityStateService */
                $getOrderStatusForCurrentEntityStateService = $serviceManager->get('GetOrderStatusForCurrentEntityStateService');
                $order->setStatus($getOrderStatusForCurrentEntityStateService->getStatus($order));

                $entityManager->persist($order);
                $entityManager->flush();

                return [
                    'order' => $order,
                ];
            }
        ]);
    }
}
