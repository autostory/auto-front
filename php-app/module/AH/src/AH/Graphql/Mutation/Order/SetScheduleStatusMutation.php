<?php

namespace AH\GraphQL\Mutation\Order;

use AH\Core\Service\GetOrderStatusForCurrentEntityStateService;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\BoxSchedule;
use AH\Graphql\Enum\ScheduleStatusEnumType;
use AH\Graphql\Enum\ServiceWorkStatusEnumType;
use AH\GraphQL\Enums;
use AH\Graphql\Type\BoxScheduleType;
use AH\GraphQL\Types;
use Exception;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class SetScheduleStatusMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'setScheduleStatusMutation',
            'inputFields' => [
                'scheduleId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'status' => [
                    'type' => Type::nonNull(Enums::scheduleStatusEnumType()),
                ],
            ],
            'outputFields' => [
                'schedule' => [
                    'type' => Types::boxScheduleType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['schedule'];
                    },
                ],
                'order' => [
                    'type' => Types::orderType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['order'];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                if (!isset($input['scheduleId'])) {
                    return null;
                }

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                $scheduleId = intval(RelayIdUtils::getId($input['scheduleId'], BoxScheduleType::TYPE_NAME));

                if ($scheduleId <= 0) {
                    throw new \Exception('Wrong scheduleId was provided.');
                }

                /** @var BoxSchedule $schedule */
                $schedule = $entityManager->find(BoxSchedule::class, $scheduleId);
                if (empty($schedule)) {
                    throw new \Exception('Wrong schedule was provided.');
                }

                $scheduleStatus = $input['status'];
                $schedule->setStatus($scheduleStatus);

                $sws = $schedule->getServiceWorks();
                if (!empty($sws)) {
                    $swStatus = ServiceWorkStatusEnumType::SERVICE_WORK_IN_WORK_VALUE;
                    if ($scheduleStatus === ScheduleStatusEnumType::SCHEDULE_DONE_VALUE) {
                        $swStatus = ServiceWorkStatusEnumType::SERVICE_WORK_DONE_VALUE;
                    }

                    foreach ($sws as $sw) {
                        $sw->setStatus($swStatus);
                        $entityManager->persist($sw);
                    }
                }

                $entityManager->persist($schedule);
                $entityManager->flush();

                $order = $schedule->getOrder();

                /** @var GetOrderStatusForCurrentEntityStateService $getOrderStatusForCurrentEntityStateService */
                $getOrderStatusForCurrentEntityStateService = $serviceManager->get('GetOrderStatusForCurrentEntityStateService');
                $order->setStatus($getOrderStatusForCurrentEntityStateService->getStatus($order));

                $entityManager->persist($order);
                $entityManager->flush();

                return [
                    'schedule' => $schedule,
                    'order' => $order,
                ];
            }
        ]);
    }
}
