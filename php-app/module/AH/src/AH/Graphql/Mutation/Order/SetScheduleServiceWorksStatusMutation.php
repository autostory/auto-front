<?php

namespace AH\GraphQL\Mutation\Order;

use AH\Core\Mail\MailService;
use AH\Core\Service\ClientAutoLoginLinkGenerator;
use AH\Core\Service\GetOrderStatusForCurrentEntityStateService;
use AH\Core\Service\PasswordGenerator;
use AH\Core\SMS\SMSService;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\BoxSchedule;
use AH\Entity\Log\SmsLogRecord;
use AH\Entity\Service\ServiceWork;
use AH\GraphQL\Enums;
use AH\GraphQL\Inputs;
use AH\Graphql\Type\BoxScheduleType;
use AH\Graphql\Type\ServiceWorkType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Crypt\Password\Bcrypt;
use Zend\ServiceManager\ServiceManager;

class SetScheduleServiceWorksStatusMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'setScheduleServiceWorksStatusMutation',
            'inputFields' => [
                'scheduleId' => [
                    'type' => Type::nonNull(Type::id())
                ],
                'scheduleStatus' => [
                    'type' => Type::nonNull(Enums::scheduleStatusEnumType()),
                ],
                'serviceWorksInput' => [
                    'type' => Type::listOf(Inputs::serviceWorkStatusInputType()),
                ],
                'sendEmailNotification' => [
                    'type' => Type::nonNull(Type::boolean()),
                    'defaultValue' => false,
                ],
                'sendSMSNotification' => [
                    'type' => Type::nonNull(Type::boolean()),
                    'defaultValue' => false,
                ],
            ],
            'outputFields' => [
                'schedule' => [
                    'type' => Types::boxScheduleType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['schedule'];
                    },
                ],
                'order' => [
                    'type' => Types::orderType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['order'];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                if (!isset($input['scheduleId'])) {
                    return null;
                }

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                $scheduleId = intval(RelayIdUtils::getId($input['scheduleId'], BoxScheduleType::TYPE_NAME));

                if ($scheduleId <= 0) {
                    throw new \Exception('Wrong scheduleId was provided.');
                }

                /** @var BoxSchedule $schedule */
                $schedule = $entityManager->find(BoxSchedule::class, $scheduleId);
                if (empty($schedule)) {
                    throw new \Exception('Wrong schedule was provided.');
                }

                $serviceWorksInputs = $input['serviceWorksInput'];

                if (!empty($serviceWorksInputs)) {
                    $rServiceWork = $entityManager->getRepository(ServiceWork::class);
                    foreach ($serviceWorksInputs as $serviceWorksInput) {
                        $serviceWorkId = intval(RelayIdUtils::getId($serviceWorksInput['id'], ServiceWorkType::TYPE_NAME));
                        $serviceWork = $rServiceWork->findOneBy([
                            'id' => $serviceWorkId,
                        ]);

                        if (!$schedule->getServiceWorks()->contains($serviceWork)) {
                            throw new \Exception('ServiceWork is not from provided Schedule');
                        }

                        if (!empty($serviceWork)) {
                            $serviceWork->setStatus($serviceWorksInput['status']);
                            $entityManager->persist($serviceWork);
                        }
                    }
                }

                $scheduleStatus = $input['scheduleStatus'];
                $schedule->setStatus($scheduleStatus);

                $entityManager->persist($schedule);
                $entityManager->flush();

                $order = $schedule->getOrder();

                /** @var GetOrderStatusForCurrentEntityStateService $getOrderStatusForCurrentEntityStateService */
                $getOrderStatusForCurrentEntityStateService = $serviceManager->get('GetOrderStatusForCurrentEntityStateService');
                $order->setStatus($getOrderStatusForCurrentEntityStateService->getStatus($order));

                $entityManager->persist($order);
                $entityManager->flush();

                $client = $order->getClient();

                if (
                    ($input['sendEmailNotification'] && !empty($client->getRegistrationEmail()))
                    || ($input['sendSMSNotification'] && !empty($client->getRegistrationPhone()))
                ) {
                    /** @var ClientAutoLoginLinkGenerator $clientAutoLoginLinkGenerator */
                    $clientAutoLoginLinkGenerator = $serviceManager->get(ClientAutoLoginLinkGenerator::class);

                    $link = $clientAutoLoginLinkGenerator->generate($client);

                    $login = null;
                    $newPassword = null;
                    if (!$client->getInviteSent()) {
                        $login = $client->getRegistrationEmail();
                        if (!empty($client->getRegistrationPhone())) {
                            $login = $client->getRegistrationPhone();
                        }

                        /** @var PasswordGenerator $passwordGenerator */
                        $passwordGenerator = $serviceManager->get(PasswordGenerator::class);
                        $newPassword = $passwordGenerator->generateStrongPassword();

                        $bcrypt = new Bcrypt();
                        $bcrypt->setCost(10);
                        $passwordHash = $bcrypt->create($newPassword);
                        $client->setPassword($passwordHash);
                        $entityManager->persist($client);
                        $entityManager->flush();
                    }

                    /** @var MailService $mailService */
                    $mailService = $serviceManager->get(MailService::class);

                    if ($input['sendEmailNotification'] && !empty($client->getRegistrationEmail())) {
                        $mailService->sendToClientAboutOrderWorkCompleted(
                            $client->getRegistrationEmail(),
                            $client->getFullName(),
                            $order->getId(),
                            $link,
                            $login,
                            $newPassword
                        );

                        if (!$client->getInviteSent()) {
                            $client->setInviteSent(true);
                            $entityManager->persist($client);
                            $entityManager->flush();
                        }
                    }

                    if ($input['sendSMSNotification'] && !empty($client->getRegistrationPhone())) {
                        /** @var SMSService $smsService */
                        $smsService = $serviceManager->get(SMSService::class);

                        $message = 'Работы по заказу SW-' . $order->getId() . ' выполнены.
Для отслеживания перейдите по ссылке ' . $link;
                        $phone = '7' . $client->getRegistrationPhone();
                        if (!empty($login) && !empty($newPassword)) {
                            $message = $message . '
логин ' . $login . '
пароль ' . $newPassword;
                        }
                        $result = $smsService->sendSms($phone, $message);

                        $smsId = isset($result['smsId']) ? $result['smsId'] : null;
                        $cost = isset($result['cost']) ? $result['cost'] : null;

                        if (empty($smsId)) {
                            throw new \Exception('Sms send error.');
                        }

                        /** @var \Zend\Authentication\AuthenticationService $authenticationService */
                        $authenticationService = $serviceManager->get('AuthenticationService');
                        $authUser = $authenticationService->getIdentity();

                        $smsLog = new SmsLogRecord();
                        $smsLog->setType(SmsLogRecord::CLIENT_NOTIFY_ORDER_WORK_COMPLETED);
                        $smsLog->setAddressee($client);
                        $smsLog->setInitiator($authUser);
                        $smsLog->setSmsId($smsId);
                        $smsLog->setSmsProvider($smsService->getDefaultSmsProvider());
                        $smsLog->setCost($cost);
                        $smsLog->setMessage($message);
                        $smsLog->setPhone($client->getRegistrationPhone());

                        if (!$client->getInviteSent()) {
                            $client->setInviteSent(true);
                            $entityManager->persist($client);
                            $entityManager->flush();
                        }

                        $entityManager->persist($smsLog);

                        $entityManager->flush();
                    }
                };

                return [
                    'schedule' => $schedule,
                    'order' => $order,
                ];
            }
        ]);
    }
}
