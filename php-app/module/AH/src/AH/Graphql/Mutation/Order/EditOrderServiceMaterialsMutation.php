<?php

namespace AH\GraphQL\Mutation\Order;

use AH\Core\Utils\RelayIdUtils;
use AH\Entity\Basic\UnitMeasureType;
use AH\Entity\Order;
use AH\Entity\Service\ServiceMaterial;
use AH\GraphQL\Inputs;
use AH\Graphql\Type\Basic\UnitMeasureTypeType;
use AH\Graphql\Type\OrderType;
use AH\Graphql\Type\ServiceMaterialType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class EditOrderServiceMaterialsMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'editOrderServiceMaterialsMutation',
            'inputFields' => [
                'orderId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'deletingServiceMaterialsIds' => [
                    'type' => Type::listOf(Type::nonNull(Type::id())),
                ],
                'serviceMaterials' => [
                    'type' => Type::listOf(Inputs::serviceMaterialInputType()),
                ],
            ],
            'outputFields' => [
                'order' => [
                    'type' => Types::orderType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['order'];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                if (!isset($input['orderId'])) {
                    return null;
                }

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                $orderId = intval(RelayIdUtils::getId($input['orderId'],OrderType::TYPE_NAME));

                if ($orderId <= 0) {
                    throw new \Exception('Wrong orderId was provided.');
                }

                /** @var Order $order */
                $order = $entityManager->find(Order::class, $orderId);
                if (empty($order)) {
                    throw new \Exception('Wrong order was provided.');
                }

                if (isset($input['deletingServiceMaterialsIds']) && !empty($input['deletingServiceMaterialsIds'])) {
                    foreach ($input['deletingServiceMaterialsIds'] as $deletingServiceMaterialsId) {
                        $serviceMaterialId = RelayIdUtils::getId($deletingServiceMaterialsId, ServiceMaterialType::TYPE_NAME);
                        $serviceMaterial = $entityManager->find(ServiceMaterial::class, $serviceMaterialId);
                        if (!empty($serviceMaterial)) {
                            $entityManager->remove($serviceMaterial);
                        }
                    }
                }

                if (isset($input['serviceMaterials'])) {
                    $serviceMaterialsInput = $input['serviceMaterials'];
                    if (!empty($serviceMaterialsInput)) {
                        foreach ($serviceMaterialsInput as $serviceMaterialInput) {
                            if (mb_strpos($serviceMaterialInput['id'], 'NEW_ROW') === 0) {
                                $serviceMaterial = new ServiceMaterial();
                            } else {
                                $serviceMaterialId = RelayIdUtils::getId($serviceMaterialInput['id'], ServiceMaterialType::TYPE_NAME);
                                $serviceMaterial = $entityManager->find(ServiceMaterial::class, $serviceMaterialId);
                                if (empty($serviceMaterial)) {
                                    $serviceMaterial = new ServiceMaterial();
                                }
                            }

                            $serviceMaterial->setOrder($order);

                            $unitMeasureTypeId = RelayIdUtils::getId($serviceMaterialInput['unitMeasureTypeId'], UnitMeasureTypeType::TYPE_NAME);
                            $unitMeasureType = $entityManager->getReference(UnitMeasureType::class, $unitMeasureTypeId);
                            if (empty($unitMeasureType)) {
                                return null;
                            }
                            $serviceMaterial->setUnitMeasureType($unitMeasureType);

                            $totalCost = (floatval($serviceMaterialInput['costPerUnit']) * floatval($serviceMaterialInput['amount'])) * ((100 - floatval($serviceMaterialInput['discountPercent'])) / 100);

                            $serviceMaterial->setAmount($serviceMaterialInput['amount']);
                            $serviceMaterial->setCostPerUnit($serviceMaterialInput['costPerUnit']);
                            $serviceMaterial->setDiscountPercent($serviceMaterialInput['discountPercent']);
                            $serviceMaterial->setName($serviceMaterialInput['name']);
                            $serviceMaterial->setTotalCost($totalCost);

                            $order->addServiceMaterial($serviceMaterial);
                            $entityManager->persist($serviceMaterial);
                        }
                    }
                }

                $entityManager->persist($order);
                $entityManager->flush();

                return [
                    'order' => $order,
                ];
            }
        ]);
    }
}
