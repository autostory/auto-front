<?php

namespace AH\GraphQL\Mutation\Order;

use AH\Core\Mail\MailService;
use AH\Core\Service\ClientAutoLoginLinkGenerator;
use AH\Core\Service\PasswordGenerator;
use AH\Core\SMS\SMSService;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\Log\SmsLogRecord;
use AH\Entity\Order;
use AH\Graphql\Type\OrderType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\Crypt\Password\Bcrypt;
use Zend\ServiceManager\ServiceManager;

class NotifyClientAboutOrderWorkCompletedMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'notifyClientAboutOrderWorkCompletedMutation',
            'inputFields' => [
                'orderId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'sendEmailNotification' => [
                    'type' => Type::nonNull(Type::boolean()),
                ],
                'sendSMSNotification' => [
                    'type' => Type::nonNull(Type::boolean()),
                ],
            ],
            'outputFields' => [
                'order' => [
                    'type' => Types::orderType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['order'];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                if (!isset($input['orderId'])) {
                    return null;
                }

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                $orderId = intval(RelayIdUtils::getId($input['orderId'], OrderType::TYPE_NAME));

                if ($orderId <= 0) {
                    throw new \Exception('Wrong orderId was provided.');
                }

                /** @var Order $order */
                $order = $entityManager->find(Order::class, $orderId);
                if (empty($order)) {
                    throw new \Exception('Wrong order was provided.');
                }

                $client = $order->getClient();

                if (
                    ($input['sendEmailNotification'] && !empty($client->getRegistrationEmail()))
                    || ($input['sendSMSNotification'] && !empty($client->getRegistrationPhone()))
                ) {
                    /** @var ClientAutoLoginLinkGenerator $clientAutoLoginLinkGenerator */
                    $clientAutoLoginLinkGenerator = $serviceManager->get(ClientAutoLoginLinkGenerator::class);

                    $link = $clientAutoLoginLinkGenerator->generate($client);

                    $login = null;
                    $newPassword = null;
                    if (!$client->getInviteSent()) {
                        $login = $client->getRegistrationEmail();
                        if (!empty($client->getRegistrationPhone())) {
                            $login = $client->getRegistrationPhone();
                        }

                        /** @var PasswordGenerator $passwordGenerator */
                        $passwordGenerator = $serviceManager->get(PasswordGenerator::class);
                        $newPassword = $passwordGenerator->generateStrongPassword();

                        $bcrypt = new Bcrypt();
                        $bcrypt->setCost(10);
                        $passwordHash = $bcrypt->create($newPassword);
                        $client->setPassword($passwordHash);
                        $entityManager->persist($client);
                        $entityManager->flush();
                    }

                    /** @var MailService $mailService */
                    $mailService = $serviceManager->get(MailService::class);

                    if ($input['sendEmailNotification'] && !empty($client->getRegistrationEmail())) {
                        $mailService->sendToClientAboutOrderWorkCompleted(
                            $client->getRegistrationEmail(),
                            $client->getFullName(),
                            $order->getId(),
                            $link,
                            $login,
                            $newPassword
                        );

                        if (!$client->getInviteSent()) {
                            $client->setInviteSent(true);
                            $entityManager->persist($client);
                            $entityManager->flush();
                        }
                    }

                    if ($input['sendSMSNotification'] && !empty($client->getRegistrationPhone())) {
                        /** @var SMSService $smsService */
                        $smsService = $serviceManager->get(SMSService::class);

                        $message = 'Работы по заказу SW-' . $order->getId() . ' выполнены.
Для отслеживания перейдите по ссылке ' . $link;
                        $phone = '7' . $client->getRegistrationPhone();
                        if (!empty($login) && !empty($newPassword)) {
                            $message = $message . '
логин ' . $login . '
пароль ' . $newPassword;
                        }
                        $result = $smsService->sendSms($phone, $message);

                        $smsId = isset($result['smsId']) ? $result['smsId'] : null;
                        $cost = isset($result['cost']) ? $result['cost'] : null;

                        if (empty($smsId)) {
                            throw new \Exception('Sms send error.');
                        }

                        /** @var \Zend\Authentication\AuthenticationService $authenticationService */
                        $authenticationService = $serviceManager->get('AuthenticationService');
                        $authUser = $authenticationService->getIdentity();

                        $smsLog = new SmsLogRecord();
                        $smsLog->setType(SmsLogRecord::CLIENT_NOTIFY_ORDER_WORK_COMPLETED);
                        $smsLog->setAddressee($client);
                        $smsLog->setInitiator($authUser);
                        $smsLog->setSmsId($smsId);
                        $smsLog->setSmsProvider($smsService->getDefaultSmsProvider());
                        $smsLog->setCost($cost);
                        $smsLog->setMessage($message);
                        $smsLog->setPhone($client->getRegistrationPhone());

                        if (!$client->getInviteSent()) {
                            $client->setInviteSent(true);
                            $entityManager->persist($client);
                            $entityManager->flush();
                        }

                        $entityManager->persist($smsLog);

                        $entityManager->flush();
                    }
                };

                return [
                    'order' => $order,
                ];
            }
        ]);
    }
}
