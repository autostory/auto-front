<?php

namespace AH\GraphQL\Mutation\Order;

use AH\Core\Service\GetOrderStatusForCurrentEntityStateService;
use AH\Core\Utils\RelayIdUtils;
use AH\Core\Utils\WrongRelayIdException;
use AH\Entity\Basic\SparePartNode;
use AH\Entity\Basic\UnitMeasureType;
use AH\Entity\BoxSchedule;
use AH\Entity\Car;
use AH\Entity\Client;
use AH\Entity\Manager;
use AH\Entity\OfficeBox;
use AH\Entity\Order;
use AH\Entity\Service\ServiceMaterial;
use AH\Entity\Service\ServiceWork;
use AH\Entity\Service\ServiceWorkActionType;
use AH\GraphQL\Enums;
use AH\GraphQL\Inputs;
use AH\Graphql\Type\Basic\ServiceWorkActionTypeType;
use AH\Graphql\Type\Basic\SparePartNodeType;
use AH\Graphql\Type\Basic\UnitMeasureTypeType;
use AH\Graphql\Type\CarType;
use AH\Graphql\Type\ClientType;
use AH\Graphql\Type\ManagerType;
use AH\Graphql\Type\OfficeBoxType;
use AH\Graphql\Type\OrderType;
use AH\Graphql\Type\ServiceMaterialType;
use AH\Graphql\Type\ServiceWorkType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class EditOrderMainInfoMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'editOrderMainInfoMutation',
            'inputFields' => [
                'orderId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'clientId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'carId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'contactName' => [
                    'type' => Type::string(),
                ],
                'contactPhone' => [
                    'type' => Type::string(),
                ],
                'phoneSameAsInProfile' => [
                    'type' => Type::boolean(),
                ],
                'contactNote' => [
                    'type' => Type::string(),
                ],
                'orderReason' => [
                    'type' => Type::nonNull(Type::string()),
                ],
                'estimatedClientArrivalDate' => [
                    'type' => Type::string(),
                ],
                'isClientVisitRequired' => [
                    'type' => Type::nonNull(Type::boolean()),
                ],
            ],
            'outputFields' => [
                'order' => [
                    'type' => Types::orderType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['order'];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                if (!isset($input['orderId']) || !isset($input['clientId']) || !isset($input['carId'])) {
                    return null;
                }

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                $orderId = intval(RelayIdUtils::getId($input['orderId'],OrderType::TYPE_NAME));

                if ($orderId <= 0) {
                    throw new \Exception('Wrong orderId was provided.');
                }

                /** @var Order $order */
                $order = $entityManager->find(Order::class, $orderId);
                if (empty($order)) {
                    throw new \Exception('Wrong order was provided.');
                }

                $clientId = intval(RelayIdUtils::getId($input['clientId'],ClientType::TYPE_NAME));
                $carId = intval(RelayIdUtils::getId($input['carId'],CarType::TYPE_NAME));

                if ($clientId <= 0) {
                    throw new \Exception('Wrong clientId was provided.');
                }

                if ($carId <= 0) {
                    throw new \Exception('Wrong carId was provided.');
                }

                /** @var Client $newOrderClient */
                $newOrderClient = $entityManager->find(Client::class, $clientId);
                /** @var Car $newOrderCar */
                $newOrderCar = $entityManager->find(Car::class, $carId);

                if (empty($newOrderClient)) {
                    throw new \Exception('Wrong client was provided.');
                }

                if (empty($newOrderCar)) {
                    throw new \Exception('Wrong car was provided.');
                }

                $currentOrderClient = $order->getClient();
                $currentOrderCar = $order->getCar();

                if ($currentOrderClient->getId() !== $newOrderClient->getId()) {
                    $order->setClient($newOrderClient);
                }

                if ($currentOrderCar->getId() !== $newOrderCar->getId()) {
                    if (!$newOrderCar->getOwners()->contains($newOrderClient)) {
                        throw new \Exception('Provided car not owned by provided client.');
                    }

                    $order->setCar($newOrderCar);
                }

                $orderReason = strval($input['orderReason']);
                if ($orderReason === '') {
                    throw new \Exception('Wrong orderReason was provided.');
                }

                $order->setOrderReason($orderReason);

                if (isset($input['phoneSameAsInProfile']) && $input['phoneSameAsInProfile'] === true) {
                    $order->setContactName($newOrderClient->getFullName());
                    $order->setContactPhone($newOrderClient->getRegistrationPhone());
                    $order->setPhoneSameAsInProfile(true);
                    $order->setContactNote(null);
                } else {
                    $order->setPhoneSameAsInProfile(false);
                    $contactName = strval($input['contactName']);
                    $contactPhone = strval($input['contactPhone']);

                    $order->setContactName($contactName);
                    $order->setContactPhone($contactPhone);
                }

                $order->setClientVisitRequired($input['isClientVisitRequired']);

                if (isset($input['estimatedClientArrivalDate']) && $input['estimatedClientArrivalDate'] !== null) {
                    if (empty($input['estimatedClientArrivalDate'])) {
                        $order->setEstimatedClientArrivalDate(null);
                    } else {
                        $estimatedClientArrivalDate = \DateTime::createFromFormat('U', $input['estimatedClientArrivalDate']);

                        if ($estimatedClientArrivalDate) {
                            $order->setEstimatedClientArrivalDate($estimatedClientArrivalDate);
                        }
                    }
                }

                $contactNote = strval($input['contactNote']);
                $order->setContactNote($contactNote);

                $entityManager->persist($order);
                $entityManager->flush();

                /** @var GetOrderStatusForCurrentEntityStateService $getOrderStatusForCurrentEntityStateService */
                $getOrderStatusForCurrentEntityStateService = $serviceManager->get('GetOrderStatusForCurrentEntityStateService');
                $order->setStatus($getOrderStatusForCurrentEntityStateService->getStatus($order));

                $entityManager->persist($order);
                $entityManager->flush();

                return [
                    'order' => $order,
                ];
            }
        ]);
    }
}
