<?php

namespace AH\GraphQL\Mutation\Order;

use AH\Core\Utils\RelayIdUtils;
use AH\Entity\Order;
use AH\Graphql\Enum\OrderStatusEnumType;
use AH\GraphQL\Enums;
use AH\Graphql\Type\OrderType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class RejectOrderMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'rejectOrderMutation',
            'inputFields' => [
                'orderId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'rejectReasonId' => [
                    'type' => Type::nonNull(Enums::resultRequestCommunicationRejectReasonEnumType()),
                    'description' => 'ID причины отказа',
                ],
                'rejectReasonDescription' => [
                    'type' => Type::string(),
                    'description' => 'Описание причины отказа',
                ],
            ],
            'outputFields' => [
                'order' => [
                    'type' => Types::orderType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['order'];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                $orderId = intval(RelayIdUtils::getId($input['orderId'], OrderType::TYPE_NAME));

                /** @var Order $order */
                $order = $entityManager->find(Order::class, $orderId);
                if (empty($order)) {
                    throw new \Exception('Wrong order was provided.');
                }

                $order->setStatus(OrderStatusEnumType::REJECT_VALUE);
                $order->setRejectReasonId($input['rejectReasonId']);
                if (!empty($input['rejectReasonDescription'])) {
                    $order->setRejectReasonDescription($input['rejectReasonDescription']);
                }

                $entityManager->persist($order);
                $entityManager->flush();

                return [
                    'order' => $order,
                ];
            }
        ]);
    }
}
