<?php

namespace AH\GraphQL\Mutation\Order;

use AH\Core\Service\GetOrderStatusForCurrentEntityStateService;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\Order;
use AH\Graphql\Enum\OrderStatusEnumType;
use AH\Graphql\Type\OrderType;
use AH\GraphQL\Types;
use DateTime;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class EditOrderCarArriveInfoMutation
{
    public static function create(ServiceManager $serviceManager)
    {
        return Relay::mutationWithClientMutationId([
            'name' => 'editOrderCarArriveInfoMutation',
            'inputFields' => [
                'orderId' => [
                    'type' => Type::nonNull(Type::id()),
                ],
                'factCarReturnDateTime' => [
                    'type' => Type::string(),
                ],
                'factClientVisitDateTime' => [
                    'type' => Type::string(),
                ],
            ],
            'outputFields' => [
                'order' => [
                    'type' => Types::orderType($serviceManager),
                    'resolve' => function ($payload) {
                        return $payload['order'];
                    },
                ]
            ],
            'mutateAndGetPayload' => function ($input) use ($serviceManager) {
                if (!isset($input['orderId'])) {
                    return null;
                }

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                $orderId = intval(RelayIdUtils::getId($input['orderId'], OrderType::TYPE_NAME));

                if ($orderId <= 0) {
                    throw new \Exception('Wrong orderId was provided.');
                }

                /** @var Order $order */
                $order = $entityManager->find(Order::class, $orderId);
                if (empty($order)) {
                    throw new \Exception('Wrong order was provided.');
                }

                if (!empty($input['factCarReturnDateTime'])) {
                    $factCarReturnDateTime = DateTime::createFromFormat('U', $input['factCarReturnDateTime']);
                    $order->setFactCarReturnDateTime($factCarReturnDateTime);
                }

                if (!empty($input['factClientVisitDateTime'])) {
                    $factClientVisitDateTime = DateTime::createFromFormat('U', $input['factClientVisitDateTime']);
                    $order->setFactClientVisitDateTime($factClientVisitDateTime);
                }
                $entityManager->persist($order);
                $entityManager->flush();

                /** @var GetOrderStatusForCurrentEntityStateService $getOrderStatusForCurrentEntityStateService */
                $getOrderStatusForCurrentEntityStateService = $serviceManager->get('GetOrderStatusForCurrentEntityStateService');
                $order->setStatus($getOrderStatusForCurrentEntityStateService->getStatus($order));

                $entityManager->persist($order);
                $entityManager->flush();

                return [
                    'order' => $order,
                ];
            }
        ]);
    }
}
