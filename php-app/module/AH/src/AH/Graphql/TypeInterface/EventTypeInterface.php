<?php
namespace AH\GraphQL\TypeInterface;

use AH\Entity\Client;
use AH\Entity\Event\AbstractEvent;
use AH\Entity\Manager;
use AH\Entity\User;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\InterfaceType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use Zend\ServiceManager\ServiceManager;

class EventTypeInterface extends InterfaceType
{
    const INTERFACE_NAME = 'EventTypeInterface';

    public function __construct(ServiceManager $serviceManager)
    {
        $clientType = Types::clientType($serviceManager);
        $managerType = Types::managerType($serviceManager);

        $config = [
            'name' => self::INTERFACE_NAME,
            'description' => 'Интерфейс для события с техникой.',
            'fields' => [
                'car' => [
                    'type' => Type::nonNull(Types::carType($serviceManager)),
                    'description' => 'Автомобиль с которым случилось событие.',
                    'resolve' => function (AbstractEvent $event) {
                        return $event->getCar();
                    },
                ],
                'mileage' => [
                    'type' => Type::nonNull(Type::int()),
                    'description' => 'Пробег на момент события.',
                    'resolve' => function (AbstractEvent $event) {
                        return $event->getMileage();
                    },
                ],
                'whenDidItHappen' => [
                    'type' => Types::dateType(),
                    'description' => 'Когла случилось событие.',
                    'resolve' => function (AbstractEvent $event) {
                        return $event->getDate();
                    },
                ],
                'createdAt' => [
                    'type' => Types::dateType(),
                    'description' => 'Когда было добавлено.',
                    'resolve' => function (AbstractEvent $event) {
                        return $event->getCreatedAt();
                    },
                ],
                'totalCost' => [
                    'type' => Type::nonNull(Type::float()),
                    'description' => 'Общая стоимость события.',
                    'resolve' => function (AbstractEvent $event) {
                        return $event->getTotalCost();
                    },
                ],
                'note' => [
                    'type' => Type::string(),
                    'description' => 'Заметка к событию.',
                    'resolve' => function (AbstractEvent $event) {
                        return $event->getNote();
                    },
                ],
                'initiator' => [
                    'type' => Types::eventInitiatorType($serviceManager),
                    'description' => 'Инициатор события.',
                    'resolve' => function (AbstractEvent $event) {
                        return $event->getInitiator();
                    },
                ],
            ],
        ];

        parent::__construct($config);
    }
}