<?php
namespace AH\GraphQL\TypeInterface;

use AH\Core\Acl\ClientAccess;
use AH\Entity\Client;
use AH\Entity\User;
use GraphQL\Type\Definition\InterfaceType;
use GraphQL\Type\Definition\Type;
use Zend\ServiceManager\ServiceManager;

class UserTypeInterface extends InterfaceType
{
    const INTERFACE_NAME = 'UserTypeInterface';

    public function __construct(ServiceManager $serviceManager)
    {
        /** @var ClientAccess $clientAccess */
        $clientAccess = $serviceManager->get(ClientAccess::class);

        $config = [
            'name' => self::INTERFACE_NAME,
            'description' => 'Интерфейс c полями общими для всех пользователей.',
            'fields' => [
                'email' => [
                    'type' => Type::string(),
                    'description' => 'Email пользователя.',
                    'resolve' => function (User $user) use ($clientAccess) {
                        if ($user instanceof Client && !$clientAccess->canViewClient($user)) {
                            return null;
                        }

                        // TODO: Добавить проверку и для Manager

                        return $user->getRegistrationEmail();
                    },
                ],
                'phone' => [
                    'type' => Type::string(),
                    'description' => 'Сотовый пользователя.',
                    'resolve' => function (User $user) use ($clientAccess) {
                        if ($user instanceof Client && !$clientAccess->canViewClient($user)) {
                            return null;
                        }

                        return $user->getRegistrationPhone();
                    },
                ],
                'fullName' => [
                    'type' => Type::string(),
                    'description' => 'Полное имя пользователя.',
                    'resolve' => function (User $user) use ($clientAccess) {
                        if ($user instanceof Client && !$clientAccess->canViewClient($user)) {
                            return null;
                        }

                        return $user->getFullName();
                    },
                ],
                'firstName' => [
                    'type' => Type::string(),
                    'description' => 'Имя пользователя.',
                    'resolve' => function (User $user) use ($clientAccess) {
                        if ($user instanceof Client && !$clientAccess->canViewClient($user)) {
                            return null;
                        }

                        return $user->getFirstName();
                    },
                ],
                'surname' => [
                    'type' => Type::string(),
                    'description' => 'Фамилия пользователя.',
                    'resolve' => function (User $user) use ($clientAccess) {
                        if ($user instanceof Client && !$clientAccess->canViewClient($user)) {
                            return null;
                        }

                        return $user->getSurname();
                    },
                ],
                'patronymic' => [
                    'type' => Type::string(),
                    'description' => 'Отчество пользователя.',
                    'resolve' => function (User $user) use ($clientAccess) {
                        if ($user instanceof Client && !$clientAccess->canViewClient($user)) {
                            return null;
                        }

                        return $user->getPatronymic();
                    },
                ],
            ],
        ];

        parent::__construct($config);
    }
}