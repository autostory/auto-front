<?php

namespace AH\GraphQL;

use AH\GraphQL\Input\OfficeWorkTimeInputType;
use AH\GraphQL\Input\ScheduleInputType;
use AH\GraphQL\Input\ServiceMaterialInputType;
use AH\GraphQL\Input\ServiceWorkInputType;
use AH\GraphQL\Input\ServiceWorkStatusInputType;

class Inputs
{
    private static $serviceWorkInputType;
    private static $serviceMaterialInputType;
    private static $officeWorkTimeInputType;
    private static $scheduleInputType;
    private static $serviceWorkStatusInputType;

    /**
     * @return ServiceWorkInputType
     */
    public static function serviceWorkInputType()
    {
        if (self::$serviceWorkInputType) {
            return self::$serviceWorkInputType;
        }

        return (self::$serviceWorkInputType = new ServiceWorkInputType());
    }

    /**
     * @return ServiceMaterialInputType
     */
    public static function serviceMaterialInputType()
    {
        if (self::$serviceMaterialInputType) {
            return self::$serviceMaterialInputType;
        }

        return (self::$serviceMaterialInputType = new ServiceMaterialInputType());
    }

    /**
     * @return OfficeWorkTimeInputType
     */
    public static function officeWorkTimeInputType()
    {
        if (self::$officeWorkTimeInputType) {
            return self::$officeWorkTimeInputType;
        }

        return (self::$officeWorkTimeInputType = new OfficeWorkTimeInputType());
    }

    /**
     * @return ScheduleInputType
     */
    public static function scheduleInputType()
    {
        if (self::$scheduleInputType) {
            return self::$scheduleInputType;
        }

        return (self::$scheduleInputType = new ScheduleInputType());
    }

    /**
     * @return ServiceWorkStatusInputType
     */
    public static function serviceWorkStatusInputType()
    {
        if (self::$serviceWorkStatusInputType) {
            return self::$serviceWorkStatusInputType;
        }

        return (self::$serviceWorkStatusInputType = new ServiceWorkStatusInputType());
    }
}
