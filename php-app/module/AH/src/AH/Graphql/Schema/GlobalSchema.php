<?php

namespace AH\Graphql\Schema;

use AH\GraphQL\Types;
use GraphQL\Type\Schema;

use Zend\ServiceManager\ServiceManager;

class GlobalSchema
{
    public static function getSchema(ServiceManager $serviceManager)
    {
        $schema = new Schema([
            'query' => Types::query($serviceManager),
            'mutation' => Types::mutation($serviceManager),
        ]);

        return $schema;
    }
}
