<?php

namespace AH\Graphql\Type;

use AH\Entity\Client;
use AH\Entity\OrderPayment;
use AH\Repository\OrderPaymentRepository;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Zend\ServiceManager\ServiceManager;

class ClientPaymentStatType extends ObjectType
{
    const TYPE_NAME = 'ClientPaymentStat';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Статистическая информация о платежах клиента.',
            'fields' => function () use ($serviceManager) {
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                return [
                    'averageBill' => [
                        'type' => Type::nonNull(Type::float()),
                        'description' => 'Средний чек.',
                        'resolve' => function (Client $client) use ( $entityManager) {
                            /** @var OrderPaymentRepository $rOrderPayment */
                            $rOrderPayment = $entityManager->getRepository(OrderPayment::class);

                            return $rOrderPayment->getPaymentAvgForClient($client);
                        },
                    ],
                    'sumBill' => [
                        'type' => Type::nonNull(Type::float()),
                        'description' => 'Суммарный чек.',
                        'resolve' => function (Client $client) use ( $entityManager) {
                            /** @var OrderPaymentRepository $rOrderPayment */
                            $rOrderPayment = $entityManager->getRepository(OrderPayment::class);

                            return $rOrderPayment->getPaymentSumForClient($client);
                        },
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}