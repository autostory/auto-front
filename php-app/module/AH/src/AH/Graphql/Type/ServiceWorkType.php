<?php

namespace AH\Graphql\Type;

use AH\Entity\Service\ServiceWork;
use AH\GraphQL\Enums;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class ServiceWorkType extends ObjectType
{
    const TYPE_NAME = 'ServiceWork';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Единица услуги оказанной в рамках сервиса.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор офиса.',
                        'resolve' => function (ServiceWork $serviceWork) {
                            return Relay::toGlobalId(self::TYPE_NAME, $serviceWork->getId());
                        },
                    ],
                    'sparePartNode' => [
                        'type' => Types::sparePartNodeType(),
                        'description' => 'Узел затронутый в работе.',
                        'resolve' => function (ServiceWork $serviceWork) {
                            return $serviceWork->getSparePartNode();
                        },
                    ],
                    'amount' => [
                        'type' => Type::nonNull(Type::float()),
                        'description' => 'Кол-во услуги.',
                        'resolve' => function (ServiceWork $serviceWork) {
                            return $serviceWork->getAmount();
                        },
                    ],
                    'schedules' => [
                        'type' => Type::listOf(Types::boxScheduleType($serviceManager)),
                        'resolve' => function (ServiceWork $serviceWork) {
                            return $serviceWork->getSchedules();
                        },
                    ],
                    'status' => [
                        'type' => Type::nonNull(Enums::serviceWorkStatusEnumType()),
                        'description' => 'Статуст работы.',
                        'resolve' => function (ServiceWork $serviceWork) {
                            return $serviceWork->getStatus();
                        },
                    ],
                    'shortDescription' => [
                        'type' => Type::nonNull(Type::string()),
                        'description' => 'Короткое описание работы.',
                        'resolve' => function (ServiceWork $serviceWork) {
                            $nodeTitle = '';

                            if (!empty($serviceWork->getSparePartNode())) {
                                $nodeTitle = $serviceWork->getSparePartNode()->getName();
                            }

                            return implode(' ', [$nodeTitle, $serviceWork->getNote(), $serviceWork->getTotalCost() . 'р.']);
                        },
                    ],
                    'totalCost' => [
                        'type' => Type::nonNull(Type::float()),
                        'description' => 'Стоимость позиции.',
                        'resolve' => function (ServiceWork $serviceWork) {
                            return $serviceWork->getTotalCost();
                        },
                    ],
                    'costPerUnit' => [
                        'type' => Type::nonNull(Type::float()),
                        'description' => 'Стоимость единицы услуги.',
                        'resolve' => function (ServiceWork $serviceWork) {
                            return $serviceWork->getCostPerUnit();
                        },
                    ],
                    'action' => [
                        'type' => Types::serviceWorkActionTypeType(),
                        'description' => 'Выполненное действие.',
                        'resolve' => function (ServiceWork $serviceWork) {
                            return $serviceWork->getServiceWorkActionType();
                        },
                    ],
                    'discountPercent' => [
                        'type' => Type::nonNull(Type::float()),
                        'resolve' => function (ServiceWork $serviceWork) {
                            return $serviceWork->getDiscountPercent();
                        },
                    ],
                    'note' => [
                        'type' => Type::string(),
                        'description' => 'Заметка.',
                        'resolve' => function (ServiceWork $serviceWork) {
                            return $serviceWork->getNote();
                        },
                    ],
                    'laborHour' => [
                        'type' => Type::nonNull(Type::float()),
                        'description' => 'Норма час.',
                        'resolve' => function (ServiceWork $serviceWork) {
                            return $serviceWork->getLaborHour();
                        },
                    ],
                    'master' => [
                        'type' => Types::managerType($serviceManager),
                        'description' => 'Мастер выполняющий работу.',
                        'resolve' => function (ServiceWork $serviceWork) {
                            return $serviceWork->getMaster();
                        },
                    ],
                    'doneAt' => [
                        'type' => Types::dateType(),
                        'description' => 'Когда работа была фактически выполнена.',
                        'resolve' => function (ServiceWork $serviceWork) {
                            // Сейчас это будет дата смены статуса заказа на выполнено
                            return $serviceWork->getOrder()->getDoneDate();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}