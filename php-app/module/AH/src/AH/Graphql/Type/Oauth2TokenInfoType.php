<?php

namespace AH\Graphql\Type;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class Oauth2TokenInfoType extends ObjectType
{
    const TYPE_NAME = 'Oauth2TokenInfo';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Oauth2 token info.',
            'fields' => function () {
                return [
                    'access_token' => Type::string(),
                    'expires_in' => Type::string(),
                    'refresh_token' => Type::string(),
                ];
            },
        ];

        parent::__construct($config);
    }
}