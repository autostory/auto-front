<?php

namespace AH\Graphql\Type\CatalogCar;

use AH\Entity\CatalogCar;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class CatalogCarCharacteristicType extends ObjectType
{
    const TYPE_NAME = 'CatalogCarCharacteristicType';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Тип характеристики транспорта.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function (CatalogCar\CarCharacteristic $carCharacteristic) {
                            return Relay::toGlobalId(self::TYPE_NAME, $carCharacteristic->getId());
                        },
                    ],
                    'name' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (CatalogCar\CarCharacteristic $carCharacteristic) {
                            $carCharacteristic->getIdParent();
                            return $carCharacteristic->getName();
                        },
                    ],
                    'parentCarCharacteristic' => [
                        'type' => Types::catalogCarCharacteristicType($serviceManager),
                        'resolve' => function (CatalogCar\CarCharacteristic $carCharacteristic) use ($serviceManager) {
                            /** @var \Doctrine\ORM\EntityManager $entityManager */
                            $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                            if (!empty($carCharacteristic->getIdParent())) {
                                return $entityManager->find(
                                    CatalogCar\CarCharacteristic::class,
                                    $carCharacteristic->getIdParent()
                                );
                            }

                            return null;
                        },
                    ],
                    'catalogCarType' => [
                        'type' => Types::catalogCarType($serviceManager),
                        'resolve' => function (CatalogCar\CarCharacteristic $carCharacteristic) {
                            return $carCharacteristic->getCarType();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}