<?php

namespace AH\Graphql\Type\CatalogCar;

use AH\Entity\CatalogCar;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class CatalogCarMarkType extends ObjectType
{
    const TYPE_NAME = 'CatalogCarMark';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Марка транспорта.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function (CatalogCar\CarMark $catalogCarMark) {
                            return Relay::toGlobalId(self::TYPE_NAME, $catalogCarMark->getId());
                        },
                    ],
                    'name' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (CatalogCar\CarMark $catalogCarMark) {
                            return $catalogCarMark->getName();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}