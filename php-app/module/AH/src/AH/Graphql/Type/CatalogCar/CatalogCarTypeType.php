<?php

namespace AH\Graphql\Type\CatalogCar;

use AH\Entity\CatalogCar\CarType;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class CatalogCarTypeType extends ObjectType
{
    const TYPE_NAME = 'CatalogCarType';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Вид транспорта.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function (CarType $carType) {
                            return Relay::toGlobalId(self::TYPE_NAME, $carType->getId());
                        },
                    ],
                    'name' => [
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function (CarType $carType) {
                            return $carType->getName();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}