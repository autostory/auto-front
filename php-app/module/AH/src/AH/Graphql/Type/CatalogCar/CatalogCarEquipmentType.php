<?php

namespace AH\Graphql\Type\CatalogCar;

use AH\Entity\CatalogCar;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class CatalogCarEquipmentType extends ObjectType
{
    const TYPE_NAME = 'CatalogCarEquipmentType';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Комплектация транспорта.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function (CatalogCar\CarEquipment $carEquipment) {
                            return Relay::toGlobalId(self::TYPE_NAME, $carEquipment->getId());
                        },
                    ],
                    'name' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (CatalogCar\CarEquipment $carEquipment) {
                            return $carEquipment->getName();
                        },
                    ],
                    'catalogCarModification' => [
                        'type' => Types::catalogCarModificationType($serviceManager),
                        'resolve' => function (CatalogCar\CarEquipment $carEquipment) {
                            return $carEquipment->getCarModification();
                        },
                    ],
                    'year' => [
                        'type' => Type::int(),
                        'resolve' => function (CatalogCar\CarEquipment $carEquipment) {
                            return $carEquipment->getYear();
                        },
                    ],
                    'catalogCarType' => [
                        'type' => Types::catalogCarType($serviceManager),
                        'resolve' => function (CatalogCar\CarEquipment $carEquipment) {
                            return $carEquipment->getCarType();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}