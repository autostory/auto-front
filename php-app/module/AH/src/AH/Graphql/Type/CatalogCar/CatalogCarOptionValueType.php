<?php

namespace AH\Graphql\Type\CatalogCar;

use AH\Entity\CatalogCar;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class CatalogCarOptionValueType extends ObjectType
{
    const TYPE_NAME = 'CatalogCarOptionValueType';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Значение опции транспорта.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function (CatalogCar\CarOptionValue $carOptionValue) {
                            return Relay::toGlobalId(self::TYPE_NAME, $carOptionValue->getId());
                        },
                    ],
                    'catalogCarOption' => [
                        'type' => Types::catalogCarOptionType($serviceManager),
                        'resolve' => function (CatalogCar\CarOptionValue $carOptionValue) {
                            return $carOptionValue->getCarOption();
                        },
                    ],
                    'catalogCarEquipment' => [
                        'type' => Types::catalogCarEquipmentType($serviceManager),
                        'resolve' => function (CatalogCar\CarOptionValue $carOptionValue) {
                            return $carOptionValue->getCarEquipment();
                        },
                    ],
                    'isBase' => [
                        'type' => Type::boolean(),
                        'resolve' => function (CatalogCar\CarOptionValue $carOptionValue) {
                            return $carOptionValue->getIsBase();
                        },
                    ],
                    'catalogCarType' => [
                        'type' => Types::catalogCarType($serviceManager),
                        'resolve' => function (CatalogCar\CarOptionValue $carOptionValue) {
                            return $carOptionValue->getCarType();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}