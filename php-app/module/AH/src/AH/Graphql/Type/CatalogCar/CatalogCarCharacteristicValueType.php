<?php

namespace AH\Graphql\Type\CatalogCar;

use AH\Entity\CatalogCar;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class CatalogCarCharacteristicValueType extends ObjectType
{
    const TYPE_NAME = 'CatalogCarCharacteristicValueType';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Характеристика транспорта.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function (CatalogCar\CarCharacteristicValue $carCharacteristicValue) {
                            return Relay::toGlobalId(self::TYPE_NAME, $carCharacteristicValue->getId());
                        },
                    ],
                    'value' => [
                        'type' => Type::string(),
                        'resolve' => function (CatalogCar\CarCharacteristicValue $carCharacteristicValue) {
                            return $carCharacteristicValue->getValue();
                        },
                    ],
                    'catalogCarModification' => [
                        'type' => Types::catalogCarModificationType($serviceManager),
                        'resolve' => function (CatalogCar\CarCharacteristicValue $carCharacteristicValue) {
                            return $carCharacteristicValue->getCarModification();
                        },
                    ],
                    'catalogCarCharacteristic' => [
                        'type' => Types::catalogCarCharacteristicType($serviceManager),
                        'resolve' => function (CatalogCar\CarCharacteristicValue $carCharacteristicValue) {
                            return $carCharacteristicValue->getCarCharacteristic();
                        },
                    ],
                    'unit' => [
                        'type' => Type::string(),
                        'resolve' => function (CatalogCar\CarCharacteristicValue $carCharacteristicValue) {
                            return $carCharacteristicValue->getUnit();
                        },
                    ],
                    'catalogCarType' => [
                        'type' => Types::catalogCarType($serviceManager),
                        'resolve' => function (CatalogCar\CarCharacteristicValue $carCharacteristicValue) {
                            return $carCharacteristicValue->getCarType();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}