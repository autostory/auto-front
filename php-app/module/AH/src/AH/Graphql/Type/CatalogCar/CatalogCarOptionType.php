<?php

namespace AH\Graphql\Type\CatalogCar;

use AH\Entity\CatalogCar;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class CatalogCarOptionType extends ObjectType
{
    const TYPE_NAME = 'CatalogCarOptionType';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Тип опции транспорта.',
            'fields' => function () use ($serviceManager) {
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function (CatalogCar\CarOption $carOption) {
                            return Relay::toGlobalId(self::TYPE_NAME, $carOption->getId());
                        },
                    ],
                    'name' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (CatalogCar\CarOption $carOption) {
                            return $carOption->getName();
                        },
                    ],
                    'catalogCarType' => [
                        'type' => Types::catalogCarType($serviceManager),
                        'resolve' => function (CatalogCar\CarOption $carOption) {
                            return $carOption->getCarType();
                        },
                    ],
                    'parentCatalogCarOption' => [
                        'type' => Types::catalogCarOptionType($serviceManager),
                        'resolve' => function (CatalogCar\CarOption $carOption) use ($entityManager) {
                            if (!empty($carOption->getIdParent())) {
                                return $entityManager->find(
                                    CatalogCar\CarOption::class,
                                    $carOption->getIdParent()
                                );
                            }

                            return null;
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}