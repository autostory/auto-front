<?php

namespace AH\Graphql\Type\CatalogCar;

use AH\Entity\CatalogCar;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class CatalogCarGenerationType extends ObjectType
{
    const TYPE_NAME = 'CatalogCarGenerationType';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Поколение транспорта.',
            'fields' => function () use ($serviceManager) {
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function (CatalogCar\CarGeneration $carGeneration) {
                            return Relay::toGlobalId(self::TYPE_NAME, $carGeneration->getId());
                        },
                    ],
                    'name' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (CatalogCar\CarGeneration $carGeneration) {
                            return $carGeneration->getName();
                        },
                    ],
                    'yearBegin' => [
                        'type' => Type::string(),
                        'resolve' => function (CatalogCar\CarGeneration $carGeneration) {
                            return $carGeneration->getYearBegin();
                        },
                    ],
                    'yearEnd' => [
                        'type' => Type::string(),
                        'resolve' => function (CatalogCar\CarGeneration $carGeneration) {
                            return $carGeneration->getYearEnd();
                        },
                    ],
                    'catalogCarSeries' => [
                        'type' => Type::listOf(Types::catalogCarSerie($serviceManager)),
                        'resolve' => function (CatalogCar\CarGeneration $carGeneration) use ($entityManager) {
                            $rCarSerie = $entityManager->getRepository(CatalogCar\CarSerie::class);
                            $generationCarSeries = $rCarSerie->findBy(
                                ['carGeneration' => $carGeneration]
                            );

                            return $generationCarSeries;
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}