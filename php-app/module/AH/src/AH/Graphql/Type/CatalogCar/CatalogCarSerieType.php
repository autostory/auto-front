<?php

namespace AH\Graphql\Type\CatalogCar;

use AH\Entity\CatalogCar;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class CatalogCarSerieType extends ObjectType
{
    const TYPE_NAME = 'CatalogCarSerieType';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Кузов транспорта.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function (CatalogCar\CarSerie $carSerie) {
                            return Relay::toGlobalId(self::TYPE_NAME, $carSerie->getId());
                        },
                    ],
                    'name' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (CatalogCar\CarSerie $carSerie) {
                            return $carSerie->getName();
                        },
                    ],
                    'catalogCarType' => [
                        'type' => Types::catalogCarType($serviceManager),
                        'resolve' => function (CatalogCar\CarSerie $carSerie) {
                            $carSerie->getCarModel();
                            return $carSerie->getCarType();
                        },
                    ],
                    'catalogCarGeneration' => [
                        'type' => Types::catalogCarGeneration($serviceManager),
                        'resolve' => function (CatalogCar\CarSerie $carSerie) {
                            $carSerie->getCarModel();
                            return $carSerie->getCarGeneration();
                        },
                    ],
                    'catalogCarModel' => [
                        'type' => Types::catalogCarModel($serviceManager),
                        'resolve' => function (CatalogCar\CarSerie $carSerie) {
                            $carSerie->getCarModel();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}