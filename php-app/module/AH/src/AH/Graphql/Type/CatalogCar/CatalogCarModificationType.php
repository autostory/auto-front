<?php

namespace AH\Graphql\Type\CatalogCar;

use AH\Entity\CatalogCar;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class CatalogCarModificationType extends ObjectType
{
    const TYPE_NAME = 'CatalogCarModificationType';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Модификация транспорта.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function (CatalogCar\CarModification $carModification) {
                            return Relay::toGlobalId(self::TYPE_NAME, $carModification->getId());
                        },
                    ],
                    'name' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (CatalogCar\CarModification $carModification) {
                            return $carModification->getName();
                        },
                    ],
                    'catalogCarModel' => [
                        'type' => Types::catalogCarModel($serviceManager),
                        'resolve' => function (CatalogCar\CarModification $carModification) {
                            return $carModification->getCarModel();
                        },
                    ],
                    'catalogCarSerie' => [
                        'type' => Types::catalogCarSerie($serviceManager),
                        'resolve' => function (CatalogCar\CarModification $carModification) {
                            return $carModification->getCarSerie();
                        },
                    ],
                    'catalogCarType' => [
                        'type' => Types::catalogCarType($serviceManager),
                        'resolve' => function (CatalogCar\CarModification $carModification) {
                            return $carModification->getCarType();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}