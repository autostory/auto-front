<?php

namespace AH\Graphql\Type;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class AccessRightsType extends ObjectType
{
    const TYPE_NAME = 'AccessRights';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Права доступа аутентифицированного пользователя.',
            'fields' => function() {
                return [
                    'canCreateUserAsManager' => [
                        'type' => Type::nonNull(Type::boolean()),
                        'description' => 'Может ли создавать клиентов как менеджер.',
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}