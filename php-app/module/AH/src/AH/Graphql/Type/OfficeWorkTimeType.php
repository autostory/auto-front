<?php

namespace AH\Graphql\Type;

use AH\Entity\OfficeWorkTime;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class OfficeWorkTimeType extends ObjectType
{
    const TYPE_NAME = 'OfficeWorkTime';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор офиса.',
                        'resolve' => function (OfficeWorkTime $officeWorkTime) {
                            return Relay::toGlobalId(self::TYPE_NAME, $officeWorkTime->getId());
                        },
                    ],
                    'office' => [
                        'type' => Type::nonNull(Types::officeType($serviceManager)),
                        'description' => 'Офис.',
                        'resolve' => function (OfficeWorkTime $officeWorkTime) {
                            return $officeWorkTime->getOffice();
                        },
                    ],
                    'start' => [
                        'type' => Types::dateType(),
                        'resolve' => function (OfficeWorkTime $officeWorkTime) {
                            return $officeWorkTime->getStart();
                        },
                    ],
                    'end' => [
                        'type' => Types::dateType(),
                        'resolve' => function (OfficeWorkTime $officeWorkTime) {
                            return $officeWorkTime->getEnd();
                        },
                    ],
                    'dayOfWeek' => [
                        'type' => Type::nonNull(Type::int()),
                        'resolve' => function (OfficeWorkTime $officeWorkTime) {
                            return $officeWorkTime->getDayOfWeek();
                        },
                    ],
                    'holiday' => [
                        'type' => Type::nonNull(Type::boolean()),
                        'description' => 'Выходной.',
                        'resolve' => function (OfficeWorkTime $officeWorkTime) {
                            return $officeWorkTime->getHoliday();
                        },
                    ],
                    'roundTheClock' => [
                        'type' => Type::nonNull(Type::boolean()),
                        'description' => 'Весь день.',
                        'resolve' => function (OfficeWorkTime $officeWorkTime) {
                            return $officeWorkTime->getRoundTheClock();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}