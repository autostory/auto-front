<?php
namespace AH\Graphql\Type;

use AH\GraphQL\Mutations;
use GraphQL\Type\Definition\ObjectType;
use Zend\ServiceManager\ServiceManager;

class MutationType extends ObjectType
{
    const TYPE_NAME = 'Mutation';

    public function __construct(ServiceManager $serviceManager)
    {
        // TODO: Надо групировать мутации и делать nested. https://github.com/webonyx/graphql-php/issues/189
        $config = [
            'name' => self::TYPE_NAME,
            'fields' => [
                'login' => Mutations::login($serviceManager),
                'autoLogin' => Mutations::autoLoginMutation($serviceManager),
                'logout' => Mutations::logout($serviceManager),
                'tokenRefresh' => Mutations::tokenRefresh($serviceManager),
                'activateAccount' => Mutations::activateAccount($serviceManager),
                'clientRegistration' => Mutations::clientRegistration($serviceManager),
                'firmRegistration' => Mutations::firmRegistration($serviceManager),

                'sendPasswordRestoreMessage' => Mutations::sendPasswordRestoreMessage($serviceManager),
                'passwordRestoreUsingSecretKey' => Mutations::passwordRestoreUsingSecretKey($serviceManager),

                'createClientAsManager' => Mutations::createClientAsManager($serviceManager),

                'createManagerMutation' => Mutations::createManagerMutation($serviceManager),
                'editManagerMutation' => Mutations::editManagerMutation($serviceManager),
                'deleteManagerMutation' => Mutations::deleteManagerMutation($serviceManager),
                'restoreManagerMutation' => Mutations::restoreManagerMutation($serviceManager),

                'createOffice' => Mutations::createOffice($serviceManager),
                'editOffice' => Mutations::editOffice($serviceManager),

                'changeMyPassword' => Mutations::changeMyPassword($serviceManager),
                'editMyProfileMutation' => Mutations::editMyProfile($serviceManager),

                'editOfficeBox' => Mutations::editOfficeBox($serviceManager),
                'createOfficeBox' => Mutations::createOfficeBox($serviceManager),
                'addRequestCommunicationVisitResult' => Mutations::addRequestCommunicationVisitResult($serviceManager),
                'addRequestCommunicationRecallResult' => Mutations::addRequestCommunicationRecallResult($serviceManager),
                'addRequestCommunicationNoAnswerResult' => Mutations::addRequestCommunicationNoAnswerResult($serviceManager),
                'addRequestCommunicationRejectResult' => Mutations::addRequestCommunicationRejectResult($serviceManager),
                'createOrderAsManager' => Mutations::createOrderAsManagerMutation($serviceManager),
                'addCarToClient' => Mutations::addCarToClientMutation($serviceManager),
                'editCarMutation' => Mutations::editCarMutation($serviceManager),

                'editOrderMainInfoMutation' => Mutations::editOrderMainInfoMutation($serviceManager),
                'addOrderPaymentMutation' => Mutations::addOrderPaymentMutation($serviceManager),
                'editOrderServiceWorksMutation' => Mutations::editOrderServiceWorksMutation($serviceManager),
                'editOrderServiceMaterialsMutation' => Mutations::editOrderServiceMaterialsMutation($serviceManager),
                'editOrderSchedulesMutation' => Mutations::editOrderSchedulesMutation($serviceManager),
                'editOrderCarArriveInfoMutation' => Mutations::editOrderCarArriveInfoMutation($serviceManager),
                'setScheduleStatusMutation' => Mutations::setScheduleStatusMutation($serviceManager),
                'setScheduleServiceWorksStatusMutation' => Mutations::setScheduleServiceWorksStatusMutation($serviceManager),
                'rejectOrderMutation' => Mutations::rejectOrderMutation($serviceManager),
                'notifyClientAboutOrderWorkCompletedMutation' => Mutations::notifyClientAboutOrderWorkCompletedMutation($serviceManager),
            ],
        ];

        parent::__construct($config);
    }
}
