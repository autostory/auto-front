<?php

namespace AH\Graphql\Type;

use AH\Core\Acl\CarAccess;
use AH\Entity\Car;
use AH\Entity\CarMileage;
use AH\Entity\CatalogCar\CarCharacteristicValue;
use AH\Entity\CatalogCar\CarOptionValue;
use AH\Entity\Service\ServiceMaterial;
use AH\Entity\Service\ServiceWork;
use AH\GraphQL\Types;
use AH\Repository\ServiceMaterialRepository;
use AH\Repository\ServiceWorkRepository;
use Doctrine\Common\Collections\ArrayCollection;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class CarType extends ObjectType
{
    const TYPE_NAME = 'Car';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Car info.',
            'fields' => function () use ($serviceManager) {
                /** @var CarAccess $carAccess */
                $carAccess = $serviceManager->get(CarAccess::class);

                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор автомобиля.',
                        'resolve' => function (Car $car) use ($carAccess) {
                            if (!$carAccess->canViewCar($car)) {
                                return null;
                            }

                            return Relay::toGlobalId(self::TYPE_NAME, $car->getId());
                        },
                    ],
                    'vin' => [
                        'type' => Type::string(),
                        'description' => 'VIN номер автомобиля.',
                        'resolve' => function (Car $car) use ($carAccess) {
                            if (!$carAccess->canViewCar($car)) {
                                return null;
                            }

                            return strtoupper($car->getVin());
                        },
                    ],
                    'year' => [
                        'type' => Type::int(),
                        'description' => 'Год выпуска автомобиля.',
                        'resolve' => function (Car $car) use ($carAccess) {
                            if (!$carAccess->canViewCar($car)) {
                                return null;
                            }

                            return $car->getYear();
                        },
                    ],
                    'color' => [
                        'type' => Type::string(),
                        'description' => 'Цвет автомобиля.',
                        'resolve' => function (Car $car) use ($carAccess) {
                            if (!$carAccess->canViewCar($car)) {
                                return null;
                            }

                            return $car->getColor();
                        },
                    ],
                    'registrationSignNumber' => [
                        'type' => Type::string(),
                        'description' => 'Регистрационный номер автомобиля.',
                        'resolve' => function (Car $car) use ($carAccess) {
                            if (!$carAccess->canViewCar($car)) {
                                return null;
                            }

                            return strtoupper($car->getRegistrationSignNumber());
                        },
                    ],
                    'serviceBookRecords' => [
                        'description' => 'История всех работ и материалов по клиенту.',
                        'resolve' => function (Car $car) use ($entityManager, $carAccess) {
                            if (!$carAccess->canViewCar($car)) {
                                return null;
                            }

                            /** @var ServiceWorkRepository $rServiceWork */
                            $rServiceWork = $entityManager->getRepository(ServiceWork::class);
                            $allServiceWorks = $rServiceWork->getDoneOrdersServiceWorksByCar($car);

                            /** @var ServiceMaterialRepository $rServiceMaterial */
                            $rServiceMaterial = $entityManager->getRepository(ServiceMaterial::class);
                            $allServiceMaterials = $rServiceMaterial->getDoneOrdersServiceMaterialByCar($car);

                            $allRecords = new ArrayCollection(
                                array_merge($allServiceWorks->toArray(), $allServiceMaterials->toArray())
                            );

                            $iterator = $allRecords->getIterator();
                            $iterator->uasort(function ($a, $b) {
                                return ($a->getId() < $b->getId()) ? -1 : 1;
                            });

                            return new ArrayCollection(iterator_to_array($iterator));
                        },
                        'type' => Type::listOf(new UnionType([
                            'name' => 'CarServiceBookRecords',
                            'types' => [
                                Types::serviceWorkType($serviceManager),
                                Types::serviceMaterialType($serviceManager),
                            ],
                            'resolveType' => function ($item) use ($serviceManager) {
                                if ($item instanceof ServiceWork) {
                                    return Types::serviceWorkType($serviceManager);
                                } else if ($item instanceof ServiceMaterial) {
                                    return Types::serviceMaterialType($serviceManager);
                                }

                                return null;
                            }
                        ])),
                    ],
                    'displayName' => [
                        'type' => Type::nonNull(Type::string()),
                        'description' => 'Название марки, модели и серии автомобиля',
                        'resolve' => function (Car $car) use ($carAccess) {
                            if (!$carAccess->canViewCar($car)) {
                                return null;
                            }

                            $mark = $car->getCatalogCarMark();
                            $model = $car->getCatalogCarModel();
                            $modification = $car->getCatalogCarModification();

                            $displayName = '';
                            if (!empty($mark)) {
                                $displayName = $mark->getName();
                            }

                            if (!empty($model)) {
                                $displayName = $displayName . ' ' . $model->getName();
                            }

                            if (!empty($modification)) {
                                $displayName = $displayName . ' ' . $modification->getName();
                            }

                            $year = $car->getYear();
                            if (!empty($year)) {
                                $displayName = $displayName . ' ' . $year . 'г.';
                            }

                            return $displayName;
                        },
                    ],
                    'engineType' => [
                        'type' => Type::string(),
                        'description' => 'Тип двигателя',
                        'resolve' => function (Car $car) use ($entityManager) {
                            $carModification = $car->getCatalogCarModification();
                            if (empty($carModification)) {
                                return null;
                            }

                            $rCarCharacteristicValue = $entityManager
                                ->getRepository(CarCharacteristicValue::class);

                            $carType = $carModification->getCarType();

                            if (empty($carType)) {
                                return null;
                            }

                            if ($carType->getId() === 1) {
                                /** @var CarCharacteristicValue $carCharacteristicValue */
                                $carCharacteristicValue = $rCarCharacteristicValue
                                    ->findOneBy([
                                        'carModification' => $carModification->getId(),
                                        'carCharacteristic' => 12,
                                    ]);
                            }


                            if ($carType->getId() === 2) {
                                /** @var CarCharacteristicValue $carCharacteristicValue */
                                $carCharacteristicValue = $rCarCharacteristicValue
                                    ->findOneBy([
                                        'carModification' => $carModification->getId(),
                                        'carCharacteristic' => 383,
                                    ]);
                            }


                            if (empty($carCharacteristicValue)) {
                                return null;
                            }

                            return trim(implode(
                                ' ',
                                [
                                    $carCharacteristicValue->getValue(),
                                    $carCharacteristicValue->getUnit()
                                ]
                            ));
                        },
                    ],
                    'bodyType' => [
                        'type' => Type::string(),
                        'description' => 'Кузов',
                        'resolve' => function (Car $car) use ($entityManager) {
                            $carModification = $car->getCatalogCarModification();
                            if (empty($carModification)) {
                                return null;
                            }

                            $rCarCharacteristicValue = $entityManager
                                ->getRepository(CarCharacteristicValue::class);

                            $carType = $carModification->getCarType();

                            if (empty($carType)) {
                                return null;
                            }

                            if ($carType->getId() === 1) {
                                /** @var CarCharacteristicValue $carCharacteristicValue */
                                $carCharacteristicValue = $rCarCharacteristicValue
                                    ->findOneBy([
                                        'carModification' => $carModification->getId(),
                                        'carCharacteristic' => 2,
                                    ]);
                            }

                            if ($carType->getId() === 2) {
                                /** @var CarCharacteristicValue $carCharacteristicValue */
                                $carCharacteristicValue = $rCarCharacteristicValue
                                    ->findOneBy([
                                        'carModification' => $carModification->getId(),
                                        'carCharacteristic' => 307,
                                    ]);
                            }


                            if (empty($carCharacteristicValue)) {
                                return null;
                            }

                            return trim(implode(
                                ' ',
                                [
                                    $carCharacteristicValue->getValue(),
                                    $carCharacteristicValue->getUnit()
                                ]
                            ));
                        },
                    ],
                    'engineCapacity' => [
                        'type' => Type::string(),
                        'description' => 'Объем двигателя',
                        'resolve' => function (Car $car) use ($entityManager) {
                            $carModification = $car->getCatalogCarModification();
                            if (empty($carModification)) {
                                return null;
                            }

                            $rCarCharacteristicValue = $entityManager
                                ->getRepository(CarCharacteristicValue::class);

                            $carType = $carModification->getCarType();

                            if (empty($carType)) {
                                return null;
                            }

                            if ($carType->getId() === 1) {
                                /** @var CarCharacteristicValue $carCharacteristicValue */
                                $carCharacteristicValue = $rCarCharacteristicValue
                                    ->findOneBy([
                                        'carModification' => $carModification->getId(),
                                        'carCharacteristic' => 13,
                                    ]);
                            }


                            if ($carType->getId() === 2) {
                                /** @var CarCharacteristicValue $carCharacteristicValue */
                                $carCharacteristicValue = $rCarCharacteristicValue
                                    ->findOneBy([
                                        'carModification' => $carModification->getId(),
                                        'carCharacteristic' => 379,
                                    ]);
                            }


                            if (empty($carCharacteristicValue)) {
                                return null;
                            }

                            return trim(implode(
                                ' ',
                                [
                                    $carCharacteristicValue->getValue(),
                                    $carCharacteristicValue->getUnit()
                                ]
                            ));
                        },
                    ],
                    'enginePower' => [
                        'type' => Type::string(),
                        'description' => 'Мощность двигателя',
                        'resolve' => function (Car $car) use ($entityManager) {
                            $carModification = $car->getCatalogCarModification();
                            if (empty($carModification)) {
                                return null;
                            }

                            $rCarCharacteristicValue = $entityManager
                                ->getRepository(CarCharacteristicValue::class);

                            $carType = $carModification->getCarType();

                            if (empty($carType)) {
                                return null;
                            }

                            if ($carType->getId() === 1) {
                                /** @var CarCharacteristicValue $carCharacteristicValue */
                                $carCharacteristicValue = $rCarCharacteristicValue
                                    ->findOneBy([
                                        'carModification' => $carModification->getId(),
                                        'carCharacteristic' => 14,
                                    ]);
                            }


                            if ($carType->getId() === 2) {
                                /** @var CarCharacteristicValue $carCharacteristicValue */
                                $carCharacteristicValue = $rCarCharacteristicValue
                                    ->findOneBy([
                                        'carModification' => $carModification->getId(),
                                        'carCharacteristic' => 380,
                                    ]);
                            }


                            if (empty($carCharacteristicValue)) {
                                return null;
                            }

                            return trim(implode(
                                ' ',
                                [
                                    $carCharacteristicValue->getValue(),
                                    $carCharacteristicValue->getUnit()
                                ]
                            ));
                        },
                    ],
                    'engineModel' => [
                        'type' => Type::string(),
                        'description' => 'Модель двигателя',
                        'resolve' => function (Car $car) use ($entityManager) {
                            $carModification = $car->getCatalogCarModification();
                            if (empty($carModification)) {
                                return null;
                            }

                            $rCarCharacteristicValue = $entityManager
                                ->getRepository(CarCharacteristicValue::class);

                            $carType = $carModification->getCarType();

                            if (empty($carType)) {
                                return null;
                            }

                            if ($carType->getId() === 2) {
                                /** @var CarCharacteristicValue $carCharacteristicValue */
                                $carCharacteristicValue = $rCarCharacteristicValue
                                    ->findOneBy([
                                        'carModification' => $carModification->getId(),
                                        'carCharacteristic' => 402,
                                    ]);
                            }


                            if (empty($carCharacteristicValue)) {
                                return null;
                            }

                            return trim(implode(
                                ' ',
                                [
                                    $carCharacteristicValue->getValue(),
                                    $carCharacteristicValue->getUnit()
                                ]
                            ));
                        },
                    ],
                    'catalogCarType' => [
                        'type' => Types::catalogCarType($serviceManager),
                        'resolve' => function (Car $car) {
                            return $car->getCatalogCarType();
                        },
                    ],
                    'catalogCarMark' => [
                        'type' => Types::catalogCarMark($serviceManager),
                        'resolve' => function (Car $car) {
                            return $car->getCatalogCarMark();
                        },
                    ],
                    'catalogCarModel' => [
                        'type' => Types::catalogCarModel($serviceManager),
                        'resolve' => function (Car $car) {
                            return $car->getCatalogCarModel();
                        },
                    ],
                    'catalogCarGeneration' => [
                        'type' => Types::catalogCarGeneration($serviceManager),
                        'resolve' => function (Car $car) {
                            return $car->getCatalogCarGeneration();
                        },
                    ],
                    'catalogCarSerie' => [
                        'type' => Types::catalogCarSerie($serviceManager),
                        'resolve' => function (Car $car) {
                            return $car->getCatalogCarSerie();
                        },
                    ],
                    'catalogCarModification' => [
                        'type' => Types::catalogCarModificationType($serviceManager),
                        'resolve' => function (Car $car) {
                            return $car->getCatalogCarModification();
                        },
                    ],
                    'catalogCarEquipment' => [
                        'type' => Types::catalogCarEquipmentType($serviceManager),
                        'resolve' => function (Car $car) {
                            return $car->getCatalogCarEquipment();
                        },
                    ],
                    'catalogCarCharacteristics' => [
                        'type' => Type::listOf(Types::catalogCarCharacteristicValueType($serviceManager)),
                        'description' => 'Характеристики модификации',
                        'resolve' => function (Car $car) use ($entityManager) {
                            $carModification = $car->getCatalogCarModification();
                            if (empty($carModification)) {
                                return null;
                            }

                            $rCarCharacteristicValue = $entityManager->getRepository(CarCharacteristicValue::class);

                            $carCharacteristicValues = $rCarCharacteristicValue
                                ->findBy(['carModification' => $carModification->getId()]);

                            return $carCharacteristicValues;
                        },
                    ],
                    'catalogCarOptionValues' => [
                        'type' => Type::listOf(Types::catalogCarOptionValueType($serviceManager)),
                        'resolve' => function (Car $car) use ($entityManager) {
                            $equipment = $car->getCatalogCarEquipment();

                            if (empty($equipment)) {
                                return null;
                            }

                            $rCarOptionValue = $entityManager->getRepository(CarOptionValue::class);

                            $carOptionValue = $rCarOptionValue->findBy(['carEquipment' => $equipment->getId()]);

                            return $carOptionValue;
                        },
                    ],
                    'mileages' => [
                        'type' => Type::listOf(Types::carMileageType($serviceManager)),
                        'resolve' => function (Car $car) use ($carAccess) {
                            if (!$carAccess->canViewCar($car)) {
                                return null;
                            }

                            return $car->getMileages();
                        },
                    ],
                    'lastMileage' => [
                        'type' => Types::carMileageType($serviceManager),
                        'resolve' => function (Car $car) use ($carAccess, $entityManager) {
                            if (!$carAccess->canViewCar($car)) {
                                return null;
                            }

                            $rCarMileage = $entityManager->getRepository(CarMileage::class);
                            $carMileage = $rCarMileage->findOneBy(['car' => $car->getId()], ['measuredAt' => 'DESC']);

                            return $carMileage;
                        },
                    ],
                    'orders' => [
                        'type' => Type::listOf(Types::orderType($serviceManager)),
                        'resolve' => function (Car $car) use ($carAccess) {
                            if (!$carAccess->canViewCar($car)) {
                                return null;
                            }

                            return $car->getOrders();
                        },
                    ],
                    'owners' => [
                        'type' => Types::carOwners($serviceManager),
                        'description' => 'Коллекция владельцев автомобиля.',
                        'resolve' => function (Car $car) use ($carAccess) {
                            if (!$carAccess->canViewCar($car)) {
                                return [
                                    'ownersArray' => new ArrayCollection(),
                                ];
                            }

                            return [
                                'ownersArray' => $car->getOwners(),
                            ];
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}