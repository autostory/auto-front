<?php

namespace AH\Graphql\Type\Event;

use AH\Entity\Event\SparePartServiceEvent;
use AH\GraphQL\Interfaces;
use AH\GraphQL\TypeInterface\EventTypeInterface;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class SparePartServiceEventType extends ObjectType
{
    const TYPE_NAME = 'SparePartServiceEvent';

    public function __construct(ServiceManager $serviceManager)
    {
        /** @var EventTypeInterface $eventInterface */
        $eventInterface = Interfaces::eventTypeInterface($serviceManager);

        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Событие сервиса автомобиля связаного с запчастями.',
            'interfaces' => [
                Types::nodeInterface($serviceManager),
                Interfaces::eventTypeInterface($serviceManager),
            ],
            'fields' => function () use ($serviceManager, $eventInterface) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор события заправки.',
                        'resolve' => function (SparePartServiceEvent $sparePartServiceEvent) {
                            return Relay::toGlobalId(self::TYPE_NAME, $sparePartServiceEvent->getId());
                        },
                    ],
                    $eventInterface->getField('car'),
                    $eventInterface->getField('mileage'),
                    $eventInterface->getField('whenDidItHappen'),
                    $eventInterface->getField('createdAt'),
                    $eventInterface->getField('totalCost'),
                    $eventInterface->getField('note'),
                    $eventInterface->getField('initiator'),
                    'organisationName' => [
                        'type' => Type::string(),
                        'description' => 'Название организации где проводился ремонт. Заполняется если не указана фирма из системы.',
                        'resolve' => function (SparePartServiceEvent $sparePartServiceEvent) {
                            $sparePartServiceEvent->getServiceWorks();
                            return $sparePartServiceEvent->getOrganisationName();
                        },
                    ],
                    'works' => [
                        'type' => Type::listOf(Types::serviceWorkType($serviceManager)),
                        'description' => 'Список выполненных работ в рамках сервиса.',
                        'resolve' => function (SparePartServiceEvent $sparePartServiceEvent) {
                            return $sparePartServiceEvent->getServiceWorks();
                        },
                    ],
                    'globalServiceType' => [
                        'type' => Types::globalServiceTypeType(),
                        'description' => 'Тип оказанного сервиса.',
                        'resolve' => function (SparePartServiceEvent $sparePartServiceEvent) {
                            return $sparePartServiceEvent->getGlobalServiceType();
                        },
                    ],
                    'firm' => [
                        'type' => Types::firmType($serviceManager),
                        'description' => 'Фирма которая оказала сервис.',
                        'resolve' => function (SparePartServiceEvent $sparePartServiceEvent) {
                            return $sparePartServiceEvent->getFirm();
                        },
                    ],
                    'office' => [
                        'type' => Types::officeType($serviceManager),
                        'description' => 'Офис фирмы который оказал сервис.',
                        'resolve' => function (SparePartServiceEvent $sparePartServiceEvent) {
                            return $sparePartServiceEvent->getOffice();
                        },
                    ],
                    'selfService' => [
                        'type' => Type::nonNull(Type::boolean()),
                        'description' => 'Сервис выполнил сам владелец техники.',
                        'resolve' => function (SparePartServiceEvent $sparePartServiceEvent) {
                            return $sparePartServiceEvent->getSelfService();
                        },
                    ],
                    'causeOfAccident' => [
                        'type' => Type::nonNull(Type::boolean()),
                        'description' => 'Необходимость сервиса возникла в результате ДТП.',
                        'resolve' => function (SparePartServiceEvent $sparePartServiceEvent) {
                            return $sparePartServiceEvent->getCauseOfAccident();
                        },
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}