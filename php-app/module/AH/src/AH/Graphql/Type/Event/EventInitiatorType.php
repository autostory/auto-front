<?php

namespace AH\Graphql\Type\Event;

use AH\Entity\Client;
use AH\Entity\Manager;
use AH\Entity\User;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\UnionType;
use Zend\ServiceManager\ServiceManager;

class EventInitiatorType extends UnionType
{
    const TYPE_NAME = 'EventInitiator';

    public function __construct(ServiceManager $serviceManager)
    {
        $clientType = Types::clientType($serviceManager);
        $managerType = Types::managerType($serviceManager);

        $config = [
            'name' => self::TYPE_NAME,
            'types' => [
                $clientType,
                $managerType,
            ],
            'resolveType' => function (User $initiator) use ($clientType, $managerType) {
                if ($initiator instanceof Client) {
                    return $clientType;
                }

                if ($initiator instanceof Manager) {
                    return $managerType;
                }

                return null;
            }
        ];

        parent::__construct($config);
    }
}