<?php

namespace AH\Graphql\Type\Event;

use AH\Entity\Event\GasEvent;
use AH\GraphQL\Interfaces;
use AH\GraphQL\TypeInterface\EventTypeInterface;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class GasEventType extends ObjectType
{
    const TYPE_NAME = 'GasEvent';

    public function __construct(ServiceManager $serviceManager)
    {
        /** @var EventTypeInterface $eventInterface */
        $eventInterface = Interfaces::eventTypeInterface($serviceManager);

        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Событие заправки автомобиля.',
            'interfaces' => [
                Types::nodeInterface($serviceManager),
                Interfaces::eventTypeInterface($serviceManager),
            ],
            'fields' => function () use ($serviceManager, $eventInterface) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор события заправки.',
                        'resolve' => function (GasEvent $gasEvent) {
                            return Relay::toGlobalId(self::TYPE_NAME, $gasEvent->getId());
                        },
                    ],
                    $eventInterface->getField('car'),
                    $eventInterface->getField('mileage'),
                    $eventInterface->getField('whenDidItHappen'),
                    $eventInterface->getField('createdAt'),
                    $eventInterface->getField('totalCost'),
                    $eventInterface->getField('note'),
                    $eventInterface->getField('initiator'),
                    'fuelAmount' => [
                        'type' => Type::nonNull(Type::float()),
                        'description' => 'Количество заправленного топлива',
                        'resolve' => function (GasEvent $gasEvent) {
                            return $gasEvent->getFuelAmount();
                        },
                    ],
                    'fullTank' => [
                        'type' => Type::nonNull(Type::boolean()),
                        'description' => 'Была ли эта заправка до полного бака.',
                        'resolve' => function (GasEvent $gasEvent) {
                            return $gasEvent->getFullTank();
                        },
                    ],
                    'oneUnitCost' => [
                        'type' => Type::nonNull(Type::float()),
                        'description' => 'Стоимость единицы топлива.',
                        'resolve' => function (GasEvent $gasEvent) {
                            return $gasEvent->getOneUnitCost();
                        },
                    ],
                    'gasStationName' => [
                        'type' => Type::string(),
                        'description' => 'Название АЗС.',
                        'resolve' => function (GasEvent $gasEvent) {
                            return $gasEvent->getGasStationName();
                        },
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}