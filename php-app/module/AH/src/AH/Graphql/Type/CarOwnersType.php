<?php

namespace AH\Graphql\Type;

use AH\Core\Relay\CustomRelay;
use AH\Entity\Client;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\ServiceManager\ServiceManager;

class CarOwnersType extends ObjectType
{
    const TYPE_NAME = 'CarOwners';

    public function __construct(ServiceManager $serviceManager)
    {
        /** @var \Doctrine\ORM\EntityManager $entityManager */
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');
        $hydrator = new DoctrineHydrator($entityManager);

        $config = [
            'name' => self::TYPE_NAME,
            'interfaces' => [Types::nodeInterface($serviceManager)],
            'description' => 'Владельцы автомобиля.',
            'fields' => function () use ($hydrator, $serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'name' => 'id',
                        'description' => 'Идентификатор набора.',
                        'resolve' => function ($payload) {
                            if (!isset($payload['ownersArray']) || empty($payload['ownersArray'])) {
                                return Relay::toGlobalId(self::TYPE_NAME, 0);
                            }

                            $owners = $payload['ownersArray'];

                            $dataToHash = [];
                            /** @var Client $client */
                            foreach ($owners as $client) {
                                $dataToHash[] = $client->getId();
                                if (!empty($client->getUpdatedAt())) {
                                    $dataToHash[] = $client->getUpdatedAt()->format('U');
                                }
                            }

                            $hash = md5(implode(',', $dataToHash));

                            return Relay::toGlobalId(self::TYPE_NAME, $hash);
                        }
                    ],
                    'itemsCount' => [
                        'name' => 'itemsCount',
                        'description' => 'Количество владельцев автомобиля.',
                        'type' => Type::nonNull(Type::int()),
                        'resolve' => function ($payload) {
                            if (!isset($payload['ownersArray'])) {
                                return 0;
                            }

                            return count($payload['ownersArray']);
                        }
                    ],
                    'ownersList' => [
                        'type' => Types::clientConnection($serviceManager),
                        'description' => 'Коллекция владельцев автомобиля.',
                        'resolve' => function ($payload) use ($hydrator) {
                            return CustomRelay::connectionFromArrayCollection(
                                $payload['ownersArray'],
                                [],
                                ClientType::TYPE_NAME
                            );
                        }
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}