<?php

namespace AH\Graphql\Type;

use AH\Entity\Order;
use AH\Entity\OrderPayment;
use AH\Entity\Service\ServiceMaterial;
use AH\Entity\Service\ServiceWork;
use AH\Graphql\Enum\PaymentTypeEnumType;
use AH\GraphQL\Enums;
use AH\GraphQL\Types;
use AH\Repository\OrderPaymentRepository;
use Doctrine\Common\Collections\Criteria;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class OrderType extends ObjectType
{
    const TYPE_NAME = 'Order';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Заказ клиента.',
            'fields' => function () use ($serviceManager) {
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор.',
                        'resolve' => function (Order $order) {
                            return Relay::toGlobalId(self::TYPE_NAME, $order->getId());
                        },
                    ],
                    'serviceAddress' => [
                        'type' => Type::nonNull(Type::string()),
                        'description' => 'Название фирмы, офиса и адрес офиса.',
                        'resolve' => function (Order $order) {
                            $firm = $order->getFirm();
                            $office = $order->getOffice();

                            $data = [
                                $firm->getName(),
                                $office->getTitle(),
                                $office->getAddress(),
                                $office->getPhone()
                            ];

                            return implode(', ',$data);
                        },
                    ],
                    'firm' => [
                        'type' => Type::nonNull(Types::firmType($serviceManager)),
                        'resolve' => function (Order $order) {
                            return $order->getFirm();
                        },
                    ],
                    'payments' => [
                        'type' => Type::listOf(Types::orderPaymentType($serviceManager)),
                        'description' => 'Оплаты по заказу.',
                        'resolve' => function (Order $order) {
                            return $order->getPayments();
                        },
                    ],
                    'paymentsSum' => [
                        'type' => Type::nonNull(Type::float()),
                        'description' => 'Сумма оплат по заказу.',
                        'resolve' => function (Order $order) use ($entityManager) {
                            /** @var OrderPaymentRepository $rOrderPayments */
                            $rOrderPayments = $entityManager->getRepository(OrderPayment::class);

                            return $rOrderPayments->getPaymentSumForOrder($order);
                        },
                    ],
                    'totalBill' => [
                        'type' => Type::nonNull(Type::float()),
                        'description' => 'Суммарный счет по заказу.',
                        'resolve' => function (Order $order) {
                            $totalBill = 0;
                            $serviceWorks = $order->getServiceWorks();
                            if (!empty($serviceWorks)) {
                                /** @var ServiceWork $serviceWork */
                                foreach ($serviceWorks as $serviceWork) {
                                    $totalBill = $totalBill + $serviceWork->getTotalCost();
                                }
                            }

                            $serviceMaterials = $order->getServiceMaterials();
                            if (!empty($serviceMaterials)) {
                                /** @var ServiceMaterial $serviceMaterial */
                                foreach ($serviceMaterials as $serviceMaterial) {
                                    $totalBill = $totalBill + $serviceMaterial->getTotalCost();
                                }
                            }

                            return round($totalBill,2);
                        },
                    ],
                    'serviceWorkTotalBill' => [
                        'type' => Type::nonNull(Type::float()),
                        'description' => 'Суммарный счет только по работам заказа, без материалов.',
                        'resolve' => function (Order $order) {
                            $serviceWorkTotalBill = 0;
                            $serviceWorks = $order->getServiceWorks();
                            if (!empty($serviceWorks)) {
                                /** @var ServiceWork $serviceWork */
                                foreach ($serviceWorks as $serviceWork) {
                                    $serviceWorkTotalBill = $serviceWorkTotalBill + $serviceWork->getTotalCost();
                                }
                            }

                            return round($serviceWorkTotalBill,2);
                        },
                    ],
                    'clientWillReceivePointAmount' => [
                        'type' => Type::nonNull(Type::float()),
                        'description' => 'Сколько балов получит клиент после выполнения заказа.',
                        'resolve' => function (Order $order) {
                            $serviceWorksBill = 0;
                            $serviceWorks = $order->getServiceWorks();
                            if (!empty($serviceWorks)) {
                                /** @var ServiceWork $serviceWork */
                                foreach ($serviceWorks as $serviceWork) {
                                    $serviceWorksBill = $serviceWorksBill + $serviceWork->getTotalCost();
                                }
                            }

                            $serviceMaterialsBill = 0;
                            $serviceMaterials = $order->getServiceMaterials();
                            if (!empty($serviceMaterials)) {
                                /** @var ServiceMaterial $serviceMaterial */
                                foreach ($serviceMaterials as $serviceMaterial) {
                                    $serviceMaterialsBill = $serviceMaterialsBill + $serviceMaterial->getTotalCost();
                                }
                            }

                            $pointPayments = $order->getPayments()->filter(function (OrderPayment $payment) {
                                return $payment->getPaymentType() === PaymentTypeEnumType::POINT_VALUE;
                            });

                            $pointPaymentSum = 0;

                            if (!empty($pointPayments)) {
                                /** @var OrderPayment $pointPayment */
                                foreach ($pointPayments as $pointPayment) {
                                    $pointPaymentSum = $pointPaymentSum + $pointPayment->getAmount();
                                }
                            }

                            $delta = 0;
                            if ($pointPaymentSum > $serviceMaterialsBill) {
                                $delta = abs($serviceMaterialsBill - $pointPaymentSum);
                            }

                            $sumToPoints = 0;
                            if ($serviceWorksBill > $delta) {
                                $sumToPoints = $serviceWorksBill - $delta;
                            }

                            $cashBackPercent = $order->getOffice()->getCashBackPercent();

                            $willReceivePointAmount = round(($sumToPoints * $cashBackPercent) / 100, 2);

                            return $willReceivePointAmount;
                        },
                    ],
                    'serviceMaterialsTotalBill' => [
                        'type' => Type::nonNull(Type::float()),
                        'description' => 'Суммарный счет по заказу за материалы.',
                        'resolve' => function (Order $order) {
                            $totalBill = 0;

                            $serviceMaterials = $order->getServiceMaterials();
                            if (!empty($serviceMaterials)) {
                                /** @var ServiceMaterial $serviceMaterial */
                                foreach ($serviceMaterials as $serviceMaterial) {
                                    $totalBill = $totalBill + $serviceMaterial->getTotalCost();
                                }
                            }

                            return round($totalBill,2);
                        },
                    ],
                    'serviceWorks' => [
                        'type' => Type::listOf(Types::serviceWorkType($serviceManager)),
                        'args' => [
                            'statuses' => [
                                'type' => Type::listOf(Enums::serviceWorkStatusEnumType()),
                                'defaultValue' => [],
                            ],
                        ],
                        'resolve' => function (Order $order, $args) {
                            $serviceWorks = $order->getServiceWorks();

                            if (isset($args['statuses']) && !empty($args['statuses'])) {
                                $statusCriteria = Criteria::create()
                                    ->where(Criteria::expr()->in(
                                        'status',
                                        $args['statuses']
                                    ));

                                $serviceWorks = $serviceWorks->matching($statusCriteria);
                            }

                            return $serviceWorks;
                        },
                    ],
                    'serviceMaterials' => [
                        'type' => Type::listOf(Types::serviceMaterialType($serviceManager)),
                        'resolve' => function (Order $order) {
                            return $order->getServiceMaterials();
                        },
                    ],
                    'makeAt' => [
                        'type' => Type::nonNull(Types::dateType()),
                        'description' => 'Дата создания.',
                        'resolve' => function (Order $order) {
                            return $order->getCreatedAt();
                        },
                    ],
                    'status' => [
                        'type' => Type::nonNull(Enums::orderStatusEnumType()),
                        'description' => 'Статус заказа.',
                        'resolve' => function (Order $order) {
                            return $order->getStatus();
                        },
                    ],
                    'isClientVisitRequiredForPlaning' => [
                        'type' => Type::nonNull(Type::boolean()),
                        'description' => 'Треубется ли визит клиента для формирования списка работ.',
                        'resolve' => function (Order $order) {
                            return $order->isClientVisitRequired();
                        },
                    ],
                    'car' => [
                        'type' => Types::carType($serviceManager),
                        'description' => 'Автомобиль заказа.',
                        'resolve' => function (Order $order) {
                            return $order->getCar();
                        },
                    ],
                    'client' => [
                        'type' => Types::clientType($serviceManager),
                        'description' => 'Клиент заказа.',
                        'resolve' => function (Order $order) {
                            return $order->getClient();
                        },
                    ],
                    'request' => [
                        'type' => Types::requestType($serviceManager),
                        'description' => 'Заявка из которой был сформирован заказ.',
                        'resolve' => function (Order $order) {
                            return $order->getRequest();
                        },
                    ],
                    'office' => [
                        'type' => Type::nonNull(Types::officeType($serviceManager)),
                        'description' => 'Офис к которому привязана заявка.',
                        'resolve' => function (Order $order) {
                            return $order->getOffice();
                        },
                    ],
                    'contactName' => [
                        'type' => Type::string(),
                        'description' => 'Имя контактного лица.',
                        'resolve' => function (Order $order) {
                            return $order->getContactName();
                        },
                    ],
                    'phoneSameAsInProfile' => [
                        'type' => Type::boolean(),
                        'resolve' => function (Order $order) {
                            return $order->getPhoneSameAsInProfile();
                        },
                    ],
                    'contactPhone' => [
                        'type' => Type::string(),
                        'description' => 'Телефон контактного лица.',
                        'resolve' => function (Order $order) {
                            return $order->getContactPhone();
                        },
                    ],
                    'contactNote' => [
                        'type' => Type::string(),
                        'description' => 'Заметка к контактам.',
                        'resolve' => function (Order $order) {
                            return $order->getContactNote();
                        },
                    ],
                    'reason' => [
                        'type' => Type::string(),
                        'description' => 'Причина заказа.',
                        'resolve' => function (Order $order) {
                            return $order->getOrderReason();
                        },
                    ],
                    'estimatedClientArrivalDate' => [
                        'type' => Types::dateType(),
                        'description' => 'Когда предположительно приедет клиент',
                        'resolve' => function (Order $order) {
                            return $order->getEstimatedClientArrivalDate();
                        },
                    ],
                    'factClientVisitDateTime' => [
                        'type' => Types::dateType(),
                        'description' => 'Когда действительн приехал клиент',
                        'resolve' => function (Order $order) {
                            return $order->getFactClientVisitDateTime();
                        },
                    ],
                    'factCarReturnDateTime' => [
                        'type' => Types::dateType(),
                        'description' => 'Когда действительно вернули авто клиенту',
                        'resolve' => function (Order $order) {
                            return $order->getFactCarReturnDateTime();
                        },
                    ],
                    'schedules' => [
                        'type' => Type::listOf(Types::boxScheduleType($serviceManager)),
                        'description' => 'Список броней боксов по заказу.',
                        'args' => [
                            'statuses' => [
                                'type' => Type::listOf(Enums::scheduleStatusEnumType()),
                                'defaultValue' => [],
                            ],
                        ],
                        'resolve' => function (Order $order, $args) {
                            $schedules = $order->getSchedules();

                            if (isset($args['statuses']) && !empty($args['statuses'])) {
                                $statusCriteria = Criteria::create()
                                    ->where(Criteria::expr()->in(
                                        'status',
                                        $args['statuses']
                                    ));

                                $schedules = $schedules->matching($statusCriteria);
                            }

                            return $schedules;
                        },
                    ],
                    'rejectReasonId' => [
                        'type' => Enums::resultRequestCommunicationRejectReasonEnumType(),
                        'resolve' => function (Order $order) {
                            return $order->getRejectReasonId();
                        },
                    ],
                    'rejectReasonDescription' => [
                        'type' => Type::string(),
                        'resolve' => function (Order $order) {
                            return $order->getRejectReasonDescription();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}