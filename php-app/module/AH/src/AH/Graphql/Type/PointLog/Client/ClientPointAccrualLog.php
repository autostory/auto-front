<?php

namespace AH\Graphql\Type\PointLog\Client;

use AH\Entity\ClientPointAccrualLog as AccrualLogEntity;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class ClientPointAccrualLog extends ObjectType
{
    const TYPE_NAME = 'ClientPointAccrualLog';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'История начисления баллов клиенту.',
            'fields' => function () use ($serviceManager) {
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор записи лога списания баллов.',
                        'resolve' => function (AccrualLogEntity $accrualLog) use ($entityManager) {
                            return Relay::toGlobalId(self::TYPE_NAME, $accrualLog->getId());
                        },
                    ],
                    'client' => [
                        'type' => Type::nonNull(Types::clientType($serviceManager)),
                        'resolve' => function (AccrualLogEntity $accrualLog) use ($entityManager) {
                            return $accrualLog->getClient();
                        },
                    ],
                    'firmTitle' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (AccrualLogEntity $accrualLog) use ($entityManager) {
                            return $accrualLog->getOrder()->getFirm()->getName();
                        },
                    ],
                    'order' => [
                        'type' => Type::nonNull(Types::orderType($serviceManager)),
                        'resolve' => function (AccrualLogEntity $accrualLog) use ($entityManager) {
                            return $accrualLog->getOrder();
                        },
                    ],
                    'amount' => [
                        'type' => Type::nonNull(Type::float()),
                        'resolve' => function (AccrualLogEntity $accrualLog) use ($entityManager) {
                            return $accrualLog->getAmount();
                        },
                    ],
                    'accrualDate' => [
                        'type' => Type::nonNull(Types::dateType()),
                        'resolve' => function (AccrualLogEntity $accrualLog) use ($entityManager) {
                            return $accrualLog->getAccrualDate();
                        },
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}