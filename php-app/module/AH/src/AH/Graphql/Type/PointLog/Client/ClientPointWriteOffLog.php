<?php

namespace AH\Graphql\Type\PointLog\Client;

use AH\Entity\ClientPointWriteOffLog as WriteOffLogEntity;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class ClientPointWriteOffLog extends ObjectType
{
    const TYPE_NAME = 'ClientPointWriteOffLog';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'История списания баллов клиента.',
            'fields' => function () use ($serviceManager) {
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор записи лога списания баллов.',
                        'resolve' => function (WriteOffLogEntity $writeOffLog) use ($entityManager) {
                            return Relay::toGlobalId(self::TYPE_NAME, $writeOffLog->getId());
                        },
                    ],
                    'client' => [
                        'type' => Type::nonNull(Types::clientType($serviceManager)),
                        'resolve' => function (WriteOffLogEntity $writeOffLog) use ($entityManager) {
                            return $writeOffLog->getClient();
                        },
                    ],
                    'firmTitle' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (WriteOffLogEntity $writeOffLog) use ($entityManager) {
                            return $writeOffLog->getOrderPayment()->getOrder()->getFirm()->getName();
                        },
                    ],
                    'orderPayment' => [
                        'type' => Type::nonNull(Types::orderPaymentType($serviceManager)),
                        'resolve' => function (WriteOffLogEntity $writeOffLog) use ($entityManager) {
                            return $writeOffLog->getOrderPayment();
                        },
                    ],
                    'amount' => [
                        'type' => Type::nonNull(Type::float()),
                        'resolve' => function (WriteOffLogEntity $writeOffLog) use ($entityManager) {
                            return $writeOffLog->getAmount();
                        },
                    ],
                    'createdAt' => [
                        'type' => Type::nonNull(Types::dateType()),
                        'resolve' => function (WriteOffLogEntity $writeOffLog) use ($entityManager) {
                            return $writeOffLog->getCreatedAt();
                        },
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}