<?php

namespace AH\Graphql\Type;

use AH\Core\Relay\CustomRelay;
use AH\Entity\Car;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\ServiceManager\ServiceManager;

class ClientCarsType extends ObjectType
{
    const TYPE_NAME = 'ClientCars';

    public function __construct(ServiceManager $serviceManager)
    {
        /** @var \Doctrine\ORM\EntityManager $entityManager */
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');
        $hydrator = new DoctrineHydrator($entityManager);

        $config = [
            'name' => self::TYPE_NAME,
            'interfaces' => [Types::nodeInterface($serviceManager)],
            'description' => 'Автомобили клиента.',
            'fields' => function () use ($hydrator, $serviceManager) {
                return [
                    'id' => [
                        'name' => 'id',
                        'description' => 'The ID of an object',
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function ($cars) {
                            if (empty($cars)) {
                                return Relay::toGlobalId(self::TYPE_NAME, 0);
                            }

                            $ids = [];
                            // TODO: Сделать hash с uodatedAt
                            /** @var Car $car */
                            foreach ($cars as $car) {
                                $ids[] = $car->getId();
                            }

                            return Relay::toGlobalId(self::TYPE_NAME, implode(',', $ids));
                        }
                    ],
                    'itemsCount' => [
                        'name' => 'itemsCount',
                        'description' => 'Количество автомобилей клиента.',
                        'type' => Type::nonNull(Type::int()),
                        'resolve' => function ($cars) {
                            return count($cars);
                        }
                    ],
                    'carList' => [
                        'type' => Types::carConnection($serviceManager),
                        'description' => 'Коллекция автомобилей клиенкта',
                        'resolve' => function ($cars) use ($hydrator) {
                            return CustomRelay::connectionFromArrayCollection(
                                $cars,
                                [],
                                CarType::TYPE_NAME
                            );
                        }
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}