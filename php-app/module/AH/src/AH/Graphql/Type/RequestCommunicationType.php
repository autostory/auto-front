<?php

namespace AH\Graphql\Type;

use AH\Entity\RequestCommunication;
use AH\GraphQL\Types;
use AH\GraphQL\Enums;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class RequestCommunicationType extends ObjectType
{
    const TYPE_NAME = 'RequestCommunication';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Запись в истории взаимодействия с клиентом.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор.',
                        'resolve' => function (RequestCommunication $rc) {
                            return Relay::toGlobalId(self::TYPE_NAME, $rc->getId());
                        },
                    ],
                    'manager' => [
                        'type' => Type::nonNull(Types::managerType($serviceManager)),
                        'resolve' => function (RequestCommunication $rc) {
                            return $rc->getManager();
                        },
                    ],
                    'resultId' => [
                        'type' => Enums::resultRequestCommunicationEnumType(),
                        'resolve' => function (RequestCommunication $rc) {
                            return $rc->getResultId();
                        },
                    ],
                    'request' => [
                        'type' => Type::nonNull(Types::requestType($serviceManager)),
                        'resolve' => function (RequestCommunication $rc) {
                            return $rc->getRequest();
                        },
                    ],
                    'rejectReasonId' => [
                        'type' => Enums::resultRequestCommunicationRejectReasonEnumType(),
                        'resolve' => function (RequestCommunication $rc) {
                            return $rc->getRejectReasonId();
                        },
                    ],
                    'rejectReasonDescription' => [
                        'type' => Type::string(),
                        'resolve' => function (RequestCommunication $rc) {
                            return $rc->getRejectReasonDescription();
                        },
                    ],
                    'communicationDateTime' => [
                        'type' => Types::dateType(),
                        'resolve' => function (RequestCommunication $rc) {
                            return $rc->getCommunicationDateTime();
                        },
                    ],
                    'nextCommunicationDateTime' => [
                        'type' => Types::dateType(),
                        'resolve' => function (RequestCommunication $rc) {
                            return $rc->getNextCommunicationDateTime();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}

