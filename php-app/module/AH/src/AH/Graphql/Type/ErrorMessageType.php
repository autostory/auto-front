<?php

namespace AH\Graphql\Type;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class ErrorMessageType extends ObjectType
{
    const TYPE_NAME = 'ErrorMessage';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Некое сообщение об ошибке.',
            'fields' => function () {
                return [
                    'text' => [
                        'type' => Type::nonNull(Type::string()),
                        'description' => 'Юзеро читабельное сообщние об ошибке.',
                    ],
                    'type' => [
                        'type' => Type::string(),
                        'description' => 'Название типа ошибки.',
                    ],
                    'fieldName' => [
                        'type' => Type::string(),
                        'description' => 'Название поля по которому получили ошибку. Если поле не указано, то значит это относится к форме в целом.',
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}