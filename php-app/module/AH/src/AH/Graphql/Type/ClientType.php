<?php

namespace AH\Graphql\Type;

use AH\Core\Acl\ClientAccess;
use AH\Entity\Client;
use AH\Entity\ClientPointWriteOffLog;
use AH\Entity\Order;
use AH\GraphQL\Enums;
use AH\GraphQL\Interfaces;
use AH\GraphQL\Types;
use AH\Repository\OrderRepository;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class ClientType extends ObjectType
{
    const TYPE_NAME = 'Client';

    public function __construct(ServiceManager $serviceManager)
    {
        $userTypeInterface = Interfaces::userTypeInterface($serviceManager);

        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Client info.',
            'interfaces' => [
                Types::nodeInterface($serviceManager),
                $userTypeInterface,
            ],

            'fields' => function () use ($serviceManager, $userTypeInterface) {
                /** @var ClientAccess $clientAccess */
                $clientAccess = $serviceManager->get(ClientAccess::class);
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор клиента.',
                        'resolve' => function (Client $client) use ($clientAccess) {
                            if (!$clientAccess->canViewClient($client)) {
                                return null;
                            }

                            return Relay::toGlobalId(self::TYPE_NAME, $client->getId());
                        },
                    ],
                    $userTypeInterface->getField('email'),
                    $userTypeInterface->getField('phone'),
                    $userTypeInterface->getField('fullName'),
                    $userTypeInterface->getField('firstName'),
                    $userTypeInterface->getField('surname'),
                    $userTypeInterface->getField('patronymic'),
                    'cars' => [
                        'type' => Types::clientCars($serviceManager),
                        'resolve' => function (Client $client) use ($clientAccess) {
                            if (!$clientAccess->canViewClient($client)) {
                                return null;
                            }

                            return $client->getCars();
                        },
                    ],
                    'points' => [
                        'type' => Type::nonNull(Type::float()),
                        'resolve' => function (Client $client) use ($clientAccess) {
                            if (!$clientAccess->canViewClient($client)) {
                                return null;
                            }

                            return $client->getPoints();
                        },
                    ],
                    'pointLog' => [
                        'description' => 'История списания\получения баллов клиентом.',
                        'resolve' => function (Client $client) use ($entityManager, $clientAccess) {
                            if (!$clientAccess->canViewClient($client)) {
                                return null;
                            }

                            $rClientPointWriteOffLog = $entityManager->getRepository(ClientPointWriteOffLog::class);
                            $allClientWriteOff = $rClientPointWriteOffLog->findBy([
                                'client' => $client,
                            ]);

                            $allClientAccrual = [];

                            return array_merge($allClientWriteOff, $allClientAccrual);
                        },
                        'type' => Type::listOf(new UnionType([
                            'name' => 'ClientPointLog',
                            'types' => [
                                Types::clientPointWriteOffLog($serviceManager),
                            ],
                            'resolveType' => function ($logRecord) use ($serviceManager) {
                                if ($logRecord instanceof ClientPointWriteOffLog) {
                                    return Types::clientPointWriteOffLog($serviceManager);
                                }

                                return null;
                            }
                        ])),
                    ],
                    'paymentStat' => [
                        'type' => Types::clientPaymentStatType($serviceManager),
                        'description' => 'Средний и суммарный чек по клиенту.',
                        'resolve' => function (Client $client) use ($clientAccess) {
                            if (!$clientAccess->canViewClient($client)) {
                                return null;
                            }

                            return $client;
                        },
                    ],
                    'orders' => [
                        'type' => Type::listOf(Types::orderType($serviceManager)),
                        'resolve' => function (Client $client) use ($clientAccess) {
                            if (!$clientAccess->canViewClient($client)) {
                                return null;
                            }

                            return $client->getOrders();
                        },
                    ],
                    'activeOrders' => [
                        'type' => Type::listOf(Types::orderType($serviceManager)),
                        'resolve' => function (Client $client) use ($clientAccess, $entityManager) {
                            if (!$clientAccess->canViewClient($client)) {
                                return null;
                            }

                            /** @var OrderRepository $rOrder */
                            $rOrder = $entityManager->getRepository(Order::class);
                            $activeOrders = $rOrder->getClientActiveOrders($client);

                            return $activeOrders;
                        },
                    ],
                    'ordersCount' => [
                        'type' => Type::nonNull(Type::int()),
                        'resolve' => function (Client $client) use ($clientAccess, $entityManager) {
                            if (!$clientAccess->canViewClient($client)) {
                                return null;
                            }

                            /** @var OrderRepository $rOrder */
                            $rOrder = $entityManager->getRepository(Order::class);

                            return $rOrder->getOrderCountByClient($client);
                        },
                    ],
                    'category' => [
                        'type' => Enums::clientCategoryEnumType(),
                        'resolve' => function (Client $client) use ($clientAccess) {
                            if (!$clientAccess->canViewClient($client)) {
                                return null;
                            }

                            return $client->getCategory();
                        },
                    ],
                ];
            },

        ];

        parent::__construct($config);
    }
}