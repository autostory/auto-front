<?php

namespace AH\Graphql\Type;

use AH\Core\Relay\CustomRelay;
use AH\Entity\Client;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\ServiceManager\ServiceManager;

class FirmClientsType extends ObjectType
{
    const TYPE_NAME = 'FirmClients';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'interfaces' => [Types::nodeInterface($serviceManager)],
            'description' => 'Клиенты фирмы.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'name' => 'id',
                        'description' => 'Идентификатор набора.',
                        'resolve' => function ($payload) {
                            if (!isset($payload['clientsArray'])) {
                                return Relay::toGlobalId(self::TYPE_NAME, 0);
                            }

                            $clients = $payload['clientsArray'];
                            $argsHash = '';

                            if (isset($payload['args'])) {
                                $argsHash = md5(implode(',', $payload['args']));
                            }

                            $dataToHash = [];
                            /** @var Client $client */
                            if (!empty($clients)) {
                                foreach ($clients as $client) {
                                    $dataToHash[] = $client->getId();
                                    if (!empty($client->getUpdatedAt())) {
                                        $dataToHash[] = $client->getUpdatedAt()->format('U');
                                    }
                                }
                            }

                            $hash = md5(implode(',', $dataToHash) . $argsHash);

                            return Relay::toGlobalId(self::TYPE_NAME, $hash);
                        }
                    ],
                    'itemsCount' => [
                        'name' => 'itemsCount',
                        'description' => 'Количество клиентов фирмы',
                        'type' => Type::nonNull(Type::int()),
                        'resolve' => function ($payload) {
                            if (!isset($payload['totalCount'])) {
                                return 0;
                            }

                            return intval($payload['totalCount']);
                        }
                    ],
                    'clientList' => [
                        'type' => Types::clientConnection($serviceManager),
                        'description' => 'Коллекция клиентов фирмы',
                        'resolve' => function ($payload) {
                            return CustomRelay::connectionFromArrayCollection(
                                isset($payload['clientsArray']) ? $payload['clientsArray'] : [],
                                isset($payload['args']) ? $payload['args'] : [],
                                ClientType::TYPE_NAME
                            );
                        }
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}