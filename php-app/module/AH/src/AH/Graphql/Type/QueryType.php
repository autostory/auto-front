<?php
namespace AH\Graphql\Type;

use AH\Entity\Client;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ObjectType;
use Zend\ServiceManager\ServiceManager;

class QueryType extends ObjectType
{
    const TYPE_NAME = 'Query';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'fields' => [
                'viewer' => [
                    'type' => Types::viewer($serviceManager),
                    'description' => 'Represents currently logged-in user.',
                    'resolve' => function ($user) {
                        if (empty($user)) {
                            return new Client();
                        }

                        return $user;
                    },
                ],
                'node' => Types::nodeField($serviceManager),
            ],
        ];

        parent::__construct($config);
    }
}
