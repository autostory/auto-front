<?php

namespace AH\Graphql\Type;

use AH\Entity\CarMileage;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class CarMileageType extends ObjectType
{
    const TYPE_NAME = 'CarMileageType';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function (CarMileage $carMileage) {
                            return Relay::toGlobalId(self::TYPE_NAME, $carMileage->getId());
                        },
                    ],
                    'order' => [
                        'type' => Types::orderType($serviceManager),
                        'resolve' => function (CarMileage $carMileage) {
                            return $carMileage->getOrder();
                        },
                    ],
                    'car' => [
                        'type' => Types::carType($serviceManager),
                        'resolve' => function (CarMileage $carMileage) {
                            return $carMileage->getCar();
                        },
                    ],
                    'value' => [
                        'type' => Type::nonNull(Type::int()),
                        'resolve' => function (CarMileage $carMileage) {
                            return $carMileage->getValue();
                        },
                    ],
                    'measuredAt' => [
                        'type' => Type::nonNull(Types::dateType()),
                        'resolve' => function (CarMileage $carMileage) {
                            return $carMileage->getMeasuredAt();
                        },
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}