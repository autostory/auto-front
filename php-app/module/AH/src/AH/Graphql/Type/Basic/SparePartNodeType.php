<?php

namespace AH\Graphql\Type\Basic;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use \AH\Entity\Basic\SparePartNode;

class SparePartNodeType extends ObjectType
{
    const TYPE_NAME = 'SparePartNode';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Узлы и агрегаты  техники. Не путать с запчастями.',
            'fields' => function () {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор узла техники.',
                        'resolve' => function (SparePartNode $sparePartNode) {
                            return Relay::toGlobalId(self::TYPE_NAME, $sparePartNode->getId());
                        },
                    ],
                    'name' => [
                        'type' => Type::nonNull(Type::string()),
                        'description' => 'Название узла.',
                        'resolve' => function (SparePartNode $sparePartNode) {
                            return $sparePartNode->getName();
                        },
                    ],
                    'parent' => [
                        'type' => $this,
                        'description' => 'Родитель узла.',
                        'resolve' => function (SparePartNode $sparePartNode) {
                            return $sparePartNode->getParent();
                        },
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}