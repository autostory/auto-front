<?php

namespace AH\Graphql\Type\Basic;

use AH\Entity\Basic\GlobalServiceType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;

class GlobalServiceTypeType extends ObjectType
{
    const TYPE_NAME = 'GlobalServiceType';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Тип услуг которые могут быть оказаны автосервисом.',
            'fields' => function () {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function (GlobalServiceType $globalServiceType) {
                            return Relay::toGlobalId(self::TYPE_NAME, $globalServiceType->getId());
                        },
                    ],
                    'name' => [
                        'type' => Type::nonNull(Type::string()),
                        'description' => 'Название сервиса.',
                        'resolve' => function (GlobalServiceType $globalServiceType) {
                            return $globalServiceType->getName();
                        },
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}