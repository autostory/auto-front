<?php

namespace AH\Graphql\Type\Basic;

use AH\Entity\Basic\UnitMeasureType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use \AH\Entity\Basic\SparePartNode;

class UnitMeasureTypeType extends ObjectType
{
    const TYPE_NAME = 'UnitMeasureType';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Типы измерений.',
            'fields' => function () {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор.',
                        'resolve' => function (UnitMeasureType $unitMeasureType) {
                            return Relay::toGlobalId(self::TYPE_NAME, $unitMeasureType->getId());
                        },
                    ],
                    'name' => [
                        'type' => Type::nonNull(Type::string()),
                        'description' => 'Название типа измерений.',
                        'resolve' => function (UnitMeasureType $unitMeasureType) {
                            return $unitMeasureType->getName();
                        },
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}