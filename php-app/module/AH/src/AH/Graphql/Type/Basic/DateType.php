<?php

namespace AH\Graphql\Type\Basic;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class DateType extends ObjectType
{
    const TYPE_NAME = 'Date';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Дата.',
            'fields' => function () {
                return [
                    'timestamp' => [
                        'type' => Type::nonNull(Type::int()),
                        'resolve' => function (\DateTime $dateTime) {
                            return $dateTime->getTimestamp();
                        },
                    ],
                    'timeZoneOffset' => [
                        'type' => Type::nonNull(Type::int()),
                        'resolve' => function (\DateTime $dateTime) {
                            return $dateTime->getOffset();
                        },
                    ],
                    'timeZoneName' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (\DateTime $dateTime) {
                            return $dateTime->getTimezone()->getName();
                        },
                    ],
                    'formattedDateTime' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (\DateTime $dateTime) {
                            return $dateTime->format('d.m.Y H:i:s');
                        },
                    ],
                    'formattedDate' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (\DateTime $dateTime) {
                            return $dateTime->format('d.m.Y');
                        },
                    ],
                    'formattedTime' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (\DateTime $dateTime) {
                            return $dateTime->format('H:i:s');
                        },
                    ],
                    'inputFormatted' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (\DateTime $dateTime) {
                            return $dateTime->format('Y-m-d\TH:i:s');
                        },
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}