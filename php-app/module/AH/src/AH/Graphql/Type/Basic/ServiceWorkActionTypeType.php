<?php

namespace AH\Graphql\Type\Basic;

use AH\Entity\Service\ServiceWorkActionType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;

class ServiceWorkActionTypeType extends ObjectType
{
    const TYPE_NAME = 'ServiceWorkActionType';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Тип действия выполненнего в рамках единицы услуг сервиса.',
            'fields' => function () {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'resolve' => function (ServiceWorkActionType $serviceWorkActionType) {
                            return Relay::toGlobalId(self::TYPE_NAME, $serviceWorkActionType->getId());
                        },
                    ],
                    'name' => [
                        'type' => Type::nonNull(Type::string()),
                        'description' => 'Название действия.',
                        'resolve' => function (ServiceWorkActionType $serviceWorkActionType) {
                            return $serviceWorkActionType->getName();
                        },
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}