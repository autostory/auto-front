<?php

namespace AH\Graphql\Type\Basic;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use \AH\Entity\Basic\FuelType;

class FuelTypeType extends ObjectType
{
    const TYPE_NAME = 'FuelType';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Тип топлива.',
            'fields' => function () {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор типа топлива.',
                        'resolve' => function (FuelType $fuelType) {
                            return Relay::toGlobalId(self::TYPE_NAME, $fuelType->getId());
                        },
                    ],
                    'name' => [
                        'type' => Type::nonNull(Type::string()),
                        'description' => 'Название топлива.',
                        'resolve' => function (FuelType $fuelType) {
                            return $fuelType->getName();
                        },
                    ],
                    'unitName' => [
                        'type' => Type::nonNull(Type::string()),
                        'description' => 'Единица измерения топлива.',
                        'resolve' => function (FuelType $fuelType) {
                            return $fuelType->getUnitName();
                        },
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}