<?php
namespace AH\Graphql\Type;

use AH\Entity\Basic\FuelType;
use AH\Entity\Basic\SparePartNode;
use AH\Entity\Basic\UnitMeasureType;
use AH\Entity\Service\ServiceWorkActionType;
use AH\Entity\User;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ObjectType;
use Zend\ServiceManager\ServiceManager;

class BasicType extends ObjectType
{
    const TYPE_NAME = 'BasicType';

    public function __construct(ServiceManager $serviceManager)
    {
        /** @var \Doctrine\ORM\EntityManager $entityManager */
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

        $config = [
            'name' => self::TYPE_NAME,
            'fields' => [
                'fuelTypes' => [
                    'type' => Type::listOf(Types::fuelTypeType()),
                    'description' => 'Список типов топлива',
                    'resolve' => function (?User $user) use ($entityManager) {
                        $rFuelType = $entityManager->getRepository(FuelType::class);

                        return $rFuelType->findAll();
                    },
                ],
                'spareParts' => [
                    'type' => Type::listOf(Types::sparePartNodeType()),
                    'description' => 'Список узлов и агрегатов техники.',
                    'resolve' => function (?User $user) use ($entityManager) {
                        $rSparePart = $entityManager->getRepository(SparePartNode::class);

                        return $rSparePart->findAll();
                    },
                ],
                'serviceWorkActionsType' => [
                    'type' => Type::listOf(Types::serviceWorkActionTypeType()),
                    'description' => 'Список всех возможных типов сервисных работ.',
                    'resolve' => function (?User $user) use ($entityManager) {
                        $rServiceWorkActionType = $entityManager->getRepository(ServiceWorkActionType::class);

                        return $rServiceWorkActionType->findAll();
                    },
                ],
                'unitMeasureTypes' => [
                    'type' => Type::listOf(Types::unitMeasureTypeType()),
                    'resolve' => function (?User $user) use ($entityManager) {
                        $rUnitMeasureType = $entityManager->getRepository(UnitMeasureType::class);

                        return $rUnitMeasureType->findBy([], ['id' => 'ASC']);
                    },
                ]
            ],
        ];

        parent::__construct($config);
    }
}
