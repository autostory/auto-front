<?php

namespace AH\Graphql\Type;

use AH\Entity\BoxSchedule;
use AH\GraphQL\Enums;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class BoxScheduleType extends ObjectType
{
    const TYPE_NAME = 'BoxSchedule';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Запись заказа в боксе.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор.',
                        'resolve' => function (BoxSchedule $schedule) {
                            return Relay::toGlobalId(self::TYPE_NAME, $schedule->getId());
                        },
                    ],
                    'box' => [
                        'type' => Type::nonNull(Types::officeBoxType($serviceManager)),
                        'resolve' => function (BoxSchedule $schedule) {
                            return $schedule->getBox();
                        },
                    ],
                    'status' => [
                        'type' => Type::nonNull(Enums::scheduleStatusEnumType()),
                        'resolve' => function (BoxSchedule $schedule) {
                            return $schedule->getStatus();
                        },
                    ],
                    'serviceWorks' => [
                        'type' => Type::listOf(Types::serviceWorkType($serviceManager)),
                        'resolve' => function (BoxSchedule $schedule) {
                            return $schedule->getServiceWorks();
                        },
                    ],
                    'order' => [
                        'type' => Type::nonNull(Types::orderType($serviceManager)),
                        'resolve' => function (BoxSchedule $schedule) {
                            return $schedule->getOrder();
                        },
                    ],
                    'start' => [
                        'type' => Types::dateType(),
                        'resolve' => function (BoxSchedule $schedule) {
                            return $schedule->getStart();
                        },
                    ],
                    'finish' => [
                        'type' => Types::dateType(),
                        'resolve' => function (BoxSchedule $schedule) {
                            return $schedule->getFinish();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}
