<?php

namespace AH\Graphql\Type;

use AH\Core\Relay\CustomRelay;
use AH\Entity\Office;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class ManagerOfficesType extends ObjectType
{
    const TYPE_NAME = 'ManagerOffices';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'interfaces' => [Types::nodeInterface($serviceManager)],
            'description' => 'Офисы где присутвует этот менеджер.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'name' => 'id',
                        'description' => 'Идентификатор набора.',
                        'resolve' => function ($payload) {
                            if (!isset($payload['officesArray'])) {
                                return Relay::toGlobalId(self::TYPE_NAME, 0);
                            }

                            $offices = $payload['officesArray'];

                            $dataToHash = [];
                            /** @var Office $office */
                            if (!empty($offices)) {
                                foreach ($offices as $office) {
                                    $dataToHash[] = $office->getId();
                                    if (!empty($office->getUpdatedAt())) {
                                        $dataToHash[] = $office->getUpdatedAt()->format('U');
                                    }
                                }
                            }

                            $hash = md5(implode(',', $dataToHash));

                            return Relay::toGlobalId(self::TYPE_NAME, $hash);
                        }
                    ],
                    'itemsCount' => [
                        'name' => 'itemsCount',
                        'description' => 'Количество офисов где присутвует этот менеджер.',
                        'type' => Type::nonNull(Type::int()),
                        'resolve' => function ($payload) {
                            if (!isset($payload['totalCount'])) {
                                return 0;
                            }

                            return intval($payload['totalCount']);
                        }
                    ],
                    'officesList' => [
                        'type' => Types::officeConnection($serviceManager),
                        'description' => 'Коллекция офисов где присутвует этот менеджер.',
                        'resolve' => function ($payload) {
                            return CustomRelay::connectionFromArrayCollection(
                                isset($payload['officesArray']) ? $payload['officesArray'] : [],
                                [],
                                OfficeType::TYPE_NAME
                            );
                        }
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}