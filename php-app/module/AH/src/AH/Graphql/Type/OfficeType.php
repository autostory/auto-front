<?php

namespace AH\Graphql\Type;

use AH\Core\Relay\CustomRelay;
use AH\Entity\Office;
use AH\Entity\OfficeBox;
use AH\Entity\Order;
use AH\GraphQL\Types;
use AH\Repository\OrderRepository;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class OfficeType extends ObjectType
{
    const TYPE_NAME = 'Office';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Office info.',
            'fields' => function () use ($serviceManager) {
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор офиса.',
                        'resolve' => function (Office $office) {
                            return Relay::toGlobalId(self::TYPE_NAME, $office->getId());
                        },
                    ],
                    'firm' => [
                        'type' => Type::nonNull(Types::firmType($serviceManager)),
                        'resolve' => function (Office $office) {
                            return $office->getFirm();
                        },
                    ],
                    'title' => [
                        'type' => Type::string(),
                        'description' => 'Название офиса.',
                        'resolve' => function (Office $office) {
                            return $office->getTitle();
                        },
                    ],
                    'cashBackPercent' => [
                        'type' => Type::float(),
                        'description' => 'Процент цешбека возвращаемого клинту.',
                        'resolve' => function (Office $office) {
                            return $office->getCashBackPercent();
                        },
                    ],
                    'active' => [
                        'type' => Type::nonNull(Type::boolean()),
                        'resolve' => function (Office $office) {
                            return $office->getActive();
                        },
                    ],
                    'phone' => [
                        'type' => Type::string(),
                        'description' => 'Контактный номер офиса.',
                        'resolve' => function (Office $office) {
                            return $office->getPhone();
                        },
                    ],
                    'address' => [
                        'type' => Type::string(),
                        'description' => 'Адресс офисе.',
                        'resolve' => function (Office $office) {
                            return $office->getAddress();
                        },
                    ],
                    'workTimes' => [
                        'type' => Type::listOf(Types::officeWorkTimeType($serviceManager)),
                        'description' => 'Режим работы офиса.',
                        'resolve' => function (Office $office) {
                            return $office->getWorkTimes();
                        },
                    ],
                    'boxes' => [
                        'args' => [
                            'active' => [
                                'type' => Type::boolean(),
                            ],
                        ],
                        'type' => Type::listOf(Types::officeBoxType($serviceManager)),
                        'resolve' => function (Office $office, $args) use ($entityManager) {
                            $criteria = [
                                'office' => $office->getId(),
                            ];

                            if (isset($args['active'])) {
                                $active = boolval($args['active']);
                                $criteria['active'] = $active;
                            }

                            $rBox = $entityManager->getRepository(OfficeBox::class);

                            $boxes = $rBox->findBy($criteria);

                            return $boxes;
                        },
                    ],
                    'managers' => [
                        'type' => Types::managerConnection($serviceManager),
                        'resolve' => function (Office $office) {
                            $managers = $office->getManagers();

                            return CustomRelay::connectionFromArrayCollection(
                                $managers,
                                [],
                                ManagerType::TYPE_NAME
                            );
                        },
                    ],
                    'dailyOfficeReport' => [
                        'type' => Types::dailyFirmOfficeReportType($serviceManager),
                        'description' => 'Сводка фирмы по дню.',
                        'resolve' => function (Office $office) use ($entityManager) {
                            return $office;
                        },
                    ],
                    'totalOrderPaymentDebt' => [
                        'type' => Type::float(),
                        'description' => 'Сумма долга по заказам в этом офисе.',
                        'resolve' => function (Office $office) use ($entityManager) {
                            /** @var OrderRepository $rOrder */
                            $rOrder = $entityManager->getRepository(Order::class);

                            return $rOrder->getOrderDebtByOffice($office);
                        },
                    ],
                    'orderCountInWaitPaymentStatus' => [
                        'type' => Type::int(),
                        'description' => 'Количество заказов в статусе ожидает оплаты в этом офисе.',
                        'resolve' => function (Office $office) use ($entityManager) {
                            /** @var OrderRepository $rOrder */
                            $rOrder = $entityManager->getRepository(Order::class);

                            return $rOrder->getOrderCountInWaitPaymentStatus($office);
                        },
                    ]
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}