<?php

namespace AH\Graphql\Type\Report;

use AH\Entity\Firm;
use AH\Entity\Office;
use AH\Entity\Order;
use AH\Entity\OrderPayment;
use AH\GraphQL\Types;
use AH\Repository\OrderPaymentRepository;
use AH\Repository\OrderRepository;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Zend\ServiceManager\ServiceManager;
use AH\Core\Acl\OfficeAccess;

class DailyFirmOfficeReport extends ObjectType
{
    const TYPE_NAME = 'DailyFirmOfficeReport';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Сводка фирмы по дню.',
            'fields' => function () use ($serviceManager) {
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');
                /** @var OfficeAccess $officeAccessService */
                $officeAccessService = $serviceManager->get(OfficeAccess::class);

                /** @var OrderRepository $rOrder */
                $rOrder = $entityManager->getRepository(Order::class);
                /** @var OrderPaymentRepository $rOrderPayment */
                $rOrderPayment = $entityManager->getRepository(OrderPayment::class);

                return [
                    'office' => [
                        'type' => Types::officeType($serviceManager),
                        'description' => 'Офис по которому показана статистика.',
                        'resolve' => function (Office $office) use ($officeAccessService) {
                            if (!$officeAccessService->canViewOffice($office)) {
                                return null;
                            }

                            return $office;
                        },
                    ],
                    'createdOrderCount' => [
                        'type' => Type::int(),
                        'description' => 'Количество поступивших заказов за сегодня.',
                        'resolve' => function (Office $office) use ($officeAccessService, $rOrder) {
                            if (!$officeAccessService->canViewOffice($office)) {
                                return null;
                            }

                            $createdOrderCount = $rOrder->getReceivedOfficeOrdersCountByDay($office);

                            return $createdOrderCount;
                        },
                    ],
                    'doneOrderCount' => [
                        'type' => Type::int(),
                        'description' => 'Количество выполненных заказов за сегодня.',
                        'resolve' => function (Office $office) use ($officeAccessService, $rOrder) {
                            if (!$officeAccessService->canViewOffice($office)) {
                                return null;
                            }

                            $createdOrderCount = $rOrder->getDoneOfficeOrdersCountByDay($office);

                            return $createdOrderCount;
                        },
                    ],
                    'totalRevenues' => [
                        'type' => Type::float(),
                        'description' => 'Сумма платежей поступивших сегодня.',
                        'resolve' => function (Office $office) use ($officeAccessService, $rOrderPayment) {
                            if (!$officeAccessService->canViewOffice($office)) {
                                return null;
                            }

                            $totalRevenues = $rOrderPayment->getDayOrderPaymentSumByOffice($office);

                            return $totalRevenues;
                        },
                    ],
                ];
            }
        ];

        parent::__construct($config);
    }
}