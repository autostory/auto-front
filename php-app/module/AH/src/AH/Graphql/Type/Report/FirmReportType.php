<?php

namespace AH\Graphql\Type\Report;

use AH\Entity\Firm;
use AH\Entity\Office;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Zend\ServiceManager\ServiceManager;
use AH\Core\Acl\FirmAccess;

class FirmReportType extends ObjectType
{
    const TYPE_NAME = 'FirmReport';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Отчеты, сводка, статистика.',
            'fields' => function () use ($serviceManager) {
                /** @var FirmAccess $firmAccessService */
                $firmAccessService = $serviceManager->get(FirmAccess::class);
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                return [
                    'dailyFirmOfficesReport' => [
                        'type' => Type::listOf(Types::dailyFirmOfficeReportType($serviceManager)),
                        'description' => 'Сводка фирмы по дню.',
                        'resolve' => function (Firm $firm) use ($firmAccessService, $entityManager) {
                            if (!$firmAccessService->canViewFirm($firm)) {
                                return null;
                            }

                            $rOffice = $entityManager->getRepository(Office::class);
                            $firmActiveOffices = $rOffice->findBy([
                                'firm' => $firm,
                                'active' => true,
                            ]);

                            if (empty($firmActiveOffices)) {
                                return null;
                            }

                            return $firmActiveOffices;
                        },
                    ],
                    'totalOrderPaymentDebt' => [
                        'type' => Type::listOf(Types::dailyFirmOfficeReportType($serviceManager)),
                        'description' => 'Сводка фирмы по дню.',
                        'resolve' => function (Firm $firm) use ($firmAccessService, $entityManager) {
                            if (!$firmAccessService->canViewFirm($firm)) {
                                return null;
                            }

                            $rOffice = $entityManager->getRepository(Office::class);
                            $firmActiveOffices = $rOffice->findBy([
                                'firm' => $firm,
                                'active' => true,
                            ]);

                            if (empty($firmActiveOffices)) {
                                return null;
                            }

                            return $firmActiveOffices;
                        },
                    ]
                ];
            }
        ];

        parent::__construct($config);
    }
}