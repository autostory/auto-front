<?php

namespace AH\Graphql\Type;

use AH\Entity\Service\ServiceMaterial;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class ServiceMaterialType extends ObjectType
{
    const TYPE_NAME = 'ServiceMaterial';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Материалы используемые в заказе.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор офиса.',
                        'resolve' => function (ServiceMaterial $serviceMaterial) {
                            return Relay::toGlobalId(self::TYPE_NAME, $serviceMaterial->getId());
                        },
                    ],
                    'order' => [
                        'type' => Type::nonNull(Types::orderType($serviceManager)),
                        'resolve' => function (ServiceMaterial $serviceMaterial) {
                            return $serviceMaterial->getOrder();
                        },
                    ],
                    'amount' => [
                        'type' => Type::nonNull(Type::float()),
                        'description' => 'Количество',
                        'resolve' => function (ServiceMaterial $serviceMaterial) {
                            return $serviceMaterial->getAmount();
                        },
                    ],
                    'name' => [
                        'type' => Type::nonNull(Type::string()),
                        'resolve' => function (ServiceMaterial $serviceMaterial) {
                            return $serviceMaterial->getName();
                        },
                    ],
                    'costPerUnit' => [
                        'type' => Type::nonNull(Type::float()),
                        'resolve' => function (ServiceMaterial $serviceMaterial) {
                            return $serviceMaterial->getCostPerUnit();
                        },
                    ],
                    'totalCost' => [
                        'type' => Type::nonNull(Type::float()),
                        'resolve' => function (ServiceMaterial $serviceMaterial) {
                            return $serviceMaterial->getTotalCost();
                        },
                    ],
                    'discountPercent' => [
                        'type' => Type::nonNull(Type::float()),
                        'resolve' => function (ServiceMaterial $serviceMaterial) {
                            return $serviceMaterial->getDiscountPercent();
                        },
                    ],
                    'unitMeasureType' => [
                        'type' => Type::nonNull(Types::unitMeasureTypeType()),
                        'resolve' => function (ServiceMaterial $serviceMaterial) {
                            return $serviceMaterial->getUnitMeasureType();
                        },
                    ],
                    'doneAt' => [
                        'type' => Types::dateType(),
                        'description' => 'Когда работа была фактически выполнена.',
                        'resolve' => function (ServiceMaterial $serviceMaterial) {
                            // Сейчас это будет дата смены статуса заказа на выполнено
                            return $serviceMaterial->getOrder()->getDoneDate();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}