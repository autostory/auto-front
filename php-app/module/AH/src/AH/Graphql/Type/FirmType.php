<?php

namespace AH\Graphql\Type;

use AH\Core\Relay\CustomRelay;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\Car;
use AH\Entity\Client;
use AH\Entity\Firm;
use AH\Entity\Manager;
use AH\Entity\Office;
use AH\Entity\OfficeBox;
use AH\Entity\Order;
use AH\Entity\OrderPayment;
use AH\Entity\Request;
use AH\Entity\User;
use AH\GraphQL\Enums;
use AH\GraphQL\Types;
use AH\Repository\CarRepository;
use AH\Repository\ClientRepository;
use AH\Repository\OrderPaymentRepository;
use AH\Repository\OrderRepository;
use AH\Repository\RequestRepository;
use AH\Repository\OfficeRepository;
use Doctrine\Common\Collections\Criteria;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;
use AH\Core\Acl\FirmAccess;
use AH\Core\Acl\OfficeAccess;

class FirmType extends ObjectType
{
    const TYPE_NAME = 'Firm';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Firm info.',
            'fields' => function () use ($serviceManager) {
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');
                /** @var FirmAccess $firmAccessService */
                $firmAccessService = $serviceManager->get(FirmAccess::class);
                /** @var OfficeAccess $officeAccessService */
                $officeAccessService = $serviceManager->get(OfficeAccess::class);

                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор фирмы.',
                        'resolve' => function (Firm $firm) use ($firmAccessService) {
                            if ($firmAccessService->canViewFirm($firm)) {
                                return Relay::toGlobalId(self::TYPE_NAME, $firm->getId());
                            }

                            return null;
                        },
                    ],
                    'canCreateOfficeInThatFirm' => [ // TODO: Вынести в alc или тут уже оставить?
                        'type' => Type::string(),
                        'description' => 'Может ли создавать офисы в этой фирме.',
                        'resolve' => function (Firm $firm) use ($officeAccessService) {
                            return $officeAccessService->canCreateOffice($firm);
                        },
                    ],
                    'name' => [
                        'type' => Type::string(),
                        'description' => 'Название офиса.',
                        'resolve' => function (Firm $firm) use ($firmAccessService) {
                            if ($firmAccessService->canViewFirm($firm)) {
                                return $firm->getName();
                            }

                            return null;
                        },
                    ],
                    'requestCount' => [
                        'args' => [
                            'active' => [
                                'type' => Type::boolean(),
                                'defaultValue' => null,
                            ],
                        ],
                        'type' => Type::nonNull(Type::int()),
                        'resolve' => function (Firm $firm, $args) use ($entityManager) {
                            /** @var RequestRepository $rRequest */
                            $rRequest = $entityManager->getRepository(Request::class);

                            return $rRequest->getRequestCountForFirm($firm, $args['active']);
                        },
                    ],
                    'requests' => [
                        'args' => [
                            'active' => [
                                'type' => Type::boolean(),
                                'defaultValue' => null,
                            ],
                        ],
                        'type' => Types::requestConnection($serviceManager),
                        'resolve' => function (Firm $firm, $args) use ($entityManager) {
                            /** @var RequestRepository $rRequest */
                            $rRequest = $entityManager->getRepository(Request::class);
                            $requests = $rRequest->getRequestsForFirm($firm, $args['active']);

                            return CustomRelay::connectionFromArrayCollection(
                                $requests,
                                [],
                                RequestType::TYPE_NAME
                            );
                        },
                    ],
                    'orderCount' => [
                        'args' => [
                            'active' => [
                                'type' => Type::boolean(),
                                'defaultValue' => null,
                            ],
                            'withActiveSchedules' => [
                                'type' => Type::boolean(),
                                'defaultValue' => null,
                            ],
                            'statuses' => [
                                'type' => Type::listOf(Enums::orderStatusEnumType()),
                                'defaultValue' => [],
                            ],
                            'boxId' => [
                                'type' => Type::ID(),
                                'defaultValue' => null,
                            ],
                            'officeId' => [
                                'type' => Type::ID(),
                                'defaultValue' => null,
                            ],
                        ],
                        'type' => Type::nonNull(Type::int()),
                        'resolve' => function (Firm $firm, $args) use ($entityManager) {
                            /** @var OrderRepository $rOrders */
                            $rOrders = $entityManager->getRepository(Order::class);

                            $active = null;
                            if (isset($args['active']) && $args['active'] !== null) {
                                $active = boolval($args['active']);
                            }

                            $withActiveSchedules = null;
                            if (isset($args['withActiveSchedules']) && $args['withActiveSchedules'] !== null) {
                                $withActiveSchedules = boolval($args['withActiveSchedules']);
                            }

                            $box = null;
                            if (isset($args['boxId'])) {
                                $boxId = RelayIdUtils::getId($args['boxId'], OfficeBoxType::TYPE_NAME);
                                /** @var OfficeBox $box */
                                $box = $entityManager->find(OfficeBox::class, $boxId);
                            }

                            $office = null;
                            if (isset($args['officeId'])) {
                                $officeId = RelayIdUtils::getId($args['officeId'], OfficeType::TYPE_NAME);
                                /** @var Office $car */
                                $office = $entityManager->find(Office::class, $officeId);
                            }

                            return $rOrders->getFirmOrderCountByFilter($firm, $active, $withActiveSchedules, $box, $args['statuses'], $office);
                        }
                    ],
                    'orders' => [
                        'args' => array_merge(
                            Relay::forwardConnectionArgs(),
                            [
                                'active' => [
                                    'type' => Type::boolean(),
                                ],
                                'withActiveSchedules' => [
                                    'type' => Type::boolean(),
                                ],
                                'boxId' => [
                                    'type' => Type::ID(),
                                ],
                                'carId' => [
                                    'type' => Type::ID(),
                                ],
                                'clientId' => [
                                    'type' => Type::ID(),
                                ],
                                'officeId' => [
                                    'type' => Type::ID(),
                                ],
                                'statuses' => [
                                    'type' => Type::listOf(Enums::orderStatusEnumType()),
                                    'defaultValue' => [],
                                ],
                                'orderIdPart' => [
                                    'type' => Type::int(),
                                    'defaultValue' => null,
                                ],
                            ]
                        ),
                        'type' => Types::orderConnection($serviceManager),
                        'resolve' => function (Firm $firm, $args = []) use ($entityManager) {
                            /** @var OrderRepository $rOrders */
                            $rOrders = $entityManager->getRepository(Order::class);

                            $active = null;
                            if (isset($args['active']) && $args['active'] !== null) {
                                $active = boolval($args['active']);
                            }

                            $withActiveSchedules = null;
                            if (isset($args['withActiveSchedules']) && $args['withActiveSchedules'] !== null) {
                                $withActiveSchedules = boolval($args['withActiveSchedules']);
                            }

                            $box = null;
                            if (isset($args['boxId'])) {
                                $boxId = RelayIdUtils::getId($args['boxId'], OfficeBoxType::TYPE_NAME);
                                /** @var OfficeBox $box */
                                $box = $entityManager->find(OfficeBox::class, $boxId);
                            }

                            $car = null;
                            if (isset($args['carId'])) {
                                $carId = RelayIdUtils::getId($args['carId'], CarType::TYPE_NAME);
                                /** @var Car $car */
                                $car = $entityManager->find(Car::class, $carId);
                            }

                            $client = null;
                            if (isset($args['clientId'])) {
                                $carId = RelayIdUtils::getId($args['clientId'], ClientType::TYPE_NAME);
                                /** @var Client $client */
                                $client = $entityManager->find(Client::class, $carId);
                            }

                            $office = null;
                            if (isset($args['officeId'])) {
                                $officeId = RelayIdUtils::getId($args['officeId'], OfficeType::TYPE_NAME);
                                /** @var Office $car */
                                $office = $entityManager->find(Office::class, $officeId);
                            }

                            $orders = $rOrders->getFirmOrdersByFilter(
                                $firm,
                                $active,
                                $withActiveSchedules,
                                $box,
                                $args['statuses'],
                                $office,
                                $car,
                                $client,
                                $args['orderIdPart']
                            );

                            return CustomRelay::connectionFromArrayCollection(
                                $orders,
                                $args,
                                OrderType::TYPE_NAME
                            );
                        }
                    ],
                    'offices' => [
                        'type' => Types::firmOffices($serviceManager),
                        'resolve' => function (Firm $firm, $args) use ($entityManager, $officeAccessService) {
                            if (!$officeAccessService->canViewFirmOffices($firm)) {
                                return [
                                    'officesArray' => null,
                                    'totalCount' => null,
                                ];
                            }

                            /** @var OfficeRepository $rOffice */
                            $rOffice = $entityManager->getRepository('AH\Entity\Office');
                            $offices = $rOffice->getOfficesByFirm($firm);

                            $totalCount = count($offices);

                            return [
                                'officesArray' => $offices,
                                'totalCount' => $totalCount,
                            ];
                        },
                    ],
                    'orderPayments' => [
                        'type' => Types::orderPaymentsConnection($serviceManager),
                        'args' => array_merge(
                            Relay::forwardConnectionArgs(),
                            [
                                'clientId' => [
                                    'type' => Type::id(),
                                ],
                                'cashierId' => [
                                    'type' => Type::id(),
                                ],
                                'paymentType' => [
                                    'type' => Enums::paymentTypeEnumType(),
                                ],
                                'orderIdPart' => [
                                    'type' => Type::int(),
                                    'defaultValue' => null,
                                ],
                            ]
                        ),
                        'resolve' => function (Firm $firm, $args) use ($entityManager) {
                            /** @var OrderPaymentRepository $rOrders */
                            $rOrders = $entityManager->getRepository(OrderPayment::class);

                            $payments = $rOrders->getPaymentForFirm($firm);

                            $criteria = Criteria::create();

                            if (!empty($args['paymentType'])) {
                                $criteria
                                    ->andWhere(Criteria::expr()->eq(
                                        'paymentType',
                                        $args['paymentType']
                                    ));
                            }

                            if (!empty($args['clientId'])) {
                                $clientId = RelayIdUtils::getId($args['clientId'], ClientType::TYPE_NAME);
                                /** @var Client $client */
                                $client = $entityManager->find(Client::class, $clientId);

                                $criteria
                                    ->andWhere(Criteria::expr()->eq(
                                        'client',
                                        $client
                                    ));
                            }

                            if (!empty($args['cashierId'])) {
                                $cashierId = RelayIdUtils::getId($args['cashierId'], ManagerType::TYPE_NAME);
                                /** @var Manager $cashier */
                                $cashier = $entityManager->find(Manager::class, $cashierId);

                                $criteria
                                    ->andWhere(Criteria::expr()->eq(
                                        'cashier',
                                        $cashier
                                    ));
                            }

                            if (!empty($args['orderIdPart'])) {
                                $orderIdPart = $args['orderIdPart'];
                                $payments = $payments->filter(function ($element) use ($orderIdPart) {
                                        /** @var Order $order */
                                        $order = $element->getOrder();
                                        if (!empty($order)) {
                                            return strpos(strval($order->getId()), strval($orderIdPart)) !== false;
                                        }

                                        return false;
                                    });
                            }

                            $payments = $payments
                                ->matching($criteria);

                            return CustomRelay::connectionFromArrayCollection(
                                $payments,
                                isset($args) ? $args : [],
                                OrderPaymentType::TYPE_NAME
                            );
                        }
                    ],
                    'managers' => [
                        'type' => Types::managerConnection($serviceManager),
                        'resolve' => function (Firm $firm) {
                            return CustomRelay::connectionFromArrayCollection(
                                $firm->getManagers(),
                                [],
                                ManagerType::TYPE_NAME
                            );
                        }
                    ],
                    'clients' => [
                        'type' => Types::firmClients($serviceManager),
                        'resolve' => function (Firm $firm, $args) use ($entityManager, $firmAccessService) {
                            if (!$firmAccessService->canViewFirmClients($firm)) {
                                return [
                                    'clientsArray' => null,
                                    'totalCount' => null,
                                    'args' => null,
                                ];
                            }

                            $fioFilter = null;
                            if (isset($args['fio'])) {
                                $fioFilter = trim(strval($args['fio']));
                            }

                            $idFilter = null;
                            if (isset($args['id'])) {
                                $clear = trim(filter_var($args['id'], FILTER_SANITIZE_NUMBER_INT));
                                if ($clear !== '') {
                                    $idFilter = $clear;
                                }
                            }

                            $vinFilter = null;
                            if (isset($args['vin'])) {
                                $vinFilter = trim(strval($args['vin']));
                            }

                            $emailFilter = null;
                            if (isset($args['email'])) {
                                $emailFilter = trim(strval($args['email']));
                            }

                            $phoneFilter = null;
                            if (isset($args['phone'])) {
                                $clear = filter_var($args['phone'], FILTER_SANITIZE_NUMBER_INT);
                                if ($clear !== '') {
                                    $phoneFilter = $clear;
                                }
                            }

                            $registrationSignNumberFilter = null;
                            if (isset($args['registrationSignNumber'])) {
                                $registrationSignNumberFilter = strval($args['registrationSignNumber']);
                            }

                            $afterId = null;
                            if (isset($args['after'])) {
                                $afterId = intval(substr(
                                    base64_decode($args['after']),
                                    strlen(ClientType::TYPE_NAME . ':')
                                ));
                            }

                            $first = null;
                            if (isset($args['first']) && intval($args['first']) > 0) {
                                $first = intval($args['first']);
                            }

                            /** @var ClientRepository $rClient */
                            $rClient = $entityManager->getRepository('AH\Entity\Client');

                            $startFromId = $afterId;
                            $maxResult = null;
                            if ($first > 0) {
                                $maxResult = $first + 1;
                            }

                            $orderType = 'DESC';
                            if (isset($args['orderType'])) {
                                if (!in_array(strtoupper($args['orderType']), ['DESC', 'ASC'])) {
                                    throw new \Exception('Wrong orderType arg provided.');
                                }

                                $orderType = strtoupper($args['orderType']);
                            }

                            $orderBy = '';
                            if (isset($args['orderBy'])) {
                                $orderBy = strtolower($args['orderBy']);
                            }

                            $clientsArray = $rClient->findFirmClientByFilter(
                                $firm,
                                $idFilter,
                                $fioFilter,
                                $phoneFilter,
                                $emailFilter,
                                $registrationSignNumberFilter,
                                $vinFilter,
                                $startFromId,
                                $maxResult,
                                $orderBy,
                                $orderType
                            );

                            $ids = [];
                            foreach ($clientsArray as $client) {
                                if ($client instanceof User) {
                                    $ids[] = $client->getId();
                                } else {
                                    $ids[] = $client['id'];
                                }
                            }

                            $totalCount = $rClient->getFirmClientsCountByFilter(
                                $firm,
                                $fioFilter,
                                $phoneFilter,
                                $emailFilter,
                                $registrationSignNumberFilter
                            );

                            return [
                                'clientsArray' => $clientsArray,
                                'totalCount' => $totalCount, // Всего клиентов по такому фильтру
                                'args' => $args,
                            ];
                        },
                        'args' => array_merge(
                            Relay::forwardConnectionArgs(),
                            [
                                'id' => [
                                    'name' => 'id',
                                    'description' => 'Часть или целый id',
                                    'type' => Type::string()
                                ],
                                'fio' => [
                                    'name' => 'fio',
                                    'description' => 'ФИО клиента',
                                    'type' => Type::string()
                                ],
                                'email' => [
                                    'name' => 'email',
                                    'description' => 'email клиента',
                                    'type' => Type::string()
                                ],
                                'phone' => [
                                    'name' => 'phone',
                                    'description' => 'Сотовый клиента',
                                    'type' => Type::string()
                                ],
                                'registrationSignNumber' => [
                                    'name' => 'registrationSignNumber',
                                    'description' => 'Гос. номер авто клиента',
                                    'type' => Type::string()
                                ],
                                'orderBy' => [
                                    'name' => 'orderBy',
                                    'type' => Type::string()
                                ],
                                'orderType' => [
                                    'name' => 'orderType',
                                    'type' => Type::string()
                                ],
                                'vin' => [
                                    'name' => 'vin',
                                    'type' => Type::string()
                                ],
                            ]
                        ),
                    ],
                    'boxes' => [
                        'args' => [
                            'active' => [
                                'type' => Type::boolean(),
                            ],
                        ],
                        'type' => Type::listOf(Types::officeBoxType($serviceManager)),
                        'resolve' => function (Firm $firm, $args) use ($entityManager) {
                            $rBox = $entityManager->getRepository(OfficeBox::class);

                            $criteria = [];
                            if (isset($args['active']) && $args['active'] !== null) {
                                $criteria['active'] = boolval($args['active']);
                            }

                            $boxes = $rBox->findBy($criteria);

                            return $boxes;
                        }
                    ],
                    'report' => [
                        'type' => Types::firmReportType($serviceManager),
                        'description' => 'Отчеты, статистика и сводки по фирме.',
                        'resolve' => function (Firm $firm) use ($firmAccessService) {
                            if (!$firmAccessService->canViewFirm($firm)) {
                                return null;
                            }

                            return $firm;
                        },
                    ],
                    'points' => [
                        'type' => Type::nonNull(Type::float()),
                        'resolve' => function (Firm $firm) use ($firmAccessService) {
                            if (!$firmAccessService->canViewFirm($firm)) {
                                return null;
                            }

                            return $firm->getPoints();
                        },
                    ],
                    'clientCars' => [
                        'type' => Types::carConnection($serviceManager),
                        'resolve' => function (Firm $firm, $args) use ($entityManager, $firmAccessService) {
                            if (!$firmAccessService->canViewFirmClientsCars($firm)) {
                                return null;
                            }

                            $vinFilter = null;
                            if (isset($args['vin'])) {
                                $vinFilter = trim(strval($args['vin']));
                            }

                            $registrationSignNumberFilter = null;
                            if (isset($args['registrationSignNumber'])) {
                                $registrationSignNumberFilter = strval($args['registrationSignNumber']);
                            }

                            /** @var CarRepository $rCar */
                            $rCar = $entityManager->getRepository(Car::class);
                            $cars = $rCar->findFirmClientCarsByFilter(
                                $firm,
                                $vinFilter,
                                $registrationSignNumberFilter
                            );

                            return CustomRelay::connectionFromArrayCollection(
                                $cars,
                                [],
                                CarType::TYPE_NAME
                            );
                        },
                        'args' => array_merge(
                            Relay::forwardConnectionArgs(),
                            [
                                'registrationSignNumber' => [
                                    'name' => 'registrationSignNumber',
                                    'description' => 'Гос. номер авто клиента',
                                    'type' => Type::string()
                                ],
                                'vin' => [
                                    'name' => 'vin',
                                    'type' => Type::string()
                                ],
                            ]
                        ),
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}