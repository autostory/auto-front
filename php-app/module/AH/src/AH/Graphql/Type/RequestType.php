<?php

namespace AH\Graphql\Type;

use AH\Entity\Request;
use AH\Entity\RequestCommunication;
use AH\GraphQL\Types;
use AH\Repository\RequestCommunicationRepository;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class RequestType extends ObjectType
{
    const TYPE_NAME = 'Request';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Заявка клиента.',
            'fields' => function () use ($serviceManager) {
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор.',
                        'resolve' => function (Request $request) {
                            return Relay::toGlobalId(self::TYPE_NAME, $request->getId());
                        },
                    ],
                    'car' => [
                        'type' => Types::carType($serviceManager),
                        'description' => 'Автомобиль заявки.',
                        'resolve' => function (Request $request) {
                            return $request->getCar();
                        },
                    ],
                    'client' => [
                        'type' => Types::clientType($serviceManager),
                        'description' => 'Клиент заявки.',
                        'resolve' => function (Request $request) {
                            return $request->getClient();
                        },
                    ],
                    'userMessage' => [
                        'type' => Type::string(),
                        'description' => 'Сообщение которое отправил пользователь в заявке.',
                        'resolve' => function (Request $request) {
                            return $request->getUserMessage();
                        },
                    ],
                    'makeAt' => [
                        'type' => Types::dateType(),
                        'description' => 'Когда заявка поступила.',
                        'resolve' => function (Request $request) {
                            return $request->getMakeAt();
                        },
                    ],
                    'order' => [
                        'type' => Types::orderType($serviceManager),
                        'description' => 'Заявка из который был сформирован заказ.',
                        'resolve' => function (Request $request) {
                            return $request->getOrder();
                        },
                    ],
                    'active' => [
                        'type' => Type::nonNull(Type::boolean()),
                        'description' => 'Активна ли заявка.',
                        'resolve' => function (Request $request) {
                            return $request->getActive();
                        },
                    ],
                    'communicationHistory' => [
                        'type' => Type::listOf(Types::requestCommunicationType($serviceManager)),
                        'description' => 'Список событие коммуникации по заявке.',
                        'resolve' => function (Request $request) {
                            return $request->getRequestCommunications();
                        },
                    ],
                    'lastCommunication' => [
                        'type' => Types::requestCommunicationType($serviceManager),
                        'description' => 'Последнее событие коммуникации по заявке.',
                        'resolve' => function (Request $request) use ($entityManager) {
                            /** @var RequestCommunicationRepository $rRequestCommunicationRepository */
                            $rRequestCommunicationRepository = $entityManager->getRepository(RequestCommunication::class);

                            return $rRequestCommunicationRepository->getLastRequestCommunicationForRequest($request);
                        },
                    ]
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}