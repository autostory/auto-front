<?php

namespace AH\Graphql\Type;

use AH\Core\Acl\CarAccess;
use AH\Core\Acl\ClientAccess;
use AH\Core\Acl\FirmAccess;
use AH\Core\Acl\OfficeAccess;
use AH\Core\Utils\RelayIdUtils;
use AH\Entity\BoxSchedule;
use AH\Entity\Car;
use AH\Entity\Client;
use AH\Entity\Event\GasEvent;
use AH\Entity\Event\SparePartServiceEvent;
use AH\Entity\Office;
use AH\Entity\OfficeBox;
use AH\Entity\Order;
use AH\Entity\Request;
use AH\Entity\Service\ServiceWork;
use AH\Entity\User;
use AH\GraphQL\Enums;
use AH\Graphql\Type\CatalogCar\CatalogCarModelType;
use AH\Graphql\Type\CatalogCar\CatalogCarModificationType;
use AH\Graphql\Type\CatalogCar\CatalogCarSerieType;
use AH\GraphQL\Types;
use AH\Entity\Manager;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;
use AH\Entity\CatalogCar;
use Ah\GraphQL\Type\CatalogCar as CatalogCarTypes;
use GraphQL\Type\Definition\UnionType;

class ViewerType extends ObjectType
{
    const TYPE_NAME = 'Viewer';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Current authenticated user info.',
            'fields' => function () use ($serviceManager) {
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                /** @var CarAccess $carAccessService */
                $carAccessService = $serviceManager->get(CarAccess::class);
                /** @var ClientAccess $clientAccessService */
                $clientAccessService = $serviceManager->get(ClientAccess::class);
                /** @var FirmAccess $firmAccessService */
                $firmAccessService = $serviceManager->get(FirmAccess::class);
                /** @var OfficeAccess $officeAccessService */
                $officeAccessService = $serviceManager->get(OfficeAccess::class);

                /** @var \Zend\Authentication\AuthenticationService $authenticationService */
                $authenticationService = $serviceManager->get('AuthenticationService');

                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор авторизированного пользователя.',
                        'resolve' => function (?User $user) {
                            if (empty($user) || empty($user->getId())) {
                                return Relay::toGlobalId(self::TYPE_NAME, 0);
                            }

                            return Relay::toGlobalId(self::TYPE_NAME, $user->getId());
                        },
                    ],
                    'events' => [
                        'description' => 'Все события связанные с техникой.',
                        'resolve' => function (?User $user) use ($entityManager) {
                            $rGasEvent = $entityManager->getRepository(GasEvent::class);
                            $allGasEvents = $rGasEvent->findAll();

                            $rSparePartServiceEvent = $entityManager->getRepository(SparePartServiceEvent::class);
                            $allSparePartServiceEvents = $rSparePartServiceEvent->findAll();

                            return array_merge($allGasEvents, $allSparePartServiceEvents);
                        },
                        'type' => Type::listOf(new UnionType([
                            'name' => 'CarEvents',
                            'types' => [
                                Types::gasEventType($serviceManager),
                                Types::sparePartServiceEventType($serviceManager),
                            ],
                            'resolveType' => function ($event) use ($serviceManager) {
                                if ($event instanceof GasEvent) {
                                    return Types::gasEventType($serviceManager);
                                } else if ($event instanceof SparePartServiceEvent) {
                                    return Types::sparePartServiceEventType($serviceManager);
                                }

                                return null;
                            }
                        ])),
                    ],
                    'basic' => [
                        'type' => Types::basic($serviceManager),
                        'description' => 'Базовые типы.',
                        'resolve' => function (?User $user) {
                            return $user;
                        },
                    ],
                    'isAuthenticated' => [
                        'type' => Type::nonNull(Type::boolean()),
                        'description' => 'Авторизирован ли пользователь.',
                        'resolve' => function () use ($authenticationService) {
                            return $authenticationService->hasIdentity();
                        },
                    ],
                    'fullName' => [
                        'type' => Type::string(),
                        'description' => 'Полное имя авторизированного пользователя.',
                        'resolve' => function (?User $user) {
                            return $user->getFullName();
                        },
                    ],
                    'firstName' => [
                        'type' => Type::string(),
                        'description' => 'Имя авторизированного пользователя.',
                        'resolve' => function (?User $user) {
                            return $user->getFirstName();
                        },
                    ],
                    'email' => [
                        'type' => Type::string(),
                        'resolve' => function (?User $user) {
                            return $user->getRegistrationEmail();
                        },
                    ],
                    'phone' => [
                        'type' => Type::string(),
                        'resolve' => function (?User $user) {
                            return $user->getRegistrationPhone();
                        },
                    ],
                    'surname' => [
                        'type' => Type::string(),
                        'description' => 'Фамилия авторизированного пользователя.',
                        'resolve' => function (?User $user) {
                            return $user->getSurname();
                        },
                    ],
                    'patronymic' => [
                        'type' => Type::string(),
                        'description' => 'Отчество авторизированного пользователя.',
                        'resolve' => function (?User $user) {
                            return $user->getPatronymic();
                        },
                    ],
                    'car' => [
                        'type' => Types::carType($serviceManager),
                        'resolve' => function (?User $user, $args) use ($entityManager, $carAccessService) {
                            $carId = 0;
                            if (isset($args['carId'])) {
                                $carId = RelayIdUtils::getId($args['carId'], CarType::TYPE_NAME);
                            }

                            if ($carId <= 0) {
                                return null;
                            }

                            /** @var Car $car */
                            $car = $entityManager->find(Car::class, $carId);

                            if (empty($car)) {
                                return null;
                            }

                            if (!$carAccessService->canViewCar($car)) {
                                return null;
                            }

                            return $car;
                        },
                        'args' => [
                            'carId' => [
                                'name' => 'carId',
                                'description' => 'Id автомобиля',
                                'type' => Type::nonNull(Type::id())
                            ],
                        ],
                    ],
                    'serviceWork' => [
                        'type' => Types::serviceWorkType($serviceManager),
                        'resolve' => function (?User $user, $args) use ($entityManager, $carAccessService) {
                            $serviceWorkId = RelayIdUtils::getId(
                                $args['serviceWorkId'],
                                ServiceWorkType::TYPE_NAME
                            );

                            /** @var ServiceWork $serviceWork */
                            $serviceWork = $entityManager->find(ServiceWork::class, $serviceWorkId);

                            if (empty($serviceWork)) {
                                return null;
                            }

                            if (!$carAccessService->canViewCar($serviceWork->getOrder()->getCar())) {
                                return null;
                            }

                            return $serviceWork;
                        },
                        'args' => [
                            'serviceWorkId' => [
                                'name' => 'serviceWorkId',
                                'description' => 'Id работы',
                                'type' => Type::nonNull(Type::id())
                            ],
                        ],
                    ],
                    'client' => [
                        'type' => Types::clientType($serviceManager),
                        'resolve' => function (?User $user, $args) use ($entityManager, $clientAccessService) {
                            $clientId = 0;
                            if (isset($args['clientId'])) {
                                $clientId = RelayIdUtils::getId($args['clientId'], ClientType::TYPE_NAME);
                            }

                            /** @var Client $client */
                            $client = $entityManager->find(Client::class, $clientId);

                            if (empty($client)) {
                                return null;
                            }

                            if (!$clientAccessService->canViewClient($client)) {
                                return null;
                            }

                            return $client;
                        },
                        'args' => [
                            'clientId' => [
                                'name' => 'clientId',
                                'description' => 'Id клиента',
                                'type' => Type::nonNull(Type::id())
                            ],
                        ],
                    ],
                    'me' => [ // TODO: придумать название получше...
                        'type' => Types::clientType($serviceManager),
                        'description' => 'Тут данные по клиенту, если авторизовался как клиент',
                        'resolve' => function (?User $user, $args) {
                            if (empty($user) || !($user instanceof Client)) {
                                return null;
                            }

                            return $user;
                        },
                    ],
                    'manager' => [
                        'type' => Types::managerType($serviceManager),
                        'resolve' => function (?User $user, $args) use ($entityManager, $clientAccessService) {
                            $managerId = RelayIdUtils::getId($args['managerId'], ManagerType::TYPE_NAME);

                            /** @var Manager $manager */
                            $manager = $entityManager->find(Manager::class, $managerId);

                            if (empty($manager)) {
                                return null;
                            }

                            return $manager;
                        },
                        'args' => [
                            'managerId' => [
                                'name' => 'managerId',
                                'description' => 'Id менеджера',
                                'type' => Type::nonNull(Type::id())
                            ],
                        ],
                    ],
                    'box' => [
                        'type' => Types::officeBoxType($serviceManager),
                        'resolve' => function (?User $user, $args) use ($entityManager, $carAccessService) {
                            $boxId = 0;
                            if (isset($args['boxId'])) {
                                $boxId = RelayIdUtils::getId($args['boxId'], OfficeBoxType::TYPE_NAME);
                            }

                            if ($boxId <= 0) {
                                return null;
                            }

                            /** @var OfficeBox $box */
                            $box = $entityManager->find(OfficeBox::class, $boxId);

                            if (empty($box)) {
                                return null;
                            }

                            return $box;
                        },
                        'args' => [
                            'boxId' => [
                                'name' => 'boxId',
                                'description' => 'Id бокса',
                                'type' => Type::nonNull(Type::id())
                            ],
                        ],
                    ],
                    'request' => [
                        'type' => Types::requestType($serviceManager),
                        'resolve' => function (?User $user, $args) use ($entityManager, $carAccessService) {
                            $requestId = 0;
                            if (isset($args['requestId'])) {
                                $requestId = RelayIdUtils::getId($args['requestId'], RequestType::TYPE_NAME);
                            }

                            if ($requestId <= 0) {
                                return null;
                            }

                            /** @var Request $request */
                            $request = $entityManager->find(Request::class, $requestId);

                            if (empty($request)) {
                                return null;
                            }

                            return $request;
                        },
                        'args' => [
                            'requestId' => [
                                'name' => 'requestId',
                                'description' => 'Id заявки',
                                'type' => Type::nonNull(Type::id())
                            ],
                        ],
                    ],
                    'office' => [
                        'type' => Types::officeType($serviceManager),
                        'resolve' => function (?User $user, $args) use ($entityManager, $officeAccessService) {
                            $officeId = 0;
                            if (isset($args['officeId'])) {
                                $officeId = RelayIdUtils::getId($args['officeId'], OfficeType::TYPE_NAME);
                            }

                            if ($officeId <= 0) {
                                return null;
                            }

                            /** @var Office $office */
                            $office = $entityManager->find(Office::class, $officeId);

                            if (empty($office)) {
                                return null;
                            }

                            if (!$officeAccessService->canViewOffice($office)) {
                                return null;
                            }

                            return $office;
                        },
                        'args' => [
                            'officeId' => [
                                'name' => 'officeId',
                                'description' => 'Id офиса.',
                                'type' => Type::nonNull(Type::id())
                            ],
                        ],
                    ],
                    'myFirm' => [
                        'type' => Types::firmType($serviceManager),
                        'resolve' => function (?User $manager) use ($serviceManager, $firmAccessService) {
                            if (
                                !empty($manager)
                                && $manager instanceof Manager
                                && $firmAccessService->canViewFirm($manager->getFirm())
                            ) {
                                return $manager->getFirm();
                            }

                            return null;
                        },
                    ],
                    'schedule' => [
                        'type' => Types::boxScheduleType($serviceManager),
                        'resolve' => function (?User $manager, $args) use ($serviceManager, $entityManager) {
                            $scheduleId = 0;
                            if (isset($args['scheduleId'])) {
                                $scheduleId = RelayIdUtils::getId($args['scheduleId'], BoxScheduleType::TYPE_NAME);
                            }

                            if ($scheduleId <= 0) {
                                return null;
                            }

                            /** @var BoxSchedule $schedule */
                            $schedule = $entityManager->find(BoxSchedule::class, $scheduleId);

                            if (empty($schedule)) {
                                return null;
                            }

                            return $schedule;
                        },
                        'args' => [
                            'scheduleId' => [
                                'name' => 'scheduleId',
                                'type' => Type::nonNull(Type::id())
                            ],
                        ],
                    ],
                    'order' => [
                        'type' => Types::orderType($serviceManager),
                        'resolve' => function (?User $manager, $args) use ($serviceManager, $entityManager) {
                            $orderId = 0;
                            if (isset($args['orderId'])) {
                                $orderId = RelayIdUtils::getId($args['orderId'], OrderType::TYPE_NAME);
                            }

                            if ($orderId <= 0) {
                                return null;
                            }

                            /** @var Order $order */
                            $order = $entityManager->find(Order::class, $orderId);

                            if (empty($order)) {
                                return null;
                            }

                            return $order;
                        },
                        'args' => [
                            'orderId' => [
                                'name' => 'orderId',
                                'type' => Type::nonNull(Type::id())
                            ],
                        ],
                    ],
                    'catalogCarTypes' => [
                        'type' => Type::listOf(Types::catalogCarType($serviceManager)),
                        'description' => 'Все виды транспорта.',
                        'resolve' => function () use ($entityManager) {
                            $rCarType = $entityManager->getRepository(CatalogCar\CarType::class);
                            $allCarTypes = $rCarType->findAll();

                            return $allCarTypes;
                        },
                    ],
                    'catalogCarMarks' => [
                        'type' => Type::listOf(Types::catalogCarMark($serviceManager)),
                        'description' => 'Марки вида транспорта.',
                        'args' => [
                            'carTypeId' => [
                                'name' => 'carTypeId',
                                'description' => 'Id вида транспорта.',
                                'type' => Type::nonNull(Type::id()),
                            ],
                        ],
                        'resolve' => function ($payload, $args) use ($entityManager) {
                            if (!isset($args['carTypeId'])) {
                                throw new \Exception('Wrong carTypeId was provided.');
                            }

                            $carTypeId = RelayIdUtils::getId(
                                $args['carTypeId'],
                                CatalogCarTypes\CatalogCarTypeType::TYPE_NAME
                            );

                            if ($carTypeId <= 0) {
                                throw new \Exception('Wrong carTypeId was provided.');
                            }

                            $rCarType = $entityManager->getRepository(CatalogCar\CarType::class);
                            $carType = $rCarType->find($carTypeId);

                            if (empty($carType)) {
                                throw new \Exception('СarType not found.');
                            }

                            $rCarMark = $entityManager->getRepository(CatalogCar\CarMark::class);
                            $carMarks = $rCarMark->findBy(['carType' => $carType]);

                            return $carMarks;
                        },
                    ],
                    'catalogCarModels' => [
                        'type' => Type::listOf(Types::catalogCarModel($serviceManager)),
                        'description' => 'Модельный ряд марки транспорта.',
                        'args' => [
                            'carMarkId' => [
                                'name' => 'carMarkId',
                                'description' => 'Id марки транспорта.',
                                'type' => Type::nonNull(Type::id()),
                            ],
                        ],
                        'resolve' => function ($payload, $args) use ($entityManager) {
                            if (!isset($args['carMarkId'])) {
                                throw new \Exception('Wrong carMarkId was provided.');
                            }

                            $carMarkId = 0;
                            $idComponents = Relay::fromGlobalId($args['carMarkId']);
                            if ($idComponents['type'] === CatalogCarTypes\CatalogCarMarkType::TYPE_NAME) {
                                $carMarkId = intval($idComponents['id']);
                            }

                            if ($carMarkId <= 0) {
                                throw new \Exception('Wrong carMarkId was provided.');
                            }

                            $rCarMark = $entityManager->getRepository(CatalogCar\CarMark::class);
                            $carMark = $rCarMark->find($carMarkId);

                            if (empty($carMark)) {
                                throw new \Exception('СarMark not found.');
                            }

                            $rCarModel = $entityManager->getRepository(CatalogCar\CarModel::class);
                            $carModels = $rCarModel->findBy(['carMark' => $carMark]);

                            return $carModels;
                        },
                    ],
                    'catalogCarGenerations' => [
                        'type' => Type::listOf(Types::catalogCarGeneration($serviceManager)),
                        'description' => 'Поколение модели транспорта.',
                        'args' => [
                            'carModelId' => [
                                'name' => 'carModelId',
                                'description' => 'Id модели транспорта.',
                                'type' => Type::nonNull(Type::id()),
                            ],
                        ],
                        'resolve' => function ($payload, $args) use ($entityManager) {
                            if (!isset($args['carModelId'])) {
                                throw new \Exception('Wrong carModelId was provided.');
                            }

                            $carModelId = 0;
                            $idComponents = Relay::fromGlobalId($args['carModelId']);
                            if ($idComponents['type'] === CatalogCarTypes\CatalogCarModelType::TYPE_NAME) {
                                $carModelId = intval($idComponents['id']);
                            }

                            if ($carModelId <= 0) {
                                throw new \Exception('Wrong carModelId was provided.');
                            }

                            $rCarModel = $entityManager->getRepository(CatalogCar\CarModel::class);
                            $carModel = $rCarModel->find($carModelId);

                            if (empty($carModel)) {
                                throw new \Exception('СarMark not found.');
                            }

                            $rCarGeneration = $entityManager->getRepository(CatalogCar\CarGeneration::class);
                            $carGenerations = $rCarGeneration->findBy(['carModel' => $carModel], ['yearBegin' => 'ASC']);

                            return $carGenerations;
                        },
                    ],
                    'catalogCarModifications' => [
                        'type' => Type::listOf(Types::catalogCarModificationType($serviceManager)),
                        'description' => 'Модификации транспорта.',
                        'args' => [
                            'carSerieId' => [
                                'name' => 'carSerieId',
                                'type' => Type::id(),
                            ],
                            'carModelId' => [
                                'name' => 'carModelId',
                                'type' => Type::id(),
                            ],
                        ],
                        'resolve' => function ($payload, $args) use ($entityManager) {
                            if (isset($args['carSerieId'])) {
                                $carSerieId = RelayIdUtils::getId($args['carSerieId'], CatalogCarSerieType::TYPE_NAME);
                            }

                            if (isset($args['carModelId'])) {
                                $carModelId = RelayIdUtils::getId($args['carModelId'], CatalogCarModelType::TYPE_NAME);
                            }

                            $criteria = [];
                            if (!empty($carSerieId)) {
                                $criteria['carSerie'] = $carSerieId;
                            }

                            if (!empty($carModelId)) {
                                $criteria['carModel'] = $carModelId;
                            }

                            $rCarModification = $entityManager->getRepository(CatalogCar\CarModification::class);
                            $carModifications = $rCarModification->findBy($criteria);

                            return $carModifications;
                        },
                    ],
                    'catalogCarEquipments' => [
                        'type' => Type::listOf(Types::catalogCarEquipmentType($serviceManager)),
                        'description' => 'Комплектации транспорта.',
                        'args' => [
                            'carModificationId' => [
                                'name' => 'carModificationId',
                                'type' => Type::id(),
                            ],
                        ],
                        'resolve' => function ($payload, $args) use ($entityManager) {
                            if (isset($args['carModificationId'])) {
                                $carModificationId = RelayIdUtils::getId($args['carModificationId'], CatalogCarModificationType::TYPE_NAME);
                            }

                            $criteria = [];
                            if (!empty($carModificationId)) {
                                $criteria['carModification'] = $carModificationId;
                            }

                            $rCatalogCarEquipment = $entityManager->getRepository(CatalogCar\CarEquipment::class);
                            $catalogCarEquipments = $rCatalogCarEquipment->findBy($criteria);

                            return $catalogCarEquipments;
                        },
                    ],
                    'catalogCarSeries' => [
                        'type' => Type::listOf(Types::catalogCarSerie($serviceManager)),
                        'description' => 'Серии транспорта.',
                        'args' => [
                            'carModelId' => [
                                'name' => 'carModelId',
                                'type' => Type::id(),
                            ],
                        ],
                        'resolve' => function ($payload, $args) use ($entityManager) {
                            if (isset($args['carModelId'])) {
                                $carModelId = RelayIdUtils::getId($args['carModelId'], CatalogCarModelType::TYPE_NAME);
                            }

                            $criteria = [];
                            if (!empty($carModelId)) {
                                $criteria['carModel'] = $carModelId;
                            }

                            $rCatalogCarSerie = $entityManager->getRepository(CatalogCar\CarSerie::class);
                            $catalogCarSeries = $rCatalogCarSerie->findBy($criteria);

                            return $catalogCarSeries;
                        },
                    ],
                    'accessRights' => [
                        'type' => Types::accessRightsType(),
                        'resolve' => function (?User $user) {
                            if (empty($user)) {
                                return null;
                            }

                            return [
                                'canCreateUserAsManager' => $user instanceof Manager
                            ];
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}