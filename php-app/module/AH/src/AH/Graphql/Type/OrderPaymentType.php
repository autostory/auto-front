<?php

namespace AH\Graphql\Type;

use AH\Entity\OrderPayment;
use AH\GraphQL\Enums;
use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class OrderPaymentType extends ObjectType
{
    const TYPE_NAME = 'OrderPayment';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Оплаты по заказу.',
            'fields' => function () use ($serviceManager) {
                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор.',
                        'resolve' => function (OrderPayment $payment) {
                            return Relay::toGlobalId(self::TYPE_NAME, $payment->getId());
                        },
                    ],
                    'firm' => [
                        'type' => Type::nonNull(Types::firmType($serviceManager)),
                        'resolve' => function (OrderPayment $payment) {
                            return $payment->getFirm();
                        },
                    ],
                    'cashier' => [
                        'type' => Type::nonNull(Types::managerType($serviceManager)),
                        'resolve' => function (OrderPayment $payment) {
                            return $payment->getCashier();
                        },
                    ],
                    'client' => [
                        'type' => Type::nonNull(Types::clientType($serviceManager)),
                        'resolve' => function (OrderPayment $payment) {
                            return $payment->getClient();
                        },
                    ],
                    'order' => [
                        'type' => Type::nonNull(Types::orderType($serviceManager)),
                        'resolve' => function (OrderPayment $payment) {
                            return $payment->getOrder();
                        },
                    ],
                    'paymentType' => [
                        'type' => Type::nonNull(Enums::paymentTypeEnumType()),
                        'resolve' => function (OrderPayment $payment) {
                            return $payment->getPaymentType();
                        },
                    ],
                    'note' => [
                        'type' => Type::string(),
                        'resolve' => function (OrderPayment $payment) {
                            return $payment->getNote();
                        },
                    ],
                    'amount' => [
                        'type' => Type::nonNull(Type::float()),
                        'resolve' => function (OrderPayment $payment) {
                            return $payment->getAmount();
                        },
                    ],
                    'makeAt' => [
                        'type' => Type::nonNull(Types::dateType()),
                        'resolve' => function (OrderPayment $payment) {
                            return $payment->getMakeAt();
                        },
                    ],
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}