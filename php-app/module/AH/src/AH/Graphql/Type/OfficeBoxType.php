<?php

namespace AH\Graphql\Type;

use AH\Entity\OfficeBox;
use AH\GraphQL\Types;
use AH\Repository\OfficeBoxRepository;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class OfficeBoxType extends ObjectType
{
    const TYPE_NAME = 'OfficeBox';

    public function __construct(ServiceManager $serviceManager)
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Бокс офиса.',
            'fields' => function () use ($serviceManager) {
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор бокса.',
                        'resolve' => function (OfficeBox $officeBox) {
                            return Relay::toGlobalId(self::TYPE_NAME, $officeBox->getId());
                        },
                    ],
                    'title' => [
                        'type' => Type::string(),
                        'description' => 'Название бокса.',
                        'resolve' => function (OfficeBox $officeBox) {
                            return $officeBox->getTitle();
                        },
                    ],
                    'active' => [
                        'type' => Type::boolean(),
                        'description' => 'Активирован ли офис.',
                        'resolve' => function (OfficeBox $officeBox) {
                            return $officeBox->getActive();
                        },
                    ],
                    'office' => [
                        'type' => Types::officeType($serviceManager),
                        'description' => 'Офис бокса.',
                        'resolve' => function (OfficeBox $officeBox) {
                            return $officeBox->getOffice();
                        },
                    ],
                    'schedules' => [
                        'type' => Type::listOf(Types::boxScheduleType($serviceManager)),
                        'description' => 'Расписания в боксе.',
                        'resolve' => function (OfficeBox $officeBox) {
                            return $officeBox->getSchedules();
                        },
                    ],
                    'releaseDateTime' => [
                        'type' => Types::dateType(),
                        'description' => 'Когда бок будет полностью свободен.',
                        'resolve' => function (OfficeBox $officeBox) use ($entityManager) {
                            /** @var OfficeBoxRepository $rOfficeBoxRepository */
                            $rOfficeBoxRepository = $entityManager->getRepository(OfficeBox::class);

                            return $rOfficeBoxRepository->getReleaseDateTime($officeBox);
                        },
                    ]
                ];
            },
            'interfaces' => [Types::nodeInterface($serviceManager)]
        ];

        parent::__construct($config);
    }
}