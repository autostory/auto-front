<?php

namespace AH\Graphql\Type;

use AH\Core\Acl\OfficeAccess;
use AH\Entity\Manager;
use AH\GraphQL\Interfaces;
use AH\GraphQL\Types;
use AH\Repository\OfficeRepository;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class ManagerType extends ObjectType
{
    const TYPE_NAME = 'Manager';

    public function __construct(ServiceManager $serviceManager)
    {
        $userTypeInterface = Interfaces::userTypeInterface($serviceManager);

        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Manager info.',
            'interfaces' => [
                Types::nodeInterface($serviceManager),
                $userTypeInterface,
            ],
            'fields' => function () use ($serviceManager, $userTypeInterface) {
                /** @var \Doctrine\ORM\EntityManager $entityManager */
                $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');
                /** @var OfficeAccess $officeAccessService */
                $officeAccessService = $serviceManager->get(OfficeAccess::class);

                return [
                    'id' => [
                        'type' => Type::nonNull(Type::id()),
                        'description' => 'Глобально уникальный идентификатор менеджера.',
                        'resolve' => function (Manager $manager) {
                            return Relay::toGlobalId(self::TYPE_NAME, $manager->getId());
                        },
                    ],
                    $userTypeInterface->getField('email'),
                    $userTypeInterface->getField('phone'),
                    $userTypeInterface->getField('fullName'),
                    $userTypeInterface->getField('firstName'),
                    $userTypeInterface->getField('surname'),
                    $userTypeInterface->getField('patronymic'),
                    'active' => [
                        'type' => Type::boolean(),
                        'resolve' => function (Manager $manager)  {
                            return boolval($manager->getActive());
                        },
                    ],
                    'offices' => [
                        'type' => Types::firmOffices($serviceManager),
                        'resolve' => function (Manager $manager) use ($entityManager, $officeAccessService) {
                            $firm = $manager->getFirm();
                            if (!$officeAccessService->canViewFirmOffices($firm)) {
                                return [
                                    'officesArray' => null,
                                    'totalCount' => null,
                                ];
                            }

                            /** @var OfficeRepository $rOffice */
                            $rOffice = $entityManager->getRepository('AH\Entity\Office');
                            $offices = $rOffice->getOfficesByFirm($firm);

                            $totalCount = count($offices);

                            return [
                                'officesArray' => $offices,
                                'totalCount' => $totalCount,
                            ];
                        },
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}