<?php

namespace AH\Graphql\Type;

use AH\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class MutationInputValidationErrorMessagesType extends ObjectType
{
    const TYPE_NAME = 'MutationInputValidationErrorMessage';

    public function __construct()
    {
        $config = [
            'name' => self::TYPE_NAME,
            'description' => 'Список сообщений об ошибках валидации при исполнении мутации.',
            'fields' => function () {
                return [
                    'messages' => [
                        'type' => Type::listOf(Types::errorMessage()),
                        'description' => 'Набор сообщений об ошибках.',
                    ],
                ];
            },
        ];

        parent::__construct($config);
    }
}