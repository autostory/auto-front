<?php

namespace AH\GraphQL;

use AH\Core\Acl\CarAccess;
use AH\Core\Acl\ClientAccess;
use AH\Core\Acl\FirmAccess;
use AH\Core\Acl\OfficeAccess;
use AH\Entity\Car;
use AH\Entity\Client;
use AH\Entity\Firm;
use AH\Entity\Manager;
use AH\Entity\Office;
use AH\Entity\Order;
use AH\Entity\Request;
use AH\Graphql\Type\CarType;
use AH\Graphql\Type\ClientType;
use AH\Graphql\Type\FirmType;
use AH\Graphql\Type\ManagerType;
use AH\Graphql\Type\OfficeType;
use AH\Graphql\Type\OrderType;
use AH\Graphql\Type\RequestType;
use AH\Graphql\Type\ViewerType;
use GraphQLRelay\Relay;
use Zend\ServiceManager\ServiceManager;

class NodeDefinition
{
    public static function getNodeDefinition(ServiceManager $serviceManager)
    {
        /** @var \Zend\Authentication\AuthenticationService $authenticationService */
        $authenticationService = $serviceManager->get('AuthenticationService');
        $authUser = null;
        if ($authenticationService->hasIdentity()) {
            $authUser = $authenticationService->getIdentity();
        }
        /** @var ClientAccess $clientAccess */
        $clientAccess = $serviceManager->get(ClientAccess::class);
        /** @var CarAccess $carAccess */
        $carAccess = $serviceManager->get(CarAccess::class);
        /** @var FirmAccess $firmAccess */
        $firmAccess = $serviceManager->get(FirmAccess::class);

        /** @var OfficeAccess $officeAccess */
        $officeAccess = $serviceManager->get(OfficeAccess::class);
        /** @var \Doctrine\ORM\EntityManager $entityManager */
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

        $runtimeNodeType = null; // Т.к. типы у нас не соответствуют сущностям, то надо помогать.
        return Relay::nodeDefinitions(
        // The ID fetcher definition
            function ($globalId) use (
                $entityManager,
                $authUser,
                $clientAccess,
                $carAccess,
                $firmAccess,
                $officeAccess,
                &$runtimeNodeType
            ) {
                $idComponents = Relay::fromGlobalId($globalId);

                if (
                    !isset($idComponents['type'])
                    || mb_strlen($idComponents['type']) === 0
                    || !isset($idComponents['id'])
                    || intval($idComponents['id']) <= 0
                ) {
                    return null;
                }

                $type = $idComponents['type'];
                $id = intval($idComponents['id']);

                switch (strtolower($type)) {
                    case(strtolower(ViewerType::TYPE_NAME)):
                        $runtimeNodeType = ViewerType::TYPE_NAME;

                        return $authUser;
                        break;
                    case(strtolower(CarType::TYPE_NAME)):
                        $runtimeNodeType = CarType::TYPE_NAME;

                        /** @var Car $car */
                        $car = $entityManager->find(Car::class, $id);
                        if (empty($car) || !$carAccess->canViewCar($car)) {
                            return null;
                        }

                        return $car;
                        break;
                    case(strtolower(FirmType::TYPE_NAME)):
                        $runtimeNodeType = FirmType::TYPE_NAME;

                        /** @var Firm $firm */
                        $firm = $entityManager->find(Firm::class, $id);
                        if (empty($firm) || !$firmAccess->canViewFirm($firm)) {
                            return null;
                        }

                        return $firm;
                        break;
                    case(strtolower(OfficeType::TYPE_NAME)):
                        $runtimeNodeType = OfficeType::TYPE_NAME;

                        /** @var Office $office */
                        $office = $entityManager->find(Office::class, $id);
                        if (empty($office) || !$officeAccess->canViewOffice($office)) {
                            return null;
                        }

                        return $office;
                        break;
                    case(strtolower(RequestType::TYPE_NAME)):
                        $runtimeNodeType = RequestType::TYPE_NAME;

                        /** @var Request $request */
                        $request = $entityManager->find(Request::class, $id);
                        if (empty($request)) {
                            return null;
                        }

                        return $request;
                        break;
                    case(strtolower(OrderType::TYPE_NAME)):
                        $runtimeNodeType = OrderType::TYPE_NAME;

                        /** @var Order $order */
                        $order = $entityManager->find(Order::class, $id);
                        if (empty($order)) {
                            return null;
                        }

                        return $order;
                        break;
                    case(strtolower(ClientType::TYPE_NAME)):
                        $runtimeNodeType = ClientType::TYPE_NAME;

                        /** @var Client $client */
                        $client = $entityManager->find(Client::class, $id);
                        if (empty($client)) {
                            return null;
                        }

                        return $client;
                        break;
                    case(strtolower(ManagerType::TYPE_NAME)):
                        $runtimeNodeType = ManagerType::TYPE_NAME;

                        /** @var Manager $manager */
                        $manager = $entityManager->find(Manager::class, $id);
                        if (empty($manager)) {
                            return null;
                        }

                        return $manager;
                        break;
                    default:
                        return null;
                        break;
                }
            },
            // Type resolver
            function ($object) use ($serviceManager, &$runtimeNodeType) {
                $runtimeNodeType = strtolower($runtimeNodeType);
                // Используется $runtimeNodeType т.к. по $object невозможно определить какую именно ноду запросили.

                if (!empty($object)) {
                    if ($runtimeNodeType === strtolower(ViewerType::TYPE_NAME)) {
                        return Types::viewer($serviceManager);
                    }

                    if ($runtimeNodeType === strtolower(ClientType::TYPE_NAME)) {
                        return Types::clientType($serviceManager);
                    }

                    if ($runtimeNodeType === strtolower(ManagerType::TYPE_NAME)) {
                        return Types::managerType($serviceManager);
                    }

                    if ($runtimeNodeType === strtolower(CarType::TYPE_NAME)) {
                        return Types::carType($serviceManager);
                    }

                    if ($runtimeNodeType === strtolower(FirmType::TYPE_NAME)) {
                        return Types::firmType($serviceManager);
                    }

                    if ($runtimeNodeType === strtolower(OfficeType::TYPE_NAME)) {
                        return Types::officeType($serviceManager);
                    }

                    if ($runtimeNodeType === strtolower(RequestType::TYPE_NAME)) {
                        return Types::requestType($serviceManager);
                    }

                    if ($runtimeNodeType === strtolower(OrderType::TYPE_NAME)) {
                        return Types::orderType($serviceManager);
                    }
                }

                throw new \Exception('Такого типа нет. Сообщить об ошибке бэкенд разработчику.');
            }
        );
    }
}