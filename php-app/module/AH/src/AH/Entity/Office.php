<?php

namespace AH\Entity;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="office")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AH\Repository\OfficeRepository")
 */
class Office implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="office_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var Firm
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Firm", inversedBy="offices")
     * @ORM\JoinColumn(name="firm_id", referencedColumnName="id", nullable=false)
     */
    private $firm;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="AH\Entity\Manager", inversedBy="offices")
     * @ORM\JoinTable(name="office_manager_linker",
     *      joinColumns={@ORM\JoinColumn(name="office_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="manager_id", referencedColumnName="id")}
     * )
     **/
    private $managers;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\Order", mappedBy="office")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $orders;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\OfficeWorkTime", mappedBy="office")
     * @ORM\OrderBy({"dayOfWeek" = "ASC"})
     */
    private $workTimes;

    /**
     * @var Collection
     * @ORM\OrderBy({"active" = "DESC", "createdAt" = "DESC"})
     * @ORM\OneToMany(targetEntity="AH\Entity\OfficeBox", mappedBy="office")
     */
    protected $boxes;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false, unique=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true, unique=false)
     */
    private $address = null;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true, unique=false)
     */
    private $phone = null;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, unique=false)
     */
    private $active = true;

    /**
     * @var integer
     *
     * @ORM\Column(name="cash_back_percent", type="integer", nullable=false, unique=false)
     */
    private $cashBackPercent = 0;

    public function __construct()
    {
        $this->managers = new ArrayCollection();
        $this->boxes = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->workTimes = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Firm
     */
    public function getFirm()
    {
        return $this->firm;
    }

    /**
     * @param Firm $firm
     */
    public function setFirm($firm)
    {
        $this->firm = $firm;
    }

    /**
     * @return Collection
     */
    public function getManagers(): Collection
    {
        return $this->managers;
    }

    /**
     * @param Manager $manager
     */
    public function addManager(Manager $manager)
    {
        $this->managers->add($manager);
    }

    /**
     * @param Manager $manager
     */
    public function removeManager(Manager $manager)
    {
        $this->managers->removeElement($manager);
    }

    /**
     * @return Collection
     */
    public function getBoxes(): Collection
    {
        return $this->boxes;
    }

    /**
     * @param OfficeBox $officeBox
     */
    public function addBox(OfficeBox $officeBox)
    {
        $this->boxes->add($officeBox);
    }

    /**
     * @param OfficeBox $officeBox
     */
    public function removeBox(OfficeBox $officeBox)
    {
        $this->boxes->removeElement($officeBox);
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(?string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(?string $address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(?string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return Collection
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    /**
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        $this->orders->add($order);
    }

    /**
     * @param Order $order
     */
    public function removeOrder(Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * @return Collection
     */
    public function getWorkTimes(): Collection
    {
        return $this->workTimes;
    }

    /**
     * @param OfficeWorkTime $workTime
     */
    public function addWorkTime(OfficeWorkTime $workTime)
    {
        $this->workTimes->add($workTime);
    }

    /**
     * @param OfficeWorkTime $workTime
     */
    public function removeWorkTime(OfficeWorkTime $workTime)
    {
        $this->workTimes->removeElement($workTime);
    }

    /**
     * @return int
     */
    public function getCashBackPercent(): int
    {
        return $this->cashBackPercent;
    }

    /**
     * @param int $cashBackPercent
     */
    public function setCashBackPercent(int $cashBackPercent)
    {
        $this->cashBackPercent = $cashBackPercent;
    }
}
