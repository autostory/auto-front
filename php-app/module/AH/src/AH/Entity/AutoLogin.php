<?php

namespace AH\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="auto_login")
 * @ORM\Entity(repositoryClass="AH\Repository\AutoLoginRepository")
 */
class AutoLogin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=false, unique=true)
     */
    private $token;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="validBefore", type="datetimetz", nullable=false)
     */
    private $validBefore;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     */
    private $client;

    public function __construct()
    {
        // По умолчанию токен считается валидным 1 день
        $this->validBefore = new \DateTime();
        $this->validBefore->add(new \DateInterval('P1D'));
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return DateTime
     */
    public function getValidBefore(): DateTime
    {
        return $this->validBefore;
    }

    /**
     * @param DateTime $validBefore
     */
    public function setValidBefore(DateTime $validBefore)
    {
        $this->validBefore = $validBefore;
    }
}
