<?php

namespace AH\Entity;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use BjyAuthorize\Provider\Role\ProviderInterface as BjyAuthorizeRoleInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="identity", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="client_email_idx", columns={"registration_email"}, options={"where": "((user_type)::text = 'client'::text)"}),
 *     @ORM\UniqueConstraint(name="client_phone_idx", columns={"registration_phone"}, options={"where": "((user_type)::text = 'client'::text)"}),
 *     @ORM\UniqueConstraint(name="manager_email_idx", columns={"registration_email"}, options={"where": "((user_type)::text = 'manager'::text)"}),
 *     @ORM\UniqueConstraint(name="manager_phone_idx", columns={"registration_phone"}, options={"where": "((user_type)::text = 'manager'::text)"}),
 * })
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="user_type", type="string")
 * @ORM\DiscriminatorMap({"manager" = "Manager", "client" = "Client"})
 */
abstract class User implements BjyAuthorizeRoleInterface, BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="identity_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="AH\Entity\Role", inversedBy="users")
     * @ORM\JoinTable(name="user_role_linker")
     */
    protected $roles;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\UserCommunication", mappedBy="user")
     */
    protected $communications;

    /**
     * @var integer
     *
     * @ORM\Column(name="registration_phone", type="bigint", nullable=true)
     */
    private $registrationPhone = null;

    /**
     * @var string
     *
     * @ORM\Column(name="registration_email", type="string", length=255, nullable=true)
     */
    private $registrationEmail = null;

    /**
     * Фамилия
     *
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255,  nullable=true, unique=false)
     */
    private $surname;

    /**
     * Имя
     *
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=true, unique=false)
     */
    private $firstName;

    /**
     * Отчество
     *
     * @var string
     *
     * @ORM\Column(name="patronymic", type="string", length=255, nullable=true, unique=false)
     */
    private $patronymic;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false, unique=false)
     */
    private $password;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, unique=false)
     */
    private $active = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="inviteSent", type="boolean", nullable=false, unique=false)
     */
    private $inviteSent = false;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->communications = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getRegistrationEmail()
    {
        return $this->registrationEmail;
    }

    /**
     * @param string $registrationEmail
     */
    public function setRegistrationEmail($registrationEmail)
    {
        $this->registrationEmail = $registrationEmail;
    }

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get roles.
     *
     * @return Collection
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    /**
     * Add a role to the user.
     *
     * @param Role $role
     * @return void
     */
    public function addRole(Role $role)
    {
        $this->roles->add($role);
    }

    /**
     * Remove role from user.
     *
     * @param Role $role
     * @return void
     */
    public function removeRole(Role $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * Remove all roles from user.
     *
     * @return void
     */
    public function clearRoles()
    {
        $this->roles->clear();
    }

    /**
     * @return integer
     */
    public function getRegistrationPhone()
    {
        return $this->registrationPhone;
    }

    /**
     * @param integer $registrationPhone
     */
    public function setRegistrationPhone($registrationPhone)
    {
        $this->registrationPhone = $registrationPhone;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * @param string $patronymic
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;
    }

    public function getFullName()
    {
        $fullNameParts = [];
        if (!empty($this->getSurname()) && trim($this->getSurname()) !== '') {
            $fullNameParts[] = trim($this->getSurname());
        }

        if (!empty($this->getFirstName()) && trim($this->getFirstName()) !== '') {
            $fullNameParts[] = trim($this->getFirstName());
        }

        if (!empty($this->getPatronymic()) && trim($this->getPatronymic()) !== '') {
            $fullNameParts[] = trim($this->getPatronymic());
        }

        return implode($fullNameParts, ' ');
    }

    /**
     * @return Collection
     */
    public function getCommunications(): Collection
    {
        return $this->communications;
    }

    public function addCommunication(UserCommunication $communication)
    {
        $this->getCommunications()->add($communication);
    }

    public function removeCommunication(UserCommunication $communication)
    {
        $this->getCommunications()->remove($communication);
    }

    public function getPreferredCommunications()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('preferred', true))
            ->andWhere(Criteria::expr()->neq('value', ''));

        return $this->getCommunications()->matching($criteria);
    }

    /**
     * @return bool
     */
    public function getInviteSent(): bool
    {
        return $this->inviteSent;
    }

    /**
     * @param bool $inviteSent
     */
    public function setInviteSent(bool $inviteSent)
    {
        $this->inviteSent = $inviteSent;
    }
}
