<?php

namespace AH\Entity;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Лог начислений баллов клиенту
 *
 * @ORM\Table(name="`client_point_accrual_log`")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity
 */
class ClientPointAccrualLog implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     */
    private $client;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Order")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private $order;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $amount;

    /**
     * Когда были начислены баллы
     *
     * @var DateTime
     *
     * @ORM\Column(name="accrual_date", type="datetimetz", nullable=false)
     */
    private $accrualDate;

    public function __construct()
    {
        $this->accrualDate= new DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return DateTime
     */
    public function getAccrualDate(): DateTime
    {
        return $this->accrualDate;
    }

    /**
     * @param DateTime $accrualDate
     */
    public function setAccrualDate(DateTime $accrualDate)
    {
        $this->accrualDate = $accrualDate;
    }
}
