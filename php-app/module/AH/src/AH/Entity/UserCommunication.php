<?php

namespace AH\Entity;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user_communication")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AH\Repository\UserCommunicationRepository")
 */
class UserCommunication implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="user_communication_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="preferred", type="boolean")
     */
    private $preferred = false;

    /**
     * @ORM\ManyToOne(targetEntity="AH\Entity\User", inversedBy="communications")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false, unique=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=false, unique=false)
     */
    private $value;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, unique=false)
     */
    private $active = false;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function getPreferred(): bool
    {
        return $this->preferred;
    }

    /**
     * @param bool $preferred
     */
    public function setPreferred(bool $preferred)
    {
        $this->preferred = $preferred;
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}
