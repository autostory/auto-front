<?php

namespace AH\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="account_activate")
 * @ORM\Entity(repositoryClass="AH\Repository\PasswordRestoreRepository")
 */
class AccountActivate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="account_activate_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=false, unique=true)
     */
    private $token;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activated", type="boolean")
     */
    private $activated = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="activatedOn", type="datetimetz", nullable=true)
     */
    private $activatedOn = null;

    /**
     * @ORM\ManyToOne(targetEntity="AH\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return bool
     */
    public function getActivated(): bool
    {
        return $this->activated;
    }

    /**
     * @param bool $activated
     */
    public function setActivated(bool $activated)
    {
        $this->activated = $activated;
    }

    /**
     * @return \DateTime
     */
    public function getActivatedOn(): \DateTime
    {
        return $this->activatedOn;
    }

    /**
     * @param \DateTime $activatedOn
     */
    public function setActivatedOn(\DateTime $activatedOn)
    {
        $this->activatedOn = $activatedOn;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}
