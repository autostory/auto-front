<?php

namespace AH\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Manager extends User
{
    /**
     * @var Firm
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Firm", inversedBy="managers")
     * @ORM\JoinColumn(name="firm_id", referencedColumnName="id", nullable=true)
     */
    private $firm;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="AH\Entity\Office", mappedBy="managers")
     */
    private $offices;

    public function __construct() {
        parent::__construct();

        $this->offices = new ArrayCollection();
    }

    /**
     * Get userType
     *
     * @return string
     */
    public function getUserType()
    {
        return 'manager';
    }

    /**
     * @return Firm
     */
    public function getFirm()
    {
        return $this->firm;
    }

    /**
     * @param Firm $firm
     */
    public function setFirm(?Firm $firm)
    {
        $this->firm = $firm;
    }

    /**
     * @return Collection
     */
    public function getOffices(): Collection
    {
        return $this->offices;
    }

    /**
     * @param Office $office
     */
    public function addOffice(Office $office)
    {
        $this->offices->add($office);
    }

    /**
     * @param Office $office
     */
    public function removeOffice(Office $office)
    {
        $this->offices->removeElement($office);
    }
}
