<?php

namespace AH\Entity;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use \DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="request_communication")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AH\Repository\RequestCommunicationRepository")
 */
class RequestCommunication implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="request_communication_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="result_id", type="integer", nullable=false, unique=false)
     */
    private $resultId;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="reject_reason_id", type="integer", nullable=true, unique=false)
     */
    private $rejectReasonId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="reject_reason_description", type="string", length=255, nullable=true, unique=false)
     */
    private $rejectReasonDescription;

    /**
     * @var Request
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Request", inversedBy="requestCommunications")
     * @ORM\JoinColumn(name="request_id", referencedColumnName="id", nullable=false)
     */
    private $request;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="communication_date_time", type="datetimetz", nullable=true)
     */
    private $communicationDateTime = null;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="next_communication_date_time", type="datetimetz", nullable=true)
     */
    private $nextCommunicationDateTime = null;

    /**
     * @var Manager
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Manager")
     * @ORM\JoinColumn(name="manager_id", referencedColumnName="id", nullable=false)
     */
    private $manager;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getResultId(): int
    {
        return $this->resultId;
    }

    /**
     * @param int $resultId
     */
    public function setResultId(int $resultId): void
    {
        $this->resultId = $resultId;
    }

    /**
     * @return Manager
     */
    public function getManager(): Manager
    {
        return $this->manager;
    }

    /**
     * @param Manager $manager
     */
    public function setManager(Manager $manager): void
    {
        $this->manager = $manager;
    }

    /**
     * @return int|null
     */
    public function getRejectReasonId(): ?int
    {
        return $this->rejectReasonId;
    }

    /**
     * @param int|null $rejectReasonId
     */
    public function setRejectReasonId(?int $rejectReasonId): void
    {
        $this->rejectReasonId = $rejectReasonId;
    }

    /**
     * @return DateTime|null
     */
    public function getNextCommunicationDateTime(): ?DateTime
    {
        return $this->nextCommunicationDateTime;
    }

    /**
     * @param DateTime|null $nextCommunicationDateTime
     */
    public function setNextCommunicationDateTime(?DateTime $nextCommunicationDateTime): void
    {
        $this->nextCommunicationDateTime = $nextCommunicationDateTime;
    }

    /**
     * @return DateTime
     */
    public function getCommunicationDateTime(): DateTime
    {
        return $this->communicationDateTime;
    }

    /**
     * @param DateTime $communicationDateTime
     */
    public function setCommunicationDateTime(DateTime $communicationDateTime): void
    {
        $this->communicationDateTime = $communicationDateTime;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request): void
    {
        $this->request = $request;
    }

    /**
     * @return null|string
     */
    public function getRejectReasonDescription(): ?string
    {
        return $this->rejectReasonDescription;
    }

    /**
     * @param null|string $rejectReasonDescription
     */
    public function setRejectReasonDescription(?string $rejectReasonDescription): void
    {
        $this->rejectReasonDescription = $rejectReasonDescription;
    }

}
