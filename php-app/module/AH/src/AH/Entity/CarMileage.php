<?php

namespace AH\Entity;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="car_mileage")
 */
class CarMileage implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="car_millage_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var Car
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Car", inversedBy="mileages")
     * @ORM\JoinColumn(name="car_id", referencedColumnName="id", nullable=false)
     */
    private $car;

    /**
     * @var Order|null
     *
     * @ORM\OneToOne(targetEntity="AH\Entity\Order", inversedBy="mileage")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=true)
     */
    private $order;

    /**
     * @var integer
     *
     * @ORM\Column(name="value", type="decimal", nullable=false)
     */
    private $value;

    /**
     * Когда был измерен пробег
     *
     * @var DateTime
     *
     * @ORM\Column(name="measured_at", type="datetimetz", nullable=false)
     */
    private $measuredAt;

    public function __construct()
    {
        $this->measuredAt = new DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Car
     */
    public function getCar(): Car
    {
        return $this->car;
    }

    /**
     * @param Car $car
     */
    public function setCar(Car $car): void
    {
        $this->car = $car;
    }

    /**
     * @return Order|null
     */
    public function getOrder(): ?Order
    {
        return $this->order;
    }

    /**
     * @param Order|null $order
     */
    public function setOrder(?Order $order): void
    {
        $this->order = $order;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue(int $value): void
    {
        $this->value = $value;
    }

    /**
     * @return DateTime
     */
    public function getMeasuredAt(): DateTime
    {
        return $this->measuredAt;
    }

    /**
     * @param DateTime $measuredAt
     */
    public function setMeasuredAt(DateTime $measuredAt): void
    {
        $this->measuredAt = $measuredAt;
    }
}
