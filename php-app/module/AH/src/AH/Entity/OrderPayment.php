<?php

namespace AH\Entity;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="`order_payment`")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AH\Repository\OrderPaymentRepository")
 */
class OrderPayment implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="order_payment_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var Firm
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Firm", inversedBy="orders")
     * @ORM\JoinColumn(name="firm_id", referencedColumnName="id", nullable=false)
     */
    private $firm;

    /**
     * @var Manager
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Manager")
     * @ORM\JoinColumn(name="cashier_id", referencedColumnName="id", nullable=false)
     */
    private $cashier;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Order", inversedBy="payments")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private $order;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Client", inversedBy="payments")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     */
    private $client;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_type", type="integer", nullable=false)
     */
    private $paymentType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="note", type="string", length=255, nullable=true, unique=false)
     */
    private $note = null;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $amount;

    /**
     * Когда был сделан платеж
     *
     * @var DateTime
     *
     * @ORM\Column(name="make_at", type="datetimetz", nullable=false)
     */
    private $makeAt;

    public function __construct()
    {
        $this->makeAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Firm
     */
    public function getFirm(): Firm
    {
        return $this->firm;
    }

    /**
     * @param Firm $firm
     */
    public function setFirm(Firm $firm): void
    {
        $this->firm = $firm;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }

    /**
     * @return int
     */
    public function getPaymentType(): int
    {
        return $this->paymentType;
    }

    /**
     * @param int $paymentType
     */
    public function setPaymentType(int $paymentType): void
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return Manager
     */
    public function getCashier(): Manager
    {
        return $this->cashier;
    }

    /**
     * @param Manager $cashier
     */
    public function setCashier(Manager $cashier): void
    {
        $this->cashier = $cashier;
    }

    /**
     * @return DateTime
     */
    public function getMakeAt(): DateTime
    {
        return $this->makeAt;
    }

    /**
     * @param DateTime $makeAt
     */
    public function setMakeAt(DateTime $makeAt): void
    {
        $this->makeAt = $makeAt;
    }

    /**
     * @return null|string
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param null|string $note
     */
    public function setNote(?string $note): void
    {
        $this->note = $note;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient(Client $client): void
    {
        $this->client = $client;
    }
}
