<?php

namespace AH\Entity\Service;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use AH\Entity\Basic\UnitMeasureType;
use AH\Entity\Order;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="service_material")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AH\Repository\ServiceMaterialRepository")
 */
class ServiceMaterial implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="service_material_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Order", inversedBy="serviceMaterials")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private $order;

    /**
     * Тип единицы измерения
     *
     * @var UnitMeasureType
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Basic\UnitMeasureType")
     * @ORM\JoinColumn(name="unit_measure_type_id", referencedColumnName="id", nullable=false)
     */
    private $unitMeasureType;

    /**
     * Стоимость одного юнита
     *
     * @var float
     *
     * @ORM\Column(name="cost_per_unit", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $costPerUnit = 0;

    /**
     * Количество запчастей
     *
     * @var float
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $amount = 1;

    /**
     * Скидка в процентах
     *
     * @var float
     *
     * @ORM\Column(name="discount_percent", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $discountPercent = 0;

    /**
     * Стоимость запчасти
     *
     * @var float
     *
     * @ORM\Column(name="total_cost", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $totalCost = 0;

    /**
     * Название детали
     *
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return UnitMeasureType
     */
    public function getUnitMeasureType(): UnitMeasureType
    {
        return $this->unitMeasureType;
    }

    /**
     * @param UnitMeasureType $unitMeasureType
     */
    public function setUnitMeasureType(UnitMeasureType $unitMeasureType): void
    {
        $this->unitMeasureType = $unitMeasureType;
    }

    /**
     * @return float
     */
    public function getCostPerUnit(): float
    {
        return $this->costPerUnit;
    }

    /**
     * @param float $costPerUnit
     */
    public function setCostPerUnit(float $costPerUnit): void
    {
        $this->costPerUnit = $costPerUnit;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getDiscountPercent(): float
    {
        return $this->discountPercent;
    }

    /**
     * @param float $discountPercent
     */
    public function setDiscountPercent(float $discountPercent): void
    {
        $this->discountPercent = $discountPercent;
    }

    /**
     * @return float
     */
    public function getTotalCost(): float
    {
        return $this->totalCost;
    }

    /**
     * @param float $totalCost
     */
    public function setTotalCost(float $totalCost): void
    {
        $this->totalCost = $totalCost;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }
}
