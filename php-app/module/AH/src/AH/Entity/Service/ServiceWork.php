<?php

namespace AH\Entity\Service;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use AH\Entity\Basic\SparePartNode;
use AH\Entity\BoxSchedule;
use AH\Entity\Manager;
use AH\Entity\Order;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="service_work")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AH\Repository\ServiceWorkRepository")
 */
class ServiceWork implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="service_work_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Order", inversedBy="serviceWorks")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private $order;

    /**
     * @var Manager|null
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Manager")
     * @ORM\JoinColumn(name="master_id", referencedColumnName="id", nullable=true)
     */
    private $master;

    /**
     * Действие выполненное в ходе сервиса.
     *
     * @var ServiceWorkActionType
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Service\ServiceWorkActionType")
     * @ORM\JoinColumn(name="service_work_type_id", referencedColumnName="id", nullable=false)
     */
    private $serviceWorkActionType;

    /**
     * Запчасть затронутая в сервисе
     *
     * @var SparePartNode
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Basic\SparePartNode")
     * @ORM\JoinColumn(name="spare_part_node_id", referencedColumnName="id", nullable=false)
     */
    private $sparePartNode;

    /**
     * Стоимость одной детали
     *
     * @var float
     *
     * @ORM\Column(name="cost_per_unit", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $costPerUnit = 0;

    /**
     * Норма час
     *
     * @var float
     *
     * @ORM\Column(name="labor_hour", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $laborHour = 0;

    /**
     * Количество запчастей
     *
     * @var float
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $amount = 0;

    /**
     * Стоимость запчасти
     *
     * @var float
     *
     * @ORM\Column(name="total_cost", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $totalCost = 0;

    /**
     * Заметка к позиции
     *
     * @var string|null
     *
     * @ORM\Column(name="note", type="string", nullable=true)
     */
    private $note = null;

    /**
     * Скидка в процентах
     *
     * @var float
     *
     * @ORM\Column(name="discount_percent", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $discountPercent = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="AH\Entity\BoxSchedule", mappedBy="serviceWorks")
     */
    private $schedules;

    public function __construct()
    {
        $this->schedules = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return ServiceWorkActionType
     */
    public function getServiceWorkActionType(): ServiceWorkActionType
    {
        return $this->serviceWorkActionType;
    }

    /**
     * @param ServiceWorkActionType $serviceWorkActionType
     */
    public function setServiceWorkActionType(ServiceWorkActionType $serviceWorkActionType): void
    {
        $this->serviceWorkActionType = $serviceWorkActionType;
    }

    /**
     * @return float|null
     */
    public function getCostPerUnit(): ?float
    {
        return $this->costPerUnit;
    }

    /**
     * @param float|null $costPerUnit
     */
    public function setCostPerUnit(?float $costPerUnit): void
    {
        $this->costPerUnit = $costPerUnit;
    }

    /**
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @param float|null $amount
     */
    public function setAmount(?float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return float|null
     */
    public function getTotalCost(): ?float
    {
        return $this->totalCost;
    }

    /**
     * @param float|null $totalCost
     */
    public function setTotalCost(?float $totalCost): void
    {
        $this->totalCost = $totalCost;
    }

    /**
     * @return null|string
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param null|string $note
     */
    public function setNote(?string $note): void
    {
        $this->note = $note;
    }

    /**
     * @return SparePartNode
     */
    public function getSparePartNode(): SparePartNode
    {
        return $this->sparePartNode;
    }

    /**
     * @param SparePartNode $sparePartNode
     */
    public function setSparePartNode(SparePartNode $sparePartNode): void
    {
        $this->sparePartNode = $sparePartNode;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }

    /**
     * @return float
     */
    public function getDiscountPercent(): float
    {
        return $this->discountPercent;
    }

    /**
     * @param float $discountPercent
     */
    public function setDiscountPercent(float $discountPercent)
    {
        $this->discountPercent = $discountPercent;
    }

    /**
     * @return Manager|null
     */
    public function getMaster(): ?Manager
    {
        return $this->master;
    }

    /**
     * @param Manager|null $master
     */
    public function setMaster(?Manager $master): void
    {
        $this->master = $master;
    }

    /**
     * @return float
     */
    public function getLaborHour(): float
    {
        return $this->laborHour;
    }

    /**
     * @param float $laborHour
     */
    public function setLaborHour(float $laborHour): void
    {
        $this->laborHour = $laborHour;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return Collection
     */
    public function getSchedules(): Collection
    {
        return $this->schedules;
    }

    /**
     * @param BoxSchedule $schedule
     * @return void
     */
    public function addSchedule(BoxSchedule $schedule): void
    {
        $this->schedules->add($schedule);
    }

    /**
     * @param BoxSchedule $schedule
     * @return void
     */
    public function removeSchedule(BoxSchedule $schedule): void
    {
        $this->schedules->removeElement($schedule);
    }
}
