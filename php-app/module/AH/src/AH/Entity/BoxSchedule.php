<?php

namespace AH\Entity;

use AH\Entity\Service\ServiceWork;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="box_schedule")
 * @ORM\HasLifecycleCallbacks
 */
class BoxSchedule implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="box_schedule_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var OfficeBox
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\OfficeBox", inversedBy="schedules")
     * @ORM\JoinColumn(name="office_box_id", referencedColumnName="id", nullable=false)
     */
    private $box;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Order", inversedBy="schedules")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private $order;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date_time", type="datetimetz", nullable=false)
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish_date_time", type="datetimetz", nullable=false)
     */
    private $finish;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="AH\Entity\Service\ServiceWork", inversedBy="schedules")
     * @ORM\JoinTable(name="service_work_schedule_linker")
     */
    private $serviceWorks;

    public function __construct()
    {
        $this->serviceWorks = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return OfficeBox
     */
    public function getBox(): OfficeBox
    {
        return $this->box;
    }

    /**
     * @param OfficeBox $box
     */
    public function setBox(OfficeBox $box): void
    {
        $this->box = $box;
    }

    /**
     * @return \DateTime
     */
    public function getStart(): \DateTime
    {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     */
    public function setStart(\DateTime $start): void
    {
        $this->start = $start;
    }

    /**
     * @return \DateTime
     */
    public function getFinish(): \DateTime
    {
        return $this->finish;
    }

    /**
     * @param \DateTime $finish
     */
    public function setFinish(\DateTime $finish): void
    {
        $this->finish = $finish;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return Collection
     */
    public function getServiceWorks(): Collection
    {
        return $this->serviceWorks;
    }

    /**
     * @param ServiceWork $serviceWork
     */
    public function addServiceWork(ServiceWork $serviceWork): void
    {
        $this->serviceWorks->add($serviceWork);
    }

    /**
     * @param ServiceWork $serviceWork
     */
    public function removeServiceWork(ServiceWork $serviceWork): void
    {
        $this->serviceWorks->removeElement($serviceWork);
    }

    public function clearServiceWorks(): void
    {
        $this->serviceWorks->clear();
    }
}
