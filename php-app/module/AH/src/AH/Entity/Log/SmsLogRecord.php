<?php

namespace AH\Entity\Log;

use AH\Core\SMS\SMSService;
use AH\Entity\User;
use DateTime;
use InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="sms_log_record")
 * @ORM\Entity
 */
class SmsLogRecord
{
    const CLIENT_REGISTRATION_TYPE = 1;
    const MANAGER_REGISTRATION_TYPE = 10;
    const PASSWORD_RESTORE_TOKEN_TYPE = 2;
    const PASSWORD_RESTORE_LOGIN_PASSWORD_TYPE = 3;
    const CLIENT_NOTIFY_ORDER_CREATED = 50;
    const CLIENT_NOTIFY_ORDER_WORK_COMPLETED = 60;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sms_id", type="string", nullable=false)
     */
    private $smsId;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", nullable=false)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="sms_provider", type="string", nullable=false)
     */
    private $smsProvider;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status = SMSService::SMS_WAIT_SENDING;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", nullable=false)
     */
    private $phone;

    /**
     * Инициатор отправки
     *
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\User")
     * @ORM\JoinColumn(name="initiator_id", referencedColumnName="id", nullable=true)
     */
    private $initiator;

    /**
     * Адресат сообщения
     *
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\User")
     * @ORM\JoinColumn(name="addressee_id", referencedColumnName="id", nullable=true)
     */
    private $addressee;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="send_date", type="datetime", nullable=false)
     */
    private $sendDate;

    /**
     * @var float
     *
     * @ORM\Column(name="cost", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $cost;

    public function __construct()
    {
        $this->sendDate = new DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSmsId(): string
    {
        return $this->smsId;
    }

    /**
     * @param string $smsId
     */
    public function setSmsId(string $smsId)
    {
        $this->smsId = $smsId;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return User|null
     */
    public function getInitiator(): User
    {
        return $this->initiator;
    }

    /**
     * @param User|null $initiator
     */
    public function setInitiator(User $initiator)
    {
        $this->initiator = $initiator;
    }

    /**
     * @return User|null
     */
    public function getAddressee(): User
    {
        return $this->addressee;
    }

    /**
     * @param User|null $addressee
     */
    public function setAddressee(User $addressee)
    {
        $this->addressee = $addressee;
    }

    /**
     * @return float
     */
    public function getCost(): float
    {
        return $this->cost;
    }

    /**
     * @param float $cost
     */
    public function setCost(float $cost)
    {
        $this->cost = $cost;
    }

    /**
     * @return string
     */
    public function getSmsProvider(): string
    {
        return $this->smsProvider;
    }

    /**
     * @param string $smsProvider
     */
    public function setSmsProvider(string $smsProvider)
    {
        $this->smsProvider = $smsProvider;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type)
    {
        if (!in_array(
            $type,
            [
                self::CLIENT_REGISTRATION_TYPE,
                self::MANAGER_REGISTRATION_TYPE,
                self::PASSWORD_RESTORE_TOKEN_TYPE,
                self::PASSWORD_RESTORE_LOGIN_PASSWORD_TYPE,
                self::CLIENT_NOTIFY_ORDER_CREATED,
                self::CLIENT_NOTIFY_ORDER_WORK_COMPLETED
            ]
        )) {
            throw new InvalidArgumentException('Invalid sms type provided');
        }

        $this->type = $type;
    }
}
