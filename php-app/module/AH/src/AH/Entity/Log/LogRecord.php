<?php

namespace AH\Entity\Log;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="t_history")
 * @ORM\Entity
 */
class LogRecord
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="t_history_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="schemaname", type="text", nullable=true)
     */
    private $schemaname = null;

    /**
     * @var string
     *
     * @ORM\Column(name="tabname", type="text", nullable=true)
     */
    private $tabname = null;

    /**
     * @var string
     *
     * @ORM\Column(name="operation", type="text", nullable=true)
     */
    private $operation = null;

    /**
     * @var string
     *
     * @ORM\Column(name="new_val", type="json_array", nullable=true)
     */
    private $newVal = null;

    /**
     * @var string
     *
     * @ORM\Column(name="old_val", type="json_array", nullable=true)
     */
    private $oldVal = null;

    /**
     * @var string
     *
     * @ORM\Column(name="who", type="text", nullable=true, options={"default" : "app_user_id()"})
     */
    private $who = null;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity_id", type="integer", nullable=true)
     */
    private $entityId = null;

    /**
     * @var boolean
     *
     * @ORM\Column(name="differ", type="boolean", nullable=true)
     */
    private $differ = null;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tstamp", type="datetime", nullable=true, options={"default" : "now()"})
     */
    private $tstamp = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getTstamp(): \DateTime
    {
        return $this->tstamp;
    }

    /**
     * @param \DateTime $tstamp
     */
    public function setTstamp(\DateTime $tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * @return bool
     */
    public function isDiffer(): bool
    {
        return $this->differ;
    }

    /**
     * @param bool $differ
     */
    public function setDiffer(bool $differ)
    {
        $this->differ = $differ;
    }

    /**
     * @return int
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }

    /**
     * @param int $entityId
     */
    public function setEntityId(int $entityId)
    {
        $this->entityId = $entityId;
    }

    /**
     * @return string
     */
    public function getWho(): string
    {
        return $this->who;
    }

    /**
     * @param string $who
     */
    public function setWho(string $who)
    {
        $this->who = $who;
    }

    /**
     * @return string
     */
    public function getNewVal(): string
    {
        return $this->newVal;
    }

    /**
     * @param string $newVal
     */
    public function setNewVal(string $newVal)
    {
        $this->newVal = $newVal;
    }

    /**
     * @return string
     */
    public function getOldVal(): string
    {
        return $this->oldVal;
    }

    /**
     * @param string $oldVal
     */
    public function setOldVal(string $oldVal)
    {
        $this->oldVal = $oldVal;
    }

    /**
     * @return string
     */
    public function getOperation(): string
    {
        return $this->operation;
    }

    /**
     * @param string $operation
     */
    public function setOperation(string $operation)
    {
        $this->operation = $operation;
    }

    /**
     * @return string
     */
    public function getTabname(): string
    {
        return $this->tabname;
    }

    /**
     * @param string $tabname
     */
    public function setTabname(string $tabname)
    {
        $this->tabname = $tabname;
    }

    /**
     * @return string
     */
    public function getSchemaname(): string
    {
        return $this->schemaname;
    }

    /**
     * @param string $schemaname
     */
    public function setSchemaname(string $schemaname)
    {
        $this->schemaname = $schemaname;
    }
}
