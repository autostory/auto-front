<?php

namespace AH\Entity;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use \DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="request")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AH\Repository\RequestRepository")
 */
class Request implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="request_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var Client|null
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Client", inversedBy="requests")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=true)
     */
    private $client;

    /**
     * @var Order|null
     *
     * @ORM\OneToOne(targetEntity="AH\Entity\Order", inversedBy="request")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @var Car|null
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Car", inversedBy="orders")
     * @ORM\JoinColumn(name="car_id", referencedColumnName="id", nullable=true)
     */
    private $car;

    /**
     * Когда был сделан запрос
     *
     * @var DateTime|null
     *
     * @ORM\Column(name="make_at", type="datetimetz", nullable=true)
     */
    private $makeAt = null;

    /**
     * @var Firm
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Firm", inversedBy="requests")
     * @ORM\JoinColumn(name="firm_id", referencedColumnName="id", nullable=false)
     */
    private $firm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="userMessage", type="string", nullable=false, unique=false)
     */
    private $userMessage = null;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\RequestCommunication", mappedBy="request")
     **/
    private $requestCommunications;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, unique=false)
     */
    private $active = true;

    public function __construct()
    {
        $this->requestCommunications = new ArrayCollection();
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Client|null
     */
    public function getClient(): ?Client
    {
        return $this->client;
    }

    /**
     * @param Client|null $client
     */
    public function setClient(?Client $client): void
    {
        $this->client = $client;
    }

    /**
     * @return Car|null
     */
    public function getCar(): ?Car
    {
        return $this->car;
    }

    /**
     * @param Car|null $car
     */
    public function setCar(?Car $car): void
    {
        $this->car = $car;
    }

    /**
     * @return null|string
     */
    public function getUserMessage(): ?string
    {
        return $this->userMessage;
    }

    /**
     * @param null|string $userMessage
     */
    public function setUserMessage(?string $userMessage): void
    {
        $this->userMessage = $userMessage;
    }

    /**
     * @return DateTime|null
     */
    public function getMakeAt(): ?DateTime
    {
        return $this->makeAt;
    }

    /**
     * @param DateTime|null $makeAt
     */
    public function setMakeAt(?DateTime $makeAt): void
    {
        $this->makeAt = $makeAt;
    }

    /**
     * @return Firm
     */
    public function getFirm(): Firm
    {
        return $this->firm;
    }

    /**
     * @param Firm $firm
     */
    public function setFirm(Firm $firm): void
    {
        $this->firm = $firm;
    }

    /**
     * @return Collection
     */
    public function getRequestCommunications(): Collection
    {
        return $this->requestCommunications;
    }

    /**
     * @param RequestCommunication $requestCommunication
     */
    public function addRequestCommunication(RequestCommunication $requestCommunication)
    {
        $this->requestCommunications->add($requestCommunication);
    }

    /**
     * @param RequestCommunication $requestCommunication
     */
    public function removeRequestCommunication(RequestCommunication $requestCommunication)
    {
        $this->requestCommunications->removeElement($requestCommunication);
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return Order|null
     */
    public function getOrder(): ?Order
    {
        return $this->order;
    }

    /**
     * @param Order|null $order
     */
    public function setOrder(?Order $order): void
    {
        $this->order = $order;
    }
}
