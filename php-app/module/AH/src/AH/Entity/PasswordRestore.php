<?php

namespace AH\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="password_restore")
 * @ORM\Entity(repositoryClass="AH\Repository\PasswordRestoreRepository")
 */
class PasswordRestore
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="password_restore_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=false, unique=true)
     */
    private $token;

    /**
     * @var string|null
     *
     * @ORM\Column(name="smsCode", type="string", length=255, nullable=true, unique=false)
     */
    private $smsCode = null;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="validBefore", type="datetimetz", nullable=false)
     */
    private $validBefore;

    /**
     * @ORM\ManyToOne(targetEntity="AH\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="AH\Entity\Manager")
     * @ORM\JoinColumn(name="manager_id", referencedColumnName="id")
     */
    private $manager;

    public function __construct()
    {
        $this->validBefore = new \DateTime();
        $this->validBefore->add(new \DateInterval('PT30M'));
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param mixed $manager
     */
    public function setManager($manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return \DateTime
     */
    public function getValidBefore(): \DateTime
    {
        return $this->validBefore;
    }

    /**
     * @param \DateTime $validBefore
     */
    public function setValidBefore(\DateTime $validBefore)
    {
        $this->validBefore = $validBefore;
    }

    /**
     * @return null|string
     */
    public function getSmsCode(): ?string
    {
        return $this->smsCode;
    }

    /**
     * @param null|string $smsCode
     */
    public function setSmsCode(?string $smsCode): void
    {
        $this->smsCode = $smsCode;
    }
}
