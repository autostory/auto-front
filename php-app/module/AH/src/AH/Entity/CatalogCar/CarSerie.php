<?php

namespace AH\Entity\CatalogCar;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarSerie
 *
 * @ORM\Table(name="car_serie", indexes={@ORM\Index(name="car_serie_id_car_model_idx", columns={"id_car_model"}), @ORM\Index(name="car_serie_id_car_generation_idx", columns={"id_car_generation"}), @ORM\Index(name="car_serie_id_car_type_idx", columns={"id_car_type"})})
 * @ORM\Entity
 */
class CarSerie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_car_serie", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="car_serie_id_car_serie_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=510, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_create", type="integer", nullable=true)
     */
    private $dateCreate;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_update", type="integer", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \AH\Entity\CatalogCar\CarGeneration
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarGeneration")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_generation", referencedColumnName="id_car_generation")
     * })
     */
    private $carGeneration;

    /**
     * @var \AH\Entity\CatalogCar\CarModel
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarModel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_model", referencedColumnName="id_car_model")
     * })
     */
    private $carModel;

    /**
     * @var \AH\Entity\CatalogCar\CarType
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_type", referencedColumnName="id_car_type")
     * })
     */
    private $carType;

    /**
     * Get idCarSerie
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CarSerie
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateCreate
     *
     * @param integer $dateCreate
     *
     * @return CarSerie
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return integer
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param integer $dateUpdate
     *
     * @return CarSerie
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return integer
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set idCarGeneration
     *
     * @param \AH\Entity\CatalogCar\CarGeneration $carGeneration
     *
     * @return CarSerie
     */
    public function setCarGeneration(CarGeneration $carGeneration = null)
    {
        $this->carGeneration = $carGeneration;

        return $this;
    }

    /**
     * Get idCarGeneration
     *
     * @return \AH\Entity\CatalogCar\CarGeneration
     */
    public function getCarGeneration()
    {
        return $this->carGeneration;
    }

    /**
     * Set idCarModel
     *
     * @param \AH\Entity\CatalogCar\CarModel $carModel
     *
     * @return CarSerie
     */
    public function setCarModel(CarModel $carModel = null)
    {
        $this->carModel = $carModel;

        return $this;
    }

    /**
     * Get idCarModel
     *
     * @return \AH\Entity\CatalogCar\CarModel
     */
    public function getCarModel()
    {
        return $this->carModel;
    }

    /**
     * Set idCarType
     *
     * @param \AH\Entity\CatalogCar\CarType $carType
     *
     * @return CarSerie
     */
    public function setCarType(CarType $carType = null)
    {
        $this->carType = $carType;

        return $this;
    }

    /**
     * Get idCarType
     *
     * @return \AH\Entity\CatalogCar\CarType
     */
    public function getCarType()
    {
        return $this->carType;
    }
}
