<?php

namespace AH\Entity\CatalogCar;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarModification
 *
 * @ORM\Table(name="car_modification", indexes={@ORM\Index(name="car_modification_id_car_serie_idx", columns={"id_car_serie"}), @ORM\Index(name="car_modification_id_car_model_idx", columns={"id_car_model"}), @ORM\Index(name="car_modification_id_car_type_idx", columns={"id_car_type"})})
 * @ORM\Entity
 */
class CarModification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_car_modification", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="car_modification_id_car_modification_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=510, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_create", type="integer", nullable=true)
     */
    private $dateCreate;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_update", type="integer", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="start_production_year", type="integer", nullable=true)
     */
    private $startProductionYear;

    /**
     * @var integer
     *
     * @ORM\Column(name="end_production_year", type="integer", nullable=true)
     */
    private $endProductionYear;

    /**
     * @var \AH\Entity\CatalogCar\CarModel
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarModel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_model", referencedColumnName="id_car_model")
     * })
     */
    private $carModel;

    /**
     * @var \AH\Entity\CatalogCar\CarSerie
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarSerie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_serie", referencedColumnName="id_car_serie")
     * })
     */
    private $carSerie;

    /**
     * @var \AH\Entity\CatalogCar\CarType
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_type", referencedColumnName="id_car_type")
     * })
     */
    private $carType;

    /**
     * Get idCarModification
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CarModification
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateCreate
     *
     * @param integer $dateCreate
     *
     * @return CarModification
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return integer
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param integer $dateUpdate
     *
     * @return CarModification
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return integer
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set idCarModel
     *
     * @param \AH\Entity\CatalogCar\CarModel $carModel
     *
     * @return CarModification
     */
    public function setCarModel(CarModel $carModel = null)
    {
        $this->carModel = $carModel;

        return $this;
    }

    /**
     * Get idCarModel
     *
     * @return \AH\Entity\CatalogCar\CarModel
     */
    public function getCarModel()
    {
        return $this->carModel;
    }

    /**
     * Set idCarSerie
     *
     * @param \AH\Entity\CatalogCar\CarSerie $carSerie
     *
     * @return CarModification
     */
    public function setCarSerie(CarSerie $carSerie = null)
    {
        $this->carSerie = $carSerie;

        return $this;
    }

    /**
     * Get idCarSerie
     *
     * @return \AH\Entity\CatalogCar\CarSerie
     */
    public function getCarSerie()
    {
        return $this->carSerie;
    }

    /**
     * Set idCarType
     *
     * @param \AH\Entity\CatalogCar\CarType $carType
     *
     * @return CarModification
     */
    public function setCarType(CarType $carType = null)
    {
        $this->carType = $carType;

        return $this;
    }

    /**
     * Get idCarType
     *
     * @return \AH\Entity\CatalogCar\CarType
     */
    public function getCarType()
    {
        return $this->carType;
    }

    /**
     * @return int
     */
    public function getStartProductionYear(): int
    {
        return $this->startProductionYear;
    }

    /**
     * @param int $startProductionYear
     */
    public function setStartProductionYear(int $startProductionYear)
    {
        $this->startProductionYear = $startProductionYear;
    }

    /**
     * @return int
     */
    public function getEndProductionYear(): int
    {
        return $this->endProductionYear;
    }

    /**
     * @param int $endProductionYear
     */
    public function setEndProductionYear(int $endProductionYear)
    {
        $this->endProductionYear = $endProductionYear;
    }
}
