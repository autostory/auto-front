<?php

namespace AH\Entity\CatalogCar;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarOptionValue
 *
 * @ORM\Table(name="car_option_value", uniqueConstraints={@ORM\UniqueConstraint(name="car_option_value_id_car_option_id_car_equipment_id_car_type_key", columns={"id_car_option", "id_car_equipment", "id_car_type"})}, indexes={@ORM\Index(name="car_option_value_id_car_type_idx", columns={"id_car_type"}), @ORM\Index(name="car_option_value_id_car_option_idx", columns={"id_car_option"}), @ORM\Index(name="car_option_value_id_car_equipment_idx", columns={"id_car_equipment"})})
 * @ORM\Entity
 */
class CarOptionValue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_car_option_value", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="car_option_value_id_car_option_value_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_base", type="boolean", nullable=false)
     */
    private $isBase;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_create", type="integer", nullable=false)
     */
    private $dateCreate;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_update", type="integer", nullable=false)
     */
    private $dateUpdate;

    /**
     * @var \AH\Entity\CatalogCar\CarEquipment
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarEquipment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_equipment", referencedColumnName="id_car_equipment")
     * })
     */
    private $carEquipment;

    /**
     * @var \AH\Entity\CatalogCar\CarOption
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarOption")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_option", referencedColumnName="id_car_option")
     * })
     */
    private $carOption;

    /**
     * @var \AH\Entity\CatalogCar\CarType
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_type", referencedColumnName="id_car_type")
     * })
     */
    private $carType;

    /**
     * Get idCarOptionValue
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isBase
     *
     * @param boolean $isBase
     *
     * @return CarOptionValue
     */
    public function setIsBase($isBase)
    {
        $this->isBase = $isBase;

        return $this;
    }

    /**
     * Get isBase
     *
     * @return boolean
     */
    public function getIsBase()
    {
        return $this->isBase;
    }

    /**
     * Set dateCreate
     *
     * @param integer $dateCreate
     *
     * @return CarOptionValue
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return integer
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param integer $dateUpdate
     *
     * @return CarOptionValue
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return integer
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set idCarEquipment
     *
     * @param \AH\Entity\CatalogCar\CarEquipment $carEquipment
     *
     * @return CarOptionValue
     */
    public function setCarEquipment(CarEquipment $carEquipment = null)
    {
        $this->carEquipment = $carEquipment;

        return $this;
    }

    /**
     * Get idCarEquipment
     *
     * @return \AH\Entity\CatalogCar\CarEquipment
     */
    public function getCarEquipment()
    {
        return $this->carEquipment;
    }

    /**
     * Set idCarOption
     *
     * @param \AH\Entity\CatalogCar\CarOption $carOption
     *
     * @return CarOptionValue
     */
    public function setCarOption(CarOption $carOption = null)
    {
        $this->carOption = $carOption;

        return $this;
    }

    /**
     * Get idCarOption
     *
     * @return \AH\Entity\CatalogCar\CarOption
     */
    public function getCarOption()
    {
        return $this->carOption;
    }

    /**
     * Set idCarType
     *
     * @param \AH\Entity\CatalogCar\CarType $carType
     *
     * @return CarOptionValue
     */
    public function setCarType(CarType $carType = null)
    {
        $this->carType = $carType;

        return $this;
    }

    /**
     * Get idCarType
     *
     * @return \AH\Entity\CatalogCar\CarType
     */
    public function getCarType()
    {
        return $this->carType;
    }
}
