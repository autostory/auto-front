<?php

namespace AH\Entity\CatalogCar;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarCharacteristic
 *
 * @ORM\Table(name="car_characteristic", indexes={@ORM\Index(name="car_characteristic_id_car_type_idx", columns={"id_car_type"})})
 * @ORM\Entity
 */
class CarCharacteristic
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_car_characteristic", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="car_characteristic_id_car_characteristic_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=510, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_parent", type="integer", nullable=true)
     */
    private $idParent;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_create", type="integer", nullable=true)
     */
    private $dateCreate;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_update", type="integer", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_delete", type="integer", nullable=true)
     */
    private $dateDelete;

    /**
     * @var \AH\Entity\CatalogCar\CarType
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_type", referencedColumnName="id_car_type")
     * })
     */
    private $carType;

    /**
     * Get idCarCharacteristic
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CarCharacteristic
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set idParent
     *
     * @param integer $idParent
     *
     * @return CarCharacteristic
     */
    public function setIdParent($idParent)
    {
        $this->idParent = $idParent;

        return $this;
    }

    /**
     * Get idParent
     *
     * @return integer
     */
    public function getIdParent()
    {
        return $this->idParent;
    }

    /**
     * Set dateCreate
     *
     * @param integer $dateCreate
     *
     * @return CarCharacteristic
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return integer
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param integer $dateUpdate
     *
     * @return CarCharacteristic
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return integer
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set idCarType
     *
     * @param \AH\Entity\CatalogCar\CarType $carType
     *
     * @return CarCharacteristic
     */
    public function setCarType(CarType $carType = null)
    {
        $this->carType = $carType;

        return $this;
    }

    /**
     * Get idCarType
     *
     * @return \AH\Entity\CatalogCar\CarType
     */
    public function getCarType()
    {
        return $this->carType;
    }

    /**
     * @return int
     */
    public function getDateDelete(): int
    {
        return $this->dateDelete;
    }

    /**
     * @param int $dateDelete
     */
    public function setDateDelete(int $dateDelete)
    {
        $this->dateDelete = $dateDelete;
    }
}
