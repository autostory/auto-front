<?php

namespace AH\Entity\CatalogCar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Вид транспорта
 *
 * @ORM\Table(name="car_type")
 * @ORM\Entity
 */
class CarType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_car_type", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="car_type_id_car_type_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=510, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_delete", type="integer", nullable=true)
     */
    private $dateDelete;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CarType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getDateDelete(): int
    {
        return $this->dateDelete;
    }

    /**
     * @param int $dateDelete
     */
    public function setDateDelete(int $dateDelete)
    {
        $this->dateDelete = $dateDelete;
    }
}
