<?php

namespace AH\Entity\CatalogCar;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CarEquipment
 *
 * @ORM\Table(name="car_equipment", indexes={@ORM\Index(name="car_equipment_id_car_modification_idx", columns={"id_car_modification"}), @ORM\Index(name="car_equipment_id_car_type_idx", columns={"id_car_type"})})
 * @ORM\Entity
 */
class CarEquipment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_car_equipment", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="car_equipment_id_car_equipment_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=510, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_create", type="integer", nullable=false)
     */
    private $dateCreate;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_update", type="integer", nullable=false)
     */
    private $dateUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="price_min", type="integer", nullable=true)
     */
    private $priceMin;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer", nullable=true)
     */
    private $year;

    /**
     * @var \AH\Entity\CatalogCar\CarModification
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarModification")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_modification", referencedColumnName="id_car_modification")
     * })
     */
    private $carModification;

    /**
     * @var \AH\Entity\CatalogCar\CarType
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_type", referencedColumnName="id_car_type")
     * })
     */
    private $carType;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CarEquipment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateCreate
     *
     * @param integer $dateCreate
     *
     * @return CarEquipment
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return integer
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param integer $dateUpdate
     *
     * @return CarEquipment
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return integer
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set priceMin
     *
     * @param integer $priceMin
     *
     * @return CarEquipment
     */
    public function setPriceMin($priceMin)
    {
        $this->priceMin = $priceMin;

        return $this;
    }

    /**
     * Get priceMin
     *
     * @return integer
     */
    public function getPriceMin()
    {
        return $this->priceMin;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return CarEquipment
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set idCarModification
     *
     * @param \AH\Entity\CatalogCar\CarModification $carModification
     *
     * @return CarEquipment
     */
    public function setCarModification(CarModification $carModification = null)
    {
        $this->carModification = $carModification;

        return $this;
    }

    /**
     * Get idCarModification
     *
     * @return \AH\Entity\CatalogCar\CarModification
     */
    public function getCarModification()
    {
        return $this->carModification;
    }

    /**
     * Set idCarType
     *
     * @param \AH\Entity\CatalogCar\CarType $carType
     *
     * @return CarEquipment
     */
    public function setCarType(CarType $carType = null)
    {
        $this->carType = $carType;

        return $this;
    }

    /**
     * Get idCarType
     *
     * @return \AH\Entity\CatalogCar\CarType
     */
    public function getCarType()
    {
        return $this->carType;
    }
}
