<?php

namespace AH\Entity\CatalogCar;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarModel
 *
 * @ORM\Table(name="car_model", indexes={@ORM\Index(name="car_model_id_car_type_idx", columns={"id_car_type"}), @ORM\Index(name="car_model_id_car_mark_idx", columns={"id_car_mark"})})
 * @ORM\Entity
 */
class CarModel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_car_model", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="car_model_id_car_model_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=510, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_create", type="integer", nullable=true)
     */
    private $dateCreate;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_update", type="integer", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var string
     *
     * @ORM\Column(name="name_rus", type="string", length=510, nullable=true)
     */
    private $nameRus;

    /**
     * @var \AH\Entity\CatalogCar\CarMark
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarMark")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_mark", referencedColumnName="id_car_mark")
     * })
     */
    private $carMark;

    /**
     * @var \AH\Entity\CatalogCar\CarType
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_type", referencedColumnName="id_car_type")
     * })
     */
    private $carType;

    /**
     * Get idCarModel
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CarModel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateCreate
     *
     * @param integer $dateCreate
     *
     * @return CarModel
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return integer
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param integer $dateUpdate
     *
     * @return CarModel
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return integer
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set nameRus
     *
     * @param string $nameRus
     *
     * @return CarModel
     */
    public function setNameRus($nameRus)
    {
        $this->nameRus = $nameRus;

        return $this;
    }

    /**
     * Get nameRus
     *
     * @return string
     */
    public function getNameRus()
    {
        return $this->nameRus;
    }

    /**
     * Set idCarMark
     *
     * @param \AH\Entity\CatalogCar\CarMark $carMark
     *
     * @return CarModel
     */
    public function setCarMark(\AH\Entity\CatalogCar\CarMark $carMark = null)
    {
        $this->carMark = $carMark;

        return $this;
    }

    /**
     * Get idCarMark
     *
     * @return \AH\Entity\CatalogCar\CarMark
     */
    public function getCarMark()
    {
        return $this->carMark;
    }

    /**
     * Set idCarType
     *
     * @param \AH\Entity\CatalogCar\CarType $carType
     *
     * @return CarModel
     */
    public function setCarType(CarType $carType = null)
    {
        $this->carType = $carType;

        return $this;
    }

    /**
     * Get idCarType
     *
     * @return \AH\Entity\CatalogCar\CarType
     */
    public function getCarType()
    {
        return $this->carType;
    }
}
