<?php

namespace AH\Entity\CatalogCar;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarGeneration
 *
 * @ORM\Table(name="car_generation", indexes={@ORM\Index(name="car_generation_id_car_type_idx", columns={"id_car_type"}), @ORM\Index(name="car_generation_id_car_model_idx", columns={"id_car_model"})})
 * @ORM\Entity
 */
class CarGeneration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_car_generation", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="car_generation_id_car_generation_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=510, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="year_begin", type="string", length=510, nullable=true)
     */
    private $yearBegin;

    /**
     * @var string
     *
     * @ORM\Column(name="year_end", type="string", length=510, nullable=true)
     */
    private $yearEnd;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_create", type="integer", nullable=false)
     */
    private $dateCreate;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_update", type="integer", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \AH\Entity\CatalogCar\CarModel
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarModel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_model", referencedColumnName="id_car_model")
     * })
     */
    private $carModel;

    /**
     * @var \AH\Entity\CatalogCar\CarType
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_type", referencedColumnName="id_car_type")
     * })
     */
    private $carType;

    /**
     * Get idCarGeneration
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CarGeneration
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set yearBegin
     *
     * @param string $yearBegin
     *
     * @return CarGeneration
     */
    public function setYearBegin($yearBegin)
    {
        $this->yearBegin = $yearBegin;

        return $this;
    }

    /**
     * Get yearBegin
     *
     * @return string
     */
    public function getYearBegin()
    {
        return $this->yearBegin;
    }

    /**
     * Set yearEnd
     *
     * @param string $yearEnd
     *
     * @return CarGeneration
     */
    public function setYearEnd($yearEnd)
    {
        $this->yearEnd = $yearEnd;

        return $this;
    }

    /**
     * Get yearEnd
     *
     * @return string
     */
    public function getYearEnd()
    {
        return $this->yearEnd;
    }

    /**
     * Set dateCreate
     *
     * @param integer $dateCreate
     *
     * @return CarGeneration
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return integer
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param integer $dateUpdate
     *
     * @return CarGeneration
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return integer
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set idCarModel
     *
     * @param \AH\Entity\CatalogCar\CarModel $carModel
     *
     * @return CarGeneration
     */
    public function setCarModel(CarModel $carModel = null)
    {
        $this->carModel = $carModel;

        return $this;
    }

    /**
     * Get idCarModel
     *
     * @return \AH\Entity\CatalogCar\CarModel
     */
    public function getCarModel()
    {
        return $this->carModel;
    }

    /**
     * Set idCarType
     *
     * @param \AH\Entity\CatalogCar\CarType $carType
     *
     * @return CarGeneration
     */
    public function setCarType(CarType $carType = null)
    {
        $this->carType = $carType;

        return $this;
    }

    /**
     * Get idCarType
     *
     * @return \AH\Entity\CatalogCar\CarType
     */
    public function getCarType()
    {
        return $this->carType;
    }
}
