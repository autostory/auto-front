<?php

namespace AH\Entity\CatalogCar;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarCharacteristicValue
 *
 * @ORM\Table(name="car_characteristic_value", uniqueConstraints={@ORM\UniqueConstraint(name="car_characteristic_value_id_car_characteristic_id_car_modi_key1", columns={"id_car_characteristic", "id_car_modification", "id_car_type"}), @ORM\UniqueConstraint(name="car_characteristic_value_id_car_characteristic_id_car_modif_key", columns={"id_car_characteristic", "id_car_modification"})}, indexes={@ORM\Index(name="car_characteristic_value_id_car_type_idx", columns={"id_car_type"}), @ORM\Index(name="car_characteristic_value_id_car_modification_idx", columns={"id_car_modification"}), @ORM\Index(name="car_characteristic_value_id_car_characteristic_idx", columns={"id_car_characteristic"})})
 * @ORM\Entity
 */
class CarCharacteristicValue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_car_characteristic_value", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="car_characteristic_value_id_car_characteristic_value_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=510, nullable=true)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="unit", type="string", length=510, nullable=true)
     */
    private $unit;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_create", type="integer", nullable=true)
     */
    private $dateCreate;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_update", type="integer", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \AH\Entity\CatalogCar\CarCharacteristic
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarCharacteristic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_characteristic", referencedColumnName="id_car_characteristic")
     * })
     */
    private $carCharacteristic;

    /**
     * @var \AH\Entity\CatalogCar\CarModification
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarModification")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_modification", referencedColumnName="id_car_modification")
     * })
     */
    private $carModification;

    /**
     * @var \AH\Entity\CatalogCar\CarType
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_car_type", referencedColumnName="id_car_type")
     * })
     */
    private $carType;

    /**
     * Get idCarCharacteristicValue
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return CarCharacteristicValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set unit
     *
     * @param string $unit
     *
     * @return CarCharacteristicValue
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set dateCreate
     *
     * @param integer $dateCreate
     *
     * @return CarCharacteristicValue
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return integer
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param integer $dateUpdate
     *
     * @return CarCharacteristicValue
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return integer
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set idCarCharacteristic
     *
     * @param \AH\Entity\CatalogCar\CarCharacteristic $carCharacteristic
     *
     * @return CarCharacteristicValue
     */
    public function setCarCharacteristic(CarCharacteristic $carCharacteristic = null)
    {
        $this->carCharacteristic = $carCharacteristic;

        return $this;
    }

    /**
     * Get idCarCharacteristic
     *
     * @return \AH\Entity\CatalogCar\CarCharacteristic
     */
    public function getCarCharacteristic()
    {
        return $this->carCharacteristic;
    }

    /**
     * Set idCarModification
     *
     * @param \AH\Entity\CatalogCar\CarModification $carModification
     *
     * @return CarCharacteristicValue
     */
    public function setCarModification(CarModification $carModification = null)
    {
        $this->carModification = $carModification;

        return $this;
    }

    /**
     * Get idCarModification
     *
     * @return \AH\Entity\CatalogCar\CarModification
     */
    public function getCarModification()
    {
        return $this->carModification;
    }

    /**
     * Set idCarType
     *
     * @param \AH\Entity\CatalogCar\CarType $carType
     *
     * @return CarCharacteristicValue
     */
    public function setCarType(CarType $carType = null)
    {
        $this->carType = $carType;

        return $this;
    }

    /**
     * Get idCarType
     *
     * @return \AH\Entity\CatalogCar\CarType
     */
    public function getCarType()
    {
        return $this->carType;
    }
}
