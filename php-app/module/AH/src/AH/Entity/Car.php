<?php

namespace AH\Entity;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="car")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AH\Repository\CarRepository")
 */
class Car implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="car_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="vin", type="string", length=255, nullable=true, unique=true)
     */
    private $vin;

    /**
     * @var string
     *
     * @ORM\Column(name="assume_vin", type="string", length=255, nullable=true, unique=false)
     */
    private $assumeVin;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="year", type="integer", nullable=true, unique=false)
     */
    private $year;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true, unique=false)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="registration_sign_number", type="string", length=255, nullable=true, unique=false)
     */
    private $registrationSignNumber;

    /**
     * @ORM\ManyToMany(targetEntity="AH\Entity\Client", mappedBy="cars")
     */
    private $owners;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\Order", mappedBy="car")
     **/
    private $orders;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\CarMileage", mappedBy="car")
     **/
    private $mileages;

    /**
     * @var CatalogCar\CarType|null
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarType")
     * @ORM\JoinColumn(name="catalog_car_type_id", referencedColumnName="id_car_type", nullable=true)
     */
    private $catalogCarType;

    /**
     * @var CatalogCar\CarMark|null
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarMark")
     * @ORM\JoinColumn(name="catalog_car_mark_id", referencedColumnName="id_car_mark", nullable=true)
     */
    private $catalogCarMark;

    /**
     * @var CatalogCar\CarModel|null
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarModel")
     * @ORM\JoinColumn(name="catalog_car_model_id", referencedColumnName="id_car_model", nullable=true)
     */
    private $catalogCarModel;

    /**
     * @var CatalogCar\CarSerie|null
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarSerie")
     * @ORM\JoinColumn(name="catalog_car_serie_id", referencedColumnName="id_car_serie", nullable=true)
     */
    private $catalogCarSerie;

    /**
     * @var CatalogCar\CarGeneration|null
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarGeneration")
     * @ORM\JoinColumn(name="catalog_car_generation_id", referencedColumnName="id_car_generation", nullable=true)
     */
    private $catalogCarGeneration;

    /**
     * @var CatalogCar\CarModification|null
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarModification")
     * @ORM\JoinColumn(name="catalog_car_modification_id", referencedColumnName="id_car_modification", nullable=true)
     */
    private $catalogCarModification;

    /**
     * @var CatalogCar\CarEquipment|null
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\CatalogCar\CarEquipment")
     * @ORM\JoinColumn(name="catalog_car_equipment_id", referencedColumnName="id_car_equipment", nullable=true)
     */
    private $catalogCarEquipment;

    public function __construct()
    {
        $this->owners = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->mileages = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getVin()
    {
        return $this->vin;
    }

    /**
     * @param string $vin
     */
    public function setVin($vin)
    {
        $this->vin = $vin;
    }

    /**
     * @return string
     */
    public function getRegistrationSignNumber()
    {
        return $this->registrationSignNumber;
    }

    /**
     * @param string $registrationSignNumber
     */
    public function setRegistrationSignNumber($registrationSignNumber)
    {
        $this->registrationSignNumber = $registrationSignNumber;
    }

    /**
     * @return Collection
     */
    public function getOwners(): Collection
    {
        return $this->owners;
    }

    /**
     * @param Client $owner
     */
    public function addOwner(Client $owner)
    {
        $this->owners->add($owner);
    }

    /**
     * @param Client $owner
     */
    public function removeOwner(Client $owner)
    {
        $this->owners->removeElement($owner);
    }

    /**
     * @return Collection
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    /**
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        $this->orders->add($order);
    }

    /**
     * @param Order $order
     */
    public function removeOrder(Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * @return int|null
     */
    public function getYear(): ?int
    {
        return $this->year;
    }

    /**
     * @param int|null $year
     */
    public function setYear(?int $year): void
    {
        $this->year = $year;
    }

    /**
     * @return null|string
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param null|string $color
     */
    public function setColor(?string $color): void
    {
        $this->color = $color;
    }

    /**
     * @return CatalogCar\CarType|null
     */
    public function getCatalogCarType(): ?CatalogCar\CarType
    {
        return $this->catalogCarType;
    }

    /**
     * @param CatalogCar\CarType|null $catalogCarType
     */
    public function setCatalogCarType(?CatalogCar\CarType $catalogCarType): void
    {
        $this->catalogCarType = $catalogCarType;
    }

    /**
     * @return CatalogCar\CarMark|null
     */
    public function getCatalogCarMark(): ?CatalogCar\CarMark
    {
        return $this->catalogCarMark;
    }

    /**
     * @param CatalogCar\CarMark|null $catalogCarMark
     */
    public function setCatalogCarMark(?CatalogCar\CarMark $catalogCarMark): void
    {
        $this->catalogCarMark = $catalogCarMark;
    }

    /**
     * @return CatalogCar\CarModel|null
     */
    public function getCatalogCarModel(): ?CatalogCar\CarModel
    {
        return $this->catalogCarModel;
    }

    /**
     * @param CatalogCar\CarModel|null $catalogCarModel
     */
    public function setCatalogCarModel(?CatalogCar\CarModel $catalogCarModel): void
    {
        $this->catalogCarModel = $catalogCarModel;
    }

    /**
     * @return CatalogCar\CarGeneration|null
     */
    public function getCatalogCarGeneration(): ?CatalogCar\CarGeneration
    {
        return $this->catalogCarGeneration;
    }

    /**
     * @param CatalogCar\CarGeneration|null $catalogCarGeneration
     */
    public function setCatalogCarGeneration(?CatalogCar\CarGeneration $catalogCarGeneration): void
    {
        $this->catalogCarGeneration = $catalogCarGeneration;
    }

    /**
     * @return CatalogCar\CarSerie|null
     */
    public function getCatalogCarSerie(): ?CatalogCar\CarSerie
    {
        return $this->catalogCarSerie;
    }

    /**
     * @param CatalogCar\CarSerie|null $catalogCarSerie
     */
    public function setCatalogCarSerie(?CatalogCar\CarSerie $catalogCarSerie): void
    {
        $this->catalogCarSerie = $catalogCarSerie;
    }

    /**
     * @return CatalogCar\CarModification|null
     */
    public function getCatalogCarModification(): ?CatalogCar\CarModification
    {
        return $this->catalogCarModification;
    }

    /**
     * @param CatalogCar\CarModification|null $catalogCarModification
     */
    public function setCatalogCarModification(?CatalogCar\CarModification $catalogCarModification): void
    {
        $this->catalogCarModification = $catalogCarModification;
    }

    /**
     * @return CatalogCar\CarEquipment|null
     */
    public function getCatalogCarEquipment(): ?CatalogCar\CarEquipment
    {
        return $this->catalogCarEquipment;
    }

    /**
     * @param CatalogCar\CarEquipment|null $catalogCarEquipment
     */
    public function setCatalogCarEquipment(?CatalogCar\CarEquipment $catalogCarEquipment): void
    {
        $this->catalogCarEquipment = $catalogCarEquipment;
    }

    /**
     * @return Collection
     */
    public function getMileages(): Collection
    {
        return $this->mileages;
    }

    /**
     * @param CarMileage $carMileage
     */
    public function addMillage(CarMileage $carMileage)
    {
        $this->mileages->add($carMileage);
    }

    /**
     * @param CarMileage $carMileage
     */
    public function removeMillage(CarMileage $carMileage)
    {
        $this->mileages->removeElement($carMileage);
    }

    /**
     * @return string
     */
    public function getAssumeVin(): string
    {
        return $this->assumeVin;
    }

    /**
     * @param string $assumeVin
     */
    public function setAssumeVin(string $assumeVin): void
    {
        $this->assumeVin = $assumeVin;
    }

}
