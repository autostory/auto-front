<?php

namespace AH\Entity;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="firm")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity
 */
class Firm implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="firm_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\Office", mappedBy="firm")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $offices;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\Order", mappedBy="firm")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $orders;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="AH\Entity\Client", inversedBy="firms")
     * @ORM\JoinTable(name="firm_clients_linker",
     *      joinColumns={@ORM\JoinColumn(name="firm_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="client_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"id" = "DESC"})
     **/
    private $clients;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\Manager", mappedBy="firm")
     * @ORM\OrderBy({"id" = "DESC"})
     **/
    private $managers;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\Request", mappedBy="firm")
     * @ORM\OrderBy({"makeAt" = "ASC"})
     **/
    private $requests;

    /**
     * @var float
     *
     * @ORM\Column(name="points", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $points = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, unique=false)
     */
    private $active = false;

    public function __construct()
    {
        $this->offices = new ArrayCollection();
        $this->clients = new ArrayCollection();
        $this->managers = new ArrayCollection();
        $this->requests = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Collection
     */
    public function getOffices(): Collection
    {
        return $this->offices;
    }

    /**
     * @param Office $office
     * @return void
     */
    public function addOffice(Office $office)
    {
        $office->setFirm($this);

        $this->offices->add($office);
    }

    /**
     * @param Office $office
     * @return void
     */
    public function removeOffice(Office $office)
    {
        $this->offices->removeElement($office);
    }

    /**
     * @return Collection
     */
    public function getClients(): Collection
    {
        return $this->clients;
    }

    /**
     * @param Client $client
     */
    public function addClient(Client $client)
    {
        $this->clients->add($client);
    }

    /**
     * @param Client $client
     */
    public function removeClient(Client $client)
    {
        $this->clients->removeElement($client);
    }

    /**
     * @return Collection
     */
    public function getManagers(): Collection
    {
        return $this->managers;
    }

    /**
     * @param Manager $manager
     */
    public function addManager(Manager $manager)
    {
        $this->managers->add($manager);
    }

    /**
     * @param Manager $manager
     */
    public function removeManager(Manager $manager)
    {
        $this->managers->removeElement($manager);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
    }

    /**
     * @return Collection
     */
    public function getRequests(): Collection
    {
        return $this->requests;
    }

    /**
     * @param Request $request
     */
    public function addRequest(Request $request)
    {
        $this->requests->add($request);
    }

    /**
     * @param Request $request
     */
    public function removeRequest(Request $request)
    {
        $this->requests->removeElement($request);
    }

    /**
     * @return Collection
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    /**
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        $this->orders->add($order);
    }

    /**
     * @param Order $order
     */
    public function removeOrder(Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * @return float
     */
    public function getPoints(): float
    {
        return $this->points;
    }

    /**
     * @param float $points
     */
    public function setPoints(float $points): void
    {
        $this->points = $points;
    }
}
