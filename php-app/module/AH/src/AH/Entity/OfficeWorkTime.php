<?php

namespace AH\Entity;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="office_work_time")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity
 */
class OfficeWorkTime implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="office_work_time_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var Office
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Office", inversedBy="workTimes")
     * @ORM\JoinColumn(name="office_id", referencedColumnName="id", nullable=false)
     */
    private $office;

    /**
     * @var integer
     *
     * @ORM\Column(name="day_of_week", type="integer", nullable=false)
     */
    private $dayOfWeek;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="start_time", type="time", nullable=true)
     */
    private $start;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="end_time", type="time", nullable=true)
     */
    private $end;

    /**
     * @var boolean
     *
     * @ORM\Column(name="holiday", type="boolean", nullable=false)
     */
    private $holiday = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="round_the_clock", type="boolean", nullable=false)
     */
    private $roundTheClock = false;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Office
     */
    public function getOffice(): Office
    {
        return $this->office;
    }

    /**
     * @param Office $office
     */
    public function setOffice(Office $office): void
    {
        $this->office = $office;
    }

    /**
     * @return int
     */
    public function getDayOfWeek(): int
    {
        return $this->dayOfWeek;
    }

    /**
     * @param int $dayOfWeek
     */
    public function setDayOfWeek(int $dayOfWeek): void
    {
        $this->dayOfWeek = $dayOfWeek;
    }

    /**
     * @return bool
     */
    public function getHoliday(): bool
    {
        return $this->holiday;
    }

    /**
     * @param bool $holiday
     */
    public function setHoliday(bool $holiday): void
    {
        $this->holiday = $holiday;
    }

    /**
     * @return \DateTime|null
     */
    public function getStart(): ?\DateTime
    {
        return $this->start;
    }

    /**
     * @param \DateTime|null $start
     */
    public function setStart(?\DateTime $start): void
    {
        $this->start = $start;
    }

    /**
     * @return \DateTime|null
     */
    public function getEnd(): ?\DateTime
    {
        return $this->end;
    }

    /**
     * @param \DateTime|null $end
     */
    public function setEnd(?\DateTime $end): void
    {
        $this->end = $end;
    }

    /**
     * @return bool
     */
    public function getRoundTheClock(): bool
    {
        return $this->roundTheClock;
    }

    /**
     * @param bool $roundTheClock
     */
    public function setRoundTheClock(bool $roundTheClock)
    {
        $this->roundTheClock = $roundTheClock;
    }

}
