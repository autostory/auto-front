<?php

namespace AH\Entity\Event;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Ah\Entity\Basic;

/**
 * @ORM\Table(name="event_gas")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class GasEvent extends AbstractEvent implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="event_gas_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * Тип заправленного топлива
     *
     * @var Basic\FuelType
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Basic\FuelType")
     * @ORM\JoinColumn(name="fuel_type_id", referencedColumnName="id", nullable=false)
     */
    private $fuelType;

    /**
     * Сколько топлива было заправлено
     *
     * @var float
     *
     * @ORM\Column(name="fuel_amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $fuelAmount;

    /**
     * Заправка до полного бака или нет.
     *
     * @var boolean
     *
     * @ORM\Column(name="full_tank", type="boolean", nullable=false)
     */
    private $fullTank = false;

    /**
     * Стоимость за единицу топлива.
     *
     * @var float
     *
     * @ORM\Column(name="one_unit_cost", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $oneUnitCost;

    /**
     * Название АЗС.
     *
     * @var string
     *
     * @ORM\Column(name="gas_station_name", type="string", nullable=true, unique=false)
     */
    private $gasStationName = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Basic\FuelType
     */
    public function getFuelType(): ?Basic\FuelType
    {
        return $this->fuelType;
    }

    /**
     * @param Basic\FuelType $fuelType
     */
    public function setFuelType(Basic\FuelType $fuelType)
    {
        $this->fuelType = $fuelType;
    }

    /**
     * @return float
     */
    public function getOneUnitCost(): float
    {
        return $this->oneUnitCost;
    }

    /**
     * @param float $oneUnitCost
     */
    public function setOneUnitCost(float $oneUnitCost): void
    {
        $this->oneUnitCost = $oneUnitCost;
    }

    /**
     * @return float
     */
    public function getFuelAmount(): float
    {
        return $this->fuelAmount;
    }

    /**
     * @param float $fuelAmount
     */
    public function setFuelAmount(float $fuelAmount): void
    {
        $this->fuelAmount = $fuelAmount;
    }

    /**
     * @return string
     */
    public function getGasStationName(): ?string
    {
        return $this->gasStationName;
    }

    /**
     * @param string $gasStationName
     */
    public function setGasStationName(string $gasStationName = null): void
    {
        $this->gasStationName = $gasStationName;
    }

    /**
     * @return bool
     */
    public function getFullTank(): bool
    {
        return $this->fullTank;
    }

    /**
     * @param bool $fullTank
     */
    public function setFullTank(bool $fullTank): void
    {
        $this->fullTank = $fullTank;
    }
}
