<?php

namespace AH\Entity\Event;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\Entity\Car;
use AH\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use \DateTime;

abstract class AbstractEvent implements BlamableInterface
{
    use BlamableEntityTrait;
    /**
     * Инициатор события
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\User")
     * @ORM\JoinColumn(name="initiator_id", referencedColumnName="id", nullable=false)
     */
    protected $initiator;

    /**
     * Техника на которой произошло событие
     *
     * @var Car
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Car")
     * @ORM\JoinColumn(name="car_id", referencedColumnName="id", nullable=false)
     */
    protected $car;

    /**
     * Пробег техники на момент события
     *
     * @var integer
     *
     * @ORM\Column(name="mileage", type="integer", nullable=false)
     */
    protected $mileage;

    /**
     * Дата события
     *
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="datetimetz", nullable=true)
     */
    protected $date = null;

    /**
     * Сколько было затрачено на событие.
     *
     * @var float
     *
     * @ORM\Column(name="total_cost", type="decimal", precision=10, scale=2, nullable=false)
     */
    protected $totalCost;

    /**
     * Заметка к событию
     *
     * @var string|null
     *
     * @ORM\Column(name="note", type="string", nullable=true, unique=false)
     */
    protected $note = null;

    public function __construct()
    {
        $this->date = new DateTime();
    }

    /**
     * @return float
     */
    public function getTotalCost(): float
    {
        return $this->totalCost;
    }

    /**
     * @param float $totalCost
     */
    public function setTotalCost(float $totalCost)
    {
        $this->totalCost = $totalCost;
    }

    /**
     * @return User
     */
    public function getInitiator(): ?User
    {
        return $this->initiator;
    }

    /**
     * @param User $initiator
     */
    public function setInitiator(User $initiator): void
    {
        $this->initiator = $initiator;
    }

    /**
     * @return Car
     */
    public function getCar(): Car
    {
        return $this->car;
    }

    /**
     * @param Car $car
     */
    public function setCar(Car $car): void
    {
        $this->car = $car;
    }

    /**
     * @return int
     */
    public function getMileage(): int
    {
        return $this->mileage;
    }

    /**
     * @param int $mileage
     */
    public function setMileage(int $mileage): void
    {
        $this->mileage = $mileage;
    }

    /**
     * @return DateTime|null
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime|null $date
     */
    public function setDate(?DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return null|string
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param null|string $note
     */
    public function setNote(?string $note): void
    {
        $this->note = $note;
    }
}
