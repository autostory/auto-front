<?php

namespace AH\Entity\Event;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\Entity\Basic\GlobalServiceType;
use AH\Entity\Client;
use AH\Entity\Firm;
use AH\Entity\Office;
use AH\Entity\Service\ServiceWork;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Событие с запчастями
 *
 * @ORM\Table(name="event_spare_part_service")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SparePartServiceEvent extends AbstractEvent implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="event_spare_part_service_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * Фирма в которой было событие
     * Если null то событие добавлено самим клиентом, а не менеджером СТО.
     *
     * @var Firm|null
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Firm")
     * @ORM\JoinColumn(name="firm_id", referencedColumnName="id", nullable=true)
     */
    private $firm;


    /**
     * Офис в котором была оказана услуга
     *
     * @var Office|null
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Office")
     * @ORM\JoinColumn(name="office_id", referencedColumnName="id", nullable=true)
     */
    private $office;

    /**
     * Клиент которому была оказана этот сервис
     *
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Firm")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     */
    private $client;

    /**
     * Описание выполненных работ
     *
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="AH\Entity\Service\ServiceWork")
     * @ORM\JoinTable(name="spare_part_service_event_works",
     *      joinColumns={@ORM\JoinColumn(name="event_spare_part_service_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="service_work_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $serviceWorks;

    /**
     * Тип сервиса.
     *
     * ремонт\обслуживание - надо делать
     * тюнинг\допоборудование - можно и не делать вообще
     *
     * @var GlobalServiceType
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Basic\GlobalServiceType")
     * @ORM\JoinColumn(name="global_service_type_id", referencedColumnName="id", nullable=false)
     */
    private $globalServiceType;

    /**
     * Сервис оказал клиент сам себе.
     *
     * @var boolean
     *
     * @ORM\Column(name="self_service", type="boolean")
     */
    private $selfService = false;

    /**
     * Необходимость оказания услуги связана с ДТП
     *
     * @var boolean
     *
     * @ORM\Column(name="cause_of_accident", type="boolean")
     */
    private $causeOfAccident = false;

    /**
     * Название организации оказавшую услугу не из нашей системы, заполняется если не указана $firm.
     *
     * @var string|null
     *
     * @ORM\Column(name="organisation_name", type="string", nullable=true, unique=false)
     */
    private $organisationName = null;

    public function __construct()
    {
        parent::__construct();

        $this->serviceWorks = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Firm|null
     */
    public function getFirm(): ?Firm
    {
        return $this->firm;
    }

    /**
     * @param Firm|null $firm
     */
    public function setFirm(?Firm $firm): void
    {
        $this->firm = $firm;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient(Client $client): void
    {
        $this->client = $client;
    }

    /**
     * @return GlobalServiceType
     */
    public function getGlobalServiceType(): GlobalServiceType
    {
        return $this->globalServiceType;
    }

    /**
     * @param GlobalServiceType $globalServiceType
     */
    public function setGlobalServiceType(GlobalServiceType $globalServiceType): void
    {
        $this->globalServiceType = $globalServiceType;
    }

    /**
     * @return bool
     */
    public function getCauseOfAccident(): bool
    {
        return $this->causeOfAccident;
    }

    /**
     * @param bool $causeOfAccident
     */
    public function setCauseOfAccident(bool $causeOfAccident): void
    {
        $this->causeOfAccident = $causeOfAccident;
    }

    /**
     * @return null|string
     */
    public function getOrganisationName(): ?string
    {
        return $this->organisationName;
    }

    /**
     * @param null|string $organisationName
     */
    public function setOrganisationName(?string $organisationName): void
    {
        $this->organisationName = $organisationName;
    }

    /**
     * @return bool
     */
    public function getSelfService(): bool
    {
        return $this->selfService;
    }

    /**
     * @param bool $selfService
     */
    public function setSelfService(bool $selfService): void
    {
        $this->selfService = $selfService;
    }

    /**
     * @return Collection
     */
    public function getServiceWorks(): Collection
    {
        return $this->serviceWorks;
    }

    /**
     * @param ServiceWork $serviceWork
     * @return void
     */
    public function addServiceWork(ServiceWork $serviceWork): void
    {
        $this->serviceWorks->add($serviceWork);
    }

    /**
     * @param ServiceWork $serviceWork
     * @return void
     */
    public function removeServiceWork(ServiceWork $serviceWork): void
    {
        $this->serviceWorks->removeElement($serviceWork);
    }

    /**
     * @return Office|null
     */
    public function getOffice(): ?Office
    {
        return $this->office;
    }

    /**
     * @param Office|null $office
     */
    public function setOffice(?Office $office): void
    {
        $this->office = $office;
    }
}
