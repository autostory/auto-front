<?php

namespace AH\Entity;

use AH\DoctrineSubscriber\BlameSubscriber\BlamableEntityTrait;
use AH\DoctrineSubscriber\BlameSubscriber\BlamableInterface;
use AH\Entity\Service\ServiceMaterial;
use AH\Entity\Service\ServiceWork;
use AH\Graphql\Enum\OrderStatusEnumType;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="`client_order`")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AH\Repository\OrderRepository")
 */
class Order implements BlamableInterface
{
    use BlamableEntityTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\SequenceGenerator(sequenceName="client_order_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var Firm
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Firm", inversedBy="orders")
     * @ORM\JoinColumn(name="firm_id", referencedColumnName="id", nullable=false)
     */
    private $firm;

    /**
     * @var Office
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Office", inversedBy="orders")
     * @ORM\JoinColumn(name="office_id", referencedColumnName="id", nullable=false)
     */
    private $office;

    /**
     * @var CarMileage
     *
     * @ORM\OneToOne(targetEntity="AH\Entity\CarMileage", mappedBy="order")
     */
    private $mileage;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\BoxSchedule", mappedBy="order")
     * @ORM\OrderBy({"start" = "ASC"})
     */
    private $schedules;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\OrderPayment", mappedBy="order")
     */
    private $payments;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\Service\ServiceWork", mappedBy="order")
     */
    private $serviceWorks;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\Service\ServiceMaterial", mappedBy="order")
     */
    private $serviceMaterials;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Client", inversedBy="orders")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=true)
     */
    private $client;

    /**
     * @var Car
     *
     * @ORM\ManyToOne(targetEntity="AH\Entity\Car", inversedBy="orders")
     * @ORM\JoinColumn(name="car_id", referencedColumnName="id", nullable=true)
     */
    private $car;

    /**
     * @var Request|null
     *
     * @ORM\OneToOne(targetEntity="AH\Entity\Request", mappedBy="order")
     */
    private $request = null;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="phone_same_as_in_profile", type="boolean", nullable=false, unique=false)
     */
    private $phoneSameAsInProfile = false;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contact_name", type="string", length=255, nullable=true, unique=false)
     */
    private $contactName = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contact_phone", type="string", length=255, nullable=true, unique=false)
     */
    private $contactPhone = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contact_note", type="string", length=255, nullable=true, unique=false)
     */
    private $contactNote = null;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, unique=false)
     */
    private $active = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="client_visit_required", type="boolean", nullable=false, unique=false)
     */
    private $clientVisitRequired = false;

    /**
     * @var string|null
     *
     * @ORM\Column(name="order_reason", type="string", length=255, nullable=true, unique=false)
     */
    private $orderReason = null;

    /**
     * @var \DateTime|null
     *
     * Когда клиент пообещал приехать
     *
     * @ORM\Column(name="estimated_client_arrival_date", type="datetimetz", nullable=true)
     */
    private $estimatedClientArrivalDate;

    /**
     * @var \DateTime|null
     *
     * Когда фактически приехал клиент
     *
     * @ORM\Column(name="fact_client_visit_date_time", type="datetimetz", nullable=true)
     */
    private $factClientVisitDateTime;

    /**
     * @var \DateTime|null
     *
     * Когда фактически вернули авто клиенту
     *
     * @ORM\Column(name="fact_car_return_date_time", type="datetimetz", nullable=true)
     */
    private $factCarReturnDateTime;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="reject_reason_id", type="integer", nullable=true, unique=false)
     */
    private $rejectReasonId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="reject_reason_description", type="string", length=255, nullable=true, unique=false)
     */
    private $rejectReasonDescription;

    /**
     * Дата смены статуса заказа на отказ
     *
     * @var \DateTime|null
     *
     * @ORM\Column(name="reject_date", type="datetimetz", nullable=true)
     */
    private $rejectDate = null;

    /**
     * Дата смены статуса заказа на выполнено
     *
     * @var \DateTime|null
     *
     * @ORM\Column(name="done_date", type="datetimetz", nullable=true)
     */
    private $doneDate = null;

    public function __construct()
    {
        $this->schedules = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->serviceWorks = new ArrayCollection();
        $this->serviceMaterials = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Collection
     */
    public function getSchedules(): Collection
    {
        return $this->schedules;
    }

    /**
     * @param BoxSchedule $schedule
     */
    public function addSchedule(BoxSchedule $schedule): void
    {
        $this->schedules->add($schedule);
    }

    /**
     * @param BoxSchedule $schedule
     */
    public function removeSchedule(BoxSchedule $schedule): void
    {
        $this->schedules->removeElement($schedule);
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient(Client $client): void
    {
        $this->client = $client;
    }

    /**
     * @return Car
     */
    public function getCar(): Car
    {
        return $this->car;
    }

    /**
     * @param Car $car
     */
    public function setCar(Car $car): void
    {
        $this->car = $car;
    }

    /**
     * @return string
     */
    public function getContactName(): ?string
    {
        return $this->contactName;
    }

    /**
     * @param string $contactName
     */
    public function setContactName(?string $contactName): void
    {
        $this->contactName = $contactName;
    }

    /**
     * @return string
     */
    public function getContactPhone(): ?string
    {
        return $this->contactPhone;
    }

    /**
     * @param string $contactPhone
     */
    public function setContactPhone(?string $contactPhone): void
    {
        $this->contactPhone = $contactPhone;
    }

    /**
     * @return null|string
     */
    public function getContactNote(): ?string
    {
        return $this->contactNote;
    }

    /**
     * @param null|string $contactNote
     */
    public function setContactNote(?string $contactNote): void
    {
        $this->contactNote = $contactNote;
    }

    /**
     * @return Request|null
     */
    public function getRequest(): ?Request
    {
        return $this->request;
    }

    /**
     * @param Request|null $request
     */
    public function setRequest(?Request $request): void
    {
        $this->request = $request;
    }

    /**
     * @return Firm
     */
    public function getFirm(): Firm
    {
        return $this->firm;
    }

    /**
     * @param Firm $firm
     */
    public function setFirm(Firm $firm): void
    {
        $this->firm = $firm;
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return Collection
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    /**
     * @param OrderPayment $payment
     */
    public function addPayment(OrderPayment $payment): void
    {
        $this->payments->add($payment);
    }

    /**
     * @param OrderPayment $payment
     */
    public function removePayment(OrderPayment $payment): void
    {
        $this->payments->removeElement($payment);
    }

    /**
     * @return Collection
     */
    public function getServiceWorks(): Collection
    {
        return $this->serviceWorks;
    }

    /**
     * @param ServiceWork $serviceWork
     */
    public function addServiceWork(ServiceWork $serviceWork): void
    {
        $this->serviceWorks->add($serviceWork);
    }

    /**
     * @param ServiceWork $serviceWork
     */
    public function removeServiceWork(ServiceWork $serviceWork): void
    {
        $this->serviceWorks->removeElement($serviceWork);
    }

    /**
     * @return null|string
     */
    public function getOrderReason(): ?string
    {
        return $this->orderReason;
    }

    /**
     * @param null|string $orderReason
     */
    public function setOrderReason(?string $orderReason): void
    {
        $this->orderReason = $orderReason;
    }

    /**
     * @return Collection
     */
    public function getServiceMaterials(): Collection
    {
        return $this->serviceMaterials;
    }

    /**
     * @param ServiceMaterial $serviceMaterial
     */
    public function addServiceMaterial(ServiceMaterial $serviceMaterial): void
    {
        $this->serviceMaterials->add($serviceMaterial);
    }

    /**
     * @param ServiceMaterial $serviceMaterial
     */
    public function removeServiceMaterial(ServiceMaterial $serviceMaterial): void
    {
        $this->serviceMaterials->removeElement($serviceMaterial);
    }

    /**
     * @return CarMileage
     */
    public function getMillage(): CarMileage
    {
        return $this->mileage;
    }

    /**
     * @param CarMileage $mileage
     */
    public function setMillage(CarMileage $mileage): void
    {
        $this->mileage = $mileage;
    }

    /**
     * @return bool
     */
    public function getPhoneSameAsInProfile(): bool
    {
        return $this->phoneSameAsInProfile;
    }

    /**
     * @param bool $phoneSameAsInProfile
     */
    public function setPhoneSameAsInProfile(bool $phoneSameAsInProfile)
    {
        $this->phoneSameAsInProfile = $phoneSameAsInProfile;
    }

    /**
     * @return Office
     */
    public function getOffice(): Office
    {
        return $this->office;
    }

    /**
     * @param Office $office
     */
    public function setOffice(Office $office)
    {
        $this->office = $office;
    }

    /**
     * @return \DateTime|null
     */
    public function getEstimatedClientArrivalDate(): ?\DateTime
    {
        return $this->estimatedClientArrivalDate;
    }

    /**
     * @param \DateTime|null $estimatedClientArrivalDate
     */
    public function setEstimatedClientArrivalDate(?\DateTime $estimatedClientArrivalDate): void
    {
        $this->estimatedClientArrivalDate = $estimatedClientArrivalDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getFactClientVisitDateTime(): ?\DateTime
    {
        return $this->factClientVisitDateTime;
    }

    /**
     * @param \DateTime|null $factClientVisitDateTime
     */
    public function setFactClientVisitDateTime(?\DateTime $factClientVisitDateTime): void
    {
        $this->factClientVisitDateTime = $factClientVisitDateTime;
    }

    /**
     * @return \DateTime|null
     */
    public function getFactCarReturnDateTime(): ?\DateTime
    {
        return $this->factCarReturnDateTime;
    }

    /**
     * @param \DateTime|null $factCarReturnDateTime
     */
    public function setFactCarReturnDateTime(?\DateTime $factCarReturnDateTime): void
    {
        $this->factCarReturnDateTime = $factCarReturnDateTime;
    }

    /**
     * @return bool
     */
    public function isClientVisitRequired(): bool
    {
        return $this->clientVisitRequired;
    }

    /**
     * @param bool $clientVisitRequired
     */
    public function setClientVisitRequired(bool $clientVisitRequired): void
    {
        $this->clientVisitRequired = $clientVisitRequired;
    }

    /**
     * @return int|null
     */
    public function getRejectReasonId(): ?int
    {
        return $this->rejectReasonId;
    }

    /**
     * @param int|null $rejectReasonId
     */
    public function setRejectReasonId(?int $rejectReasonId): void
    {
        $this->rejectReasonId = $rejectReasonId;
    }

    /**
     * @return null|string
     */
    public function getRejectReasonDescription(): ?string
    {
        return $this->rejectReasonDescription;
    }

    /**
     * @param null|string $rejectReasonDescription
     */
    public function setRejectReasonDescription(?string $rejectReasonDescription): void
    {
        $this->rejectReasonDescription = $rejectReasonDescription;
    }

    /**
     * @return \DateTime|null
     */
    public function getRejectDate(): ?\DateTime
    {
        return $this->rejectDate;
    }

    /**
     * @param \DateTime|null $rejectDate
     */
    public function setRejectDate(?\DateTime $rejectDate): void
    {
        $this->rejectDate = $rejectDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getDoneDate(): ?\DateTime
    {
        return $this->doneDate;
    }

    /**
     * @param \DateTime|null $doneDate
     */
    public function setDoneDate(?\DateTime $doneDate): void
    {
        $this->doneDate = $doneDate;
    }
}
