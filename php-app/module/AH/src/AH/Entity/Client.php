<?php

namespace AH\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AH\Repository\ClientRepository")
 */
class Client extends User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="category", type="integer", nullable=false)
     */
    private $category = 0;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="AH\Entity\Car", inversedBy="owners")
     * @ORM\JoinTable(name="client_cars")
     */
    private $cars;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="AH\Entity\Firm", mappedBy="clients")
     */
    private $firms;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\Order", mappedBy="client")
     **/
    private $orders;

    /**
     * @var float
     *
     * @ORM\Column(name="points", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $points = 0;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\Request", mappedBy="client")
     **/
    private $requests;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AH\Entity\OrderPayment", mappedBy="client")
     */
    private $payments;

    public function __construct()
    {
        parent::__construct();

        $this->cars = new ArrayCollection();
        $this->firms = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->requests = new ArrayCollection();
        $this->payments = new ArrayCollection();
    }

    /**
     * Get userType
     *
     * @return string
     */
    public function getUserType()
    {
        return 'client';
    }

    /**
     * @return Collection
     */
    public function getCars(): Collection
    {
        return $this->cars;
    }

    /**
     * @param Car $car
     */
    public function addCar(Car $car)
    {
        $this->cars->add($car);
    }

    /**
     * @param Car $car
     */
    public function removeCar(Car $car)
    {
        $this->cars->removeElement($car);
    }

    /**
     * @return Collection
     */
    public function getFirms(): Collection
    {
        return $this->firms;
    }

    /**
     * @param Firm $firm
     */
    public function addFirm(Firm $firm)
    {
        $this->firms->add($firm);
    }

    /**
     * @param Firm $firm
     */
    public function removeFirm(Firm $firm)
    {
        $this->firms->removeElement($firm);
    }

    /**
     * @return Collection
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    /**
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        $this->orders->add($order);
    }

    /**
     * @param Order $order
     */
    public function removeOrder(Order $order)
    {
        $this->orders->removeElement($order);
    }
    
    /**
     * @return Collection
     */
    public function getRequests(): Collection
    {
        return $this->requests;
    }

    /**
     * @param Request $request
     */
    public function addRequest(Request $request)
    {
        $this->requests->add($request);
    }

    /**
     * @param Request $request
     */
    public function removeRequest(Request $request)
    {
        $this->requests->removeElement($request);
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category)
    {
        $this->category = $category;
    }

    /**
     * @return float
     */
    public function getPoints(): float
    {
        return $this->points;
    }

    /**
     * @param float $points
     */
    public function setPoints(float $points)
    {
        $this->points = $points;
    }

    /**
     * @return Collection
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    /**
     * @param OrderPayment $payment
     */
    public function addPayment(OrderPayment $payment): void
    {
        $this->payments->add($payment);
    }

    /**
     * @param OrderPayment $payment
     */
    public function removePayment(OrderPayment $payment): void
    {
        $this->payments->removeElement($payment);
    }
}
