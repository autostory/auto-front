<?php

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => [
                    'route' => '',
                    'defaults' => [
                        'controller' => 'AH\Controller\Index',
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'doctrine' => [
        'driver' => [
            'ah_driver' => [
                'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/AH/Entity',
                ],
            ],
            'orm_default' => [
                'drivers' => [
                    'AH\Entity' => 'ah_driver',
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            'AH\Core\Adapter\CustomOAuth2PdoAdapter' => 'AH\Core\Adapter\CustomOAuth2PdoAdapterFactory',
            'AH\Core\AuthenticationService' => 'AH\Core\AuthenticationServiceFactory',
            'AH\Core\Provider\Identity\AuthenticationIdentityProvider' => 'AH\Core\Provider\AuthenticationIdentityProviderFactory',
            'AH\DoctrineSubscriber\BlameSubscriber' => 'AH\DoctrineSubscriber\BlameSubscriber\BlameSubscriberFactory',
            'AH\Core\Provider\AuthenticationIdentityProvider' => 'AH\Core\Provider\AuthenticationIdentityProviderFactory',
            'AH\Core\Service\PasswordRestoreService' => 'AH\Core\Service\PasswordRestoreServiceFactory',
            'AH\Core\Service\EventLogService' => 'AH\Core\Service\EventLogServiceFactory',
            'AH\Core\Mail\MailService' => 'AH\Core\Mail\MailServiceFactory',
            'AH\Core\Service\OAuth2Service' => 'AH\Core\Service\OAuth2ServiceFactory',
            'AH\Core\Service\GooGlShorter' => 'AH\Core\Service\GooGlShorterFactory',
            'AH\Core\Service\ClientAutoLoginLinkGenerator' => 'AH\Core\Service\ClientAutoLoginLinkGeneratorFactory',

            'AH\Core\Acl\OfficeAccess' => 'AH\Core\Acl\OfficeAccessFactory',
            'AH\Core\Acl\OfficeBoxAccess' => 'AH\Core\Acl\OfficeBoxAccessFactory',
            'AH\Core\Acl\FirmAccess' => 'AH\Core\Acl\FirmAccessFactory',
            'AH\Core\Acl\CarAccess' => 'AH\Core\Acl\CarAccessFactory',
            'AH\Core\Acl\ClientAccess' => 'AH\Core\Acl\ClientAccessFactory',
            'AH\Core\SMS\SMSService' => 'AH\Core\SMS\SMSServiceFactory',

            'AH\Core\InputFilter\ClientAuthInputFilter' => 'AH\Core\InputFilter\ClientAuthInputFilterFactory',
            'AH\Core\InputFilter\ManagerAuthInputFilter' => 'AH\Core\InputFilter\ManagerAuthInputFilterFactory',
            'AH\Core\InputFilter\ClientRegistrationPhoneInputFilter' => 'AH\Core\InputFilter\ClientRegistrationPhoneInputFilterFactory',
            'AH\Core\InputFilter\ManagerRegistrationPhoneInputFilter' => 'AH\Core\InputFilter\ManagerRegistrationPhoneInputFilterFactory',
            'AH\Core\InputFilter\MyMobilePhoneInputFilter' => 'AH\Core\InputFilter\MyMobilePhoneInputFilterFactory',
            'AH\Core\InputFilter\MyEmailFilter' => 'AH\Core\InputFilter\MyEmailFilterFactory',
            'AH\Core\InputFilter\PhoneInputFilter' => 'AH\Core\InputFilter\PhoneInputFilterFactory',
            'AH\Core\InputFilter\NewFirmAndManagerInputFilter' => 'AH\Core\InputFilter\NewFirmAndManagerInputFilterFactory',
            'AH\Core\InputFilter\IdentityFullNameFilter' => 'AH\Core\InputFilter\IdentityFullNameFilterFactory',
            'AH\Core\Service\GetOrderStatusForCurrentEntityStateService' => 'AH\Core\Service\GetOrderStatusForCurrentEntityStateServiceFactory',
        ],
        'invokables' => [
            'AH\Core\Relation\RelationDeterminer' => 'AH\Core\Relation\RelationDeterminer',
            'AH\Core\Validator\PasswordStrengthValidator' => 'AH\Core\Validator\PasswordStrengthValidator',
            'AH\Core\Validator\FieldEqValidator' => 'AH\Core\Validator\FieldEqValidator',
            'AH\Core\Service\PasswordGenerator' => 'AH\Core\Service\PasswordGenerator',
            'AH\DoctrineSubscriber\OrderStatusChangeSubscriber' => 'AH\DoctrineSubscriber\OrderStatusChangeSubscriber\OrderStatusChangeSubscriber'
        ],
        'aliases' => [
            'AuthenticationService' => 'AH\Core\AuthenticationService',
            'NewFirmAndManagerInputFilter' => 'AH\Core\InputFilter\NewFirmAndManagerInputFilter',
            'ClientAuthInputFilter' => 'AH\Core\InputFilter\ClientAuthInputFilter',
            'ManagerAuthInputFilter' => 'AH\Core\InputFilter\ManagerAuthInputFilter',
            'IdentityFullNameFilter' => 'AH\Core\InputFilter\IdentityFullNameFilter',
            'ClientRegistrationPhoneInputFilter' => 'AH\Core\InputFilter\ClientRegistrationPhoneInputFilter',
            'ManagerRegistrationPhoneInputFilter' => 'AH\Core\InputFilter\ManagerRegistrationPhoneInputFilter',
            'MyMobilePhoneInputFilter' => 'AH\Core\InputFilter\MyMobilePhoneInputFilter',
            'MyEmailFilter' => 'AH\Core\InputFilter\MyEmailFilter',
            'PhoneInputFilter' => 'AH\Core\InputFilter\PhoneInputFilter',
            'GetOrderStatusForCurrentEntityStateService' => 'AH\Core\Service\GetOrderStatusForCurrentEntityStateService',
            'SMSService' => 'AH\Core\SMS\SMSService',
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
