<?php

use AH\Entity\User;
use Zend\ServiceManager\ServiceManager;
use Zend\Mvc\Service\ServiceManagerConfig;
use \GraphQL\GraphQL;
use \GraphQL\Error\Debug;
use \GraphQL\Error\FormattedError;
use AH\Graphql\Schema\GlobalSchema;
use \OAuth2\Storage\Pdo as OAuth2Storage;
use GraphQL\Validator\Rules\DisableIntrospection;
use GraphQL\Validator\DocumentValidator;
//use GraphQL\Doctrine\DefaultFieldResolver;

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

// Setup autoloading
require 'init_autoloader.php';

$requestURI = $_SERVER['REQUEST_URI'];
if (!empty($requestURI) && strpos('/oauth', $requestURI) === 0) {
    Zend\Mvc\Application::init(require 'config/application.config.php')->run();
    die();
}

$configuration = require 'config/application.config.php';
$smConfig = isset($configuration['service_manager']) ? $configuration['service_manager'] : [];
$smConfig = new ServiceManagerConfig($smConfig);
$serviceManager = new ServiceManager();
$smConfig->configureServiceManager($serviceManager);
$serviceManager->setService('ApplicationConfig', $configuration);
$serviceManager->get('ModuleManager')->loadModules();

$module = new \AH\Module();
$module->onBootstrap(null, $serviceManager);

$debug = getenv('AH_GRAPHQL_DEBUG') === 'on';
if ($debug) {
    ini_set('display_errors', true);
    ini_set('display_startup_errors', true);
}
if (!empty($debug)) {
    set_error_handler(function ($severity, $message, $file, $line) use (&$phpErrors) {
        throw new ErrorException($message, 0, $severity, $file, $line);
    });
    $debug = Debug::INCLUDE_DEBUG_MESSAGE | Debug::INCLUDE_TRACE;
}

try {
    /** @var Doctrine\ORM\EntityManager $entityManager */
    $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

    $oauthPdo = new OAuth2Storage($entityManager->getConnection()->getWrappedConnection());
    $user = null;
    if (isset($_COOKIE['accessToken']) && !empty($_COOKIE['accessToken'])) {
        $tokenInfo = $oauthPdo->getAccessToken($_COOKIE['accessToken']);
        $userId = $tokenInfo['user_id'];
        $rUser = $entityManager->getRepository(User::class);
        $user = $rUser->findOneBy([
            'id' => $userId,
            'active' => true,
        ]);

        if (!empty($user)) {
            /** @var \Zend\Authentication\AuthenticationService $authenticationService */
            $authenticationService = $serviceManager->get('AuthenticationService');

            /** @var \AH\Core\Adapter\AuthenticatedIdentityAdapter $authenticationAdapter */
            $authenticationAdapter = $authenticationService->getAdapter();
            $authenticationAdapter->setUserId($user->getId());
            $authenticationService->authenticate();

            $entityManager->getConnection()->exec(
                sprintf(
                    'set session "myapp.user_id" = "%d";',
                    intval($user->getId())
                )
            );
        }
    }

    if (empty($user)) {
        $entityManager->getConnection()->exec(sprintf('set session "myapp.user_id" = "0";'));
    }

    if (isset($_SERVER['CONTENT_TYPE']) && strpos($_SERVER['CONTENT_TYPE'], 'application/json') !== false) {
        $raw = file_get_contents('php://input') ?: '';
        $data = json_decode($raw, true);
    } else {
        $data = $_REQUEST;
    }
    $data += ['query' => null, 'variables' => null];

//    GraphQL::setDefaultFieldResolver(new DefaultFieldResolver());

    $schema = new GlobalSchema();

    if (!$debug) {
        // Отключаем палево схемы graphql
        DocumentValidator::addRule(new DisableIntrospection());
    }

    $result = GraphQL::executeQuery(
        $schema->getSchema($serviceManager),
        $data['query'],
        $user,
        $serviceManager,
        (array)$data['variables']
    );

    $output = $result->toArray($debug);

    if (!empty($output['errors'])) {
        error_log(json_encode($output['errors']));
    }

    $httpStatus = 200;
} catch (\Exception $error) {
    $httpStatus = 500;
    if ($debug) {
        $output['errors'] = [
            'errors' => [
                'message' => FormattedError::createFromException($error, true),
            ],
        ];
    } else {
        $output['errors'] = [
            FormattedError::createFromException($error, false)
        ];
    }

    error_log(json_encode(FormattedError::createFromException($error, true)));
}

header('Content-Type: application/json', true, $httpStatus);
echo json_encode($output);